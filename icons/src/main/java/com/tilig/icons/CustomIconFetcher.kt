package com.tilig.icons

import android.graphics.Bitmap
import android.util.Base64
import com.bumptech.glide.Priority
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.data.DataFetcher
import com.bumptech.glide.load.data.DataFetcher.DataCallback
import com.tilig.icons.format.IconFormat
import com.tilig.icons.format.PNG
import com.tilig.icons.format.SVG
import com.tilig.icons.format.ICO

class CustomIconFetcher(private val icon: Icon) : DataFetcher<Bitmap> {

    fun loadSync(callback: (Bitmap?) -> Unit) {
        loadData(Priority.IMMEDIATE, object : DataCallback<Bitmap> {
            override fun onDataReady(data: Bitmap?) {
                callback.invoke(data)
            }

            override fun onLoadFailed(e: Exception) {
                callback.invoke(null)
            }
        })
    }

    override fun loadData(priority: Priority, callback: DataCallback<in Bitmap>) {
        val iconFormat: IconFormat? = when {
            icon.mime == ICO.MIME -> ICO(icon)
            icon.mime == SVG.MIME -> SVG(icon)
            icon.mime == PNG.MIME -> PNG(icon)
            else -> null
        }

        iconFormat?.apply {
            val data = getDataString()
            val bitmap: Bitmap?
            if (isBase64()) {
                val raw = Base64.decode(data, Base64.DEFAULT)
                bitmap = generateBitmap(raw)
            } else {
                bitmap = generateBitmap(data)
            }

            callback.onDataReady(bitmap)
        } ?: callback.onLoadFailed(IllegalArgumentException())
    }

    override fun getDataClass(): Class<Bitmap> = Bitmap::class.java

    override fun cleanup() {
        // no-op
    }

    override fun getDataSource(): DataSource = DataSource.LOCAL

    override fun cancel() {
        // no-op
    }
}