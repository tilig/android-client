@file:Suppress("unused")

package com.tilig.icons

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import androidx.annotation.Keep
import com.bumptech.glide.Glide
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.LibraryGlideModule

@Keep
@GlideModule
class CustomIconGlideModule : LibraryGlideModule() {

    init {
        //Log.d("Glide", "I'm here! Hi bumptech")
    }

    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        registry.prepend(
            Icon::class.java,
            Bitmap::class.java,
            CustomIconLoaderFactory()
        )
    }
}