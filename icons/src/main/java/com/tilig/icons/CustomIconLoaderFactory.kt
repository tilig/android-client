package com.tilig.icons

import android.graphics.Bitmap
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.ModelLoaderFactory
import com.bumptech.glide.load.model.MultiModelLoaderFactory

class CustomIconLoaderFactory : ModelLoaderFactory<Icon, Bitmap> {

    override fun build(unused: MultiModelLoaderFactory): ModelLoader<Icon, Bitmap> {
        return CustomIconLoader()
    }

    override fun teardown() {
        // Do nothing.
    }
}