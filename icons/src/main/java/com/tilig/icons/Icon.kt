package com.tilig.icons

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Icon(
    val data: String,
    val mime: String,
    val uri: String
) : Parcelable
