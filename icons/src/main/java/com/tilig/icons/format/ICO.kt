package com.tilig.icons.format

import android.graphics.Bitmap
import com.tilig.icons.Icon

class ICO(val icon: Icon) : IconFormat {

    companion object {

        const val MIME = "image/x-icon"

    }

    // .ICO
    // data "data:image/x-icon;base64,"
    // "ext":"ico"
    // "mime":"image/x-icon"
    // "uri":"data:image/x-icon;base64,"

    override fun isBase64(): Boolean = true

    override fun getDataString(): String = icon.data.substring(icon.uri.length)

    override fun generateBitmap(raw: ByteArray): Bitmap? {
        // FIXME Re-enable this once we have a new .ico decoder
        /* try {
            val images: List<Bitmap> = ICODecoder.read(ByteArrayInputStream(raw))
            // The last one in the list is (usually?) the largest
            return images.last()
        } catch (e : EOFException) {
            Log.e("ICO", "Error in icon file ${icon.uri}")
        } */
        return null
    }

    override fun generateBitmap(raw: String): Bitmap? = null

}
