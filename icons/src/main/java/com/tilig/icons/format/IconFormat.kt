package com.tilig.icons.format

import android.graphics.Bitmap
import com.tilig.icons.Icon

interface IconFormat {

    fun isBase64() : Boolean

    fun getDataString(): String

    fun generateBitmap(raw: ByteArray): Bitmap?

    fun generateBitmap(raw: String): Bitmap?

}