package com.tilig.icons.format

import android.graphics.Bitmap
import com.tilig.icons.Icon

class SVG(val icon: Icon) : IconFormat {

    companion object {
        const val MIME = "image/svg+xml"
    }

    // SVG
    // "ext":"svg"
    // "mime":"image/svg+xml"
    // "uri":"data:image/svg+xml;base64,"
    // "data": <svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fas\"

    override fun isBase64(): Boolean = false

    override fun getDataString(): String = icon.data

    override fun generateBitmap(raw: ByteArray): Bitmap? = null

    override fun generateBitmap(raw: String): Bitmap? {
//        val bitmap = Bitmap.createBitmap(32, 32, Bitmap.Config.ARGB_8888)
//        val canvas = Canvas(bitmap)
//        val pic = Sharp.loadString(raw).sharpPicture
//        pic.picture.draw(canvas)
//        return bitmap
        return null
    }

}
