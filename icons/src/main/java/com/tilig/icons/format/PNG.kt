package com.tilig.icons.format

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.tilig.icons.Icon

class PNG(val icon: Icon) : IconFormat {

    companion object {

        const val MIME = "image/png"

    }

    // PNG
    // Data: "data:image/x-icon;base64, ......"
    // "ext":"png"
    // "mime":"image/png"
    // "uri":"data:image/png;base64,"

    override fun isBase64(): Boolean = true

    override fun getDataString(): String = icon.data.substring(icon.uri.length)

    override fun generateBitmap(raw: ByteArray): Bitmap =
        BitmapFactory.decodeByteArray(raw, 0, raw.size)

    override fun generateBitmap(raw: String): Bitmap? = null

}
