package com.tilig.icons

import android.graphics.Bitmap
import com.bumptech.glide.load.Options
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.signature.ObjectKey
import com.tilig.icons.format.PNG
import com.tilig.icons.format.ICO

class CustomIconLoader : ModelLoader<Icon, Bitmap> {

    override fun buildLoadData(
        model: Icon,
        width: Int,
        height: Int,
        options: Options
    ): ModelLoader.LoadData<Bitmap>? = ModelLoader.LoadData(
        ObjectKey(model.uri),
        CustomIconFetcher(model)
    )

    fun loadSync(model: Icon, callback: (Bitmap?) -> Unit) =
        CustomIconFetcher(model).loadSync(callback)

    override fun handles(model: Icon): Boolean =
        model.mime == ICO.MIME
                // || model.mime == SVG.MIME
                || model.mime == PNG.MIME

}