package com.tilig.autofilltest

import android.content.Intent
import android.os.Bundle
import android.view.autofill.AutofillManager
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class LoginPlainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_plain)

        val bt_fill = findViewById<Button>(R.id.bt_fill)
        val bt_clear = findViewById<Button>(R.id.bt_clear)
        val bt_save = findViewById<Button>(R.id.bt_save)
        val et_email_1 = findViewById<EditText>(R.id.et_login_id_1)
        val et_email_2 = findViewById<EditText>(R.id.et_login_id_2)
        val et_username_1 = findViewById<EditText>(R.id.et_user_1)
        val et_username_2 = findViewById<EditText>(R.id.et_user_2)
        val et_password_1 = findViewById<EditText>(R.id.et_secret_1)
        val et_password_2 = findViewById<EditText>(R.id.et_secret_2)
        val et_password_3 = findViewById<EditText>(R.id.et_secret_3)

        bt_fill.setOnClickListener {
            val afm = getSystemService(AutofillManager::class.java)
            afm?.requestAutofill(et_email_1)
        }

        bt_clear.setOnClickListener {
            et_username_1.text.clear()
            et_username_2.text.clear()
            et_email_1.text.clear()
            et_email_2.text.clear()
            et_password_1.text.clear()
            et_password_2.text.clear()
            et_password_3.text.clear()
        }

        bt_save.setOnClickListener {
            val afm = getSystemService(AutofillManager::class.java)
            afm?.commit()
            startActivity(Intent(this, DummyActivity::class.java))
            finish()
        }
    }
}
