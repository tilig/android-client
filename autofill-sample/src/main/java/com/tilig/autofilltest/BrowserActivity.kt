package com.tilig.autofilltest

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity


class BrowserActivity : AppCompatActivity() {

    private lateinit var webView: WebView

    companion object {
        const val EXTRA_URL = "url"
        const val EXTRA_GRAB_FOCUS = "autofocus"

        fun launch(context: Context, url: String, autofocus: Boolean = false) = context.startActivity(
            Intent(
                context,
                BrowserActivity::class.java
            ).apply {
                putExtra(EXTRA_URL, url)
                putExtra(EXTRA_GRAB_FOCUS, autofocus)
            }
        )
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        webView = WebView(this)
        webView.settings.javaScriptEnabled = true
        intent.getStringExtra(EXTRA_URL)?.let {
            webView.loadUrl(
                it
            )
            webView.webViewClient = object : WebViewClient() {
                var finishedOnce = false
                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    if (!finishedOnce) {
                        finishedOnce = true
                        grabFocus()
                    }
                }
            }
        }
        setContentView(webView)
    }

    private fun grabFocus() {
        if (intent.getBooleanExtra(EXTRA_GRAB_FOCUS, false)) {
            webView.requestFocus()
            // Hacky, but necessary
            // https://stackoverflow.com/a/27871439
            webView.loadUrl("javascript:document.getElementsByName('email')[0].focus();")
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(webView, InputMethodManager.SHOW_IMPLICIT)
        }
    }
}
