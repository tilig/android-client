package com.tilig.autofilltest

import android.os.Build
import android.os.Bundle
import android.widget.Button
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity

class DummyActivity : AppCompatActivity() {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dummy)

        val bt_return = findViewById<Button>(R.id.bt_return)

        bt_return.setOnClickListener {
            //startActivity(Intent(this@DummyActivity, LoginActivity::class.java))
            finish()
        }
    }

}
