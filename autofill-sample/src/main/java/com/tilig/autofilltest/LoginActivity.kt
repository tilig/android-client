package com.tilig.autofilltest

import android.content.Intent
import android.os.Bundle
import android.view.autofill.AutofillManager
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val bt_fill = findViewById<Button>(R.id.bt_fill)
        val bt_clear = findViewById<Button>(R.id.bt_clear)
        val bt_save = findViewById<Button>(R.id.bt_save)
        val et_username = findViewById<EditText>(R.id.et_username)
        val et_email = findViewById<EditText>(R.id.et_login_id_1)
        val et_password = findViewById<EditText>(R.id.et_password)

        bt_fill.setOnClickListener {
            val afm = getSystemService(AutofillManager::class.java)
            afm?.requestAutofill(et_password)
        }

        bt_clear.setOnClickListener {
            et_username.text.clear()
            et_email.text.clear()
            et_password.text.clear()
        }

        bt_save.setOnClickListener {
            val afm = getSystemService(AutofillManager::class.java)
            afm?.commit()
            startActivity(Intent(this, DummyActivity::class.java))
            finish()
        }
    }
}
