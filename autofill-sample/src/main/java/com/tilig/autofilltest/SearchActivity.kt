package com.tilig.autofilltest

import android.content.Intent
import android.os.Bundle
import android.view.autofill.AutofillManager
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class SearchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        val bt_clear = findViewById<Button>(R.id.bt_clear)
        val bt_search = findViewById<Button>(R.id.bt_search)
        val et_search = findViewById<EditText>(R.id.et_search)
        val et_search_2 = findViewById<EditText>(R.id.et_search_2)

        bt_clear.setOnClickListener {
            et_search.text.clear()
            et_search_2.text.clear()
        }

        // Note: in this activity specifically, autofill/autosave should NOT trigger
        bt_search.setOnClickListener {
            startActivity(Intent(this, DummyActivity::class.java))
            finish()
        }
    }
}
