package com.tilig.autofilltest

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class ListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        findViewById<Button>(R.id.bt_settings_dev).setOnClickListener {
            startActivityForResult(Intent(Settings.ACTION_REQUEST_SET_AUTOFILL_SERVICE).apply {
                data = Uri.parse("package:com.tilig.android.debug")
            }, 1)
        }
        findViewById<Button>(R.id.bt_settings_prod).setOnClickListener {
            startActivityForResult(Intent(Settings.ACTION_REQUEST_SET_AUTOFILL_SERVICE).apply {
                data = Uri.parse("package:com.tilig.android")
            }, 1)
        }

        findViewById<Button>(R.id.bt_login).setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }
        findViewById<Button>(R.id.bt_login_plain).setOnClickListener {
            startActivity(Intent(this, LoginPlainActivity::class.java))
        }
        findViewById<Button>(R.id.bt_signup).setOnClickListener {
            startActivity(Intent(this, SignupActivity::class.java))
        }
        findViewById<Button>(R.id.bt_browser).setOnClickListener {
            startActivity(Intent(this, BrowserListActivity::class.java))
        }
        findViewById<Button>(R.id.bt_otp).setOnClickListener {
            startActivity(Intent(this, OTPActivity::class.java))
        }
        findViewById<Button>(R.id.bt_search).setOnClickListener {
            startActivity(Intent(this, SearchActivity::class.java))
        }
        findViewById<Button>(R.id.bt_links).setOnClickListener {
            startActivity(Intent(this, LinkActivity::class.java))
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Toast.makeText(this, "AutoFill provider set", Toast.LENGTH_SHORT).show()
    }
}
