package com.tilig.autofilltest

import android.content.Intent
import android.os.Bundle
import android.view.autofill.AutofillManager
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class OTPActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)

        val bt_fill = findViewById<Button>(R.id.bt_fill)
        val bt_clear = findViewById<Button>(R.id.bt_clear)
        val bt_save = findViewById<Button>(R.id.bt_save)
        val et_otp = findViewById<EditText>(R.id.et_otp)

        bt_fill.setOnClickListener {
            val afm = getSystemService(AutofillManager::class.java)
            afm?.requestAutofill(et_otp)
        }

        bt_clear.setOnClickListener {
            et_otp.text.clear()
        }

        // Note: in this activity specifically, the OTP code should NOT be saved
        bt_save.setOnClickListener {
            val afm = getSystemService(AutofillManager::class.java)
            afm?.commit()
            startActivity(Intent(this, DummyActivity::class.java))
            finish()
        }
    }
}
