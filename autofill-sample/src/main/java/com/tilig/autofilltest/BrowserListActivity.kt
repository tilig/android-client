package com.tilig.autofilltest

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class BrowserListActivity : AppCompatActivity() {

    companion object {
        private const val URL_LOGIN_1 = "file:///android_asset/index-login.html"
        private const val URL_LOGIN_2 = "file:///android_asset/index-login-2.html"
        private const val URL_LOGIN_3 = "file:///android_asset/index-login-3.html"
        private const val URL_LOGIN_4 = "file:///android_asset/index-login-4.html"
        private const val URL_LOGIN_5 = "file:///android_asset/index-login-5.html"
        private const val URL_LOGIN_6 = "file:///android_asset/index-login-6.html"
        private const val URL_SIGNUP_1 = "file:///android_asset/index-signup.html"
        private const val URL_SIGNUP_2 = "file:///android_asset/index-signup-2.html"
        private const val URL_UPDATE = "file:///android_asset/index-update.html"
        private const val URL_2FA = "file:///android_asset/index-2fa-1.html"
        private const val URL_TWO_STEP = "file:///android_asset/index-twostep-1.html"
        private const val URL_SEARCH = "file:///android_asset/index-search.html"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_browser_list)

        val links = hashMapOf(
            R.id.bt_login_1 to URL_LOGIN_1,
            R.id.bt_login_2 to URL_LOGIN_2,
            R.id.bt_login_3 to URL_LOGIN_3,
            R.id.bt_login_4 to URL_LOGIN_4,
            R.id.bt_login_5 to URL_LOGIN_5,
            R.id.bt_login_6 to URL_LOGIN_6,
            R.id.bt_signup_1 to URL_SIGNUP_1,
            R.id.bt_signup_2 to URL_SIGNUP_2,
            R.id.bt_update_1 to URL_UPDATE,
            R.id.bt_2fa to URL_2FA,
            R.id.bt_twosteps to URL_TWO_STEP,
            R.id.bt_search to URL_SEARCH,
        )

        links.keys.forEach { id ->
            findViewById<Button>(id).setOnClickListener {
                BrowserActivity.launch(
                    this@BrowserListActivity,
                    links[id]!!,
                    autofocus = id == R.id.bt_login_5
                )
            }
        }
    }
}
