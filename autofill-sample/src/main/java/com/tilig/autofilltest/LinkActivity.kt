package com.tilig.autofilltest

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.view.autofill.AutofillManager
import android.widget.Button
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.browser.customtabs.CustomTabsIntent
import com.google.android.material.checkbox.MaterialCheckBox

class LinkActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_links)

        val cb_internal_tab = findViewById<MaterialCheckBox>(R.id.cb_internal_tab)

        val prefs = getSharedPreferences("tilig_sample", Context.MODE_PRIVATE)

        arrayOf<View>(
            findViewById(R.id.link_formtest_1),
            findViewById(R.id.link_formtest_2),
            findViewById(R.id.link_facebook),
            findViewById(R.id.link_marktplaats),
            findViewById(R.id.link_123inkt)
        ).forEach {
            it.setOnClickListener { v ->
                val uri = Uri.parse(v.tag.toString())

                if (cb_internal_tab.isChecked) {
                    val builder = CustomTabsIntent.Builder()
                    val customTabsIntent :CustomTabsIntent  = builder.build()
                    customTabsIntent.launchUrl(this@LinkActivity, uri)
                } else {
                    val intent = Intent(Intent.ACTION_VIEW).apply {
                        data = uri
                    }
                    startActivity(intent)
                }
            }
        }

        cb_internal_tab.setOnCheckedChangeListener { button, checked ->
            setCheckboxText(button, checked)
            prefs.edit().putBoolean("internal_tab", checked).apply()
        }
        setCheckboxText(cb_internal_tab, prefs.getBoolean("internal_tab", false))
    }

    private fun setCheckboxText(button: CompoundButton, checked: Boolean) {
        button.text = getString(
            if (checked)
                R.string.open_in_tab
            else
                R.string.open_in_browser
        )
    }
}
