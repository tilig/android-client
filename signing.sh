#!/bin/bash

mkdir -p .signing
echo -n ${KEYSTORE} | base64 -d > .signing/tilig-release.jks
echo "tilig_release_storePassword=${STORE_PASSWORD}" > .signing/tilig.properties
echo "tilig_release_keyAlias=${KEY_ALIAS}" >> .signing/tilig.properties
echo "tilig_release_keyPassword=${KEY_PASSWORD}" >> .signing/tilig.properties
echo ${FIREBASE_DEV_DISTRIBUTION} | base64 -d > .signing/tilig-dev-13dd41b27573.json
