#!/usr/bin/env python3
#
# This script takes the related-domains.json from
# https://github.com/apple/password-manager-resources/blob/main/quirks/websites-with-shared-credential-backends.json
# and checks if there are associated Android apps for each of those.
#
# Full explanation is in the README.md file.
#

import sys
import time
import json
from urllib.request import urlopen
import urllib.error
import http
import os
import socket

INPUT_FILE_APPLE = './related-domains-apple.json'
INPUT_FILE_ALEXA = './alexa_1_million.csv'
INPUT_FILE_DASHLANE_CSV = './related-domains-dashlane.csv'
INPUT_FILE_DASHLANE_JSON = './related-domains-dashlane.json'

OUTPUT_FILE_APPLE = './related-domains-and-app-ids-apple.json'
OUTPUT_FILE_DASHLANE = './related-domains-and-app-ids-dashlane.json'
OUTPUT_FILE_ALEXA = './related-domains-and-app-ids-alexa.json'
OUTPUT_FILE = './related-domains-and-app-ids.json'

FLAT_FILE_OUTPUT_APPLE = './log.apple.txt'
FLAT_FILE_OUTPUT_ALEXA = './log.alexa.txt'
FLAT_FILE_OUTPUT_DASHLANE = './log.dashlane.txt'
JSON_FILE_OUTPUT_DASHLANE = './related-domains-dashlane.json'

ASSETS_PATH = '/.well-known/assetlinks.json'
MODE_APPLE = 'apple'
MODE_ALEXA = 'alexa'
MODE_DASHLANE = 'dashlane'
MODE_CONVERT = 'convert'
MODE_MERGE = 'merge'
MODE_CLEAN = 'clean'

def fetch_assets(domain):
    """
    Checks the given domain for a .well-known/assetlinks.json file, which
    may indicate that an Android app exists for this website.
    Returns the JSON object if that is the case, throws all kinds of
    exceptions if it fails.
    """
    url = 'https://' + domain + ASSETS_PATH
    with urlopen(url, timeout=2) as response:
        return json.loads(response.read().decode())

def load_json_file(file):
    with open(file) as f:
        return json.load(f)

def process_domain(domain_group):
    """
    Processes a group of domains. Checks the assets file for each,
    merges the results, removes the duplicates, and returns a new, single
    brand object that contains all urls and android app IDs
    """
    app_ids = []
    for domain in domain_group:
        print(f"Processing {domain}...")
        try:
            assets = fetch_assets(domain)            
            for item in assets:
                if 'target' in item and 'namespace' in item['target'] and item['target']['namespace'] == 'android_app' and 'package_name' in item['target']:
                    app_ids.append(item['target']['package_name'])
        except TimeoutError as e:
            print(f'Error loading {domain}: {e}')
        except UnicodeEncodeError as e:
            print(f'Unicode url not supported: {domain} {e}')
        except ConnectionResetError as e:
            print(f'Error loading {domain}: {e}')
        except urllib.error.URLError as e:
            print(f'Error loading {domain}: {e}')
        except urllib.error.HTTPError as e:
            print(f'Error loading {domain}: {e}')
        except socket.timeout as e:
            print(f'Socket timeout on {domain}: {e}')
        except KeyboardInterrupt:
            return None
        except:
            print(f'Misc. error on {domain}')
    # Creating a list of a set removes all duplicate entries
    unique_app_ids = list(set(app_ids))
    print(json.dumps(unique_app_ids, indent=4, sort_keys=False))
    # Regroup in our favourite format
    return len(unique_app_ids), { 'urls': domain_group, 'android_app_ids': unique_app_ids }

def process_json_file(input_file, output_file, log_file):
    """
    Runs process_domain on each entry in the JSON file input_file.
    The JSON is expected to be in Apple's format.
    Will write a flat log file to log_file, and a new json to output_file
    (which'll include the android_app_ids).
    """
    result = []
    app_id_count = 0
    app_id_sum = 0
    interrupted = False
    # Load input JSON
    all_domains = load_json_file(input_file)
    logfile = open(log_file, 'a')
    try:
        # Process all domains in the file
        for related_domains in all_domains:
            print(f"===== {related_domains[0]} =====")
            c, domain_result = process_domain(related_domains)
            if domain_result is None:
                print('Bye.')
                return False                
            if c > 0:
                app_id_count += 1
                app_id_sum += c
            result.append(domain_result)
            # Write to log file as well:
            if len(domain_result['android_app_ids']) > 0:
                logfile.write(f'{related_domains[0]}: ')
                logfile.write(','.join(domain_result['android_app_ids']))
                logfile.write('\n')
                logfile.flush()
    except KeyboardInterrupt:
        interrupted = True
    logfile.close()
    # Write the result as JSON again
    out_json = json.dumps(result, indent=2, sort_keys=True)
    with open(output_file, 'w') as f:
        f.write(out_json)
    if interrupted:
        print(f'Process interrupted after finding {app_id_sum} apps in total.')
    else:
        print(f'Done. Found app IDs for {app_id_count} out of {len(all_domains)} websites ({app_id_sum} apps in total).')
    return interrupted

def process_csv_file(input_file, output_file, log_file, skip_to_line = 1, max_lines = 4000):
    """
    Default skip_to_line is 1 because the first line is the CSV header.
    Use max_line to automatically stop after a few lines.
    """
    result = []
    app_id_count = 0
    app_id_sum = 0
    interrupted = False
    count = 0
    logfile = open(log_file, 'a')
    try:
        # Load input CSV
        with open(input_file) as file:
            for line in file:
                # Skip when needed
                if count < skip_to_line:
                    count += 1
                    continue
                # break when done
                if max_lines > 0 and count > skip_to_line + max_lines:
                    break
                # parse each line
                count += 1
                parts = line.strip().split(',')
                domain = parts[2]
                # Double check: the first line of the CSV is a header
                if domain == "Domain":
                    continue
                related_domains = [domain]
                # process it
                print(f"===== {count}: {related_domains[0]} =====")
                c, domain_result = process_domain(related_domains)
                if domain_result is None:
                    print('Bye.')
                    return False                
                if c > 0:
                    app_id_count += 1
                    app_id_sum += c
                result.append(domain_result)
                # Write to log file as well:
                if len(domain_result['android_app_ids']) > 0:
                    logfile.write(f'{domain}: ')
                    logfile.write(','.join(domain_result['android_app_ids']))
                    logfile.write('\n')
                    logfile.flush()
    except KeyboardInterrupt:
        interrupted = True
    logfile.close()
    # Write the result as JSON again
    out_json = json.dumps(result, indent=2, sort_keys=True)
    with open(output_file, 'w') as f:
        f.write(out_json)
    if interrupted:
        print(f'Process interrupted after {count} lines and finding {app_id_sum} apps in total.')
    else:
        print(f'Done. Found app IDs for {app_id_count} out of {count-1} websites ({app_id_sum} apps in total).')
    return interrupted

def convert(input_file, output_file):
    """
    Converts the Dashlane file to Apple format
    """
    result = []
    app_id_count = 0
    app_id_sum = 0
    interrupted = False
    outfile = open(output_file, 'w')
    outfile.write('[\n')
    first = True
    try:
        # Load input CSV
        with open(input_file) as file:
            for line in file:
                parts = line.strip().split(',')
                # Add quotes
                arr = [f'"{domain.strip()}"' for domain in parts]
                string = ','.join(arr)
                if first:
                    first = False
                else:
                    outfile.write(',\n')
                outfile.write(f'\t[{string}]')
    except KeyboardInterrupt:
        interrupted = True
    outfile.write('\n]')
    outfile.close()
    if interrupted:
        print(f'Process interrupted.')
    else:
        print(f'Done. Result stored in {output_file}.')
    return interrupted

def clean_json():
    """
    Loads the json and re-saves it without all the domains that are not related to anything.
    @TODO make sure the Alexa process never even saves those, it's useless
    """
    all_domains = load_json_file(OUTPUT_FILE)
    result_arr = []
    # Only copy the useful ones
    for group in all_domains:
        if len(group['u']) + len(group['a']) > 1:
            result_arr.append(group)
    # Write the result as JSON again
    out_json = json.dumps(result_arr) #, indent=2, sort_keys=True)
    with open(OUTPUT_FILE + ".cleaned", 'w') as f:
        f.write(out_json)

def merge_logs():
    result = {}
    result_arr = []
    index = {}
    interrupted = False
    # Load input JSON
    all_domains = load_json_file(INPUT_FILE_APPLE)
    try:
        # Process all domains in the file
        for related_domains in all_domains:
            # Add all related domains to the result.
            # Use the first one in the list as a key
            key = related_domains[0]
            domain_result = { 'u': related_domains, 'a': [] }
            # Keep a reverse index of which key is used for each domain, so we
            # can lookup easier hereafter
            for rd in related_domains:
                if not rd in index:
                    index[rd] = key
            result[key] = domain_result
        # Load log files
        for filename in [FLAT_FILE_OUTPUT_APPLE, FLAT_FILE_OUTPUT_ALEXA, FLAT_FILE_OUTPUT_DASHLANE]:
            with open(filename) as file:
                for line in file:
                    # parse each line
                    parts = line.split(':')
                    domain = parts[0].strip()
                    app_ids = parts[1].split(',')
                    for app_id in app_ids:
                        app_id = app_id.strip()
                        # If the app ID is already in the index, we can skip it altogether
                        if app_id in index:
                            continue
                        else:
                            # check if domain is already added or not. Add if needed
                            if not domain in index:
                                result[domain] = { 'u': [domain], 'a': [] }
                                index[domain] = domain
                                key = domain
                            else:
                                key = index[domain]
                            # Add to the result
                            result[key]['a'].append(app_id)
                            # Add to the index
                            index[app_id] = key
        # Convert the convenient map to something more convenient for the app
        for key in result:
            result_arr.append(result[key])
    except KeyboardInterrupt:
        interrupted = True
    # Write the result as JSON again
    out_json = json.dumps(result_arr) #, indent=2, sort_keys=True)
    with open(OUTPUT_FILE, 'w') as f:
        f.write(out_json)
    if interrupted:
        print(f'Interrupted after processing {len(index)} websites.')
    else:
        print(f'Done. Merged {len(index)} websites and apps into {len(result)} groups.')
    return interrupted

if __name__ == '__main__':
    start_line = 0
    ok = False
    mode = MODE_APPLE
    if len(sys.argv) > 1:
        mode = sys.argv[1]
    if mode == MODE_ALEXA and len(sys.argv) > 2:
        arg2 = sys.argv[2]
        if arg2.isdigit():
            start_line = int(arg2)
    # Process the file
    if mode == MODE_APPLE:
        ok = process_json_file(INPUT_FILE_APPLE, OUTPUT_FILE_APPLE, FLAT_FILE_OUTPUT_APPLE)
    elif mode == MODE_ALEXA:
        ok = process_csv_file(INPUT_FILE_ALEXA, OUTPUT_FILE_ALEXA, FLAT_FILE_OUTPUT_ALEXA, start_line)
    elif mode == MODE_DASHLANE:
        ok = process_json_file(INPUT_FILE_DASHLANE_JSON, OUTPUT_FILE_DASHLANE, FLAT_FILE_OUTPUT_DASHLANE)
    elif mode == MODE_CONVERT:
        ok = convert(INPUT_FILE_DASHLANE_CSV, JSON_FILE_OUTPUT_DASHLANE)
    elif mode == MODE_MERGE:
        ok = merge_logs()
    elif mode == MODE_CLEAN:
        ok = clean_json()
    else:
        print(f'Invalid mode {mode}')
        ok = False
    # Exit with a proper exit code
    sys.exit(0 if ok else 1)
