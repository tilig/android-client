
# Tilig for Android

## Project structure

#### Modules

- `app` - The main app.
- `crypto` - The crypto-related functionality, used by the app.
- `autofill-sample` - A small Android app that is not packaged with the builds. See below for more info.
- `crypto-sample` - A small utility app that is not packaged either. Useful to key generation.

## Build requirements and instructions

Built with Android Studio / Gradle, written in Kotlin.

#### Debug builds

Use the `debug.keystore` that is included in the root of this project.
Without it, the signature will not match the one know by Google and sign-in
will be impossible.

Pushing to `develop` or `master` triggers the CI to distribute a debug build through Firebase.

#### Release builds

Make sure you have the release keystores (not included in this repo)
in the `subs/packages/android/Tilig/.signing/` folder. Then run

```bash
./gradlew clean assembleRelease
```

Yes, we still distribute APKs instead of bundles.

#### Gitlab runner

There is a Gitlab Ci/CD pipeline that can build the app, archive it,
run tests, push debug builds to Firebase App Distribution and, in the future,
might push it to the Play store too.

To run the runner locally:

```bash
export KEYSTORE=<base64 string of the keystore file>
export STORE_PASSWORD=<store password>
export KEY_ALIAS=release
export KEY_PASSWORD=<key password>
export FIREBASE_DEV_DISTRIBUTION=<base64 of the tilig-dev service account json>
gitlab-runner exec docker assembleDebug
```

Assuming the gitlab-runner tool and Docker is installed: https://docs.gitlab.com/runner/install/osx.html

```bash
# Unofficial Homebrew package:
brew install gitlab-runner
brew services start gitlab-runner
```

## Autofill-sample

This module is a standalone app that is useful for testing the Autofill functionality.
It is not included in the releases.

Install it by running the module from Android studio, or build it like so:

```bash
./gradlew clean assembleAutofillsampleDebug
```
