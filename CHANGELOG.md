
## 4.4.2

### Fixed

- Ignore incompatible values in the Get Started settings data


## 4.4.1

### Changed

- Disabled the onboarding survey for all ([#625](https://gitlab.com/subshq/clients/tilig-android/-/issues/625))
- To be sure, the autofill-detector no longer parser `<input>` fields that do not accept text input.

### Fixed

- Creditcards in overview still showing empty, but only for cards added on other platforms ([!357](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/357)) 
- Password field ignored in autocapture if a later password-like field on the page was left blank ([comment #544](https://gitlab.com/subshq/clients/tilig-android/-/issues/544#note_1343888453)) 
- Get Started bar would disappear when user connects desktop ([#640](https://gitlab.com/subshq/clients/tilig-android/-/issues/640)).
- Autosave not triggering on sign-up form in Test Lab app when creditcard not filled ([#544](https://gitlab.com/subshq/clients/tilig-android/-/issues/544))


## 4.4 - Rejected by QA

### Added

- Support for custom fields ([!355](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/355))
- Ask for a rating after users have used the app for a bit ([!340](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/340))
- When autocapturing incomplete data (e.g. Booking), a flow similar to Autofill shows to ask to complete the data ([!320](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/320))

### Fixed

- "Share with" User should not appear in Suggested list ([!358](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/359))
- User is not able to Discard changes of edited item on 1st try ([!359](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/359))
- Wrong copy text: 2 questions should be 3 ([#625](https://gitlab.com/subshq/clients/tilig-android/-/issues/625))
- An "Eye" icon is shown next to Network name field on Add WiFi screen ([!360](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/360))
- "Type the existing password" may wrap to two lines which looks weird ([#615](https://gitlab.com/subshq/clients/tilig-android/-/issues/615/))
- Autocapture too cautious with triggering on web password fields on the off chance they might be masked ([#560](https://gitlab.com/subshq/clients/tilig-android/-/issues/560))  
- Autocapture would not save the username in a 2-step login process, if the password was masked in the second step
- Autocapture could store an account with the application ID for Chrome, when it should be storing it for a website, in a 2-step login process
- "Share link" is broken if item is shared with other User ([!363](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/363))
- When card has no card number, card number is shown as null on All items / Cards screens([!357](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/357))
- Auto-capture does not store login on Sign up form ([!365](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/365))

### Fixed after QA, before release

- Crash when autofilling second step in Booking login flow ([original MR !320](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/320), crash [reported in #519](https://gitlab.com/subshq/clients/tilig-android/-/issues/559#note_1336965931) and ())
- Instruct users properly when they tried biometrics too many times.
- 16-dot masked creditcard number was shown in the overview for cards that had a blank creditcard number ([original MR !357](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/357))
- Contact list appeared empty when sharing items that weren't upgraded to folders yet ([#634](https://gitlab.com/subshq/clients/tilig-android/-/issues/634))


## 4.3.1 - 2023-03-28

### Fixed

- Crash for some users when loading Getting Started


## 4.3 - 2023-03-27

### Added

- Getting Started ([#607](https://gitlab.com/subshq/clients/tilig-android/-/issues/607), [#614](https://gitlab.com/subshq/clients/tilig-android/-/issues/614))


## 4.2.2 - 2023-03-10

### Fixed

- Importing accounts through web would break login on Android ([!353](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/353))


## 4.2.1 - 2023-03-07

### Added

- Turbosprint feature: Sharing with non-Tilig users
- Turbosprint feature: Contact list
- Add to settings page a export section that bring the user directly to app.tilig.com/export ([!337](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/337))
- In-app update notifications (when updates are pushed with high priority indication)
- Update related-domains list with Dashlane info ([!344](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/344))

### Fixed

- Removed unused text from Your Data section ([!336](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/336))
- Microsoft accounts did not display a name in the settings ([#595](https://gitlab.com/subshq/clients/tilig-android/-/issues/595))
- Microsoft login is always enabled and included in 'More Info' ([!343](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/343))
- Possible continuous login errors after a failed login


## 4.2 (previously 3.13) - 2023-02-27

### Added

- Support for feature flag `android-test_drive` and related Mixpanel tracking ([!313](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/313))
- New pre-signin onboarding ([!324](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/324))
- New post-signin onboarding ([!329](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/329))
- Import passwords instructions ([!327](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/327))

### Fixed

- Sometimes navigate to item details multiple times ([!319](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/319))
- Add a slight delay to make sure Unleash works ([!326](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/326))
- Support new fields in brand model ([!325](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/325))
- Redirect user to onboarding if they autofill while logged out ([!333](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/333))


## 4.1.1 - Hotfix - 2023-02-23

### Fixed

- Conversion events are underreported, not tracked when autofilling (i) after phishing warning and (ii) when creating account through autofill


## 4.1 (previously 3.12) - 2023-02-22

### Added

- Use heuristics to detect and fill more OTP fields ([!295](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/295))
- Support for user settings in the backend. Used to display survey only once. ([!298](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/298))
- For developers only: Notes, Creditcards and WiFi items now also have a working Debug button

### Analytics

- Add event for 3rd Autofill on Android to Firebase ([!308](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/308))
- Extra logging to figure out issues [#454](https://gitlab.com/subshq/clients/tilig-android/-/issues/454) and [#517](https://gitlab.com/subshq/clients/tilig-android/-/issues/517)
- New analytics for Test Drive: "Test Note|Wifi|Credit Card Viewed" ([!315](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/315))
- New analytics for Test Drive being enabled ([!313](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/313))
- Remove event `First password website selected` ([!294](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/294))

### Changed

- Remove squirrel image from "no search results" placeholder ([!300](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/300))
- New loader spinner when loading or saving items ([!301](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/301))

### Fixed

- Onboarding video covers "Open settings" button and background text in Onboarding screen ([!292](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/292))
- Biometric unlock events were tracked with lock was disabled ([!305](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/305))
- Copy text differences from webapp, inconsistent WiFi capitalization, missing titles in 'add account' sheets ([!297](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/297))
- Autosaving a submitted login on a website could save it with the app ID for Chrome instead of the url of the website ([!310](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/310))
- Autosaving in a 2-step flow, after autofilling and manually editing the password, would overwrite the username to null.
- "Back" button does not work on Add a new login screen (Extension) ([!304](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/304))
- Clicking Cancel icon (x) in Autofill Extension isn't easy enough ([!304](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/304))
- Prevent getting 2x copied notifcations inside password history ([!306](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/306))
- Search field continue to be active and sticky if keyboard is closed by "Close Keyboard" button ([!307](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/307))
- Sometimes item is created multiple times ([!311](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/311))
- Test Drive account is forever loading ([!312](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/312))


## 4.0.2 - 2023-02-20

### Fixed

- New style error box for sharing errors.
- Debounce clicks to prevent various errors.

From the sharing MR ([!323](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/323)

- 1. "You cannot share an item with yourself." message is not shown
- 2. Error message is not showing if User taps Share button 2nd time
- 3. here is no email validation for share email input field. Field accepts "qwerty"
- 4, 7, 9. Various issues caused by the UI not refreshing after add or revoke.


## 4.0.1 - 2023-02-13

### Fixed

- Crash when showing an error response from the backend


## 4.0 - 2023-02-13

### Added

- Sharing


### Known issues

- UI does not refresh after revoking an invite
- Share icon missing in the main list for accounts that are shared by share-link only
- The nice error message style when sharing goes wrong


## 3.11.2

### Fixed

- Crash when Test Drive items are present from older app versions ([#574](https://gitlab.com/subshq/clients/tilig-android/-/issues/574))


## 3.11.1 - 2023-01-24

### Fixed

- Test drive note does not show title and cannot be edited ([!302](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/302))


## 3.11

### Added

- Three-people icon for shared items ([!265](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/265))
- Add to Microsoft login the option to select the exact MS account ([!274](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/274))
- Add copy actions to website field of login view page and extra information fields of all item types ([!276](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/276))
- Add events to password restore functionality ([!278](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/278))
- Biometric events ([!282](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/282))
- Copy OTP code to clipboard when autofilling account with known OTP code ([!284](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/284))

### Fixed

- Disable swiping to close bottom sheet ([!263](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/263))
- Bottom part of a screen becomes disabled if user taps Scan QR code button while keyboard is shown ([!268](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/268/))
- Login name becomes not prefilled after tapping "Add a new login" button ([!269](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/269))
- Keyboard is not hiding after closing "Add new note" window by swipe down action ([!273](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/273))
- Sentry crash ANDROID-APP-HT, unexpected date format for a HistoryEntry.
- Harmless background crash ANDROID-APP-HY in autocapture.
- When yellow bar is active (autofill disabled) search isn't working [!280](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/280))
- Fix crash when rapidly clicking 'Delete forever' account ([!281](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/281))
- Prevent entering non-numeric characters into creditcard field ([!285](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/285))
- Ensure drag-down to refresh also works when zero items are present ([!279](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/279))
- Events - Ensure when user continues on phishing screen, event `Autofill Selected` is fired ([!290](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/290))
- Use ranking and `is_fetched` in the brand search on Android ([!289](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/289))
- Stop setting legacy name field on secure notes.
- Make the search field used for autocomplete in Add Account behave like URL field ([!282](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/282))


-------------------------------------------------------

# Archive

Older versions are archived in [CHANGELOG.archive.md](./CHANGELOG.archive.md),
because this file exceeded the maximum length allowed as release notes on Firebase.


# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

