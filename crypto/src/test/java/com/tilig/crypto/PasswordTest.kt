package com.tilig.crypto

import org.junit.Assert
import org.junit.Test

class PasswordTest {

    @Test
    fun generate_password(){
        val password = Password.generate().toString()

        Assert.assertEquals(13, password.length)
        Assert.assertTrue(password.contains("-"))
        Assert.assertTrue(password.substring(0, 6).contains("[0-9]".toRegex()) || password.substring(0, 6).contains("[A-Z]".toRegex()))
        Assert.assertTrue(password.substring(7, 13).contains("[0-9]".toRegex()) || password.substring(7, 13).contains("[A-Z]".toRegex()))
    }
}