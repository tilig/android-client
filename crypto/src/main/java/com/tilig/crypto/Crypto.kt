package com.tilig.crypto

import android.util.Base64
import android.util.Log
import com.goterl.lazysodium.LazySodiumAndroid
import com.goterl.lazysodium.SodiumAndroid
import com.goterl.lazysodium.interfaces.Box
import com.goterl.lazysodium.interfaces.KeyDerivation
import com.goterl.lazysodium.interfaces.SecretBox
import com.goterl.lazysodium.utils.Base64MessageEncoder
import com.goterl.lazysodium.utils.Key
import com.goterl.lazysodium.utils.KeyPair
import com.tilig.crypto.algorithms.Algorithm
import com.tilig.crypto.algorithms.RSAAlgorithm
import com.tilig.crypto.algorithms.RSAAlgorithmCompat
import com.tilig.crypto.keyutils.KeyUtils
import org.bouncycastle.jce.provider.BouncyCastleProvider
import java.security.PrivateKey
import java.security.Provider
import java.security.PublicKey
import java.util.*
import javax.crypto.Cipher


fun String.stripNewlines(): String = this.replace("\n", "")

class Crypto {

    private var legacyKey: LegacyKeyPair? = null
    private var key: KeyPair? = null
    private val algorithm: Algorithm
    private val provider: Provider
    private val sodium = LazySodiumAndroid(
        SodiumAndroid(),
        Base64MessageEncoder()
    )

    init {
        Log.w(TAG, "Creating a new Crypto instance! ${hashCode()}")
    }

    companion object {

        private const val TAG = "Crypto"
        private const val ENABLE_NEW_METHOD = false
        private const val ENABLE_VERBOSE_LOGGING = false

        private val CHARSET = Charsets.UTF_8

        const val PEM_TYPE_PUBLIC_KEY = "PUBLIC KEY"
        const val PEM_TYPE_PRIVATE_KEY = "PRIVATE KEY"

        /**
         * Returns a new BouncyCastle provider,
         * guaranteed to be from the packaged version and not the
         * broken, incompatible OS version
         */
        fun createProvider(): Provider = BouncyCastleProvider()

        fun logBytes(message: String, data: ByteArray) {
            val dataStartStr = data.copyOfRange(0, 3).joinToString(", ")
            val dataEndStr = data.last().toString()
            Log.d(TAG, "$message: [$dataStartStr... , $dataEndStr] length = ${data.size}")
        }
    }

    init {
        provider = createProvider()

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M
            && ENABLE_NEW_METHOD
        ) {
            algorithm = RSAAlgorithm(provider)
        } else {
            algorithm = RSAAlgorithmCompat(provider)
        }

        algorithm.init()
    }

    fun getKeyPairs(): KeyPairs = KeyPairs(legacy = legacyKey, keypair = key)

    fun hasKeys(): Boolean = key != null || legacyKey != null
    fun hasAllKeys(): Boolean = key != null && legacyKey != null
    fun hasLegacyKeys(): Boolean = legacyKey != null
    fun hasNewKeys(): Boolean = key != null

    fun uninitialize() {
        key = null
        legacyKey = null
    }

    fun generateLegacyKey(): LegacyKeyPair {
        legacyKey = algorithm.generateKeyPair()
        return legacyKey!!
    }

    fun generateKey(): KeyPair {
        key = sodium.cryptoBoxKeypair()
        return key!!
    }

    fun getNewGeneratedKey(): KeyPair =
        sodium.cryptoBoxKeypair()

    fun legacyEncryptIfNotBlank(text: String?): String? {
        return if (text.isNullOrBlank()) null else legacyEncrypt(text)
    }

    fun legacyEncrypt(text: String): String? = legacyEncrypt(text.toByteArray(CHARSET))

    private fun legacyEncrypt(text: ByteArray, useLegacyBase64: Boolean = true): String? {
        if (legacyKey == null) {
            throw RuntimeException("Can't encrypt before loading or generating a legacy keypair")
        }
        return try {
            legacyKey?.let {
                val cipher = algorithm.createCipher(provider)
                cipher.init(Cipher.ENCRYPT_MODE, legacyKey!!.public)
                val encrypted = cipher.doFinal(text)
                if (useLegacyBase64)
                    base64encodeLegacy(encrypted)
                else
                    base64encode(encrypted)
            }
        } catch (e: Exception) {
            Log.e(TAG, "Unable to encrypt", e)
            null
        }
    }

    fun seal(messageBytes: ByteArray, publicKeyRecipient: ByteArray): String? {
        return try {
            // Easy, but with unreliable base64 encoding included:
            // sodium.cryptoBoxSealEasy(message, publicKey)

            // More boilerplate, but more versatile
            val cipherTextBytes = ByteArray(Box.SEALBYTES + messageBytes.size)
            sodium.cryptoBoxSeal(
                cipherTextBytes,
                messageBytes,
                messageBytes.size.toLong(),
                publicKeyRecipient
            )
            return base64encode(cipherTextBytes)
        } catch (e: Exception) {
            Log.e(
                TAG,
                "Unable to seal: ${e.message}"
            )
            null
        }
    }

    fun open(cipherText: String, keyPair: KeyPair): ByteArray? {
        return try {
            val cipherTextBytes = base64decode(cipherText)
            val messageBytes = ByteArray(cipherTextBytes.size - Box.SEALBYTES)
            sodium.cryptoBoxSealOpen(
                messageBytes,
                cipherTextBytes,
                cipherTextBytes.size.toLong(),
                keyPair.publicKey.asBytes,
                keyPair.secretKey.asBytes
            )
            return messageBytes
        } catch (e: Exception) {
            Log.e(
                TAG,
                "Unable to seal: ${e.message}"
            )
            null
        }
    }

    fun encryptDek(messageBytes: ByteArray): String? {
        return try {
            key?.let {
                val nonce = generateAsymNonce()
                val cipherTextBytes = ByteArray(Box.MACBYTES + messageBytes.size)
                sodium.cryptoBoxEasy(
                    cipherTextBytes,
                    messageBytes,
                    messageBytes.size.toLong(),
                    nonce,
                    it.publicKey.asBytes,
                    it.secretKey.asBytes
                )
                val cipherText = nonce + cipherTextBytes
                return@let base64encode(cipherText)
            }
                ?: throw RuntimeException("Can't encrypt before loading or generating a new libsodium keypair")
        } catch (e: Exception) {
            Log.e(
                TAG,
                "Unable to encrypt: ${e.message}; will skip encryptedKey and use encryptedDek only"
            )
            null
        }
    }

    fun encryptDekWithPublicKey(messageBytes: ByteArray, publicKeyRecipient: ByteArray, privateKeySender: ByteArray = byteArrayOf()): String? {
        return try {
            val nonce = generateAsymNonce()
            val cipherTextBytes = ByteArray(Box.MACBYTES + messageBytes.size)
            sodium.cryptoBoxEasy(
                cipherTextBytes,
                messageBytes,
                messageBytes.size.toLong(),
                nonce,
                publicKeyRecipient,
                privateKeySender
            )
            val cipherText = nonce + cipherTextBytes
            return base64encode(cipherText)
        } catch (e: Exception) {
            Log.e(
                TAG,
                "Unable to encrypt: ${e.message}; will skip encryptedKey and use encryptedDek only"
            )
            null
        }
    }

    fun encryptWithKeypair(messageBytes: ByteArray, keyPair: KeyPair): String? {
        return try {
            val nonce = generateAsymNonce()
            val cipherTextBytes = ByteArray(Box.MACBYTES + messageBytes.size)
            sodium.cryptoBoxEasy(
                cipherTextBytes,
                messageBytes,
                messageBytes.size.toLong(),
                nonce,
                keyPair.publicKey.asBytes,
                keyPair.secretKey.asBytes
            )
            val cipherText = nonce + cipherTextBytes
            return base64encode(cipherText)
        } catch (e: Exception) {
            Log.e(
                TAG,
                "Unable to encrypt: ${e.message}; will skip encryptedKey and use encryptedDek only"
            )
            null
        }
    }

    fun decryptDek(cipherText: String): ByteArray? {
        if (cipherText.isEmpty()) {
            return null
        }
        return try {
            key?.let {
                val totalCipherTextBytes = base64decode(cipherText)
                if (totalCipherTextBytes.size < Box.NONCEBYTES + Box.MACBYTES) {
                    throw RuntimeException("Wrong payload length. ${totalCipherTextBytes.size} should be larger than ${Box.NONCEBYTES} + ${Box.MACBYTES}")
                }
                val nonce = totalCipherTextBytes.copyOfRange(0, Box.NONCEBYTES)
                val cipherTextBytes =
                    totalCipherTextBytes.copyOfRange(Box.NONCEBYTES, totalCipherTextBytes.size)
                val messageBytes = ByteArray(cipherTextBytes.size - Box.MACBYTES)

                sodium.cryptoBoxOpenEasy(
                    messageBytes,
                    cipherTextBytes,
                    cipherTextBytes.size.toLong(),
                    nonce,
                    it.publicKey.asBytes,
                    it.secretKey.asBytes
                )
                return@let messageBytes
            }
                ?: throw RuntimeException("Can't decrypt before loading or generating a new libsodium keypair")
        } catch (e: Exception) {
            Log.e(
                TAG,
                "Unable to decrypt: " + e.message + "; will fallback to encryptedDek instead of encryptedKey if possible"
            )
            null
        }
    }

    fun decryptWithKeypair(cipherText: String, keyPair: KeyPair): ByteArray? {
        if (cipherText.isEmpty()) {
            return null
        }
        return try {
            val totalCipherTextBytes = base64decode(cipherText)
            if (totalCipherTextBytes.size < Box.NONCEBYTES + Box.MACBYTES) {
                throw RuntimeException("Wrong payload length. ${totalCipherTextBytes.size} should be larger than ${Box.NONCEBYTES} + ${Box.MACBYTES}")
            }
            val nonce = totalCipherTextBytes.copyOfRange(0, Box.NONCEBYTES)
            val cipherTextBytes =
                totalCipherTextBytes.copyOfRange(Box.NONCEBYTES, totalCipherTextBytes.size)
            val messageBytes = ByteArray(cipherTextBytes.size - Box.MACBYTES)

            sodium.cryptoBoxOpenEasy(
                messageBytes,
                cipherTextBytes,
                cipherTextBytes.size.toLong(),
                nonce,
                keyPair.publicKey.asBytes,
                keyPair.secretKey.asBytes
            )
            return messageBytes
        } catch (e: Exception) {
            Log.e(
                TAG,
                "Unable to decrypt: " + e.message + "; will fallback to encryptedDek instead of encryptedKey if possible"
            )
            null
        }
    }

    fun legacyDecrypt(cipherText: String?): String? {
        if (legacyKey == null) {
            Log.w(TAG, "Can't decrypt before loading or generating a legacy keypair")
            return null
        }
        if (cipherText.isNullOrEmpty()) {
            return null
        }
        try {
            return legacyKey?.let {
                val cipher = algorithm.createCipher(provider)
                cipher.init(Cipher.DECRYPT_MODE, it.private)
                val input = base64decode(cipherText)
                val decrypted = cipher.doFinal(input)
                return@let String(decrypted, CHARSET)
            }
        } catch (e: Exception) {
            val fallback = legacyDecryptWithLegacyBase64Encoding(cipherText)
            if (fallback == null) {
                if (BuildConfig.DEBUG) {
                    // Short version in the debug build -- this is likely due to us switching keys between dev and prod
                    Log.e(TAG, "Unable to decrypt ($cipherText)")
                } else {
                    // Long error in release builds. It's probably still us though.
                    Log.e(TAG, "Unable to decrypt ($cipherText)", e)
                }
            } else {
                Log.w(TAG, "Decryption failed, but fallback with legacy base64 decoding worked")
            }
            return fallback
        }
    }

    /**
     * Same as decrypt, but using base64Legacy.
     * Only to be used as a fallback if the first method fails.
     */
    private fun legacyDecryptWithLegacyBase64Encoding(cipherText: String): String? = try {
        legacyKey?.let {
            val cipher = algorithm.createCipher(provider)
            cipher.init(Cipher.DECRYPT_MODE, it.private)
            val input = base64decodeLegacy(cipherText)
            val decrypted = cipher.doFinal(input)
            return@let String(decrypted, CHARSET)
        }
    } catch (e: Exception) {
        Log.e(TAG, "Unable to decryptLegacy ($cipherText)")
        null
    }

    /* fun setKeypairFromPlainString(secret: String, pub: String) {
        try {
             val secretKey = Key.fromPlainString(secret)
             val publicKey = Key.fromPlainString(pub)
            key = KeyPair(publicKey, secretKey)
        } catch(e: Exception) {
            Log.e(TAG, "Unable to base64 decode new keypair:\n   priv = $secret\n   pub =  $pub")
            throw e
        }
    } */

    fun setKeypairFromString(secret: String, pub: String) {
        try {
            // Does NOT work. Even for keys that are 100% base64 encoded using libsodium's own
            // method on web, ARE NOT recognized with libsodium's own method here. So, we use our own:
            // val secretKey = Key.fromBase64String(secret)
            // val publicKey = Key.fromBase64String(pub)
            val secretKey = Key.fromBytes(base64decode(secret))
            val publicKey = Key.fromBytes(base64decode(pub))
            key = KeyPair(publicKey, secretKey)
        } catch (e: Exception) {
            Log.e(TAG, "Unable to base64 decode new keypair:\n   priv = $secret\n   pub =  $pub")
            throw e
        }
    }

    fun setLegacyKeypairFromString(priv: String, pub: String) {
        val privateKey = parseLegacyPrivateKey(priv)
        val publicKey = parseLegacyPublicKey(pub)
        legacyKey = LegacyKeyPair(publicKey, privateKey)
    }

    private fun parseLegacyPrivateKey(priv: String): PrivateKey {
        var result = KeyUtils.parsePrivateKey(priv, provider)
        if (result == null) {
            result = algorithm.loadPrivateKeyBase64(KeyUtils.stripHeaders(priv), true)
        }
        return result
    }

    private fun parseLegacyPublicKey(priv: String): PublicKey {
        var result = KeyUtils.parsePublicKey(priv, provider)
        if (result == null) {
            result = algorithm.loadPublicKeyBase64(KeyUtils.stripHeaders(priv), true)
        }
        return result
    }

    fun legacyPublicKeyAsString(): String? =
        legacyKey?.let { KeyUtils.getPublicKeyAsString(it.public) }

    fun legacyPrivateKeyAsString(): String? =
        legacyKey?.let { KeyUtils.getPrivateKeyAsString(it.private) }

    fun getKeyPairAsStrings(): KeyPairString? = key?.let { pair ->
        KeyPairString(
            private = base64encode(pair.secretKey.asBytes),
            public = base64encode(pair.publicKey.asBytes)
        )
    }

    fun getKeyPairsAsStrings(): KeyPairsString = KeyPairsString(
        legacy = KeyPairString(
            private = legacyPrivateKeyAsString(),
            public = legacyPublicKeyAsString()
        ),
        keypair = getKeyPairAsStrings()
    )

    fun getAlgorithmName(): String = algorithm.getName()

    private fun generateNonce(): ByteArray = sodium.randomBytesBuf(SecretBox.NONCEBYTES)

    private fun generateAsymNonce(): ByteArray = sodium.randomBytesBuf(Box.NONCEBYTES)

    /**
     * Generates a DEK that can be used to encrypt longer messages than is possible with just
     * the keypairs.
     * The generated DEK can be used for encryption using [encryptUsingDek] and [decryptUsingDek].
     * Store the DEK itself using [encryptDek] and [decryptDek].
     * @return the DEK
     */
    fun generateDek(): ByteArray {
        // We could use: sodium.cryptoSecretBoxKeygen()
        // but this encodes it into a key object. To get raw bytes, we do:
        val key = ByteArray(SecretBox.KEYBYTES)
        sodium.cryptoSecretBoxKeygen(key)
        if (BuildConfig.DEBUG) {
            logBytes("Generated key", key)
        }
        return key
    }

    /**
     * Encrypts the share-link master key with the known keypair.
     * @param masterKey the master key to encrypt
     * @return the encrypted key, Base64 encoded
     */
    fun encryptMasterSecret(masterKey: ByteArray): String? = encryptDek(masterKey)

    /**
     * Encrypts the DEK with the known keypair. The DEK can be created with [generateDek].
     * @param dek the DEK to encrypt
     * @return the encrypted DEK, Base64 encoded
     */
    fun legacyEncryptDek(dek: ByteArray): String? = legacyEncrypt(dek, useLegacyBase64 = true)

    /**
     * Decrypts the DEK using the known keypair.
     * @param encryptedDek the DEK as received from the API. Base64 encoded.
     * @return the decrypted DEK
     */
    fun legacyDecryptDek(encryptedDek: String): ByteArray? {
        try {
            legacyKey?.let {
                val cipher = algorithm.createCipher(provider)
                cipher.init(Cipher.DECRYPT_MODE, it.private)
                val input = base64decode(encryptedDek)
                return cipher.doFinal(input)
            } ?: throw RuntimeException("Can't decrypt DEK before loading or generating a keypair")
        } catch (e: Exception) {
            // Fallback, try again with legacy base64
            try {
                return legacyKey?.let {
                    val cipher = algorithm.createCipher(provider)
                    cipher.init(Cipher.DECRYPT_MODE, it.private)
                    val input = base64decodeLegacy(encryptedDek)
                    val output = cipher.doFinal(input)
                    Log.w(TAG, "Decrypting DEK required base64decodeLegacy")
                    output
                }
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) {
                    // Short version in the debug build -- this is likely due to us switching keys between dev and prod
                    Log.e(
                        TAG,
                        "Unable to decrypt DEK with DEFAULT or LEGACY base64 (${e.javaClass.simpleName})"
                    )
                } else {
                    // Long error in release builds. It's probably still us though.
                    Log.e(TAG, "Unable to decrypt DEK with DEFAULT or LEGACY base64", e)
                }
                return null
            }
        }
    }

    /**
     * Encrypts the given text using the DEK. If the DEK is invalid, throws a RuntimeException.
     * After encryption, prepends the auto generated nonce to the output, and base64's the result.
     * [decryptUsingDek] automatically picks this nonce from the cipherText again.
     * @param dek The key to use for encryption
     * @param plaintext The text to encrypt. In our case, this is a JSON string of an object.
     * @return The encrypted text as a Base64 string.
     */
    fun encryptUsingDek(dek: ByteArray, plaintext: String): String? {
        return encryptUsingDek(dek, plaintext.toByteArray(CHARSET))
    }

    /**
     * Encrypts the given text using the DEK. If the DEK is invalid, throws a RuntimeException.
     * After encryption, prepends the auto generated nonce to the output, and base64's the result.
     * [decryptUsingDek] automatically picks this nonce from the cipherText again.
     * @param dek The key to use for encryption
     * @param messageBytes The value to encrypt
     * @return The encrypted text as a Base64 string.
     */
    fun encryptUsingDek(dek: ByteArray, messageBytes: ByteArray): String? {
        if (dek.size != SecretBox.KEYBYTES) {
            val message =
                "Encryption error: DEK is the wrong size (${dek.size}, should be ${SecretBox.KEYBYTES})"
            Log.e(TAG, message)
            return null
        }
        val nonce = generateNonce()
        val cipherTextBytes = ByteArray(SecretBox.MACBYTES + messageBytes.size)

        if (!sodium.cryptoSecretBoxEasy(
                cipherTextBytes,
                messageBytes,
                messageBytes.size.toLong(),
                nonce,
                dek
            )
        ) {
            throw RuntimeException("Could not encrypt message.")
        }

        if (BuildConfig.DEBUG) {
            logBytes("Generated nonce", nonce)
            logBytes("Message bytes", messageBytes)
            logBytes("Ciphertext", cipherTextBytes)
        }

        // We include the nonce in the byte array
        return base64encode(nonce + cipherTextBytes)
    }

    /**
     * Uses the decrypted DEK to decrypt the message.
     * Assumes UTF-8 (compatible with web app).
     * Throws a runtime exception if anything is wrong (DEK the wrong size, payload invalid,
     * encryption fails, etc.).
     * The cipherText should contain the nonce at the start. [encryptUsingDek] adds this automatically.
     * @param dek the key to use for decryption
     * @param cipherText Base64 encoded bytes that need to be decrypted
     *
     */
    fun decryptUsingDek(dek: ByteArray, cipherText: String): String? {
        if (dek.size != SecretBox.KEYBYTES) {
            val message =
                "Decryption error: DEK is the wrong size (${dek.size}, should be ${SecretBox.KEYBYTES})"
            Log.e(TAG, message)
            return null
        }

        val input = base64decode(cipherText)
        if (input.size < SecretBox.NONCEBYTES + SecretBox.MACBYTES) {
            Log.e(
                TAG,
                "Decryption error: payload too small (${input.size}, should be at least ${SecretBox.NONCEBYTES} + ${SecretBox.MACBYTES})"
            )
            return null
        }

        val nonce = input.copyOfRange(0, SecretBox.NONCEBYTES)
        val cipherBytes = input.copyOfRange(SecretBox.NONCEBYTES, input.size)
        val messageBytes = ByteArray(cipherBytes.size - SecretBox.MACBYTES)

        if (!sodium.cryptoSecretBoxOpenEasy(
                messageBytes,
                cipherBytes,
                cipherBytes.size.toLong(),
                nonce,
                dek
            )
        ) {
            Log.e(TAG, "Could not decrypt message.")
            return null
        }

        if (BuildConfig.DEBUG && ENABLE_VERBOSE_LOGGING) {
            logBytes("Key", dek)
            logBytes("Input including nonce", input)
            logBytes("Read nonce", nonce)
            logBytes("Ciphertext", cipherBytes)
            logBytes("Decrypted message", messageBytes)
        }

        return String(messageBytes, CHARSET)
    }

    fun base64decode(encodedText: String): ByteArray = try {
        Base64.decode(encodedText.stripNewlines(), Base64.NO_PADDING or Base64.URL_SAFE)
    } catch (e: IllegalArgumentException) {
        // Backup method for un-published versions
        base64decodeLegacy(encodedText)
    }

    private fun base64decodeLegacy(encodedText: String): ByteArray =
        Base64.decode(encodedText, Base64.DEFAULT)

    fun base64encode(encrypted: ByteArray?): String =
        Base64.encodeToString(encrypted, Base64.NO_PADDING or Base64.URL_SAFE).stripNewlines()

    private fun base64encodeLegacy(encrypted: ByteArray?): String =
        Base64.encodeToString(encrypted, Base64.DEFAULT).stripNewlines()

    /**
     * Derive all needed data from a sharing master secret
     * @param masterSecret Sharing secret
     */
    fun deriveDataFromMasterSecret(decryptedDek: ByteArray): ShareMasterKeyData? {
        // We could use: sodium.cryptoKdfKeygen()
        // but this encodes it into a key object. To get raw bytes, we do:
        val key = ByteArray(KeyDerivation.MASTER_KEY_BYTES)
        sodium.cryptoKdfKeygen(key)

        val masterSecret = Key.fromBytes(key)
        val keyEncryptionKey = sodium.cryptoKdfDeriveFromKey(32, 1, "ShareKek", masterSecret)
        val uuid =
            sodium.cryptoKdfDeriveFromKey(16, 2, "ShareUid", masterSecret).asHexString.lowercase(
                Locale.getDefault()
            )
        val accessToken =
            sodium.cryptoKdfDeriveFromKey(16, 3, "ShareTkn", masterSecret).asHexString.lowercase(
                Locale.getDefault()
            )
        val encryptedNewDek = encryptUsingDek(keyEncryptionKey.asBytes, decryptedDek)
        val encryptedMasterKey = encryptMasterSecret(key)
        // ensure we can decrypt this key(sometimes decryption fails). If null try again
        if (encryptedMasterKey == null || decryptDek(encryptedMasterKey) == null) {
            Log.w(TAG, "master key decryption failed, try to generate again")
            return deriveDataFromMasterSecret(decryptedDek)
        }
        return if (encryptedNewDek != null) {
            ShareMasterKeyData(
                encryptMasterSecret = encryptedMasterKey,
                encryptedKey = encryptedNewDek,
                uuid = uuid,
                accessToken = accessToken
            )
        } else {
            null
        }
    }

    fun decryptMasterSecret(encryptedMasterSecret: String): String? {
        val decryptedSecret = decryptDek(encryptedMasterSecret) ?: return null
        return base64encode(decryptedSecret)
    }

    fun decodeKeypair(secret: String, pub: String): KeyPair {
        try {
            val secretKey = Key.fromBytes(base64decode(secret))
            val publicKey = Key.fromBytes(base64decode(pub))
            return KeyPair(publicKey, secretKey)
        } catch (e: Exception) {
            Log.e(TAG, "Unable to base64 decode new keypair:\n   priv = $secret\n   pub =  $pub")
            throw e
        }
    }
}

data class ShareMasterKeyData(
    val encryptedKey: String,
    val encryptMasterSecret: String,
    val uuid: String,
    val accessToken: String
)

