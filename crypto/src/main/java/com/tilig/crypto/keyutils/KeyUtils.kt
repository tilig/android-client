package com.tilig.crypto.keyutils

import android.util.Log
import com.tilig.crypto.Crypto
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo
import org.bouncycastle.openssl.PEMParser
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter
import org.bouncycastle.util.io.pem.PemObject
import org.bouncycastle.util.io.pem.PemWriter
import java.io.StringReader
import java.io.StringWriter
import java.security.PrivateKey
import java.security.Provider
import java.security.PublicKey

object KeyUtils {


    // This could be regexified but this is nice and recognizable
    fun stripHeaders(key: String) =
        key
            .replace("-----BEGIN PUBLIC KEY-----", "")
            .replace("-----END PUBLIC KEY-----", "")
            .replace("-----BEGIN PRIVATE KEY-----", "")
            .replace("-----END PRIVATE KEY-----", "")
            .replace("-----BEGIN RSA PRIVATE KEY-----", "")
            .replace("-----END RSA PRIVATE KEY-----", "")
            .replace("-----BEGIN RSA PUBLIC KEY-----", "")
            .replace("-----END RSA PUBLIC KEY-----", "")
            .replace("\n", "")
            .trim()

    fun getPublicKeyAsString(publicKey: PublicKey): String {
        val writer = StringWriter()
        val pemWriter = PemWriter(writer)
        pemWriter.writeObject(PemObject(Crypto.PEM_TYPE_PUBLIC_KEY, publicKey.encoded))
        pemWriter.flush()
        pemWriter.close()
        return writer.toString()
    }

    fun getPrivateKeyAsString(privateKey: PrivateKey): String {
        val writer = StringWriter()
        val pemWriter = PemWriter(writer)
        pemWriter.writeObject(PemObject(Crypto.PEM_TYPE_PRIVATE_KEY, privateKey.encoded))
        pemWriter.flush()
        pemWriter.close()
        return writer.toString()
    }

    fun parsePrivateKey(priv: String, provider: Provider): PrivateKey? {
        val parser = PEMParser(StringReader(priv))
        var readObject = parser.readObject()
        val converter = JcaPEMKeyConverter()
            .setProvider(provider)
        while (readObject != null) {
            val privateKeyInfo = getPrivateKeyInfoOrNull(readObject)
            if (privateKeyInfo != null) {
                return converter.getPrivateKey(privateKeyInfo)
            }
            readObject = parser.readObject() as? PemObject
        }
        return null
    }

    private fun getPrivateKeyInfoOrNull(readObject: Any): PrivateKeyInfo? {
        return if (readObject is PrivateKeyInfo) {
            try {
                PrivateKeyInfo.getInstance(readObject)
            } catch (e: Exception) {
                Log.e("Crypto", "Unable to create PrivateKeyInfo", e)
                null
            }
        } else {
            null
        }
    }

    fun parsePublicKey(pub: String, provider: Provider): PublicKey? {
        val parser = PEMParser(StringReader(pub))
        val converter = JcaPEMKeyConverter()
            .setProvider(provider)
        var readObject = parser.readObject()
        while (readObject != null) {
            if (readObject is SubjectPublicKeyInfo) {
                return converter.getPublicKey(readObject)
            }
            readObject = parser.readObject() as? PemObject
        }
        return null
    }
}
