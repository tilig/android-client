package com.tilig.crypto

import kotlin.random.Random.Default.nextBoolean
import kotlin.random.Random.Default.nextInt


class Password(private val password: String) {

    companion object {

        private val LOWER = ('a'..'z').toList()
        private val UPPER = ('A'..'Z').toList()
        private val DIGIT = ('0'..'9').toList()

        private const val L_LENGTH = 5
        private const val U_LENGTH = 1L
        private const val D_LENGTH = 1L

        fun generate(): Password {
            val firstGroup = (1..L_LENGTH).map { nextInt(0, LOWER.size) }.toMutableList()
                .map(LOWER::get).toMutableList()
            firstGroup.shuffle()

            val secondGroup = (1..L_LENGTH).map { nextInt(0, LOWER.size) }
                .map(LOWER::get).toMutableList()
            secondGroup.shuffle()

            val digit = (1..D_LENGTH).map { nextInt(0, DIGIT.size) }
                .map(DIGIT::get)
            val upper = (1..U_LENGTH).map { nextInt(0, UPPER.size) }
                .map(UPPER::get)
            // Randomly prepend or append digit to the first group.
            // We shuffle the parts later anyway, so it might end up in the second group in the last step
            if (nextBoolean()) {
                firstGroup.addAll(digit)
            } else {
                firstGroup.addAll(0, digit)
            }
            // Randomly prepend or append upper to the second group
            if (nextBoolean()) {
                secondGroup.addAll(upper)
            } else {
                secondGroup.addAll(0, upper)
            }

            val allGroups = mutableListOf(firstGroup.joinToString(""), secondGroup.joinToString(""))
            allGroups.shuffle()
            return Password(allGroups.joinToString("-"))
        }
    }

    override fun toString(): String {
        return password
    }

}
