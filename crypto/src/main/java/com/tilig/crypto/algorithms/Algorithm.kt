package com.tilig.crypto.algorithms

import android.util.Base64
import java.security.KeyPair
import java.security.PrivateKey
import java.security.Provider
import java.security.PublicKey
import javax.crypto.Cipher

abstract class Algorithm {

    lateinit var provider: Provider

    open fun init() {}

    abstract fun generateKeyPair(): KeyPair

    fun loadPrivateKeyBase64(base64: String, isPKCS8: Boolean = false): PrivateKey {
        val raw = Base64.decode(base64, Base64.DEFAULT)
        return loadPrivateKey(raw, isPKCS8)
    }

    fun loadPublicKeyBase64(base64: String, isPKCS8: Boolean = false): PublicKey {
        val raw = Base64.decode(base64, Base64.DEFAULT)
        return loadPublicKey(raw, isPKCS8)
    }

    abstract fun loadPrivateKey(raw: ByteArray, isPKCS8: Boolean): PrivateKey

    abstract fun loadPublicKey(raw: ByteArray, isPKCS8: Boolean): PublicKey

    abstract fun createCipher(provider: Provider): Cipher

    abstract fun getName(): String

}
