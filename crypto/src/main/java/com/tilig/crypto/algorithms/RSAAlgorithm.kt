package com.tilig.crypto.algorithms

import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import androidx.annotation.RequiresApi
import org.bouncycastle.jce.provider.BouncyCastleProvider
import java.math.BigInteger
import java.security.*
import java.security.spec.X509EncodedKeySpec
import javax.crypto.Cipher
import javax.security.auth.x500.X500Principal

@RequiresApi(Build.VERSION_CODES.M)
class RSAAlgorithm(provider: Provider) : Algorithm() {

    private lateinit var keyPairGenerator: KeyPairGenerator

    companion object {
        private const val KEY_ALIAS = "tilig_key"
        private const val KEY_SERIAL = 1L
        private const val TIMEOUT_S = 30

        private const val CIPHER = "RSA/ECB/PKCS1PADDING"
    }

    init {
        super.provider = provider
    }

    override fun init() {

        val providers = Security.getProviders()
        val provider = providers.first()

        // Create a RSA key pair and store it in the Android Keystore
        keyPairGenerator = KeyPairGenerator.getInstance(
            KeyProperties.KEY_ALGORITHM_RSA,
            provider
        )

        val parameterSpec: KeyGenParameterSpec = KeyGenParameterSpec.Builder(
            KEY_ALIAS,
            KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
        ).run {
            setCertificateSerialNumber(BigInteger.valueOf(KEY_SERIAL))
            setCertificateSubject(X500Principal("CN=$KEY_ALIAS"))
            setDigests(KeyProperties.DIGEST_SHA256)
            setSignaturePaddings(KeyProperties.SIGNATURE_PADDING_RSA_PKCS1)
            //setCertificateNotBefore(startDate.time)
            //setCertificateNotAfter(endDate.time)
            setUserAuthenticationRequired(true)
            //setUserAuthenticationParameters(30, KeyGenParameterSpec.Builder.)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                setUserAuthenticationParameters(
                    TIMEOUT_S,
                    KeyProperties.AUTH_DEVICE_CREDENTIAL
                            or KeyProperties.AUTH_BIOMETRIC_STRONG
                )
            } else {
                @Suppress("DEPRECATION")
                setUserAuthenticationValidityDurationSeconds(TIMEOUT_S)
            }
            build()
        }

        // Initialization of key generator with the parameters we have specified above
        keyPairGenerator.initialize(parameterSpec)
    }

    override fun getName(): String = CIPHER

    // If we want to use the ones from the Bouncycastle lib
    private fun setSecurityProvider() {
        Security.removeProvider("BC")
        Security.addProvider(BouncyCastleProvider())
    }

    override fun generateKeyPair(): KeyPair {
        return keyPairGenerator.genKeyPair()
    }

    override fun loadPublicKey(raw: ByteArray, isPKCS8: Boolean): PublicKey {
        val keySpec = X509EncodedKeySpec(raw)
        val keyFactory = KeyFactory.getInstance(KeyProperties.KEY_ALGORITHM_RSA)
        return keyFactory.generatePublic(keySpec)
    }

    override fun loadPrivateKey(raw: ByteArray, isPKCS8: Boolean): PrivateKey {
        val keySpec = X509EncodedKeySpec(raw)
        val keyFactory = KeyFactory.getInstance(KeyProperties.KEY_ALGORITHM_RSA)
        return keyFactory.generatePrivate(keySpec)
    }

    override fun createCipher(provider: Provider): Cipher =
        Cipher.getInstance(CIPHER, provider)

}
