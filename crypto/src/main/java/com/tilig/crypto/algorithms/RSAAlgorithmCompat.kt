package com.tilig.crypto.algorithms

import java.security.*
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import javax.crypto.Cipher

class RSAAlgorithmCompat(provider: Provider) : Algorithm() {

    companion object {
        const val KEY_SIZE = 2048
        const val KEY_ALGORITHM = "RSA"

        //        const val CIPHER = "RSA/NONE/OAEPWithSHA256AndMGF1Padding"
        const val CIPHER = "RSA/NONE/OAEPWithSHA1AndMGF1Padding"

        //        const val CIPHER = "RSA/ECB/PKCS1Padding"
//        const val CIPHER = "AES/CBC/PKCS7PADDING"
        const val FORCE_PKCS8 = true
    }

    /*
const KEY_ALGORITHM = "rsa";
const KEY_SIZE = 4096;
const PUB_KEY_TYPE = "spki";
const KEY_FORMAT = "pem";
const PRIV_KEY_TYPE = "pkcs8";
     */

    init {
        super.provider = provider
    }

    override fun getName(): String = CIPHER

    override fun generateKeyPair(): KeyPair {
        val kpg = KeyPairGenerator.getInstance(KEY_ALGORITHM)
        kpg.initialize(KEY_SIZE)
        return kpg.genKeyPair()
    }

    override fun loadPublicKey(raw: ByteArray, isPKCS8: Boolean): PublicKey {
        val keySpec = if (isPKCS8KeySpec(raw)) {
            PKCS8EncodedKeySpec(raw)
        } else {
            X509EncodedKeySpec(raw)
        }
        val keyFactory = KeyFactory.getInstance(KEY_ALGORITHM, provider)
        return keyFactory.generatePublic(keySpec)
    }

    override fun loadPrivateKey(raw: ByteArray, isPKCS8: Boolean): PrivateKey {
        val keySpec = if (isPKCS8 || isPKCS8KeySpec(raw)) {
            PKCS8EncodedKeySpec(raw)
        } else {
            X509EncodedKeySpec(raw)
        }
        val keyFactory = KeyFactory.getInstance(KEY_ALGORITHM, provider)
        return keyFactory.generatePrivate(keySpec)
    }

    private fun isPKCS8KeySpec(raw: ByteArray): Boolean =
        String(raw).contains(" RSA ")

    override fun createCipher(provider: Provider): Cipher = Cipher.getInstance(CIPHER, provider)


}
