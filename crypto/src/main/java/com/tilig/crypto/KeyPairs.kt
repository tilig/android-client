package com.tilig.crypto
import com.goterl.lazysodium.utils.KeyPair
import java.security.KeyPair as JavaKeyPair

typealias LegacyKeyPair = JavaKeyPair

data class KeyPairString(
    /**
     * Base64 encoded version of the private key (or 'secret' key in libsodium)
     */
    val private: String?,
    /**
     * Base64 encoded version of the public key
     */
    val public: String?
)

data class KeyPairsString(
    val legacy: KeyPairString?,
    val keypair: KeyPairString?
)

data class KeyPairs(
    val legacy: LegacyKeyPair?,
    val keypair: KeyPair?
)
