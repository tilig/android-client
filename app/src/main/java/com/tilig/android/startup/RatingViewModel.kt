package com.tilig.android.startup

import android.app.Activity
import android.util.Log
import androidx.lifecycle.ViewModel
import com.google.android.play.core.review.ReviewManager
import com.google.android.play.core.review.ReviewManagerFactory
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.utils.SharedPrefs
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

enum class RatingStatus {
    IDLE,
    ERROR,
    TIME_TO_ASK,
    COMPLETED
}

data class RatingUiState(
    val status: RatingStatus,
)

class RatingViewModel : ViewModel(), KoinComponent {

    companion object {

        private const val TAG = "RatingHelper"
        private const val AUTOFILLS_BEFORE_ASKING = 3
    }

    private val prefs: SharedPrefs by inject()
    private val tracker: Mixpanel by inject()

    val uiState = MutableStateFlow(
        RatingUiState(
            status = RatingStatus.IDLE
        )
    )

    fun checkForRating() {
        // Check the prefs if we really need to ask for a rating
        val isTime = prefs.autofillCount >= AUTOFILLS_BEFORE_ASKING
                && !prefs.hasAskedForRating
                && prefs.isAutofillEnabled == true
                && !prefs.isFirstLaunch
        if (isTime) {
            uiState.update {
                it.copy(
                    status = RatingStatus.TIME_TO_ASK
                )
            }
        }
    }

    private fun neverAskAgain() {
        prefs.hasAskedForRating = true
    }

    fun askForRating(activity: Activity): ReviewManager {
        neverAskAgain()

        tracker.trackEvent(Tracker.EVENT_ASKED_FOR_RATING)

        val manager = ReviewManagerFactory.create(activity)
        manager
            .requestReviewFlow()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val reviewInfo = task.result
                    if (!activity.isFinishing) {
                        manager
                            .launchReviewFlow(activity, reviewInfo)
                            .addOnCompleteListener {
                                // The flow has finished. The API does not indicate whether the user
                                // reviewed or not, or even whether the review dialog was shown. Thus, no
                                // matter the result, we continue our app flow.
                                uiState.update {
                                    it.copy(
                                        status = RatingStatus.COMPLETED
                                    )
                                }
                            }
                    }
                } else {
                    Log.e(
                        TAG, "Error while checking for rating request: ${task.exception?.message}"
                    )
                    uiState.update {
                        it.copy(
                            status = RatingStatus.ERROR
                        )
                    }
                }
            }
        return manager
    }
}
