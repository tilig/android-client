package com.tilig.android.startup

import android.app.Application
import android.content.Context
import android.os.StrictMode
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.startup.Initializer
import com.tilig.android.BuildConfig
import com.tilig.android.TiligApp
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Breadcrumb
import com.tilig.android.featureflag.FeatureFlagClient
import com.tilig.android.utils.KoinHelper
import com.tilig.android.utils.SharedPrefs
import com.tilig.android.utils.lock.LockUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.util.concurrent.Executors

// We use a dummy class because we're not actually initializing a singleton,
// we're just executing some boilerplate inside the [TiligAppInitializer#create] method.
class TiligAppDummy

class TiligAppInitializer : Initializer<TiligAppDummy>, KoinComponent {

    companion object {
        // Helps figuring out why we get "A resource failed to call close"
        private const val DEBUG_ENABLE_UNCLOSED_RESOURCE_DETECTION = false
    }

    override fun create(context: Context): TiligAppDummy {
        // This before we can inject anything
        KoinHelper.start(context.applicationContext as Application)

        val prefs: SharedPrefs by inject()
        val mixpanel: Mixpanel by inject()
        val lockUtil: LockUtil by inject()
        val featureFlagClient: FeatureFlagClient by inject()

        if (!prefs.mixpanelUserId.isNullOrEmpty()) {
            mixpanel.setIdentity(prefs.mixpanelUserId!!)
            Executors.newSingleThreadExecutor().execute {
                featureFlagClient.setIdentity(prefs.mixpanelUserId!!)
            }
        }
        Breadcrumb.identify(prefs)

        // Set up lifecycle listener
        (context.applicationContext as TiligApp).lockUtil = lockUtil
        ProcessLifecycleOwner.get().lifecycle.addObserver(lockUtil)

        // Helps figuring out why we get "A resource failed to call close"
        // Note: so far it looks like Sentry is not always closing the file it is writing its
        // stacktraces or breadcrumbs to. Seems to be out of our control.
        if (BuildConfig.DEBUG && DEBUG_ENABLE_UNCLOSED_RESOURCE_DETECTION) {
            StrictMode.setVmPolicy(
                StrictMode.VmPolicy.Builder(StrictMode.getVmPolicy())
                    .detectLeakedClosableObjects()
                    .build()
            )
        }

        return TiligAppDummy()
    }

    override fun dependencies(): MutableList<Class<out Initializer<*>>> =
        emptyList<Class<out Initializer<*>>>().toMutableList()
}
