package com.tilig.android.startup

import android.annotation.SuppressLint
import android.app.Activity
import android.util.Log
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.tilig.android.R
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update

enum class UpdateStatus {
    IDLE,
    UPDATE_AVAILABLE,
    APPROVED,
    DOWNLOADING,
    DOWNLOADED,
    INSTALLING,
    DONE,
    ERROR
}

data class UpdateUiState(
    val status: UpdateStatus,
    val progress: Int,
    val isHighPriority: Boolean,
    val info: AppUpdateInfo? = null,
    @StringRes val message: Int? = null
)

class UpdateViewModel : ViewModel() {

    companion object {

        private const val TAG = "UpdateHelper"
        private const val UPDATE_REQUEST_CODE = 23564
        private const val DAYS_BEFORE_FLEXIBLE_UPDATE_BECOMES_FORCED = 3
        private const val DAYS_BEFORE_LOWPRIO_UPDATE_BECOMES_VISIBLE = 14

        /**
         * Priority thresholds on a 0-5 scale.
         * Traditionally:
         * 4+  is considered high-prio (blocking)
         * 2-3 is medium (flexible update, user can close the popup)
         * <2  is low prio
         *
         * However, since priorities cannot be set when publishing through a standard upload,
         * the priority will always be 0.
         *
         * Priorities can only be set when publishing through the Play Store API,
         * which we'll do via Bitrise.
         * To kinda-sorta force less important updates as well, we automatically increase the
         * priority from 0 after 2 weeks.
         */
        private const val HIGH_PRIORITY = 4
        private const val MEDIUM_PRIORITY = 2

        // NOTICE
        // Looks like the medium-priority updates don't quite work yet. We can initate the update
        // but we never get the callback that the download is finished
        private const val DEBUG_EVERY_UPDATE_IS_HIGH_PRIORITY = false
        private const val DEBUG_EVERY_UPDATE_IS_MEDIUM_PRIORITY = false
    }

    val uiState = MutableStateFlow(
        UpdateUiState(
            status = UpdateStatus.IDLE,
            progress = 0,
            isHighPriority = false,
            message = null
        )
    )

    @SuppressLint("SwitchIntDef")
    private val listener: InstallStateUpdatedListener =
        InstallStateUpdatedListener { installState ->
            when (installState.installStatus()) {
                InstallStatus.DOWNLOADED -> {
                    uiState.update {
                        it.copy(
                            status = UpdateStatus.DOWNLOADED,
                            message = R.string.update_message_downloaded
                        )
                    }
                }
                InstallStatus.FAILED -> {
                    // TODO track in mixpanel?
                    uiState.update {
                        it.copy(
                            status = UpdateStatus.ERROR,
                            message = R.string.update_message_failed
                        )
                    }
                }
                InstallStatus.DOWNLOADING -> {
                    Log.v(TAG, "Downloading ${installState.bytesDownloaded()} / ${installState.totalBytesToDownload()}")
                }
            }
        }

    fun checkForUpdates(activity: Activity): AppUpdateManager {
        val appUpdateManager = AppUpdateManagerFactory.create(activity)
        val appUpdateInfoTask = appUpdateManager.appUpdateInfo
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->

            val stalenessDays = appUpdateInfo.clientVersionStalenessDays() ?: -1
            val priority = appUpdateInfo.updatePriority()
            val isHighPriority =
                DEBUG_EVERY_UPDATE_IS_HIGH_PRIORITY
                        || priority >= HIGH_PRIORITY
                        || stalenessDays >= DAYS_BEFORE_FLEXIBLE_UPDATE_BECOMES_FORCED
            val isMediumPriority =
                DEBUG_EVERY_UPDATE_IS_MEDIUM_PRIORITY
                        || priority >= MEDIUM_PRIORITY
                        || stalenessDays >= DAYS_BEFORE_LOWPRIO_UPDATE_BECOMES_VISIBLE

            when (appUpdateInfo.updateAvailability()) {
                UpdateAvailability.UPDATE_AVAILABLE -> {
                    Log.v(TAG, "Update available")
                    if (
                        appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
                        && isHighPriority
                    ) {
                        // Request an immediate update (blocking)
                        uiState.update {
                            it.copy(
                                status = UpdateStatus.UPDATE_AVAILABLE,
                                isHighPriority = true,
                                info = appUpdateInfo,
                            )
                        }
                    } else if (
                        appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)
                        && isMediumPriority
                    ) {
                        uiState.update {
                            it.copy(
                                status = UpdateStatus.UPDATE_AVAILABLE,
                                isHighPriority = false,
                                info = appUpdateInfo,
                            )
                        }
                    } else {
                        // no-op. The update is (still) low priority so we wait for the
                        // Play Store to update it on its own time.
                    }
                }
                UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS -> {
                    Log.v(TAG, "Developer triggered update in progress")
                    uiState.update {
                        it.copy(
                            status = UpdateStatus.DOWNLOADED,
                            isHighPriority = isHighPriority,
                            info = appUpdateInfo,
                            message = R.string.update_message_downloaded
                        )
                    }
                }
                UpdateAvailability.UNKNOWN -> {
                    // ignore
                    Log.v(TAG, "Update status unknown")
                }
                UpdateAvailability.UPDATE_NOT_AVAILABLE -> {
                    // all is well
                    Log.v(TAG, "No update available")
                }
            }
        }
        return appUpdateManager
    }

    fun startUpdate(
        activity: Activity,
        appUpdateManager: AppUpdateManager,
        appUpdateInfo: AppUpdateInfo,
        force: Boolean
    ) {
        appUpdateManager.startUpdateFlowForResult(
            appUpdateInfo,
            if (force) AppUpdateType.IMMEDIATE else AppUpdateType.FLEXIBLE,
            activity,
            UPDATE_REQUEST_CODE
        )
    }

    fun completeUpdate(appUpdateManager: AppUpdateManager) {
        appUpdateManager.completeUpdate()
        uiState.update {
            it.copy(
                status = UpdateStatus.INSTALLING,
                isHighPriority = false,
                info = null,
                message = null
            )
        }
    }

    fun registerListener(appUpdateManager: AppUpdateManager) {
        appUpdateManager.registerListener(listener)

//        appUpdateManager
//            .appUpdateInfo
//            .addOnSuccessListener(listener)
    }

    fun unregisterListener(appUpdateManager: AppUpdateManager) {
        appUpdateManager.unregisterListener(listener)
    }

    fun handleActivityResult(requestCode: Int, resultCode: Int) {
        if (requestCode == UPDATE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                // Update flow is finished  (or: started downloading and we'll be called later -- either way we're done here for now)
                Log.v(TAG, "Update flow finished")
                uiState.update {
                    it.copy(
                        status = UpdateStatus.APPROVED
                    )
                }
            } else {
                Log.e(TAG, "Update flow failed! Result code: $resultCode")
                uiState.update {
                    it.copy(
                        status = UpdateStatus.ERROR
                    )
                }
            }
        }
    }

    fun tryLater() {
        uiState.update {
            it.copy(
                status = UpdateStatus.IDLE,
                message = null
            )
        }
    }
}
