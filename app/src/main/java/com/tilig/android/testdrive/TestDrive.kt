package com.tilig.android.testdrive

import com.tilig.android.data.models.tilig.*
import com.tilig.crypto.Password

/**
 * Sample items for a new user.
 * @Deprecated
 */
@Deprecated(
    "Is not used with new get started flow. Is used only for migration to make sure all test drive items" +
            " that have been updated  previously, were migrated to regular items"
)
object TestDrive {

    private const val TEST_DRIVE_ACCOUNT_ID = "account_test_drive_id"
    private const val TEST_DRIVE_NOTE_ID = "account_test_note_id"
    private const val TEST_DRIVE_CREDIT_CARD_ID = "credit_card_test_drive_id"
    private const val TEST_DRIVE_WIFI_ID = "wifi_test_drive_id"

    fun isTestDriveItem(id: String?) = listOf(
        TEST_DRIVE_ACCOUNT_ID,
        TEST_DRIVE_NOTE_ID,
        TEST_DRIVE_CREDIT_CARD_ID,
        TEST_DRIVE_WIFI_ID
    ).contains(id)

    fun getLocalAccountWebsite() =
        "https://somewebsite.online/"

    fun isLegacyTestItemWasUpdated(legacyItem: SecretItem): Boolean {
        when (legacyItem) {
            is Account -> {
                val originalTestAccount = createLocalTestAccount(null)
                // ignore password in this case. As the site is not real and even
                // if a user updated a pass it could not be considered a real account
                return !(originalTestAccount.id == legacyItem.id &&
                        originalTestAccount.name == legacyItem.name &&
                        originalTestAccount.username == legacyItem.username &&
                        originalTestAccount.website?.contains(
                            legacyItem.website ?: ""
                        ) == true &&
                        originalTestAccount.notes == legacyItem.notes)
            }
            is Note -> {
                val originalTestNote = createLocalTestNote()
                return !(originalTestNote.id == legacyItem.id &&
                        originalTestNote.name == legacyItem.name &&
                        originalTestNote.content == legacyItem.content)
            }
            is WifiPassword -> {
                val originalTestWifi = createLocalTestWifi()
                return !(originalTestWifi.id == legacyItem.id &&
                        originalTestWifi.name == legacyItem.name &&
                        originalTestWifi.networkName == legacyItem.networkName &&
                        originalTestWifi.password == legacyItem.password &&
                        originalTestWifi.extraInfo == legacyItem.extraInfo)
            }
            is CreditCard -> {
                val originalTestCreditCard = createLocalTestCreditCard()
                return !(originalTestCreditCard.id == legacyItem.id &&
                        originalTestCreditCard.name == legacyItem.name &&
                        originalTestCreditCard.cardNumber == legacyItem.cardNumber &&
                        originalTestCreditCard.holderName == legacyItem.holderName &&
                        originalTestCreditCard.expireDate == legacyItem.expireDate &&
                        originalTestCreditCard.securityCode == legacyItem.securityCode &&
                        originalTestCreditCard.pinCode == legacyItem.pinCode &&
                        originalTestCreditCard.extraInfo == legacyItem.extraInfo)
            }
        }
        return false
    }

    private fun createLocalTestAccount(brand: Brand?): Account {
        val account = Account.createEmpty().copy(
            id = TEST_DRIVE_ACCOUNT_ID,
            brand = brand
        )
        account.apply {
            name = "Demo Account"
            username = "tilig_demo"
            website = getLocalAccountWebsite()
            notes =
                " \uD83D\uDC4B This is what an account looks like in Tilig. Try clicking on the Website link to visit our demo website and experience how Tilig can autofill your account details everywhere you login."
            password = Password.generate().toString()
        }
        return account
    }

    private fun createLocalTestNote(): Note {
        val note = Note.createEmpty().copy(
            id = TEST_DRIVE_NOTE_ID
        )
        note.name = "Secure notes: an intro"
        note.legacyEncryptionDisabled = true
        note.content =
            "Secure notes are ideal for storing sensitive information. They benefit from the same encrypted as your passwords. Hackers will need thousands of years to crack it. "
        return note
    }

    private fun createLocalTestCreditCard(): CreditCard {
        return CreditCard.createEmpty().copy(
            id = TEST_DRIVE_CREDIT_CARD_ID
        ).apply {
            name = "Credit Card: an example"
            cardNumber = "1234567890123456"
            holderName = "Your name"
            expireDate = "12/99"
            securityCode = "789"
            pinCode = "0000"
            extraInfo =
                "This is how your credit card information would appear.\n\n\n---\nFeel free to delete this credit card"
        }
    }

    private fun createLocalTestWifi(): WifiPassword {
        return WifiPassword.createEmpty().copy(
            id = TEST_DRIVE_WIFI_ID
        ).apply {
            name = "Wifi - An intro"
            networkName = "Home Wireless Lan Network"
            password = "The_password_of_the_network"
            extraInfo =
                "In this area, you can store more information about the network.\n\nFor example, about the location, if it is metered or perhaps hidden.\n\n---\nYou can delete this item whenever you like."
        }
    }
}

