package com.tilig.android

import android.app.Activity
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.DeadObjectException
import android.service.autofill.FillRequest
import android.view.WindowManager
import android.view.autofill.AutofillManager
import android.view.inputmethod.InputMethodManager
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.android.installreferrer.api.InstallReferrerClient
import com.android.installreferrer.api.InstallReferrerStateListener
import com.android.installreferrer.api.ReferrerDetails
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.tilig.android.analytics.Breadcrumb
import com.tilig.android.analytics.ConversionAnalytics
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.autofill.AuthLevel
import com.tilig.android.autofill.FillResponseFactory
import com.tilig.android.autofill.PackageVerifier
import com.tilig.android.autofill.google.StructureParser
import com.tilig.android.data.livedata.ForcedLogoutData
import com.tilig.android.data.livedata.ForcedLogoutLiveData
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.ui.CryptoModel
import com.tilig.android.ui.LaunchModeView
import com.tilig.android.ui.StartFragment
import com.tilig.android.utils.DebugLogger
import com.tilig.android.utils.SharedPrefs
import com.tilig.android.utils.getDomain
import com.tilig.crypto.Crypto
import io.sentry.Sentry
import io.sentry.SentryLevel
import org.koin.core.component.inject
import kotlin.random.Random


class MainActivity : LockActivity(R.layout.activity_main), LaunchModeView {

    private val forceLogout: ForcedLogoutLiveData by inject()
    private val preferences: SharedPrefs by inject()
    private val tracker: Mixpanel by inject()
    private val cryptoModel: CryptoModel by viewModels()

    @Suppress("UnnecessaryOptInAnnotation")
    @OptIn(ExperimentalAnimationApi::class, ExperimentalPagerApi::class)
    private val logoutObserver: (ForcedLogoutData) -> Unit = { logout ->
        if (logout.forceLogout && preferences.firebaseUserId != null) {
            forceLogout.preventLoop()
            // Stop receiving loop false value
            forceLogout.removeObservers(this)

            Breadcrumb.drop(Breadcrumb.LoggingOut)

            // Logout from Google
            val gso =
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).build()
            GoogleSignIn.getClient(this@MainActivity, gso).signOut()

            // End mixpanel session
            mixpanel.logout()

            // Logout from app itself
            preferences.clear(logoutOnly = true)
            cryptoModel.uninitialize()

            if (BuildConfig.DEBUG) {
                DebugLogger().apply {
                    collect("Logged out")
                    collect("CryptoModel has keys: ${cryptoModel.hasLoadedKeys()}")
                    logCollection(this@MainActivity)
                }
            }

            // Navigate to start fragment where log out will be performed
            val bundle = Bundle()
            bundle.putBoolean(StartFragment.ARG_ACCOUNT_DELETED, !logout.userExists)
            findNavController(R.id.nav_host_fragment).navigate(R.id.startFragment, args = bundle)
        }
    }

    companion object {
        private const val REQUEST_CODE = 123

        // Screenshots will be disabled for most devices, because our method of hiding exposed
        // passwords in the onPause is not reliable.
        // Update: disabled since the redesign
        const val ENABLE_SCREENSHOT_PREVENTION = false

        private const val EXTRA_LAUNCH_MODE_STR = "launch_mode"
        const val VALUE_LAUNCH_MODE_ADD = "create_new"
        const val VALUE_LAUNCH_MODE_SEARCH = "search"

        const val EXTRA_FILL_REQUEST_PARC = "fill_request"
        const val EXTRA_ADD_APP_ID_STR = "app_id"
        const val EXTRA_ADD_URL_STR = "url"
        const val EXTRA_ADD_LOGIN_STR = "login"
        const val EXTRA_ADD_PASSWORD_STR = "passwd"
        const val EXTRA_ADD_USERNAME_STR = "username"

        @RequiresApi(Build.VERSION_CODES.O)
        fun createIntentForSearch(
            context: Context,
            fillRequest: FillRequest,
            applicationId: String?,
            url: String? = null,
            login: String? = null
        ): Intent {
            return Intent(context, MainActivity::class.java).apply {
                putExtra(EXTRA_FILL_REQUEST_PARC, fillRequest)
                putExtra(EXTRA_LAUNCH_MODE_STR, VALUE_LAUNCH_MODE_SEARCH)
                putExtra(EXTRA_ADD_APP_ID_STR, applicationId)
                putExtra(EXTRA_ADD_URL_STR, url)
                putExtra(EXTRA_ADD_LOGIN_STR, login)
                // This is a hack that enables us to create two PendingIntents at the same
                // time for the same Activity: one for search, one for creating a new secret
                action = Random.Default.nextInt().toString()
            }
        }

        @RequiresApi(Build.VERSION_CODES.O)
        fun createPendingIntentForSearch(
            context: Context,
            fillRequest: FillRequest,
            applicationId: String?,
            url: String? = null,
            login: String? = null
        ): PendingIntent {
            val intent = createIntentForSearch(
                context, fillRequest, applicationId, url, login
            )
            return PendingIntent.getActivity(
                context,
                REQUEST_CODE,
                intent,
                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Deeplinks
        FirebaseDynamicLinks.getInstance()
            .getDynamicLink(intent)
            .addOnSuccessListener(
                this
            ) {
                // no-op
            }
            .addOnFailureListener(
                this
            ) {
            }

        // Prevent screenshots, screen recordings, and showing details in the app switcher
        if (!BuildConfig.DEBUG && ENABLE_SCREENSHOT_PREVENTION) {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE
            )
        }

        // Always hide the keyboard when changing fragments
        supportFragmentManager.findFragmentById(R.id.nav_host_fragment)?.findNavController()
            ?.addOnDestinationChangedListener { _, _, _ ->
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(window.decorView.windowToken, 0)
            }

        // Do some logging
        Breadcrumb.drop(
            if (preferences.isLoggedIn())
                Breadcrumb.CryptoTrail_LoggedIn
            else
                Breadcrumb.CryptoTrail_LoggedOut
        )

        // Check install referrer
        if (preferences.isFirstLaunch) {
            runOnFirstLaunch()
            preferences.isFirstLaunch = false
        }
    }

    private fun runOnFirstLaunch() {
        val refClient = InstallReferrerClient.newBuilder(this).build()
        refClient.startConnection(object : InstallReferrerStateListener {

            fun collectInstallInformation() {
                val response: ReferrerDetails = refClient.installReferrer
                val conversions: ConversionAnalytics by inject()
                val mixpanel: Mixpanel by inject()

                conversions.trackUtmReferrer(response)
                mixpanel.trackUtmReferrer(response)
            }

            override fun onInstallReferrerSetupFinished(responseCode: Int) {
                when (responseCode) {
                    InstallReferrerClient.InstallReferrerResponse.OK -> {
                        // Connection established.
                        collectInstallInformation()
                    }
                    InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED -> {
                        // API not available on the current Play Store app.
                    }
                    InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE -> {
                        // Connection couldn't be established.
                    }
                }
                refClient.endConnection()
            }

            override fun onInstallReferrerServiceDisconnected() {

            }

        })
    }

    override fun onResume() {
        super.onResume()

        // observe logout event if no active observers and user is logged in
        if (!forceLogout.hasActiveObservers() && preferences.firebaseUserId != null) {
            forceLogout.observe(this, logoutObserver)
        }

        // Track changes to autofill. We do that here, in the onResume, to make sure we also catch
        // the change when the user switched Autofill provider by following a competitor's
        // instructions.
        val isEnabled = autofill.isAutofillEnabled()
        if (isEnabled != preferences.isAutofillEnabled) {
            if (isEnabled) {
                tracker.trackEvent(Tracker.EVENT_AUTOFILL_ENABLED)
                conversions.trackEvent(ConversionAnalytics.EVENT_AUTOFILL_ENABLED)
            } else {
                tracker.trackEvent(Tracker.EVENT_AUTOFILL_DISABLED)
            }
            // Note that we leave prefs.mayAskForAutofill the way it is.
            // Since we have the yellow bar at the bottom now, we WON'T
            // start the autofill screen anymore, we'll use that bar instead.
            preferences.isAutofillEnabled = isEnabled
        }
    }

    override fun isInCreateNewMode(): Boolean = intent.hasExtra(EXTRA_LAUNCH_MODE_STR)
            && intent.getStringExtra(EXTRA_LAUNCH_MODE_STR) == VALUE_LAUNCH_MODE_ADD

    override fun isInSearchMode(): Boolean = intent.hasExtra(EXTRA_LAUNCH_MODE_STR)
            && intent.getStringExtra(EXTRA_LAUNCH_MODE_STR) == VALUE_LAUNCH_MODE_SEARCH

    override fun getSearchQuery(): String? {
        val appId = intent.getStringExtra(EXTRA_ADD_APP_ID_STR)
        val url = intent.getStringExtra(EXTRA_ADD_URL_STR)
        if (!url.isNullOrBlank()) {
            return url.getDomain()
        } else if (!appId.isNullOrEmpty()) {
            return PackageVerifier.getAppName(this, appId)
        }
        return null
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun returnAutofillWithAccount(account: Account) {
        Breadcrumb.drop(Breadcrumb.CryptoTrail_ReturnToAutofill)

        // Make sure the keypair is set. This usually already happened
        // when editing an account, but when adding a new one from the
        // autofill flow, we still need to:
        cryptoModel.withInitializedCrypto { crypto ->
            returnAutofillWithAccount(account, crypto)
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun returnAutofillWithAccount(account: Account, crypto: Crypto) {
        val fillRequest = intent.getParcelableExtra<FillRequest>(EXTRA_FILL_REQUEST_PARC)!!
        val parser: StructureParser

        try {
            parser = StructureParser(applicationContext, fillRequest)
            parser.parse(forFill = true)
        } catch (e: Exception) {
            // This happens, and we don't know why. Docs don't say much, the internet suggests there's
            // a parcel too big somewhere, but that doesn't make sense either. I suspect the parser
            // tries to access the previous screen again, and the device has already cleared it
            // from memory. In that case, there's not much we can do: we can't parse it,
            // then we cant tell the system which value to put in which field. The rest of this
            // method will still execute, but the target app will not be filled with anything.
            // Original Sentry error: https://sentry.io/organizations/tilig/issues/3084436429/?environment=production&project=5237937
            // Note the 'DEVICE_STORAGE_LOW' event right before, usually.
            //
            // We catch 3 errors btw, as sometimes the error is wrapped in Runtime- or IllegalStateException.
            when (e) {
                is DeadObjectException,
                is RuntimeException,
                is IllegalStateException -> {
                    Sentry.captureMessage(
                        e.message ?: "DeadObjectException in returnAutofillWithAccount",
                        SentryLevel.DEBUG
                    )
                    finish()
                    return
                }
                else -> throw e
            }
        }

        val intent = Intent()

        if (cryptoModel.hasLoadedKeys()) {
            val dataset = FillResponseFactory.createDataset(
                context = this,
                auth = AuthLevel.AUTHENTICATED_UNLOCKED,
                fillRequest = fillRequest,
                account = account,
                autofillFieldMetadata = parser.autofillFields,
                accounts = arrayListOf(account),
                crypto = crypto
            )
            intent.apply {
                // We return a dataset here to replace the
                // ResponseRequest that was created in the service.
                // The new dataset replaces the dataset that requires authentication and the views are filled out immediately
                putExtra(AutofillManager.EXTRA_AUTHENTICATION_RESULT, dataset)
            }
        } else {
            // This scenario is theoretically impossibly,
            // since this method is only reachable when the user is logged in
            // (at which point crypto keys are present)
            // but sometimes the crypto keys cannot are apparently empty anyway.
            // So, we redirect to the login screen again in that case, to be sure.
            // The unlikeliness is already logged to Sentry in the .initCryptoSafely
            // method so we don't overlook this issue.
            val response = FillResponseFactory.createAuthResponse(
                context = this,
                fillRequest = fillRequest,
                parser = parser
            )
            intent.apply {
                // We return a fillResponse here to replace the
                // ResponseRequest that was created in the service.
                putExtra(AutofillManager.EXTRA_AUTHENTICATION_RESULT, response)
            }
        }

        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        mixpanel.onSessionEnd()
    }
}
