package com.tilig.android.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.compose.ui.graphics.toArgb
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.tilig.android.BaseActivity
import com.tilig.android.autofill.Autofill
import com.tilig.android.ui.theme.DefaultBackground
import com.tilig.android.utils.SharedPrefs
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

open class BaseFragment(@LayoutRes layout: Int) : Fragment(layout), KoinComponent {

    protected val prefs: SharedPrefs by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("BaseFrag", "${this::class.java.name}.onCreate")
    }

    protected fun getBaseActivity(): BaseActivity? = activity as? BaseActivity

    fun setStatusbarColorBright(bright: Boolean) {
        getBaseActivity()?.setStatusbarColorBright(bright)
    }

    fun snackbar(
        container: View,
        @StringRes msg: Int,
        duration: Int = Snackbar.LENGTH_LONG
    ) = snackbar(
        container,
        getString(msg),
        duration
    )

    fun snackbar(
        container: View,
        msg: String,
        duration: Int = Snackbar.LENGTH_LONG
    ) = Snackbar.make(
        container,
        msg,
        duration
    ).apply {
        view.setBackgroundColor(DefaultBackground.toArgb())
        show()
    }

    protected val autofill: Autofill?
        get() = (requireActivity() as? BaseActivity)?.autofill

}
