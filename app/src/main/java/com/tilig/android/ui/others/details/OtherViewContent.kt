package com.tilig.android.ui.others.details

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.UiError
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.Membership
import com.tilig.android.data.models.tilig.OtherItem
import com.tilig.android.ui.accounts.detail.components.ShareLinkComponent
import com.tilig.android.ui.accounts.detail.components.ShareLinkComponentCallback
import com.tilig.android.ui.components.TiligSpacerVerticalDefault
import com.tilig.android.ui.secrets.components.AllCustomFieldsViewComponent
import com.tilig.android.ui.secrets.details.BottomSheetType
import com.tilig.android.ui.secrets.details.ItemDetailsCallback
import com.tilig.android.ui.secrets.details.events.ItemDetailsActionEvent
import com.tilig.android.ui.theme.DefaultPadding

@Composable
fun OtherViewContent(
    otherItem: OtherItem,
    folder: Folder?,
    isLoading: Boolean,
    shareErrorMessage: UiError? = null,
    tracker: Tracker,
    callBack: ItemDetailsCallback
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(DefaultPadding)
    ) {

        AllCustomFieldsViewComponent(
            fields = otherItem.details.customFields
        ) {
            callBack.onShowSnackbarMessage(it)
        }

        TiligSpacerVerticalDefault()
        ShareLinkComponent(
            item = otherItem,
            folder = folder,
            isActionEnabled = !isLoading,
            shareErrorMessage = shareErrorMessage,
            callback = object : ShareLinkComponentCallback {
                override fun onRevokeAccess(member: Membership) {
                    callBack.onActionEvent(
                        ItemDetailsActionEvent.ShareRevoke(
                            folderId = folder!!.id,
                            memberId = member.id
                        )
                    )
                }

                override fun onOpenShareScreen() {
                    callBack.onUpdateBottomSheet(BottomSheetType.ShareWith(otherItem))
                }
            })
    }
}