import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.tilig.android.ui.theme.BackgroundGrey
import com.tilig.android.ui.theme.StyleStolzRegularDarkBlue16
import com.tilig.android.ui.theme.White

// A Text inside a Box inside a Column will allow us to vertically align the text. Source: https://stackoverflow.com/a/69406922
@Composable
fun SingleLetterIcon(
    text: String,
    backgroundColor: Color,
    size: Dp = 32.dp,
    textStyle: TextStyle = StyleStolzRegularDarkBlue16.copy(
        fontWeight = FontWeight.Bold,
        color = White
    ),
    applyBorder: Boolean = false,
    borderColor: Color = BackgroundGrey
) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .size(size)
                .background(color = backgroundColor, shape = CircleShape)
                .clip(CircleShape)
                .then(
                    if (applyBorder) {
                        Modifier.border(1.dp, color = borderColor, shape = CircleShape)
                    } else {
                        Modifier
                    }
                )
        ) {
            Text(
                style = textStyle,
                text = text.firstOrNull()?.toString() ?: " ",
                textAlign = TextAlign.Center,
            )
        }
    }
}
