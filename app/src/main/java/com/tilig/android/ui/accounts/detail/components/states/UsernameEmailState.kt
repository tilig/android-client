package com.tilig.android.ui.accounts.detail.components.states

import com.tilig.android.utils.isValidEmail

class UsernameEmailState(value: String?) :
    TextFieldState(
        fieldValue = value,
        validator = ::isUsernameEmailValid,
        errorFor = ::emailValidationError
    )

/**
 * Returns an error to be displayed or null if no error was found
 */
private fun emailValidationError(email: String): String {
    return "Invalid email or username: $email"
}

private fun isUsernameEmailValid(email: String): Boolean {
    return email.isValidEmail()
}
