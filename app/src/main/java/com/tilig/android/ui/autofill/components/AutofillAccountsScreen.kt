package com.tilig.android.ui.autofill.components

import android.annotation.SuppressLint
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.analytics.ConversionAnalytics
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.ui.components.*
import com.tilig.android.ui.secrets.SearchResultsEmptyScreen
import com.tilig.android.ui.secrets.SecretRow
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.SecretItemExt.getSecretIcon
import com.tilig.android.utils.SharedPrefs
import com.tilig.android.utils.ellipsize
import kotlinx.coroutines.launch
import org.koin.androidx.compose.inject


@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun AutofillMainScreen(
    account: Account,
    matchedAccounts: List<Account>,
    allAccounts: List<Account>,
    totalAccounts: Int,
    searchState: MutableState<String>,
    isDemoMode: Boolean,
    action: (AutofillPopupActions) -> Unit
) {
    val scaffoldState = rememberScaffoldState()

    val isSearchFocused = remember {
        mutableStateOf(false)
    }

    val isKeyboardVisible = WindowInsets.ime.getBottom(LocalDensity.current) > 0
    LaunchedEffect(key1 = isKeyboardVisible) {
        if (!isKeyboardVisible) {
            isSearchFocused.value = false
        }
    }

    Scaffold(
        scaffoldState = scaffoldState,
        floatingActionButton = {
            // only show if the logins are not empty and keyboard is hidden
            if (totalAccounts != 0 && WindowInsets.ime.getBottom(LocalDensity.current) == 0) {
                FloatingActionButton(
                    shape = RoundedCornerShape(DefaultCornerRadius),
                    backgroundColor = LightBlue,
                    contentColor = Color.White,
                    elevation = FloatingActionButtonDefaults.elevation(
                        pressedElevation = 0.dp,
                        defaultElevation = 0.dp
                    ),
                    onClick = {
                        action.invoke(AutofillPopupActions.StartAddAccount(account))
                    }) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_add),
                        contentDescription = "",
                        modifier = Modifier.padding(bottom = 0.dp)
                    )
                }
            }
        },
        modifier = Modifier
            .fillMaxSize()
            .clip(
                RoundedCornerShape(
                    bottomStart = DefaultCornerRadius,
                    bottomEnd = DefaultCornerRadius
                )
            )
    ) {
        AutofillResultsList(
            prefilledAccount = account,
            matchedAccounts = matchedAccounts,
            allAccounts = allAccounts,
            totalAccounts = totalAccounts,
            searchState = searchState,
            isSearchFocused = isSearchFocused,
            isDemoMode = isDemoMode,
            action = action
        )
    }
}

@Composable
private fun AutofillResultsList(
    prefilledAccount: Account,
    matchedAccounts: List<Account>,
    allAccounts: List<Account>,
    totalAccounts: Int,
    searchState: MutableState<String>,
    isSearchFocused: MutableState<Boolean>,
    isDemoMode: Boolean,
    action: (AutofillPopupActions) -> Unit
) {
    val tracker: Mixpanel by inject()
    val conversions: ConversionAnalytics by inject()
    val prefs: SharedPrefs by inject()

    val defaultName = stringResource(id = R.string.label_autofill_this_website)
    val title = remember {
        if (isDemoMode)
            defaultName
        else
            prefilledAccount
                .name
                .ellipsize(20, 15)
                .orEmpty()
                .ifEmpty { defaultName }
    }

    val focusRequester = remember { FocusRequester() }
    val listLoginsState = rememberLazyListState()
    val coroutineScope = rememberCoroutineScope()

    if (isSearchFocused.value) {
        Column(modifier = Modifier.fillMaxSize()) {
            if (allAccounts.isNotEmpty() || searchState.value.isNotEmpty()) {
                AutofillSearchComponent(
                    searchState = searchState,
                    isSearchActivated = isSearchFocused,
                    focusRequester = focusRequester,
                    tracker = tracker,
                    onEmptySearchQuery = {
                        coroutineScope.launch {
                            listLoginsState.scrollToItem(0)
                        }
                    }
                )
            }
            LazyColumn(modifier = Modifier.fillMaxSize(), state = listLoginsState) {
                if (allAccounts.isNotEmpty()) {
                    item {
                        TiligSpacerVerticalDefault()
                    }
                    items(items = allAccounts, key = { item ->
                        item.id!!
                    }) { item ->
                        SecretRow(
                            name = item.name,
                            icon = item.getSecretIcon(context = LocalContext.current),
                            withChevron = true,
                            isShared = item.hasSharees(),
                            description = AnnotatedString(item.username?:"")
                        ) {
                            action.invoke(AutofillPopupActions.ChosenAccount(item))
                        }
                    }
                    if (searchState.value.isNotEmpty()) {
                        item {
                            NewAutofillAccountRow(
                                title = searchState.value,
                                backgroundColor = White
                            ) {
                                action.invoke(
                                    AutofillPopupActions.StartAddAccount(
                                        account = Account.createBasic(
                                            name = searchState.value
                                        )
                                    )
                                )
                            }
                        }
                    }
                } else {
                    item {
                        Box(Modifier.padding(vertical = DefaultPadding)) {
                            SearchResultsEmptyScreen(
                                addExtraSpace = false
                            )
                        }
                    }
                }
            }
        }
    } else {
        LazyColumn(modifier = Modifier.fillMaxSize()) {
            autofillMatchedItems(
                matchedAccounts = matchedAccounts,
                accountTitle = title,
                onMatchedAccountClick = { account ->
                    action.invoke(AutofillPopupActions.FillSelected(account))
                    tracker.trackAutofillEvent(prefs, conversions)
                },
                onAddNewAccountClick = {
                    action.invoke(AutofillPopupActions.StartAddAccount(account = prefilledAccount))
                })
            if (totalAccounts > 0) {
                // display if there are accounts or search is unfocused but accounts were filtered
                if (allAccounts.isNotEmpty() || searchState.value.isNotEmpty()) {
                    item(key = "search") {
                        AutofillSearchComponent(
                            searchState = searchState,
                            isSearchActivated = isSearchFocused,
                            focusRequester = focusRequester,
                            tracker = tracker
                        )
                    }
                }
                if (allAccounts.isNotEmpty()) {
                    items(items = allAccounts, key = { item ->
                        item.id!!
                    }) { item ->
                        SecretRow(
                            name = item.overview.name,
                            icon = item.getSecretIcon(context = LocalContext.current),
                            withChevron = true,
                            isShared = item.hasSharees(),
                            description = AnnotatedString(item.username?:"")
                        ) {
                            action.invoke(AutofillPopupActions.ChosenAccount(item))
                        }
                    }
                } else if (searchState.value.isNotEmpty()) {
                    item {
                        Box(Modifier.padding(vertical = DefaultPadding)) {
                            SearchResultsEmptyScreen(
                                addExtraSpace = false
                            )
                        }
                    }
                }
            } else {
                item {
                    AutofillNoAccountsView(modifier = Modifier.fillMaxSize()) {
                        action.invoke(AutofillPopupActions.StartAddAccount(prefilledAccount))
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
private fun AutofillSearchComponent(
    searchState: MutableState<String>,
    isSearchActivated: MutableState<Boolean>,
    focusRequester: FocusRequester,
    tracker: Tracker,
    onEmptySearchQuery: (() -> Unit)? = null
) {
    val keyboardManager = LocalSoftwareKeyboardController.current

    BasicTextField(
        value = searchState.value,
        onValueChange = {
            searchState.value = it
            if (it.isEmpty()) {
                onEmptySearchQuery?.invoke()
            }
        },
        modifier = Modifier
            .padding(start = DefaultPadding, end = DefaultPadding, top = DefaultPadding)
            .fillMaxWidth()
            .height(48.dp)
            .focusRequester(focusRequester)
            .onFocusChanged {
                if (it.isFocused && !isSearchActivated.value) {
                    isSearchActivated.value = it.isFocused
                    tracker.trackEvent(
                        Tracker.EVENT_SEARCH_ITEM_TAPPED,
                        mapOf(Tracker.EVENT_SEARCH_ITEM_TAPPED_SOURCE to Tracker.EVENT_SEARCH_ITEM_TAPPED_IN_AUTOFILL)
                    )
                }
            }
            .border(
                DefaultSmallBorder,
                if (isSearchActivated.value) LightBlue else SearchBarBorder,
                shape = RoundedCornerShape(8.dp)
            )
            .padding(start = 12.dp),
        decorationBox = { innerTextField ->
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_search),
                    contentDescription = "",
                    modifier = Modifier
                        .size(24.dp)
                        .align(Alignment.CenterVertically)
                )
                Box(
                    modifier = Modifier
                        .weight(1f)
                        .fillMaxHeight(),
                    contentAlignment = Alignment.CenterStart
                ) {
                    if (searchState.value.isEmpty()) {
                        Text(
                            text = stringResource(id = R.string.label_search_for_login),
                            style = StyleStolzRegularDarkBlue14.copy(color = DimmedBlue)
                        )
                    }
                    innerTextField()
                }
                if (isSearchActivated.value) {
                    IconButton(onClick = {
                        searchState.value = ""
                        isSearchActivated.value = false
                    }) {
                        Icon(
                            Icons.Default.Close,
                            contentDescription = "",
                            modifier = Modifier
                                .size(24.dp)
                        )
                    }
                }
            }

        },
        textStyle = StyleStolzBookDarkBlue16,
        singleLine = true,
        keyboardOptions = KeyboardOptions(
            imeAction = ImeAction.Done
        ),
        keyboardActions = KeyboardActions(
            onDone = {
                keyboardManager?.hide()
                isSearchActivated.value = false
            }
        )
    )
    LaunchedEffect(Unit) {
        if (isSearchActivated.value) {
            focusRequester.requestFocus()
        }
    }
}

private fun LazyListScope.autofillMatchedItems(
    matchedAccounts: List<Account>,
    accountTitle: String,
    onMatchedAccountClick: (Account) -> Unit,
    onAddNewAccountClick: () -> Unit
) {
    item {
        AppendableTextView(
            appendableTexts = listOf(
                AppendableText(
                    stringResource(id = if (matchedAccounts.isNotEmpty()) R.string.label_autofill_header_your_login_for else R.string.label_autofill_header_no_login_for),
                    false
                ),
                AppendableText(
                    accountTitle,
                    true
                )
            ),
            textAlign = TextAlign.Start,
            textStyle = StyleStolzMediumDarkBlue14,
            spanStyle = SpanStyle(LightBlue),
            modifier = Modifier
                .fillMaxWidth()
                .background(color = Blue4)
                .padding(bottom = 8.dp, top = 20.dp, start = 16.dp, end = 16.dp)
        )
    }
    if (matchedAccounts.isNotEmpty()) {
        items(items = matchedAccounts) { item ->
            MatchedAccountAutofillRow(
                account = item,
                onClick = {
                    onMatchedAccountClick.invoke(item)
                }
            )
        }
    } else {
        item {
            NewAutofillAccountRow(title = accountTitle, onClick = onAddNewAccountClick)
        }
    }

    item {
        Box(
            Modifier
                .height(8.dp)
                .background(Blue4)
        ) {
            Divider(modifier = Modifier.align(Alignment.BottomStart))
        }
    }
}