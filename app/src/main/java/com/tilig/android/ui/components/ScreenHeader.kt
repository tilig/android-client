package com.tilig.android.ui.components

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tilig.android.R
import com.tilig.android.ui.theme.DarkBlue
import com.tilig.android.ui.theme.Stolzl
import com.tilig.android.ui.theme.White

@Composable
fun ScreenHeader(
    modifier: Modifier = Modifier,
    closeSheet: () -> Unit = {},
    rightAction: (() -> Unit)? = null,
    @StringRes headerTextRes: Int?,
    @DrawableRes headerBg: Int? = null,
    @DrawableRes leftActionIconRes: Int? = R.drawable.ic_close,
    @DrawableRes rightActionIconRes: Int? = null,
    tintColor: Color = DarkBlue,
    textColor: Color = White
) {
    Box(
        modifier = modifier
            .fillMaxWidth()
            .height(56.dp)
    ) {
        headerBg?.let {
            Image(
                modifier = modifier,
                painter = painterResource(id = headerBg),
                contentDescription = null,
                contentScale = ContentScale.FillWidth
            )
        }
        leftActionIconRes?.let {
            IconButton(
                modifier = Modifier
                    .align(Alignment.CenterStart),
                onClick = closeSheet
            ) {
                Icon(
                    modifier = Modifier.size(16.dp),
                    painter = painterResource(id = it),
                    tint = tintColor,
                    contentDescription = null
                )
            }
        }
        if (headerTextRes != null) {
            Text(
                modifier = Modifier.align(Alignment.Center),
                text = stringResource(id = headerTextRes),
                color = textColor,
                fontSize = 16.sp,
                fontFamily = Stolzl
            )
        }
        if (rightActionIconRes != null && rightAction != null) {
            IconButton(
                modifier = Modifier
                    .align(Alignment.CenterEnd),
                onClick = rightAction
            ) {
                Icon(
                    modifier = Modifier.size(16.dp),
                    painter = painterResource(id = rightActionIconRes),
                    tint = tintColor,
                    contentDescription = null
                )
            }
        }
    }

    Divider()
}
