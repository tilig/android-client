package com.tilig.android.ui.autofill

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import androidx.activity.result.contract.ActivityResultContract
import androidx.annotation.RequiresApi
import com.tilig.android.BuildConfig

@RequiresApi(Build.VERSION_CODES.O)
class AutofillEnableContract : ActivityResultContract<Void?, Boolean>() {

    override fun createIntent(context: Context, input: Void?): Intent {
        return Intent(Settings.ACTION_REQUEST_SET_AUTOFILL_SERVICE).apply {
            data = Uri.parse("package:${BuildConfig.APPLICATION_ID}")
        }
    }

    override fun parseResult(resultCode: Int, intent: Intent?): Boolean {
        if (resultCode == Activity.RESULT_OK) {
            Log.v("Autofill", "Autofill result: OK")
            return true
        } else {
            Log.v("Autofill", "Autofill result: CANCELED")
        }
        return false
    }
}
