package com.tilig.android.ui.cards.add

import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import com.tilig.android.data.models.tilig.CreditCard
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.Folders
import com.tilig.android.ui.cards.CardFieldsStateHolder
import com.tilig.android.ui.cards.rememberCardFieldsStateHolder
import com.tilig.android.utils.CardUtils
import kotlinx.coroutines.flow.MutableStateFlow

class AddCardStateHolder(
    val scrollableState: ScrollState,
    val selectedFolder: MutableState<Folder?>,
    val cardFieldsStateHolder: CardFieldsStateHolder,
) {

    fun buildCreditCardModel(): CreditCard {
        val creditCard = CreditCard.createEmpty()
        return creditCard.apply {
            name = cardFieldsStateHolder.nameState.text
            cardNumber = cardFieldsStateHolder.cardNumberState.text
            expireDate = CardUtils.addSeparatorIfCanToCardExpireDate(cardFieldsStateHolder.cardExpireState.text)
            holderName = cardFieldsStateHolder.cardHolderState.text
            securityCode = cardFieldsStateHolder.cardSecurityCodeState.text
            pinCode = cardFieldsStateHolder.cardPinState.text
            zipCode = cardFieldsStateHolder.cardZipCodeState.text
            folder = selectedFolder.value
        }
    }

    fun isSaveButtonEnabled(): Boolean = cardFieldsStateHolder.areAllRequiredFieldsSetup()
}

@Composable
fun rememberAddCardStateHolder(
    scrollableState: ScrollState = rememberScrollState(),
    cardFieldsStateHolder: CardFieldsStateHolder = rememberCardFieldsStateHolder(
        cardName = "",
        cardHolder = "",
        cardNumber = "",
        cardSecret = "",
        cardExpire = "",
        cardPin = "",
        cardZipCode = "",
        cardExtraInfo = "",
        cardCustomFields = null
    )
) = remember {
    AddCardStateHolder(
        scrollableState = scrollableState,
        cardFieldsStateHolder = cardFieldsStateHolder,
        selectedFolder = mutableStateOf(null)
    )
}