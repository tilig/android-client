package com.tilig.android.ui.autofill.components

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.tilig.android.R
import com.tilig.android.analytics.ConversionAnalytics
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.ui.components.AccountIcon
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.SharedPrefs
import kotlinx.coroutines.flow.MutableStateFlow
import org.koin.androidx.compose.inject

@Preview
@Composable
fun AutofillPhishingViewPreview() {
    AutofillPhishingView(
        visitingAccount = Account.createEmpty(),
        chosenAccount = Account.createEmpty(),
        onAction = {}
    )
}

@Composable
fun AutofillPhishingView(
    visitingAccount: Account,
    chosenAccount: Account,
    onAction: (AutofillPopupActions) -> Unit
) {

    val tracker: Mixpanel by inject()
    val conversions: ConversionAnalytics by inject()
    val prefs: SharedPrefs by inject()

    ConstraintLayout(
        modifier = Modifier
            .fillMaxSize()
            .padding(bottom = 24.dp)
            .verticalScroll(rememberScrollState())
    ) {
        val (header, content, bottomView) = createRefs()
        TiligButton(
            modifier = Modifier
                .constrainAs(bottomView) {
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                }
                .fillMaxWidth()
                .padding(horizontal = DefaultPadding)
                .height(48.dp),
            iconRes = null,
            text = stringResource(
                R.string.use_password_from,
                chosenAccount.name ?: ""
            ),
            enabled = true,
            isDark = true,
            extraPadding = true,
            isSizeConstrained = false,
            onClick = {
                tracker.trackAutofillEvent(prefs, conversions)
                tracker.trackEvent(Tracker.EVENT_PHISHING_WARNING_ACCEPTED)

                onAction(AutofillPopupActions.FillSelected(chosenAccount))
            })

        Column(modifier = Modifier
            .constrainAs(header) {
                top.linkTo(parent.top)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            }
            .fillMaxWidth()
            .padding(horizontal = DefaultPadding)) {
            Image(
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(top = 20.dp),
                painter = painterResource(id = R.drawable.img_choose_account),
                contentDescription = "",
                contentScale = ContentScale.FillWidth
            )
            Text(
                text = stringResource(id = R.string.are_you_sure),
                modifier = Modifier.align(Alignment.CenterHorizontally),
                style = StyleSpoofMediumBlue24
            )
            Spacer(modifier = Modifier.height(HalfPadding))
            Text(
                text = stringResource(id = R.string.autofill_login_with_another_cred),
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .fillMaxWidth(),
                textAlign = TextAlign.Center,
                style = StyleStolzBookDarkBlue12
            )
        }

        Column(
            modifier = Modifier
                .constrainAs(content) {
                    top.linkTo(header.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    bottom.linkTo(bottomView.top)
                    height = Dimension.wrapContent
                }
                .padding(
                    start = DefaultPadding,
                    end = DefaultPadding,
                    top = DefaultPadding,
                    bottom = 24.dp
                )
        ) {
            Text(
                text = stringResource(id = R.string.you_currently_visiting),
                style = StyleStolzRegularDarkBlue12
            )
            AccountBorderedContainer(
                bgColor = Blue3,
                borderColor = Blue2,
                account = visitingAccount
            )
            Spacer(modifier = Modifier.height(24.dp))
            Text(
                text = stringResource(id = R.string.you_entering_password_from),
                style = StyleStolzRegularDarkBlue12
            )
            AccountBorderedContainer(
                bgColor = Green2,
                borderColor = Green,
                account = chosenAccount
            )
        }
    }
}

@Composable
private fun AccountBorderedContainer(
    bgColor: Color,
    borderColor: Color,
    account: Account,
) {
    Row(
        modifier = Modifier.height(56.dp)
    ) {

        val shape = RoundedCornerShape(8.dp)
        Row(
            modifier = Modifier
                .border(1.dp, color = borderColor, shape = shape)
                .background(
                    bgColor,
                    shape = shape
                )
                .fillMaxHeight()
        ) {
            Text(
                text = account.getHostOrAppName(LocalContext.current),
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(3f)
                    .padding(start = DefaultPadding)
                    .align(Alignment.CenterVertically),
                style = StyleStolzRegularDarkBlue16
            )
            AccountIcon(
                modifier = Modifier
                    .align(Alignment.CenterVertically),
                account = account,
                shouldUpdateIcon = MutableStateFlow(false)
            )
        }
    }
}

