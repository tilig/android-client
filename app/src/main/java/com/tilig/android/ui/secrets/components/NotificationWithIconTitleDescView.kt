package com.tilig.android.ui.secrets.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.tilig.android.ui.theme.DarkGrey
import com.tilig.android.ui.theme.DefaultTextColor
import com.tilig.android.ui.theme.FontSizeLarge
import com.tilig.android.ui.theme.Typography

@Composable
fun NotificationWithIconTitleDescView(
    painter: Painter,
    title: String,
    description: String?,
    titleStyle: TextStyle = Typography.h3.copy(color = DefaultTextColor, fontSize = FontSizeLarge)
) {
    Image(
        modifier = Modifier,
        painter = painter,
        contentDescription = "",
        contentScale = ContentScale.FillWidth,
    )
    Text(
        modifier = Modifier.padding(vertical = 8.dp),
        text = title,
        style = titleStyle
    )
    if (description != null) {
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 32.dp),
            text = description,
            textAlign = TextAlign.Center,
            style = Typography.body1,
            color = DarkGrey
        )
    }
}