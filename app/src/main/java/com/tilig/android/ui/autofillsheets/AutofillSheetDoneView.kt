package com.tilig.android.ui.autofillsheets

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.tilig.android.ui.autofill.components.instructions.AutofillSetupDoneContentComponent

@RequiresApi(Build.VERSION_CODES.O)
@ExperimentalMaterialApi
@Composable
fun AutofillSheetDoneView(
    onClose: () -> Unit
) {
    AutofillSetupDoneContentComponent {
        onClose()
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@ExperimentalMaterialApi
@Composable
@Preview(showBackground = true)
fun AutofillSheetDonePreview() {
    AutofillSheetDoneView(
        onClose = {}
    )
}
