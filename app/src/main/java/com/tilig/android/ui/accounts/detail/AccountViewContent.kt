package com.tilig.android.ui.accounts.detail

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.UiError
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.Membership
import com.tilig.android.ui.accounts.detail.components.*
import com.tilig.android.ui.accounts.detail.events.AccountActionEvent
import com.tilig.android.ui.components.TiligSpacerVerticalDefault
import com.tilig.android.ui.secrets.components.*
import com.tilig.android.ui.secrets.details.BottomSheetType
import com.tilig.android.ui.secrets.details.ItemDetailsCallback
import com.tilig.android.ui.secrets.details.events.ItemDetailsActionEvent
import com.tilig.android.ui.theme.*
import com.tilig.android.ui.twofa.TwoFactorAuthEnabled
import com.tilig.android.utils.VersionUtil
import com.tilig.android.utils.copyToClipboard
import com.tilig.android.utils.isValidEmail

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun AccountViewContent(
    account: Account,
    folder: Folder?,
    isLoading: Boolean,
    shareErrorMessage: UiError? = null,
    tracker: Tracker,
    callBack: ItemDetailsCallback
) {
    val context = LocalContext.current

    val accountUserNameEmail = remember { account.username ?: "" }
    // can be updated after password history restore
    val accountPassword = account.password
    val accountWebsite = remember { account.website }
    val accountNotes = remember { account.notes }

    val otpState = remember { account.otp }
    val accountAppId = remember { account.androidAppId }
    val qrCodeValue = remember { mutableStateOf<String?>(null) }

    val emailCopiedMessage = stringResource(id = R.string.copied_email)
    val usernameCopiedMessage = stringResource(id = R.string.copied_username)
    val passwordCopiedMessage = stringResource(id = R.string.copied_password)
    val websiteCopiedMessage = stringResource(id = R.string.copied_website)
    val tfaCopiedMessage = stringResource(id = R.string.copied_tfa)
    val extraInfoCopiedMessage = stringResource(id = R.string.copied_extra_info)
    val linkCopiedMessage = stringResource(id = R.string.notice_copied_to_clipboard)

    val copyAction: (String?, String) -> Unit = { message, copyMessage ->
        if (message?.copyToClipboard(context) == true
            && !VersionUtil.showsSystemMessageOnCopyToClipboard()
        ) {
            callBack.onShowSnackbarMessage(copyMessage)
        }
    }

    val showDeleteLinkConfirmDialog = rememberSaveable { mutableStateOf(false) }

    if (showDeleteLinkConfirmDialog.value) {
        ShareLinkDeleteDialog(
            onCancel = {
                showDeleteLinkConfirmDialog.value = false
            },
            onConfirm = {
                showDeleteLinkConfirmDialog.value = false
                callBack.onActionEvent(AccountActionEvent.DeleteShareLink)
            }
        )
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(DefaultPadding)
    ) {
        Box(
            modifier = Modifier.background(
                color = White,
                shape = RoundedCornerShape(MediumCornerRadius)
            )
        ) {
            Column(
                modifier = Modifier.padding(
                    start = DefaultPadding,
                    end = DefaultPadding,
                    top = DefaultPadding,
                    bottom = HalfPadding
                )
            ) {

                UserNameEmailView(
                    userNameEmail = accountUserNameEmail,
                    tracker = tracker,
                    onCopyClick = {
                        copyAction(
                            accountUserNameEmail,
                            if (accountUserNameEmail.isValidEmail()) emailCopiedMessage else usernameCopiedMessage
                        )
                    }
                )

                Divider(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = DefaultPadding)
                )

                TitledContent(
                    modifier = Modifier,
                    tracker = tracker,
                    title = stringResource(id = R.string.label_login_password),
                    event = Tracker.EVENT_PASSWORD_COPY,
                    onClick = { copyAction(accountPassword, passwordCopiedMessage) },
                ) {
                    DetailsPasswordView(
                        tracker = tracker,
                        password = accountPassword
                    )
                }
            }
        }

        TiligSpacerVerticalDefault()
        WebView(
            websiteUrl = accountWebsite,
            androidAppId = accountAppId,
            tracker = tracker,
            onCopyClick = {
                copyAction(
                    accountWebsite,
                    websiteCopiedMessage
                )
            }
        )

        if (account.hasOtp()) {
            TiligSpacerVerticalDefault()
            BoxWithTitledContent(
                modifier = Modifier.fillMaxWidth(),
                titleStr = stringResource(id = R.string.tfa_acc_details),
                tracker = tracker,
                event = Tracker.EVENT_QR_CODE_COPY,
                onClick = {
                    qrCodeValue.value?.let {
                        copyAction(it, tfaCopiedMessage)
                    }
                },
            ) {
                TwoFactorAuthEnabled(
                    modifier = Modifier
                        .fillMaxSize(),
                    otp = otpState,
                    onNewCodeGenerated = {
                        qrCodeValue.value = it
                    })
            }
        }

        if (accountNotes.isNotEmpty()) {
            TiligSpacerVerticalDefault()
            ExtraInfoView(notes = accountNotes,
                tracker = tracker,
                onCopyClick = {
                    copyAction(accountNotes, extraInfoCopiedMessage)
                }
            )
        }

        AllCustomFieldsViewComponent(
            fields = account.details.customFields
        ) {
            callBack.onShowSnackbarMessage(it)
        }

        if (account.isTestDriveItem()) {
            Spacer(modifier = Modifier.height(56.dp))
            TextButton(
                modifier = Modifier.align(Alignment.CenterHorizontally),
                onClick = { callBack.onActionEvent(ItemDetailsActionEvent.DeleteItem) },
            ) {
                Text(
                    stringResource(id = R.string.bt_delete_account),
                    color = LightBlue,
                    style = StyleStolzRegularDarkBlue16
                )
            }
        } else {
            TiligSpacerVerticalDefault()
            ShareLinkComponent(
                item = account,
                folder = folder,
                isActionEnabled = !isLoading,
                shareErrorMessage = shareErrorMessage,
                callback = object : ShareLinkComponentCallback {
                    override fun onCreateShareLink() {
                        callBack.onActionEvent(AccountActionEvent.CreateShareLink)
                    }

                    override fun onDeleteShareUrl() {
                        showDeleteLinkConfirmDialog.value = true
                    }

                    override fun onLinkCopied() {
                        if (!VersionUtil.showsSystemMessageOnCopyToClipboard()) {
                            callBack.onShowSnackbarMessage(linkCopiedMessage)
                        }
                    }

                    override fun onRevokeAccess(member: Membership) {
                        callBack.onActionEvent(
                            ItemDetailsActionEvent.ShareRevoke(
                                folderId = folder!!.id,
                                memberId = member.id
                            )
                        )
                    }

                    override fun onOpenShareScreen() {
                        callBack.onUpdateBottomSheet(BottomSheetType.ShareWith(account))
                    }

                })

            if (account.details.getSortedHistory().isNotEmpty()) {
                TiligSpacerVerticalDefault()
                TextButton(modifier = Modifier
                    .fillMaxWidth()
                    .background(color = White, shape = RoundedCornerShape(MediumCornerRadius))
                    .height(DefaultButtonHeight),
                    elevation = ButtonDefaults.elevation(
                        defaultElevation = 0.dp,
                        focusedElevation = 0.dp,
                        hoveredElevation = 0.dp
                    ),
                    shape = RoundedCornerShape(8.dp),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = White,
                        disabledBackgroundColor = Grey,
                    ),
                    onClick = {
                        callBack.onUpdateBottomSheet(
                            BottomSheetType.PasswordHistory(
                                account.details.getSortedHistory()
                            )
                        )
                    }) {
                    Text(
                        stringResource(id = R.string.view_password_history),
                        style = StyleStolzRegularDarkBlue16,
                        color = LightBlue
                    )
                }
            }
        }
    }
}