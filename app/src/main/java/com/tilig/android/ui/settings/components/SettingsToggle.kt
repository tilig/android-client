package com.tilig.android.ui.settings.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.theme.*


@Composable
fun SettingsToggle(
    text: String,
    checked: Boolean,
    checkedState: MutableState<Boolean> = remember { mutableStateOf(checked) },
    enabled: Boolean = true,
    modifier: Modifier = Modifier,
    /**
     * Callback called when the switch is flipped to a new state.
     * @param the new state
     * @return if the new state is accepted: true if the switch may flip, false if the switch should stay in its previous position (i.e. !it)
     */
    onCheckedChanged: (Boolean) -> Boolean,
    onInfoTapped: (() -> Unit)? = null
) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.Top
    ) {
        Row(modifier = Modifier
            .weight(1f)
            .align(Alignment.CenterVertically)) {
            Text(
                modifier = Modifier
                    // Note: a padding of 2 makes a single-line checkbox appear to be centered vertically.
                    // We *could* also use 0 padding and simply align vertically, but that doesn't work for
                    // multiline texts
                    .padding(end = HalfPadding, top = 2.dp)
                    .wrapContentHeight()
                    .clickable {
                        if (enabled) {
                            checkedState.value = !checkedState.value
                            onCheckedChanged(checkedState.value)
                        }
                    },
                text = text,
                style = StyleStolzRegularDarkBlue14.copy(
                    color = when {
                        !enabled -> DisabledTextColor
                        checkedState.value -> LightBlue
                        else -> DarkBlue
                    }
                ),
                overflow = TextOverflow.Visible,
                maxLines = 2
            )

            if (onInfoTapped != null) {
                IconButton(
                    modifier = Modifier
                        .width(24.dp)
                        .height(24.dp)
                        .padding(top = 2.dp),
                    onClick = {
                        onInfoTapped.invoke()
                    }
                ) {
                    Icon(
                        imageVector = Icons.Default.Info,
                        contentDescription = stringResource(R.string.more_info_biometrics),
                        tint = LightBlue
                    )
                }
            }
        }

        Switch(
            checked = checkedState.value,
            colors = SwitchDefaults.colors(
                checkedThumbColor = LightBlue,
                checkedTrackColor = DimmedLightBlue
            ),
            onCheckedChange = {
                if (onCheckedChanged(it)) {
                    checkedState.value = it
                }
            }
        )
    }
}

@Preview(showBackground = true)
@Composable
fun SettingsTogglePreview() {
    Column {
        SettingsToggle(
            text = "Lorem Ipsum",
            checked = true,
            onInfoTapped = {},
            onCheckedChanged = { true })
        SettingsToggle(
            text = "Lorem Ipsum",
            checked = false,
            onCheckedChanged = { true })
        SettingsToggle(
            text = "Lorem ipsum dolor sit amet, consectetur adipiscin elit",
            checked = true,
            onInfoTapped = {},
            onCheckedChanged = { true })
    }
}
