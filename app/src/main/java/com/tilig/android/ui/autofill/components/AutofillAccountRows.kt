package com.tilig.android.ui.autofill.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tilig.android.R
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.ui.components.AccountIcon
import com.tilig.android.ui.components.TiligImage
import com.tilig.android.ui.theme.*
import kotlinx.coroutines.flow.MutableStateFlow

val BaseAutofillRowHeight = 56.dp

@Composable
fun MatchedAccountAutofillRow(
    account: Account,
    onClick: () -> Unit,
) {
    BaseAutofillRow(
        icon = {
            AccountIcon(
                account = account,
                modifier = Modifier.padding(0.dp),
                shouldUpdateIcon = MutableStateFlow(false)
            )
        },
        title = {
            Text(
                text = account.name ?: stringResource(id = R.string.untitled_account),
                style = Typography.h3,
                fontSize = 14.sp
            )
        },
        subtitle = {
            Text(
                text = account.username ?: "",
                style = Typography.subtitle2,
                color = DisabledGrey,
                fontSize = 12.sp
            )
        },
        showAutofillBtn = true,
        onClick = onClick
    )
}

@Composable
fun NewAutofillAccountRow(
    title: String,
    backgroundColor: Color = Blue4,
    onClick: () -> Unit
) {
    BaseAutofillRow(
        icon = {
            TiligImage(
                modifier = Modifier
                    .padding(end = 16.dp)
                    .size(24.dp)
                    .clip(DefaultAccountClipShape),
                image = R.drawable.ic_add_account,
                contentDescription = null,
            )
        },
        title = {
            Text(
                modifier = Modifier,
                text = stringResource(id = R.string.label_add_a_new_login_for),
                style = StyleStolzRegularDarkBlue14.copy(color = DimmedBlue, fontSize = 10.sp),
            )
        },
        subtitle = {
            Text(
                modifier = Modifier,
                text = title,
                style = StyleStolzRegularDarkBlue14,
            )
        },
        backgroundColor = backgroundColor,
        showAutofillBtn = false,
        onClick = onClick,
    )
}

@Composable
private fun BaseAutofillRow(
    icon: @Composable () -> Unit,
    title: @Composable () -> Unit,
    subtitle: @Composable () -> Unit,
    backgroundColor: Color = Blue4,
    showAutofillBtn: Boolean,
    onClick: () -> Unit,
) {
    Row(
        Modifier
            .fillMaxWidth()
            .height(BaseAutofillRowHeight)
            .background(color = backgroundColor)
            .padding(horizontal = DefaultPadding)
            .clickable(onClick = onClick),
        verticalAlignment = Alignment.CenterVertically
    ) {
        icon()
        Column(Modifier.weight(1f)) {
            title()
            subtitle()
        }
        if (showAutofillBtn) {
            Button(
                modifier = Modifier
                    .padding(start = DefaultPadding)
                    .height(32.dp),
                shape = RoundedCornerShape(6.dp),
                border = BorderStroke(1.dp, color = LightBlue),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = White,
                    disabledBackgroundColor = Grey,
                ),
                contentPadding = PaddingValues(horizontal = 8.dp, vertical = 4.dp),
                elevation = null,
                onClick = onClick,
            ) {
                Icon(
                    modifier = Modifier.size(12.dp),
                    painter = painterResource(id = R.drawable.ic_magic), contentDescription = null,
                    tint = LightBlue
                )
                Text(
                    modifier = Modifier
                        .padding(start = 4.dp)
                        .align(Alignment.CenterVertically),
                    text = stringResource(id = R.string.autofill), color = LightBlue,
                    style = StyleStolzRegularDarkBlue14
                )
            }
        }
    }
}