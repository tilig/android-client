package com.tilig.android.ui.onboarding.survey

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ChainStyle
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import androidx.navigation.findNavController
import com.tilig.android.R
import com.tilig.android.ui.components.StatusBarAndNavigationBarContrast
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.catchNavigate

class SurveyIntroFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        return ComposeView(requireContext()).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            setContent {
                SurveyIntroScreen(findNavController())
            }
        }
    }
}

@Composable
private fun SurveyIntroScreen(
    navController: NavController,
    viewModel: SurveyViewModel = viewModel()
) {
    val goNextScreen = viewModel.finishSurvey.collectAsState()
    if (goNextScreen.value) {
        navController.catchNavigate(SurveyIntroFragmentDirections.actionSurveyIntroFragmentToHomeFragment())
    }
    val letsGoBtnEnabled = remember { mutableStateOf(true) }

    Surface(
        modifier = Modifier
            .fillMaxSize()
            .navigationBarsPadding()
            .statusBarsPadding(),
        color = MaterialTheme.colors.background
    ) {
        StatusBarAndNavigationBarContrast(
            statusBarBright = true,
            navigationBarBright = true,
            navigationBarScrollBehind = false
        )
        ConstraintLayout(
            modifier = Modifier.fillMaxSize()
        ) {
            val (btSkip, title, desc, image, btnGo) = createRefs()
            TextButton(modifier = Modifier
                .constrainAs(btSkip) {
                    end.linkTo(parent.end)
                    top.linkTo(parent.top)
                }
                .padding(
                    start = DefaultPadding,
                    top = DefaultPadding,
                    bottom = DefaultPadding,
                    end = 0.dp
                ), onClick = {
                viewModel.skipSurvey()
                letsGoBtnEnabled.value = false
            }
            ) {
                Text(
                    stringResource(id = R.string.skip),
                    style = Typography.button,
                    fontSize = 14.sp,
                    color = DisabledGrey
                )
            }
            TiligButton(
                modifier = Modifier
                    .constrainAs(btnGo) {
                        end.linkTo(parent.end)
                        start.linkTo(parent.start)
                        bottom.linkTo(parent.bottom)
                    },
                iconRes = null,
                text = stringResource(R.string.lets_go),
                enabled = letsGoBtnEnabled.value,
                isDark = true,
                isSizeConstrained = true,
                onClick = {
                    navController.catchNavigate(SurveyIntroFragmentDirections.actionSurveyIntroFragmentToSurveyFragment())
                })
            Image(
                painter = painterResource(id = R.drawable.img_servey),
                contentDescription = "",
                contentScale = ContentScale.FillBounds,
                modifier = Modifier
                    .constrainAs(image) {
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        top.linkTo(parent.top)
                        bottom.linkTo(title.top)
                    }
                    .padding(DoublePadding)
            )

            Text(
                modifier = Modifier
                    .constrainAs(title) {
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        top.linkTo(image.bottom)
                        bottom.linkTo(desc.top)
                    }
                    .padding(DefaultPadding)
                    .fillMaxWidth(),
                textAlign = TextAlign.Center,
                text = stringResource(id = R.string.survey_intro_title),
                style = Typography.h1.copy(
                    color = DarkBlue,
                    fontSize = 28.sp,
                    lineHeight = 32.sp
                ),
            )
            Text(
                modifier = Modifier
                    .constrainAs(desc) {
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        top.linkTo(title.bottom)
                        bottom.linkTo(btnGo.top)
                    }
                    .padding(horizontal = MediumPadding),
                text = stringResource(id = R.string.survey_intro_desc),
                style = Typography.body1.copy(
                    color = DarkBlue,
                    fontSize = 16.sp,
                ),
                textAlign = TextAlign.Center,
            )

            createVerticalChain(
                image, title, desc,
                chainStyle = ChainStyle.Packed
            )
        }
    }
}

@Composable
@Preview
fun SurveyIntroScreenPreview() {
    SurveyIntroScreen(viewModel = viewModel(), navController = rememberNavController())
}
