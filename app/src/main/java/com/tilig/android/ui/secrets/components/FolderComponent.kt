package com.tilig.android.ui.secrets.components

import androidx.annotation.StringRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.google.accompanist.flowlayout.FlowCrossAxisAlignment
import com.google.accompanist.flowlayout.FlowRow
import com.tilig.android.R
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.ui.theme.*

@Composable
fun FolderComponent(
    modifier: Modifier,
    folder: Folder,
    @StringRes itemTypeMarkerRes: Int
) {
    BoxWithTitledContent(
        modifier = modifier.fillMaxWidth(),
        titleStr = stringResource(id = R.string.label_folder)
    ) {
        FlowRow(crossAxisAlignment = FlowCrossAxisAlignment.Center) {
            Text(
                text = stringResource(
                    id = R.string.item_part_of,
                    stringResource(id = itemTypeMarkerRes)
                ), style = StyleStolzRegularBookDarkBlue13,
                color = Neutral40
            )
            Row(
                modifier = Modifier
                    .padding(start = 4.dp)
                    .background(color = Blue3, shape = Shapes.medium)
                    .padding(4.dp), verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(
                    modifier = Modifier.size(16.dp),
                    painter = painterResource(id = R.drawable.ic_folder),
                    contentDescription = null,
                    tint = LightBlue
                )
                Text(
                    modifier = Modifier
                        .padding(start = 4.dp),
                    text = folder.name ?: "",
                    style = StyleStolzRegularDarkBlue12.copy(color = LightBlue)
                )
            }

            Text(
                text = if (folder.getAllMemberships().size > 1) {
                    stringResource(
                        id = R.string.folder_access_members,
                        folder.getAllMemberships().size
                    )
                } else {
                    stringResource(id = R.string.folder_access_private)
                },
                style = StyleStolzRegularBookDarkBlue13,
                color = Neutral40
            )
        }
    }
}