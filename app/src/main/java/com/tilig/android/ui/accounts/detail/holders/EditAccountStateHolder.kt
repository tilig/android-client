package com.tilig.android.ui.accounts.detail.holders

import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import com.tilig.android.data.models.tilig.CustomField
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.Folders
import com.tilig.android.ui.accounts.detail.AccountEditWrapper
import com.tilig.android.ui.accounts.detail.components.states.PasswordState
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.accounts.detail.components.states.UsernameEmailState
import com.tilig.android.ui.secrets.CustomFieldStateWrapper
import com.tilig.android.utils.isValidUrl

// This is to preserve the state of Compose so that it is not bound to any other lifecycle.
data class EditAccountStateHolder(
    val foldersList: Folders?,
    val nameState: TextFieldState,
    val passwordState: PasswordState,
    val notesState: TextFieldState,
    val websiteState: TextFieldState,
    val userNameEmailState: UsernameEmailState,
    val qrCodeValue: MutableState<String?>,
    val customFields: List<CustomFieldStateWrapper>,
    val selectedFolder: Folder?
)

@Composable
fun rememberEditAccountStateHolder(
    foldersList: Folders?,
    accountEditWrapper: AccountEditWrapper
) =
    EditAccountStateHolder(
        foldersList = foldersList,
        nameState = remember {
            TextFieldState(
                accountEditWrapper.name,
                validator = { it.isNotEmpty() })
        },
        // can be forced updated (restore from password history)
        passwordState = PasswordState(accountEditWrapper.password),
        notesState = remember {
            TextFieldState(
                accountEditWrapper.extraInfo,
                isGrowingMultiline = true
            )
        },
        websiteState = remember {
            TextFieldState(
                accountEditWrapper.website,
                validator = { it.isEmpty() || it.isValidUrl() })
        },
        userNameEmailState = remember { UsernameEmailState(accountEditWrapper.username) },
        qrCodeValue = remember { mutableStateOf(null) },
        customFields =
        accountEditWrapper.customFields?.map {
            CustomFieldStateWrapper(it)
        } ?: emptyList(),
        selectedFolder = accountEditWrapper.selectedFolder
    )
