package com.tilig.android.ui.secrets.components

import androidx.annotation.StringRes
import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.Velocity
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.lerp
import com.tilig.android.ui.secrets.details.SwipingStates
import com.tilig.android.ui.theme.AppBarCollapsedTotalHeight
import com.tilig.android.ui.theme.DefaultPadding
import kotlinx.coroutines.flow.MutableStateFlow

interface SecretItemCollapsableContainerCallBack {
    fun onBackClicked()
    fun onActionBtnClicked()
    fun onDebugBtnClicked()
}

@OptIn(ExperimentalFoundationApi::class)
@ExperimentalMaterialApi
@Composable
fun SecretItemCollapsableContainer(
    swipingState: SwipeableState<SwipingStates>,
    expandedHeight: Dp,
    addExtraHeightShift: Boolean,
    accountIcon: Any? = null,
    backgroundColor: Color?,
    itemName: String?,
    isLoading: Boolean,
    isActionBtnAvailable: Boolean,
    @StringRes actionBtnTitleRes: Int,
    shouldUpdateIcon: MutableStateFlow<Boolean> = MutableStateFlow(false),
    callBack: SecretItemCollapsableContainerCallBack,
    content: @Composable () -> Unit
) {
    val scrollableState = rememberScrollState()

    BoxWithConstraints(modifier = Modifier.fillMaxSize()) {

        val heightInPx = with(LocalDensity.current) { maxHeight.toPx() } // Get height of screen
        val heightConsideredAsExpanded = remember {
            heightInPx / 4
        }
        val connection = remember {
            object : NestedScrollConnection {

                override fun onPreScroll(
                    available: Offset,
                    source: NestedScrollSource
                ): Offset {
                    val delta = available.y
                    return if (delta < 0) {
                        swipingState.performDrag(delta).toOffset()
                    } else {
                        Offset.Zero
                    }
                }

                override fun onPostScroll(
                    consumed: Offset,
                    available: Offset,
                    source: NestedScrollSource
                ): Offset {
                    val delta = available.y
                    return swipingState.performDrag(delta).toOffset()
                }

                override suspend fun onPostFling(
                    consumed: Velocity,
                    available: Velocity
                ): Velocity {
                    swipingState.performFling(velocity = available.y)
                    return super.onPostFling(consumed, available)
                }

                private fun Float.toOffset() = Offset(0f, this)
            }
        }
        CompositionLocalProvider(
            LocalOverscrollConfiguration provides null
        ) {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .swipeable(
                        state = swipingState,
                        thresholds = { _, _ -> FractionalThreshold(0.5f) },
                        orientation = Orientation.Vertical,
                        anchors = mapOf(
                            // Maps anchor points (in px) to states
                            0f to SwipingStates.COLLAPSED,
                            heightConsideredAsExpanded to SwipingStates.EXPANDED,
                        )
                    )
                    .nestedScroll(connection)
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    val progress =
                        if (swipingState.progress.to == SwipingStates.EXPANDED) swipingState.progress.fraction else 1f - swipingState.progress.fraction
                    CollapsableToolbar(
                        progress = progress,
                        expandedHeight = expandedHeight,
                        title = itemName ?: "",
                        isLoading = isLoading,
                        accountIcon = accountIcon,
                        backgroundColor = backgroundColor,
                        actionBtnTitleRes = actionBtnTitleRes,
                        isActionBtnAvailable = isActionBtnAvailable,
                        shouldUpdateIcon = shouldUpdateIcon,
                        callBack = callBack
                    )
                    val dynamicSize = lerp(
                        AppBarCollapsedTotalHeight,
                        expandedHeight - if (addExtraHeightShift) DefaultPadding * 4 else 0.dp,
                        progress
                    )
                    Box(
                        modifier = Modifier
                            .padding(top = dynamicSize)
                            .verticalScroll(scrollableState)
                            .background(color = Color.Transparent)
                    ) {
                        content()
                    }
                }
            }
        }
    }
}