package com.tilig.android.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.tooling.preview.Preview
import com.tilig.android.ui.theme.*

@Composable
fun TiligCheckBox(
    modifier: Modifier,
    checked: Boolean,
    onCheckedChange: ((Boolean) -> Unit)?,
    enabled: Boolean = true,
) {
    Box(
        modifier = modifier
            .clip(Shapes.small)
            .clickable {
                if (onCheckedChange != null && enabled) {
                    run { onCheckedChange(!checked) }
                }
            }
            .border(
                width = DefaultSmallBorder,
                shape = Shapes.small,
                color = if (checked) LightBlue else Grey
            )
            .background(color = if (checked) LightBlue else White, shape = Shapes.small)
    ) {
            Icon(
                imageVector = Icons.Default.Check,
                contentDescription = null,
                tint = White
            )
    }
}

@Preview
@Composable
fun TiligCheckBoxPreview() {
    TiligCheckBox(modifier = Modifier, checked = true, onCheckedChange = {})
}
