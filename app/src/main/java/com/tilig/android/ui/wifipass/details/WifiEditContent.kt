package com.tilig.android.ui.wifipass.details

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.data.models.tilig.CustomField
import com.tilig.android.ui.components.*
import com.tilig.android.ui.secrets.add.AddToFolderComponent
import com.tilig.android.ui.secrets.components.*
import com.tilig.android.ui.secrets.details.ItemDetailsCallback
import com.tilig.android.ui.secrets.details.events.ItemDetailsActionEvent
import com.tilig.android.ui.secrets.details.events.BaseEditEvent
import com.tilig.android.ui.theme.*
import com.tilig.android.ui.wifipass.details.holders.EditWifiStateHolder

@Composable
fun WifiEditContent(
    displayUnsavedAlert: Boolean,
    state: EditWifiStateHolder,
    callback: ItemDetailsCallback
) {

    val openDialog = remember { mutableStateOf(false) }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = DefaultPadding)
    ) {

        WifiPassFieldsContent(stateHolder = state, callback = callback, supportCustomFields = true)

        Spacer(modifier = Modifier.height(56.dp))
        DeleteSecretItemView(
            titleId = R.string.bt_delete_wifi,
            dialogDescriptionId = R.string.dialog_delete_item,
            openDialog = openDialog
        ) {
            callback.onActionEvent(ItemDetailsActionEvent.DeleteItem)
        }
    }

    if (displayUnsavedAlert) {
        TiligDialog(
            title = R.string.alert_unsaved_changes,
            text = R.string.alert_unsaved_changes_desc,
            onCancel = {
                callback.onActionEvent(ItemDetailsActionEvent.CancelChangesDialog)
            },
            onDismissRequest = {},
            confirmButtonText = R.string.alert_unsaved_changes_discard,
            cancelButtonText = R.string.alert_unsaved_changes_confirm,
            onConfirm = { callback.onActionEvent(ItemDetailsActionEvent.ConfirmDiscardChanges) })
    }

    BackHandler {
        callback.onActionEvent(ItemDetailsActionEvent.OnGoBack)
    }
}

@Composable
private fun ColumnScope.WifiPassFieldsContent(
    stateHolder: EditWifiStateHolder,
    showWifiIcon: Boolean = false,
    supportCustomFields: Boolean = false,
    callback: ItemDetailsCallback,
) {
    val focusManager = LocalFocusManager.current
    val focusRequester = remember { FocusRequester() }

    if (showWifiIcon) {
        AddSecretItemNameAndLogoView(
            icon = painterResource(id = R.drawable.ic_wifi),
            hintInputRes = R.string.hint_wifi_title,
            focusManager = focusManager,
            focusRequester = focusRequester,
            nameState = stateHolder.nameState)
    } else {
        TiligSpacerVerticalMedium()
        Box {
            BorderedBasicTextField(
                modifier = Modifier.padding(top = if (stateHolder.nameState.isValid) DefaultErrorMessageHeight else 0.dp),
                errorHandler = ErrorHandler(
                    errorMessage = stringResource(id = R.string.error_name),
                    errorColor = DefaultErrorColor
                ),
                singleLine = false,
                hint = stringResource(id = R.string.hint_wifi_title),
                state = stateHolder.nameState,
                textModifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 12.dp, vertical = 4.dp),
                keyboardType = KeyboardType.Text,
                capitalization = KeyboardCapitalization.Sentences,
                onNext = {
                    focusManager.moveFocus(FocusDirection.Down)
                },
                onValueChanged = {
                    callback.onActionEvent(ItemDetailsActionEvent.Edit(WifiEditEvents.NameChanged(stateHolder.nameState.text)))
                }
            )
            FieldTitle(
                title = stringResource(id = R.string.label_wifi_title),
                modifier = Modifier
            )
        }
    }

    TiligSpacerVerticalMedium()
    TitledContent(
        modifier = Modifier,
        title = stringResource(id = R.string.label_wifi_name_network)
    ) {
        BorderedBasicTextField(
            modifier = Modifier,
            hint = stringResource(id = R.string.hint_wifi_name_network),
            state = stateHolder.networkNameState,
            singleLine = false,
            showRevealButton = stateHolder.networkNameState.isFocused,
            onNext = {
                focusManager.moveFocus(FocusDirection.Next)
            },
            onValueChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(WifiEditEvents.NetworkNameChanged(stateHolder.networkNameState.text)))
            }
        )
    }

    TiligSpacerVerticalMedium()
    TitledContent(
        modifier = Modifier,
        title = stringResource(id = R.string.label_wifi_password)
    ) {
        BorderedBasicTextField(
            modifier = Modifier,
            hint = stringResource(id = R.string.hint_wifi_password),
            state = stateHolder.passwordState,
            singleLine = false,
            hideVisualTransformation = PasswordVisualTransformation(),
            showRevealButton = stateHolder.passwordState.isFocused,
            onNext = {
                focusManager.moveFocus(FocusDirection.Next)
            },
            onValueChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(WifiEditEvents.PasswordChanged(stateHolder.passwordState.text)))
            },
            keyboardType = KeyboardType.Text,
        )
    }

    TiligSpacerVerticalMedium()
    TitledContent(
        modifier = Modifier,
        title = stringResource(id = R.string.label_extra_info)
    ) {
        BorderedBasicTextField(
            modifier = Modifier,
            textStyle = StyleStolzRegularDarkBlue16,
            hint = stringResource(id = R.string.hint_wifi_extra_info),
            state = stateHolder.extraInfoState,
            singleLine = false,
            textModifier = Modifier
                .fillMaxWidth()
                .padding(12.dp),
            keyboardType = KeyboardType.Text,
            capitalization = KeyboardCapitalization.Sentences,
            onValueChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(WifiEditEvents.ExtraInfoChanged(stateHolder.extraInfoState.text)))
            }
        )
    }

    if (supportCustomFields) {
        AllCustomFieldsEditComponent(
            focusManager = focusManager,
            fieldsWrapper = stateHolder.customFields,
            onFieldsChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(BaseEditEvent.CustomFieldsChanged(stateHolder.customFields.map {
                    CustomField(
                        it.labelFieldState.text,
                        it.customField.kind,
                        it.textFieldState.text
                    )
                })))
            }) {
            val updated = stateHolder.customFields.toMutableList()
            updated.remove(it)
            callback.onActionEvent(
                ItemDetailsActionEvent.Edit(
                    BaseEditEvent.CustomFieldsChanged(
                        updated.map {
                            CustomField(
                                it.labelFieldState.text,
                                it.customField.kind,
                                it.textFieldState.text
                            )
                        })
                )
            )
        }

        TiligSpacerVerticalDefault()
        AddCustomField(modifier = Modifier.fillMaxWidth()) {
            callback.onActionEvent(ItemDetailsActionEvent.Edit(BaseEditEvent.AddCustomField(it)))
        }
    }

    TiligSpacerVerticalDefault()
    AddToFolderComponent(
        modifier = Modifier,
        folders = stateHolder.foldersList,
        selectedFolder = stateHolder.selectedFolder,
        onFolderChanged = {
            callback.onActionEvent(ItemDetailsActionEvent.Edit(BaseEditEvent.ChangeFolder(it)))
        }
    )

    BackHandler {
        callback.onActionEvent(ItemDetailsActionEvent.OnGoBack)
    }
}