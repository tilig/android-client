package com.tilig.android.ui.settings

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.tilig.android.R
import com.tilig.android.ui.components.AppendableText
import com.tilig.android.ui.components.InstructionStep
import com.tilig.android.ui.components.StatusBarAndNavigationBarContrast
import com.tilig.android.ui.components.TiligSpacerVerticalMediumLarge
import com.tilig.android.ui.theme.*


private const val TAG_OPEN_WEB_PAGE = "open_web_page"

@Composable
fun ImportPasswordsScreen(navController: NavHostController) {
    StatusBarAndNavigationBarContrast(
        statusBarBright = true,
        navigationBarBright = true,
        navigationBarScrollBehind = true
    )

    Column(
        Modifier
            .fillMaxSize()
            .background(color = BackgroundGrey)
    ) {
        TopAppBar(
            backgroundColor = Color.Transparent,
            modifier = Modifier
                .padding(top = StatusBarHeight),
            elevation = 0.dp,
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight(align = Alignment.CenterVertically)
                    .align(Alignment.CenterVertically)
                    .padding(horizontal = 8.dp)
            ) {
                IconButton(
                    modifier = Modifier
                        .then(Modifier.size(32.dp))
                        .align(Alignment.CenterStart),
                    onClick = { navController.navigateUp() }) {
                    Icon(
                        modifier = Modifier.size(16.dp),
                        painter = painterResource(id = R.drawable.ic_back),
                        contentDescription = null,
                        tint = DarkBlue
                    )
                }
                Text(
                    text = stringResource(id = R.string.import_passwords),
                    style = StyleStolzRegularDarkBlue16,
                    modifier = Modifier
                        .align(Alignment.Center)
                        .fillMaxWidth(),
                    textAlign = TextAlign.Center
                )
            }
        }
        Column(
            Modifier
                .fillMaxSize()
                .padding(horizontal = DefaultPadding)
        ) {
            Spacer(modifier = Modifier.height(MediumPadding))
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(color = Blue4, shape = Shapes.medium)
                    .padding(horizontal = DefaultPadding, vertical = HalfPadding),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = stringResource(id = R.string.use_your_desktop_computer),
                    style = StyleStolzRegularDarkBlue14
                )
                Image(
                    painter = painterResource(id = R.drawable.ic_desktop),
                    contentDescription = stringResource(id = R.string.use_your_desktop_computer)
                )
            }

            InstructionsComponent(modifier = Modifier.padding(top = DefaultPadding))

            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f),
                verticalArrangement = Arrangement.Center
            ) {
                Image(
                    modifier = Modifier.align(Alignment.CenterHorizontally),
                    painter = painterResource(id = R.drawable.img_devices),
                    contentDescription = null
                )
                TiligSpacerVerticalMediumLarge()
                Text(
                    modifier = Modifier.align(Alignment.CenterHorizontally),
                    text = stringResource(id = R.string.available_across_your_devices),
                    style = StyleStolzRegularDarkBlue12.copy(
                        color = Neutral40,
                        textAlign = TextAlign.Center
                    )
                )
            }
        }
    }
}

@Composable
private fun InstructionsComponent(modifier: Modifier) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .background(color = White, shape = Shapes.medium)
            .padding(horizontal = DefaultPadding, vertical = MediumWithQuarter)
    ) {
        InstructionStep(
            index = 1, listOf(
                AppendableText(
                    stringResource(
                        id = R.string.import_instruction_step_1,
                    ),
                    false
                ),
                AppendableText(
                    stringResource(
                        id = R.string.import_instruction_step_1_1,
                    ),
                    true,
                    tag = TAG_OPEN_WEB_PAGE,
                    annotation = TAG_OPEN_WEB_PAGE,
                    addSpace = true
                )
            ),
            tag = TAG_OPEN_WEB_PAGE
        )

        InstructionStep(
            index = 2, listOf(
                AppendableText(
                    stringResource(
                        id = R.string.import_instruction_step_2,
                    ),
                    false
                )
            )
        )

        InstructionStep(
            index = 3, listOf(
                AppendableText(
                    stringResource(
                        id = R.string.import_instruction_step_3,
                    ),
                    false
                )
            )
        )

        InstructionStep(
            index = 4, listOf(
                AppendableText(
                    stringResource(
                        id = R.string.import_instruction_step_4,
                    ),
                    false
                )
            )
        )
    }
}