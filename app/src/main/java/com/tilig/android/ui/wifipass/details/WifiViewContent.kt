package com.tilig.android.ui.wifipass.details

import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.UiError
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.Membership
import com.tilig.android.data.models.tilig.WifiPassword
import com.tilig.android.ui.accounts.detail.components.ShareLinkComponent
import com.tilig.android.ui.accounts.detail.components.ShareLinkComponentCallback
import com.tilig.android.ui.components.TiligSpacerVerticalDefault
import com.tilig.android.ui.secrets.components.AllCustomFieldsViewComponent
import com.tilig.android.ui.secrets.components.BoxWithTitledContent
import com.tilig.android.ui.secrets.components.DetailsTextField
import com.tilig.android.ui.secrets.details.BottomSheetType
import com.tilig.android.ui.secrets.details.ItemDetailsCallback
import com.tilig.android.ui.secrets.details.events.ItemDetailsActionEvent
import com.tilig.android.ui.theme.DefaultPadding
import com.tilig.android.ui.theme.LightBlue
import com.tilig.android.ui.theme.StyleStolzRegularDarkBlue16
import com.tilig.android.utils.VersionUtil
import com.tilig.android.utils.copyToClipboard

@Composable
fun WifiViewContent(
    wifiPassword: WifiPassword,
    folder: Folder?,
    isLoading: Boolean,
    shareErrorMessage: UiError? = null,
    tracker: Tracker,
    callBack: ItemDetailsCallback
) {
    val context = LocalContext.current

    val wifiPasswordCopiedMessage = stringResource(id = R.string.copied_wifi_pass)
    val wifiNetworkNameCopiedMessage = stringResource(id = R.string.copied_wifi_network_name)
    val extraInfoCopiedMessage = stringResource(id = R.string.copied_extra_info)

    val copyAction: (String?, String) -> Unit = { message, copyMessage ->
        if (message?.copyToClipboard(context) == true
            && !VersionUtil.showsSystemMessageOnCopyToClipboard()
        ) {
            callBack.onShowSnackbarMessage(copyMessage)
        }
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(DefaultPadding)
    ) {

        if (!wifiPassword.networkName.isNullOrEmpty()) {
            TiligSpacerVerticalDefault()
            BoxWithTitledContent(
                modifier = Modifier.fillMaxWidth(),
                titleStr = stringResource(id = R.string.label_wifi_name_network),
                tracker = tracker,
                event = Tracker.EVENT_WIFI_NETWORK_NAME_COPIED,
                onClick = { copyAction(wifiPassword.networkName, wifiNetworkNameCopiedMessage) }
            ) {
                DetailsTextField(
                    value = wifiPassword.networkName
                )
            }
        }

        if (!wifiPassword.password.isNullOrEmpty()) {
            TiligSpacerVerticalDefault()
            BoxWithTitledContent(
                modifier = Modifier.fillMaxWidth(),
                titleStr = stringResource(id = R.string.label_wifi_password),
                tracker = tracker,
                event = Tracker.EVENT_WIFI_PASSWORD_COPIED,
                onClick = { copyAction(wifiPassword.password, wifiPasswordCopiedMessage) }
            ) {
                DetailsTextField(
                    value = wifiPassword.password,
                    showRevealButton = true,
                    showContentInitially = false
                )
            }
        }

        if (!wifiPassword.extraInfo.isNullOrEmpty()) {
            TiligSpacerVerticalDefault()
            BoxWithTitledContent(
                modifier = Modifier.fillMaxWidth(),
                titleStr = stringResource(id = R.string.label_extra_info),
                tracker = tracker,
                event = Tracker.EVENT_EXTRA_INFO_COPY,
                onClick = { copyAction(wifiPassword.extraInfo, extraInfoCopiedMessage) }
            ) {
                DetailsTextField(
                    value = wifiPassword.extraInfo,
                    enabled = true
                )
            }
        }

        AllCustomFieldsViewComponent(wifiPassword.details.customFields) {
            callBack.onShowSnackbarMessage(it)
        }

        if (wifiPassword.isTestDriveItem()) {
            Spacer(modifier = Modifier.height(56.dp))
            TextButton(
                modifier = Modifier.align(Alignment.CenterHorizontally),
                onClick = { callBack.onActionEvent(ItemDetailsActionEvent.DeleteItem) },
            ) {
                Text(
                    stringResource(id = R.string.bt_delete_wifi),
                    color = LightBlue,
                    style = StyleStolzRegularDarkBlue16
                )
            }
        } else {
            TiligSpacerVerticalDefault()
            ShareLinkComponent(
                item = wifiPassword,
                folder = folder,
                isActionEnabled = !isLoading,
                shareErrorMessage = shareErrorMessage,
                callback = object : ShareLinkComponentCallback {

                    override fun onRevokeAccess(member: Membership) {
                        callBack.onActionEvent(
                            ItemDetailsActionEvent.ShareRevoke(
                                folderId = folder!!.id,
                                memberId = member.id
                            )
                        )
                    }

                    override fun onOpenShareScreen() {
                        callBack.onUpdateBottomSheet(BottomSheetType.ShareWith(wifiPassword))
                    }
                })
        }
    }
}