package com.tilig.android.ui.onboarding.getstarted

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.tilig.android.BuildConfig
import com.tilig.android.R
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.components.StatusBarAndNavigationBarContrast
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.components.TiligSpacerVerticalMediumLarge
import com.tilig.android.ui.components.TiligSpacerVerticalSmall
import com.tilig.android.ui.onboarding.getstarted.components.*
import com.tilig.android.ui.settings.components.LockDialog
import com.tilig.android.ui.settings.components.LockUtilDialogState
import com.tilig.android.ui.settings.components.enableBiometricsWarning
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.SharedPrefs
import com.tilig.android.utils.lock.LockViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.koin.androidx.compose.inject

@OptIn(ExperimentalMaterialApi::class, ExperimentalComposeUiApi::class)
@Composable
fun GetStartedContent(
    navController: NavController,
    viewModel: GetStartedViewModel,
    lockViewModel: LockViewModel,
    currentBottomSheet: MutableState<GetStartedBottomSheetType?>,
    modalBottomSheetState: ModalBottomSheetState,
    coroutineScope: CoroutineScope,
    displayBackBtn: Boolean,
    tracker: Mixpanel
) {
    val uiState by viewModel.uiState.collectAsState()

    val context = LocalContext.current
    val keyboarController = LocalSoftwareKeyboardController.current
    val scrollState = rememberScrollState()
    val currentStep = remember {
        mutableStateOf<Step?>(null)
    }
    val showConfirmDismissDialog = remember {
        mutableStateOf(false)
    }

    LaunchedEffect(uiState.lastCreatedAccount) {
        if (currentStep.value == Step.TRY_AUTOFILL && uiState.lastCreatedAccount != null && !modalBottomSheetState.isVisible) {
            coroutineScope.launch {
                currentBottomSheet.value =
                    GetStartedBottomSheetType.SimulateAutofill(uiState.lastCreatedAccount!!)
                modalBottomSheetState.animateTo(ModalBottomSheetValue.Expanded)
            }
            viewModel.lastCreatedAccountOpened()
        }
    }
    LaunchedEffect(uiState.stepsStatus) {
        if (currentStep.value == Step.TRY_AUTOFILL && uiState.stepsStatus[Step.TRY_AUTOFILL] == true) {
            keyboarController?.hide()
        }
    }
    LaunchedEffect(currentStep.value) {
        when(currentStep.value) {
            Step.BIOMETRIC_STEP -> tracker.trackEvent(Tracker.GETTING_STARTED_BIOMETRICS_EXPLANATION_APPEARED)
            Step.LOGIN -> tracker.trackEvent(Tracker.GETTING_STARTED_ADD_LOGIN_EXPLANATION_APPEARED)
            Step.TRY_AUTOFILL -> tracker.trackEvent(Tracker.GETTING_STARTED_AUTOFILL_EXPLANATION_APPEARED)
            Step.CONNECT_DESKTOP -> tracker.trackEvent(Tracker.GETTING_STARTED_CONNECT_DESKTOP_APPEARED)
            else -> {}
        }
    }

    StatusBarAndNavigationBarContrast(
        statusBarBright = true,
        navigationBarBright = true,
        navigationBarScrollBehind = true
    )

    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(scrollState)
            .statusBarsPadding()
            .padding(start = DefaultPadding, top = DefaultPadding, end = DefaultPadding)
    ) {
        if (displayBackBtn) {
            IconButton(
                modifier = Modifier
                    .then(Modifier.size(32.dp)),
                onClick = {
                    navController.navigateUp()
                }) {
                Icon(
                    modifier = Modifier.size(16.dp),
                    painter = painterResource(id = R.drawable.ic_back_btn),
                    contentDescription = null,
                    tint = DarkBlue
                )
            }
        }

        TiligSpacerVerticalMediumLarge()

        Text(text = stringResource(id = R.string.get_started), style = StyleSpoofBoldDarkBlue24)

        Text(
            modifier = Modifier.padding(top = HalfPadding),
            text = stringResource(id = R.string.get_started_desc),
            style = StyleStolzRegularNeutral16
        )

        TiligSpacerVerticalMediumLarge()

        val dialogState = remember { mutableStateOf(LockUtilDialogState.NONE) }
        val prefs: SharedPrefs by inject()

        var index = 1
        uiState.stepsStatus.forEach {
            GetStartedStepItem(
                currentExpandedStep = currentStep,
                step =
                GetStartedStepUI(
                    step = it.key,
                    isCompleted = it.value,
                    orderNumber = index
                ) {
                    when (it.key) {
                        Step.BIOMETRIC_STEP -> {
                            BiometricContent(
                                onUseBiometric = {
                                    // Don't allow users to enable this if they have no fingerprints setup
                                    if (!lockViewModel.lockUtil.canAuthorize(context)) {
                                        // Can we instruct users to enable it?
                                        enableBiometricsWarning(
                                            context,
                                            lockViewModel.lockUtil,
                                            tracker,
                                            dialogState
                                        )
                                    } else {
                                        dialogState.value = LockUtilDialogState.GET_STARTED
                                    }
                                },
                                onSkip = {
                                    currentStep.value = Step.LOGIN
                                })

                            LockDialog(
                                dialogState,
                                onEnabled = {
                                    viewModel.updateStep(Step.BIOMETRIC_STEP, true)
                                    prefs.isLockEnabled = true
                                    tracker.trackEvent(Tracker.EVENT_BIOMETRICS_ENABLED)
                                }
                            )
                        }
                        Step.LOGIN -> {
                            AddLoginContent(
                                onSkip = {
                                    currentStep.value = Step.TRY_AUTOFILL
                                }
                            ) {
                                coroutineScope.launch {
                                    currentBottomSheet.value =
                                        GetStartedBottomSheetType.AddAccount(null)
                                    modalBottomSheetState.animateTo(ModalBottomSheetValue.Expanded)
                                }
                            }
                        }
                        Step.TRY_AUTOFILL -> {
                            TryAutofillContent(
                                enabled = uiState.stepsStatus[Step.LOGIN] == true,
                                onSkip = {
                                    currentStep.value = Step.CONNECT_DESKTOP
                                }
                            ) {
                                viewModel.getLastCreatedAccount()
                            }
                        }
                        Step.CONNECT_DESKTOP -> {
                            ConnectDesktopContent(
                                email = uiState.userEmail ?: "",
                                onSkip = {
                                    currentStep.value = null
                                }
                            )
                        }
                        Step.UNKNOWN -> {}
                    }
                }
            )
            index++
            TiligSpacerVerticalSmall()
        }

        // The buttons to the same thing, but the style changes once all tasks are completed.
        if (uiState.getProgress() < 1f) {
            TextButton(
                modifier = Modifier.align(Alignment.CenterHorizontally),
                onClick = {
                    showConfirmDismissDialog.value = true
                    tracker.trackEvent(Tracker.DISMISS_GETTING_STARTED_APPEARED)
                },
            ) {
                Text(
                    stringResource(id = R.string.get_started_dismiss_forever),
                    color = LightBlue,
                    style = StyleStolzRegularDarkBlue16
                )
            }
        } else {
            LaunchedEffect(uiState) {
                tracker.trackEvent(Tracker.GETTING_STARTED_COMPLETED_APPEARED)
            }

            TiligSpacerVerticalSmall()
            TiligButton(
                modifier = Modifier
                    .fillMaxWidth(),
                iconRes = null,
                text = stringResource(R.string.get_started_explore_tilig),
                enabled = true,
                isDark = true,
                isSizeConstrained = true,
                extraPadding = false,
                onClick = {
                    viewModel.dismissForever()
                    navController.navigateUp()
                    tracker.trackEvent(Tracker.GETTING_STARTED_DISMISSED)
                }
            )
        }

        if (BuildConfig.DEBUG) {
            TextButton(
                modifier = Modifier.align(Alignment.CenterHorizontally),
                onClick = {
                    viewModel.debugResetAll(completed = false)
                },
            ) {
                Text(
                    stringResource(id = R.string.get_started_debug_reset_all),
                    color = LightBlue,
                    style = StyleStolzRegularDarkBlue16
                )
            }
            TextButton(
                modifier = Modifier.align(Alignment.CenterHorizontally),
                onClick = {
                    viewModel.debugResetAll(completed = true)
                },
            ) {
                Text(
                    stringResource(id = R.string.get_started_debug_reset_all_green),
                    color = LightBlue,
                    style = StyleStolzRegularDarkBlue16
                )
            }
        }

        if (showConfirmDismissDialog.value) {
            GetStartedConfirmDismissDialog(
                progress = uiState.getProgress(),
                onClose = {
                    showConfirmDismissDialog.value = false
                }) {
                showConfirmDismissDialog.value = false
                viewModel.dismissForever()
                navController.navigateUp()
            }
        }
    }
}
