package com.tilig.android.ui.components

import androidx.compose.animation.core.Transition
import androidx.compose.animation.core.tween
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.data.models.SecretItemType
import com.tilig.android.ui.modifiers.dropShadow
import com.tilig.android.ui.theme.*
import androidx.compose.animation.core.animateFloat as animateFloat1

enum class MultiFabState {
    COLLAPSED, EXPANDED
}

class MultiFabItem(
    val icon: Painter,
    val label: String,
    val onFabItemClicked: () -> Unit
)

@Composable
fun MultiFloatingActionButton(
    modifier: Modifier = Modifier,
    fabIcon: Painter,
    // if null show all
    displayType: SecretItemType?,
    toState: MultiFabState,
    onItemClick: (SecretItemType) -> Unit,
    stateChanged: (fabstate: MultiFabState) -> Unit,
) {
    val items = getMultiActionButtons(onItemClick = onItemClick)
    val transition: Transition<MultiFabState> = updateTransition(targetState = toState, label = "")
    val alpha: Float by transition.animateFloat1(
        transitionSpec = {
            tween(durationMillis = 50)
        }, label = ""
    ) { state ->
        if (state == MultiFabState.EXPANDED) 1f else 0f
    }
    val rotation: Float by transition.animateFloat1(label = "") { state ->
        if (state == MultiFabState.EXPANDED) 45f else 0f
    }

    Column(modifier = modifier, horizontalAlignment = Alignment.End) {
        ExpandedActionButtons(
            items, transition, alpha, stateChanged
        )
        FloatingActionButton(
            shape = RoundedCornerShape(DefaultCornerRadius),
            backgroundColor = if (toState == MultiFabState.EXPANDED) DarkGrey else LightBlue,
            contentColor = Color.White,
            elevation = FloatingActionButtonDefaults.elevation(
                pressedElevation = 0.dp,
                defaultElevation = 0.dp
            ),
            onClick = {
                when (displayType) {
                    null -> {
                        stateChanged(
                            if (transition.currentState == MultiFabState.EXPANDED) {
                                MultiFabState.COLLAPSED
                            } else MultiFabState.EXPANDED
                        )
                    }
                    else -> onItemClick(displayType)
                }
            }) {
            Icon(
                painter = fabIcon,
                contentDescription = "",
                modifier = Modifier.rotate(rotation)
            )
        }
    }
}

@Composable
private fun getMultiActionButtons(onItemClick: (SecretItemType) -> Unit) = listOf(
    MultiFabItem(
        painterResource(id = R.drawable.ic_add),
        stringResource(id = R.string.bt_add_login),
        onFabItemClicked = { onItemClick.invoke(SecretItemType.ACCOUNT) }
    ),
    MultiFabItem(
        painterResource(id = R.drawable.ic_add),
        stringResource(id = R.string.bt_add_note),
        onFabItemClicked = { onItemClick.invoke(SecretItemType.NOTE) }
    ),
    MultiFabItem(
        painterResource(id = R.drawable.ic_add),
        stringResource(id = R.string.bt_add_card),
        onFabItemClicked = { onItemClick.invoke(SecretItemType.CREDIT_CARD) }
    ),
    MultiFabItem(
        painterResource(id = R.drawable.ic_add),
        stringResource(id = R.string.bt_add_wifi),
        onFabItemClicked = { onItemClick.invoke(SecretItemType.WIFI) }
    )
)

@Composable
private fun SmallFabButton(
    alpha: Float,
    title: String,
    onFabItemClicked: () -> Unit
) {
    ExtendedFloatingActionButton(
        modifier = Modifier
            .alpha(alpha)
            .dropShadow(),
        shape = RoundedCornerShape(DefaultCornerRadius),
        backgroundColor = LightBlue,
        contentColor = Color.White,
        elevation = FloatingActionButtonDefaults.elevation(
            pressedElevation = 0.dp,
            defaultElevation = 0.dp
        ),
        text = {
            Text(
                text = title,
                fontFamily = Stolzl,
                fontSize = FontSizeNormal,
                fontWeight = FontWeight.Normal,
            )
        },
        onClick = {
            onFabItemClicked.invoke()
        },
        icon = { Icon(painterResource(id = R.drawable.ic_add), "") }
    )
}

@Composable
private fun ExpandedActionButtons(
    items: List<MultiFabItem>,
    transition: Transition<MultiFabState>,
    alpha: Float,
    stateChanged: (fabstate: MultiFabState) -> Unit,
) {
    items.forEach { item ->
        SmallFabButton(alpha, item.label) {
            if (transition.currentState == MultiFabState.EXPANDED) {
                item.onFabItemClicked.invoke()
                stateChanged(MultiFabState.COLLAPSED)
            }
        }
        Spacer(modifier = Modifier.height(20.dp))
    }
}