package com.tilig.android.ui.onboarding.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Warning
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.tilig.android.ui.theme.Red
import com.tilig.android.ui.theme.StatusBarHeight
import com.tilig.android.ui.theme.StyleStolzRegularDarkBlue14
import com.tilig.android.ui.theme.White

@Composable
fun WarnSnackbar(
    snackbarData: SnackbarData,
    modifier: Modifier = Modifier,
    shape: Shape = RoundedCornerShape(8.dp),
    backgroundColor: Color = Red,
    contentColor: Color = White,
    actionColor: Color = SnackbarDefaults.primaryActionColor,
    elevation: Dp = 0.dp,
    textStyle: TextStyle = StyleStolzRegularDarkBlue14
) {
    Surface(
        modifier = modifier
            .fillMaxWidth()
            .padding(top = StatusBarHeight)
            .padding(horizontal = 8.dp),
        shape = shape,
        elevation = elevation,
        color = backgroundColor,
        contentColor = contentColor
    ) {
        CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.high) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 10.dp, horizontal = 16.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(
                    modifier = Modifier.size(12.dp),
                    imageVector = Icons.Outlined.Warning,
                    contentDescription = null,
                    tint = contentColor
                )
                Text(
                    modifier = Modifier.padding(start = 4.dp),
                    text = snackbarData.message,
                    style = textStyle,
                    color = contentColor,
                )
            }
        }
    }
}