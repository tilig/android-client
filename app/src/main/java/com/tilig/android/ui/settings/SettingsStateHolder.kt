package com.tilig.android.ui.settings

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ModalBottomSheetState
import androidx.compose.material.ModalBottomSheetValue
import androidx.compose.material.rememberModalBottomSheetState
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.platform.SoftwareKeyboardController
import androidx.core.app.ShareCompat
import androidx.navigation.NavController
import com.tilig.android.TiligApp
import com.tilig.android.utils.IntentsBuilder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class SettingsStateHolder @OptIn(ExperimentalMaterialApi::class, ExperimentalComposeUiApi::class) constructor(
    val navigator: NavController,
    val scrollState: ScrollState,
    val scope: CoroutineScope,
    val modalBottomSheetState: ModalBottomSheetState,
    private val webViewModalSettingsUrl: MutableState<SettingsUrl?>,
    private val keyboardController: SoftwareKeyboardController?
) {

    fun getCurrentSettingsUrl() = webViewModalSettingsUrl.value

    fun leaveScreen() {
        TiligApp.debounceClicks {
            navigator.navigateUp()
        }
    }

    fun sendMessageToSupport(context: Context) = context.startActivity(IntentsBuilder.getEmailSendIntent())

    @OptIn(ExperimentalMaterialApi::class)
    fun canCloseSheet() = modalBottomSheetState.isVisible

    @OptIn(ExperimentalMaterialApi::class, ExperimentalComposeUiApi::class)
    val closeSheet: () -> Unit = {
        keyboardController?.hide()
        scope.launch {
            modalBottomSheetState.animateTo(ModalBottomSheetValue.Hidden)
        }
        webViewModalSettingsUrl.value = null
    }

    @OptIn(ExperimentalMaterialApi::class)
    val openSheet: (SettingsUrl) -> Unit = {
        webViewModalSettingsUrl.value = it
        scope.launch { modalBottomSheetState.animateTo(ModalBottomSheetValue.Expanded) }
    }
}

@OptIn(ExperimentalMaterialApi::class, ExperimentalComposeUiApi::class)
@Composable
fun rememberSettingsStateHolder(
    navigator: NavController,
    scrollState: ScrollState = rememberScrollState(),
    scope: CoroutineScope = rememberCoroutineScope(),
    webViewModalSettingsUrl: MutableState<SettingsUrl?> = remember {
        mutableStateOf(
            null
        )
    },
    modalBottomSheetState: ModalBottomSheetState =  rememberModalBottomSheetState(
        initialValue = ModalBottomSheetValue.Hidden,
        confirmStateChange = {
            it != ModalBottomSheetValue.HalfExpanded
        }),
    keyboardController: SoftwareKeyboardController? = LocalSoftwareKeyboardController.current
): SettingsStateHolder = remember {
    SettingsStateHolder(
        navigator = navigator,
        scrollState = scrollState,
        scope = scope,
        webViewModalSettingsUrl = webViewModalSettingsUrl,
        modalBottomSheetState = modalBottomSheetState,
        keyboardController = keyboardController
        )
}
