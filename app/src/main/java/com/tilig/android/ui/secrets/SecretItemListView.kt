package com.tilig.android.ui.secrets

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import com.tilig.android.data.models.SecretItemType
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.models.tilig.CreditCard
import com.tilig.android.data.models.tilig.SecretItem
import com.tilig.android.utils.SecretItemExt.getSecretIcon

@Composable
fun SecretItemListView(
    modifier: Modifier = Modifier,
    state: SecretItemsUiState.SecretItemsSuccess,
    viewModel: SecretItemsViewModel,
    searchState: MutableState<TextFieldValue>,
    displayFilters: Boolean,
    withChevron: Boolean = true,
    onSecretItemClick: (SecretItem) -> Unit,
    onFilterDisplayType: (SecretItemType?) -> Unit,
) {
    val secretItems = state.secretItems

    // search for items by search query
    viewModel.searchForSecretItems(searchState.value.text)

    Column(modifier = modifier) {

        if (displayFilters) {
            FilterTabs(
                currentDisplayType = state.displayType,
                onFilterDisplayType = onFilterDisplayType,
                // if search is empty we don't display indicator count
                filteredSecretItemsByDisplayType = if (searchState.value.text.isNotEmpty()) state.filteredSecretItemsByDisplayType
                else null,
                displayOthers = state.hasCustomItems
            )
        }

        if (secretItems.isNotEmpty()) {
            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(horizontal = 16.dp)
            ) {
                items(items = secretItems, key = { item ->
                    item.id!!
                }) { item ->
                    SecretRow(
                        name = item.name,
                        icon = item.getSecretIcon(context = LocalContext.current),
                        withChevron = withChevron,
                        isShared = item.hasSharees(),
                        description = when(item){
                            is Account -> AnnotatedString(item.username?:"")
                            is CreditCard -> item.getCardNumberForOverview().text
                            else -> null
                        }
                    ) {
                        onSecretItemClick.invoke(item)
                    }
                }
            }
        } else {
            if (searchState.value.text.isNotEmpty() && state.totalNoSearchSecretItemsAmount > 0) {
                SearchResultsEmptyScreen()
            } else if (state.totalNoSearchSecretItemsAmount == 0) {
                SecretItemListEmptyScreen(
                    modifier = Modifier
                        .fillMaxSize()
                        .verticalScroll(rememberScrollState()),
                    displayType = state.displayType,
                )
            }
        }
    }
}