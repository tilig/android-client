package com.tilig.android.ui.secrets.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.unit.dp
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.theme.DefaultPadding
import com.tilig.android.ui.theme.MediumCornerRadius
import com.tilig.android.ui.theme.White

@Composable
fun BoxWithTitledContent(
    modifier: Modifier = Modifier,
    titleStr: String,
    tracker: Tracker? = null,
    event: String? = null,
    onClick: (() -> Unit)? = null,
    content: @Composable () -> Unit
) {
    BoxWithTitledContent(
        modifier,
        AnnotatedString(titleStr),
        tracker,
        event,
        onClick,
        content
    )
}

@Composable
fun BoxWithTitledContent(
    modifier: Modifier = Modifier,
    title: AnnotatedString,
    tracker: Tracker? = null,
    event: String? = null,
    onClick: (() -> Unit)? = null,
    content: @Composable () -> Unit
) {
    Box(
        modifier = modifier
            .background(
                color = White,
                shape = RoundedCornerShape(MediumCornerRadius)
            )
            .then(
                if (onClick != null) {
                    Modifier.clickable {
                        event?.let { tracker?.trackEvent(it) }
                        onClick.invoke()
                    }
                } else {
                    Modifier
                }
            )
    ) {
        TitledContent(
            modifier = Modifier.padding(
                start = DefaultPadding,
                end = DefaultPadding,
                top = DefaultPadding,
                bottom = 20.dp
            ),
            title = title
        ) {
            content()
        }
    }
}