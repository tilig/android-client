package com.tilig.android.ui.modifiers

import android.os.Build
import android.util.Log
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Paint
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.unit.Dp
import com.tilig.android.ui.theme.*

fun Modifier.dropShadow(
    color: Color = DefaultShadowColor,
    alpha: Float = 0.2f,
    borderRadius: Dp = DefaultCornerRadius,
    shadowRadius: Dp = DefaultShadowBlur,
    offsetY: Dp = DefaultShadowOffsetY,
    offsetX: Dp = DefaultShadowOffsetX,
    enabled: Boolean = true
) = this.drawBehind {

    if (!enabled) {
        return@drawBehind
    }

    // Shadows are not supported < Oreo
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val transparentColor =
            android.graphics.Color.toArgb(color.copy(alpha = 0.0f).value.toLong())
        val shadowColor = android.graphics.Color.toArgb(color.copy(alpha = alpha).value.toLong())

        this.drawIntoCanvas {
            val paint = Paint()
            val frameworkPaint = paint.asFrameworkPaint()
            frameworkPaint.color = transparentColor
            frameworkPaint.setShadowLayer(
                shadowRadius.toPx(),
                offsetX.toPx(),
                offsetY.toPx(),
                shadowColor
            )
            it.drawRoundRect(
                0f,
                0f,
                this.size.width,
                this.size.height,
                borderRadius.toPx(),
                borderRadius.toPx(),
                paint
            )
        }
    }
}
