package com.tilig.android.ui.settings.components

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tilig.android.DevActivity
import com.tilig.android.R
import com.tilig.android.ui.components.FieldTitle
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.onboarding.getstarted.GetStartedViewModel
import com.tilig.android.ui.theme.DoublePadding
import com.tilig.android.ui.theme.HalfPadding
import com.tilig.android.utils.DebugLogger
import com.tilig.android.utils.SharedPrefs
import org.koin.androidx.compose.get
import androidx.lifecycle.viewmodel.compose.viewModel

@Preview
@Composable
fun DevTogglesView(
    prefs: SharedPrefs = get(),
    getStartedViewModel: GetStartedViewModel = viewModel()
) {
    val stateIcon = remember {
        mutableStateOf(
            prefs.devToggleSideIconAutofill
                    && prefs.devToggleEnableInlineAutofill
        )
    }
    val stateSearch = remember {
        mutableStateOf(
            prefs.devToggleShowSearchInAutofill
        )
    }
    val stateAddNew = remember {
        mutableStateOf(
            prefs.devToggleShowAddNewInAutofill
        )
    }

    val enabledIcon = remember { mutableStateOf(prefs.devToggleEnableInlineAutofill) }
    val enabledSearch = remember { mutableStateOf(false) }
    val enabledAddNew = remember { mutableStateOf(false) }
    val context = LocalContext.current

    Column {
        FieldTitle(title = stringResource(id = R.string.debug_tools_for_developers))

        SettingsToggle(
            "Enable Inline Autofill",
            checked = prefs.devToggleEnableInlineAutofill,
            onCheckedChanged = {
                prefs.devToggleEnableInlineAutofill = it
                if (it) {
                    enabledIcon.value = true
                } else {
                    enabledIcon.value = false
                    stateIcon.value = false
                }
                true
            }
        )
        SettingsToggle(
            text = "Show Tilig icon on right side of autofill",
            checkedState = stateIcon,
            enabled = enabledIcon.value,
            checked = prefs.devToggleSideIconAutofill,
            modifier = Modifier.padding(
                start = DoublePadding,
                top = HalfPadding,
                end = HalfPadding,
                bottom = HalfPadding
            ),
            onCheckedChanged = {
                prefs.devToggleSideIconAutofill = it
                true
            }
        )
        SettingsToggle(
            "Include Search in Autofill",
            checkedState = stateSearch,
            enabled = enabledSearch.value,
            checked = prefs.devToggleShowSearchInAutofill,
            modifier = Modifier.padding(
                start = DoublePadding,
                top = HalfPadding,
                end = HalfPadding,
                bottom = HalfPadding
            ),
            onCheckedChanged = {
                prefs.devToggleShowSearchInAutofill = it
                true
            }
        )
        SettingsToggle(
            "Include 'Add New Account' in Autofill",
            checkedState = stateAddNew,
            enabled = enabledAddNew.value,
            checked = prefs.devToggleShowAddNewInAutofill,
            modifier = Modifier.padding(
                start = DoublePadding,
                top = HalfPadding,
                end = HalfPadding,
                bottom = HalfPadding
            ),
            onCheckedChanged = {
                prefs.devToggleShowAddNewInAutofill = it
                true
            }
        )
        SettingsToggle(
            "Force-enable FLAG_SAVE_ON_ALL_VIEWS_INVISIBLE",
            checked = prefs.devToggleEnableAutosaveFlag,
            onCheckedChanged = {
                prefs.devToggleEnableAutosaveFlag = it
                true
            }
        )

        FieldTitle(title = stringResource(id = R.string.debug_logs_for_developers))

        SettingsToggle(
            "Show debug after autosave",
            checked = prefs.devToggleEnableDebugAutosave,
            onCheckedChanged = {
                prefs.devToggleEnableDebugAutosave = it
                true
            }
        )
        SettingsToggle(
            "Keep autofill logs",
            checked = prefs.devToggleEnableDebugLogsAutofill,
            modifier = Modifier.padding(
                start = 0.dp,
                top = HalfPadding,
                end = HalfPadding,
                bottom = DoublePadding
            ),
            onCheckedChanged = {
                prefs.devToggleEnableDebugLogsAutofill = it
                true
            }
        )
        TiligButton(
            modifier = Modifier
                .padding(top = 8.dp)
                .fillMaxWidth(),
            iconRes = null,
            text = stringResource(R.string.bt_debug_show_logs),
            enabled = true,
            isDark = true,
            isSizeConstrained = true,
            onClick = {
                context.startActivity(
                    DevActivity.createIntent(
                        context,
                        DebugLogger().read(context)
                    )
                )
            })

        FieldTitle(title = stringResource(id = R.string.debug_password_managers))
        Row(
            modifier = Modifier.fillMaxWidth()
        ) {
            SwitchAutofillButton(
                context = context,
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
                title = "Tilig DEV",
                packageName = "com.tilig.android.debug"
            )
            SwitchAutofillButton(
                context = context,
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
                title = "Tilig PROD",
                packageName = "com.tilig.android"
            )
            SwitchAutofillButton(
                context = context,
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
                title = "OnePass",
                packageName = "com.onepassword.android"
            )
        }
        Row(
            modifier = Modifier.fillMaxWidth()
        ) {
            SwitchAutofillButton(
                context = context,
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
                title = "LastPass",
                packageName = "com.lastpass.lpandroid"
            )
            SwitchAutofillButton(
                context = context,
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
                title = "Dashlane",
                packageName = "com.dashlane"
            )
            SwitchAutofillButton(
                context = context,
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
                title = "Bitwarden",
                packageName = "com.x8bit.bitwarden"
            )
        }

        FieldTitle(title = stringResource(id = R.string.get_started))
        TiligButton(
            modifier = Modifier
                .padding(top = 8.dp)
                .fillMaxWidth(),
            iconRes = null,
            text = stringResource(R.string.get_started_debug_reset_all),
            enabled = true,
            isDark = true,
            isSizeConstrained = true,
            onClick = {
                getStartedViewModel.debugResetAll()
                Toast.makeText(context, "Exit and reopen the app to see the Getting Started bar.", Toast.LENGTH_LONG).show()
            })
    }

}

@Composable
private fun SwitchAutofillButton(
    context: Context,
    modifier: Modifier,
    title: String,
    packageName: String
) {
    TiligButton(
        modifier = modifier
            .height(64.dp)
            .padding(4.dp),
        iconRes = null,
        text = title,
        enabled = true,
        isDark = true,
        isSizeConstrained = false,
        onClick = {
            (context as Activity).startActivityForResult(Intent(Settings.ACTION_REQUEST_SET_AUTOFILL_SERVICE).apply {
                data = Uri.parse("package:$packageName")
            }, 1)
        })
}
