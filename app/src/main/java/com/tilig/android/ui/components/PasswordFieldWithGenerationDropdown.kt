package com.tilig.android.ui.components

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.theme.LightBlue
import com.tilig.android.ui.theme.StolzlBook
import com.tilig.android.ui.theme.Typography
import com.tilig.android.ui.theme.White

private val GeneratePasswordDropDownHeight = 48.dp
private val DropDownVerticalOffset = 42.dp
private val DropDownShape =  RoundedCornerShape(
    0.dp,
    0.dp,
    8.dp,
    8.dp
)

@Preview
@Composable
fun PasswordFieldWithGenerationDropdownPreview() {
    PasswordFieldWithGenerationDropdown(
        hint = "Lorem ipsum",
        state = TextFieldState(fieldValue = "Lipsum"),
        tracker = null
    )
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun PasswordFieldWithGenerationDropdown(
    hint: String,
    state: TextFieldState,
    tracker: Tracker?,
    onDone: (() -> Unit)? = null,
    onNext: (() -> Unit)? = null,
    onGeneratePassword: (() -> Unit)? = null,
) {
    val expanded = state.isFocused
    var rowSize by remember { mutableStateOf(Size.Zero) }

    Box(
        modifier = Modifier.onGloballyPositioned { layoutCoordinates ->
            rowSize = layoutCoordinates.size.toSize()
        },
    ) {
        PasswordField(
            hint = hint,
            state = state,
            tracker = tracker,
            onDone = onDone,
            onNext = onNext
        )

        if (expanded) {
            Row(
                horizontalArrangement = Arrangement.SpaceAround,
                modifier = Modifier
                    .offset(0.dp, DropDownVerticalOffset)
                    .clip(DropDownShape)
                    .align(Alignment.Center)
                    .height(GeneratePasswordDropDownHeight)
                    .width(with(LocalDensity.current) { rowSize.width.toDp() + 8.dp })
                    .background(color = LightBlue)
                    .clickable( interactionSource = remember { MutableInteractionSource() },
                        indication = rememberRipple(color = LightBlue),) {
                        onGeneratePassword?.invoke()
                    },
            ) {
                Text(
                    modifier = Modifier.align(Alignment.CenterVertically),
                    text = stringResource(id = R.string.generate_new_password),
                    color = White,
                    style = Typography.body1,
                    fontFamily = StolzlBook,
                )
                Icon(
                    modifier = Modifier
                        .padding(start = 8.dp)
                        .align(Alignment.CenterVertically),
                    painter = painterResource(id = R.drawable.ic_generate_password),
                    tint = White,
                    contentDescription = stringResource(id = R.string.cd_generate_password)
                )
            }
        }
    }
}
