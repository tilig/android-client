package com.tilig.android.ui.theme

import androidx.compose.ui.graphics.Color

// Standard colors
val White = Color(0xFFFFFFFF)
val Grey = Color(0xFFE8E8ED)
val DarkGrey = Color(0xFF666666)
val Black = Color(0xFF000000)
val Grey2 = Color(0xFFD0D0DC)

// Brand colors
val LightBlue = Color(0xFF695CFF)
val DarkBlue = Color(0xFF05003F)
val DarkGreen = Color(0xFF166A05)
val DimmedBlue = Color(0x8005003F) // 50% transparent DarkBlue
val DimmedBlue75 = Color(0x4B05003F) // 75% transparent DarkBlue

val DimmedLightBlue = Color(0x80695CFF) // 50% transparent LightBlue
// Because the buttons have a dropshadow, so making the buttons 50% also makes them more gray
val DimmedLightBlueButNotTransparent = Color(0xFFA49CFF) // 50% transparent LightBlue
val DimmedWhite = Color(0x80FFFFFF) // 50% transparent white
val Blue2 = Color(0xFFC7C2FF)
val Blue3 = Color(0xFFECEBFF)
val Blue4 = Color(0xFFF5F5FF)
val TiligGreen = Color(0xFF52A382)
val Green = Color(0xFF91C7B1)
val Green2 = Color(0xFFF1F8F5)
val Green3 = Color(0xFFC9E4D9)
val TiligYellow = Color(0xFFFCC200)
val TiligYellow2 = Color(0xFFFFFAEB)
val Pink = Color(0xFFFC9CAF)
val Pink2 = Color(0xFFFEEBEF)
val Red = Color(0xFFF9103F)

// Common colors
val DisabledGrey = Color(0xFFADADBF)
val BackgroundGrey = Color(0xFFF7F7F9)
val DefaultTextColor = DarkBlue
val DefaultIconColor = DarkBlue
val DisabledTextColor = DimmedBlue
val InverseTextColor = White
val InverseDisabledTextColor = DimmedWhite
val DefaultBackground = White
val DimmedBackground = DimmedWhite
val DefaultBorderColor = Grey
val DefaultShadowColor = Black
val PlaceholderTextColor = DimmedBlue
val WeblinkColor = Color(0xFF4E46E5)
val WifiBgColor = Color(0xFF08A1EC)

// Colors for specific Views
val SearchBarBg = BackgroundGrey
val SearchBarBorder = Grey
val SearchBarBorderFocused = LightBlue
val StatusBarColor = DarkBlue
val ProgressIndicatorColor = DarkBlue
val TwoFATutorialIndicatorInactive = Blue3
val ProgressColorInactive = Color(0x1A695CFF)
val SettingsArrowColor = Grey2
val SettingsWarnColor = Red
val IconColor = Color(0xFFB9B9CB)
val IconColorRed = Color(0xFFF9103F)
val DarkRedColor = Color(0xFFB30024)
val OtherPinkColor = Color(0xFFF66285)

val DefaultErrorColor = Pink
val DefaultWarnColor = Color(0xFFFFE285)
val WarningButtonTextColor = Color(0xFFFF0000)

val Neutral40 = Color(0xFF827FA4)
val Neutral30 = Color(0xFFA1A1BA)
val Neutral10 = Grey

val DeepGrey = Color(0xB305003F)
