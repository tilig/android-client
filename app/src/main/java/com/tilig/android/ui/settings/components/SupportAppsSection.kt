package com.tilig.android.ui.settings.components

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.OpenInNew
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.components.*
import com.tilig.android.ui.theme.*
import kotlinx.coroutines.flow.MutableStateFlow

private enum class AppType(
    @StringRes val nameRes: Int,
    @DrawableRes val iconRes: Int,
    @StringRes val linkRes: Int
) {
    APP_WEB(
        nameRes = R.string.label_open_webapp,
        iconRes = R.drawable.ic_squirrel,
        linkRes = R.string.tilig_web_app
    ),
    APP_ANDROID(
        nameRes = R.string.app_android,
        iconRes = R.drawable.ic_playstore_badge,
        linkRes = R.string.tilig_google_app
    ),
    APP_IOS(
        nameRes = R.string.app_ios,
        iconRes = R.drawable.ic_appstore_badge,
        linkRes = R.string.tilig_apple_app
    ),
    BROWSER_CHROME(
        nameRes = R.string.browser_chrome,
        iconRes = R.drawable.ic_chrome,
        linkRes = R.string.extension_chrome_link
    ),
    BROWSER_EDGE(
        nameRes = R.string.browser_edge,
        iconRes = R.drawable.ic_edge,
        linkRes = R.string.extension_edge_link
    ),
    BROWSER_OPERA(
        nameRes = R.string.browser_opera,
        iconRes = R.drawable.ic_opera,
        linkRes = R.string.extension_opera_link
    )
}

@Composable
fun SupportAppsComponent() {
    FieldTitle(
        title = stringResource(id = R.string.label_apps),
        titlePadding = 8.dp,
        style = StyleSpoofMediumDarkBlue20
    )

    val uriHandler = LocalUriHandler.current
    val webUri = stringResource(id = AppType.APP_WEB.linkRes)
    val androidUri = stringResource(id = AppType.APP_ANDROID.linkRes)
    val iosUri = stringResource(id = AppType.APP_IOS.linkRes)


    Column(
        modifier = Modifier
            .background(color = White, shape = RoundedCornerShape(MediumCornerRadius))
            .fillMaxWidth(),
    ) {
        Row(
            Modifier
                .fillMaxWidth()
                .clickable {
                    uriHandler.openUri(webUri)
                }
                .padding(DefaultPadding),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
                style = StyleStolzRegularDarkBlue14,
                text = stringResource(AppType.APP_WEB.nameRes)
            )
            Icon(
                Icons.Default.OpenInNew,
                contentDescription = "",
                tint = SettingsArrowColor,
                modifier = Modifier
                    .size(24.dp)
            )
        }

        Divider(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = DefaultPadding)
        )

        val expandMobileAppsState: MutableStateFlow<Boolean> = remember { MutableStateFlow(false) }
        val isMobileExpanded = expandMobileAppsState.collectAsState()
        val expandExtensionsState: MutableStateFlow<Boolean> = remember { MutableStateFlow(false) }
        val isExtensionsExpanded = expandExtensionsState.collectAsState()

        ExpandView(
            header = {
                ExpandHeaderTitleWithArrow(
                    title = stringResource(id = R.string.label_mobile_apps),
                    onHeaderClick = {
                        expandMobileAppsState.value = !expandMobileAppsState.value
                        expandExtensionsState.value = false
                    },
                    expanded = isMobileExpanded.value
                )
            },
            content = {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(
                            start = DefaultPadding,
                            bottom = DefaultPadding,
                            end = DefaultPadding
                        )
                ) {
                    Image(
                        modifier = Modifier
                            .clickable(
                                interactionSource = remember { MutableInteractionSource() },
                                indication = rememberRipple(color = Grey),
                            ) {
                                uriHandler.openUri(iosUri)
                            },
                        painter = painterResource(id = AppType.APP_IOS.iconRes),
                        contentDescription = stringResource(
                            id = AppType.APP_IOS.nameRes
                        )
                    )
                    TiligSpacerHorizontalMedium()
                    Image(
                        modifier = Modifier
                            .clickable(
                                interactionSource = remember { MutableInteractionSource() },
                                indication = rememberRipple(color = Grey),
                            ) {
                                uriHandler.openUri(androidUri)
                            },
                        painter = painterResource(id = AppType.APP_ANDROID.iconRes),
                        contentDescription = stringResource(
                            id = AppType.APP_ANDROID.nameRes
                        )
                    )
                }
            },
            expanded = isMobileExpanded.value,
            withBorders = false,
            cardShape = RectangleShape,
            elevation = 0.dp
        )

        Divider(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = DefaultPadding)
        )

        ExpandView(
            header = {
                ExpandHeaderTitleWithArrow(
                    title = stringResource(id = R.string.label_browser_extensions),
                    onHeaderClick = {
                        expandExtensionsState.value = !expandExtensionsState.value
                        expandMobileAppsState.value = false
                    },
                    expanded = isExtensionsExpanded.value
                )
            },
            content = {
                Column {
                    SupportedItem(AppType.BROWSER_CHROME)
                    SupportedItem(AppType.BROWSER_EDGE)
                    SupportedItem(AppType.BROWSER_OPERA)
                    TiligSpacerVerticalSmall()
                }
            },
            expanded = isExtensionsExpanded.value,
            withBorders = false,
            cardShape = RoundedCornerShape(
                bottomStart = MediumCornerRadius,
                bottomEnd = MediumCornerRadius
            ),
            elevation = 0.dp
        )

    }
}

@Composable
private fun SupportedItem(
    type: AppType
) {
    val uriHandler = LocalUriHandler.current
    val uri = stringResource(id = type.linkRes)
    Row(modifier = Modifier
        .fillMaxWidth()
        .clickable(
            interactionSource = remember { MutableInteractionSource() },
            indication = rememberRipple(color = Grey),
        ) {
            uriHandler.openUri(uri)
        }
        .padding(start = 18.dp, end = DefaultPadding, top = HalfPadding, bottom = HalfPadding),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Image(
            modifier = Modifier
                .size(24.dp),
            painter = painterResource(id = type.iconRes),
            contentDescription = stringResource(id = type.nameRes),
        )
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
                .padding(start = HalfPadding),
            style = StyleStolzBookDarkBlue14,
            text = stringResource(id = type.nameRes)
        )
    }
}

@Composable
@Preview
fun SupportAppsComponentPreview() {
    SupportAppsComponent()
}