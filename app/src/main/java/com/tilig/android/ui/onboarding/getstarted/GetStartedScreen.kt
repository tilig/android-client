package com.tilig.android.ui.onboarding.getstarted

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.models.tilig.SecretItem
import com.tilig.android.ui.accounts.addaccount.AddAccountBottomSheet
import com.tilig.android.ui.accounts.addaccount.AddAccountViewModel
import com.tilig.android.ui.onboarding.getstarted.components.*
import com.tilig.android.ui.secrets.SecretItemsViewModel
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.OnLifecycleEvent
import com.tilig.android.utils.lock.LockViewModel
import kotlinx.coroutines.launch
import org.koin.androidx.compose.get

@OptIn(ExperimentalMaterialApi::class, ExperimentalComposeUiApi::class)
@Composable
fun GetStartedScreen(
    navController: NavHostController,
    tracker: Mixpanel = get(),
    viewModel: SecretItemsViewModel = viewModel(),
    addAccountViewModel: AddAccountViewModel = viewModel(),
    getStartedViewModel: GetStartedViewModel,
    lockViewModel: LockViewModel,
    displayBackBtn: Boolean = true,
) {

    val modalBottomSheetState =
        rememberModalBottomSheetState(
            initialValue = ModalBottomSheetValue.Hidden,
            skipHalfExpanded = true
        )
    val currentBottomSheet = remember {
        mutableStateOf<GetStartedBottomSheetType?>(null)
    }
    val coroutineScope = rememberCoroutineScope()

    val addedItem = remember {
        mutableStateOf<Account?>(null)
    }
    val keyboardController = LocalSoftwareKeyboardController.current

    OnLifecycleEvent { _, event ->
        if (event == Lifecycle.Event.ON_RESUME) {
            // Start polling for profile changes
            getStartedViewModel.startPolling()
        } else if (event == Lifecycle.Event.ON_PAUSE) {
            // Stop polling
            getStartedViewModel.stopPolling()
        }
    }

    ModalBottomSheetLayout(
        sheetState = modalBottomSheetState,
        scrimColor = Black.copy(alpha = 0.75f),
        sheetContent = {
            GetStartedBottomSheetContent(
                tracker = tracker,
                addAccountViewModel = addAccountViewModel,
                secretItemsViewModel = viewModel,
                onItemCreated = {
                    coroutineScope.launch {
                        addedItem.value = it as Account
                        getStartedViewModel.attachRecentlyCreatedAccount(it)
                        getStartedViewModel.updateStep(Step.LOGIN, true)
                        modalBottomSheetState.animateTo(ModalBottomSheetValue.Hidden)
                    }
                },
                onAutofillCompleted = {
                    // if successfully autofilled on the device after got it
                    getStartedViewModel.updateStep(
                        Step.TRY_AUTOFILL,
                        getStartedViewModel.getAutofillCount() > 0
                    )
                    //  close the sheet
                    coroutineScope.launch {
                        modalBottomSheetState.animateTo(ModalBottomSheetValue.Hidden)
                        keyboardController?.hide()
                    }
                },
                bottomSheetType = currentBottomSheet.value
            ) {
                coroutineScope.launch {
                    modalBottomSheetState.animateTo(ModalBottomSheetValue.Hidden)
                }
                keyboardController?.hide()
            }
        },
        sheetShape = RoundedCornerShape(
            topStart = DefaultCornerRadius,
            topEnd = DefaultCornerRadius
        )
    ) {
        Column(
            modifier = Modifier
                .statusBarsPadding()
                .padding(top = DefaultPadding)
                .fillMaxSize()
        ) {
            GetStartedContent(
                navController = navController,
                currentBottomSheet = currentBottomSheet,
                modalBottomSheetState = modalBottomSheetState,
                coroutineScope = coroutineScope,
                displayBackBtn = displayBackBtn,
                viewModel = getStartedViewModel,
                lockViewModel = lockViewModel,
                tracker = tracker
            )
        }
    }
}

@Composable
private fun GetStartedBottomSheetContent(
    tracker: Mixpanel,
    addAccountViewModel: AddAccountViewModel,
    secretItemsViewModel: SecretItemsViewModel,
    onItemCreated: (secretItem: SecretItem) -> Unit,
    onAutofillCompleted: () -> Unit,
    bottomSheetType: GetStartedBottomSheetType?,
    closeSheet: () -> Unit,
) {
    Box(
        Modifier
            .navigationBarsPadding()
            .defaultMinSize(
                minWidth = 100.dp,
                minHeight = if (bottomSheetType is GetStartedBottomSheetType.AddAccount)
                    LocalConfiguration.current.screenHeightDp.dp
                else
                    600.dp
            )
            .imePadding()
    ) {
        when (bottomSheetType) {
            is GetStartedBottomSheetType.AddAccount -> {
                LaunchedEffect(Unit) {
                    addAccountViewModel.clearUIState()
                }
                AddAccountBottomSheet(
                    tracker = tracker,
                    viewModel = addAccountViewModel,
                    secretItemsViewModel = secretItemsViewModel,
                    suggestedAccount = bottomSheetType.suggestedAccount,
                    onCloseSheet = closeSheet
                ) {
                    onItemCreated(it)
                }
            }
            is GetStartedBottomSheetType.SimulateAutofill -> {
                SimulateAutofillBottomSheet(
                    bottomSheetType.account,
                    onFinish = onAutofillCompleted,
                    onClose = closeSheet
                )
            }
            null -> {
            }
        }
    }
}
