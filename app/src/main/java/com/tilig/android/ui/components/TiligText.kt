package com.tilig.android.ui.components

import androidx.compose.foundation.layout.Column
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import com.tilig.android.R
import com.tilig.android.ui.theme.DefaultTextColor
import com.tilig.android.ui.theme.FontSizeNormal
import com.tilig.android.ui.theme.Typography


@Composable
fun TiligText(
    modifier: Modifier = Modifier,
    textRes: Int,
    textColor: Color = DefaultTextColor,
    enabled: Boolean = true
) {
    Text(
        stringResource(id = textRes),
        modifier = modifier,
        style = Typography.h3,
        fontSize = FontSizeNormal,
        color = if (enabled) textColor else textColor.copy(alpha = 0.5f),
    )
}


@Composable
fun TiligText(
    modifier: Modifier = Modifier,
    text: String,
    textColor: Color = DefaultTextColor,
    overflow: TextOverflow = TextOverflow.Clip,
    enabled: Boolean = true
) {
    Text(
        text = text,
        modifier = modifier,
        style = Typography.h3,
        fontSize = FontSizeNormal,
        color = if (enabled) textColor else textColor.copy(alpha = 0.5f),
        overflow = overflow
    )
}

@Composable
@Preview(showBackground = true)
fun TiligTextPreview() {
    Column {
        TiligText(textRes = R.string.app_name, enabled = true)
        TiligText(textRes = R.string.app_name, enabled = false)
    }
}
