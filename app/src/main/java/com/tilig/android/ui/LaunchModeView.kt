package com.tilig.android.ui

import com.tilig.android.data.models.tilig.Account

interface LaunchModeView {

    fun isInCreateNewMode(): Boolean

    fun isInSearchMode(): Boolean

    fun getSearchQuery(): String?

    fun returnAutofillWithAccount(account: Account)
}
