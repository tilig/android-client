package com.tilig.android.ui.secrets.components

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tilig.android.R
import com.tilig.android.data.models.tilig.ItemFieldType
import com.tilig.android.ui.theme.*

@Composable
fun AddCustomField(modifier: Modifier, onSelectCustomField: (ItemFieldType) -> Unit) {
    var expanded by remember { mutableStateOf(false) }

    Box(
        modifier = modifier.wrapContentSize(Alignment.TopStart)
    ) {

        Button(modifier = Modifier
            .height(38.dp),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = White,
                contentColor = White,
                disabledBackgroundColor = White,
                disabledContentColor = White
            ),
            shape = RoundedCornerShape(8.dp),
            onClick = {
                expanded = !expanded
            }) {
            Icon(
                painter = painterResource(id = R.drawable.ic_add),
                contentDescription = "",
                tint = LightBlue,
                modifier = Modifier
                    .size(24.dp)
                    .align(Alignment.CenterVertically)
            )
            Spacer(modifier = Modifier.width(8.dp))
            Text(
                text = stringResource(id = R.string.add_field),
                style = TextStyle(
                    color = LightBlue,
                    fontSize = 14.sp,
                    fontFamily = Stolzl
                )
            )
        }

        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier
        ) {
            UserCustomField.values().forEach {
                DropdownMenuItem(onClick = {
                    onSelectCustomField.invoke(it.type)
                    expanded = false
                }) {
                    Icon(
                        modifier = Modifier.size(24.dp),
                        painter = painterResource(id = it.iconResId),
                        contentDescription = null
                    )
                    Text(
                        modifier = Modifier.padding(start = DefaultPadding),
                        text = stringResource(id = it.titleResId),
                        style = StyleStolzRegularDarkBlue14
                    )
                }
            }
        }
    }
}

/**
 * Custom fields that users can add to they item.
 */
private enum class UserCustomField(
    @StringRes val titleResId: Int,
    @DrawableRes val iconResId: Int,
    val type: ItemFieldType
) {
    TEXT(R.string.custom_field_text, R.drawable.ic_edit_note, ItemFieldType.TEXT),
    PASSWORD(R.string.custom_field_password, R.drawable.ic_lock, ItemFieldType.PASSWORD),
    SECRET(R.string.custom_field_secret, R.drawable.ic_revealed, ItemFieldType.SECRET),
    TWO_FA(R.string.custom_field_2fa, R.drawable.ic_clock, ItemFieldType.TOTP),
    DATE(R.string.custom_field_date, R.drawable.ic_calendar, ItemFieldType.DATE);
}