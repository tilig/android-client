package com.tilig.android.ui.secrets.components

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.tilig.android.R
import com.tilig.android.ui.components.TiligDialog
import com.tilig.android.ui.theme.LightBlue
import com.tilig.android.ui.theme.StyleStolzRegularDarkBlue16

@Composable
fun ColumnScope.DeleteSecretItemView(
    @StringRes titleId: Int,
    @StringRes dialogDescriptionId: Int,
    openDialog: MutableState<Boolean>,
    onConfirmDelete: () -> Unit
) {
    TextButton(
        modifier = Modifier.align(Alignment.CenterHorizontally),
        onClick = { openDialog.value = true },
    ) {
        Text(
            stringResource(id = titleId),
            color = LightBlue,
            style = StyleStolzRegularDarkBlue16
        )
    }
    if (openDialog.value) {
        TiligDialog(
            title = titleId,
            text = dialogDescriptionId,
            onDismissRequest = { openDialog.value = false },
            onCancel = { openDialog.value = false },
            cancelButtonText = R.string.cancel,
            confirmButtonText = R.string.delete,
            onConfirm = {
                openDialog.value = false
                onConfirmDelete.invoke()
            })
    }
}

@Composable
@Preview(showBackground = true)
fun DeleteSecretItemViewPreview() {
    Column {
        DeleteSecretItemView(
            titleId = R.string.bt_delete_account,
            dialogDescriptionId = R.string.dialog_delete_item,
            onConfirmDelete = {},
            openDialog = remember { mutableStateOf(false) }
        )
    }
}