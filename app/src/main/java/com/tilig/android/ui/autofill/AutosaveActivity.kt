package com.tilig.android.ui.autofill

import RelatedDomainsHelper
import Relation
import android.annotation.SuppressLint
import android.app.Activity
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.os.DeadObjectException
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.platform.ComposeView
import androidx.core.view.WindowCompat
import com.tilig.android.LockActivity
import com.tilig.android.MainActivity
import com.tilig.android.analytics.Breadcrumb
import com.tilig.android.analytics.ConversionAnalytics
import com.tilig.android.analytics.Tracker
import com.tilig.android.autofill.PackageVerifier
import com.tilig.android.autofill.google.StructureParser
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.ui.CryptoModel
import com.tilig.android.ui.LaunchModeView
import com.tilig.android.ui.autofill.components.AutofillActivityView
import com.tilig.android.ui.autofill.components.AutofillBottomSheetScreen
import com.tilig.android.ui.autofill.components.AutofillPopupActions
import com.tilig.android.utils.*
import io.sentry.Sentry
import io.sentry.SentryLevel
import org.koin.core.component.inject
import kotlin.random.Random

@RequiresApi(Build.VERSION_CODES.O)
class AutosaveActivity : LockActivity(), LaunchModeView {

    private val preferences: SharedPrefs by inject()
    private val autofillViewModel: AutofillViewModel by viewModels()
    private var parser: StructureParser? = null

    companion object {
        private const val REQUEST_CODE: Int = 7584

        fun createPendingIntentForCreatingNew(
            context: Context,
            applicationId: String?,
            url: String? = null,
            login: String? = null,
            password: String? = null,
            username: String? = null
        ): PendingIntent {
            // Note that from the FillResponseFactory we can only set an Intent
            // for opening the app. What we want is for all these arguments
            // to end up in the EditFragment instead. So, in the startup flow
            // we pick all these Intent extras and repackage them into Fragment
            // arguments (in the shape of a Parcelable called NewAccountHint).
            // This happens in the StartFragment.
            val intent = Intent(context, AutosaveActivity::class.java).apply {
                putExtra(MainActivity.EXTRA_ADD_APP_ID_STR, applicationId)
                putExtra(MainActivity.EXTRA_ADD_URL_STR, url)
                putExtra(MainActivity.EXTRA_ADD_LOGIN_STR, login)
                putExtra(MainActivity.EXTRA_ADD_PASSWORD_STR, password)
                putExtra(MainActivity.EXTRA_ADD_USERNAME_STR, username)
                // This is a hack that enables us to create two PendingIntents at the same
                // time for the same Activity: one for search, one for creating a new secret
                action = Random.Default.nextInt().toString()
            }
            return PendingIntent.getActivity(
                context,
                REQUEST_CODE,
                intent,
                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
            )
        }
    }

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        WindowCompat.setDecorFitsSystemWindows(window, false)

        // Try to fix to portrait.
        // According to Sentry reports, some OS versions don't allow locking to portrait when
        // the Activity has a translucent background. Since it seems to work for most users,
        // we try it anyway.
        // More info: https://stackoverflow.com/questions/48072438/java-lang-illegalstateexception-only-fullscreen-opaque-activities-can-request-o
        if (Build.VERSION.SDK_INT != Build.VERSION_CODES.O) {
            try {
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            } catch (e: IllegalStateException) {
                Sentry.captureMessage("Not allowed to lock translucent activity to portrait")
            }
        }

        // Do some logging
        Breadcrumb.drop(
            if (preferences.isLoggedIn())
                Breadcrumb.CryptoTrail_LoggedIn
            else
                Breadcrumb.CryptoTrail_LoggedOut
        )

        // Track this event (User tapped the 'Autofill with Tilig' autofill entry)
        mixpanel.trackEvent(Tracker.EVENT_AUTOSAVE_EXTENSION_OPENED)

        val username = intent.getStringExtra(MainActivity.EXTRA_ADD_USERNAME_STR)
        val password = intent.getStringExtra(MainActivity.EXTRA_ADD_PASSWORD_STR)
        val appId = intent.getStringExtra(MainActivity.EXTRA_ADD_APP_ID_STR)
        val url = intent.getStringExtra(MainActivity.EXTRA_ADD_URL_STR)

        // Pick up the name, based on either the App name or the website URL
        val name = if (appId.isNullOrEmpty()) {
            url?.getDomain()?.removePredefinedPrefixesFromDomain()
        } else {
            PackageVerifier.getAppName(this, appId)
                ?: url?.getDomain()?.removePredefinedPrefixesFromDomain()
        }
        val account = Account.createEmpty().apply {
            this.name = name
            this.website = url
            this.androidAppId = appId
            this.username = username
            this.password = password
        }

        // Fetch accounts
        initParser()
        autofillViewModel.enterAddingModeImmediately(account)

        setContentView(ComposeView(this).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )

            val chosenAccount = mutableStateOf<Account?>(null)
            setContent {
                AutofillActivityView(
                    autofillViewModel,
                    lockViewModel,
                    account,
                    chosenAccount.value,
                    startScreen = AutofillBottomSheetScreen.AddAccountScreen,
                    action = { action ->
                        if (!isFinishing) {
                            when (action) {
                                is AutofillPopupActions.FillSelected -> {
                                    returnAutofillWithAccount(action.account)
                                }
                                is AutofillPopupActions.StartAddAccount -> {
                                    autofillViewModel.enterAddingMode(action.account)
                                }
                                AutofillPopupActions.ClosePopup -> {
                                    mixpanel.trackEvent(Tracker.EVENT_AUTOFILL_EXTENSION_CLOSED)
                                    finish()
                                }
                                is AutofillPopupActions.ChosenAccount -> {
                                    val relation: Relation? = if (parser?.isWebsite == true) {
                                        parser?.getWebsite()
                                            ?.removePredefinedPrefixesFromDomain()
                                            ?.let {
                                                RelatedDomainsHelper.lookupDomain(it)
                                            }
                                    } else {
                                        RelatedDomainsHelper.lookupApp(parser?.getStructPackageName())
                                    }
                                    if (autofillViewModel.isAccountMatchesWithAutofill(
                                            parser = parser,
                                            account = action.account,
                                            relation = relation
                                        )
                                    ) {
                                        returnAutofillWithAccount(action.account)
                                    } else {
                                        mixpanel.trackEvent(Tracker.EVENT_PHISHING_WARNING_APPEARED)
                                        chosenAccount.value = action.account
                                    }
                                }
                                AutofillPopupActions.ReturnToAccountsList -> {
                                    chosenAccount.value = null
                                    autofillViewModel.enterDefaultMode()
                                }
                                AutofillPopupActions.RefreshAutofillAccountsList -> {
                                    autofillViewModel.secretItems(parser = parser)
                                }
                            }
                        }
                    }
                )
            }
        })
    }

    private fun initParser() {
        try {
            if (parser == null) {
                Breadcrumb.drop(Breadcrumb.AutofillTrail_Parser_Null)
                //parser = StructureParser(applicationContext, fillRequest!!)
            } else {
                Breadcrumb.drop(Breadcrumb.AutofillTrail_Parser_Not_Null)
            }
            parser?.parse(forFill = true)
        } catch (e: DeadObjectException) {
            // This happens, and we don't know why. Docs don't say much, the internet suggests there's
            // a parcel too big somewhere, but that doesn't make sense either. I suspect the parser
            // tries to access the previous screen again, and the device has already cleared it
            // from memory. In that case, there's not much we can do: we can't parse it,
            // then we cant tell the system which value to put in which field. The rest of this
            // method will still execute, but the target app will not be filled with anything.
            // Original Sentry error: https://sentry.io/organizations/tilig/issues/3084436429/?environment=production&project=5237937
            // Note the 'DEVICE_STORAGE_LOW' event right before.
            Sentry.captureMessage(
                e.message ?: "DeadObjectException in initFillRequestAndParser",
                SentryLevel.DEBUG
            )
        } catch (e: RuntimeException) {
            // Samesies, but the DeadObjectException is wrapped in a RuntimeException
            Sentry.captureMessage(
                e.message ?: "RuntimeException(DeadObjectException) in initFillRequestAndParser",
                SentryLevel.DEBUG
            )
        }
    }

    override fun isInCreateNewMode(): Boolean = true
    override fun isInSearchMode(): Boolean = false
    override fun getSearchQuery(): String? = null

    override fun returnAutofillWithAccount(account: Account) {
        // Unlike in the AutofillActivity, we are not here to fill anything.
        // So, we don't do anything (no parsing, no getting the fillrequest,
        // not constructing a response), we simply exit:
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        mixpanel.onSessionEnd()
    }
}
