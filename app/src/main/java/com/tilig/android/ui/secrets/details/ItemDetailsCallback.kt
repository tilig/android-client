package com.tilig.android.ui.secrets.details

import com.tilig.android.ui.secrets.details.events.ItemDetailsActionEvent

interface ItemDetailsCallback {
    // todo think about better name for business logic actions
    fun onActionEvent(event: ItemDetailsActionEvent)
    fun onOpenQrCodeScanner()
    fun onShowSnackbarMessage(message: String)
    fun onUpdateBottomSheet(type: BottomSheetType?)
}
