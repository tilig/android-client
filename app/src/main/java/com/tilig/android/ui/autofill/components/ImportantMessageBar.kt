package com.tilig.android.ui.autofill.components

import androidx.annotation.StringRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.theme.TiligYellow
import com.tilig.android.ui.theme.Typography

@Composable
fun ImportantMessageBar(
    modifier: Modifier = Modifier,
    @StringRes message: Int,
    @StringRes buttonText: Int,
    onClick: () -> Unit = {}
) {
    Row(
        modifier = modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .imePadding()
            .navigationBarsPadding()
            .background(TiligYellow),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = stringResource(id = message),
            style = Typography.h3,
            modifier = Modifier
                .padding(
                    top = 16.dp,
                    bottom = 16.dp,
                    end = 16.dp,
                    start = 24.dp
                )
                .weight(1f)
        )
        TiligButton(
            modifier = Modifier
                .padding(top = 16.dp, bottom = 16.dp, end = 24.dp)
                // Note: this button *could* be a little less tall, but since upgrading the
                // androidx.compose.material:material library from 1.0.5 to 1.1.0, the shadows are
                // too large on small buttons.
                // We can't downgrade the library because we need it for the collapsing toolbar
                .height(48.dp),
            iconRes = null,
            text = stringResource(buttonText),
            enabled = true,
            isSizeConstrained = false,
            onClick = onClick
        )
    }
}

@Preview
@Composable
fun ImportantMessageBarPreview() {
    ImportantMessageBar(
        message = R.string.label_enable_autofill,
        buttonText = R.string.bt_enable
    )
}