package com.tilig.android.ui.twofa.bottomsheets.tutorial

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.insets.navigationBarsWithImePadding
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPagerIndicator
import com.google.accompanist.pager.rememberPagerState
import com.tilig.android.R
import com.tilig.android.ui.components.ScreenHeader
import com.tilig.android.ui.theme.*

internal const val PAGES_COUNT = 4

@OptIn(
    ExperimentalPagerApi::class, androidx.compose.animation.ExperimentalAnimationApi::class,
    kotlinx.coroutines.InternalCoroutinesApi::class
)
@Composable
fun TwoFATutorialBottomSheet(closeSheet: () -> Unit, goToScan: () -> Unit) {
    val pagerState = rememberPagerState(
        initialPage = 0,
    )
    val showEnableButton = remember { mutableStateOf(false) }
    val bottomSheetHeight = LocalConfiguration.current.screenHeightDp.dp - 24.dp

    Column(
        modifier = Modifier
            .navigationBarsWithImePadding()
            .height(bottomSheetHeight)
    ) {
        ScreenHeader(headerTextRes = R.string.enable_2fa, closeSheet = closeSheet)
        TwoFATutorialPager(
            pagerState = pagerState, modifier = Modifier.weight(1f)
        )
        HorizontalPagerIndicator(
            pagerState = pagerState,
            modifier = Modifier
                .padding(16.dp)
                .height(20.dp)
                .align(Alignment.CenterHorizontally),
            activeColor = LightBlue,
            inactiveColor = TwoFATutorialIndicatorInactive
        )
        LaunchedEffect(pagerState) {
            snapshotFlow { pagerState.currentPage }.collect {
                showEnableButton.value = pagerState.currentPage == pagerState.pageCount - 1
            }
        }
        if (showEnableButton.value) {
            TextButton(
                modifier = Modifier
                    .align(alignment = Alignment.CenterHorizontally)
                    .padding(
                        start = DefaultPadding,
                        end = DefaultPadding
                    )
                    .background(color = LightBlue, shape = RoundedCornerShape(8.dp))
                    .fillMaxWidth(),
                onClick = {
                    closeSheet()
                    goToScan()
                },
            ) {
                Text(
                    text = stringResource(id = R.string.enable_2fa),
                    style = TextStyle(color = White, fontFamily = Stolzl, fontSize = 14.sp)
                )
            }
        }
    }

    BackHandler {
        closeSheet()
    }
}