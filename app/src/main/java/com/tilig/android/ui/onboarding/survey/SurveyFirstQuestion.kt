package com.tilig.android.ui.onboarding.survey

import androidx.annotation.DrawableRes
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.lifecycle.viewmodel.compose.viewModel
import com.tilig.android.R
import com.tilig.android.ui.components.TiligCheckBox
import com.tilig.android.ui.theme.*

@Composable
fun SurveyFirstQuestion(viewModel: SurveyViewModel) {
    val firstQuestion = surveyQuestions[FIRST_QUESTION_KEY]!!
    val answerOptions = remember {
        mapOf(
            firstQuestion.answerOptions[0] to R.drawable.ic_phone,
            firstQuestion.answerOptions[1] to R.drawable.ic_tablet,
            firstQuestion.answerOptions[2] to R.drawable.ic_laptop
        )
    }
    val checkedSet = remember { viewModel.getFirstQuestionAnswers() }
    Column(
        modifier = Modifier
            .padding(horizontal = 32.dp)
            .fillMaxSize()
    ) {
        Text(
            text = firstQuestion.content,
            style = Typography.h1.copy(fontSize = 24.sp, lineHeight = 32.sp, color = DarkBlue),
            textAlign = TextAlign.Start
        )
        Spacer(modifier = Modifier.height(16.dp))
        answerOptions.forEach { item ->
            Spacer(modifier = Modifier.height(16.dp))
            SurveyFirstQuestionCard(
                item.key,
                item.value,
                checkedSet.contains(item.key),
                viewModel
            )
        }
    }
}

@Composable
private fun SurveyFirstQuestionCard(
    title: String,
    @DrawableRes iconRes: Int,
    isChecked: Boolean,
    viewModel: SurveyViewModel
) {
    val checked = remember {
        mutableStateOf(isChecked)
    }

    val onCheckChange: ((Boolean) -> Unit) = {
        checked.value = it
        if (checked.value) {
            viewModel.addAnswerToFirstQuestion(title)
        } else {
            viewModel.removeAnswerForFirstQuestion(title)
        }
    }

    ConstraintLayout(
        modifier = Modifier
            .fillMaxWidth()
            .height(64.dp)
            .clip(RoundedCornerShape(DefaultCornerRadius))
            .background(
                color = BackgroundGrey,
                shape = RoundedCornerShape(DefaultCornerRadius)
            )
            .border(
                width = DefaultSmallBorder,
                shape = RoundedCornerShape(DefaultCornerRadius),
                color = if (checked.value) LightBlue else Grey
            )
            .clickable {
                onCheckChange(!checked.value)
            }
    ) {
        val (checkbox, titleView, icon) = createRefs()

        TiligCheckBox(
            checked = checked.value,
            onCheckedChange = onCheckChange,
            modifier = Modifier
                .constrainAs(checkbox) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start, margin = DefaultPadding)
                }
                .size(20.dp)
        )

        Text(
            modifier = Modifier
                .padding(horizontal = DefaultPadding)
                .constrainAs(titleView) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(checkbox.end)
                },
            text = title,
            style = Typography.h3.copy(
                fontSize = 16.sp,
                color = if (checked.value) LightBlue else DarkBlue
            )
        )
        Icon(
            modifier = Modifier
                .padding(end = 24.dp)
                .constrainAs(icon) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    end.linkTo(parent.end)
                },
            painter = painterResource(id = iconRes),
            contentDescription = title,
            tint = if (checked.value) LightBlue else DarkBlue
        )
    }
}

@Preview
@Composable
fun SurveyFirstQuestionPreview() {
    SurveyFirstQuestion(viewModel = viewModel())
}