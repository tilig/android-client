package com.tilig.android.ui.onboarding.survey

import com.tilig.android.data.models.tilig.SurveyQuestionAttribute

const val FIRST_QUESTION_KEY = "FirstQuestion"
const val SECOND_QUESTION_KEY = "SecondQuestion"
const val THIRD_A_QUESTION_KEY = "ThirdAQuestion"
const val THIRD_B_QUESTION_KEY = "ThirdBQuestion"
const val FINISH_QUESTION_KEY = "FinishQuestion"

val surveyQuestions = mapOf(
    FIRST_QUESTION_KEY to SurveyQuestionAttribute(
        content = "On which devices do you need to access your passwords?",
        surveyToken = "c702462a-a330-4bad-bc6f-73dc9dfc7949",
        token = "5977c0cb-5145-418b-a2b4-03da2ca0381d",
        answerOptions = listOf("Phone", "Tablet", "Computer")
    ),
    SECOND_QUESTION_KEY to SurveyQuestionAttribute(
        content = "Have you used a password manager before?",
        surveyToken = "c702462a-a330-4bad-bc6f-73dc9dfc7949",
        token = "277d8e7d-a733-4377-8a7c-32dffd46e5c1",
        answerOptions = listOf(
            "Yes",
            "No, but I know how it works",
            "No, and I don’t know how it works"
        )
    ),
    THIRD_A_QUESTION_KEY to SurveyQuestionAttribute(
        content = "Which password manager(s) do you have experience using?",
        surveyToken = "c702462a-a330-4bad-bc6f-73dc9dfc7949",
        token = "cd91eae8-6a6d-4fba-befc-d0aba7046632",
        answerOptions = listOf(
            "LastPass",
            "Keeper",
            "Bitwarden",
            "1Password",
            "Apple Keychain",
            "Google/Chrome",
        )
    ),
    THIRD_B_QUESTION_KEY to SurveyQuestionAttribute(
        content = "How many of the following apply to you?",
        surveyToken = "c702462a-a330-4bad-bc6f-73dc9dfc7949",
        token = "a2df4855-6dba-414c-8ddb-18d44f85af83",
        answerOptions = listOf(
            "I reuse the same password in many places",
            "I write down my passwords in a safe place",
            "I share passwords for some accounts with other people",
            "I usually use two-factor authentication, when it’s available"
        )
    ),
    FINISH_QUESTION_KEY to SurveyQuestionAttribute(
        content = "Available for a short chat?",
        surveyToken = "c702462a-a330-4bad-bc6f-73dc9dfc7949",
        token = "7da5e13d-e8b0-4cfa-af87-8e2725c02529",
        answerOptions = listOf("Yes", "No")
    )
)