package com.tilig.android.ui.twofa

import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.LinearOutSlowInEasing
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.ui.theme.*

@OptIn(ExperimentalFoundationApi::class)
@ExperimentalMaterialApi
@Composable
fun TwoFactorAuthEnabled(
    modifier: Modifier,
    otp: String?,
    onNewCodeGenerated: (code: String?) -> Unit = {},
) {
    val otpState = remember { otp }

    otpState?.let {
        SecureCode(
            modifier = modifier,
            state = it,
            onNewCodeGenerated = onNewCodeGenerated
        )
    }
}

@ExperimentalMaterialApi
@Composable
fun TwoFactorAuthNotEnabled(
    onOpenScanScreen: () -> Unit,
    accountName: String,
    tracker: Tracker,
    onShowTutorial: () -> Unit
) {
    var expandedState by remember { mutableStateOf(false) }
    val rotationState by animateFloatAsState(
        targetValue = if (expandedState) 180f else 0f
    )

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .animateContentSize(
                animationSpec = tween(
                    durationMillis = 300,
                    easing = LinearOutSlowInEasing
                )
            ),
        elevation = 0.dp,
        onClick = {
            expandedState = !expandedState
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(color = White, shape = RoundedCornerShape(8.dp))
        ) {
            Row(
                modifier = Modifier
                    .height(DefaultButtonHeight)
                    .padding(horizontal = 16.dp, vertical = 12.dp)
                    .fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                Text(
                    modifier = Modifier.weight(1f),
                    text = stringResource(id = R.string.tfa_enable),
                    style = TextStyle(fontSize = 14.sp, fontFamily = Stolzl),
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
                IconButton(
                    modifier = Modifier
                        .size(16.dp)
                        .rotate(rotationState),
                    onClick = {
                        expandedState = !expandedState
                    }) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_expand_dropdown),
                        contentDescription = null
                    )
                }
            }
            if (expandedState) {
                Column(
                    modifier = Modifier.padding(start = 16.dp, end = 16.dp, bottom = 12.dp),
                ) {
                    Text(
                        text = stringResource(
                            id = R.string.two_factor_explanation_description,
                            accountName
                        ),
                        style = TextStyle(fontFamily = StolzlBook, fontSize = 14.sp),
                        overflow = TextOverflow.Ellipsis
                    )
                    Spacer(modifier = Modifier.height(12.dp))
                    TwoFactorNotEnabledActionRow(
                        onOpenScanScreen = onOpenScanScreen,
                        onShowTutorial = onShowTutorial,
                        tracker = tracker
                    )
                }
            }
        }
    }
}

@Composable
private fun TwoFactorNotEnabledActionRow(
    onOpenScanScreen: () -> Unit,
    tracker: Tracker,
    onShowTutorial: () -> Unit
) {
    Row(verticalAlignment = Alignment.CenterVertically) {
        Button(modifier = Modifier
            .height(38.dp),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = White,
                contentColor = White,
                disabledBackgroundColor = White,
                disabledContentColor = White
            ),
            shape = RoundedCornerShape(8.dp),
            onClick = {
                onShowTutorial()
            }) {
            Text(
                text = stringResource(id = R.string.learn_more),
                style = TextStyle(
                    color = LightBlue,
                    fontSize = 14.sp,
                    fontFamily = Stolzl
                )
            )
        }
        Spacer(Modifier.width(16.dp))
        Button(modifier = Modifier
            .height(38.dp),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = LightBlue,
                contentColor = LightBlue,
                disabledBackgroundColor = LightBlue,
                disabledContentColor = LightBlue
            ),
            shape = RoundedCornerShape(8.dp),
            onClick = {
                tracker.trackEvent(Tracker.EVENT_2FA_SCAN_QR_CODE_TAPPED)
                onOpenScanScreen()
            }) {
            Icon(
                modifier = Modifier.size(16.dp),
                painter = painterResource(id = R.drawable.ic_scan_white),
                contentDescription = "",
                tint = White
            )
            Spacer(modifier = Modifier.width(8.dp))
            Text(
                modifier = Modifier.height(32.dp),
                text = stringResource(id = R.string.scan_qr_code),
                style = TextStyle(
                    color = White,
                    fontSize = 14.sp,
                    fontFamily = Stolzl,
                    textAlign = TextAlign.Center
                )
            )
        }
    }
}

@ExperimentalFoundationApi
@Composable
private fun SecureCode(
    modifier: Modifier,
    state: String,
    onNewCodeGenerated: (code: String?) -> Unit = {},
) {

    val codeValue = remember {
        mutableStateOf<String?>(null)
    }

    Box(
        modifier = Modifier
            .fillMaxSize()
    ) {
        SecurityCodeComponent(
            modifier = modifier,
            token = state,
            isSmallView = true,
            onNewCodeGenerated = { newCode ->
                codeValue.value = newCode
                onNewCodeGenerated(codeValue.value)
            }
        )
    }
}
