package com.tilig.android.ui.cards.details.holders

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import com.tilig.android.data.models.tilig.CardType
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.Folders
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.cards.details.CardEditWrapper
import com.tilig.android.ui.secrets.CustomFieldStateWrapper
import com.tilig.android.utils.CardUtils
import kotlinx.coroutines.selects.select

data class EditCardStateHolder(
    val foldersList: Folders?,
    val nameState: TextFieldState,
    val cardNumberState: TextFieldState,
    val cardHolderState: TextFieldState,
    val cardExpireState: TextFieldState,
    val cardSecurityCodeState: TextFieldState,
    val cardPinState: TextFieldState,
    val cardZipCodeState: TextFieldState,
    val cardExtraInfoState: TextFieldState,
    val customFields: List<CustomFieldStateWrapper>,
    val selectedFolder: Folder?
){
    fun getCardNumberFieldMaxLength(): Int? {
        return when (CardUtils.identifyCardType(cardNumberState.text)) {
            CardType.AMEX -> 15
            CardType.DINERS_CLUB -> 14
            else -> null
        }
    }
}

@Composable
fun rememberEditCardStateHolder(
    foldersList: Folders?,
    cardEditWrapper: CardEditWrapper
) =
    EditCardStateHolder(
        foldersList = foldersList,
        nameState = remember {
            TextFieldState(
                cardEditWrapper.name,
                validator = { it.isNotEmpty() })
        },
        cardNumberState = remember {
            TextFieldState(
                cardEditWrapper.cardNumber,
                validator = {    it.isNotEmpty() && !CardUtils.isValidCardNumber(it)
                })
        },
        cardHolderState = remember {
            TextFieldState(cardEditWrapper.holderName) },
        cardExpireState = remember {
            TextFieldState(
                cardEditWrapper.expirationDate,
                validator = { it.isNotEmpty() })
        },
        cardSecurityCodeState = remember {
            TextFieldState(cardEditWrapper.secureCode)
        },
        cardPinState = remember {
            TextFieldState(cardEditWrapper.pinCode) },
        cardZipCodeState = remember { TextFieldState(cardEditWrapper.zipCode) },
        cardExtraInfoState = remember { TextFieldState(cardEditWrapper.extraInfo) },
        customFields =
        cardEditWrapper.customFields?.map {
            CustomFieldStateWrapper(it)
        } ?: emptyList(),
        selectedFolder = cardEditWrapper.selectedFolder
    )