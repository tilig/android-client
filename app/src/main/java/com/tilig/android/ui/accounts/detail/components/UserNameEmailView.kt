package com.tilig.android.ui.accounts.detail.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.components.BorderedBasicTextField
import com.tilig.android.ui.secrets.components.DetailsTextField
import com.tilig.android.ui.secrets.components.TitledContent
import com.tilig.android.ui.theme.StyleStolzRegularDarkBlue16
import com.tilig.android.utils.isValidEmail

@Composable
fun UserNameEmailView(
    userNameEmail: String,
    tracker: Tracker,
    onCopyClick: (() -> Unit)? = null
) {
    TitledContent(
        tracker = tracker,
        event = Tracker.EVENT_USERNAME_COPY,
        title =  stringResource(id = if (userNameEmail.isValidEmail()) R.string.label_login_email else R.string.label_login_username),
        onClick = onCopyClick
    ) {
        DetailsTextField(value = userNameEmail,
            textStyle = StyleStolzRegularDarkBlue16)
    }
}

@Composable
fun UserNameEmailEdit(
    userNameEmailState: TextFieldState,
    focusManager: FocusManager,
    onValueChanged: ((String) -> Unit)? = null,
    ) {
    TitledContent(title = stringResource(id = if (userNameEmailState.isValid) R.string.label_login_email else R.string.label_login_username))
    {
        BorderedBasicTextField(modifier = Modifier,
            textStyle = StyleStolzRegularDarkBlue16,
            hint = stringResource(id = R.string.hint_login_username_email),
            state = userNameEmailState,
            singleLine = false,
            textModifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 12.dp, vertical = 4.dp),
            keyboardType = KeyboardType.Text,
            onValueChanged = onValueChanged,
            onNext = {
                focusManager.moveFocus(FocusDirection.Down)
            }
        )
    }
}
