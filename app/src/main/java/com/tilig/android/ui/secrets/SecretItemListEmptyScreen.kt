package com.tilig.android.ui.secrets

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.data.models.SecretItemType
import com.tilig.android.ui.secrets.components.NotificationWithIconTitleDescView
import com.tilig.android.ui.theme.DarkBlue

@Composable
fun SecretItemListEmptyScreen(
    modifier: Modifier = Modifier
        .fillMaxSize(),
    displayType: SecretItemType?,
) {
    Column(
        modifier,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.height(96.dp))
        NotificationWithIconTitleDescView(
            painter = painterResource(id = R.drawable.ic_no_items),
            title = stringResource(
                id = when (displayType) {
                    SecretItemType.ACCOUNT -> R.string.no_logins_stored_title
                    SecretItemType.NOTE -> R.string.no_notes_stored_title
                    SecretItemType.CREDIT_CARD -> R.string.no_credit_cards_stored_title
                    SecretItemType.WIFI -> R.string.no_wifi_stored_title
                    else -> R.string.no_item_stored_title
                }
            ),
            description = stringResource(
                id = when (displayType) {
                    SecretItemType.ACCOUNT -> R.string.no_logins_stored_desc
                    SecretItemType.NOTE -> R.string.no_notes_stored_desc
                    SecretItemType.CREDIT_CARD -> R.string.no_credit_cards_stored_desc
                    SecretItemType.WIFI -> R.string.no_wifi_stored_desc
                    else -> R.string.no_item_stored_desc
                }
            )
        )
        HelperNavigationArrow(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(start = 32.dp, end = 32.dp, bottom = 42.dp)
                .fillMaxSize()
                .weight(2f)
        )
    }
}

@Composable
private fun HelperNavigationArrow(modifier: Modifier) {
    Canvas(
        modifier = modifier
    ) {
        val strokeStyle = Stroke(width = 2.dp.toPx())
        val arrowLength = 8.dp.toPx()

        //create a path to draw on
        val arrowPath = Path()

        val arrowOval = Rect(this.size.width * 0.2f, 0f, this.size.width * 1.2f, this.size.height)

        //add the oval to path
        arrowPath.addArc(arrowOval, 90f, 160f)

        //draw arrowhead on path start
        arrowPath.moveTo(this.size.width * 0.7f, this.size.height)
        arrowPath.lineTo(this.size.width * 0.7f - arrowLength, this.size.height - arrowLength)
        arrowPath.moveTo(this.size.width * 0.7f, this.size.height)
        arrowPath.lineTo(this.size.width * 0.7f - arrowLength, this.size.height + arrowLength)

        drawPath(arrowPath, DarkBlue, style = strokeStyle)
    }
}

@Composable
@Preview(showBackground = true)
fun SecretItemListEmptyScreenPreview() {
    SecretItemListEmptyScreen(
        displayType = null,
    )
}
