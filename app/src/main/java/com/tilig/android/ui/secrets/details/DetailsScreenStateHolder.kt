package com.tilig.android.ui.secrets.details

import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.platform.SoftwareKeyboardController
import androidx.navigation.NavController
import com.tilig.android.MainRoute
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.utils.safeNavigate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.compose.get
import java.io.Serializable

data class DetailsScreenStateHolder @OptIn(
    ExperimentalMaterialApi::class,
    ExperimentalComposeUiApi::class
) constructor(
    val navigator: NavController,
    val scaffoldState: ScaffoldState,
    val modalBottomSheetState: ModalBottomSheetState,
    val keyboardController: SoftwareKeyboardController?,
    val currentBottomSheet: MutableState<BottomSheetType?>,
    val snackState: SnackbarHostState,
    val tracker: Mixpanel,
    val coroutineScope: CoroutineScope,
    val swipingState: SwipeableState<SwipingStates>,
    private val snackScope: CoroutineScope,
) : Serializable {

    fun navigateUp() {
        navigator.navigateUp()
    }

    @OptIn(ExperimentalComposeUiApi::class)
    fun openQRCodeScreen(accountId: String) {
        coroutineScope.launch {
            keyboardController?.hide()
            // workaround to make sure keyboard was closed, otherwise it's possible to get grey not interactive space,
            // on place where keyboard was
            delay(300)
            navigator.safeNavigate(
                MainRoute.ItemDetails.route,
                MainRoute.QRCodeScanner.createRouteForAccountId(accountId)
            )
        }
    }

    @OptIn(ExperimentalMaterialApi::class)
    private fun closeModalSheet() {
        currentBottomSheet.value = null
        coroutineScope.launch { modalBottomSheetState.animateTo(ModalBottomSheetValue.Hidden) }
    }

    @OptIn(ExperimentalComposeUiApi::class, ExperimentalMaterialApi::class)
    private fun openModalSheet(sheetType: BottomSheetType) {
        currentBottomSheet.value = sheetType
        keyboardController?.hide()
        coroutineScope.launch {
            modalBottomSheetState.animateTo(ModalBottomSheetValue.Expanded)
        }
    }

    fun updateBottomSheet(bottomSheetType: BottomSheetType?) {
        if (bottomSheetType == null) {
            closeModalSheet()
        } else {
            when (bottomSheetType) {
                is BottomSheetType.PasswordHistory -> {
                    tracker.trackEvent(Tracker.EVENT_VIEW_PASSWORD_HISTORY)
                }
                else -> {}
            }
            openModalSheet(bottomSheetType)
        }
    }

    fun showSnackBarMessage(message: String) {
        if (snackState.currentSnackbarData?.message != message) {
            snackState.currentSnackbarData?.dismiss()
            snackScope.launch { snackState.showSnackbar(message) }
        } else {

        }
    }

}

@OptIn(ExperimentalMaterialApi::class, ExperimentalComposeUiApi::class)
@Composable
fun rememberDetailsScreenStateHolder(
    navigator: NavController,
    scaffoldState: ScaffoldState = rememberScaffoldState(),
    coroutineScope: CoroutineScope = rememberCoroutineScope(),
    modalBottomSheetState: ModalBottomSheetState = rememberModalBottomSheetState(
        initialValue = ModalBottomSheetValue.Hidden,
        skipHalfExpanded = true
    ),
    keyboardController: SoftwareKeyboardController? = LocalSoftwareKeyboardController.current,
    snackState: SnackbarHostState = remember { SnackbarHostState() },
    snackScope: CoroutineScope = rememberCoroutineScope(),
    tracker: Mixpanel = get(),
    swipingState: SwipeableState<SwipingStates> = rememberSwipeableState(initialValue = SwipingStates.EXPANDED)
) = remember {
    DetailsScreenStateHolder(
        navigator = navigator,
        scaffoldState = scaffoldState,
        coroutineScope = coroutineScope,
        currentBottomSheet = mutableStateOf(null),
        modalBottomSheetState = modalBottomSheetState,
        keyboardController = keyboardController,
        snackState = snackState,
        snackScope = snackScope,
        swipingState = swipingState,
        tracker = tracker
    )
}
