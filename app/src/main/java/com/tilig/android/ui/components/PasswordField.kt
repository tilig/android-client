package com.tilig.android.ui.components

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.theme.DefaultIconColor
import com.tilig.android.ui.theme.Typography

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun PasswordField(
    modifier: Modifier = Modifier,
    hint: String,
    state: TextFieldState,
    tracker: Tracker?,
    showRevealButton: Boolean = false,
    onDone: (() -> Unit)? = null,
    onNext: (() -> Unit)? = null,
    onValueChange: ((String) -> Unit)? = null
) {
    var passwordVisibility by remember { mutableStateOf(false) }

    TextField(
        value = state.text,
        onValueChange = {
            state.text = it
            onValueChange?.invoke(it)},
        placeholder = { Text(hint, style = Typography.subtitle2) },
        singleLine = true,
        visualTransformation = if (passwordVisibility) VisualTransformation.None else PasswordVisualTransformation(),
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = Color.Transparent,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            errorIndicatorColor = Color.Transparent,
            errorTrailingIconColor = DefaultIconColor,
            trailingIconColor = DefaultIconColor
        ),
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Password,
            imeAction = if (onNext != null) ImeAction.Next else ImeAction.Done
        ),
        keyboardActions = KeyboardActions(
            onNext = {
                onNext?.invoke()
            },
            onDone = {
                onDone?.invoke()
            }
        ),
        trailingIcon = {
            if (showRevealButton) {
                val image = if (passwordVisibility)
                    R.drawable.ic_revealed
                else
                    R.drawable.ic_concealed

                IconButton(onClick = {
                    passwordVisibility = !passwordVisibility
                    if (passwordVisibility) {
                        tracker?.trackEvent(Tracker.EVENT_PASSWORD_REVEALED)
                    }
                }) {
                    Icon(
                        painter = painterResource(id = image),
                        contentDescription = stringResource(id = R.string.cd_reveal_password)
                    )
                }
            }
        },
        modifier = modifier
            .onFocusChanged { focusState ->
                state.onFocusChange(focusState.isFocused)
                passwordVisibility = focusState.isFocused
            },
        isError = state.showErrors(),
    )
}

@OptIn(ExperimentalFoundationApi::class)
@Preview(showBackground = true)
@Composable
fun PasswordFieldPreview() {
    PasswordField(
        hint = "Lorem ipsum",
        state = TextFieldState(fieldValue = "Lipsum"),
        tracker = null
    )
}
