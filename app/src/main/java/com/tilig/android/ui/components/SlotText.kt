package com.tilig.android.ui.components

import androidx.compose.animation.*
import androidx.compose.foundation.layout.Row
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import com.tilig.android.ui.components.CharChangeWheel.WheelListener
import com.tilig.android.ui.theme.Typography
import io.sentry.Sentry
import io.sentry.SentryLevel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


private val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9') + ('-')
private const val DURATION_STEP = 70L

@Composable
@Preview
fun SlotTextPreview() {
    SlotText("Text", textStyle = Typography.h1)
}

// Have characters build up from left to right until you have all.
// And keep them rotating. Start with nothing.
// Perhaps the speed should increase from left to right.
@Composable
fun SlotText(
    text: String,
    modifier: Modifier = Modifier,
    textStyle: TextStyle,
    onSlottingEnd: (() -> Unit)? = null
) {
    val charAnimatedArray = remember { mutableStateOf(emptyList<Char>()) }
    val coroutineScope = rememberCoroutineScope()
    val listWheels = remember { mutableListOf<CharChangeWheel>() }

    LaunchedEffect(Unit) {
        for (i in 1..text.length) {
            val wheel = CharChangeWheel(
                lastChar = text[i - 1],
                isLastCharInText = i == text.length,
                wheelListener = object : WheelListener {
                    override fun newCharValue(char: Char) {
                        val old = charAnimatedArray.value.toMutableList()
                        try {
                            if (i < old.size + 1) {
                                old[i - 1] = char
                            } else {
                                old.add(char)
                            }
                            charAnimatedArray.value = old
                        } catch (e: IndexOutOfBoundsException) {
                            // Doing a bunch of catches here like a brute, because there
                            // are multiple, relatively harmless, strange, timing-related bugs happening here.

                            // The first being and IndexOutOfBounds (index 2, size 1) on a list that is perfectly long enough,
                            Sentry.captureMessage(
                                "Index $i out of bounds of ${old.size}",
                                SentryLevel.INFO
                            )
                        } catch (e: IllegalStateException) {
                            // the second begin an IllegalStateException "Reading a state that was
                            // created after the snapshot was taken or in a snapshot that has not
                            // yet been applied" when assigning the new value to charAnimatedArray.
                            Sentry.captureMessage(
                                e.message ?: "Reading state failure in SlotText",
                                SentryLevel.INFO
                            )
                        }
                    }

                    override fun onEndChangingChar(isLastPosition: Boolean) {
                        if (isLastPosition) {
                            coroutineScope.launch {
                                delay(1000)
                                onSlottingEnd?.invoke()
                            }
                        }
                    }
                },
                duration = DURATION_STEP * text.length,
                delay = DURATION_STEP * i
            )

            listWheels.add(wheel)
        }
        listWheels.forEach {
            it.start()
        }
    }

    Results(modifier = modifier, textStyle = textStyle, charAnimatedArray.value)
}

/**
 * @param wheelListener listener of wheel updates
 * @param duration time between chars changing
 * @param delay delay before start char changing
 * @param isLastCharInText indicates that this char wheel is the last
 * @param lastChar the last char that should be displayed in the end of the wheel
 */
private class CharChangeWheel(
    private val wheelListener: WheelListener?,
    private val duration: Long,
    private val delay: Long,
    val isLastCharInText: Boolean = false,
    private val lastChar: Char
) : Thread() {

    interface WheelListener {
        fun newCharValue(char: Char)
        fun onEndChangingChar(isLastPosition: Boolean)
    }

    private var currentChar: Char = ' '
    private var isStarted = true
    private var playedDuration = 0L

    private fun nextValue() {
        currentChar = charPool[kotlin.random.Random.nextInt(0, charPool.size)]
    }

    override fun run() {
        try {
            sleep(delay)
        } catch (e: InterruptedException) {
        }
        while (isStarted) {
            val trans = calculateTransformation(duration, playedDuration)
            try {
                sleep(trans)
            } catch (e: InterruptedException) {
            }
            nextValue()
            wheelListener?.newCharValue(currentChar)

            if (playedDuration > duration) {
                wheelListener?.newCharValue(lastChar)
                stopWheel()
            } else {
                playedDuration += trans
            }
        }
    }

    private fun stopWheel() {
        isStarted = false
        wheelListener?.onEndChangingChar(isLastCharInText)
    }

    private fun calculateTransformation(duration: Long, playedTime: Long): Long {
        val rawFraction =
            if (duration == 0L) 1f else (playedTime).coerceIn(100, duration) / duration.toFloat()
        return (300 * rawFraction.coerceIn(0.2f, 1f)).toLong()
    }

}

@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun Results(modifier: Modifier, textStyle: TextStyle, charAnimatedArray: List<Char>) {
    Row(modifier = modifier, verticalAlignment = Alignment.CenterVertically) {
        charAnimatedArray.forEach { c ->
            AnimatedContent(targetState = c,
                transitionSpec = {
                    (
                            slideInVertically { -it / 2 }
                                    + fadeIn()
                                    with slideOutVertically { it / 2 }
                                    + fadeOut()
                            )
                        .using(
                            // Disable clipping since the faded slide-in/out should
                            // be displayed out of bounds.
                            SizeTransform(clip = false)
                        )
                }
            ) {
                Text(text = it.toString(), style = textStyle)
            }
        }
    }
}
