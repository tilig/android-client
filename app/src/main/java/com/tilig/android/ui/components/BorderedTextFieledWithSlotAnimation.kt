package com.tilig.android.ui.components

import android.annotation.SuppressLint
import androidx.annotation.DrawableRes
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tilig.android.ui.theme.*

/**
 * Input TextField with a border around it, an optional copy-to-clipboard button,
 * and support for password ******'s.
 * @param state
 * @param leadingIcon If not null display icon on the left side
 * @param leadingIconModifier modifier for the leading icon
 */
@ExperimentalFoundationApi
@Composable
fun BorderedTextWithSlotAnimation(
    text: String,
    fontSize: TextUnit = 15.sp,
    @DrawableRes leadingIcon: Int? = null,
    @SuppressLint("ModifierParameter")
    textFieldModifier: Modifier = Modifier,
) {

    val rowModifier = Modifier.height(52.dp)

    Row(
        modifier = rowModifier
    ) {

        val shape = RoundedCornerShape(8.dp)
        val borderColor = SearchBarBorder
        Row(
            modifier = Modifier
                .border(1.dp, color = borderColor, shape = shape)
                .background(
                    TiligYellow2,
                    shape = shape
                )
                .weight(3f)
                .fillMaxHeight()
        ) {
            leadingIcon?.let { icon ->
                Icon(
                    painter = painterResource(id = icon),
                    contentDescription = "",
                    modifier = Modifier
                        .padding(start = 10.dp)
                        .align(Alignment.CenterVertically)
                )
            }
            Box(
                modifier = Modifier
                    .fillMaxSize(),
                contentAlignment = Alignment.CenterStart
            ) {
                val textStyle = TextStyle(
                    fontWeight = FontWeight.Normal,
                    fontSize = fontSize,
                    letterSpacing = 0.15.sp,
                    color = DefaultTextColor,
                    fontFamily = StolzlBook
                )
                SlotText(
                    text = text, textStyle = textStyle, modifier = textFieldModifier
                        .fillMaxSize()
                        .padding(start = DefaultPadding)
                        .align(Alignment.Center)
                )
            }
        }
    }
}

@ExperimentalFoundationApi
@Composable
@Preview
fun BorderedTextFieldWithSlotAnimationPreview() {
    BorderedTextWithSlotAnimation(
        text = "Lorem Ipsum"
    )
}