package com.tilig.android.ui.onboarding.survey

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Velocity
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.PagerState


object SurveyPages {
    const val SURVEY_PAGES = 4
    const val SURVEY_FIRST_QUESTION = 0
    const val SURVEY_SECOND_QUESTION = 1
    const val SURVEY_THIRD_QUESTION = 2
    const val SURVEY_FINISHED = 3
}

@ExperimentalAnimationApi
@ExperimentalPagerApi
@Composable
fun SurveyPager(
    pagerState: PagerState,
    modifier: Modifier,
    viewModel: SurveyViewModel
) {
    HorizontalPager(
        modifier = modifier.disabledHorizontalPointerInputScroll(),
        count = SurveyPages.SURVEY_PAGES,
        state = pagerState,
        verticalAlignment = Alignment.Top,
        // to prevent scroll on other pages where it's not needed
        itemSpacing = 1.dp,
    ) { position ->
        when (position) {
            SurveyPages.SURVEY_FIRST_QUESTION -> SurveyFirstQuestion(viewModel)
            SurveyPages.SURVEY_SECOND_QUESTION -> SurveySecondQuestion(viewModel)
            SurveyPages.SURVEY_THIRD_QUESTION -> SurveyThirdQuestion(viewModel)
            SurveyPages.SURVEY_FINISHED -> SurveyFinished(viewModel)
        }
    }
}

private val HorizontalScrollConsumer = object : NestedScrollConnection {
    override fun onPreScroll(available: Offset, source: NestedScrollSource) = available.copy(y = 0f)
    override suspend fun onPreFling(available: Velocity) = available.copy(y = 0f)
}

fun Modifier.disabledHorizontalPointerInputScroll(disabled: Boolean = true) =
    if (disabled) this.nestedScroll(HorizontalScrollConsumer) else this

@ExperimentalAnimationApi
@ExperimentalPagerApi
@Composable
@Preview
fun SurveyPagerPreview() {
    SurveyPager(
        PagerState(0),
        Modifier,
        viewModel = viewModel()
    )
}