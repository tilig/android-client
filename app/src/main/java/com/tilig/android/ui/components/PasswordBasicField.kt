package com.tilig.android.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.theme.DefaultPadding
import com.tilig.android.ui.theme.Typography
import com.tilig.android.utils.UITestTags

@Composable
fun PasswordBasicField(
    modifier: Modifier = Modifier,
    textStyle: TextStyle = Typography.body1,
    hint: String,
    state: TextFieldState,
    tracker: Tracker? = null,
    singleLine: Boolean = true,
    showRevealButton: Boolean = false,
    showPasswordInitially: Boolean = false,
    readOnly: Boolean = false,
    contentHorizontalPadding: Dp = DefaultPadding,
    onDone: (() -> Unit)? = null,
    onNext: (() -> Unit)? = null,
    onValueChange: ((String) -> Unit)? = null
) {
    var passwordVisibility by remember { mutableStateOf(showPasswordInitially) }
    var isPasswordVisibleInitially by remember { mutableStateOf(showPasswordInitially) }

    Row(modifier = modifier, verticalAlignment = Alignment.CenterVertically) {
        BasicTextField(
            value = state.text,
            textStyle = textStyle,
            onValueChange = {
                state.text = it
                onValueChange?.invoke(it)
            },
            decorationBox = { innerTextField ->
                if (state.text.isEmpty()) {
                    Text(hint, maxLines = 1, style = Typography.subtitle2)
                }
                innerTextField()
            },
            singleLine = singleLine,
            readOnly = readOnly,
            enabled = !readOnly,
            visualTransformation = if (passwordVisibility) VisualTransformation.None else PasswordVisualTransformation(),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password,
                imeAction = if (onNext != null) ImeAction.Next else ImeAction.Done
            ),
            keyboardActions = KeyboardActions(
                onNext = {
                    onNext?.invoke()
                },
                onDone = {
                    onDone?.invoke()
                }
            ),
            modifier = Modifier
                .testTag(UITestTags.passwordInputFieldTestTag)
                .padding(horizontal = contentHorizontalPadding)
                .weight(1f)
                .padding(end = 8.dp)
                .onFocusChanged { focusState ->
                    state.onFocusChange(focusState.isFocused)
                    passwordVisibility = focusState.isFocused || isPasswordVisibleInitially
                    isPasswordVisibleInitially = false
                },
        )
        if (showRevealButton) {
            val image = if (passwordVisibility)
                R.drawable.ic_revealed
            else
                R.drawable.ic_concealed

            val action = {
                passwordVisibility = !passwordVisibility
                if (passwordVisibility) {
                    tracker?.trackEvent(Tracker.EVENT_PASSWORD_REVEALED)
                }
            }
            Icon(
                modifier = Modifier
                    .clickable { action.invoke() }
                    .size(24.dp),
                painter = painterResource(id = image),
                contentDescription = stringResource(id = R.string.cd_reveal_password)
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PasswordBasicFieldPreview() {
    PasswordBasicField(
        hint = "Lorem ipsum",
        state = TextFieldState(fieldValue = "Lipsum"),
        tracker = null
    )
}
