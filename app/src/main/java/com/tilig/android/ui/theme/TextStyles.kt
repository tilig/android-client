package com.tilig.android.ui.theme

import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp

val StyleStolzRegularDarkBlue14 = TextStyle(
    fontFamily = Stolzl,
    fontWeight = FontWeight.Normal,
    fontSize = FontSizeDefault,
    color = DarkBlue,
    lineHeight = 20.sp
)

val StyleStolzBookDarkBlue12 = TextStyle(
    fontFamily = StolzlBook,
    fontWeight = FontWeight.Normal,
    fontSize = FontSizeSmall,
    color = DarkBlue,
    lineHeight = 16.sp
)

val StyleStolzBookDarkBlue14 = StyleStolzBookDarkBlue12.copy(
    fontSize = 14.sp,
    lineHeight = 24.sp
)

val StyleStolzRegularBookDarkBlue13 = StyleStolzRegularDarkBlue14.copy(
    fontSize = 13.sp,
    lineHeight = 16.sp
)

val StyleStolzBookDarkBlue16 = StyleStolzBookDarkBlue12.copy(
    fontSize = 16.sp,
    lineHeight = 24.sp
)

val StyleStolzMediumDarkBlue20 = TextStyle(
    fontFamily = Stolzl,
    fontWeight = FontWeight.Medium,
    fontSize = 20.sp,
    color = DarkBlue,
    lineHeight = 28.sp
)

val StyleRobotoMonoMediumDarkBlue16 = TextStyle(
    fontFamily = RobotoMono,
    fontWeight = FontWeight.Medium,
    fontSize = 16.sp,
    color = DarkBlue,
)

val StyleSpoofMediumBlue24 = TextStyle(
    fontFamily = Spoof,
    fontWeight = FontWeight.Medium,
    fontSize = 24.sp,
    color = DarkBlue,
)

val StyleSpoofMediumDarkBlue20 = StyleSpoofMediumBlue24.copy(fontSize = 20.sp)

val StyleSpoofBoldDarkBlue28 = TextStyle(
    fontFamily = Spoof,
    fontWeight = FontWeight.Bold,
    fontSize = 28.sp,
    color = DarkBlue,
    lineHeight = 32.sp
)

val StyleSpoofBoldDarkBlue24 = StyleSpoofBoldDarkBlue28.copy(fontSize = 24.sp, lineHeight = 36.sp)

val StyleStolzRegularDarkBlue12 =
    StyleStolzRegularDarkBlue14.copy(fontSize = 12.sp, lineHeight = 16.sp)

val StyleStolzRegularNeutral14 = StyleStolzRegularDarkBlue14.copy(color = Neutral40)

val StyleStolzRegularDarkBlue16 = StyleStolzRegularDarkBlue14.copy(fontSize = 16.sp, lineHeight = 24.sp)

val StyleStolzRegularNeutral16 = StyleStolzRegularDarkBlue14.copy(color = Neutral40, fontSize = 16.sp, lineHeight = 24.sp)

val StyleStolzRegularWeblink = StyleStolzRegularDarkBlue14.copy(fontSize = 16.sp, lineHeight = 24.sp, color = WeblinkColor)

val StylePasswordText = StyleRobotoMonoMediumDarkBlue16

val StyleStolzMediumDarkBlue14 = TextStyle(
    fontFamily = Stolzl,
    fontWeight = FontWeight.Medium,
    fontSize = 14.sp,
    color = DarkBlue,
    lineHeight = 24.sp
)

val StyleStolzMediumDarkBlue16 = StyleStolzMediumDarkBlue14.copy(color = DarkBlue, fontSize = 16.sp, lineHeight = 24.sp)

