package com.tilig.android.ui.notes.details

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.data.models.tilig.CustomField
import com.tilig.android.ui.components.*
import com.tilig.android.ui.notes.details.holders.EditNoteStateHolder
import com.tilig.android.ui.secrets.add.AddToFolderComponent
import com.tilig.android.ui.secrets.components.*
import com.tilig.android.ui.secrets.details.ItemDetailsCallback
import com.tilig.android.ui.secrets.details.events.ItemDetailsActionEvent
import com.tilig.android.ui.secrets.details.events.BaseEditEvent
import com.tilig.android.ui.theme.*

@Composable
fun NoteEditContent(
    displayUnsavedAlert: Boolean,
    state: EditNoteStateHolder,
    modifier: Modifier = Modifier,
    callback: ItemDetailsCallback
) {

    val focusManager = LocalFocusManager.current

    val openDialog = remember { mutableStateOf(false) }

    Column(
        modifier = modifier
            .fillMaxWidth()
            .padding(horizontal = DefaultPadding)
    ) {
        TiligSpacerVerticalDefault()
        Box(modifier = modifier) {
            BorderedBasicTextField(
                modifier = Modifier.padding(top = if (state.nameState.isValid) DefaultErrorMessageHeight else 0.dp),
                textStyle = StyleStolzRegularDarkBlue16,
                errorHandler = ErrorHandler(
                    errorMessage = stringResource(id = R.string.error_name),
                    errorColor = DefaultErrorColor
                ),
                singleLine = false,
                hint = stringResource(id = R.string.hint_note_name),
                state = state.nameState,
                textModifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 12.dp, vertical = 4.dp),
                keyboardType = KeyboardType.Text,
                capitalization = KeyboardCapitalization.Sentences,
                onNext = {
                    focusManager.moveFocus(FocusDirection.Down)
                },
                onValueChanged = {
                    callback.onActionEvent(ItemDetailsActionEvent.Edit(NoteEditEvents.NameChanged(state.nameState.text)))
                }
            )
            Text(
                text = stringResource(id = R.string.label_edit_note_name),
                modifier = Modifier,
                style = StyleStolzRegularDarkBlue12.copy(color = DarkBlue.copy(alpha = 0.75f))
            )
        }

        TiligSpacerVerticalDefault()
        NoteViewEdit(
            modifier = Modifier.fillMaxWidth(),
            notesState = state.contentState,
            title = stringResource(id = R.string.label_note_content),
            contentHintRes = R.string.hint_note_extra_info,
            contentAlignment = Alignment.TopStart,
            onValueChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(NoteEditEvents.ContentChanged(state.contentState.text)))
            }
        )

        AllCustomFieldsEditComponent(focusManager = focusManager,
            fieldsWrapper = state.customFields,
            onFieldsChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(BaseEditEvent.CustomFieldsChanged(state.customFields.map {
                    CustomField(
                        it.labelFieldState.text,
                        it.customField.kind,
                        it.textFieldState.text
                    )
                })))
            },
            onDeleteField = {
                val updated = state.customFields.toMutableList()
                updated.remove(it)
                callback.onActionEvent(
                    ItemDetailsActionEvent.Edit(
                        BaseEditEvent.CustomFieldsChanged(
                            updated.map {
                                CustomField(
                                    it.labelFieldState.text,
                                    it.customField.kind,
                                    it.textFieldState.text
                                )
                            })
                    )
                )
            }
        )

        TiligSpacerVerticalDefault()
        AddCustomField(modifier = Modifier.fillMaxWidth()) {
            callback.onActionEvent(ItemDetailsActionEvent.Edit(BaseEditEvent.AddCustomField(it)))
        }

        TiligSpacerVerticalDefault()
        AddToFolderComponent(
            modifier = Modifier,
            folders = state.foldersList,
            selectedFolder = state.selectedFolder,
            onFolderChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(BaseEditEvent.ChangeFolder(it)))
            }
        )

        Spacer(modifier = Modifier.height(56.dp))
        DeleteSecretItemView(
            titleId = R.string.bt_delete_note,
            dialogDescriptionId = R.string.dialog_delete_item,
            openDialog = openDialog
        ) {
            callback.onActionEvent(ItemDetailsActionEvent.DeleteItem)
        }
    }

    if (displayUnsavedAlert) {
        TiligDialog(
            title = R.string.alert_unsaved_changes,
            text = R.string.alert_unsaved_changes_desc,
            onCancel = {
                callback.onActionEvent(ItemDetailsActionEvent.CancelChangesDialog)
            },
            onDismissRequest = {},
            confirmButtonText = R.string.alert_unsaved_changes_discard,
            cancelButtonText = R.string.alert_unsaved_changes_confirm,
            onConfirm = { callback.onActionEvent(ItemDetailsActionEvent.ConfirmDiscardChanges) })
    }

    BackHandler {
        callback.onActionEvent(ItemDetailsActionEvent.OnGoBack)
    }
}