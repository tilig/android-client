package com.tilig.android.ui.twofa.bottomsheets.securitycode

import android.os.CountDownTimer
import android.util.Log
import androidx.lifecycle.ViewModel
import com.tilig.android.data.qrscanner.MalformedTokenException
import com.tilig.android.data.qrscanner.Token
import com.tilig.android.ui.twofa.bottomsheets.securitycode.CountDownFormatUtils.formatTime
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.util.*
import java.util.concurrent.TimeUnit

object CountDownFormatUtils {

    // time to countdown - 30 secs
    const val TIME_COUNTDOWN: Long = 30000L
    private const val TIME_FORMAT = "%d"

    // convert time to milli seconds
    fun Long.formatTime(): String = String.format(
        TIME_FORMAT,
        TimeUnit.MILLISECONDS.toSeconds(this) % 60
    )

    /**
     * The 2fa code is based on the current time, and changes every 30 seconds.
     * It should update at every 0 and every 30 seconds on the clock, independently of when you open the screen.
     */
    fun getInitialTimeCountDown(): Long {
        val currentSecond = Calendar.getInstance().get(Calendar.SECOND)
        return TIME_COUNTDOWN - (if (currentSecond > 30) currentSecond - 30 else currentSecond) * 1000L
    }
}

class SecurityCodeViewModel : ViewModel() {

    private var token: Token? = null

    private var countDownTimer: CountDownTimer? = null

    private val _time =
        MutableStateFlow(CountDownFormatUtils.getInitialTimeCountDown().formatTime())
    val time: StateFlow<String> = _time

    private val _progress = MutableStateFlow<Float?>(null)
    val progress: StateFlow<Float?> = _progress

    private val _isPlaying = MutableStateFlow(false)

    private val _code = MutableStateFlow<String?>(null)
    val code: StateFlow<String?> get() = _code

    init {
        handleCountDownTimer()
    }

    fun setToken(tokenUri: String) {
        try {
            token = Token.parse(tokenUri)
            updateCode(token!!.code)
        } catch (e: MalformedTokenException) {
            Log.e("Token", "Malformed token: $tokenUri, error: ${e.message}")
            _code.value = null
        }
    }

    private fun handleCountDownTimer() {
        if (_isPlaying.value) {
            pauseTimer()
        } else {
            startTimer()
        }
    }

    private fun pauseTimer() {
        countDownTimer?.cancel()
        handleTimerValues(false, CountDownFormatUtils.getInitialTimeCountDown().formatTime(), 1.0F)
    }

    private fun startTimer() {
        _isPlaying.value = true
        countDownTimer =
            object : CountDownTimer(CountDownFormatUtils.getInitialTimeCountDown(), 1000) {

                override fun onTick(millisRemaining: Long) {
                    val progressValue =
                        millisRemaining.toFloat() / CountDownFormatUtils.TIME_COUNTDOWN
                    handleTimerValues(true, millisRemaining.formatTime(), progressValue)
                }

                override fun onFinish() {
                    pauseTimer()
                    token?.let {
                        it.increment()
                        updateCode(it.code)
                        _isPlaying.value = false
                        startTimer()
                    }
                }
            }.start()
    }

    private fun handleTimerValues(
        isPlaying: Boolean,
        text: String,
        progress: Float,
    ) {
        _isPlaying.value = isPlaying
        _time.value = text
        _progress.value = progress
    }

    private fun updateCode(code: String) {
        val symbols = DecimalFormatSymbols(Locale.getDefault())
        symbols.groupingSeparator = ' '
        val nf: NumberFormat? =
            when (code.length) {
                6 -> DecimalFormat("000,000", symbols)
                8 -> DecimalFormat("0000,0000", symbols)
                else -> null
            }
        _code.value = if (nf != null) {
            nf.format(code.toLong())
        } else {
            code
        }
    }
}
