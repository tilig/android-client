package com.tilig.android.ui.accounts.detail.components

import SingleLetterIcon
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawWithCache
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.tilig.android.R
import com.tilig.android.TiligApp
import com.tilig.android.data.models.UiError
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.Membership
import com.tilig.android.data.models.tilig.SecretItem
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.components.BorderedBasicTextField
import com.tilig.android.ui.components.TiligSpacerHorizontalExtraSmall
import com.tilig.android.ui.components.TiligSpacerVerticalDefault
import com.tilig.android.ui.modifiers.dropShadow
import com.tilig.android.ui.secrets.components.FolderComponent
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.copyToClipboard

interface ShareLinkComponentCallback {
    fun onCreateShareLink(){}
    fun onDeleteShareUrl(){}
    fun onLinkCopied(){}
    fun onRevokeAccess(member: Membership)
    fun onOpenShareScreen()
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun ShareLinkComponent(
    modifier: Modifier = Modifier,
    item: SecretItem,
    folder: Folder?,
    isActionEnabled: Boolean,
    shareErrorMessage: UiError?,
    callback: ShareLinkComponentCallback
) {
    val revokeAccesssForMemberId = rememberSaveable { mutableStateOf<Membership?>(null) }

    if (folder != null && !folder.isSharedWithIndividuals) {
        FolderComponent(modifier = Modifier, folder = folder, itemTypeMarkerRes = R.string.note_small)
        TiligSpacerVerticalDefault()
    }

    if (folder == null || folder.isSharedWithIndividuals) {
        Box(
            modifier = modifier
                .fillMaxWidth()
                .background(
                    color = White,
                    shape = RoundedCornerShape(MediumCornerRadius)
                )
        ) {
            Column(modifier = Modifier.padding(DefaultPadding)) {

                Row(modifier = Modifier.fillMaxWidth()) {
                    Column(modifier = Modifier.weight(1f)) {
                        Text(
                            stringResource(id = R.string.share_this_login),
                            style = StyleSpoofMediumDarkBlue20,
                        )
                        Text(
                            modifier = Modifier,
                            text = stringResource(id = R.string.share_link_desc, item.name?:""),
                            style = StyleStolzRegularBookDarkBlue13,
                            color = Neutral40
                        )
                    }
                    ShareProfileIconsComponent()
                }


                val emailFieldState = remember { TextFieldState("") }
                val keyboardController = LocalSoftwareKeyboardController.current

                Row(
                    modifier = Modifier
                        .padding(top = DefaultPadding)
                        .fillMaxWidth()
                        .clickable {
                            TiligApp.debounceClicks {
                                keyboardController?.hide()
                                callback.onOpenShareScreen()
                            }
                        }
                ) {
                    BorderedBasicTextField(
                        modifier = Modifier
                            .weight(1f)
                            .height(48.dp),
                        textStyle = StyleStolzRegularBookDarkBlue13,
                        defaultShape = RoundedCornerShape(topStart = 8.dp, bottomStart = 8.dp),
                        hint = stringResource(id = R.string.share_with_email_hint),
                        state = emailFieldState,
                        singleLine = true,
                        textModifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 12.dp, vertical = 4.dp),
                        keyboardType = KeyboardType.Email,
                        enabled = false,
                        onValueChanged = {},
                    )
                    TextButton(
                        modifier = Modifier
                            .height(48.dp)
                            .dropShadow(enabled = false)
                            .background(
                                shape = RoundedCornerShape(topEnd = 8.dp, bottomEnd = 8.dp),
                                color = Color.Transparent
                            )
                            .clip(RoundedCornerShape(topEnd = 8.dp, bottomEnd = 8.dp)),
                        shape = RoundedCornerShape(topEnd = 8.dp, bottomEnd = 8.dp),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = LightBlue,
                            disabledBackgroundColor = LightBlue.copy(alpha = 0.5f)
                        ),
                        contentPadding = PaddingValues(0.dp),
                        elevation = null,
                        onClick = {
                            TiligApp.debounceClicks {
                                keyboardController?.hide()
                                callback.onOpenShareScreen()
                            }
                        },
                    ) {
                        Text(
                            text = stringResource(id = R.string.bt_share),
                            style = StyleStolzRegularBookDarkBlue13,
                            color = White
                        )
                    }
                }

                shareErrorMessage?.let {
                    ErrorBox(
                        shareErrorMessage
                    )
                }

                SharedWithMembersListView(folder) { membership ->
                    revokeAccesssForMemberId.value = membership
                }

                if (item is Account) {
                    Divider(
                        modifier = Modifier
                            .padding(vertical = DefaultPadding)
                            .fillMaxWidth()
                            .height(1.dp), color = DefaultBorderColor
                    )

                    MagicShareLinkComponent(
                        item = item,
                        isActionEnabled = isActionEnabled,
                        onDeleteShareUrl = { callback.onDeleteShareUrl() },
                        onLinkCopied = { callback.onLinkCopied() },
                        onCreateShareLink = { callback.onCreateShareLink() })
                }
            }
        }
    } else if (item is Account) {
        Box(
            modifier = modifier
                .fillMaxWidth()
                .background(
                    color = White,
                    shape = RoundedCornerShape(MediumCornerRadius)
                )
        ) {
            Column(modifier = Modifier.padding(DefaultPadding)) {
                MagicShareLinkComponent(
                    item = item,
                    isActionEnabled = isActionEnabled,
                    onDeleteShareUrl = { callback.onDeleteShareUrl() },
                    onLinkCopied = { callback.onLinkCopied() },
                    onCreateShareLink = { callback.onCreateShareLink() })
            }
        }
    }

    revokeAccesssForMemberId.value?.let { membership ->
        ShareRevokeAccessDialog(
            email = membership.user.getADisplayName(),
            accountName = item.name?:"",
            onCancel = {
                revokeAccesssForMemberId.value = null
            },
            onConfirm = {
                callback.onRevokeAccess(membership)
                revokeAccesssForMemberId.value = null
            }
        )
    }
}

@Composable
fun ErrorBox(errorMessage: UiError) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 8.dp)
            .background(color = Pink, shape = RoundedCornerShape(12.dp))
            .padding(vertical = 2.dp, horizontal = 8.dp)
    ) {
        Image(
            modifier = Modifier
                .padding(start = HalfPadding, top = HalfPadding, bottom = HalfPadding, end = 0.dp)
                .size(22.dp),
            painter = painterResource(id = R.drawable.ic_warning),
            contentDescription = null,
        )
        Text(
            modifier = Modifier.padding(HalfPadding),
            style = StyleStolzRegularDarkBlue12.copy(color = Black),
            text = if (errorMessage.msgResource != null)
                stringResource(id = errorMessage.msgResource)
            else
                errorMessage.message ?: ""
        )
    }
}

@Composable
private fun SharedWithMembersListView(
    folder: Folder?,
    onRevokeAccess: (Membership) -> Unit
) {
    folder?.let { it ->
        if ((it.getAllMemberships().size) > 1) {
            Text(
                modifier = Modifier.padding(top = DefaultPadding),
                text = stringResource(id = R.string.shared_with).uppercase(),
                style = StyleStolzRegularDarkBlue12.copy(fontSize = 10.sp, color = Neutral30)
            )
        }
        it.getAllMemberships().forEach { membership ->
            if (membership.isMe != true) {
                SharedWithMemberRow(membership = membership, onRevokeAccess = onRevokeAccess)
            }
        }
    }
}

@Composable
private fun SharedWithMemberRow(membership: Membership, onRevokeAccess: (Membership) -> Unit) {
    val email = membership.user.email
    val isPending = membership.isPending
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = HalfPadding)
    ) {
        if (membership.user.picture != null) {
            Image(
                painter = rememberAsyncImagePainter(membership.user.picture),
                contentDescription = null,
                modifier = Modifier
                    .size(32.dp)
                    .background(color = White, shape = CircleShape)
                    .clip(CircleShape)
            )
        } else {
            SingleLetterIcon(
                text = membership.user.getADisplayName(),
                backgroundColor = LightBlue
            )
        }

        Column(
            modifier = Modifier
                .padding(start = HalfPadding)
                .weight(1f)
                .align(Alignment.CenterVertically),
            horizontalAlignment = Alignment.Start,
        ) {
            Text(
                modifier = Modifier,
                text = email,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = StyleStolzRegularBookDarkBlue13
            )
            if (isPending == true) {
                Text(
                    modifier = Modifier
                        .background(color = Blue4, shape = RoundedCornerShape(12.dp))
                        .padding(vertical = 2.dp, horizontal = 8.dp),
                    style = StyleStolzRegularDarkBlue12.copy(color = LightBlue),
                    text = stringResource(id = R.string.status_pending)
                )
            }
        }

        IconButton(
            modifier = Modifier
                .size(22.dp)
                .align(Alignment.CenterVertically),
            onClick = {
                TiligApp.debounceClicks {
                    onRevokeAccess(membership)
                }
            }) {
            Image(
                modifier = Modifier.size(22.dp),
                painter = painterResource(id = R.drawable.ic_revoke_access_red),
                contentDescription = null,
            )
        }
    }
}

@Composable
private fun MagicShareLinkComponent(
    item: SecretItem,
    isActionEnabled: Boolean,
    onDeleteShareUrl: () -> Unit,
    onLinkCopied: () -> Unit,
    onCreateShareLink: () -> Unit
) {
    val shareLink = item.share?.getFullShareLink()

    if (shareLink == null) {
        Text(
            modifier = Modifier,
            text = stringResource(id = R.string.share_link_magic_desc),
            style = StyleStolzRegularDarkBlue12,
            color = Neutral40
        )
        TextButton(
            modifier = Modifier
                .wrapContentHeight()
                .background(
                    shape = RoundedCornerShape(8.dp),
                    color = Color.Transparent
                )
                .clip(RoundedCornerShape(8.dp)),
            shape = RoundedCornerShape(8.dp),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = White,
                disabledBackgroundColor = DisabledGrey
            ),
            border = BorderStroke(1.dp, LightBlue),
            contentPadding = PaddingValues(vertical = 6.dp, horizontal = HalfPadding),
            onClick = {
                TiligApp.debounceClicks { onCreateShareLink.invoke() }
            }
        ) {
            Row(
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .padding(horizontal = 8.dp)
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_share_link),
                    contentDescription = null,
                    tint = LightBlue
                )
                Text(
                    text = stringResource(id = R.string.create_share_link),
                    style = StyleStolzRegularBookDarkBlue13,
                    color = LightBlue
                )
            }
        }
    } else {
        Text(
            modifier = Modifier,
            text = stringResource(id = R.string.share_link).uppercase(),
            style = StyleStolzRegularDarkBlue12.copy(fontSize = 10.sp),
            color = Neutral30
        )
        GeneratedMagicLink(
            url = shareLink,
            isActionEnabled = isActionEnabled,
            onDeleteShareUrl = onDeleteShareUrl,
            onLinkCopied = onLinkCopied
        )
    }
}

@Composable
private fun GeneratedMagicLink(
    url: String,
    isActionEnabled: Boolean,
    onDeleteShareUrl: () -> Unit,
    onLinkCopied: () -> Unit
) {
    GeneratedShareLinkView(url, onLinkCopied)
    Row(
        modifier = Modifier.then(
            if (isActionEnabled) {
                Modifier.clickable { onDeleteShareUrl.invoke() }
            } else {
                Modifier
            }
                .height(16.dp)
        ),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Image(
            modifier = Modifier.size(16.dp),
            painter = painterResource(id = R.drawable.ic_trash),
            contentDescription = stringResource(id = R.string.delete_share_link),
        )
        TiligSpacerHorizontalExtraSmall()
        Text(
            text = stringResource(id = R.string.delete_share_link),
            style = StyleStolzRegularDarkBlue12,
            color = SettingsWarnColor,
            textAlign = TextAlign.Center
        )
    }
}

@Composable
private fun GeneratedShareLinkView(url: String, onLinkCopied: () -> Unit) {
    val context = LocalContext.current
    Row(
        modifier = Modifier
            .padding(vertical = HalfPadding)
            .fillMaxWidth()
            .height(48.dp)
            .border(
                width = DefaultSmallBorder,
                color = DefaultBorderColor,
                shape = RoundedCornerShape(8.dp)
            )
            .background(
                White,
                shape = RoundedCornerShape(8.dp)
            )
            .padding(horizontal = HalfPadding)
            .clickable {
                url.copyToClipboard(context)
                onLinkCopied.invoke()
            },
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_share_link),
            contentDescription = "",
            tint = DarkBlue,
            modifier = Modifier
                .padding(end = HalfPadding)
                .align(Alignment.CenterVertically)
                .size(16.dp)
        )
        Text(
            modifier = Modifier
                .weight(1f)
                .align(Alignment.CenterVertically)
                .graphicsLayer(alpha = 0.99f)
                .drawWithCache {
                    val brush = Brush.horizontalGradient(listOf(DarkBlue, Grey))
                    onDrawWithContent {
                        drawContent()
                        drawRect(brush, blendMode = BlendMode.SrcAtop)
                    }
                },
            text = url,
            style = StyleStolzRegularDarkBlue14,
            maxLines = 1
        )
        Icon(
            painter = painterResource(id = R.drawable.ic_copy_empty),
            contentDescription = stringResource(id = R.string.bt_copy),
            tint = DarkBlue,
            modifier = Modifier
                .align(Alignment.CenterVertically)
                .size(16.dp)
        )
    }
}

@Composable
private fun ShareProfileIconsComponent(modifier: Modifier = Modifier) {
    Box(
        modifier = modifier,
        contentAlignment = Alignment.CenterStart
    ) {
        Image(
            painter = rememberAsyncImagePainter("https://randomuser.me/api/portraits/women/60.jpg"),
            contentDescription = null,
            modifier = Modifier
                .size(32.dp)
                .background(color = White, shape = CircleShape)
                .padding(2.dp)
                .clip(CircleShape)
        )

        Image(
            painter = rememberAsyncImagePainter("https://randomuser.me/api/portraits/women/65.jpg"),
            contentDescription = null,
            modifier = Modifier
                .padding(start = 24.dp)
                .size(40.dp)
                .background(color = White, shape = CircleShape)
                .padding(2.dp)
                .clip(CircleShape)
        )

        Image(
            painter = rememberAsyncImagePainter("https://randomuser.me/api/portraits/women/58.jpg"),
            contentDescription = null,
            modifier = Modifier
                .padding(start = 56.dp)
                .size(32.dp)
                .background(color = White, shape = CircleShape)
                .padding(2.dp)
                .clip(CircleShape)

        )
    }
}

@Preview
@Composable
fun ShareProfileIconsComponentPreview() {
    ShareProfileIconsComponent()
}

@Preview
@Composable
fun ShareLinkComponentNewPreview() {
    ShareLinkComponent(
        modifier = Modifier,
        item = Account.createEmpty(),
        folder = Folder.createDummy(),
        isActionEnabled = true,
        shareErrorMessage = null,
        callback = object : ShareLinkComponentCallback {
            override fun onCreateShareLink() {
            }

            override fun onDeleteShareUrl() {
            }

            override fun onLinkCopied() {
            }

            override fun onRevokeAccess(member: Membership) {
            }

            override fun onOpenShareScreen() {
            }

        }
    )
}
