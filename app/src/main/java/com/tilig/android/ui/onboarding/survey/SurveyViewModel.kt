package com.tilig.android.ui.onboarding.survey

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tilig.android.data.models.tilig.SurveyAnswer
import com.tilig.android.data.models.tilig.UserSettings
import com.tilig.android.data.repository.Repository
import com.tilig.android.ui.onboarding.survey.SurveyPages.SURVEY_FINISHED
import com.tilig.android.ui.onboarding.survey.SurveyPages.SURVEY_FIRST_QUESTION
import com.tilig.android.ui.onboarding.survey.SurveyPages.SURVEY_SECOND_QUESTION
import com.tilig.android.ui.onboarding.survey.SurveyPages.SURVEY_THIRD_QUESTION
import com.tilig.android.utils.SharedPrefs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject


class SurveyViewModel : ViewModel(), KoinComponent {

    private val repo by inject<Repository>()
    private val preferences by inject<SharedPrefs>()

    private val _question1Answers = MutableStateFlow<MutableSet<String>>(mutableSetOf())
    private val _question2Answers = MutableStateFlow<String?>(null)
    private val _question3Answers = MutableStateFlow<MutableSet<String>>(mutableSetOf())
    private val _currentPage = MutableStateFlow(SURVEY_FIRST_QUESTION)
    private val _agreeChart = MutableStateFlow(false)
    private val _finishSurvey = MutableStateFlow(false)

    // if finish button was pressed cannot navigate between questions until server result
    private var _canNavigateBetweenQuestions = false

    val currentPage: StateFlow<Int> = _currentPage
    val finishSurvey: StateFlow<Boolean> = _finishSurvey

    val actionButtonEnabled: StateFlow<Boolean> = combine(
        _question1Answers, _question2Answers, _question3Answers, _currentPage
    ) { answers1, answers2, answers3, page ->
        when (page) {
            SURVEY_FIRST_QUESTION -> answers1.isNotEmpty()
            SURVEY_SECOND_QUESTION -> answers2 != null
            SURVEY_THIRD_QUESTION -> true // answers3.isNotEmpty()
            else -> {
                true
            }
        }
    }.stateIn(scope = viewModelScope, SharingStarted.Eagerly, false)

    fun addAnswerToFirstQuestion(answer: String) {
        val answersSet = mutableSetOf<String>()
        answersSet.addAll(_question1Answers.value)
        answersSet.add(answer)
        _question1Answers.value = answersSet
    }

    fun removeAnswerForFirstQuestion(answer: String) {
        val answersSet = mutableSetOf<String>()
        answersSet.addAll(_question1Answers.value)
        answersSet.remove(answer)
        _question1Answers.value = answersSet
    }

    fun addAnswerToThirdQuestion(answer: String) {
        val answersSet = mutableSetOf<String>()
        answersSet.addAll(_question3Answers.value)
        answersSet.add(answer)
        _question3Answers.value = answersSet
    }

    fun removeAnswerForThirdQuestion(answer: String) {
        val answersSet = mutableSetOf<String>()
        answersSet.addAll(_question3Answers.value)
        answersSet.remove(answer)
        _question3Answers.value = answersSet
    }

    fun addAnswerToSecondQuestion(answer: String) {
        _question2Answers.value = answer
    }

    fun showThirdQuestionAVariant() = _question2Answers.value.equals("Yes", true)

    fun getFirstQuestionAnswers(): Set<String> = _question1Answers.value

    fun getSecondQuestionAnswer(): String? = _question2Answers.value

    fun getThirdQuestionAnswers(): Set<String> = _question3Answers.value

    fun skipSurvey() {
        val answer = SurveyAnswer(
            skipped = true,
            chosenOptions = emptyList(),
            questionAttributes = surveyQuestions[FIRST_QUESTION_KEY]!!,
        )
        sendAnswer(answer = answer, true)
    }

    fun goToNextScreen() {
        _canNavigateBetweenQuestions = _currentPage.value != SurveyPages.SURVEY_PAGES - 1
        prepareAndSendAnswer(_currentPage.value)
        if (_currentPage.value != SurveyPages.SURVEY_PAGES - 1) {
            _currentPage.value = _currentPage.value + 1
        }
    }

    fun goToPreviousScreen() {
        if (_currentPage.value == SURVEY_THIRD_QUESTION) {
            _question3Answers.value = mutableSetOf()
        }
        if (_canNavigateBetweenQuestions) {
            _currentPage.value = (_currentPage.value - 1).coerceIn(0 until SurveyPages.SURVEY_PAGES)
        }
    }

    fun agreeOnChart(agree: Boolean) {
        _agreeChart.value = agree
    }

    private fun prepareAndSendAnswer(page: Int) {
        val answer = when (page) {
            SURVEY_FIRST_QUESTION -> {
                SurveyAnswer(
                    skipped = false,
                    chosenOptions = _question1Answers.value.toList(),
                    questionAttributes = surveyQuestions[FIRST_QUESTION_KEY]!!,
                )
            }
            SURVEY_SECOND_QUESTION -> {
                SurveyAnswer(
                    skipped = false,
                    chosenOptions = listOf(_question2Answers.value ?: ""),
                    questionAttributes = surveyQuestions[SECOND_QUESTION_KEY]!!,
                )
            }
            SURVEY_THIRD_QUESTION -> {
                SurveyAnswer(
                    skipped = false,
                    chosenOptions = _question3Answers.value.toList(),
                    questionAttributes = surveyQuestions[if (showThirdQuestionAVariant()) THIRD_A_QUESTION_KEY else THIRD_B_QUESTION_KEY]!!,
                )
            }
            else -> {
                SurveyAnswer(
                    skipped = false,
                    chosenOptions = listOf(if (_agreeChart.value) "Yes" else "No"),
                    questionAttributes = surveyQuestions[FINISH_QUESTION_KEY]!!,
                )
            }
        }
        sendAnswer(answer = answer, page == SURVEY_FINISHED)
    }

    private fun sendAnswer(answer: SurveyAnswer, isLastStep: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {
            repo.sendSurveyAnswer(answer)
            if (isLastStep) {
                val settings = UserSettings()
                settings.isSurveyFinished = true
                //silent call. don't block user to go to the next step
                repo.updateUserSettings(settings)
                preferences.isSurveyFinished = true
            }
            _finishSurvey.value = isLastStep
        }
    }
}
