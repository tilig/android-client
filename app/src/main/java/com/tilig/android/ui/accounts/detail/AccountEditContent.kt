package com.tilig.android.ui.accounts.detail

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.models.tilig.CustomField
import com.tilig.android.ui.accounts.detail.components.*
import com.tilig.android.ui.accounts.detail.events.AccountActionEvent
import com.tilig.android.ui.accounts.detail.holders.EditAccountStateHolder
import com.tilig.android.ui.components.TiligDialog
import com.tilig.android.ui.components.TiligSpacerVerticalDefault
import com.tilig.android.ui.secrets.add.AddToFolderComponent
import com.tilig.android.ui.secrets.components.*
import com.tilig.android.ui.secrets.details.BottomSheetType
import com.tilig.android.ui.secrets.details.ItemDetailsCallback
import com.tilig.android.ui.secrets.details.events.BaseEditEvent
import com.tilig.android.ui.secrets.details.events.ItemDetailsActionEvent
import com.tilig.android.ui.theme.*

@Composable
fun AccountEditContent(
    account: Account,
    displayUnsavedAlert: Boolean,
    state: EditAccountStateHolder,
    tracker: Tracker,
    callback: ItemDetailsCallback
) {
    val openDialog = remember { mutableStateOf(false) }

    val focusManager = LocalFocusManager.current

    Column(
        modifier = Modifier
            .imePadding()
            .fillMaxWidth()
            .padding(horizontal = DefaultPadding)
    ) {

        DetailsAccountNameView(
            modifier = Modifier.padding(top = DefaultPadding),
            nameState = state.nameState,
            onValueChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(AccountEditEvent.NameChanged(state.nameState.text)))
            },
            onNext = {
                focusManager.moveFocus(FocusDirection.Down)
            }
        )

        TiligSpacerVerticalDefault()

        UserNameEmailEdit(
            userNameEmailState = state.userNameEmailState,
            focusManager = focusManager,
            onValueChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(AccountEditEvent.UserNameChanged(state.userNameEmailState.text)))
            }
        )

        TiligSpacerVerticalDefault()

        TitledContent(
            modifier = Modifier,
            title = stringResource(id = R.string.label_login_password),
        ) {
            DetailsPasswordEdit(
                tracker = tracker,
                passwordState = state.passwordState,
                onNext = {
                    focusManager.moveFocus(FocusDirection.Down)
                },
                onValueChanged = {
                    callback.onActionEvent(ItemDetailsActionEvent.Edit(AccountEditEvent.PasswordChanged(state.passwordState.text)))
                }
            )
        }

        TiligSpacerVerticalDefault()
        WebViewEdit(
            websiteState = state.websiteState,
            // TODO show BOTH if app has website and android_app_id
            isApp = account.androidAppId != null,
            androidAppId = account.androidAppId,
            focusManager = focusManager,
            onFocusLost = {
                callback.onActionEvent(AccountActionEvent.GetBrandInfoByWebsite(state.websiteState.text))
            },
            onValueChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(AccountEditEvent.WebsiteChanged(state.websiteState.text)))
            }
        )

        TwoFAEdit(
            otpValue = account.otp,
            accountName = state.nameState.text,
            has2FAToken = account.hasOtp(),
            support2FA = account.brand?.totp ?: false,
            tracker = tracker,
            onOpenScanScreen = { callback.onOpenQrCodeScanner() },
            onShowTutorial = {
                tracker.trackEvent(Tracker.EVENT_2FA_READ_MORE_TAPPED)
                callback.onUpdateBottomSheet(BottomSheetType.Tutorial)
            },
            onNewCodeGenerated = {
                state.qrCodeValue.value = it
            }
        )

        TiligSpacerVerticalDefault()
        NoteViewEdit(
            notesState = state.notesState,
            title = if (account.getNotesLimit() != null) stringResource(
                id = R.string.label_extra_info_with_limit,
                state.notesState.text.length
            ) else {
                stringResource(id = R.string.label_extra_info)
            },
            maxLength = account.getNotesLimit(),
            contentHintRes = R.string.hint_note_extra_info,
            onValueChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(AccountEditEvent.ExtraInfoChanged(state.notesState.text)))
            }
        )

        AllCustomFieldsEditComponent(
            focusManager = focusManager,
            fieldsWrapper = state.customFields,
            onFieldsChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(BaseEditEvent.CustomFieldsChanged(state.customFields
                    .map {
                        CustomField(
                            it.labelFieldState.text,
                            it.customField.kind,
                            it.textFieldState.text
                        )
                    })))
            },
            onDeleteField = {
                val updated = state.customFields.toMutableList()
                updated.remove(it)
                callback.onActionEvent(ItemDetailsActionEvent.Edit(BaseEditEvent.CustomFieldsChanged(updated.map {
                    CustomField(
                        it.labelFieldState.text,
                        it.customField.kind,
                        it.textFieldState.text
                    )
                })))
            })

        TiligSpacerVerticalDefault()
        AddCustomField(modifier = Modifier.fillMaxWidth()) {
            callback.onActionEvent(ItemDetailsActionEvent.Edit(BaseEditEvent.AddCustomField(it)))
        }

        TiligSpacerVerticalDefault()
        AddToFolderComponent(
            modifier = Modifier,
            folders = state.foldersList,
            selectedFolder = state.selectedFolder,
            onFolderChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(BaseEditEvent.ChangeFolder(it)))
            }
        )

        TiligSpacerVerticalDefault()
        TextButton(modifier = Modifier
            .fillMaxWidth()
            .background(color = White, shape = RoundedCornerShape(MediumCornerRadius))
            .height(DefaultButtonHeight),
            elevation = ButtonDefaults.elevation(
                defaultElevation = 0.dp,
                focusedElevation = 0.dp,
                hoveredElevation = 0.dp
            ),
            shape = RoundedCornerShape(8.dp),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = White,
                disabledBackgroundColor = Grey,
            ),
            enabled = account.details.getSortedHistory().isNotEmpty(),
            onClick = {
                account.details.getSortedHistory().let {
                    callback.onUpdateBottomSheet(
                        BottomSheetType.PasswordHistory(
                            it
                        )
                    )
                }
            }) {
            Text(
                stringResource(id = R.string.view_password_history),
                style = StyleStolzRegularDarkBlue16,
                color = if (account.details.getSortedHistory()
                        .isEmpty()
                ) DisabledGrey else LightBlue
            )
        }

        if (!account.hasOtp() && account.brand?.totp != true) {
            TiligSpacerVerticalDefault()
            TextButton(modifier = Modifier
                .fillMaxWidth()
                .background(color = White, shape = RoundedCornerShape(MediumCornerRadius))
                .height(DefaultButtonHeight),
                elevation = ButtonDefaults.elevation(
                    defaultElevation = 0.dp,
                    focusedElevation = 0.dp,
                    hoveredElevation = 0.dp
                ),
                shape = RoundedCornerShape(8.dp),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = White,
                    disabledBackgroundColor = Grey,
                ),
                onClick = {
                    callback.onOpenQrCodeScanner()
                }) {
                Text(
                    stringResource(id = R.string.enable_2fa),
                    style = StyleStolzRegularDarkBlue16,
                    color = LightBlue
                )
            }
        }

        Spacer(modifier = Modifier.height(56.dp))
        DeleteSecretItemView(
            titleId = R.string.bt_delete_account,
            dialogDescriptionId = R.string.dialog_delete_item,
            openDialog = openDialog
        ) {
            callback.onActionEvent(ItemDetailsActionEvent.DeleteItem)
        }
    }

    if (displayUnsavedAlert) {
        TiligDialog(
            title = R.string.alert_unsaved_changes,
            text = R.string.alert_unsaved_changes_desc,
            onCancel = {
                callback.onActionEvent(ItemDetailsActionEvent.CancelChangesDialog)
            },
            onDismissRequest = {},
            confirmButtonText = R.string.alert_unsaved_changes_discard,
            cancelButtonText = R.string.alert_unsaved_changes_confirm,
            onConfirm = { callback.onActionEvent(ItemDetailsActionEvent.ConfirmDiscardChanges) })
    }

    BackHandler {
        callback.onActionEvent(ItemDetailsActionEvent.OnGoBack)
    }
}