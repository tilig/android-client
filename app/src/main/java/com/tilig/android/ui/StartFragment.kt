package com.tilig.android.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.accompanist.pager.ExperimentalPagerApi
import com.tilig.android.BuildConfig
import com.tilig.android.R
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.autofill.AutofillHelper
import com.tilig.android.data.models.tilig.Profile
import com.tilig.android.featureflag.FeatureFlagClient
import com.tilig.android.ui.onboarding.OnboardingActivity
import com.tilig.android.ui.secrets.SecretItemsViewModel
import org.koin.android.ext.android.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * This is only a splash screen.
 * It launches as the first fragment in the main navigation graph
 * and redirects to either:
 * - the list fragment (if logged in)
 * - the onboarding flow (if logged out and launched for the first time)
 * - the login fragment (if logged out)
 * - the edit fragment (if logged in and launched from the autofill)
 */
@ExperimentalAnimationApi
@ExperimentalPagerApi
class StartFragment : BaseFragment(R.layout.fragment_start) {

    companion object {
        val ARG_ACCOUNT_DELETED = "ARG_ACCOUNT_DELETED"
    }

    private val profileModel: SecretItemsViewModel by activityViewModels()
    private val tracker: Mixpanel by inject()
    private val featureFlagClient: FeatureFlagClient by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setStatusbarColorBright(false)
    }

    override fun onResume() {
        super.onResume()

        val isLoggedIn = prefs.isLoggedIn()
        val showDeleteMessage = arguments?.getBoolean(ARG_ACCOUNT_DELETED, false)?:false

        // Offer to create a new secret if the intent includes an App id
        when {
            isLoggedIn && AutofillHelper().shouldShowAutofill(prefs, requireContext()) -> {
                findNavController().navigate(StartFragmentDirections.actionStartFragmentToOnboardingAutofillFragment())
            }
            isLoggedIn -> {
                findNavController().navigate(R.id.action_startFragment_to_homeFragment)
            }
            !prefs.hasFinishedOnboarding || isLaunchedFromAutofill() -> {
                launchOnBoardingActivity(showDeleteMessage)
            }
            else -> {
                // The user has logged out.
                launchOnBoardingActivity(showDeleteMessage)
            }
        }

        runMigrations()

        // Storing the app version might be handy in the future to detect app upgrades
        prefs.appVersion = BuildConfig.VERSION_CODE
    }

    /**
    Note that starting this activity does not close the current one;
    all the other navigation options include the popupInclusive=true flag,
    but launching an activity does not have that option.
    We do this: launch the OnboardingActivity on top of this one,
    and when it closes, this method (onResume) is called again, updating
    everything to go where it should (because the signin status has changed)
     */
    private fun launchOnBoardingActivity(showAccountDeletedMessage: Boolean) {
        val intent = Intent(
            requireActivity(),
            OnboardingActivity::class.java
        )
        intent.putExtra(OnboardingActivity.ARG_ACCOUNT_DELETED, showAccountDeletedMessage)
        startActivity(intent)
    }

    private fun runMigrations() {
        if (prefs.appVersion in 2000000..2000199) {
            updateTo202MixpanelIdentity()
        }
    }

    /**
     * Fix for mixpanel identity missing in Signups happening in version 2.0.0 and 2.0.1.
     * This fix will fetch the identity after the upgrade, and set it in Mixpanel,
     * retroactively fixing all the events
     */
    private fun updateTo202MixpanelIdentity() {
        if (prefs.mixpanelUserId.isNullOrEmpty()
                && prefs.isLoggedIn()) {
            Log.i("ANALYTICS", "Fixing missing Mixpanel distinctId...")
            profileModel.myAccount().enqueue(object : Callback<Profile> {
                override fun onFailure(call: Call<Profile>, t: Throwable) {
                    Log.e("ANALYTICS", "Unable to fetch profile", t)
                }

                override fun onResponse(
                    call: Call<Profile>,
                    response: Response<Profile>
                ) {
                    response.body()?.id?.let {
                        prefs.mixpanelUserId = it
                        tracker.setIdentity(it)
                        featureFlagClient.setIdentity(it)
                    }
                }
            })
        }
    }

    private fun isLaunchedFromAutofill(): Boolean =
        (requireActivity() as? LaunchModeView)?.isInCreateNewMode() == true
                || (requireActivity() as? LaunchModeView)?.isInSearchMode() == true

}
