
package com.tilig.android.ui.settings

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tilig.android.BuildConfig
import com.tilig.android.data.models.ApiResponse
import com.tilig.android.data.models.ApiSuccessResponse
import com.tilig.android.data.models.SecretItemType
import com.tilig.android.data.models.tilig.*
import com.tilig.android.data.repository.Repository
import com.tilig.crypto.Crypto
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class SettingsViewModel : ViewModel(), KoinComponent {
    private val repo by inject<Repository>()
    var profile by mutableStateOf<ApiResponse<Profile>?>(null)

    init {
        viewModelScope.launch {
            profile = getProfile()
        }
    }

    fun getJsonConverter() = repo.jsonConverter

    private suspend fun getProfile(): ApiResponse<Profile> {
        return repo.fetchProfileAsync()
    }

    suspend fun deleteAllAccounts() {
        // extra safety guard
        if (BuildConfig.DEBUG) {
            repo.secretItems().let { response ->
                if (response is ApiSuccessResponse) {
                    response.body.items.forEach {
                        Log.w("DEV", "Deleting account ${it.domain} (id ${it.id})")
                        it.id?.let { id ->
                            repo.deleteSecretItemCall(id).execute()
                        }
                    }
                }
            }
        }
    }

    fun createDummyItems(crypto: Crypto, types: List<SecretItemType>) {
        // extra safety guard
        if (BuildConfig.DEBUG) {
            val secretItems = mutableListOf<SecretItem>()
            types.forEach {
                secretItems.addAll(
                    when (it) {
                        SecretItemType.ACCOUNT -> createDummyAccounts()
                        SecretItemType.NOTE -> createDummyNotes()
                        SecretItemType.CREDIT_CARD -> createDummyCards()
                        SecretItemType.WIFI -> createDummyWifi()
                        SecretItemType.OTHER -> createDummyCustomItems()
                    }
                )
            }
            secretItems.forEach { item ->
                val encryptedItem = EncryptedItem.encrypt(item, repo.jsonConverter, crypto)
                repo.createSecretItemCall(encryptedItem).execute()
            }
        }
    }

    private fun createDummyAccounts() =
        listOf(
            Account.createEmpty().apply {
                name = "Amazon"
                website = "https://amazon.com"
                username = "d.scully@fbi.gov"
            },
            Account.createEmpty().apply {
                name = "Apple"
                website = "https://apple.com"
                username = "pear@apple.com"
            },
            Account.createEmpty().apply {
                name = "Facebook"
                website = "https://facebook.com"
                username = "mark@facebook.com"
            },
            Account.createEmpty().apply {
                name = "Google"
                website = "https://google.com"
                username = "loremipsum@gmail.com"
            },
            Account.createEmpty().apply {
                name = "Netflix"
                website = "https://netflix.com"
                username = "Shared account"
            },
            Account.createEmpty().apply {
                name = "PayPal"
                website = "https://paypal.com"
                username = "tilig"
            },
            Account.createEmpty().apply {
                name = "Spotify"
                website = "https://open.spotify.com"
                username = "tilig"
            }
        )

    private fun createDummyNotes() =
        listOf(
            Note.createEmpty().apply {
                name = "Note"
                content =
                    "Agreed joy vanity regret met may ladies oppose who. Mile fail as left as hard eyes. Meet made call in mean four year it to. Prospect so branched wondered sensible of up. For gay consisted resolving pronounce sportsman saw discovery not. Northward or household as conveying we earnestly believing. No in up contrasted discretion inhabiting excellence. Entreaties we collecting unpleasant at everything conviction."
            },
            Note.createEmpty().apply {
                name = "YePGOOvaTo"
                content = ""
            },
            Note.createEmpty().apply {
                name = "GmxlUavtLf"
                content =
                    "HNiZsknLONryvAusUiqkTiRSipIbmqGElHSOFFjtJyqmEAjsYNPAejLUBbvXyYFLWMdIhISqtzpKNvRyLgJOYfmEtTMYrVEArHYh"
            }
        )

    private fun createDummyCards() =
        listOf(
            CreditCard.createEmpty().apply {
                name = "My Card 1"
                holderName = "Holder Name"
                expireDate = "03/23"
                cardNumber = "1234123412341234"
                securityCode = "1234"
                extraInfo = "My card description"
                pinCode = "0000"
                zipCode = "02133"
            },
            CreditCard.createEmpty().apply {
                name = "My Card 2 Empty"
                holderName = null
                expireDate = null
                cardNumber = null
                securityCode = null
                extraInfo = null
                pinCode = null
                zipCode = null
            },
            CreditCard.createEmpty().apply {
                name = "VISA"
                holderName = "Ghayda Karlsen"
                expireDate = "08/2029"
                cardNumber = "4019023621705121"
                securityCode = "1234"
                extraInfo = null
                pinCode = "000"
                zipCode = "23422B"
            },
            CreditCard.createEmpty().apply {
                name = "MASTERCARD"
                holderName = "Rasmus Krogh"
                expireDate = "08/2029"
                cardNumber = "5301532571241626"
                securityCode = "982"
                extraInfo = "ANDORRA"
                pinCode = "5863"
                zipCode = "23422B"
            },
            CreditCard.createEmpty().apply {
                name = "AMERICAN EXPRESS"
                holderName = "Theodoric Huismans"
                expireDate = "08/2029"
                cardNumber = "374104904873013"
                securityCode = "982"
                extraInfo = "ALPHA CARD S.C.R.L. - BELGIUM CARE CONS"
                pinCode = "3190"
                zipCode = "23422B"
            },
            CreditCard.createEmpty().apply {
                name = "Card"
                holderName = "Theodoric Huismans"
                expireDate = "08/2029"
                cardNumber = "37410490487301312345"
                securityCode = "982"
                extraInfo = null
                pinCode = "3"
                zipCode = "2"
            }
        )

    private fun createDummyWifi() =
        listOf(
            WifiPassword.createEmpty().apply {
                name = "Wifi 1"
                password = "*9szUG4c!F"
                extraInfo = ""
                networkName = "I am here"
            },
            WifiPassword.createEmpty().apply {
                name = "Wifi 2"
                password = null
                extraInfo = null
                networkName = null
            },
            WifiPassword.createEmpty().apply {
                name = "Wifi 3"
                password = null
                extraInfo = "I will sue you if you use my Wi-Fi"
                networkName = "Welcome 2 Azkaban"
            },
        )

    private fun createDummyCustomItems() =
        listOf(
            OtherItem.createEmpty().apply {
                name = "Wifi 1"
                details.customFields = mutableListOf(
                    CustomField(name = "Custom", kind = ItemFieldType.TEXT, "Custom value")
                )
            }
        )
}
