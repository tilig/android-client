package com.tilig.android.ui.wifipass.details

import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.SecretItemType
import com.tilig.android.data.models.tilig.CustomField
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.SecretItem
import com.tilig.android.data.models.tilig.WifiPassword
import com.tilig.android.ui.others.details.OtherEditWrapper
import com.tilig.android.ui.secrets.details.ItemDetailsViewModel
import com.tilig.android.ui.secrets.details.events.BaseEditEvent
import com.tilig.android.ui.secrets.details.states.ItemEditWrapper
import com.tilig.android.ui.secrets.details.states.ItemDetailsUiState
import kotlinx.coroutines.flow.update

data class WifiEditWrapper(
    val name: String? = null,
    val password: String? = null,
    val extraInfo: String? = null,
    val networkName: String? = null,
    override val customFields: List<CustomField>? = null,
    override val selectedFolder: Folder? = null
) : ItemEditWrapper {
    constructor(wifiPassword: WifiPassword) : this(
        name = wifiPassword.name,
        password = wifiPassword.password,
        extraInfo = wifiPassword.extraInfo,
        networkName = wifiPassword.networkName,
        customFields = wifiPassword.details.customFields,
        selectedFolder = wifiPassword.folder
    )

    override fun hadDataChanged(original: SecretItem): Boolean {
        return (original as WifiPassword).name != name || original.password != password ||
                original.networkName != networkName || original.extraInfo != extraInfo ||
                original.folder != selectedFolder || original.details.customFields != customFields
    }

    override fun getItemName(): String? = name
}

sealed interface WifiEditEvents : BaseEditEvent {
    data class NameChanged(val name: String) : WifiEditEvents
    data class PasswordChanged(val password: String) : WifiEditEvents
    data class NetworkNameChanged(val networkName: String) : WifiEditEvents
    data class ExtraInfoChanged(val extraInfo: String) : WifiEditEvents
}

class WifiViewModel : ItemDetailsViewModel() {
    override fun getSecretItemType(): SecretItemType = SecretItemType.WIFI

    override fun getItemEditWrapper(item: SecretItem?): ItemEditWrapper {
        return if (item != null) WifiEditWrapper(item as WifiPassword) else WifiEditWrapper()
    }

    override fun updateCustomFields(customFields: List<CustomField>?): ItemEditWrapper {
        return (viewModelState.value.itemEditWrapper as WifiEditWrapper).copy(
            customFields = customFields
        )
    }

    override fun updateFolder(folder: Folder?): ItemEditWrapper {
        return (viewModelState.value.itemEditWrapper as WifiEditWrapper).copy(
            selectedFolder = folder
        )
    }

    override fun onEditEvent(event: BaseEditEvent) {
        if (event is WifiEditEvents) {
            viewModelState.update {
                it.copy(
                    itemEditWrapper =
                    when (event) {
                        is WifiEditEvents.ExtraInfoChanged -> (it.itemEditWrapper as WifiEditWrapper).copy(
                            extraInfo = event.extraInfo
                        )
                        is WifiEditEvents.NameChanged -> (it.itemEditWrapper as WifiEditWrapper).copy(
                            name = event.name
                        )
                        is WifiEditEvents.NetworkNameChanged -> (it.itemEditWrapper as WifiEditWrapper).copy(
                            networkName = event.networkName
                        )
                        is WifiEditEvents.PasswordChanged -> (it.itemEditWrapper as WifiEditWrapper).copy(
                            password = event.password
                        )
                    }
                )
            }
        } else {
            super.onEditEvent(event)
        }
    }

    override fun buildItemWithUpdatedItems(): SecretItem? {
        val currentState = uiState.value
        if (currentState is ItemDetailsUiState.ItemEdit) {
            val editable = currentState.itemEditWrapper as WifiEditWrapper
            return (currentState.item as WifiPassword).apply {
                this.name = editable.name
                this.networkName = editable.networkName
                this.password = editable.password
                this.extraInfo = editable.extraInfo
                // ignore fields with empty names
                editable.customFields?.filter { it.name.isNotEmpty() }?.let {
                    this.details.customFields = it.toMutableList()
                }
                updateFolderField(this, editable.selectedFolder)
            }
        }
        return null
    }

    override fun trackDeleteItem() {
        tracker.trackEvent(Tracker.EVENT_WIFI_DELETED)
    }
}