package com.tilig.android.ui.onboarding.getstarted.components

import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.squareup.moshi.Json
import com.tilig.android.R
import com.tilig.android.ui.components.ExpandView
import com.tilig.android.ui.theme.*

enum class Step(val sortOrder: Int, @StringRes val titleRes: Int) {

    @Json(name = "is_biometric_finished")
    BIOMETRIC_STEP(0, R.string.get_started_step_biometric),

    @Json(name = "is_add_login_finished")
    LOGIN(1, R.string.get_started_step_login),

    @Json(name = "is_try_autofill_finished")
    TRY_AUTOFILL(2, R.string.get_started_step_try_autofill),

    @Json(name = "is_desktop_connect_finished")
    CONNECT_DESKTOP(3, R.string.get_started_step_desktop),

    UNKNOWN(-1, 0);
}

data class GetStartedStepUI(
    val step: Step,
    val isCompleted: Boolean,
    val orderNumber: Int,
    val content: @Composable () -> Unit,
)

@Composable
fun GetStartedStepItem(
    currentExpandedStep: MutableState<Step?>,
    step: GetStartedStepUI
) {

    if (currentExpandedStep.value == step.step && step.isCompleted) {
        currentExpandedStep.value = null
    }

    ExpandView(
        modifier = Modifier
            .fillMaxWidth()
            .background(shape = Shapes.medium, color = if (step.isCompleted) Green3 else Blue3),
        header = {
            GetStartedStepHeader(
                step = step,
                onHeaderClick = {
                    if (!step.isCompleted) {
                        currentExpandedStep.value = step.step
                    }
                }
            )
        },
        content = {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        start = MediumWithQuarter,
                        end = MediumWithQuarter,
                        top = QuarterPadding,
                        bottom = MediumWithQuarter
                    )
            ) {
                step.content.invoke()
            }
        },
        expanded = currentExpandedStep.value?.equals(step.step) ?: false,
        withBorders = false,
        cardShape = RectangleShape,
        elevation = 0.dp
    )
}

@Composable
fun GetStartedStepHeader(
    step: GetStartedStepUI,
    onHeaderClick: () -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(shape = Shapes.medium, color = if (step.isCompleted) Green3 else Blue3)
            .clickable { onHeaderClick.invoke() }
            .padding(MediumWithQuarter),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .size(32.dp)
                .background(color = White.copy(alpha = 0.3f), shape = CircleShape)
                .border(width = 1.dp, color = DimmedWhite, shape = CircleShape)
                .clip(CircleShape)
        ) {
            Text(
                style = StyleStolzMediumDarkBlue14.copy(
                    color = Neutral40
                ),
                text = step.orderNumber.toString(),
                textAlign = TextAlign.Center,
            )
        }

        Text(
            modifier = Modifier
                .weight(1f)
                .padding(horizontal = HalfPadding),
            style = StyleStolzMediumDarkBlue16,
            text = stringResource(id = step.step.titleRes),
        )

        if (step.isCompleted) {
            Image(
                painter = painterResource(id = R.drawable.ic_check),
                contentDescription = null,
                modifier = Modifier
                    .size(32.dp)
                    .background(color = White, shape = CircleShape)
                    .padding(1.dp)
                    .clip(CircleShape)
            )
        }
    }
}
