package com.tilig.android.ui.accounts.detail.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.accounts.detail.components.states.PasswordState
import com.tilig.android.ui.components.BorderTextFieldStyle
import com.tilig.android.ui.components.PasswordBasicField
import com.tilig.android.ui.components.SlotText
import com.tilig.android.ui.components.borderTextFieldColors
import com.tilig.android.ui.theme.*
import com.tilig.crypto.Password
import org.koin.androidx.compose.get

private enum class PassState {
    Initial,
    PassGenerating
}

@Composable
fun DetailsPasswordView(
    modifier: Modifier = Modifier,
    tracker: Tracker,
    password: String?,
) {
    val passwordState = PasswordState(password)

    PasswordBasicField(
        modifier = modifier
            .fillMaxWidth()
            .heightIn(min = 32.dp),
        hint = "",
        state = passwordState,
        textStyle = StylePasswordText,
        readOnly = true,
        singleLine = false,
        showRevealButton = passwordState.text.isNotEmpty(),
        contentHorizontalPadding = 0.dp,
        tracker = tracker
    )
}

@Composable
fun DetailsPasswordEdit(
    tracker: Tracker,
    modifier: Modifier = Modifier,
    passwordState: PasswordState,
    borderTextFieldColors: BorderTextFieldStyle = borderTextFieldColors(),
    onNext: () -> Unit,
    onValueChanged: ((String) -> Unit)? = null,
    ) {
    val state = remember { mutableStateOf(PassState.Initial) }
    val shape = RoundedCornerShape(8.dp)
    val borderColor = borderTextFieldColors.borderColor(focused = passwordState.isFocused)
    val bgColor = if (state.value == PassState.PassGenerating) {
        TiligYellow2
    } else borderTextFieldColors.backgroundColor()

    Row(
        modifier = modifier
            .heightIn(min = DefaultButtonHeight)
            .background(
                bgColor,
                shape = shape
            )
            .border(width = DefaultSmallBorder, color = borderColor.value, shape = shape)
            .padding(end = 12.dp)
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        val passwordModifier = Modifier
            .fillMaxSize()
            .weight(1f)
            .padding(vertical = 4.dp)
        when (state.value) {
            PassState.Initial -> {
                PasswordBasicField(
                    modifier = passwordModifier,
                    hint = stringResource(id = R.string.hint_login_password_existing),
                    state = passwordState,
                    textStyle = StylePasswordText,
                    onNext = onNext,
                    singleLine = false,
                    readOnly = false,
                    showRevealButton = false,
                    contentHorizontalPadding = 12.dp,
                    onValueChange = {
                        if (it.isEmpty()) {
                            state.value = PassState.Initial
                        }
                        onValueChanged?.invoke(it)
                    }
                )
                TextButton(modifier = Modifier
                    .height(36.dp)
                    .padding(horizontal = 0.dp),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = White,
                        contentColor = White,
                        disabledBackgroundColor = White,
                        disabledContentColor = White
                    ),
                    elevation = ButtonDefaults.elevation(),
                    onClick = {
                        state.value = PassState.PassGenerating
                        passwordState.text = Password.generate().toString()
                        onValueChanged?.invoke(passwordState.text)
                        tracker.trackEvent(Tracker.EVENT_PASSWORD_GENERATED)
                    }) {
                    Text(
                        modifier = Modifier.align(Alignment.CenterVertically),
                        text = stringResource(id = R.string.generate),
                        style = StyleStolzRegularDarkBlue14.copy(
                            color = LightBlue,
                            lineHeight = 24.sp
                        )
                    )
                }
            }
            PassState.PassGenerating -> {
                SlotText(
                    text = passwordState.text,
                    textStyle = StyleRobotoMonoMediumDarkBlue16,
                    modifier = Modifier
                        .fillMaxSize()
                        .weight(1f)
                        .padding(start = DefaultPadding),
                    onSlottingEnd = {
                        state.value = PassState.Initial
                    }
                )
            }
        }
    }
}

@Composable
@Preview
fun DetailsPasswordViewPreview() {
    DetailsPasswordView(
        tracker = get(),
        password = "valuepass",
    )
}

@Composable
@Preview
fun DetailsPasswordEditPreview() {
    DetailsPasswordEdit(
        tracker = get(),
        passwordState = PasswordState("valuepass"),
        onNext = {}
    )
}
