package com.tilig.android.ui.wifipass.add

import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.WifiPassword
import com.tilig.android.ui.wifipass.WifiPassFieldsStateHolder
import com.tilig.android.ui.wifipass.rememberWifiPassFieldsStateHolder

class AddWifiPassStateHolder(
    val scrollableState: ScrollState,
    val selectedFolder: MutableState<Folder?>,
    val wifiPassFieldsStateHolder: WifiPassFieldsStateHolder,
) {

    fun buildWifiPassModel(): WifiPassword {
        val wifiPassword = WifiPassword.createEmpty()
        return wifiPassword.apply {
            name = wifiPassFieldsStateHolder.nameState.text
            password = wifiPassFieldsStateHolder.passwordState.text
            extraInfo = wifiPassFieldsStateHolder.extraInfoState.text
            networkName = wifiPassFieldsStateHolder.networkNameState.text
            folder = selectedFolder.value
        }
    }

    fun isSaveButtonEnabled(): Boolean = wifiPassFieldsStateHolder.areAllRequiredFieldsSetup()
}

@Composable
fun rememberAddWifiPassStateHolder(
    scrollableState: ScrollState = rememberScrollState(),
    wifiPassFieldsStateHolder: WifiPassFieldsStateHolder = rememberWifiPassFieldsStateHolder(
        wifiName = "",
        password = null,
        extraInfo = null,
        networkName = null,
        wifiCustomFields = null
    )
) = remember {
    AddWifiPassStateHolder(
        scrollableState = scrollableState,
        wifiPassFieldsStateHolder = wifiPassFieldsStateHolder,
        selectedFolder = mutableStateOf(null)
    )
}