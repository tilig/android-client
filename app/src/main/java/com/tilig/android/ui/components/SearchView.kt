package com.tilig.android.ui.components

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.ime
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.theme.*
import org.koin.androidx.compose.get

@ExperimentalComposeUiApi
@Composable
fun SearchView(
    modifier: Modifier,
    tracker: Tracker,
    state: MutableState<TextFieldValue>,
    searchHint: String = stringResource(
        id = R.string.label_search
    )
) {

    val shape = RoundedCornerShape(DefaultCornerRadius)
    val focusManager = LocalFocusManager.current
    val keyboardManager = LocalSoftwareKeyboardController.current
    val isSearchActivated: MutableState<Boolean> = remember {
        mutableStateOf(false)
    }

    // remove focus if keyboard is closed
    if (WindowInsets.ime.getBottom(LocalDensity.current) == 0) {
        focusManager.clearFocus(force = true)
    }

    TextField(
        value = state.value,
        onValueChange = { value ->
            state.value = value
        },
        placeholder = {
            Text(
                searchHint,
                style = Typography.button,
                color = DimmedBlue
            )
        },
        modifier = modifier
            .border(0.5.dp, SearchBarBorder, shape = shape)
            .onFocusChanged {
                if (it.isFocused && !isSearchActivated.value) {
                    isSearchActivated.value = it.isFocused
                    tracker.trackEvent(
                        Tracker.EVENT_SEARCH_ITEM_TAPPED,
                        mapOf(Tracker.EVENT_SEARCH_ITEM_TAPPED_SOURCE to Tracker.EVENT_SEARCH_ITEM_TAPPED_ITEM_LIST)
                    )
                }
            },
        leadingIcon = {
            Icon(
                painter = painterResource(id = R.drawable.ic_search),
                contentDescription = "",
                modifier = Modifier
                    .padding(start = 14.dp, top = 14.dp, bottom = 14.dp)
                    .size(28.dp)
            )
        },
        trailingIcon = {
            if (state.value != TextFieldValue("")) {
                IconButton(onClick = { state.value = TextFieldValue("") }) {
                    Icon(
                        modifier = Modifier
                            .padding(16.dp)
                            .size(16.dp),
                        painter = painterResource(id = R.drawable.ic_close),
                        contentDescription = null
                    )
                }
            }
        },
        singleLine = true,
        shape = shape,
        textStyle = Typography.subtitle2,
        colors = TextFieldDefaults.textFieldColors(
            textColor = DefaultTextColor,
            cursorColor = DefaultTextColor,
            leadingIconColor = DimmedBlue,
            trailingIconColor = DarkBlue,
            backgroundColor = SearchBarBg,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent
        ),
        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Search),
        keyboardActions = KeyboardActions(
            onSearch = {
                focusManager.clearFocus()
                keyboardManager?.hide()
            }
        )
    )
}

@ExperimentalComposeUiApi
@Preview(showBackground = true)
@Composable
fun SearchViewPreview() {
    val textState = remember { mutableStateOf(TextFieldValue("fghdfgh")) }
    SearchView(tracker = get(), state = textState, modifier = Modifier)
}
