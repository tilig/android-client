package com.tilig.android.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.tilig.android.ui.theme.DefaultCornerRadius
import com.tilig.android.ui.theme.DimmedBackground
import com.tilig.android.ui.theme.DoublePadding
import com.tilig.android.ui.theme.ProgressIndicatorColor

@Preview
@Composable
fun LoadingScreen(
    modifier : Modifier = Modifier,
    progressSize: Dp = 84.dp
) {
    Box(
        modifier = modifier.fillMaxSize()
    ) {
        val shape = RoundedCornerShape(size = DefaultCornerRadius)
        Box(
            modifier = Modifier
                .background(DimmedBackground, shape)
                .padding(DoublePadding)
                .wrapContentSize()
                .align(Alignment.Center)
        ) {
            CircularProgressIndicator(
                modifier = Modifier
                    .size(progressSize)
                    .align(Alignment.Center),
                color = ProgressIndicatorColor,
                strokeWidth = 2.dp
            )
        }
    }
}
