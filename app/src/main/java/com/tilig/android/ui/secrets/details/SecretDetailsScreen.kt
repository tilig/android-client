package com.tilig.android.ui.secrets.details

import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.UiError
import com.tilig.android.data.models.tilig.*
import com.tilig.android.ui.accounts.detail.*
import com.tilig.android.ui.accounts.detail.holders.rememberEditAccountStateHolder
import com.tilig.android.ui.cards.details.CardEditContent
import com.tilig.android.ui.cards.details.CardEditWrapper
import com.tilig.android.ui.cards.details.CardViewContent
import com.tilig.android.ui.cards.details.CardViewModel
import com.tilig.android.ui.cards.details.holders.rememberEditCardStateHolder
import com.tilig.android.ui.notes.details.NoteEditContent
import com.tilig.android.ui.notes.details.NoteEditWrapper
import com.tilig.android.ui.notes.details.NoteViewContent
import com.tilig.android.ui.notes.details.NoteViewModel
import com.tilig.android.ui.notes.details.holders.rememberEditNoteStateHolder
import com.tilig.android.ui.others.details.OtherEditContent
import com.tilig.android.ui.others.details.OtherEditWrapper
import com.tilig.android.ui.others.details.OtherViewContent
import com.tilig.android.ui.others.details.OtherViewModel
import com.tilig.android.ui.others.details.holders.rememberEditOtherStateHolder
import com.tilig.android.ui.secrets.details.events.ItemDetailsActionEvent
import com.tilig.android.ui.secrets.details.states.ItemDetailsUiState
import com.tilig.android.ui.wifipass.details.WifiEditContent
import com.tilig.android.ui.wifipass.details.WifiEditWrapper
import com.tilig.android.ui.wifipass.details.WifiViewContent
import com.tilig.android.ui.wifipass.details.WifiViewModel
import com.tilig.android.ui.wifipass.details.holders.rememberEditWifiStateHolder
import org.koin.androidx.compose.get

@Composable
fun SecretDetailsScreen(
    initialItem: SecretItem,
    stateHolder: DetailsScreenStateHolder
) {
    val viewModel: ItemDetailsViewModel = when (initialItem) {
        is Account -> viewModel<AccountViewModel>()
        is Note -> viewModel<NoteViewModel>()
        is CreditCard -> viewModel<CardViewModel>()
        is WifiPassword -> viewModel<WifiViewModel>()
        is OtherItem -> viewModel<OtherViewModel>()
        else -> viewModel<OtherViewModel>()
    }

    val actionCallBack = object : ItemDetailsCallback {

        override fun onActionEvent(event: ItemDetailsActionEvent) {
            viewModel.onActionEvent(event)
        }

        override fun onOpenQrCodeScanner() {
            initialItem.id?.let { stateHolder.openQRCodeScreen(it) }
        }

        override fun onShowSnackbarMessage(message: String) {
            stateHolder.showSnackBarMessage(message)
        }

        override fun onUpdateBottomSheet(type: BottomSheetType?) {
            stateHolder.updateBottomSheet(type)
        }
    }

    BaseSecretDetailsScreen(
        itemViewModel = viewModel,
        initialItem = initialItem,
        state = stateHolder,
        onOpenQrCodeScanner = { initialItem.id?.let { stateHolder.openQRCodeScreen(it) } },
        content = { state ->
            SecretStateUIComponent(
                stateHolder = stateHolder,
                state = state,
                initialItem = initialItem,
                itemViewModel = viewModel,
                onActionEvent = {
                    actionCallBack.onActionEvent(it)
                },
                viewContent = { viewState ->
                    ViewContent(
                        secretItem = viewState.item,
                        folder = viewState.folder,
                        shareErrorMessage = viewState.shareErrorMessage,
                        isLoading = viewState.isLoading,
                        tracker = stateHolder.tracker,
                        callBack = actionCallBack
                    )
                },
                editContent = { editState ->
                    EditContent(
                        tracker = stateHolder.tracker,
                        editState = editState,
                        actionCallBack = actionCallBack
                    )
                },
                whatWasWrongTextResId = when (initialItem) {
                    is Account -> R.string.error_load_this_login
                    is Note -> R.string.error_load_this_note
                    is CreditCard -> R.string.error_load_this_card
                    is WifiPassword -> R.string.error_load_this_wifi
                    else -> R.string.error_load_this_item
                }

            )
        })
}

@Composable
private fun ViewContent(
    secretItem: SecretItem,
    folder: Folder?,
    isLoading: Boolean,
    shareErrorMessage: UiError? = null,
    tracker: Tracker,
    callBack: ItemDetailsCallback
) {
    when (secretItem) {
        is Account -> {
            AccountViewContent(
                account = secretItem,
                folder = folder,
                isLoading = isLoading,
                shareErrorMessage = shareErrorMessage,
                tracker = tracker,
                callBack = callBack,
            )
        }
        is Note -> NoteViewContent(
            note = secretItem,
            folder = folder,
            isLoading = isLoading,
            shareErrorMessage = shareErrorMessage,
            tracker = tracker,
            callBack = callBack
        )
        is CreditCard -> CardViewContent(
            card = secretItem,
            folder = folder,
            isLoading = isLoading,
            shareErrorMessage = shareErrorMessage,
            tracker = tracker,
            callBack = callBack
        )
        is WifiPassword -> WifiViewContent(
            wifiPassword = secretItem,
            folder = folder,
            isLoading = isLoading,
            shareErrorMessage = shareErrorMessage,
            tracker = tracker,
            callBack = callBack
        )
        is OtherItem -> OtherViewContent(
            otherItem = secretItem,
            folder = folder,
            isLoading = isLoading,
            shareErrorMessage = shareErrorMessage,
            tracker = tracker,
            callBack = callBack
        )
    }
}

@Composable
private fun EditContent(
    tracker: Tracker,
    editState: ItemDetailsUiState.ItemEdit,
    actionCallBack: ItemDetailsCallback
) {
    when (editState.item) {
        is Account -> {
            AccountEditContent(
                state = rememberEditAccountStateHolder(editState.foldersList, editState.itemEditWrapper as AccountEditWrapper),
                displayUnsavedAlert = editState.displayConfirmChangesDialog,
                account = editState.item,
                tracker = tracker,
                callback = actionCallBack
            )
        }
        is Note -> NoteEditContent(
            state = rememberEditNoteStateHolder(editState.foldersList, editState.itemEditWrapper as NoteEditWrapper),
            displayUnsavedAlert = editState.displayConfirmChangesDialog,
            callback = actionCallBack
        )
        is CreditCard -> CardEditContent(
            state = rememberEditCardStateHolder(editState.foldersList, editState.itemEditWrapper as CardEditWrapper),
            displayUnsavedAlert = editState.displayConfirmChangesDialog,
            callback = actionCallBack
        )
        is WifiPassword -> WifiEditContent(
            state = rememberEditWifiStateHolder(editState.foldersList, editState.itemEditWrapper as WifiEditWrapper),
            displayUnsavedAlert = editState.displayConfirmChangesDialog,
            callback = actionCallBack
        )
        is OtherItem -> OtherEditContent(
            state = rememberEditOtherStateHolder(editState.foldersList, editState.itemEditWrapper as OtherEditWrapper),
            displayUnsavedAlert = editState.displayConfirmChangesDialog,
            callback = actionCallBack
        )
    }
}

@OptIn(ExperimentalMaterialApi::class, ExperimentalComposeUiApi::class)
@Composable
@Preview
fun SecretDetailsScreenNotePreview() {
    SecretDetailsScreen(
        initialItem = Note.createEmpty(), stateHolder = rememberDetailsScreenStateHolder(
            navigator = get()
        )
    )
}