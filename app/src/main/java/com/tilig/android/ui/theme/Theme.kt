package com.tilig.android.ui.theme

import android.annotation.SuppressLint
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.lightColors
import androidx.compose.material.ripple.LocalRippleTheme
import androidx.compose.material.ripple.RippleAlpha
import androidx.compose.material.ripple.RippleTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.tilig.android.ui.secrets.DetailsMode

val StatusBarHeight = 25.dp
val DefaultPadding = 16.dp
val DoublePadding = 32.dp
val MediumPadding = 24.dp
val HalfPadding = 8.dp
val QuarterPadding = 4.dp
val MediumWithQuarter = 12.dp
val AppBarCollapsedHeight = 56.dp
val AppBarCollapsedTotalHeight = AppBarCollapsedHeight + StatusBarHeight + DefaultPadding

val AccountIconSizeExpanded = 64.dp

fun appBarDetailsExpandedViewHeightDp() = AppBarCollapsedTotalHeight + AccountIconSizeExpanded + 42.dp * 2

fun appBarDetailsExpandedEditHeightDp() = AppBarCollapsedTotalHeight + AccountIconSizeExpanded + 42.dp

fun appBarExpandedHeightDp(mode: DetailsMode): Dp = when (mode) {
    DetailsMode.VIEW -> appBarDetailsExpandedViewHeightDp()
    DetailsMode.EDIT -> appBarDetailsExpandedEditHeightDp()
}

val DefaultShadowOffsetX = 0.dp
val DefaultShadowOffsetY = 1.dp
val DefaultShadowBlur = 4.dp
val DefaultCornerRadius = 10.dp
val DefaultSmallBorder = 1.dp

val DefaultButtonHeight = 52.dp

val MediumCornerRadius = 8.dp

val DefaultErrorMessageHeight = 24.dp

// In case a dark theme is added, this is the day color palette
@SuppressLint("ConflictingOnColor")
private val LightColorPalette = lightColors(
    primary = DarkBlue,
    primaryVariant = LightBlue,
    secondary = White
)

@Composable
fun TiligTheme(
    darkTheme: Boolean = isSystemInDarkTheme(), // this is currently ignored
    content: @Composable () -> Unit
) {
    val colors = LightColorPalette

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes
    ) {
        CompositionLocalProvider(
            LocalRippleTheme provides TiligRippleTheme,
            content = content
        )
    }
}

@Composable
fun TiligOnboardingTheme(
    content: @Composable () -> Unit
) {
    val colors = LightColorPalette

    MaterialTheme(
        colors = colors,
        typography = Typography.copy(
            body1 = Typography.body1.copy(
                color = Color.White
            )
        ),
        shapes = Shapes
    ) {
        CompositionLocalProvider(
            LocalRippleTheme provides TiligRippleTheme,
            content = content
        )
    }
}

/**
 * Changes the ripple color
 */
private object TiligRippleTheme : RippleTheme {

    @Composable
    override fun defaultColor() =
        RippleTheme.defaultRippleColor(
            Grey,
            lightTheme = true
        )

    @Composable
    override fun rippleAlpha(): RippleAlpha =
        RippleTheme.defaultRippleAlpha(
            Grey,
            lightTheme = true
        )
}
