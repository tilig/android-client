package com.tilig.android.ui.onboarding.getstarted.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.lock.LockUtilFactory

@Composable
fun BiometricContent(onUseBiometric: () -> Unit, onSkip: () -> Unit) {

    val usesFingerprint = LockUtilFactory.isBiometricsUsingFingerprint(LocalContext.current)

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = White, shape = Shapes.medium)
            .padding(DefaultPadding)
    ) {
        Text(
            text = stringResource(id = R.string.get_started_step_biometric_desc),
            style = StyleStolzRegularNeutral14
        )
        Image(
            modifier = Modifier
                .padding(vertical = 24.dp)
                .align(Alignment.CenterHorizontally),
            painter = painterResource(id = when (usesFingerprint) {
                true -> R.drawable.ic_fingerprint
                false -> R.drawable.ic_face_id
                else -> R.drawable.ic_fingerprint
            }),
            contentDescription = null
        )
        Spacer(modifier = Modifier.height(8.dp))
        TiligButton(
            modifier = Modifier
                .height(48.dp)
                .fillMaxWidth(),
            iconRes = null,
            text = stringResource(when (usesFingerprint) {
                true -> R.string.use_biometric_fingerprint
                false -> R.string.use_biometric_face_id
                else -> R.string.use_biometric
            }),
            enabled = true,
            isDark = true,
            isSizeConstrained = false,
            onClick = onUseBiometric
        )
        DoItLater(onSkip)
    }
}
