package com.tilig.android.ui.secrets.details

import androidx.annotation.StringRes
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.tilig.android.data.models.tilig.SecretItem
import com.tilig.android.ui.SomethingWentWrongScreen
import com.tilig.android.ui.secrets.details.events.ItemDetailsActionEvent
import com.tilig.android.ui.secrets.details.states.ItemDetailsUiState

/**
 * Depending on state display view or edit mode UI
 */
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SecretStateUIComponent(
    stateHolder: DetailsScreenStateHolder,
    state: ItemDetailsUiState,
    initialItem: SecretItem,
    @StringRes whatWasWrongTextResId: Int,
    itemViewModel: ItemDetailsViewModel,
    onActionEvent: (ItemDetailsActionEvent) -> Unit,
    viewContent: @Composable (ItemDetailsUiState.ItemView) -> Unit,
    editContent: @Composable (ItemDetailsUiState.ItemEdit) -> Unit
) {
    when (state) {
        is ItemDetailsUiState.ItemView -> {
            SecretViewModeComponent(
                swipingState = stateHolder.swipingState,
                secretItem = state.item,
                isLoading = state.isLoading,
                isInInitialState = state.isInInitialState,
                onActionEvent = onActionEvent,
                content = {
                    viewContent(state)
                }
            )
        }
        is ItemDetailsUiState.ItemEdit -> {
            SecretEditViewModeComponent(
                editState = state,
                swipingState = stateHolder.swipingState,
                onActionEvent = onActionEvent,
                content = {
                    editContent(state)
                }
            )
        }
        is ItemDetailsUiState.ItemError -> {
            LaunchEffectDebugReport(state.debugMessage)
            SomethingWentWrongScreen(whatWasWrongText = stringResource(id = whatWasWrongTextResId)) {
                if (state.debugMessage == null) {
                    itemViewModel.getItemInitially(initialItem)
                } else {
                    stateHolder.navigateUp()
                }
            }
        }
    }
}