package com.tilig.android.ui.secrets

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tilig.android.data.models.ApiSuccessResponse
import com.tilig.android.data.models.tilig.Contact
import com.tilig.android.data.models.tilig.ContactsResponse
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

data class ContactsViewModelState(
    val contacts: List<Contact>? = null,
    // Filter contacts query
    val searchFilter: String? = null,
    val filteredContacts: List<Contact>? = null,
    val isLoading: Boolean = false,
)

class ContactsViewModel : ViewModel(), KoinComponent {

    private val repo by inject<Repository>()

    private val _viewModelState = MutableStateFlow(ContactsViewModelState())
    val uiState: StateFlow<ContactsViewModelState> = _viewModelState

    fun fetchContacts(folder: Folder?) {
        _viewModelState.update { it.copy(isLoading = true) }
        viewModelScope.launch(Dispatchers.IO) {
            val contactsResponse = repo.getContacts()
            val contacts: List<Contact>? = if (contactsResponse is ApiSuccessResponse) {
                contactsResponse.body.contacts
            } else {
                null
            }?.filter { contact ->
                // Include `null` values -- this just means the item isn't a folder (yet).
                if (folder == null){
                    return@filter true
                }
                // Exclude members who has access to the folder.
                folder.getAllMemberships()
                    .map { contact.publicKey }
                    .contains(contact.publicKey)
            }
            _viewModelState.update { state ->
                state.copy(
                    isLoading = false,
                    contacts = contacts,
                    filteredContacts = if (!state.searchFilter.isNullOrEmpty()) {
                        state.contacts?.filter {
                            it.name?.contains(
                                state.searchFilter,
                                true
                            ) ?: false || it.email.contains(state.searchFilter, true)
                        }
                    } else {
                        contacts
                    }
                )
            }
        }
    }

    fun search(query: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            _viewModelState.update { state ->
                state.copy(
                    searchFilter = query,
                    filteredContacts = if (!query.isNullOrEmpty()) {
                        state.contacts?.filter {
                            it.name?.contains(
                                query,
                                true
                            ) ?: false || it.email.contains(query, true)
                        }
                    } else {
                        state.contacts
                    }
                )
            }
        }
    }

    fun clearState() {
        _viewModelState.update { state ->
            state.copy(
                searchFilter = null,
                filteredContacts = null,
                contacts = null
            )
        }
    }
}
