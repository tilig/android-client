package com.tilig.android.ui.onboarding.getstarted

import com.tilig.android.data.models.tilig.Account

sealed class GetStartedBottomSheetType {
    class AddAccount(val suggestedAccount: Account?) : GetStartedBottomSheetType()
    class SimulateAutofill(val account: Account) : GetStartedBottomSheetType()
}
