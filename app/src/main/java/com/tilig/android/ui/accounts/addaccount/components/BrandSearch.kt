package com.tilig.android.ui.accounts.addaccount.components

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForwardIos
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.models.tilig.Brand
import com.tilig.android.ui.components.*
import com.tilig.android.ui.modifiers.dropShadow
import com.tilig.android.ui.theme.*

@OptIn(ExperimentalAnimationApi::class, ExperimentalComposeUiApi::class)
@Composable
fun BrandSearch(
    modifier: Modifier = Modifier,
    searchQuery: String?,
    isLoading: Boolean,
    searchResults: List<Brand>,
    onSearchTextChanged: (String) -> Unit,
    onAccountPrefill: (Account) -> Unit
) {
    Card(
        modifier = modifier
            .fillMaxWidth()
            .background(BackgroundGrey)
            .padding(bottom = 8.dp)
            .dropShadow(),
        shape = RectangleShape,
    ) {
        Column(
            modifier = modifier
                .fillMaxWidth()
                .background(White)
        ) {
            Text(
                text = stringResource(id = R.string.label_website_or_app),
                Modifier.padding(
                    top = DefaultPadding,
                    start = DefaultPadding,
                    end = DefaultPadding,
                    bottom = 4.dp
                ),
                style = Typography.h3
            )
            SearchComponent(
                searchText = searchQuery ?: "",
                onSearchTextChanged = onSearchTextChanged,
            )
            {
                LazyColumn(modifier = Modifier.fillMaxHeight()) {
                    item {
                        if (!searchQuery.isNullOrEmpty()) {
                            SearchResultItem(modifier = Modifier.clickable {
                                onAccountPrefill(Account.createBasic(
                                        name = searchQuery,
                                        website = searchQuery
                                    ))
                            }, title = searchQuery, image = R.drawable.ic_add_account)
                        }
                    }
                    items(searchResults, key = { item ->
                        item.id
                    }) { item ->
                        SearchResultItem(modifier = Modifier.clickable {
                            onAccountPrefill(Account.createBasic(
                                name = item.name ?: item.domain,
                                website = item.domain
                            ))
                        }, title = item.domain, image = item.getBrandImage())
                    }
                    if (isLoading) {
                        item {
                            LoadingScreen(modifier = Modifier.fillMaxWidth(), progressSize = 32.dp)
                        }
                    }
                }
            }
        }
    }
}

@ExperimentalAnimationApi
@ExperimentalComposeUiApi
@Composable
fun SearchComponent(
    searchText: String,
    onSearchTextChanged: (String) -> Unit = {},
    content: @Composable () -> Unit = {}
) {

    Box {
        Column(
            modifier = Modifier
                .fillMaxWidth()
        ) {

            SearchField(
                searchText,
                onSearchTextChanged,
            )

            if (searchText.isNotEmpty()) {
                TiligSpacerVerticalExtraSmall()
                content()
            } else {
                TiligSpacerVerticalDefault()
            }
        }

    }
}

@ExperimentalAnimationApi
@ExperimentalComposeUiApi
@Composable
fun SearchField(
    searchText: String,
    onSearchTextChanged: (String) -> Unit = {},
) {
    val focusRequester = remember { FocusRequester() }
    val shape = RoundedCornerShape(8.dp)

    SearchInputField(
        input = searchText,
        onValueChange = onSearchTextChanged,
        hint = stringResource(id = R.string.hint_search_website_or_app),
        modifier = Modifier
            .fillMaxWidth()
            .height(48.dp)
            .padding(horizontal = DefaultPadding)
            .focusRequester(focusRequester)
            .border(DefaultSmallBorder, LightBlue, shape = shape)
    )
}

@Composable
fun SearchResultItem(modifier: Modifier, title: String, image: Any? = R.drawable.ic_acorn_placeholder) {
    Box(
        modifier
            .fillMaxWidth()
            .height(48.dp)
    ) {

        Row(
            Modifier
                .fillMaxSize()
                .padding(horizontal = DefaultPadding),
            verticalAlignment = Alignment.CenterVertically
        ) {
            TiligImage(
                modifier = Modifier
                    .size(24.dp)
                    .padding(0.dp)
                    .clip(DefaultAccountClipShape),
                image = image,
                contentDescription = null,
            )
            Text(
                modifier = Modifier
                    .weight(1f)
                    .padding(start = 12.dp),
                text = title,
                style = StyleStolzRegularDarkBlue14,
            )
            Icon(
                Icons.Filled.ArrowForwardIos,
                "",
                tint = Grey,
                modifier = Modifier
                    .height(12.dp)
            )
        }
    }
}
