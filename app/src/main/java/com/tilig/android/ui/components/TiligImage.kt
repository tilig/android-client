package com.tilig.android.ui.components

import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import coil.compose.rememberAsyncImagePainter
import coil.decode.SvgDecoder
import coil.request.ImageRequest
import com.google.accompanist.drawablepainter.rememberDrawablePainter
import com.tilig.android.R

/**
 * Load image of any source (local/ url)
 */
@Composable
fun TiligImage(
    modifier: Modifier = Modifier,
    image: Any?,
    @DrawableRes placeholder: Int = remember { R.drawable.ic_acorn_placeholder },
    contentDescription: String?
) {

    val painter = if (image is Drawable) {
        rememberDrawablePainter(drawable = image)
    } else if (image is Painter) {
        image
    } else {
        rememberAsyncImagePainter(
            ImageRequest.Builder(LocalContext.current).data(data = image)
                .apply(block = fun ImageRequest.Builder.() {
                    error(placeholder).decoderFactory(SvgDecoder.Factory())
                }).build()
        )
    }

    Image(
        painter = painter,
        contentDescription = contentDescription,
        contentScale = ContentScale.Fit,
        modifier = modifier
    )
}

@Composable
@Preview
fun TiligImagePreview() {
    TiligImage(
        image = painterResource(id = R.drawable.ic_acorn_placeholder),
        contentDescription = null
    )
}
