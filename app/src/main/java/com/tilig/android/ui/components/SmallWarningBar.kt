package com.tilig.android.ui.components

import androidx.annotation.StringRes
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Error
import androidx.compose.material.icons.outlined.ErrorOutline
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.theme.*


@Composable
fun SmallWarningBar(
    @StringRes text: Int
) {
    val shape = RoundedCornerShape(4.dp)
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(DefaultWarnColor, shape)
            .border(
                border = BorderStroke(1.dp, TiligYellow),
                shape = shape
            )
            .padding(start = 10.dp, top = 4.dp, end = 16.dp, bottom = 4.dp)
    ) {
        Icon(
            Icons.Outlined.ErrorOutline,
            "",
            tint = TiligYellow
        )
        Text(
            text = stringResource(id = text),
            style = StyleStolzRegularDarkBlue14,
            modifier = Modifier.padding(start = HalfPadding, top = 2.dp)
        )
    }
}

@Preview(showBackground = true)
@Composable
fun SmallWarningBarPreview() {
    SmallWarningBar(
        text = R.string.warning_add_a_login
    )
}
