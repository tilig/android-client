package com.tilig.android.ui.twofa

import android.Manifest
import android.util.Log
import android.util.Size
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.insets.ProvideWindowInsets
import com.google.accompanist.insets.navigationBarsPadding
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.rememberPermissionState
import com.google.accompanist.permissions.shouldShowRationale
import com.tilig.android.R
import com.tilig.android.TiligApp
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.qrscanner.QrCodeAnalyzer
import com.tilig.android.ui.components.StatusBarAndNavigationBarContrast
import com.tilig.android.ui.theme.*
import com.tilig.android.ui.twofa.bottomsheets.GoToSettingsBottomSheet
import com.tilig.android.ui.twofa.bottomsheets.ManualCodeBottomSheet
import com.tilig.android.ui.twofa.bottomsheets.WrongQRCodeBottomSheet
import com.tilig.android.ui.twofa.bottomsheets.tutorial.TwoFATutorialBottomSheet
import com.tilig.android.ui.twofa.scanner.QRCodeScannerToolbar
import com.tilig.android.ui.twofa.scanner.QRCodeScannerUiState
import com.tilig.android.ui.twofa.scanner.QRScannerViewModel
import com.tilig.android.ui.twofa.scanner.ScannerMessage
import com.tilig.crypto.BuildConfig
import kotlinx.coroutines.launch
import org.koin.androidx.compose.get

const val SHOW_SECURE_CODE_BOTTOM_SHEET = "SHOW_SECURE_CODE_BOTTOM_SHEET"

@OptIn(ExperimentalMaterialApi::class, androidx.compose.ui.ExperimentalComposeUiApi::class)
@Composable
fun QRCodeScannerScreen(
    accountId: String,
    navController: NavController,
    tracker: Tracker,
    viewModel: QRScannerViewModel = viewModel(),
    lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current
) {
    DisposableEffect(lifecycleOwner) {
        val eventObserver = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_CREATE) {
                viewModel.getAccount(uuid = accountId)
            }
        }

        lifecycleOwner.lifecycle.addObserver(eventObserver)

        onDispose {
            lifecycleOwner.lifecycle.removeObserver(eventObserver)
        }
    }
    val uiState by viewModel.uiState.collectAsState()
    val bgColor = uiState.account?.brand?.getBrandColor() ?: DarkBlue
    val scaffoldState = rememberScaffoldState()

    val currentBottomSheet = remember {
        mutableStateOf<QrCodeScannerBottomSheetType?>(null)
    }
    val keyboardController = LocalSoftwareKeyboardController.current
    val scope = rememberCoroutineScope()
    val modalBottomSheetState =
        rememberModalBottomSheetState(
            initialValue = ModalBottomSheetValue.Hidden,
            confirmStateChange = {
                it != ModalBottomSheetValue.HalfExpanded
            })

    val closeSheet: () -> Unit = {
        keyboardController?.hide()
        viewModel.clearQRScannerState()
        currentBottomSheet.value = null
        scope.launch {
            modalBottomSheetState.animateTo(ModalBottomSheetValue.Hidden)
        }
    }
    val openSheet: (type: QrCodeScannerBottomSheetType?) -> Unit = {
        currentBottomSheet.value = it
        scope.launch { modalBottomSheetState.animateTo(ModalBottomSheetValue.Expanded) }
    }
    val onManualCodeEntered: (code: String) -> Boolean = { code ->
        viewModel.handleManualCode(code)
    }

    ProvideWindowInsets {
        TiligTheme {
            ModalBottomSheetLayout(
                modifier = Modifier.fillMaxSize(),
                sheetContent = {
                    BottomSheetContent(
                        bottomSheetType = currentBottomSheet.value,
                        closeSheet = closeSheet,
                        onManualCodeEntered = onManualCodeEntered,
                        tracker = tracker
                    )
                },
                sheetState = modalBottomSheetState,
                sheetShape = RoundedCornerShape(topStart = 16.dp, topEnd = 16.dp),
                sheetBackgroundColor = White,
            ) {
                Scaffold(
                    scaffoldState = scaffoldState,
                    modifier = Modifier.fillMaxSize()
                ) {
                    Column(
                        Modifier
                            .fillMaxSize()
                            .padding(it)
                    ) {
                        StatusBarAndNavigationBarContrast(
                            statusBarBright = false,
                            navigationBarBright = true,
                            navigationBarScrollBehind = true
                        )
                        QRScannerContent(
                            navController = navController,
                            brandColor = bgColor,
                            uiState = uiState,
                            tracker = tracker,
                            viewModel = viewModel,
                            openSheet = openSheet
                        )
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalPermissionsApi::class)
@Composable
private fun QRScannerContent(
    navController: NavController = rememberNavController(),
    brandColor: Color,
    uiState: QRCodeScannerUiState,
    tracker: Tracker = get(),
    viewModel: QRScannerViewModel,
    openSheet: (type: QrCodeScannerBottomSheetType?) -> Unit
) {

    val cameraPermissionState = rememberPermissionState(
        Manifest.permission.CAMERA
    )

    val eventTryToEnableCamera = remember {
        mutableStateOf(false)
    }
    // To track when the user allows camera permission manually
    val cameraPermissionWasNotGranted = remember {
        mutableStateOf(false)
    }

    LaunchedEffect(cameraPermissionState) {
        cameraPermissionState.launchPermissionRequest()
    }

    when {
        cameraPermissionState.status.isGranted -> {
            if (cameraPermissionWasNotGranted.value) {
                tracker.trackEvent(Tracker.EVENT_2FA_CAMERA_ENABLED)
                cameraPermissionWasNotGranted.value = false
            }
            HasCameraPermissionView(
                navController = navController,
                brandColor = brandColor,
                uiState = uiState,
                tracker = tracker,
                openSheet = openSheet,
                viewModel = viewModel
            )
        }
        !cameraPermissionState.status.isGranted -> {
            cameraPermissionWasNotGranted.value = true
            if (eventTryToEnableCamera.value && !cameraPermissionState.status.shouldShowRationale) {
                openSheet(QrCodeScannerBottomSheetType.GoToSettings)
                eventTryToEnableCamera.value = false
            }
            NoCameraPermissionView(
                brandColor = brandColor,
                navController = navController,
                tracker = tracker,
                openSheet = openSheet,
                onEnableCamera = {
                    cameraPermissionState.launchPermissionRequest()
                    eventTryToEnableCamera.value = true
                }
            )
        }
    }
}

@Composable
private fun HasCameraPermissionView(
    navController: NavController,
    brandColor: Color,
    uiState: QRCodeScannerUiState,
    tracker: Tracker,
    openSheet: (type: QrCodeScannerBottomSheetType?) -> Unit,
    viewModel: QRScannerViewModel
) {

    if (uiState is QRCodeScannerUiState.SuccessScan) {
        navController.previousBackStackEntry
            ?.savedStateHandle
            ?.set(SHOW_SECURE_CODE_BOTTOM_SHEET, true)
        TiligApp.debounceClicks {
            navController.navigateUp()
            viewModel.clearQRScannerState()
        }
    }
    if (uiState is QRCodeScannerUiState.WrongQRCode) {
        openSheet(QrCodeScannerBottomSheetType.QRCodeWrong(uiState.account.name ?: ""))
    }

    Box(Modifier.fillMaxSize()) {
        Column(Modifier.fillMaxSize()) {
            QRCodeScannerToolbar(
                navController = navController,
                bgColor = brandColor,
                openTutorial = { openSheet(QrCodeScannerBottomSheetType.Tutorial) },
                openManualCode = { openSheet(QrCodeScannerBottomSheetType.ManualCode) },
                tracker = tracker
            )
            CameraPreview(modifier = Modifier
                .fillMaxSize()
                .weight(1f), viewModel)
        }
        Column(modifier = Modifier.align(Alignment.BottomCenter)) {
            Image(
                painter = painterResource(id = R.drawable.bg_camera_preview),
                contentDescription = null,
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(start = 20.dp, end = 20.dp, bottom = 83.dp)
            )
            if (uiState is QRCodeScannerUiState.Idle) {
                (uiState).scanMessage.let {
                    ScannerMessage(
                        modifier = Modifier
                            .align(Alignment.CenterHorizontally)
                            .fillMaxWidth()
                            .padding(horizontal = DefaultPadding),
                        message = stringResource(id = it.pattern, it.website)
                    )
                }
            }
            Button(
                modifier = Modifier
                    .padding(
                        top = 12.dp,
                        bottom = 20.dp,
                        start = DefaultPadding,
                        end = DefaultPadding
                    )
                    .align(Alignment.CenterHorizontally)
                    .navigationBarsPadding()
                    .height(48.dp)
                    .fillMaxWidth(),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = White,
                    contentColor = White,
                    disabledBackgroundColor = White,
                    disabledContentColor = White
                ),
                shape = RoundedCornerShape(8.dp),
                onClick = { openSheet(QrCodeScannerBottomSheetType.Tutorial) },
            ) {
                Text(text = stringResource(id = R.string.need_help_click_here))
            }
        }

    }
}

@Composable
private fun CameraPreview(
    modifier: Modifier,
    viewModel: QRScannerViewModel
) {
    val contextCurrent = LocalContext.current
    val lifecycleOwner = LocalLifecycleOwner.current
    val cameraProviderFuture = remember {
        ProcessCameraProvider.getInstance(contextCurrent)
    }
    AndroidView(
        factory = { context ->
            val previewView = PreviewView(context)
            val preview = Preview.Builder().build()
            val selector = CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build()
            preview.setSurfaceProvider(previewView.surfaceProvider)
            val size = Size(
                480, //previewView.width,
                640, //previewView.height
            )
            val imageAnalysis = ImageAnalysis.Builder()
                .setTargetResolution(size)
                .setBackpressureStrategy(STRATEGY_KEEP_ONLY_LATEST)
                .build()
            imageAnalysis.setAnalyzer(
                ContextCompat.getMainExecutor(context),
                QrCodeAnalyzer { result ->
                    viewModel.handleQRCode(result)
                    if (BuildConfig.DEBUG) {
                        Log.d("QR-CODE", result)
                    }
                }
            )
            try {
                cameraProviderFuture.get().bindToLifecycle(
                    lifecycleOwner,
                    selector,
                    preview,
                    imageAnalysis
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }
            previewView
        },
        modifier = modifier
    )
}

@Composable
private fun NoCameraPermissionView(
    brandColor: Color,
    navController: NavController = rememberNavController(),
    openSheet: (type: QrCodeScannerBottomSheetType?) -> Unit,
    onEnableCamera: () -> Unit,
    tracker: Tracker = get()
) {

    Box(
        Modifier
            .fillMaxSize()
            .background(color = brandColor)
    ) {
        Image(
            painter = painterResource(id = R.drawable.bg_scanner_no_permission),
            contentDescription = "",
            contentScale = ContentScale.FillBounds,
            modifier = Modifier.fillMaxSize()
        )
        QRCodeScannerToolbar(
            navController = navController,
            showToolbarBackground = false,
            bgColor = brandColor,
            openTutorial = {
                openSheet(QrCodeScannerBottomSheetType.Tutorial)
            },
            openManualCode = {
                openSheet(QrCodeScannerBottomSheetType.ManualCode)
            },
            tracker = tracker
        )
        Column(modifier = Modifier.align(Alignment.BottomCenter)) {
            Box(
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(start = 20.dp, end = 20.dp, bottom = 83.dp)
            ) {
                Image(
                    painter = painterResource(id = R.drawable.bg_camera_preview),
                    contentDescription = null,
                )
                Column(modifier = Modifier.align(Alignment.Center)) {
                    Image(
                        modifier = Modifier.align(Alignment.CenterHorizontally),
                        painter = painterResource(id = R.drawable.ic_camera),
                        contentDescription = null
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    Text(
                        text = stringResource(id = R.string.please_give_permissions),
                        color = White,
                        textAlign = TextAlign.Center
                    )
                }
            }
            TextButton(
                modifier = Modifier
                    .padding(
                        top = 12.dp,
                        start = DefaultPadding,
                        end = DefaultPadding
                    )
                    .align(Alignment.CenterHorizontally)
                    .background(color = LightBlue, shape = RoundedCornerShape(8.dp))
                    .fillMaxWidth(),
                onClick = {
                    onEnableCamera()
                },
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_camera),
                    contentDescription = null,
                    modifier = Modifier.size(16.dp)
                )
                Spacer(Modifier.width(8.dp))
                Text(
                    text = stringResource(id = R.string.enable_camera),
                    style = TextStyle(color = White, fontFamily = Stolzl, fontSize = 14.sp)
                )
            }
            TextButton(
                modifier = Modifier
                    .padding(
                        top = 16.dp,
                        bottom = 20.dp,
                        start = DefaultPadding,
                        end = DefaultPadding
                    )
                    .align(Alignment.CenterHorizontally)
                    .navigationBarsPadding()
                    .background(color = Color.Transparent)
                    .border(1.dp, color = White, shape = RoundedCornerShape(8.dp))
                    .fillMaxWidth(),
                onClick = {
                    tracker.trackEvent(Tracker.EVENT_2FA_ADD_QR_MANUALLY_TAPPED)
                    openSheet(QrCodeScannerBottomSheetType.ManualCode)
                },
            ) {
                Text(
                    text = stringResource(id = R.string.no_camera_enter_manually),
                    style = TextStyle(color = White, fontFamily = Stolzl, fontSize = 14.sp)
                )
            }
        }

    }
}

@Composable
private fun BottomSheetContent(
    tracker: Tracker,
    bottomSheetType: QrCodeScannerBottomSheetType?,
    closeSheet: () -> Unit,
    onManualCodeEntered: (code: String) -> Boolean,
) {
    Box(Modifier.defaultMinSize(minHeight = 1.dp)) {
        bottomSheetType?.let { type ->
            when (type) {
                is QrCodeScannerBottomSheetType.Tutorial -> {
                    TwoFATutorialBottomSheet(closeSheet = closeSheet) {}
                }
                is QrCodeScannerBottomSheetType.QRCodeWrong -> {
                    WrongQRCodeBottomSheet(closeSheet = closeSheet, accountName = type.accountName)
                }
                is QrCodeScannerBottomSheetType.ManualCode -> {
                    ManualCodeBottomSheet(
                        tracker = tracker,
                        closeSheet = closeSheet,
                        onManualCodeEntered = onManualCodeEntered,
                    )
                }
                is QrCodeScannerBottomSheetType.GoToSettings -> {
                    GoToSettingsBottomSheet(closeSheet = closeSheet)
                }
            }
        }
    }
}

private sealed class QrCodeScannerBottomSheetType {
    object Tutorial : QrCodeScannerBottomSheetType()
    class QRCodeWrong(val accountName: String) : QrCodeScannerBottomSheetType()
    object ManualCode : QrCodeScannerBottomSheetType()
    object GoToSettings : QrCodeScannerBottomSheetType()
}

@Composable
@androidx.compose.ui.tooling.preview.Preview
private fun NoCameraPermissionPreview() {
    NoCameraPermissionView(
        brandColor = Color.Cyan,
        openSheet = { },
        onEnableCamera = {}
    )
}

@Composable
@androidx.compose.ui.tooling.preview.Preview
private fun QRScannerContentPreview() {
    QRScannerContent(
        brandColor = Color.Cyan,
        uiState = QRCodeScannerUiState.Empty,
        viewModel = viewModel(),
        openSheet = {}
    )
}

@Composable
@androidx.compose.ui.tooling.preview.Preview
fun QRCodeScannerScreenPreview() {
    QRCodeScannerScreen(accountId = "1", navController = get(), tracker = get())
}
