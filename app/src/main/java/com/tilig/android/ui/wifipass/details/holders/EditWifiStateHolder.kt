package com.tilig.android.ui.wifipass.details.holders

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.Folders
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.secrets.CustomFieldStateWrapper
import com.tilig.android.ui.wifipass.details.WifiEditWrapper

data class EditWifiStateHolder(
    val foldersList: Folders?,
    val nameState: TextFieldState,
    val passwordState: TextFieldState,
    val networkNameState: TextFieldState,
    val extraInfoState: TextFieldState,
    val customFields: List<CustomFieldStateWrapper>,
    val selectedFolder: Folder?
)

@Composable
fun rememberEditWifiStateHolder(
    foldersList: Folders?,
    wifiEditWrapper: WifiEditWrapper
) =
    EditWifiStateHolder(
        foldersList = foldersList,
        nameState = remember {
            TextFieldState(
                wifiEditWrapper.name,
                validator = { it.isNotEmpty() })
        },
        passwordState = remember {
            TextFieldState(
                wifiEditWrapper.password,
                validator = { it.isNotEmpty() })
        },
        networkNameState = remember {
            TextFieldState(wifiEditWrapper.networkName)
        },
        extraInfoState = remember {
            TextFieldState(
                wifiEditWrapper.extraInfo
            )
        },
        customFields = wifiEditWrapper.customFields?.map {
            CustomFieldStateWrapper(it)
        } ?: emptyList(),
        selectedFolder = wifiEditWrapper.selectedFolder
    )