package com.tilig.android.ui.autofill.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.theme.DarkBlue
import com.tilig.android.ui.theme.StyleSpoofBoldDarkBlue28
import com.tilig.android.ui.theme.StyleStolzRegularDarkBlue14

@Composable
fun AutofillNoAccountsView(modifier: Modifier, onAddAccountClick: () -> Unit) {
    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.height(32.dp))
        Image(
            modifier = Modifier,
            painter = painterResource(id = R.drawable.ic_squirrel_dark),
            contentDescription = "",
            contentScale = ContentScale.FillWidth,
        )
        Text(
            modifier = Modifier.padding(vertical = 8.dp),
            text = stringResource(id = R.string.no_item_stored_title),
            style = StyleSpoofBoldDarkBlue28,
        )
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 32.dp),
            text = stringResource(id = R.string.no_item_stored_autofill_desc),
            textAlign = TextAlign.Center,
            style = StyleStolzRegularDarkBlue14,
            color = DarkBlue.copy(alpha = 0.75f)
        )
        Spacer(modifier = Modifier.height(48.dp))
        TiligButton(
            modifier = Modifier,
            iconRes = R.drawable.ic_add,
            text = stringResource(id = R.string.add_a_new_login),
            enabled = true,
            isSizeConstrained = false,
            isDark = true,
            withShadow = false,
            onClick = onAddAccountClick
        )
    }
}