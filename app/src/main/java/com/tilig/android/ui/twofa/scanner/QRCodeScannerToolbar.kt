package com.tilig.android.ui.twofa.scanner

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.tilig.android.R
import com.tilig.android.TiligApp
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.theme.*

@Composable
fun QRCodeScannerToolbar(
    navController: NavController,
    bgColor: Color,
    showToolbarBackground: Boolean = true,
    openTutorial: () -> Unit,
    openManualCode: () -> Unit,
    tracker: Tracker,
) {
    Box(
        Modifier
            .height(AppBarCollapsedTotalHeight)
            .fillMaxWidth()
    ) {
        if (showToolbarBackground) {
            Image(
                modifier = Modifier
                    .fillMaxSize()
                    .background(bgColor),
                painter = painterResource(id = R.drawable.img_header_background),
                contentDescription = "",
                contentScale = ContentScale.FillBounds,
            )
        }
        TopAppBar(
            backgroundColor = Color.Transparent,
            modifier = Modifier
                .height(AppBarCollapsedTotalHeight)
                .padding(top = StatusBarHeight),
            elevation = 0.dp,
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight(align = Alignment.CenterVertically)
                    .align(Alignment.CenterVertically)
                    .padding(horizontal = 8.dp)
            ) {
                IconButton(
                    modifier = Modifier
                        .then(Modifier.size(32.dp))
                        .align(Alignment.CenterStart),
                    onClick = {
                        TiligApp.debounceClicks {
                            navController.navigateUp()
                        }
                    }) {
                    Icon(
                        modifier = Modifier.size(16.dp),
                        painter = painterResource(id = R.drawable.ic_close),
                        contentDescription = null,
                        tint = White
                    )
                }
                Text(
                    text = stringResource(id = R.string.scan_qr_code),
                    style = TextStyle(color = White, fontFamily = Stolzl, fontSize = 16.sp),
                    modifier = Modifier
                        .align(Alignment.Center)
                        .fillMaxWidth(),
                    textAlign = TextAlign.Center
                )
                Row(
                    modifier = Modifier
                        .wrapContentSize(align = Alignment.TopEnd)
                        .align(Alignment.CenterEnd)
                ) {
                    IconButton(modifier = Modifier.size(32.dp), onClick = {
                        tracker.trackEvent(Tracker.EVENT_2FA_ADD_QR_MANUALLY_TAPPED)
                        openManualCode()
                    }) {
                        Icon(painterResource(id = R.drawable.ic_keyboard), null, tint = White)
                    }
                    Spacer(modifier = Modifier.width(DefaultPadding))
                    IconButton(
                        modifier = Modifier.size(32.dp),
                        onClick = {
                            tracker.trackEvent(Tracker.EVENT_2FA_READ_MORE_TAPPED)
                            openTutorial()
                        }) {
                        Icon(painterResource(id = R.drawable.ic_question_mark), null, tint = White)
                    }
                }
            }
        }
    }
}
