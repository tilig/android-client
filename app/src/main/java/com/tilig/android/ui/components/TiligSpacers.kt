package com.tilig.android.ui.components

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun TiligSpacerVerticalLarge(modifier: Modifier = Modifier){
    Spacer(modifier = modifier.height(32.dp))
}

@Composable
fun TiligSpacerVerticalMediumLarge(modifier: Modifier = Modifier){
    Spacer(modifier = modifier.height(24.dp))
}

@Composable
fun TiligSpacerVerticalDefault(modifier: Modifier = Modifier){
    Spacer(modifier = modifier.height(16.dp))
}

@Composable
fun TiligSpacerVerticalMedium(modifier: Modifier = Modifier){
    Spacer(modifier = modifier.height(12.dp))
}

@Composable
fun TiligSpacerVerticalSmall(modifier: Modifier = Modifier){
    Spacer(modifier = modifier.height(8.dp))
}

@Composable
fun TiligSpacerVerticalExtraSmall(modifier: Modifier = Modifier){
    Spacer(modifier = modifier.height(4.dp))
}

@Composable
fun TiligSpacerHorizontalExtraSmall(modifier: Modifier = Modifier){
    Spacer(modifier = modifier.width(4.dp))
}

@Composable
fun TiligSpacerHorizontalSmall(modifier: Modifier = Modifier){
    Spacer(modifier = modifier.width(8.dp))
}

@Composable
fun TiligSpacerHorizontalDefault(modifier: Modifier = Modifier){
    Spacer(modifier = modifier.width(16.dp))
}

@Composable
fun TiligSpacerHorizontalMedium(modifier: Modifier = Modifier){
    Spacer(modifier = modifier.width(24.dp))
}