package com.tilig.android.ui.settings.components

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Cancel
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.outlined.Close
import androidx.compose.material.icons.outlined.Warning
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.tilig.android.R
import com.tilig.android.analytics.Breadcrumb
import com.tilig.android.data.livedata.ForcedLogoutLiveData
import com.tilig.android.data.models.SecretItemType
import com.tilig.android.data.models.tilig.OkResponse
import com.tilig.android.data.models.tilig.SecretItem
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.components.*
import com.tilig.android.ui.secrets.SecretItemsUiState
import com.tilig.android.ui.secrets.SecretItemsViewModel
import com.tilig.android.ui.signin.AuthViewModel
import com.tilig.android.ui.signin.SignInViewModel
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.SharedPrefs
import io.sentry.Sentry
import io.sentry.SentryLevel
import org.koin.androidx.compose.get
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@Composable
fun DangerZone(
    authStatus: ForcedLogoutLiveData = get(),
    prefs: SharedPrefs = get(),
    authViewModel: AuthViewModel = viewModel(),
    secretItemsViewModel: SecretItemsViewModel = viewModel()
) {

    val uiState by secretItemsViewModel.uiState.collectAsState()
    LaunchedEffect(secretItemsViewModel) {
        secretItemsViewModel.secretItems()
    }

    val context = LocalContext.current

    val showProgress = remember { mutableStateOf(false) }

    FieldTitle(
        title = stringResource(id = R.string.label_dangerzone),
        titlePadding = HalfPadding,
        style = StyleSpoofMediumDarkBlue20
    )
    DeleteAccountComponent(
        onConfirm = {
            if (!showProgress.value) {
                showProgress.value = true
                removeFromFirestore(
                    authStatus,
                    showProgress,
                    prefs,
                    context,
                    authViewModel
                )
            }
        },
        items = (uiState as? SecretItemsUiState.SecretItemsSuccess)?.filteredSecretItemsByDisplayType,
        showProgress = showProgress.value
    )
}

@Composable
private fun DeleteAccountComponent(
    modifier: Modifier = Modifier,
    showProgress: Boolean,
    items: Map<SecretItemType?, List<SecretItem>>?,
    onConfirm: () -> Unit
) {
    val isInitialState = remember {
        mutableStateOf(true)
    }

    val openDialog = remember {
        mutableStateOf(false)
    }

    if (isInitialState.value) {
        DeleteAccountRow(modifier = modifier, onClick = {
            isInitialState.value = false
        })
    } else {
        DeleteAccountConfirm(modifier = modifier, onCancel = {
            isInitialState.value = true
        }, onConfirm = {
            openDialog.value = true
        })
    }
    if (openDialog.value) {
        ConfirmDeleteDialog(items, onConfirmDelete = {
            onConfirm.invoke()
        }, onDismissRequest = {
            openDialog.value = false
        },
            showProgress = showProgress
        )
    }
}

private fun buildStringOfItems(
    context: Context,
    items: Map<SecretItemType?, List<SecretItem>>?
): String {
    if (items.isNullOrEmpty()) {
        return ""
    } else {
        return buildString {
            SecretItemType.values().forEach {
                val typedItems = items[it]
                if (!typedItems.isNullOrEmpty()) {
                    val appendableStringRes = when (it) {
                        SecretItemType.ACCOUNT -> if (typedItems.size == 1) R.string.login else R.string.logins
                        SecretItemType.NOTE -> if (typedItems.size == 1) R.string.secure_note else R.string.secure_notes
                        SecretItemType.CREDIT_CARD -> if (typedItems.size == 1) R.string.credit_card else R.string.credit_cards
                        SecretItemType.WIFI -> if (typedItems.size == 1) R.string.wifi_network else R.string.wifi_networks
                        SecretItemType.OTHER -> if (typedItems.size == 1) R.string.other_small else R.string.others
                    }
                    append(", ${typedItems.size} ${context.getString(appendableStringRes)}")
                }
            }
        }
    }
}

@Composable
private fun DeleteAccountRow(modifier: Modifier = Modifier, onClick: () -> Unit) {
    Row(
        modifier = modifier
            .background(color = White, shape = RoundedCornerShape(MediumCornerRadius))
            .fillMaxWidth()
            .padding(DefaultPadding)
            .clickable { onClick.invoke() },
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            modifier = Modifier.size(20.dp),
            imageVector = Icons.Filled.Cancel,
            contentDescription = stringResource(id = R.string.label_delete_tilig_account),
            tint = SettingsWarnColor
        )
        TiligSpacerHorizontalExtraSmall()
        Text(
            text = stringResource(id = R.string.label_delete_tilig_account),
            style = StyleStolzRegularDarkBlue14,
            color = SettingsWarnColor,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )
    }
}

@Composable
private fun DeleteAccountConfirm(
    modifier: Modifier = Modifier,
    onCancel: () -> Unit,
    onConfirm: () -> Unit
) {
    Column(
        modifier = modifier
            .background(color = Pink2, shape = RoundedCornerShape(MediumCornerRadius))
            .fillMaxWidth()
            .border(width = 1.dp, color = Pink, shape = RoundedCornerShape(MediumCornerRadius))
            .padding(DefaultPadding),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(id = R.string.dialog_delete_all),
            style = StyleStolzRegularDarkBlue14
        )
        TiligSpacerVerticalDefault()
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                modifier = Modifier.size(12.dp),
                imageVector = Icons.Outlined.Warning,
                contentDescription = stringResource(id = R.string.dialog_cannot_undone),
                tint = DimmedBlue
            )
            Text(
                modifier = Modifier.padding(start = 4.dp),
                text = stringResource(id = R.string.dialog_cannot_undone),
                style = StyleStolzRegularDarkBlue12,
                color = DimmedBlue,
            )
        }
        TiligSpacerVerticalDefault()
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {

            TextButton(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f)
                    .height(48.dp),
                elevation = ButtonDefaults.elevation(
                    defaultElevation = 0.dp,
                    focusedElevation = 0.dp,
                    hoveredElevation = 0.dp
                ),
                shape = RoundedCornerShape(8.dp),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Pink2,
                ),
                onClick = onCancel
            ) {
                Text(
                    modifier = Modifier,
                    text = stringResource(id = R.string.cancel),
                    textAlign = TextAlign.Center,
                    style = StyleStolzRegularDarkBlue14,
                    color = DarkBlue.copy(alpha = 0.75f)
                )
            }

            TiligSpacerHorizontalDefault()

            TextButton(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(48.dp)
                    .weight(1f)
                    .background(
                        color = WarningButtonTextColor,
                        shape = RoundedCornerShape(MediumCornerRadius)
                    ),
                elevation = ButtonDefaults.elevation(
                    defaultElevation = 0.dp,
                    focusedElevation = 0.dp,
                    hoveredElevation = 0.dp
                ),
                shape = RoundedCornerShape(8.dp),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = SettingsWarnColor,
                ),
                onClick = onConfirm
            ) {
                Text(
                    stringResource(id = R.string.yes_confirm),
                    style = StyleStolzRegularDarkBlue14,
                    color = White
                )
            }
        }
    }
}

@Composable
fun ConfirmDeleteDialog(
    items: Map<SecretItemType?, List<SecretItem>>?,
    showProgress: Boolean,
    onDismissRequest: () -> Unit,
    onConfirmDelete: () -> Unit
) {
    val context = LocalContext.current
    val confirmText =
        remember {
            TextFieldState(
                "",
                validator = { it.trim().equals(context.getString(R.string.delete_caps), true) })
        }
    val showIncorrectTextMessage = remember { mutableStateOf(false) }

    Dialog(
        onDismissRequest = onDismissRequest,
        content = {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .background(color = White, shape = RoundedCornerShape(DefaultCornerRadius))
                    .clip(RoundedCornerShape(DefaultCornerRadius))
            ) {
                Button(
                    modifier = Modifier
                        .padding(0.dp)
                        .align(Alignment.TopEnd)
                        .wrapContentSize(),
                    colors = ButtonDefaults.buttonColors(backgroundColor = Color.Transparent),
                    elevation = null, onClick = onDismissRequest,
                    contentPadding = PaddingValues(0.dp)
                ) {
                    Icon(
                        modifier = Modifier
                            .size(24.dp),
                        imageVector = Icons.Outlined.Close,
                        contentDescription = null,
                        tint = Black
                    )
                }

                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight()
                        .padding(top = 16.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Icon(
                        modifier = Modifier.size(24.dp),
                        imageVector = Icons.Filled.Cancel,
                        contentDescription = stringResource(id = R.string.label_delete_tilig_account),
                        tint = SettingsWarnColor
                    )

                    TiligSpacerVerticalExtraSmall()

                    Text(
                        text = stringResource(id = R.string.dialog_title_delete_account),
                        style = StyleStolzMediumDarkBlue20,
                    )

                    TiligSpacerVerticalSmall()

                    Text(
                        modifier = Modifier.padding(horizontal = 22.dp),
                        text = stringResource(
                            id = R.string.delete_account_description, buildStringOfItems(
                                LocalContext.current, items
                            )
                        ),
                        style = StyleStolzRegularDarkBlue14,
                    )

                    TiligSpacerVerticalSmall()

                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentHeight()
                            .background(color = Pink2)
                            .padding(start = 22.dp, end = 22.dp, bottom = 8.dp)
                    ) {

                        TiligSpacerVerticalSmall()

                        AppendableTextView(
                            appendableTexts = listOf(
                                AppendableText(
                                    stringResource(id = R.string.dialog_delete_confirm_by_typing),
                                    false,
                                    addSpace = true,
                                ),
                                AppendableText(
                                    stringResource(id = R.string.delete_caps),
                                    true
                                ),
                                AppendableText(
                                    stringResource(id = R.string.delete_below),
                                    addSpace = true,
                                    isStyled = false
                                )
                            ),
                            textStyle = StyleStolzRegularDarkBlue14,
                            spanStyle = SpanStyle(
                                SettingsWarnColor,
                                fontWeight = FontWeight.Medium
                            ),
                            modifier = Modifier
                        )

                        TiligSpacerVerticalDefault()

                        BorderedBasicTextField(
                            hint = stringResource(id = R.string.delete_caps),
                            state = confirmText,
                            borderTextFieldColors = borderTextFieldColors(
                                errorColor = SettingsWarnColor
                            ),
                            onValueChanged = {
                                showIncorrectTextMessage.value = false
                            }
                        )

                        if (showIncorrectTextMessage.value) {
                            Row(verticalAlignment = Alignment.CenterVertically) {
                                Icon(
                                    modifier = Modifier.size(12.dp),
                                    imageVector = Icons.Filled.Info,
                                    contentDescription = null,
                                    tint = SettingsWarnColor
                                )
                                Text(
                                    text = stringResource(id = R.string.delete_exact_text),
                                    style = StyleStolzRegularDarkBlue12,
                                    color = SettingsWarnColor
                                )
                            }
                        }

                        TiligSpacerVerticalSmall()

                        TextButton(
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(48.dp),
                            elevation = ButtonDefaults.elevation(
                                defaultElevation = 0.dp,
                                focusedElevation = 0.dp,
                                hoveredElevation = 0.dp
                            ),
                            shape = RoundedCornerShape(8.dp),
                            colors = ButtonDefaults.buttonColors(
                                backgroundColor = SettingsWarnColor,
                                disabledBackgroundColor = SettingsWarnColor.copy(alpha = 0.5f)
                            ),
                            enabled = confirmText.text.isNotEmpty(),
                            onClick = {
                                if (confirmText.text.trim()
                                        .equals(context.getString(R.string.delete_caps), true)
                                ) {
                                    onConfirmDelete.invoke()
                                } else {
                                    showIncorrectTextMessage.value = true
                                }
                            }
                        ) {
                            if (showProgress) {
                                CircularProgressIndicator(
                                    modifier = Modifier
                                        .size(32.dp),
                                    color = White,
                                    strokeWidth = 4.dp
                                )
                            } else {
                                Text(
                                    stringResource(id = R.string.dialog_delete_forever),
                                    style = StyleStolzRegularDarkBlue14,
                                    color = White
                                )
                            }
                        }

                        TiligSpacerVerticalSmall()

                        TiligButton(
                            modifier = Modifier
                                .align(alignment = Alignment.CenterHorizontally)
                                .fillMaxWidth()
                                .height(48.dp),
                            iconRes = null,
                            text = stringResource(R.string.cancel),
                            enabled = true,
                            isSizeConstrained = false,
                            isDark = false,
                            onClick = onDismissRequest
                        )
                    }
                }
            }
        },
    )
}


private fun removeFromFirestore(
    authStatus: ForcedLogoutLiveData,
    showProgress: MutableState<Boolean>,
    prefs: SharedPrefs,
    context: Context,
    viewModel: AuthViewModel
) {
    val db = FirebaseFirestore.getInstance()
    db
        .collection(SignInViewModel.FIRESTORE_COLLECTION)
        .document(prefs.firebaseUserId!!)
        .delete()
        .addOnSuccessListener {
            Breadcrumb.drop(Breadcrumb.DeleteAccountTrail_FirebaseSuccess)
            removeFromBackend(
                authStatus,
                showProgress,
                viewModel
            )
        }
        .addOnFailureListener {
            Breadcrumb.drop(Breadcrumb.DeleteAccountTrail_FirebaseFailure)
            Sentry.captureMessage(
                "Unable to delete profile from Firebase: ${it.message}",
                SentryLevel.WARNING
            )
            showProgress.value = false
            Toast.makeText(
                context,
                R.string.error_delete_profile_firebase,
                Toast.LENGTH_LONG
            ).show()
        }
}

private fun removeFromBackend(
    authStatus: ForcedLogoutLiveData,
    showProgress: MutableState<Boolean>,
    viewModel: AuthViewModel
) {
    viewModel.deleteFromTiligServer().enqueue(object : Callback<OkResponse> {
        override fun onFailure(call: Call<OkResponse>, t: Throwable) {
            Log.e("Settings", "Unable to delete profile from Tilig server", t)
            Breadcrumb.drop(Breadcrumb.DeleteAccountTrail_BackendFailure)
            Sentry.captureMessage(
                "Unable to delete profile from Tilig server: ${t.message}",
                SentryLevel.WARNING
            )
            showProgress.value = false
            // Don't notify the user, too late to do anything now.
            // Their account is deleted from firebase so they'll make a new one anyway if they every log in
            authStatus.forceLogout(true, userExists = false)
        }

        override fun onResponse(
            call: Call<OkResponse>,
            response: Response<OkResponse>
        ) {
            Breadcrumb.drop(Breadcrumb.DeleteAccountTrail_BackendSuccess)
            showProgress.value = false
            authStatus.forceLogout(forceLogout = true, userExists = false)
        }
    })
}

@Preview
@Composable
fun DangerZonePreview() {
    DangerZone(
        authStatus = get(),
        prefs = get(),
        authViewModel = viewModel(),
        secretItemsViewModel = viewModel()
    )
}
