package com.tilig.android.ui.onboarding.getstarted.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.tilig.android.R
import com.tilig.android.ui.components.TiligSpacerVerticalDefault
import com.tilig.android.ui.components.TiligSpacerVerticalSmall
import com.tilig.android.ui.theme.*

@Composable
fun GetStartedConfirmDismissDialog(
    progress: Float,
    onClose: () -> Unit,
    onConfirmDismiss: () -> Unit
) {

    val titleRes = if (progress == 0f) {
        R.string.are_you_sure
    } else if (progress == 0.25f) {
        R.string.doing_great
    } else if (progress == 0.5f) {
        R.string.only_two_to_go
    } else {
        R.string.you_nearly_there
    }

    Dialog(
        onDismissRequest = onClose,
        content = {
            Box(
                modifier = Modifier
                    .padding(horizontal = DefaultPadding)
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .background(color = White, shape = RoundedCornerShape(DefaultCornerRadius))
                    .clip(RoundedCornerShape(DefaultCornerRadius))
            ) {
                Button(
                    modifier = Modifier
                        .padding(top = 8.dp)
                        .align(Alignment.TopEnd)
                        .wrapContentSize(),
                    colors = ButtonDefaults.buttonColors(backgroundColor = Color.Transparent),
                    elevation = null, onClick = onClose,
                    contentPadding = PaddingValues(0.dp)
                ) {
                    Image(
                        modifier = Modifier
                            .size(32.dp),
                        painter = painterResource(id = R.drawable.ic_close_rounded),
                        contentDescription = null,
                    )
                }

                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight()
                        .padding(top = 64.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Image(
                        modifier = Modifier.size(48.dp),
                        painter = painterResource(id = R.drawable.ic_squirrel_dark),
                        contentDescription = null
                    )

                    TiligSpacerVerticalDefault()

                    Text(
                        text = stringResource(id = titleRes),
                        style = StyleSpoofMediumBlue24,
                    )

                    TiligSpacerVerticalSmall()

                    Text(
                        modifier = Modifier.padding(horizontal = DefaultPadding),
                        text = stringResource(id = R.string.get_started_desc_complete_steps),
                        style = StyleStolzRegularNeutral16,
                        textAlign = TextAlign.Center
                    )

                    Spacer(modifier = Modifier.height(40.dp))

                    TextButton(
                        modifier = Modifier
                            .padding(horizontal = DefaultPadding)
                            .fillMaxWidth()
                            .height(48.dp),
                        elevation = ButtonDefaults.elevation(
                            defaultElevation = 0.dp,
                            focusedElevation = 0.dp,
                            hoveredElevation = 0.dp
                        ),
                        shape = RoundedCornerShape(8.dp),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = DarkRedColor,
                        ),
                        enabled = true,
                        onClick = onConfirmDismiss
                    ) {
                        Text(
                            stringResource(id = R.string.get_started_dismiss_forever),
                            style = StyleStolzRegularDarkBlue14,
                            color = White
                        )
                    }

                    TiligSpacerVerticalSmall()

                    TextButton(
                        modifier = Modifier.align(Alignment.CenterHorizontally),
                        onClick = onClose,
                    ) {
                        Text(
                            stringResource(id = R.string.take_me_back),
                            style = StyleStolzRegularDarkBlue14
                        )
                    }
                }
            }
        },
    )
}