package com.tilig.android.ui.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.tooling.preview.Preview
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import androidx.navigation.findNavController
import com.tilig.android.analytics.ExperimentalKey
import com.tilig.android.analytics.Experiments
import com.tilig.android.analytics.FlagVariant
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.ui.autofill.components.instructions.AutofillSetupDoneContentComponent
import com.tilig.android.ui.components.StatusBarAndNavigationBarContrast
import com.tilig.android.utils.SharedPrefs
import com.tilig.android.utils.catchNavigate
import org.koin.androidx.compose.get

class OnboardingAutofillDoneFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        return ComposeView(requireContext()).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            setContent {
                OnboardingAutofillDoneScreen(findNavController())
            }
        }
    }
}

@Preview
@Composable
fun OnboardingAutofillDoneScreen(
    navController: NavController = rememberNavController(),
    tracker: Mixpanel = get(),
    experiments: Experiments = get(),
    preferences: SharedPrefs = get(),
) {
    val surveyEnabled = remember {
        experiments.flagVariant(ExperimentalKey.SurveyEnabled)
    }

    Surface(
        modifier = Modifier
            .fillMaxSize()
            .navigationBarsPadding(),
        color = MaterialTheme.colors.background
    ) {
        StatusBarAndNavigationBarContrast(
            statusBarBright = true,
            navigationBarBright = true,
            navigationBarScrollBehind = false
        )
        AutofillSetupDoneContentComponent{
            if (!preferences.isSurveyFinished && surveyEnabled == FlagVariant.ENABLED) {
                navController.catchNavigate(
                    OnboardingAutofillDoneFragmentDirections.actionOnboardingAutofillDoneFragmentToSurveyIntroFragment()
                )
            } else {
                navController.catchNavigate(
                    OnboardingAutofillDoneFragmentDirections.actionOnboardingAutofillDoneFragmentToHomeFragment()
                )
            }
        }
    }
}
