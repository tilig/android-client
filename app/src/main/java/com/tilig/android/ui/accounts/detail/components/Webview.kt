package com.tilig.android.ui.accounts.detail.components

import android.content.Context
import android.os.Build
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.OpenInNew
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.autofill.PackageVerifier
import com.tilig.android.autofill.TiligService
import com.tilig.android.testdrive.TestDrive
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.components.BorderedBasicTextField
import com.tilig.android.ui.components.ErrorHandler
import com.tilig.android.ui.secrets.components.BoxWithTitledContent
import com.tilig.android.ui.secrets.components.TitledContent
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.openLink
import com.tilig.android.utils.tryOpenSpecificApp

@Composable
fun WebView(
    websiteUrl: String?,
    androidAppId: String?,
    tracker: Tracker,
    onCopyClick: (() -> Unit)
) {
    val context = LocalContext.current
    val isApp = remember { !androidAppId.isNullOrEmpty() }

    val onOpenWebsiteClick: (() -> Unit) = {
        // Pre-load the autofill service for a speedy response
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            TiligService.preload(context)
        }
        if (androidAppId == null) {
            // Open website
            if (context.openLink(url = websiteUrl)) {
                tracker.trackEvent(
                    if (websiteUrl == TestDrive.getLocalAccountWebsite())
                        Tracker.EVENT_OPEN_TEST_WEBSITE
                    else
                        Tracker.EVENT_OPEN_WEBSITE
                )
            }
        } else {
            context.tryOpenSpecificApp(appId = androidAppId)
        }
    }

    BoxWithTitledContent(
        modifier = Modifier
            .fillMaxWidth(),
        titleStr = stringResource(id = if (isApp) R.string.label_login_application_id else R.string.label_login_website),
        tracker = tracker,
        event = null,
        onClick = onCopyClick
    ) {
        val fieldValue = if (isApp) getAppName(context, androidAppId) else websiteUrl

        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Box(modifier = Modifier.weight(1f)) {
                fieldValue?.let {
                    BasicTextField(
                        modifier = Modifier.clickable {
                            onOpenWebsiteClick.invoke()
                        },
                        value = it,
                        textStyle = StyleStolzRegularWeblink,
                        onValueChange = {},
                        readOnly = true,
                        enabled = false
                    )
                }
            }
            if (!isApp && !websiteUrl.isNullOrEmpty()) {
                Icon(
                    Icons.Default.OpenInNew,
                    contentDescription = "",
                    modifier = Modifier
                        .size(24.dp)
                        .clickable {
                            onOpenWebsiteClick.invoke()
                        }
                )
            }
        }
//        DetailsTextField(
//            showIcon = !isApp && !websiteUrl.isNullOrEmpty(),
//            value = if (isApp) getAppName(context, androidAppId) else websiteUrl,
//            textStyle = StyleStolzRegularWeblink,
//            onActionButtonClick = onClick
//        )
    }
}

@Composable
fun WebViewEdit(
    modifier: Modifier = Modifier,
    isApp: Boolean,
    androidAppId: String?,
    websiteState: TextFieldState,
    focusManager: FocusManager,
    titleTextStyle: TextStyle = StyleStolzRegularDarkBlue12.copy(color = DarkBlue.copy(alpha = 0.75f)),
    contentTextStyle: TextStyle = StyleStolzRegularDarkBlue16,
    onFocusLost: () -> Unit = {},
    onValueChanged: ((String) -> Unit)? = null,
) {
    if (!isApp) {
        Box(modifier = modifier.fillMaxWidth()) {
            BorderedBasicTextField(
                modifier = Modifier.padding(top = if (websiteState.isValid) DefaultErrorMessageHeight else 0.dp),
                textStyle = contentTextStyle,
                hint = stringResource(id = R.string.hint_login_website),
                state = websiteState,
                singleLine = false,
                enabled = true,
                errorHandler = ErrorHandler(
                    errorMessage = stringResource(id = R.string.error_invalid_url),
                    errorColor = DefaultWarnColor
                ),
                textModifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 12.dp, vertical = 4.dp),
                keyboardType = KeyboardType.Uri,
                onNext = {
                    focusManager.moveFocus(FocusDirection.Down)
                },
                onFocusLost = onFocusLost,
                onValueChanged = onValueChanged
            )
            Text(
                text = stringResource(id = R.string.label_login_website),
                modifier = Modifier,
                style = titleTextStyle
            )
        }
    } else {
        TitledContent(
            modifier = Modifier.fillMaxWidth(),
            title = stringResource(id = R.string.label_login_application_id),
            titleTextStyle = titleTextStyle.copy(color = DarkBlue.copy(alpha = 0.75f))
        ) {
            Box(
                modifier = Modifier
                    .heightIn(min = DefaultButtonHeight)
                    .fillMaxWidth()
                    .alpha(0.4f)
                    .background(color = DefaultBackground, shape = RoundedCornerShape(8.dp))
                    .border(
                        width = DefaultSmallBorder,
                        color = DefaultBorderColor,
                        shape = RoundedCornerShape(8.dp)
                    ),
                contentAlignment = Alignment.CenterStart
            ) {
                Text(
                    modifier = Modifier
                        .padding(horizontal = 12.dp, vertical = 4.dp),
                    text = getAppName(LocalContext.current, androidAppId),
                    style = titleTextStyle.copy(color = DarkBlue.copy(alpha = 0.75f))
                )
            }
        }
    }
}

private fun getAppName(context: Context, androidAppId: String?): String =
    PackageVerifier.getAppName(context, androidAppId)
        ?: androidAppId
        ?: context.getString(R.string.label_login_application_id)
