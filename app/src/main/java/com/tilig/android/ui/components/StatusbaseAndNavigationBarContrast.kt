package com.tilig.android.ui.components

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.compositeOver
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.tilig.android.ui.theme.DefaultBackground

/**
 * Makes it easier to adjust the statusbar and navigation bar to your needs.
 * Makes the statusbar transparent, sets the brightness of the statusbar icons
 * to contrast with your app, and sets the navigation bar background color
 * *or* if the navigation bar should be transparent.
 *
 * @param statusBarBright Indicate whether or not the app's header is bright. Determines the color of the icons
 * @param navigationBarBright Indicate if the navigation bar should be bright or dark. Ignored if `navigationScrollBehind` is `true`.
 * @param navigationBarScrollBehind If true, the navigation bar will be 50% transparent white, with contrasting icons. When using this, you will probably need to set #navigationBarWithImePadding() on your scrolling view.
 */
@Composable
fun StatusBarAndNavigationBarContrast(
    statusBarBright: Boolean,
    navigationBarBright: Boolean,
    navigationBarScrollBehind: Boolean = false
) {
    val systemUiController = rememberSystemUiController()
    systemUiController.setSystemBarsColor(
        color = Color.Transparent,
        darkIcons = statusBarBright
    )
    if (navigationBarScrollBehind) {
        systemUiController.setNavigationBarColor(
            color = WhiteTransparent,
            darkIcons = true,
            navigationBarContrastEnforced = false,
            transformColorForLightContent = BlueScrimmed
        )
    } else {
        systemUiController.setNavigationBarColor(
            color = DefaultBackground,
            darkIcons = navigationBarBright,
            navigationBarContrastEnforced = false
        )
    }
}

private val WhiteTransparent = Color(1f, 1f, 1f, 0.5f) // 50% opaque white
private val BlueScrim = Color(0f, 0f, 1f, 0.3f) // 30% opaque blue
private val BlueScrimmed: (Color) -> Color = { original ->
    BlueScrim.compositeOver(original)
}
