package com.tilig.android.ui.secrets.add

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goterl.lazysodium.utils.Key
import com.goterl.lazysodium.utils.KeyPair
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.ApiSuccessResponse
import com.tilig.android.data.models.tilig.*
import com.tilig.android.data.repository.Repository
import com.tilig.android.ui.CryptoModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

data class AddSecretItemViewModelState(
    val isLoading: Boolean = false,
    val item: SecretItem? = null,
    val itemError: Throwable? = null,
    val folders: List<Folder>? = null
)

open class BaseAddItemViewModel : ViewModel(), KoinComponent {

    private val repo by inject<Repository>()
    private val cryptoModel: CryptoModel by inject()
    private val tracker: Mixpanel by inject()

    private val viewModelState = MutableStateFlow(AddSecretItemViewModelState())

    val uiState: StateFlow<AddSecretItemViewModelState> = viewModelState

    private val _itemCreatedEvent = MutableStateFlow(false)
    val itemCreatedEvent: StateFlow<Boolean> = _itemCreatedEvent

    init {
        viewModelScope.launch(Dispatchers.IO) {
            val result = repo.getAllFolders()
            viewModelState.update {
                it.copy(
                    folders = if (result is ApiSuccessResponse) {
                        result.body.folders
                    } else null
                )
            }
        }
    }

    fun clearUIState() {
        viewModelState.value = AddSecretItemViewModelState(folders = viewModelState.value.folders)
    }

    fun clearShouldFetchItems() {
        _itemCreatedEvent.value = false
    }

    fun createItem(item: SecretItem) {
        viewModelState.update {
            it.copy(isLoading = true)
        }

        cryptoModel.withInitializedCrypto { crypto ->

            if (item.folder != null) {

                item.decryptedDek = item.decryptedDek ?: crypto.generateDek()

                // User has a private key → used to decrypt the private KEK
                val usersKeypair = crypto.getKeyPairs().keypair!!
                val privateKek = crypto.open(
                    cipherText = item.folder!!.myEncryptedPrivateKey,
                    keyPair = usersKeypair
                )
                val publicKek = crypto.base64decode(item.folder!!.publicKey)

                // Private key of the KEK is used to decrypt the DEK
                val keyPair = KeyPair(
                    Key.fromBytes(publicKek),
                    Key.fromBytes(privateKek)
                )

                // Re-encrypt DEK using the new folder KEK for the current user
                item.encryptedFolderKey = crypto.encryptWithKeypair(
                    messageBytes = item.decryptedDek!!,
                    keyPair = keyPair
                )
            }

            val encryptedItem = item.encrypt(repo.jsonConverter, crypto)
            repo.createSecretItemCall(encryptedItem)
                .enqueue(object : Callback<EncryptedItemResponse> {
                    override fun onResponse(
                        call: Call<EncryptedItemResponse>,
                        response: Response<EncryptedItemResponse>
                    ) {
                        when (item) {
                            is Note -> tracker.trackEvent(Tracker.EVENT_NOTE_CREATED)
                            is CreditCard -> tracker.trackEvent(Tracker.EVENT_CREDIT_CARD_CREATED)
                            is WifiPassword -> tracker.trackEvent(Tracker.EVENT_WIFI_CREATED)
                            else -> {}
                        }
                        onItemSuccessfullyCreated(response.body()?.item)
                    }

                    override fun onFailure(
                        call: Call<EncryptedItemResponse>,
                        throwable: Throwable
                    ) {
                        onItemCreatedFailure(throwable)
                    }
                })
        }
    }

    protected open fun onItemSuccessfullyCreated(item: EncryptedItem?) {
        val decryptedItem = item?.decrypt(
            repo.jsonConverter,
            cryptoModel.getInitializedCrypto()
        )
        viewModelState.update {
            it.copy(
                item = decryptedItem,
                itemError = null
            )
        }
        _itemCreatedEvent.value = true
    }

    protected fun onItemCreatedFailure(throwable: Throwable) {
        viewModelState.update {
            it.copy(
                isLoading = false,
                item = null,
                itemError = throwable
            )
        }
        _itemCreatedEvent.value = false
    }
}