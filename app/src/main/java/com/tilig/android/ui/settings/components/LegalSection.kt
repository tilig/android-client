package com.tilig.android.ui.settings.components

import androidx.annotation.StringRes
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForwardIos
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.components.FieldTitle
import com.tilig.android.ui.settings.SettingsUrl
import com.tilig.android.ui.theme.*

@Composable
fun LegalSection(openLink: (SettingsUrl) -> Unit) {
    FieldTitle(
        title = stringResource(id = R.string.label_legal),
        titlePadding = 8.dp,
        style = StyleSpoofMediumDarkBlue20
    )
    Column(
        modifier = Modifier
            .background(color = White, shape = RoundedCornerShape(MediumCornerRadius))
            .fillMaxWidth(),
    ) {
        LegalRow(titleId = R.string.privacy_policy, onClick = {
            openLink(SettingsUrl.PRIVACY)
        })

        Divider(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
        )

        LegalRow(
            titleId = R.string.terms_and_conditions,
            onClick = { openLink(SettingsUrl.TERMS_AND_CONDITIONS) })
    }
}

@Composable
private fun LegalRow(
    @StringRes titleId: Int,
    onClick: () -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onClick.invoke() }
            .padding(16.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            modifier = Modifier
                .padding(end = 16.dp),
            text = stringResource(id = titleId),
            style = StyleStolzRegularDarkBlue14,
            color = DefaultTextColor,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )
        Icon(
            modifier = Modifier.size(16.dp),
            imageVector = Icons.Filled.ArrowForwardIos,
            contentDescription = "",
            tint = SettingsArrowColor
        )
    }
}