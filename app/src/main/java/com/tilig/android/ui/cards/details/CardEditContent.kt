package com.tilig.android.ui.cards.details

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.data.models.tilig.CustomField
import com.tilig.android.ui.cards.details.holders.EditCardStateHolder
import com.tilig.android.ui.components.*
import com.tilig.android.ui.secrets.add.AddToFolderComponent
import com.tilig.android.ui.secrets.components.*
import com.tilig.android.ui.secrets.details.ItemDetailsCallback
import com.tilig.android.ui.secrets.details.events.ItemDetailsActionEvent
import com.tilig.android.ui.secrets.details.events.BaseEditEvent
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.CardUtils

@Composable
fun CardEditContent(
    displayUnsavedAlert: Boolean,
    state: EditCardStateHolder,
    callback: ItemDetailsCallback
) {

    val openDialog = remember { mutableStateOf(false) }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = DefaultPadding)
    ) {

        CardFieldsContent(state = state, callback = callback, supportCustomFields = true)

        Spacer(modifier = Modifier.height(56.dp))
        DeleteSecretItemView(
            titleId = R.string.bt_delete_credit_card,
            dialogDescriptionId = R.string.dialog_delete_item,
            openDialog = openDialog
        ) {
            callback.onActionEvent(ItemDetailsActionEvent.DeleteItem)
        }
    }

    if (displayUnsavedAlert) {
        TiligDialog(
            title = R.string.alert_unsaved_changes,
            text = R.string.alert_unsaved_changes_desc,
            onCancel = {
                callback.onActionEvent(ItemDetailsActionEvent.CancelChangesDialog)
            },
            onDismissRequest = {},
            confirmButtonText = R.string.alert_unsaved_changes_discard,
            cancelButtonText = R.string.alert_unsaved_changes_confirm,
            onConfirm = { callback.onActionEvent(ItemDetailsActionEvent.ConfirmDiscardChanges) })
    }

    BackHandler {
        callback.onActionEvent(ItemDetailsActionEvent.OnGoBack)
    }
}

@Composable
private fun ColumnScope.CardFieldsContent(
    state: EditCardStateHolder,
    showExtraInfoField: Boolean = true,
    showCardIcon: Boolean = false,
    supportCustomFields: Boolean = false,
    callback: ItemDetailsCallback,
) {
    val focusManager = LocalFocusManager.current
    val focusRequester = remember { FocusRequester() }

    if (showCardIcon) {
        AddSecretItemNameAndLogoView(
            icon = painterResource(id = R.drawable.ic_credit_card),
            hintInputRes = R.string.hint_card_card_name,
            focusManager = focusManager,
            focusRequester = focusRequester,
            nameState = state.nameState
        )
    } else {
        TiligSpacerVerticalMedium()
        Box {
            BorderedBasicTextField(
                modifier = Modifier.padding(top = if (state.nameState.isValid) DefaultErrorMessageHeight else 0.dp),
                errorHandler = ErrorHandler(
                    errorMessage = stringResource(id = R.string.error_name),
                    errorColor = DefaultErrorColor
                ),
                singleLine = false,
                hint = stringResource(id = R.string.hint_card_card_name),
                state = state.nameState,
                textModifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 12.dp, vertical = 4.dp),
                keyboardType = KeyboardType.Text,
                capitalization = KeyboardCapitalization.Sentences,
                onNext = {
                    focusManager.moveFocus(FocusDirection.Down)
                },
                onValueChanged = {
                    callback.onActionEvent(ItemDetailsActionEvent.Edit(CardEditEvents.NameChanged(state.nameState.text)))
                }
            )
            FieldTitle(
                title = stringResource(id = R.string.label_card_name),
                modifier = Modifier
            )
        }
    }

    TiligSpacerVerticalMedium()
    Box {
        BorderedBasicTextField(
            modifier = Modifier.padding(top = if (state.cardNumberState.isValid) DefaultErrorMessageHeight else 0.dp),
            hint = stringResource(id = R.string.hint_card_number),
            state = state.cardNumberState,
            showContentInitially = true,
            keyboardType = KeyboardType.Number,
            onNext = {
                focusManager.moveFocus(FocusDirection.Down)
            },
            onValueChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(CardEditEvents.CardNumberChanged(state.cardNumberState.text)))
            },
//            onFocusLost = {
//                state.validateCardNumber()
//            },
            hideVisualTransformation = CardUtils.CardNumberTransformation(CardUtils.BULLET),
            defaultVisualTransformation = CardUtils.CardNumberTransformation(null),
            showRevealButton = state.cardNumberState.isFocused,
            maxLength = state.getCardNumberFieldMaxLength(),
            errorHandler = ErrorHandler(
                errorMessage = stringResource(id = R.string.error_card_number_invalid),
                errorColor = DefaultWarnColor
            )
        )
        FieldTitle(
            title = stringResource(id = R.string.label_card_number),
            modifier = Modifier
        )
    }

    TiligSpacerVerticalMedium()
    Box {
        BorderedBasicTextField(
            modifier = Modifier.padding(top = if (state.cardHolderState.isValid) DefaultErrorMessageHeight else 0.dp),
            hint = stringResource(id = R.string.hint_card_cardholder_name),
            state = state.cardHolderState,
            onNext = {
                focusManager.moveFocus(FocusDirection.Next)
            },
            capitalization = KeyboardCapitalization.Words,
            onValueChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(CardEditEvents.HolderNameChanged(state.cardHolderState.text)))
            }
        )
        FieldTitle(
            title = stringResource(id = R.string.label_card_holder_name),
            modifier = Modifier
        )
    }

    TiligSpacerVerticalMedium()
    Box(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        BorderedBasicTextField(
            modifier = Modifier.padding(top = if (state.cardExpireState.isValid) DefaultErrorMessageHeight else 0.dp),
            hint = stringResource(id = R.string.hint_card_expire_date),
            state = state.cardExpireState,
            keyboardType = KeyboardType.Number,
            defaultVisualTransformation = { date ->
                CardUtils.formatExpireDate(date)
            },
            showContentInitially = true,
            onNext = {
                focusManager.moveFocus(FocusDirection.Next)
            },
            errorHandler = ErrorHandler(
                errorMessage = stringResource(id = R.string.error_expire_date),
                errorColor = DefaultWarnColor
            ),
            maxLength = 4,
            onValueChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(CardEditEvents.ExpireDateChanged(state.cardExpireState.text)))
            }
        )
        FieldTitle(
            title = stringResource(id = R.string.label_expire_date),
            modifier = Modifier
        )
    }

    TiligSpacerVerticalMedium()
    Box(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        BorderedBasicTextField(
            modifier = Modifier.padding(top = if (state.cardSecurityCodeState.isValid) DefaultErrorMessageHeight else 0.dp),
            hint = stringResource(id = R.string.hint_card_secret_code),
            state = state.cardSecurityCodeState,
            hideVisualTransformation = PasswordVisualTransformation(),
            showRevealButton = state.cardSecurityCodeState.isFocused,
            onNext = {
                focusManager.moveFocus(FocusDirection.Next)
            },
            keyboardType = KeyboardType.Number,
            maxLength = 4,
            onValueChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(CardEditEvents.SecurityCodeChanged(state.cardSecurityCodeState.text)))
            }
        )
        FieldTitle(
            title = stringResource(id = R.string.label_secret_code),
            modifier = Modifier
        )
    }

    TiligSpacerVerticalMedium()
    Box(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        BorderedBasicTextField(
            modifier = Modifier.padding(top = DefaultErrorMessageHeight),
            hint = stringResource(id = R.string.hint_card_pin_code),
            state = state.cardPinState,
            hideVisualTransformation = PasswordVisualTransformation(),
            showRevealButton = state.cardPinState.isFocused,
            keyboardType = KeyboardType.Number,
            maxLength = 4,
            onNext = {
                focusManager.moveFocus(FocusDirection.Next)
            },
            onValueChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(CardEditEvents.PinCodeChanged(state.cardPinState.text)))
            }
        )
        FieldTitle(
            title = stringResource(id = R.string.label_pin_code),
            modifier = Modifier
        )
    }

    TiligSpacerVerticalMedium()
    Box(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        BorderedBasicTextField(
            modifier = Modifier.padding(top = DefaultErrorMessageHeight),
            hint = stringResource(id = R.string.hint_card_zip_code),
            state = state.cardZipCodeState,
            onNext = {
                focusManager.moveFocus(FocusDirection.Next)
            },
            onValueChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(CardEditEvents.ZipCodeChanged(state.cardZipCodeState.text)))
            }
        )
        FieldTitle(
            title = stringResource(id = R.string.label_zip_code),
            modifier = Modifier
        )
    }

    if (showExtraInfoField) {
        TiligSpacerVerticalMedium()
        TitledContent(
            modifier = Modifier,
            title = stringResource(id = R.string.label_extra_info),
            titleTextStyle = Typography.h3,
        ) {
            BorderedBasicTextField(
                modifier = Modifier,
                textStyle = StyleStolzRegularDarkBlue16,
                hint = stringResource(id = R.string.hint_card_extra_info),
                state = state.cardExtraInfoState,
                singleLine = false,
                textModifier = Modifier
                    .fillMaxWidth()
                    .padding(12.dp),
                keyboardType = KeyboardType.Text,
                capitalization = KeyboardCapitalization.Sentences,
                onValueChanged = {
                    callback.onActionEvent(
                        ItemDetailsActionEvent.Edit(
                            CardEditEvents.ExtraInfoChanged(
                                state.cardExtraInfoState.text
                            )
                        )
                    )
                }
            )
        }
    }

    if (supportCustomFields) {
        AllCustomFieldsEditComponent(
            focusManager = focusManager,
            fieldsWrapper = state.customFields,
            onFieldsChanged = {
                callback.onActionEvent(ItemDetailsActionEvent.Edit(BaseEditEvent.CustomFieldsChanged(state.customFields.map {
                    CustomField(
                        it.labelFieldState.text,
                        it.customField.kind,
                        it.textFieldState.text
                    )
                })))
            }) {
            val updated = state.customFields.toMutableList()
            updated.remove(it)
            callback.onActionEvent(
                ItemDetailsActionEvent.Edit(
                    BaseEditEvent.CustomFieldsChanged(
                        updated.map {
                            CustomField(
                                it.labelFieldState.text,
                                it.customField.kind,
                                it.textFieldState.text
                            )
                        })
                )
            )
        }

        TiligSpacerVerticalDefault()
        AddCustomField(modifier = Modifier.fillMaxWidth()) {
            callback.onActionEvent(ItemDetailsActionEvent.Edit(BaseEditEvent.AddCustomField(it)))
        }
    }

    TiligSpacerVerticalDefault()
    AddToFolderComponent(
        modifier = Modifier,
        folders = state.foldersList,
        selectedFolder = state.selectedFolder,
        onFolderChanged = {
            callback.onActionEvent(ItemDetailsActionEvent.Edit(BaseEditEvent.ChangeFolder(it)))
        }
    )

    BackHandler {
        callback.onActionEvent(ItemDetailsActionEvent.OnGoBack)
    }
}