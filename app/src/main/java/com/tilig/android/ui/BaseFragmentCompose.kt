package com.tilig.android.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.annotation.StringRes
import androidx.compose.ui.graphics.toArgb
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.tilig.android.BaseActivity
import com.tilig.android.autofill.Autofill
import com.tilig.android.ui.theme.DefaultBackground
import com.tilig.android.ui.theme.StatusBarColor
import com.tilig.android.utils.SharedPrefs
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

open class BaseFragmentCompose : Fragment(), KoinComponent {

    protected val prefs: SharedPrefs by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("BaseFrag", "${this::class.java.name}.onCreate")
    }

    /**
     * This toggles the statusbar background color between
     * two states: dark and bright, and switches the icon colors
     * to be the contrasting color
     */
    fun setStatusbarColorBright(bright: Boolean) {
        activity?.window?.apply {
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            // Update color
            statusBarColor = if (bright) {
                DefaultBackground.toArgb()
            } else {
                StatusBarColor.toArgb()
            }

            // And toggle bright/dark icons
            decorView.let {
                it.systemUiVisibility = if (bright)
                    it.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                else
                    it.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
            }
        }
    }

    fun snackbar(
        container: View,
        @StringRes msg: Int,
        duration: Int = Snackbar.LENGTH_LONG
    ) = snackbar(
        container,
        getString(msg),
        duration
    )

    fun snackbar(
        container: View,
        msg: String,
        duration: Int = Snackbar.LENGTH_LONG
    ) = Snackbar.make(
        container,
        msg,
        duration
    ).show()

    protected val autofill: Autofill?
        get() = (requireActivity() as? BaseActivity)?.autofill

    protected fun isInSearchMode(): Boolean =
        (requireActivity() as? LaunchModeView)?.isInSearchMode() == true

    protected fun getSearchQuery(): String =
        if (isInSearchMode())
            (requireActivity() as? LaunchModeView)?.getSearchQuery() ?: ""
        else
            ""

    protected fun isInCreateNewMode(): Boolean =
        (requireActivity() as? LaunchModeView)?.isInCreateNewMode() == true

    protected fun getBaseActivity(): BaseActivity? = activity as? BaseActivity

}
