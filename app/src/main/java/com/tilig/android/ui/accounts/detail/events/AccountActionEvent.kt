package com.tilig.android.ui.accounts.detail.events

import com.tilig.android.ui.secrets.details.events.ItemDetailsActionEvent

sealed interface AccountActionEvent: ItemDetailsActionEvent {
    data class RestorePassword(val secret: String) : AccountActionEvent
    object CreateShareLink : AccountActionEvent
    object DeleteShareLink : AccountActionEvent
    data class GetBrandInfoByWebsite(val website: String) : AccountActionEvent
}