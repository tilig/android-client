package com.tilig.android.ui.onboarding.getstarted

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.autofill.AutofillNode
import androidx.compose.ui.autofill.AutofillType
import androidx.compose.ui.composed
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.layout.boundsInWindow
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.*
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.coerceAtLeast
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.tilig.android.R
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.components.*
import com.tilig.android.ui.secrets.components.TitledContent
import com.tilig.android.ui.theme.*
import kotlinx.coroutines.flow.MutableStateFlow

@OptIn(ExperimentalComposeUiApi::class)
fun Modifier.autofill(
    hasFocus: MutableState<Boolean>,
    autofillTypes: List<AutofillType>,
    onFill: ((String) -> Unit),
    onFillPass: ((String) -> Unit)
) = composed {
    val autofill = LocalAutofill.current
    val autofillNode = AutofillNode(onFill = onFill, autofillTypes = autofillTypes)
    val autofillNode1 =
        AutofillNode(onFill = onFillPass, autofillTypes = listOf(AutofillType.Password))
    LocalAutofillTree.current += autofillNode
    LocalAutofillTree.current += autofillNode1

    this
        .onGloballyPositioned {
            autofillNode.boundingBox = it.boundsInWindow()
            autofillNode1.boundingBox = it.boundsInWindow()
        }
        .onFocusChanged { focusState ->
            autofill?.run {
                if (focusState.isFocused && !hasFocus.value) {
                    hasFocus.value = true
                    requestAutofillForNode(autofillNode)
                    requestAutofillForNode(autofillNode1)
                } else {
                    hasFocus.value = false
                    cancelAutofillForNode(autofillNode)
                    cancelAutofillForNode(autofillNode1)
                }
            }
        }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun SimulateAutofillBottomSheet(
    account: Account,
    onFinish: () -> Unit,
    onClose: () -> Unit
) {
    val keyboardController = LocalSoftwareKeyboardController.current
    val focusManager = LocalFocusManager.current

    val emailState = remember { TextFieldState("") }
    val passwordState = remember { TextFieldState("") }
    val hasFocusEmail = remember { mutableStateOf(false) }

    val onFill: ((String) -> Unit) = {
        emailState.text = it
        keyboardController?.hide()
    }

    val onFillPass: ((String) -> Unit) = {
        passwordState.text = it
        keyboardController?.hide()
    }

    val autofill = LocalAutofill.current
    val autofillNodeEmail =
        AutofillNode(onFill = onFill, autofillTypes = listOf(AutofillType.EmailAddress))
    val autofillNodePassword =
        AutofillNode(onFill = onFillPass, autofillTypes = listOf(AutofillType.Password))
    LocalAutofillTree.current += autofillNodeEmail
    LocalAutofillTree.current += autofillNodePassword

    val scrollableState = rememberScrollState()
    Box(
        modifier = Modifier
            .navigationBarsPadding()
            .height(LocalConfiguration.current.screenHeightDp.dp - 64.dp)
            .imePadding()
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(scrollableState)
        ) {

            ScreenHeader(
                headerTextRes = R.string.get_started_step_try_autofill,
                textColor = DarkBlue,
                closeSheet = onClose
            )

            ConstraintLayout(
                modifier = Modifier
                    .padding(
                        top = MediumPadding,
                        start = DefaultPadding,
                        end = DefaultPadding,
                        bottom = DefaultPadding
                    )
                    .fillMaxWidth()
            ) {
                val (bg, content) = createRefs()

                Box(modifier = Modifier
                    .constrainAs(bg) {
                        top.linkTo(parent.top)
                        end.linkTo(parent.end)
                        bottom.linkTo(content.bottom)
                        start.linkTo(parent.start)
                        width = Dimension.fillToConstraints
                        height = Dimension.fillToConstraints
                    }
                    .padding(top = 32.dp)
                    .background(color = BackgroundGrey, shape = Shapes.medium))

                Column(modifier = Modifier
                    .padding(horizontal = DefaultPadding)
                    .fillMaxWidth()
                    .constrainAs(content) {
                        top.linkTo(parent.top)
                        end.linkTo(parent.end)
                        start.linkTo(parent.start)
                        width = Dimension.fillToConstraints
                        height = Dimension.wrapContent
                    }) {
                    Box(
                        modifier = Modifier
                            .background(White, shape = CircleShape)
                            .size(64.dp)
                            .align(Alignment.CenterHorizontally)
                            .padding(HalfPadding)
                    ) {
                        AccountIcon(
                            accountIcon = account.brand?.iconSource,
                            shouldUpdateIcon = MutableStateFlow(false),
                            size = 40.dp,
                            clipShape = CircleShape,
                            modifier = Modifier
                                .background(White, shape = CircleShape)
                                .align(Alignment.Center)
                        )
                    }
                    Text(
                        modifier = Modifier
                            .padding(top = HalfPadding)
                            .align(Alignment.CenterHorizontally),
                        text = account.getNonBlankDisplayNameOrNull()
                            ?: stringResource(id = R.string.untitled_account),
                        style = StyleSpoofMediumBlue24.copy(fontSize = 18.sp)
                    )

                    Spacer(modifier = Modifier.height(32.dp))

                    TitledContent(
                        modifier = Modifier,
                        title = stringResource(id = R.string.label_login_username_email)
                    ) {
                        BorderedBasicTextField(modifier = Modifier
                            .onGloballyPositioned {
                                autofillNodeEmail.boundingBox = it.boundsInWindow()
                                autofillNodePassword.boundingBox = it.boundsInWindow()
                            }
                            .onFocusChanged {
                                hasFocusEmail.value = it.hasFocus
                                if (it.hasFocus) {
                                    hasFocusEmail.value = true
                                    autofill?.requestAutofillForNode(autofillNodeEmail)
                                } else {
                                    autofill?.cancelAutofillForNode(autofillNodePassword)
                                }
                            },
                            hint = stringResource(id = R.string.hint_login_username_email),
                            state = emailState,
                            keyboardType = KeyboardType.Text,
                            onNext = {
                                focusManager.moveFocus(FocusDirection.Down)
                            },
                            errorHandler = ErrorHandler(
                                errorColor = DefaultErrorColor
                            ),
                            onValueChanged = {})
                    }

                    TitledContent(
                        modifier = Modifier.padding(top = 16.dp),
                        title = stringResource(id = R.string.label_login_password)
                    ) {
                        BorderedBasicTextField(
                            modifier = Modifier
                                .onGloballyPositioned {
                                    autofillNodeEmail.boundingBox = it.boundsInWindow()
                                    autofillNodePassword.boundingBox = it.boundsInWindow()
                                }
                                .onFocusChanged {
                                    hasFocusEmail.value = it.hasFocus
                                    if (it.hasFocus) {
                                        hasFocusEmail.value = true
                                        autofill?.requestAutofillForNode(autofillNodePassword)
                                    } else {
                                        autofill?.cancelAutofillForNode(autofillNodeEmail)
                                    }
                                },
                            hint = stringResource(id = R.string.hint_wifi_password),
                            state = passwordState,
                            hideVisualTransformation = PasswordVisualTransformation(),
                            onDone = onFinish,
                            keyboardType = KeyboardType.Text,
                        )
                    }

                    Spacer(modifier = Modifier.height(18.dp))

                }

            }

            Text(
                modifier = Modifier.align(Alignment.CenterHorizontally), text = stringResource(
                    id = R.string.simulate_your_login_to, account.name ?: stringResource(
                        id = R.string.untitled_account
                    )
                ),
                style = StyleStolzRegularDarkBlue12.copy(color = Neutral40)
            )

            Spacer(modifier = Modifier.height(32.dp))

            TiligButton(
                modifier = Modifier
                    .fillMaxWidth(),
                iconRes = null,
                text = stringResource(R.string.got_it),
                enabled = true,
                isDark = true,
                isSizeConstrained = true,
                onClick = onFinish
            )
        }
    }
}

@Composable
@Preview(showBackground = true)
fun SimulateAutofillBottomSheetPreview() {
    SimulateAutofillBottomSheet(account = Account.createEmpty(), {}, {})
}
