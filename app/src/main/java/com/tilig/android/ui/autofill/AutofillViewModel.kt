package com.tilig.android.ui.autofill

import Relation
import android.os.Build
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.viewModelScope
import com.tilig.android.autofill.AccountMatcher
import com.tilig.android.autofill.google.StructureParser
import com.tilig.android.data.livedata.ConnectivityLiveData
import com.tilig.android.data.models.*
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.models.tilig.SecretItem
import com.tilig.android.data.repository.Repository
import com.tilig.android.ui.CryptoModel
import com.tilig.android.ui.secrets.BaseSecretsListViewModel
import com.tilig.android.ui.secrets.SecretItemsUiState
import com.tilig.android.ui.secrets.SecretItemsViewModelState
import com.tilig.android.utils.DateUtils
import com.tilig.android.utils.VersionUtil
import com.tilig.android.utils.getDomain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.koin.core.component.inject
import java.util.*

sealed interface AutofillUiState {

    val isLoading: Boolean

    data class AutofillAccountsError(
        override val isLoading: Boolean,
        val errorMessages: List<UiError>
    ) : AutofillUiState

    data class AutofillAccountsList(
        override val isLoading: Boolean,
        val matchedAccounts: List<Account>,
        val allAccounts: List<Account>,
        val totalCount: Int,
        val isDemoMode: Boolean = false
    ) : AutofillUiState

    data class AddNewAccount(
        override val isLoading: Boolean,
        val prefilledAccount: Account
    ) : AutofillUiState
}

data class AutofillViewModelState(
    val accounts: ApiResponse<List<SecretItem>>? = null,
    val newAccount: Account? = null,
    // Filter secrets query
    val searchFilter: String? = null,
    // Autofill context (when autofilling)
    val parser: StructureParser? = null,
    val isLoading: Boolean = false,
) {

    /**
     *
     * Converts this [SecretItemsViewModelState] into a more strongly typed [SecretItemsUiState] for driving
     * the ui.
     */
    fun toUiState(): AutofillUiState {
        if (newAccount != null) {
            return AutofillUiState.AddNewAccount(
                prefilledAccount = newAccount,
                isLoading = false
            )
        }
        return when (accounts) {
            null,
            is ApiEmptyResponse -> AutofillUiState.AutofillAccountsList(
                isLoading = accounts?.let { isLoading } ?: true,
                matchedAccounts = listOf(),
                allAccounts = listOf(),
                totalCount = 0
            )
            is ApiErrorResponse -> AutofillUiState.AutofillAccountsError(
                isLoading = isLoading,
                errorMessages = listOf(
                    accounts.toUiError()
                ),
            )
            is ApiSuccessResponse -> {
                if (accounts.body.isEmpty()) {
                    AutofillUiState.AutofillAccountsList(
                        isLoading = isLoading,
                        matchedAccounts = listOf(),
                        allAccounts = listOf(),
                        totalCount = 0
                    )
                } else {
                    val sortedItems = accounts.body.filterIsInstance(Account::class.java)
                        .sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER) { it.overview.name ?: "" })
                    val searchTextLowerCase = searchFilter
                        ?.lowercase(Locale.getDefault())


                    val targetAppId = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        parser?.getStructPackageName()
                    } else {
                        null
                    }
                    if (targetAppId == "com.tilig.android" || targetAppId == "com.tilig.android.debug") {
                        return AutofillUiState.AutofillAccountsList(
                            isLoading = isLoading,
                            matchedAccounts = listOf(sortedItems.maxBy {
                                DateUtils.convertDateToTimeStamp(
                                    it.createdAt
                                )
                            }),
                            allAccounts = listOf(),
                            totalCount = sortedItems.size,
                            isDemoMode = true
                        )
                    }

                    val filteredAccounts = mutableListOf<Account>()

                    if (searchTextLowerCase == null) {
                        filteredAccounts.addAll(sortedItems)
                    } else {
                        // go over each item, add it to grouped by type maps and if it satisfies search
                        // add to grouped by type search map results (needed for correctly setup of
                        // empty screens and tabs items count indicators)
                        sortedItems.forEach { item ->
                            if (item.name
                                    ?.lowercase(Locale.getDefault())
                                    ?.contains(searchTextLowerCase) != false
                                || item.website.getDomain()
                                    ?.lowercase(Locale.getDefault())
                                    ?.contains(searchTextLowerCase) == true
                            ) {
                                filteredAccounts.add(item)
                            }
                        }
                    }

                    val matchedAccounts = if (parser != null && VersionUtil.supportsAutofill()) {
                        AccountMatcher.filterAccounts(
                            sortedItems,
                            parser
                        )
                    } else {
                        emptyList()
                    }

                    // don't show in search logins list items that matches autofill
                    matchedAccounts.forEach {
                        filteredAccounts.remove(it)
                    }

                    AutofillUiState.AutofillAccountsList(
                        isLoading = isLoading,
                        matchedAccounts = matchedAccounts,
                        allAccounts = filteredAccounts,
                        totalCount = sortedItems.size
                    )
                }
            }
        }
    }
}

class AutofillViewModel : BaseSecretsListViewModel(), DefaultLifecycleObserver {

    private val repo by inject<Repository>()
    private val cryptoModel: CryptoModel by inject()

    val connectivity by inject<ConnectivityLiveData>()

    private val viewModelState = MutableStateFlow(AutofillViewModelState())

    // UI state exposed to the UI
    val uiState: StateFlow<AutofillUiState> = viewModelState
        .map { it.toUiState() }
        .stateIn(
            viewModelScope,
            SharingStarted.Eagerly,
            viewModelState.value.toUiState()
        )

    /**
     * Fetches items and updates the uiState upon return.
     * The list of items can be complete (both arguments null) or filtered
     * either by a search query or by an autofill account.
     * @param query when given, is used as a text search filter on the list of items
     * @param parser if `query` is null and this parameter is not, the [AccountMatcher] is used to filter the list of items.
     */
    fun secretItems(query: String? = null, parser: StructureParser? = null) {
        viewModelState.update {
            it.copy(isLoading = true)
        }
        viewModelScope.launch(Dispatchers.IO) {
            val encryptedAccounts = repo.secretItems()
            cryptoModel.withInitializedCrypto { crypto ->
                // why not to move this into repository ?
                val decryptedAccounts =
                    (encryptedAccounts as? ApiSuccessResponse)?.body?.items?.map {
                        it.decrypt(
                            repo.jsonConverter,
                            crypto
                        )
                    }
                val decryptedResponse = ApiResponse.createCloneWithNewBody(
                    encryptedAccounts,
                    decryptedAccounts
                )
                viewModelState.update {
                    it.copy(
                        accounts = decryptedResponse,
                        searchFilter = query,
                        parser = parser,
                        isLoading = false
                    )
                }
                runMigrations {
                    secretItems(query, parser)
                }
            }
        }
    }

    fun isAccountMatchesWithAutofill(
        account: Account,
        parser: StructureParser?,
        relation: Relation?
    ): Boolean {
        if (parser != null && VersionUtil.supportsAutofill()) {
            return AccountMatcher.accountMatchesContexts(
                account,
                parser,
                relation
            ) != AccountMatcher.NO_MATCH
        }
        return false
    }

    override fun onResume(owner: LifecycleOwner) {
        super.onResume(owner)
        secretItems()
    }

    fun searchForSecretItems(searchText: String) {
        viewModelScope.launch(Dispatchers.IO) {
            viewModelState.update {
                it.copy(
                    searchFilter = searchText,
                )
            }
        }
    }

    fun enterAddingMode(account: Account) {
        viewModelScope.launch(Dispatchers.IO) {
            viewModelState.update {
                it.copy(
                    newAccount = account
                )
            }
        }
    }

    fun enterAddingModeImmediately(account: Account) {
        viewModelState.update {
            it.copy(
                newAccount = account
            )
        }
    }

    fun enterDefaultMode() {
        viewModelScope.launch(Dispatchers.IO) {
            viewModelState.update {
                it.copy(
                    isLoading = false,
                    searchFilter = null,
                    newAccount = null
                )
            }
        }
    }
}