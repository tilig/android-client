package com.tilig.android.ui.secrets

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tilig.android.R
import com.tilig.android.ui.secrets.components.NotificationWithIconTitleDescView
import com.tilig.android.ui.theme.DimmedBlue
import com.tilig.android.ui.theme.Typography

@Composable
fun SearchResultsEmptyScreen(
    addExtraSpace: Boolean = true
) {
    Column(Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally) {
        if (addExtraSpace) {
            Spacer(modifier = Modifier.height(48.dp))
        }
        NotificationWithIconTitleDescView(
            painter = painterResource(id = R.drawable.ic_search_empty),
            title = stringResource(id = R.string.label_no_results),
            titleStyle = Typography.h3.copy(color = DimmedBlue, fontSize = 16.sp),
            description = null
        )
    }
}

@Preview
@Composable
fun SearchResultsEmptyScreenPreview() {
    SearchResultsEmptyScreen()
}
