package com.tilig.android.ui.components

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.annotation.DrawableRes
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import com.tilig.android.R
import com.tilig.android.autofill.PackageVerifier
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.testdrive.TestDrive
import com.tilig.android.ui.theme.DefaultAccountClipShape
import com.tilig.android.utils.IconUrlUtil
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

@SuppressLint("CoroutineCreationDuringComposition") // this is not ideal but the coroutine action is just emitting a single value (which forces the recomposition) so there's no chance in blocking the UI thread
@Composable
fun AccountIcon(
    accountIcon: Any?,
    modifier: Modifier = Modifier,
    clipShape: RoundedCornerShape = DefaultAccountClipShape,
    size: Dp = 24.dp,
    @DrawableRes placeholder: Int = remember { R.drawable.ic_acorn_placeholder },
    shouldUpdateIcon: MutableStateFlow<Boolean>
) {
    val coroutineScope = rememberCoroutineScope()
    if (shouldUpdateIcon.collectAsState().value) {
        // this is not ideal but the coroutine action is just emitting a single value (which forces the recomposition) so there's no chance in blocking the UI thread
        coroutineScope.launch { shouldUpdateIcon.emit(false) }
    }
    TiligImage(
        modifier = modifier.size(size).clip(clipShape),
        image = accountIcon,
        contentDescription = "Account icon",
        placeholder = placeholder
    )
    Spacer(Modifier.width(16.dp))
}

@SuppressLint("CoroutineCreationDuringComposition")
@Composable
fun AccountIcon(
    account: Account?,
    modifier: Modifier = Modifier,
    clipShape: RoundedCornerShape = DefaultAccountClipShape,
    size: Dp = 24.dp,
    @DrawableRes placeholder: Int = remember { R.drawable.ic_acorn_placeholder },
    shouldUpdateIcon: MutableStateFlow<Boolean>
) {
    AccountIcon(
        modifier = modifier,
        clipShape = clipShape,
        accountIcon = account?.getIcon(LocalContext.current),
        shouldUpdateIcon = shouldUpdateIcon,
        size = size,
        placeholder = placeholder
    )
}

@Composable
@Preview
fun AccountIconPreview() {
    AccountIcon(
        account = Account.createEmpty().apply {
            this.website = "https://tilig.com"
            this.name = "Lorem ipsum"
        },
        shouldUpdateIcon = remember { MutableStateFlow(false) }
    )
}

fun Account.getIcon(context: Context): Any? {
    val logoUrl = IconUrlUtil.getIconUrl(this)
    return when {
        TestDrive.isTestDriveItem(id) -> {
            ContextCompat.getDrawable(context, R.drawable.ic_somewebsite)
        }
        logoUrl != null -> {
            Log.v("ICON", "Loading icon from url: $logoUrl")
            logoUrl
        }
        overview.androidAppIds.isNotEmpty() ->
            PackageVerifier.getIcon(context, overview.androidAppIds.first())
        else -> null
    }
}
