package com.tilig.android.ui.settings.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.tilig.android.R
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.components.FieldTitle
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.SharedPrefs
import com.tilig.android.utils.lock.LockViewModel
import org.koin.androidx.compose.inject

@Preview
@Composable
fun PreferencesSection(
    lockViewModel: LockViewModel = viewModel()
) {

    val prefs: SharedPrefs by inject()
    val tracker: Mixpanel by inject()
    val context = LocalContext.current

    val stateLock = remember {
        mutableStateOf(prefs.isLockEnabled)
    }
    val dialogState = remember { mutableStateOf(LockUtilDialogState.NONE) }

    FieldTitle(title = stringResource(id = R.string.label_preferences),
        titlePadding = 8.dp,
        style = StyleSpoofMediumDarkBlue20
    )
        Row(
            modifier = Modifier
                .background(color = White,  shape = RoundedCornerShape(MediumCornerRadius))
                .fillMaxWidth()
                .padding(16.dp),
        ) {
            SettingsToggle(
                stringResource(id = R.string.biometrics_enabled),
                checkedState = stateLock,
                checked = prefs.isLockEnabled,
                onInfoTapped = {
                    dialogState.value = LockUtilDialogState.INFORMATION
                    tracker.trackEvent(Tracker.EVENT_BIOMETRICS_INFO_TAPPED)
                },
                onCheckedChanged = {
                    // Don't allow users to enable this if they have no fingerprints setup
                    if (it && !lockViewModel.lockUtil.canAuthorize(context)) {
                        prefs.isLockEnabled = false
                        stateLock.value = false

                        // Can we instruct users to enable it?
                        enableBiometricsWarning(context, lockViewModel.lockUtil, tracker, dialogState)
                        false
                    } else {
                        if (it) {
                            dialogState.value = LockUtilDialogState.INSTRUCTIONS
                            false
                        } else {
                            prefs.isLockEnabled = false
                            tracker.trackEvent(Tracker.EVENT_BIOMETRICS_DISABLED)
                            true
                        }
                    }
                }
            )
    }

    LockDialog(
        dialogState,
        onEnabled = {
            stateLock.value = true
            prefs.isLockEnabled = true
            tracker.trackEvent(Tracker.EVENT_BIOMETRICS_ENABLED)
        }
    )
}



