package com.tilig.android.ui.accounts.addaccount.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.sp
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.Folders
import com.tilig.android.ui.accounts.detail.components.WebViewEdit
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.components.*
import com.tilig.android.ui.secrets.add.AddToFolderComponent
import com.tilig.android.ui.secrets.components.AddSecretItemNameAndLogoView
import com.tilig.android.ui.secrets.components.SaveSecretItemButton
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.SharedPrefs
import com.tilig.android.utils.UITestTags
import org.koin.androidx.compose.inject

@Composable
fun CreateAccount(
    tracker: Tracker,
    folders: Folders?,
    account: Account,
    buttonText: String = stringResource(id = R.string.save_login),
    onSaveAccount: (Account) -> Unit,
    onUpdateWebsite: (String?) -> Unit
) {
    val focusManager = LocalFocusManager.current
    val context = LocalContext.current
    val prefs: SharedPrefs by inject()

    val displayNameError = remember { mutableStateOf(false) }
    val displayWebsiteError = remember { mutableStateOf(false) }
    val displayEmailError = remember { mutableStateOf(false) }
    val displayPasswordError = remember { mutableStateOf(false) }

    val emailState =
        remember {
            TextFieldState(if (!account.username.isNullOrEmpty()) account.username else
                prefs.email, validator = { !displayEmailError.value })
        }
    val nameState =
        remember {
            TextFieldState(
                account.name,
                validator = { !displayNameError.value })
        }
    val webState =
        remember {
            TextFieldState(
                account.website,
                validator = { !displayWebsiteError.value })
        }
    val accountIcon = account.getIcon(context)
    val passwordState =
        remember {
            TextFieldState(
                if (account.areDetailsDecrypted) account.password else null,
                validator = { !displayPasswordError.value })
        }
    val selectedFolder = remember { mutableStateOf<Folder?>(null) }

    val scrollableState = rememberScrollState()

    val saveAccountCall = {
        onSaveAccount.invoke(
            account.apply {
                name = nameState.text
                username = emailState.text
                website = webState.text
                password = passwordState.text
                folder = selectedFolder.value
            }
        )
    }

    Column(
        modifier = Modifier
            .testTag(UITestTags.createAccountScreenTestTag)
            .padding(
                start = DefaultPadding,
                end = DefaultPadding,
                bottom = HalfPadding
            )
            .fillMaxSize()
            .verticalScroll(scrollableState)
    ) {
        val hasAppId = account.androidAppId != null
        AddSecretItemNameAndLogoView(
            focusManager = focusManager,
            nameState = nameState,
            icon = accountIcon,
            hintInputRes = R.string.label_login_name,
            onValueChanged = {
                displayNameError.value = false
            })
        TiligSpacerVerticalMedium()
        WebViewEdit(
            focusManager = focusManager, websiteState = webState,
            titleTextStyle = Typography.h3,
            contentTextStyle = TextStyle(
                fontWeight = FontWeight.Normal,
                fontSize = 14.sp,
                color = DefaultTextColor,
                fontFamily = StolzlBook
            ), onFocusLost = {
                onUpdateWebsite(webState.text)
            },
            onValueChanged = {
                displayWebsiteError.value = false
            },
            isApp = hasAppId,
            androidAppId = account.androidAppId
        )
        TiligSpacerVerticalDefault()
        EmailUsernameView(focusManager = focusManager, emailState = emailState, onValueChanged = {
            displayEmailError.value = false
        })

        TiligSpacerVerticalDefault()
        CreateAccountPasswordView(
            tracker = tracker,
            passwordState = passwordState,
            onDone = {
                saveAccountCall.invoke()
            },
            onValueChanged = {
                displayPasswordError.value = false
            }
        )

        TiligSpacerVerticalDefault()
        AddToFolderComponent(
            modifier = Modifier,
            folders = folders,
            selectedFolder = selectedFolder.value,
            onFolderChanged = { selectedFolder.value = it }
        )

        TiligSpacerVerticalDefault()
        val hasValidationError = nameState.text.isEmpty()
                || (webState.text.isEmpty() && !hasAppId)
                || emailState.text.isEmpty()
                || passwordState.text.isEmpty()
        SaveSecretItemButton(
            modifier = Modifier
                .align(alignment = Alignment.CenterHorizontally),
            text = buttonText,
            enabled = !hasValidationError,
            onSave = {
                if (isSaveButtonEnabled(
                        emailState = emailState,
                        nameState = nameState,
                        webState = webState,
                        hasAppId = hasAppId,
                        passwordState = passwordState,
                        displayEmailError = displayEmailError,
                        displayNameError = displayNameError,
                        displayPasswordError = displayPasswordError,
                        displayWebsiteError = displayWebsiteError
                    )
                ) {
                    saveAccountCall()
                }
            }
        )
        TiligSpacerVerticalDefault()
    }
}

@Composable
private fun EmailUsernameView(
    focusManager: FocusManager,
    emailState: TextFieldState,
    onValueChanged: ((String) -> Unit)? = null,
) {
    Column {
        FieldTitle(
            title = stringResource(id = R.string.label_login_username_email),
            modifier = Modifier
        )
        BorderedBasicTextField(
            hint = stringResource(id = R.string.hint_login_username_email),
            state = emailState,
            keyboardType = KeyboardType.Text,
            onNext = {
                focusManager.moveFocus(FocusDirection.Down)
            },
            errorHandler = ErrorHandler(
                errorColor = DefaultErrorColor
            ),
            onValueChanged = onValueChanged
        )
    }
}

private fun isSaveButtonEnabled(
    nameState: TextFieldState,
    webState: TextFieldState,
    hasAppId: Boolean,
    emailState: TextFieldState,
    passwordState: TextFieldState,
    displayNameError: MutableState<Boolean>,
    displayWebsiteError: MutableState<Boolean>,
    displayEmailError: MutableState<Boolean>,
    displayPasswordError: MutableState<Boolean>
): Boolean {
    var allFieldsEntered = true
    displayNameError.value = nameState.text.isEmpty()
    displayWebsiteError.value = webState.text.isEmpty() && !hasAppId
    displayEmailError.value = emailState.text.isEmpty()
    displayPasswordError.value = passwordState.text.isEmpty()
    if (displayNameError.value || displayWebsiteError.value || displayEmailError.value || displayPasswordError.value) {
        allFieldsEntered = false
    }
    return allFieldsEntered
}

