package com.tilig.android.ui.notes.details.holders

import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.Folders
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.notes.details.NoteEditWrapper
import com.tilig.android.ui.secrets.CustomFieldStateWrapper

// This is to preserve the state of Compose so that it is not bound to any other lifecycle.
data class EditNoteStateHolder(
    val foldersList: Folders?,
    val nameState: TextFieldState,
    val contentState: TextFieldState,
    val customFields: List<CustomFieldStateWrapper>,
    val selectedFolder: Folder?)

@Composable
fun rememberEditNoteStateHolder(
    foldersList: Folders?,
    noteEditWrapper: NoteEditWrapper
) =
    EditNoteStateHolder(
        foldersList = foldersList,
        nameState = remember {
            TextFieldState(
                noteEditWrapper.name,
                validator = { it.isNotEmpty() })
        },
        contentState = remember {
            TextFieldState(
                noteEditWrapper.content,
                validator = { it.isNotEmpty() })
        },
        customFields =
        noteEditWrapper.customFields?.map {
            CustomFieldStateWrapper(it)
        } ?: emptyList(),
        selectedFolder = noteEditWrapper.selectedFolder
    )