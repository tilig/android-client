package com.tilig.android.ui.settings.language

import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.ArrowDropUp
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import com.tilig.android.R
import com.tilig.android.ui.components.FieldTitle
import com.tilig.android.ui.theme.DefaultBorderColor
import com.tilig.android.ui.theme.DefaultIconColor
import com.tilig.android.ui.theme.FontSizeNormal
import com.tilig.android.ui.theme.StolzlBook

// TODO currently is not used. Only English is supported.
@Preview
@Composable
fun LanguageInfoView() {
    var expanded by remember { mutableStateOf(false) }
    var selectedIndex by remember { mutableStateOf(0) }
    val trailingIcon = if (expanded) Icons.Filled.ArrowDropUp else Icons.Filled.ArrowDropDown
    var rowSize by remember { mutableStateOf(Size.Zero) }
    val disabledLabel = stringResource(id = R.string.label_coming_soon)

    val languages = arrayListOf(
        Pair(stringResource(id = R.string.lang_en), painterResource(id = R.drawable.flag_en)),
        Pair(stringResource(id = R.string.lang_nl), painterResource(id = R.drawable.flag_nl))
    )

    FieldTitle(title = stringResource(id = R.string.label_language_selection))
    Row(
        modifier = Modifier
            .padding(top = 8.dp)
            .border(width = 1.dp, color = DefaultBorderColor, shape = RoundedCornerShape(8.dp))
            .fillMaxWidth()
            .wrapContentHeight()
            .defaultMinSize(minHeight = 48.dp)
            .padding(16.dp)
            .onGloballyPositioned { layoutCoordinates ->
                rowSize = layoutCoordinates.size.toSize()
            },
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        LanguageItemView(
            image = languages[selectedIndex].second,
            text = languages[selectedIndex].first,
            textModifier = Modifier
                .weight(1f)
                .clickable(onClick = { expanded = true })
        )
        Icon(
            imageVector = trailingIcon,
            contentDescription = null,
            tint = DefaultIconColor
        )
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier
                .width(with(LocalDensity.current) { rowSize.width.toDp() })
                .padding(horizontal = 8.dp)
        ) {
            languages.forEachIndexed { index, item ->
                DropdownMenuItem(
                    enabled = index == 0,  // the only available language for now is in index 0
                    onClick = {
                        selectedIndex = index
                        expanded = false
                    }) {
                    val finalText = if (index > 0) "${item.first} $disabledLabel" else item.first
                    LanguageItemView(image = item.second, text = finalText)
                }
            }
        }
    }
}

@Composable
fun LanguageItemView(
    image: Painter,
    text: String,
    textModifier: Modifier = Modifier,
    flagModifier: Modifier = Modifier
) {
    Image(
        painter = image,
        contentDescription = "",
        contentScale = ContentScale.Crop,
        modifier = flagModifier
            .size(20.dp)
            .clip(CircleShape)
    )
    Text(
        text = text, modifier = textModifier.padding(start = 8.dp),
        fontFamily = StolzlBook,
        fontSize = FontSizeNormal
    )
}
