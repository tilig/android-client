package com.tilig.android.ui.cards.details

import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.SecretItemType
import com.tilig.android.data.models.tilig.CreditCard
import com.tilig.android.data.models.tilig.CustomField
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.SecretItem
import com.tilig.android.ui.notes.details.NoteEditWrapper
import com.tilig.android.ui.secrets.details.ItemDetailsViewModel
import com.tilig.android.ui.secrets.details.events.BaseEditEvent
import com.tilig.android.ui.secrets.details.states.ItemEditWrapper
import com.tilig.android.ui.secrets.details.states.ItemDetailsUiState
import kotlinx.coroutines.flow.update

data class CardEditWrapper(
    val name: String? = null,
    val cardNumber: String? = null,
    val holderName: String? = null,
    val expirationDate: String? = null,
    val secureCode: String? = null,
    val pinCode: String? = null,
    val zipCode: String? = null,
    val extraInfo: String? = null,
    override val customFields: List<CustomField>? = null,
    override val selectedFolder: Folder? = null
) : ItemEditWrapper {
    constructor(card: CreditCard) : this(
        name = card.name,
        cardNumber = card.cardNumber,
        holderName = card.holderName,
        expirationDate = card.expireDate,
        secureCode = card.securityCode,
        pinCode = card.pinCode,
        zipCode = card.zipCode,
        extraInfo = card.extraInfo,
        customFields = card.details.customFields,
        selectedFolder = card.folder
    )

    override fun hadDataChanged(original: SecretItem): Boolean {
        return (original as CreditCard).name != name ||
                original.cardNumber != cardNumber ||
                original.holderName != holderName ||
                original.expireDate != expirationDate ||
                original.securityCode != secureCode ||
                original.pinCode != pinCode ||
                original.zipCode != zipCode ||
                original.extraInfo != extraInfo ||
                original.folder != selectedFolder ||
                original.details.customFields != customFields
    }

    override fun getItemName(): String? = name
}

sealed interface CardEditEvents : BaseEditEvent {
    data class NameChanged(val name: String) : CardEditEvents
    data class CardNumberChanged(val cardNumber: String) : CardEditEvents
    data class HolderNameChanged(val holderName: String) : CardEditEvents
    data class ExpireDateChanged(val expireDate: String) : CardEditEvents
    data class SecurityCodeChanged(val securityCode: String) : CardEditEvents
    data class PinCodeChanged(val pinCode: String) : CardEditEvents
    data class ZipCodeChanged(val zipCode: String) : CardEditEvents
    data class ExtraInfoChanged(val extraInfo: String) : CardEditEvents
}

class CardViewModel : ItemDetailsViewModel() {
    override fun getSecretItemType(): SecretItemType = SecretItemType.CREDIT_CARD

    override fun getItemEditWrapper(item: SecretItem?): ItemEditWrapper {
        return if (item != null) CardEditWrapper(item as CreditCard) else CardEditWrapper()
    }

    override fun updateCustomFields(customFields: List<CustomField>?): ItemEditWrapper {
        return (viewModelState.value.itemEditWrapper as CardEditWrapper).copy(
                customFields = customFields
            )
    }

    override fun updateFolder(folder: Folder?): ItemEditWrapper {
        return (viewModelState.value.itemEditWrapper as CardEditWrapper).copy(
            selectedFolder = folder
        )
    }

    override fun onEditEvent(event: BaseEditEvent) {
        if (event is CardEditEvents) {
            viewModelState.update {
                it.copy(
                    itemEditWrapper =
                    when (event) {
                        is CardEditEvents.CardNumberChanged -> (it.itemEditWrapper as CardEditWrapper).copy(
                            cardNumber = event.cardNumber
                        )
                        is CardEditEvents.ExpireDateChanged -> (it.itemEditWrapper as CardEditWrapper).copy(
                            expirationDate = event.expireDate
                        )
                        is CardEditEvents.ExtraInfoChanged -> (it.itemEditWrapper as CardEditWrapper).copy(
                            extraInfo = event.extraInfo
                        )
                        is CardEditEvents.HolderNameChanged -> (it.itemEditWrapper as CardEditWrapper).copy(
                            holderName = event.holderName
                        )
                        is CardEditEvents.NameChanged -> (it.itemEditWrapper as CardEditWrapper).copy(
                            name = event.name
                        )
                        is CardEditEvents.PinCodeChanged -> (it.itemEditWrapper as CardEditWrapper).copy(
                            pinCode = event.pinCode
                        )
                        is CardEditEvents.SecurityCodeChanged -> (it.itemEditWrapper as CardEditWrapper).copy(
                            secureCode = event.securityCode
                        )
                        is CardEditEvents.ZipCodeChanged -> (it.itemEditWrapper as CardEditWrapper).copy(
                            zipCode = event.zipCode
                        )
                    }
                )
            }
        } else {
            super.onEditEvent(event)
        }
    }

    override fun buildItemWithUpdatedItems(): SecretItem? {
        val currentState = uiState.value
        if (currentState is ItemDetailsUiState.ItemEdit) {
            val editable = currentState.itemEditWrapper as CardEditWrapper
            return (currentState.item as CreditCard).apply {
                this.name = editable.name
                this.cardNumber = editable.cardNumber
                this.holderName = editable.holderName
                this.expireDate = editable.expirationDate
                this.pinCode = editable.pinCode
                this.zipCode = editable.zipCode
                this.securityCode = editable.secureCode
                this.extraInfo = editable.extraInfo
                // ignore fields with empty names
                editable.customFields?.filter { it.name.isNotEmpty() }?.let {
                    this.details.customFields = it.toMutableList()
                }
                updateFolderField(this, editable.selectedFolder)
            }
        }
        return null
    }

    override fun trackDeleteItem() {
        tracker.trackEvent(Tracker.EVENT_CREDIT_CARD_DELETED)
    }
}