package com.tilig.android.ui.components

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import com.tilig.android.ui.theme.TiligGreen
import com.tilig.android.ui.theme.Typography

data class AppendableText(
    val text: String,
    val isStyled: Boolean,
    val addSpace: Boolean = true,
    val tag: String? = null,
    val annotation: String? = null
)

@Composable
fun buildAnnotatedString(
    appendableTexts: List<AppendableText>,
    spanStyle: SpanStyle
) = buildAnnotatedString {
    for (txt in appendableTexts) {
        if (txt.tag != null) {
            addStringAnnotation(
                txt.tag,
                txt.annotation ?: "",
                this.length - 1,
                this.length + txt.text.length - 1
            )
        }
        if (!txt.isStyled) {
            append(txt.text)
        } else {
            append(
                AnnotatedString(
                    txt.text,
                    spanStyle = spanStyle
                )
            )
        }
        if (txt.addSpace) {
            append(" ")
        }
    }
}

@Composable
@Preview(showBackground = true)
fun AppendableTextView(
    @PreviewParameter(SampleAppendableTextProvider::class)
    appendableTexts: List<AppendableText>,
    modifier: Modifier = Modifier,
    textAlign: TextAlign = TextAlign.Center,
    spanStyle: SpanStyle = SpanStyle(TiligGreen, fontWeight = FontWeight.Bold),
    textStyle: TextStyle = Typography.h4
) {
    Text(
        modifier = modifier,
        text = buildAnnotatedString(
            appendableTexts = appendableTexts,
            spanStyle = spanStyle
        ), style = textStyle, textAlign = textAlign
    )
}

class SampleAppendableTextProvider : PreviewParameterProvider<AppendableText> {
    override val values = sequenceOf(
        AppendableText("Welcome to Tilig. We store and fill your", isStyled = false),
        AppendableText("passwords", isStyled = true),
        AppendableText(
            "on all your devices. It's super easy, everyone can use it.",
            isStyled = false
        )
    )
}
