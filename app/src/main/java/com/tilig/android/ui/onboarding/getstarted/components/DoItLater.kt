package com.tilig.android.ui.onboarding.getstarted.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.theme.LightBlue
import com.tilig.android.ui.theme.StyleStolzBookDarkBlue14

@Composable
fun ColumnScope.DoItLater(onSkip: () -> Unit) {
    Spacer(modifier = Modifier.height(12.dp))
    Text(
        modifier = Modifier
            .padding(6.dp)
            .clickable {
                onSkip.invoke()
            }
            .align(Alignment.CenterHorizontally),
        text = stringResource(id = R.string.do_it_later),
        style = StyleStolzBookDarkBlue14.copy(color = LightBlue)
    )
    Spacer(modifier = Modifier.height(20.dp))
}
