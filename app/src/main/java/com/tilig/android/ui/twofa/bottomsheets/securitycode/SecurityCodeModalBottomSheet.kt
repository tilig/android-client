package com.tilig.android.ui.twofa.bottomsheets.securitycode

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.insets.navigationBarsWithImePadding
import com.tilig.android.R
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.ui.components.ScreenHeader
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.theme.DarkBlue
import com.tilig.android.ui.theme.Grey
import com.tilig.android.ui.theme.LightBlue
import com.tilig.android.ui.theme.Typography
import com.tilig.android.ui.twofa.SecurityCodeComponent

private val ContentHorizontalPadding = 32.dp

@OptIn(androidx.compose.animation.ExperimentalAnimationApi::class)
@Composable
fun SecurityCodeModalBottomSheet(
    account: Account,
    closeSheet: () -> Unit
) {
    val bottomSheetHeight = LocalConfiguration.current.screenHeightDp.dp - 24.dp

    Column(
        modifier = Modifier
            .navigationBarsWithImePadding()
            .height(bottomSheetHeight)
    ) {
        ScreenHeader(headerTextRes = R.string.security_code, closeSheet = closeSheet)
        Image(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(top = 23.dp),
            painter = painterResource(id = R.drawable.ic_2fa_security_code),
            contentDescription = null
        )
        Text(
            modifier = Modifier.fillMaxWidth(),
            text = stringResource(id = R.string.security_code),
            style = Typography.h1,
            fontSize = 24.sp,
            color = DarkBlue,
            textAlign = TextAlign.Center
        )
        Spacer(modifier = Modifier.height(12.dp))
        Text(
            text = stringResource(
                id = R.string.please_fill_in_this_security_code,
                account.domain ?: account.name ?: stringResource(id = R.string.untitled_account)
            ),
            modifier = Modifier.padding(horizontal = ContentHorizontalPadding),
            textAlign = TextAlign.Center
        )
        Spacer(modifier = Modifier.height(34.dp))
        SecurityCodeComponent(
            token = account.otp!!,
            modifier = Modifier
                .height(96.dp)
                .padding(horizontal = ContentHorizontalPadding)
                .background(
                    color = Grey,
                    shape = RoundedCornerShape(8.dp)
                )
                .border(width = 1.dp, color = LightBlue, shape = RoundedCornerShape(8.dp))
        )
        Spacer(modifier = Modifier.height(32.dp))
        Text(
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .fillMaxWidth(),
            text = stringResource(id = R.string.note_code_changes),
            textAlign = TextAlign.Center
        )
        Box(
            modifier = Modifier
                .weight(1f)
                .align(alignment = Alignment.CenterHorizontally)
        ) {
            TiligButton(
                modifier = Modifier
                    .align(alignment = Alignment.BottomCenter)
                    .padding(ContentHorizontalPadding)
                    .fillMaxWidth()
                    .height(48.dp),
                iconRes = null,
                text = stringResource(R.string.done),
                enabled = true,
                isSizeConstrained = false,
                isDark = true,
                onClick = {
                    closeSheet()
                })
        }

    }
}

@Preview
@Composable
fun SecurityCodeModalBottomSheetPreview() {
    Column {
        // 6 digits
        SecurityCodeModalBottomSheet(
            account = Account.createEmpty().apply {
                name = "Lorem ipsum"
                otp =
                    "otpauth://totp/Test%3Alorem?secret=5ERBJVOI2WMRNDFP&issuer=Tilig&algorithm=SHA1&digits=6&period=30"
            },
            closeSheet = {}
        )
        // 8 digits
        SecurityCodeModalBottomSheet(
            account = Account.createEmpty().apply {
                name = "Lorem ipsum"
                otp =
                    "otpauth://totp/Test%3Alorem?secret=5ERBJVOI2WMRNDFP&issuer=Tilig&algorithm=SHA1&digits=8&period=30"
            },
            closeSheet = {}
        )
    }
}
