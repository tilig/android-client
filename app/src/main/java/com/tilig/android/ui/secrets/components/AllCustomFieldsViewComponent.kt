package com.tilig.android.ui.secrets.components

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.tilig.CustomField
import com.tilig.android.data.models.tilig.ItemFieldType
import com.tilig.android.data.models.tilig.SecretItem
import com.tilig.android.ui.components.TiligSpacerVerticalDefault
import com.tilig.android.ui.twofa.TwoFactorAuthEnabled
import com.tilig.android.utils.DateUtils
import com.tilig.android.utils.VersionUtil
import com.tilig.android.utils.copyToClipboard
import java.text.SimpleDateFormat
import java.util.*

@Composable
fun AllCustomFieldsViewComponent(
    fields: List<CustomField>,
    onShowSnackbarMessage: (String) -> Unit
) {
    fields.forEach { field ->
        // don't display fields without values or empty fields
        if(!field.value.isNullOrEmpty()) {
            CustomFieldTextViewComponent(
                field = field,
                onShowSnackbarMessage = onShowSnackbarMessage
            )
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun CustomFieldTextViewComponent(
    modifier: Modifier = Modifier,
    field: CustomField,
    onShowSnackbarMessage: (String) -> Unit
) {
    val context = LocalContext.current
    val copiedMessage = stringResource(id = R.string.copied_custom_field_name, field.name)

    val copyAction: (String?, String) -> Unit = { message, copyMessage ->
        if (message?.copyToClipboard(context) == true
            && !VersionUtil.showsSystemMessageOnCopyToClipboard()
        ) {
            onShowSnackbarMessage.invoke(copyMessage)
        }
    }

    TiligSpacerVerticalDefault()
    BoxWithTitledContent(
        modifier = modifier.fillMaxWidth(),
        titleStr = field.name,
        onClick = {
            copyAction(field.value, copiedMessage)
        }
    ) {
        if (field.kind == ItemFieldType.TOTP) {
            if (!field.value.isNullOrEmpty()) {
                TwoFactorAuthEnabled(
                    modifier = Modifier
                        .fillMaxSize(),
                    otp = field.value)
            } else {
                DetailsTextField(
                    value = "-",
                    enabled = true
                )
            }
        } else {
            val fieldValue = remember {
                if (field.kind == ItemFieldType.DATE) field.value?.let {
                    DateUtils.formatDate(
                        context = context,
                        originDate = it,
                        originalFormat = SimpleDateFormat(
                            DateUtils.API_DATE_FORMAT,
                            Locale.getDefault()
                        ).apply {
                            timeZone = TimeZone.getTimeZone("UTC")
                        },
                        targetFormat = DateUtils.DATE_SLASH_FORMAT
                    )
                } else field.value
            }
            val supportHiddenFields = remember {
                field.kind != ItemFieldType.TEXT && field.kind != ItemFieldType.DATE
            }
            DetailsTextField(
                value = fieldValue,
                showRevealButton = supportHiddenFields,
                showContentInitially = !supportHiddenFields
            )
        }
    }
}