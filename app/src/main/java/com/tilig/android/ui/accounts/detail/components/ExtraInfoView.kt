package com.tilig.android.ui.accounts.detail.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.secrets.components.BoxWithTitledContent
import com.tilig.android.ui.secrets.components.DetailsTextField

@Composable
fun ExtraInfoView(
    modifier: Modifier = Modifier,
    notes: String?,
    tracker: Tracker,
    onCopyClick: (() -> Unit)? = null
) {
    BoxWithTitledContent(
        modifier = modifier.fillMaxWidth(),
        tracker = tracker,
        event = Tracker.EVENT_EXTRA_INFO_COPY,
        titleStr = stringResource(id = R.string.label_extra_info),
        onClick = onCopyClick
    ) {
        DetailsTextField(
            value = notes,
            enabled = true
        )
    }
}