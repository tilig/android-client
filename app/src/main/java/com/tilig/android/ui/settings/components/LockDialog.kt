package com.tilig.android.ui.settings.components

import android.content.Context
import android.content.Intent
import android.os.Build
import android.provider.Settings
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.lifecycle.viewmodel.compose.viewModel
import com.tilig.android.R
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.components.TiligDialog
import com.tilig.android.ui.onboarding.getstarted.components.BiometricContent
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.SharedPrefs
import com.tilig.android.utils.lock.LockUtil
import com.tilig.android.utils.lock.LockUtilFactory
import com.tilig.android.utils.lock.LockViewModel
import org.koin.androidx.compose.inject

enum class LockUtilDialogState {
    NONE,
    INFORMATION,
    ENABLE_SYSTEM_FIRST,
    INSTRUCTIONS,
    GET_STARTED,
    NOT_SUPPORTED
}

fun enableBiometricsWarning(
    context: Context,
    lockUtil: LockUtil,
    tracker: Mixpanel,
    dialogState: MutableState<LockUtilDialogState>
) {
    if (LockUtil.isSupported() && lockUtil.areBiometricsSupported(context)) {
        dialogState.value = LockUtilDialogState.ENABLE_SYSTEM_FIRST
        tracker.trackEvent(Tracker.EVENT_BIOMETRICS_NOT_SET_UP)
    } else {
        dialogState.value = LockUtilDialogState.NOT_SUPPORTED
    }
}

@Preview
@Composable
fun LockDialogPreview() {
    val state = remember { mutableStateOf(LockUtilDialogState.INFORMATION) }
    LockDialog(dialogState = state, onEnabled = {})
}

@Composable
fun LockDialog(
    dialogState: MutableState<LockUtilDialogState>,
    lockViewModel: LockViewModel = viewModel(),
    onEnabled: () -> Unit
) {
    val prefs: SharedPrefs by inject()
    val context = LocalContext.current
    val isPossible = remember { lockViewModel.lockUtil.areBiometricsSupported(context) }
    val tracker: Mixpanel by inject()

    when (dialogState.value) {
        LockUtilDialogState.NONE -> {}
        LockUtilDialogState.ENABLE_SYSTEM_FIRST -> {
            TiligDialog(
                title = R.string.alert_title_biometrics_not_set_up,
                text = R.string.alert_body_biometrics_not_set_up,
                onDismissRequest = { dialogState.value = LockUtilDialogState.NONE },
                confirmButtonText = R.string.alert_confirm_biometrics_not_set_up,
                onConfirm = {
                    dialogState.value = LockUtilDialogState.NONE

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        context.startActivity(Intent(Settings.ACTION_BIOMETRIC_ENROLL))
                    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        context.startActivity(Intent(Settings.ACTION_FINGERPRINT_ENROLL))
                    } else {
                        context.startActivity(Intent(Settings.ACTION_SECURITY_SETTINGS))
                    }
                },
                cancelButtonText = R.string.alert_dismiss_biometrics_not_set_up,
                onCancel = {
                    dialogState.value = LockUtilDialogState.NONE
                }
            )
        }
        LockUtilDialogState.INFORMATION -> {
            if (prefs.isLockEnabled) {
                TiligDialog(
                    title = R.string.alert_title_biometrics_info,
                    text = R.string.alert_body_biometrics_info,
                    onDismissRequest = { dialogState.value = LockUtilDialogState.NONE },
                    confirmButtonText = android.R.string.ok,
                    onConfirm = {
                        dialogState.value = LockUtilDialogState.NONE
                    }
                )
            } else if (isPossible) {
                TiligDialog(
                    title = R.string.alert_title_biometrics_info,
                    text = R.string.alert_body_biometrics_info,
                    onDismissRequest = { dialogState.value = LockUtilDialogState.NONE },
                    confirmButtonText = R.string.alert_confirm_biometrics_info,
                    onConfirm = {
                        if (lockViewModel.lockUtil.canAuthorize(context)) {
                            onEnabled.invoke()
                            dialogState.value = LockUtilDialogState.NONE
                        } else {
                            dialogState.value = LockUtilDialogState.ENABLE_SYSTEM_FIRST
                        }
                    },
                    cancelButtonText = R.string.alert_dismiss_biometrics_info,
                    onCancel = {
                        dialogState.value = LockUtilDialogState.NONE
                    }
                )
            } else {
                TiligDialog(
                    title = R.string.alert_title_biometrics_info,
                    text = R.string.alert_body_biometrics_info_not_supported,
                    onDismissRequest = { dialogState.value = LockUtilDialogState.NONE },
                    confirmButtonText = R.string.alert_dismiss_biometrics_info,
                    onConfirm = {
                        dialogState.value = LockUtilDialogState.NONE
                    }
                )
            }
        }
        LockUtilDialogState.NOT_SUPPORTED -> {
            TiligDialog(
                title = R.string.alert_title_biometrics_not_supported,
                text = R.string.alert_body_biometrics_not_supported,
                onDismissRequest = { dialogState.value = LockUtilDialogState.NONE },
                confirmButtonText = R.string.alert_dismiss_biometrics_not_supported,
                onConfirm = {
                    dialogState.value = LockUtilDialogState.NONE
                }
            )
        }
        LockUtilDialogState.INSTRUCTIONS -> {
            Dialog(onDismissRequest = {}) {
                Column(
                    modifier = Modifier
                        .padding(DefaultPadding)
                        .background(color = White, shape = RoundedCornerShape(MediumCornerRadius))
                        .fillMaxWidth()
                        .padding(DefaultPadding),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    BiometricContent(onUseBiometric = {
                        dialogState.value = LockUtilDialogState.NONE
                        onEnabled.invoke()
                    }, onSkip = {
                        dialogState.value = LockUtilDialogState.NONE
                    })
                }
            }
        }
        LockUtilDialogState.GET_STARTED -> {
            val isFingerprint = remember { LockUtilFactory.isBiometricsUsingFingerprint(context) }
            TiligDialog(
                title = when (isFingerprint) {
                    true -> R.string.biometric_dialog_title_fingerprint
                    false -> R.string.biometric_dialog_title_faceid
                    null -> R.string.biometric_dialog_title_generic
                },
                text = when (isFingerprint) {
                    true -> R.string.biometric_dialog_content_fingerprint
                    false -> R.string.biometric_dialog_content_faceid
                    null -> R.string.biometric_dialog_content_generic
                },
                onDismissRequest = {
                    dialogState.value = LockUtilDialogState.NONE
                },
                confirmButtonText = R.string.allow,
                onConfirm = {
                    dialogState.value = LockUtilDialogState.NONE
                    onEnabled()
                },
                cancelButtonText = R.string.dont_allow,
                onCancel = {
                    dialogState.value = LockUtilDialogState.NONE
                }
            )
        }
    }
}
