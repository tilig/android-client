package com.tilig.android.ui.signin

import androidx.lifecycle.ViewModel
import com.google.firebase.auth.AdditionalUserInfo
import com.google.firebase.auth.FirebaseUser
import com.tilig.android.data.repository.Repository
import com.tilig.android.utils.SharedPrefs
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class AuthViewModel : ViewModel(), KoinComponent {

    private val repo by inject<Repository>()

    fun storeInTiligServer(publicKey: String) =
        repo.updatePublicKey(publicKey)

    fun fetchFromTiligServer() = repo.fetchProfile()

    fun deleteFromTiligServer() = repo.deleteProfile()

}
