package com.tilig.android.ui.onboarding.getstarted.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.tilig.android.MainRoute
import com.tilig.android.R
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.components.TiligCircularProgressIndicator
import com.tilig.android.ui.theme.*
import org.koin.androidx.compose.get
import org.koin.java.KoinJavaComponent.inject
import java.lang.Float.max

@Composable
fun GetStartedLabelComponent(
    navController: NavController,
    progress: Float,
    tracker: Tracker
) {
    Row(
        modifier = Modifier
            .padding(HalfPadding)
            .fillMaxWidth()
            .background(color = DeepGrey, shape = Shapes.medium)
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(color = Grey2),
            ) {
                navController.navigate(
                    MainRoute.GetStarted.route
                )
                tracker.trackEvent(Tracker.GETTING_STARTED_PROMPT_CLICKED)
            }
            .padding(MediumWithQuarter),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_airplane),
            contentDescription = null,
            tint = White
        )
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
                .padding(HalfPadding),
            text = stringResource(id = R.string.get_started),
            style = StyleStolzRegularDarkBlue14.copy(color = White)
        )
        TiligCircularProgressIndicator(
            progressFraction = max(progress, 0.05f),
            stroke = 4.dp,
            size = 32.dp,
            bgColor = White.copy(alpha = 0.3f),
            progressColor = White
        )
    }
}

@Composable
@Preview
fun GetStartedLabelComponentPreview() {
    GetStartedLabelComponent(get(), 0f, get())
}
