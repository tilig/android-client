package com.tilig.android.ui.onboarding

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.insets.navigationBarsWithImePadding
import com.tilig.android.R
import com.tilig.android.ui.components.ScreenHeader
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.UITestTags

@Composable
fun MoreInfoSignInBottomSheet(closeSheet: () -> Unit) {
    Column(
        modifier = Modifier
            .testTag(UITestTags.onboardingMoreInfoModalTestTag)
            .navigationBarsWithImePadding()
    ) {
        ScreenHeader(
            headerTextRes = R.string.more_info_sign_in_header,
            closeSheet = closeSheet
        )
        Spacer(modifier = Modifier.height(32.dp))
        Row(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(top = 14.dp)
        ) {
            Image(
                modifier = Modifier.size(56.dp),
                painter = painterResource(id = R.drawable.ic_login_apple),
                contentDescription = null,
                contentScale = ContentScale.FillBounds
            )
            Spacer(modifier = Modifier.width(32.dp))
            Image(
                modifier = Modifier.size(56.dp),
                painter = painterResource(id = R.drawable.ic_login_google),
                contentDescription = null,
                contentScale = ContentScale.FillBounds
            )
            Spacer(modifier = Modifier.width(32.dp))
            Image(
                modifier = Modifier.size(56.dp),
                painter = painterResource(id = R.drawable.ic_login_microsoft),
                contentDescription = null,
                contentScale = ContentScale.FillBounds
            )
        }
        Spacer(modifier = Modifier.height(34.dp))
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.CenterHorizontally)
                .padding(
                    top = HalfPadding,
                    bottom = MediumPadding,
                    start = DefaultPadding,
                    end = DefaultPadding
                ),
            textAlign = TextAlign.Start,
            text = stringResource(id = R.string.more_info_sign_in_title),
            style = Typography.h1.copy(lineHeight = 26.sp),
            fontSize = 24.sp,
            color = DarkBlue,
        )
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(horizontal = DefaultPadding),
            text = stringResource(id = R.string.more_info_sign_in_desc),
            style = Typography.h4.copy(lineHeight = 24.sp),
            textAlign = TextAlign.Start,
            fontSize = 16.sp,
            color = DarkBlue,
        )
        Spacer(modifier = Modifier.height(16.dp))

        BackHandler {
            closeSheet.invoke()
        }
    }
}