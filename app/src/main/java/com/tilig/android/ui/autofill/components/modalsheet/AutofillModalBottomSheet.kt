package com.tilig.android.ui.autofill.components.modalsheet

import androidx.compose.animation.core.TweenSpec
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.isSpecified
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.semantics.*
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntOffset
import kotlin.math.max
import kotlin.math.roundToInt

/**
 * Custom modal sheet that helps to disable swipe down the sheet.
 * If content has own inner scroll it will ignore swipe gestures
 */
@Composable
@ExperimentalMaterialApi
fun AutofillModalBottomSheetLayout(
    sheetContent: @Composable ColumnScope.() -> Unit,
    modifier: Modifier = Modifier,
    sheetState: ModalBottomSheetState =
        rememberModalBottomSheetState(ModalBottomSheetValue.Hidden),
    sheetShape: Shape = MaterialTheme.shapes.large,
    sheetElevation: Dp = ModalBottomSheetDefaults.Elevation,
    sheetBackgroundColor: Color = MaterialTheme.colors.surface,
    sheetContentColor: Color = contentColorFor(sheetBackgroundColor),
    scrimColor: Color = ModalBottomSheetDefaults.scrimColor,
    content: @Composable () -> Unit
) {

    BoxWithConstraints(modifier) {
        val fullHeight = constraints.maxHeight.toFloat()
        val sheetHeightState = remember { mutableStateOf<Float?>(null) }
        Box(Modifier.fillMaxSize()) {
            content()
            Scrim(
                color = scrimColor,
                visible = sheetState.targetValue != ModalBottomSheetValue.Hidden
            )
        }
        Surface(
            Modifier
                .fillMaxWidth()
                .offset {
                    IntOffset(0, sheetState.offset.value.roundToInt())
                }
                .bottomSheetSwipeable(
                    sheetState,
                    fullHeight,
                    sheetHeightState
                )
                .onGloballyPositioned {
                    sheetHeightState.value = it.size.height.toFloat()
                },
            shape = sheetShape,
            elevation = sheetElevation,
            color = sheetBackgroundColor,
            contentColor = sheetContentColor
        ) {
            Column(content = sheetContent)
        }
    }
}

@Composable
private fun Scrim(
    color: Color,
    visible: Boolean
) {
    if (color.isSpecified) {
        val alpha by animateFloatAsState(
            targetValue = if (visible) 1f else 0f,
            animationSpec = TweenSpec()
        )

        Canvas(
            Modifier
                .fillMaxSize()
        ) {
            drawRect(color = color, alpha = alpha)
        }
    }
}

@Suppress("ModifierInspectorInfo")
@OptIn(ExperimentalMaterialApi::class)
private fun Modifier.bottomSheetSwipeable(
    sheetState: ModalBottomSheetState,
    fullHeight: Float,
    sheetHeightState: State<Float?>
): Modifier {
    val sheetHeight = sheetHeightState.value
    // !sheetState.isAnimationRunning - disable swipe when modal is animating
    val modifier = if (sheetHeight != null && !sheetState.isAnimationRunning) {
        val anchors = mapOf(
            fullHeight to ModalBottomSheetValue.Hidden,
            max(0f, fullHeight - sheetHeight) to ModalBottomSheetValue.Expanded
        )
        Modifier.swipeable(
            state = sheetState,
            anchors = anchors,
            orientation = Orientation.Vertical,
            resistance = null
        )
    } else {
        Modifier
    }

    return this.then(modifier)
}