package com.tilig.android.ui.settings.components

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.moshi.Moshi
import com.tilig.android.BuildConfig
import com.tilig.android.R
import com.tilig.android.data.livedata.ForcedLogoutLiveData
import com.tilig.android.data.models.SecretItemType
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.models.tilig.OkResponse
import com.tilig.android.data.models.tilig.Profile
import com.tilig.android.ui.CryptoModel
import com.tilig.android.ui.components.FieldTitle
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.components.TiligCheckBox
import com.tilig.android.ui.components.TiligSpacerHorizontalDefault
import com.tilig.android.ui.settings.SettingsViewModel
import com.tilig.android.ui.signin.AuthViewModel
import com.tilig.android.ui.signin.SignInViewModel
import com.tilig.android.ui.theme.DarkBlue
import com.tilig.android.ui.theme.DarkGrey
import com.tilig.android.utils.SharedPrefs
import com.tilig.crypto.Crypto
import kotlinx.coroutines.*
import org.koin.androidx.compose.get
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.math.BigInteger
import java.security.MessageDigest

@Preview
@Composable
fun KeyResetView(
    authStatus: ForcedLogoutLiveData = get(),
    prefs: SharedPrefs = get(),
    authViewModel: AuthViewModel = viewModel(),
    settingsViewModel: SettingsViewModel = viewModel(),
    cryptoModel: CryptoModel = get()
) {

    val coroutineScope = rememberCoroutineScope()
    val context = LocalContext.current

    Column {
        FieldTitle(title = stringResource(id = R.string.debug_header_accounts))
        TiligButton(
            modifier = Modifier
                .padding(top = 8.dp)
                .fillMaxWidth(),
            iconRes = null,
            text = stringResource(R.string.bt_debug_delete_accounts),
            enabled = true,
            isDark = true,
            isSizeConstrained = true,
            onClick = {
                AlertDialog.Builder(context)
                    .setMessage(R.string.debug_dialog_generic_confirm)
                    .setPositiveButton(R.string.bt_debug_delete_accounts) { _, _ ->
                        coroutineScope.async(Dispatchers.IO + Job()) {
                            deleteAllAccounts(settingsViewModel)
                        }
                    }.setNegativeButton(android.R.string.cancel, null)
                    .show()
            })

        val itemsToGenerate = remember {
            mutableStateMapOf(
                SecretItemType.ACCOUNT to true,
                SecretItemType.NOTE to true,
                SecretItemType.WIFI to true,
                SecretItemType.CREDIT_CARD to true
            )
        }
        val rowScrollable = rememberScrollState()

        TiligButton(
            modifier = Modifier
                .padding(top = 8.dp)
                .fillMaxWidth(),
            iconRes = null,
            text = stringResource(R.string.bt_debug_create_dummy),
            enabled = true,
            isDark = true,
            isSizeConstrained = true,
            onClick = {
                coroutineScope.async(Dispatchers.IO + Job()) {
                    createDummyItems(
                        settingsViewModel,
                        cryptoModel,
                        itemsToGenerate.filterValues { it }.map { it.key })
                }
            })
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .horizontalScroll(rowScrollable), horizontalArrangement = Arrangement.Center
        ) {
            itemsToGenerate.forEach { item ->
                TiligCheckBox(
                    modifier = Modifier,
                    checked = item.value,
                    onCheckedChange = { itemsToGenerate[item.key] = it })
                Text(
                    modifier = Modifier.padding(start = 4.dp),
                    text = stringResource(
                        id = when (item.key) {
                            SecretItemType.ACCOUNT -> R.string.logins
                            SecretItemType.NOTE -> R.string.notes
                            SecretItemType.CREDIT_CARD -> R.string.cards
                            SecretItemType.WIFI -> R.string.wifi
                            SecretItemType.OTHER -> R.string.others
                        }
                    )
                )
                TiligSpacerHorizontalDefault()
            }
        }

        FieldTitle(title = stringResource(id = R.string.debug_header_keys))
        TiligButton(
            modifier = Modifier
                .padding(top = 8.dp)
                .fillMaxWidth(),
            iconRes = null,
            text = stringResource(R.string.bt_debug_reset_keys),
            enabled = true,
            isDark = true,
            isSizeConstrained = true,
            onClick = {
                AlertDialog.Builder(context)
                    .setMessage(R.string.debug_dialog_reset_keys)
                    .setPositiveButton(R.string.generic_continue) { _, _ ->
                        coroutineScope.launch {
                            // FIXME this shouldn't be updating the keys in the prefs here,
                            //   it should be routed through the CryptoModel instead.
                            val crypto = withContext(Dispatchers.IO) {
                                Crypto().apply {
                                    generateKey()
                                    generateLegacyKey()
                                    prefs.updateLegacyPrivateKey(legacyPrivateKeyAsString())
                                    prefs.updatePrivateKey(getKeyPairAsStrings()?.private)
                                    prefs.updateLegacyPublicKey(legacyPublicKeyAsString())
                                    prefs.updatePublicKey(getKeyPairAsStrings()?.public)
                                }
                            }
                            storeInFirestore(
                                authViewModel,
                                crypto,
                                prefs,
                                context
                            )
                        }
                    }.setNegativeButton(android.R.string.cancel, null)
                    .show()

            })
        TiligButton(
            modifier = Modifier
                .padding(top = 8.dp)
                .fillMaxWidth(),
            iconRes = null,
            text = stringResource(R.string.bt_debug_show_local_legacy_keys),
            enabled = true,
            isDark = true,
            isSizeConstrained = true,
            onClick = {
                cryptoModel.withInitializedCrypto {
                    val privKey = it.legacyPrivateKeyAsString()
                    val pubKey = it.legacyPublicKeyAsString()
                    AlertDialog.Builder(context)
                        .setMessage("Hash: ${md5(privKey)}\nKey:\n$privKey")
                        .setPositiveButton(android.R.string.ok) { di, _ ->
                            di.dismiss()
                            AlertDialog.Builder(context)
                                .setMessage("Hash: ${md5(pubKey)}\nKey:\n$pubKey")
                                .setPositiveButton(android.R.string.ok, null)
                                .show()
                        }
                        .show()
                }
            })
        TiligButton(
            modifier = Modifier
                .padding(top = 8.dp)
                .fillMaxWidth(),
            iconRes = null,
            text = stringResource(R.string.bt_debug_show_local_keys),
            enabled = true,
            isDark = true,
            isSizeConstrained = true,
            onClick = {
                cryptoModel.withInitializedCrypto {
                    val privKey = it.getKeyPairAsStrings()?.private
                    val pubKey = it.getKeyPairAsStrings()?.public
                    AlertDialog.Builder(context)
                        .setMessage("Hash: ${md5(privKey)}\nKey:\n$privKey")
                        .setPositiveButton(android.R.string.ok) { di, _ ->
                            di.dismiss()
                            AlertDialog.Builder(context)
                                .setMessage("Hash: ${md5(pubKey)}\nKey:\n$pubKey")
                                .setPositiveButton(android.R.string.ok, null)
                                .show()
                        }
                        .show()
                }
            })
        TiligButton(
            modifier = Modifier
                .padding(top = 8.dp)
                .fillMaxWidth(),
            iconRes = null,
            text = stringResource(R.string.bt_debug_show_remote_legacy_keys),
            enabled = true,
            isDark = true,
            isSizeConstrained = true,
            onClick = {
                getFromFirestore(
                    viewModel = authViewModel,
                    prefs = prefs,
                    context = context,
                    cryptoModel = cryptoModel,
                    overwriteLocal = false
                )
            })
        TiligButton(
            modifier = Modifier
                .padding(top = 8.dp)
                .fillMaxWidth(),
            iconRes = null,
            text = stringResource(R.string.bt_debug_fetch_remote_keys),
            enabled = true,
            isDark = true,
            isSizeConstrained = true,
            onClick = {
                getFromFirestore(
                    viewModel = authViewModel,
                    prefs = prefs,
                    context = context,
                    cryptoModel = cryptoModel,
                    overwriteLocal = true
                )
            })
        TiligButton(
            modifier = Modifier
                .padding(top = 8.dp)
                .fillMaxWidth(),
            iconRes = null,
            text = stringResource(R.string.bt_debug_test_keys),
            enabled = true,
            isDark = true,
            isSizeConstrained = true,
            onClick = {
                testKeys(
                    cryptoModel = cryptoModel,
                    jsonConverter = settingsViewModel.getJsonConverter(),
                    context = context
                )
            })

        val simulateErrorResponses =
            remember { mutableStateOf(prefs.devToggleSimulateResponses) }
        val simulateErrorResponseItemsList =
            remember { mutableStateOf(prefs.devToggleSimulateErrorResponseItemsList) }
        val simulateErrorResponseItemDetails =
            remember { mutableStateOf(prefs.devToggleSimulateErrorResponseItemDetails) }

        Row(
            modifier = Modifier
                .padding(top = 8.dp)
                .fillMaxWidth()
        ) {
            TiligCheckBox(
                modifier = Modifier,
                checked = simulateErrorResponses.value,
                onCheckedChange = {
                    simulateErrorResponses.value = it
                    prefs.devToggleSimulateResponses = it
                    AlertDialog.Builder(context)
                        .setMessage(R.string.debug_tool_restart_app_message)
                        .setPositiveButton(android.R.string.ok, null)
                        .show()
                })
            Text(
                modifier = Modifier.padding(start = 4.dp),
                text = stringResource(id = R.string.debug_tool_simulate_error_responses))
        }
        Row(
            modifier = Modifier
                .padding(top = 8.dp, start = 16.dp)
                .fillMaxWidth()
        ) {
            TiligCheckBox(
                modifier = Modifier,
                checked = simulateErrorResponseItemsList.value,
                enabled = simulateErrorResponses.value,
                onCheckedChange = {
                    simulateErrorResponseItemsList.value = it
                    prefs.devToggleSimulateErrorResponseItemsList = it
                    AlertDialog.Builder(context)
                        .setMessage(R.string.debug_tool_restart_app_message)
                        .setPositiveButton(android.R.string.ok, null)
                        .show()
                })
            Text(
                modifier = Modifier.padding(start = 4.dp),
                text = stringResource(id = R.string.debug_tool_simulate_error_all_items_responses),
                color = if (simulateErrorResponses.value) DarkBlue else DarkGrey
            )
        }
        Row(
            modifier = Modifier
                .padding(top = 8.dp, start = 16.dp)
                .fillMaxWidth()
        ) {
            TiligCheckBox(
                modifier = Modifier,
                checked = simulateErrorResponseItemDetails.value,
                enabled = simulateErrorResponses.value,
                onCheckedChange = {
                    simulateErrorResponseItemDetails.value = it
                    prefs.devToggleSimulateErrorResponseItemDetails = it
                    AlertDialog.Builder(context)
                        .setMessage(R.string.debug_tool_restart_app_message)
                        .setPositiveButton(android.R.string.ok, null)
                        .show()
                })
            Text(
                modifier = Modifier.padding(start = 4.dp),
                text = stringResource(id = R.string.debug_tool_simulate_error_all_item_details),
                color = if (simulateErrorResponses.value) DarkBlue else DarkGrey
            )
        }
    }
}

/**
 * Makes sure that we're really, really in a dev or staging environment and not accidentally
 * deleting/changing anything in production.
 */
private fun extraExtraSafetyCheck() =
    BuildConfig.DEBUG
            && (BuildConfig.BASE_URL == "https://api-staging.tilig.com/api/"
            || BuildConfig.BASE_URL.contains("-review-"))

private fun toastLog(context: Context, message: String, throwable: Throwable? = null) {
    if (throwable == null) {
        Log.i("Settings", message)
    } else {
        Log.e("Settings", message, throwable)
    }
    Toast.makeText(
        context,
        message,
        Toast.LENGTH_LONG
    ).show()
}

private fun storeInFirestore(
    viewModel: AuthViewModel,
    key: Crypto,
    prefs: SharedPrefs,
    context: Context
) {
    if (extraExtraSafetyCheck()) {
        Log.w(
            "Settings",
            "Deleting user ${prefs.firebaseUserId} from Firestore. Backend ID: ${prefs.mixpanelUserId}"
        )
        val db = FirebaseFirestore.getInstance()
        db
            .collection(SignInViewModel.FIRESTORE_COLLECTION)
            .document(prefs.firebaseUserId!!)
            .set(
                hashMapOf(
                    "uid" to prefs.firebaseUserId,
                    "privateKey" to key.legacyPrivateKeyAsString()
                )
            )
            .addOnSuccessListener {
                toastLog(context, "1: Private key stored in Firebase")
                storeInTiligServer(
                    key,
                    viewModel,
                    context
                )
            }
            .addOnFailureListener {
                toastLog(context, "ERROR key NOT stored in Firebase", it)
            }
    } else {
        toastLog(context, "For your safety I refuse to delete your account. Contact a developer.")
    }
}

private fun testKeys(
    cryptoModel: CryptoModel,
    jsonConverter: Moshi,
    context: Context
) {
    cryptoModel.withInitializedCrypto { crypto ->
        val account = Account.createEmpty().apply {
            name = "Test Account"
            website = "tilig.com"
            androidAppId = "com.tilig.android.debug"
            password = "My Secret Password"
            username = "Sid"
            notes = "Some notes to ponder about"
        }
        val encryptedAccount = account.encrypt(jsonConverter, crypto)
        val decryptedAccount =
            encryptedAccount.decrypt(jsonConverter, crypto, includingDetails = true) as Account

        if (account.name == decryptedAccount.name
            && account.website == decryptedAccount.website
            && account.androidAppId == decryptedAccount.androidAppId
            && account.password == decryptedAccount.password
            && account.username == decryptedAccount.username
            && account.notes == decryptedAccount.notes
        ) {
            AlertDialog.Builder(context)
                .setMessage("Test OK: \n${decryptedAccount.toDebugString()}")
                .setPositiveButton(android.R.string.ok, null)
                .show()
        } else {
            AlertDialog.Builder(context)
                .setMessage("Test FAILED:\n${decryptedAccount.toDebugString()}\nShould be:\n${account.toDebugString()}")
                .setPositiveButton(android.R.string.ok, null)
                .show()
        }
    }
}

private fun getFromFirestore(
    viewModel: AuthViewModel,
    prefs: SharedPrefs,
    context: Context,
    cryptoModel: CryptoModel,
    overwriteLocal: Boolean
) {
    val db = FirebaseFirestore.getInstance()
    db
        .collection(SignInViewModel.FIRESTORE_COLLECTION)
        .document(prefs.firebaseUserId!!)
        .get()
        .addOnSuccessListener { document ->
            val key = if (document?.data != null) {
                Log.d("settings", "DocumentSnapshot data: ${document.data}")
                document.data?.get("privateKey") as? String
            } else {
                Log.d("settings", "No such document")
                "null"
            }
            if (overwriteLocal) {
                AlertDialog.Builder(context)
                    .setMessage("Hash: ${md5(key)}\nKey:\n$key")
                    .setNegativeButton(android.R.string.cancel, null)
                    .setPositiveButton("Overwrite local") { di, _ ->
                        di.dismiss()
                        cryptoModel.updateLegacyPrivateKey(key) {
                            getFromTiligServer(
                                viewModel,
                                context,
                                cryptoModel,
                                overwriteLocal = true
                            )
                        }
                    }
                    .show()
            } else {
                AlertDialog.Builder(context)
                    .setMessage("Hash: ${md5(key)}\nKey:\n$key")
                    .setPositiveButton(android.R.string.ok) { di, _ ->
                        di.dismiss()
                        getFromTiligServer(viewModel, context, cryptoModel, overwriteLocal = false)
                    }
                    .show()
            }
        }
        .addOnFailureListener { exception ->
            Log.d("settings", "get failed with ", exception)
            AlertDialog.Builder(context)
                .setMessage("Error")
                .setPositiveButton(android.R.string.ok) { di, _ ->
                    di.dismiss()
                    getFromTiligServer(viewModel, context, cryptoModel, overwriteLocal = false)
                }
                .show()
        }
}

private fun getFromTiligServer(
    viewModel: AuthViewModel,
    context: Context,
    cryptoModel: CryptoModel,
    overwriteLocal: Boolean
) {
    viewModel.fetchFromTiligServer().enqueue(object : Callback<Profile> {
        override fun onFailure(call: Call<Profile>, t: Throwable) {
            toastLog(context, "Unable to fetch key from Tilig server", t)
        }

        override fun onResponse(
            call: Call<Profile>,
            response: Response<Profile>
        ) {
            val key = response.body()?.public_key ?: "null"
            if (overwriteLocal) {
                AlertDialog.Builder(context)
                    .setMessage("Hash: ${md5(key)}\nKey:\n$key")
                    .setNegativeButton(android.R.string.cancel, null)
                    .setPositiveButton("Overwrite local") { di, _ ->
                        // TODO also fetch the new keys?
                        cryptoModel.updateLegacyPublicKey(key) {
                            di.dismiss()
                        }
                    }
                    .show()
            } else {
                AlertDialog.Builder(context)
                    .setMessage("Hash: ${md5(key)}\nKey:\n$key")
                    .setPositiveButton(android.R.string.ok, null)
                    .show()
            }
        }
    })
}

private fun storeInTiligServer(
    key: Crypto,
    viewModel: AuthViewModel,
    context: Context
) {
    viewModel.storeInTiligServer(
        key.legacyPublicKeyAsString()!!
    ).enqueue(object : Callback<OkResponse> {
        override fun onFailure(call: Call<OkResponse>, t: Throwable) {
            toastLog(context, "Unable to store key in Tilig server", t)
        }

        override fun onResponse(
            call: Call<OkResponse>,
            response: Response<OkResponse>
        ) {
            toastLog(context, "2: Public key stored in backend")

            AlertDialog.Builder(context)
                .setMessage(R.string.debug_dialog_reset_keys_completed)
                .setPositiveButton(android.R.string.ok, null)
                .show()
        }
    })
}

suspend fun deleteAllAccounts(viewModel: SettingsViewModel) {
    viewModel.deleteAllAccounts()
}

fun createDummyItems(
    viewModel: SettingsViewModel,
    cryptoModel: CryptoModel,
    types: List<SecretItemType>
) {
    cryptoModel.withInitializedCrypto { crypto ->
        viewModel.createDummyItems(crypto, types)
    }
}

fun md5(input: String?): String {
    if (input == null) {
        return "null"
    }
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(input.toByteArray())).toString(16).padStart(32, '0')
}
