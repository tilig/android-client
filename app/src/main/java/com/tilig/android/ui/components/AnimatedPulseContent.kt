package com.tilig.android.ui.components

import androidx.compose.animation.core.*
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.tilig.android.ui.secrets.components.TitledContent
import com.tilig.android.ui.theme.DefaultPadding
import com.tilig.android.ui.theme.Grey
import com.tilig.android.ui.theme.MediumCornerRadius
import com.tilig.android.ui.theme.White

// Used for loading screen
@Composable
fun AnimatedPulseContent(
    content: @Composable (Color) -> Unit,
) {
    val transition = rememberInfiniteTransition()
    val translateAnim = transition.animateFloat(
        initialValue = 0.5f,
        targetValue = 1f,
        animationSpec = infiniteRepeatable(
            animation = tween(
                durationMillis = 500,
                easing = FastOutSlowInEasing
            ),
            repeatMode = RepeatMode.Reverse
        )
    )

    content(Grey.copy(alpha = translateAnim.value))
}

@Composable
fun AnimatedItem(modifier: Modifier = Modifier, color: Color) {
    TitledContent(
        modifier = modifier,
        loadingColor = color,
        title = "",
    ) {
        Spacer(
            modifier = Modifier
                .height(14.dp)
                .width(124.dp)
                .background(color = color, shape = RoundedCornerShape(4.dp))
        )
    }
}

@Composable
fun AnimatedPulseField(modifier: Modifier = Modifier){
    AnimatedPulseContent {
        Box(
            modifier = modifier.background(
                color = White,
                shape = RoundedCornerShape(MediumCornerRadius)
            )
        ) {
            AnimatedItem(
                color = it,
                modifier = Modifier.padding(DefaultPadding)
            )
        }
    }
}