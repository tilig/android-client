package com.tilig.android.ui.twofa

import android.util.Log
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.ProgressIndicatorDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.tilig.android.ui.theme.*
import com.tilig.android.ui.twofa.bottomsheets.securitycode.SecurityCodeViewModel

@Composable
fun SecurityCodeComponent(
    modifier: Modifier,
    token: String,
    isSmallView: Boolean = false,
    onNewCodeGenerated: (code: String?) -> Unit = {},
    // multiple instances for different tokens
    viewModel: SecurityCodeViewModel = viewModel(key = token)
) {
    viewModel.setToken(token)
    val code by viewModel.code.collectAsState()
    onNewCodeGenerated(code)

    if (isSmallView) {
        SecurityCodeComponentSmall(modifier, code, viewModel)
    } else {
        Row(
            modifier = modifier
                .background(White, RoundedCornerShape(8.dp)),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = code ?: "--- ---", style = TextStyle(
                    fontFamily = Space,
                    fontSize = 36.sp,
                    color = DarkBlue
                ), modifier = Modifier
                    .weight(1f)
                    .padding(horizontal = 16.dp)
            )
            if (code != null) {
                CountDownView(
                    modifier = Modifier.padding(end = 16.dp),
                    viewModel = viewModel,
                    innerTextStyle = TextStyle(
                        color = TiligGreen,
                        fontSize = 14.sp,
                        fontFamily = Stolzl,
                        fontWeight = FontWeight.Medium
                    )
                )
            }
        }
    }
}

@Composable
private fun SecurityCodeComponentSmall(
    modifier: Modifier,
    codeValue: String?,
    viewModel: SecurityCodeViewModel = viewModel()
) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = codeValue ?: "--- ---",
            style = StyleRobotoMonoMediumDarkBlue16.copy(letterSpacing = 1.25.sp),
            modifier = Modifier.weight(1f)
        )
        if (codeValue != null) {
            CountDownView(
                viewModel = viewModel, innerTextStyle = TextStyle(
                    color = TiligGreen,
                    fontSize = 10.sp,
                    fontFamily = Stolzl,
                    fontWeight = FontWeight.Medium
                ), countDownIndicatorSize = 24.dp
            )
        }
    }
}

@Composable
fun CountDownView(
    modifier: Modifier = Modifier,
    viewModel: SecurityCodeViewModel,
    innerTextStyle: TextStyle,
    countDownIndicatorSize: Dp = 32.dp
) {

    val time by viewModel.time.collectAsState()
    val progress by viewModel.progress.collectAsState()
    progress?.let {
        CountDownView(
            modifier = modifier,
            time = time,
            progress = it,
            innerTextStyle = innerTextStyle,
            countDownIndicatorSize = countDownIndicatorSize
        )
    }
}

@Composable
fun CountDownView(
    modifier: Modifier = Modifier,
    innerTextStyle: TextStyle,
    time: String,
    progress: Float,
    countDownIndicatorSize: Dp
) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        CountDownIndicator(
            Modifier,
            progress = progress,
            time = time,
            size = countDownIndicatorSize,
            stroke = 2.dp,
            innerTextStyle = innerTextStyle
        )
    }
}

@Composable
fun CountDownIndicator(
    modifier: Modifier = Modifier,
    innerTextStyle: TextStyle,
    progress: Float,
    time: String,
    size: Dp,
    stroke: Dp
) {

    val animatedProgress by animateFloatAsState(
        targetValue = progress,
        animationSpec = ProgressIndicatorDefaults.ProgressAnimationSpec,
    )

    Column(modifier = modifier) {
        Box {

            CircularProgressIndicatorBackGround(
                modifier = Modifier
                    .height(size)
                    .width(size),
                color = Grey,
                stroke
            )

            CircularProgressIndicator(
                progress = animatedProgress,
                modifier = Modifier
                    .height(size)
                    .width(size),
                color = TiligGreen,
                strokeWidth = stroke,
            )

            Text(
                modifier = Modifier.align(Alignment.Center),
                text = time,
                style = innerTextStyle
            )
        }
    }
}

@Composable
private fun CircularProgressIndicatorBackGround(
    modifier: Modifier = Modifier,
    color: Color,
    stroke: Dp
) {
    val style = with(LocalDensity.current) { Stroke(stroke.toPx()) }

    Canvas(modifier = modifier, onDraw = {

        val innerRadius = (size.minDimension - style.width) / 2

        drawArc(
            color = color,
            startAngle = 0f,
            sweepAngle = 360f,
            topLeft = Offset(
                (size / 2.0f).width - innerRadius,
                (size / 2.0f).height - innerRadius
            ),
            size = Size(innerRadius * 2, innerRadius * 2),
            useCenter = false,
            style = style
        )
    })
}