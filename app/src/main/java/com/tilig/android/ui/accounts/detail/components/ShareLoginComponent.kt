package com.tilig.android.ui.accounts.detail.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tilig.android.R
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.models.tilig.Membership
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.components.BorderedBasicTextField
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.theme.*

@Composable
fun ShareLoginComponent(
    modifier: Modifier = Modifier,
    account: Account,
    isActionEnabled: Boolean,
    onShare: (String) -> Unit,
    onRevokeShare: (String) -> Unit
) {
    Box(
        modifier = modifier
            .fillMaxWidth()
            .background(
                color = White,
                shape = RoundedCornerShape(MediumCornerRadius)
            )
    ) {
        Column(modifier = Modifier.padding(DefaultPadding)) {
            ShareLoginView(
                onShare = onShare,
                isActionEnabled = isActionEnabled
            )
            SharedWithListView(
                account = account,
                onRevokeShare = onRevokeShare
            )
        }
    }
}

@Composable
private fun ShareLoginView(
    onShare: (String) -> Unit,
    isActionEnabled: Boolean,
) {
    Row(modifier = Modifier.fillMaxWidth()) {
        Column(modifier = Modifier.weight(1f)) {
            Text(
                stringResource(id = R.string.share_this_login),
                style = StyleSpoofMediumDarkBlue20,
            )
            Text(
                modifier = Modifier,
                text = stringResource(id = R.string.share_this_login),
                style = StyleStolzRegularDarkBlue12,
                color = DimmedBlue75
            )
        }
    }

    val state = remember { TextFieldState("") }
    Column(modifier = Modifier.fillMaxWidth()) {
        BorderedBasicTextField(modifier = Modifier,
            textStyle = StyleStolzRegularDarkBlue16,
            hint = stringResource(id = R.string.label_login_email),
            state = state,
            singleLine = false,
            textModifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 12.dp, vertical = 4.dp),
            keyboardType = KeyboardType.Email,
            onValueChanged = {},
            onNext = {
                onShare(state.text)
            }
        )
        TiligButton(
            modifier = Modifier
                .padding(top = HalfPadding)
                .height(56.dp),
            iconRes = R.drawable.ic_share_link,
            text = stringResource(id = R.string.bt_share),
            enabled = isActionEnabled,
            isSizeConstrained = false,
            isDark = true,
            onClick = {
                onShare(state.text)
            },
        )
    }
}

@Composable
fun SharedWithListView(account: Account, onRevokeShare: (String) -> Unit) {
    Column(
        Modifier.wrapContentSize(unbounded = true)
    ) {
        account.folder?.getAllMemberships()?.forEach {
            SharedWithRow(
                it,
                onRevokeShare
            )
        }
    }
}

@Composable
fun SharedWithRow(membership: Membership, onRevokeShare: (String) -> Unit) {
    Row(modifier = Modifier.fillMaxWidth()) {
        Text(
            text = membership.user.email.lowercase(),
            style = StyleSpoofMediumDarkBlue20,
        )
        if (membership.isPending == true) {
            Text(
                modifier = Modifier,
                text = stringResource(id = R.string.share_is_pending),
                style = StyleStolzRegularDarkBlue12,
                color = DimmedLightBlueButNotTransparent
            )
        }
        TextButton(modifier = Modifier
            .height(36.dp)
            .padding(horizontal = 8.dp)
            .align(Alignment.CenterVertically),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = White,
                contentColor = White,
                disabledBackgroundColor = White,
                disabledContentColor = White
            ),
            elevation = ButtonDefaults.elevation(),
            shape = RoundedCornerShape(8.dp),
            onClick = {
                onRevokeShare(membership.id!!)
            }) {
            Text(
                modifier = Modifier.align(Alignment.CenterVertically),
                text = stringResource(id = R.string.revoke_share),
                style = StyleStolzRegularDarkBlue14.copy(
                    color = LightBlue,
                    lineHeight = 24.sp
                )
            )
        }
    }
}
