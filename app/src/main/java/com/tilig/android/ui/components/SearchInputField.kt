package com.tilig.android.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.theme.DimmedBlue
import com.tilig.android.ui.theme.StyleStolzBookDarkBlue16

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun SearchInputField(
    modifier: Modifier = Modifier,
    hint: String,
    input: String,
    onDone: (() -> Unit)? = null,
    onValueChange: ((String) -> Unit)? = null
) {
    val keyboardManager = LocalSoftwareKeyboardController.current

    BasicTextField(
        value = input,
        onValueChange = {
            onValueChange?.invoke(it)
        },
        modifier = modifier
            .padding(start = 12.dp),
        decorationBox = { innerTextField ->
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_search),
                    contentDescription = "",
                    modifier = Modifier
                        .size(24.dp)
                )
                Box(modifier = Modifier.weight(1f)) {
                    if (input.isEmpty()) {
                        Text(hint, style = StyleStolzBookDarkBlue16.copy(color = DimmedBlue))
                    }
                    innerTextField()
                }
                if (input.isNotEmpty()) {
                    IconButton(onClick = {
                        onValueChange?.invoke("")
                    }) {
                        Icon(
                            Icons.Default.Close,
                            contentDescription = "",
                            modifier = Modifier
                                .size(24.dp)
                        )
                    }
                }
            }

        },
        textStyle = StyleStolzBookDarkBlue16,
        singleLine = true,
        keyboardOptions = KeyboardOptions(
            imeAction = ImeAction.Done,
            autoCorrect = false,
            keyboardType = KeyboardType.Uri
        ),
        keyboardActions = KeyboardActions(
            onDone = {
                keyboardManager?.hide()
                onDone?.invoke()
            }
        )
    )
}

@Composable
@Preview
fun BrandSearchInputFieldPreview() {
    SearchInputField(
        hint = "hint",
        input = "text"
    )
}