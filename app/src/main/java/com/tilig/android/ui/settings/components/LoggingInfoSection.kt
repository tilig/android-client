package com.tilig.android.ui.settings.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.tilig.android.R
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.livedata.ForcedLogoutLiveData
import com.tilig.android.data.models.ApiSuccessResponse
import com.tilig.android.ui.components.FieldTitle
import com.tilig.android.ui.settings.SettingsViewModel
import com.tilig.android.ui.theme.*
import org.koin.androidx.compose.get

@Preview
@Composable
fun LoggingInfoSection(
    authStatus: ForcedLogoutLiveData = get(),
    viewModel: SettingsViewModel = viewModel(),
    tracker: Mixpanel = get(),
) {
    val profileInfo = when (val profile = viewModel.profile) {
        is ApiSuccessResponse -> profile.body
        else -> null
    }

    FieldTitle(
        title = stringResource(id = R.string.label_account),
        titlePadding = 8.dp,
        style = StyleSpoofMediumDarkBlue20
    )
    Row(
        modifier = Modifier
            .background(color = White, shape = RoundedCornerShape(MediumCornerRadius))
            .fillMaxWidth()
            .padding(16.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Column(
            modifier = Modifier
                .padding(end = 16.dp)
                .weight(1f)
        ) {
            val email = profileInfo?.email ?: ""
            val name = profileInfo?.getFullName() ?: ""
            if (name.isNotEmpty()) {
                Text(
                    text = name,
                    style = StyleStolzRegularDarkBlue14,
                    color = DefaultTextColor,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
            }
            Text(
                modifier = Modifier.alpha(0.5f),
                text = email,
                style = StyleStolzBookDarkBlue12,
                color = DefaultTextColor,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        }

        TextButton(modifier = Modifier
            .height(32.dp)
            .padding(horizontal = 0.dp),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = White,
                contentColor = White,
                disabledBackgroundColor = White,
                disabledContentColor = White
            ),
            contentPadding = PaddingValues(vertical = 0.dp, horizontal = 8.dp),
            elevation = ButtonDefaults.elevation(),
            onClick = {
                tracker.trackEvent(Tracker.EVENT_SIGN_OUT)
                authStatus.forceLogout(true)
            },
            enabled = profileInfo != null) {
            Text(
                modifier = Modifier.align(Alignment.CenterVertically),
                text = stringResource(id = R.string.menu_log_out),
                style = StyleStolzRegularDarkBlue14.copy(
                    color = LightBlue,
                    lineHeight = 24.sp,
                ),
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}
