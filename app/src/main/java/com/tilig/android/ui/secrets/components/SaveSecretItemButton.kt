package com.tilig.android.ui.secrets.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tilig.android.ui.modifiers.dropShadow
import com.tilig.android.ui.theme.*

@Composable
fun SaveSecretItemButton(
    modifier: Modifier,
    enabled: Boolean = true,
    text: String,
    onSave: () -> Unit
) {
    TextButton(
        enabled = enabled,
        modifier = modifier
            .fillMaxWidth()
            .height(48.dp)
            .dropShadow(enabled = enabled),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = LightBlue,
            disabledBackgroundColor = DimmedLightBlue,
        ),
        shape = RoundedCornerShape(8.dp),
        onClick = { onSave() },
    ) {
        Text(
            text = text,
            style = StyleStolzRegularDarkBlue14.copy(color = White)
        )
    }
}

@Preview
@Composable
private fun SaveSecretItemButtonPreview() {
    SaveSecretItemButton(modifier = Modifier, enabled = true, onSave = {}, text = "Save")
}