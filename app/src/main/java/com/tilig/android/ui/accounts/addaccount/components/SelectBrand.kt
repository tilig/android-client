package com.tilig.android.ui.accounts.addaccount.components

import androidx.compose.runtime.Composable
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.ui.accounts.addaccount.AddAccountUiState

@Composable
fun SelectBrand(
    tracker: Mixpanel,
    uiState: AddAccountUiState.SelectBrand,
    onAccountPrefill: (Account, Boolean) -> Unit,
    onSearch: (String?) -> Unit,
) {
    BrandSearch(
        searchResults = uiState.autocompleteBrands,
        onSearchTextChanged = {
            onSearch(it)
        },
        isLoading = uiState.isLoading,
        searchQuery = uiState.currentSearchQuery,
        onAccountPrefill = {
            onAccountPrefill(it, false)
        })
    if (!uiState.isLoading) {
        SuggestedBrands(
            tracker = tracker,
            onAccountPrefill = {
                onAccountPrefill(it, true)
            })
    }
}