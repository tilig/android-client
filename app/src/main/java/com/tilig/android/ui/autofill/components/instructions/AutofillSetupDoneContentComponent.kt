package com.tilig.android.ui.autofill.components.instructions

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.tilig.android.R
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.theme.*

@Composable
fun AutofillSetupDoneContentComponent(onContinue: () -> Unit) {
    ConstraintLayout(
        modifier = Modifier.fillMaxSize()
    ) {
        val (title, image, btnContinue) = createRefs()
        TiligButton(
            modifier = Modifier
                .constrainAs(btnContinue) {
                    end.linkTo(parent.end)
                    start.linkTo(parent.start)
                    bottom.linkTo(parent.bottom)
                },
            iconRes = null,
            text = stringResource(R.string.onboarding_autofill_done_bt),
            enabled = true,
            isDark = true,
            isSizeConstrained = true,
            horizontalPadding = DefaultPadding,
            onClick = onContinue)
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .padding(
                    start = DoublePadding,
                    end = DoublePadding,
                    top = 68.dp,
                    bottom = 0.dp
                )
                .constrainAs(title) {
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    top.linkTo(parent.top)
                }) {

            Text(
                text = stringResource(id = R.string.onboarding_autofill_done_1),
                style = StyleSpoofBoldDarkBlue28.copy(fontSize = 24.sp, color = TiligGreen)
            )

            Text(
                modifier = Modifier.padding(top = HalfPadding),
                text = stringResource(id = R.string.onboarding_autofill_done_2),
                style = StyleSpoofBoldDarkBlue28.copy(fontSize = 24.sp)
            )

            Text(
                modifier = Modifier.padding(top = HalfPadding),
                text = stringResource(id = R.string.onboarding_autofill_done_3),
                style = StyleStolzRegularDarkBlue16.copy(color = Neutral40),
                textAlign = TextAlign.Center
            )
        }
        Box(modifier = Modifier
            .constrainAs(image) {
                start.linkTo(parent.start)
                end.linkTo(parent.end)
                top.linkTo(title.bottom)
                bottom.linkTo(btnContinue.top)
                width = Dimension.fillToConstraints
                height = Dimension.fillToConstraints
            }, contentAlignment = Alignment.BottomCenter){
            Image(
                painter = painterResource(id = R.drawable.img_keyboard_autofill),
                contentDescription = "")
        }
    }
}