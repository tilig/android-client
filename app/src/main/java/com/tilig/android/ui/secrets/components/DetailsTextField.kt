package com.tilig.android.ui.secrets.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.Icon
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.theme.StyleStolzRegularDarkBlue16

@Composable
fun DetailsTextField(
    value: String?,
    textStyle: TextStyle = StyleStolzRegularDarkBlue16,
    enabled: Boolean = false,
    showRevealButton: Boolean = false,
    showContentInitially: Boolean = true,
    transformation: VisualTransformation = PasswordVisualTransformation()
) {

    var contentVisibility by remember { mutableStateOf(showContentInitially) }
    var isContentVisibleInitially by remember { mutableStateOf(showContentInitially) }

    Row(
        modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Box(modifier = Modifier.weight(1f)) {
            value?.let {
                BasicTextField(
                    modifier = Modifier
                        .onFocusChanged { focusState ->
                            if (showRevealButton) {
                                contentVisibility =
                                    focusState.isFocused || isContentVisibleInitially
                                isContentVisibleInitially = false
                            }
                        },
                    value = it, textStyle = textStyle,
                    onValueChange = {
                    },
                    readOnly = true,
                    enabled = enabled,
                    visualTransformation = if (contentVisibility) VisualTransformation.None else transformation,
                )
            }
        }
        if (showRevealButton) {
            val image = if (contentVisibility)
                R.drawable.ic_revealed
            else
                R.drawable.ic_concealed

            val action = {
                contentVisibility = !contentVisibility
            }
            Icon(
                modifier = Modifier
                    .clickable { action.invoke() }
                    .size(24.dp),
                painter = painterResource(id = image),
                contentDescription = stringResource(id = R.string.cd_reveal_password)
            )
        }
    }
}
