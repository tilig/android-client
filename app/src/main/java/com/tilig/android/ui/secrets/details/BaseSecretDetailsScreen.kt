package com.tilig.android.ui.secrets.details

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import com.tilig.android.analytics.Breadcrumb
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.tilig.*
import com.tilig.android.ui.accounts.detail.components.CopySnackbar
import com.tilig.android.ui.accounts.detail.components.PasswordHistoryBottomSheet
import com.tilig.android.ui.accounts.detail.events.AccountActionEvent
import com.tilig.android.ui.components.StatusBarAndNavigationBarContrast
import com.tilig.android.ui.secrets.components.ShareWithBottomSheet
import com.tilig.android.ui.secrets.details.events.ItemDetailsActionEvent
import com.tilig.android.ui.secrets.details.states.ItemDetailsUiState
import com.tilig.android.ui.theme.BackgroundGrey
import com.tilig.android.ui.theme.TiligTheme
import com.tilig.android.ui.theme.White
import com.tilig.android.ui.twofa.SHOW_SECURE_CODE_BOTTOM_SHEET
import com.tilig.android.ui.twofa.bottomsheets.securitycode.SecurityCodeModalBottomSheet
import com.tilig.android.ui.twofa.bottomsheets.tutorial.TwoFATutorialBottomSheet

/**
 * Base compose to implement details screen. Contains main callbacks, observers
 */
@OptIn(ExperimentalMaterialApi::class, ExperimentalComposeUiApi::class)
@Composable
fun BaseSecretDetailsScreen(
    itemViewModel: ItemDetailsViewModel,
    initialItem: SecretItem,
    state: DetailsScreenStateHolder,
    lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current,
    onOpenQrCodeScanner: () -> Unit,
    content: @Composable (ItemDetailsUiState) -> Unit
) {
    val uiState = itemViewModel.uiState.collectAsState()

    // only this type supports 2fa code scanner, listen only for login screen
    if (initialItem is Account){
        val qrCodeScannerScreenResult = state.navigator.currentBackStackEntry
            ?.savedStateHandle
            ?.getLiveData<Boolean>(SHOW_SECURE_CODE_BOTTOM_SHEET)?.observeAsState()

        val item: SecretItem? = (uiState.value as? ItemDetailsUiState.ItemEdit)?.item
        if (qrCodeScannerScreenResult?.value?.and(item?.hasOtp() == true) == true) {
            state.navigator.currentBackStackEntry
                ?.savedStateHandle
                ?.set(SHOW_SECURE_CODE_BOTTOM_SHEET, false)
            state.updateBottomSheet(BottomSheetType.SecureCode(account = item as Account))
        }
    }


    // log the events only once per account view/ creation at the onCreate
    // also save the changes only onPause
    DisposableEffect(lifecycleOwner) {
        val eventObserver = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_CREATE) {
                logEvents(state.tracker, secretItem = initialItem)
                itemViewModel.getItemInitially(initialItem)
            }
        }

        val eventPauseObserver = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_PAUSE) {
                state.keyboardController?.hide()
            }
        }
        lifecycleOwner.lifecycle.addObserver(eventPauseObserver)
        lifecycleOwner.lifecycle.addObserver(eventObserver)

        onDispose {
            lifecycleOwner.lifecycle.removeObserver(eventObserver)
            lifecycleOwner.lifecycle.removeObserver(eventPauseObserver)
        }
    }

    val context = LocalContext.current

    val leaveScreen by itemViewModel.leaveScreen.collectAsState(false)
    LaunchedEffect(leaveScreen) {
        if (leaveScreen) {
            state.navigateUp()
        }
    }

    LaunchedEffect(uiState.value.errorMessage) {
        uiState.value.errorMessage?.let {
            if (it.msgResource != null) {
                Toast.makeText(
                    context,
                    it.msgResource,
                    Toast.LENGTH_SHORT,
                ).show()
            } else if (it.message != null) {
                Toast.makeText(
                    context,
                    it.message,
                    Toast.LENGTH_SHORT,
                ).show()
            }
        }
    }

    TiligTheme {
        ModalBottomSheetLayout(
            modifier = Modifier.fillMaxSize(),
            sheetContent = {
                BottomSheetContent(
                    bottomSheetType = state.currentBottomSheet.value,
                    closeSheet = { state.updateBottomSheet(null) },
                    tracker = state.tracker,
                    onOpenQrCodeScanner = onOpenQrCodeScanner,
                    onShare = {
                        itemViewModel.onActionEvent(ItemDetailsActionEvent.Share(it))
                    },
                    onRestorePassword = {
                        itemViewModel.onActionEvent(AccountActionEvent.RestorePassword(it))
                        state.showSnackBarMessage(it)
                    }
                )
            },

            sheetState = state.modalBottomSheetState,
            sheetShape = RoundedCornerShape(topStart = 16.dp, topEnd = 16.dp),
            sheetBackgroundColor = White,
        ) {
            Scaffold(
                scaffoldState = state.scaffoldState,
                snackbarHost = {
                    SnackbarHost(it) { data ->
                        Snackbar(
                            snackbarData = data
                        )
                    }
                },
                modifier = Modifier.fillMaxSize()
            ) { padding ->
                Column(
                    Modifier
                        .fillMaxSize()
                        .padding(padding)
                        .background(color = BackgroundGrey)
                        .navigationBarsPadding()
                        .imePadding()
                ) {
                    StatusBarAndNavigationBarContrast(
                        statusBarBright = false,
                        navigationBarBright = true,
                        navigationBarScrollBehind = true
                    )

                    Box(modifier = Modifier.weight(1f)) {
                        // Extracting a separate val here, because it appears (according to crash logs)
                        // that the value can change while executing below code. E.g. the AccountSuccess
                        // case fired, but when casting the value to AccountSuccess, it had already
                        // changed to AccountError, crashing the cast
                        content(uiState.value)

                        SnackbarHost(
                            hostState = state.snackState,
                            Modifier
                                .wrapContentWidth()
                                .align(Alignment.TopCenter)
                        ) {
                            CopySnackbar(snackbarData = it)
                        }
                    }
                }
            }
        }
    }

    if (state.modalBottomSheetState.currentValue != ModalBottomSheetValue.Hidden) {
        DisposableEffect(Unit) {
            onDispose {
                state.updateBottomSheet(null)
            }
        }
    }
}

private fun logEvents(
    tracker: Mixpanel,
    secretItem: SecretItem,
) {
    when (secretItem) {
        is Account -> tracker.trackEvent(Tracker.EVENT_VIEW_ACCOUNT)
        is Note -> tracker.trackEvent(Tracker.EVENT_VIEW_NOTE)
        is CreditCard -> tracker.trackEvent(Tracker.EVENT_VIEW_CREDIT_CARD)
        is WifiPassword -> tracker.trackEvent(Tracker.EVENT_VIEW_WIFI)
        else -> {}
    }
    Breadcrumb.drop(Breadcrumb.Detail)
}

@Composable
private fun BottomSheetContent(
    bottomSheetType: BottomSheetType?,
    tracker: Tracker,
    closeSheet: () -> Unit,
    onOpenQrCodeScanner: () -> Unit,
    onRestorePassword: (String) -> Unit,
    onShare: (String) -> Unit
) {
    Box(Modifier.defaultMinSize(minHeight = 1.dp)) {
        bottomSheetType?.let { type ->
            when (type) {
                is BottomSheetType.Tutorial -> {
                    TwoFATutorialBottomSheet(
                        closeSheet = closeSheet,
                        goToScan = onOpenQrCodeScanner
                    )
                }
                is BottomSheetType.SecureCode -> {
                    SecurityCodeModalBottomSheet(account = type.account, closeSheet = closeSheet)
                }
                is BottomSheetType.PasswordHistory -> {
                    PasswordHistoryBottomSheet(
                        tracker = tracker,
                        restorePassword = {
                            onRestorePassword.invoke(it)
                            closeSheet.invoke()
                        },
                        history = type.history,
                        closeSheet = closeSheet
                    )
                }
                is BottomSheetType.ShareWith -> {
                    ShareWithBottomSheet(
                        closeSheet = closeSheet,
                        onShare = {
                            onShare.invoke(it)
                            closeSheet.invoke()
                        },
                        folder = type.secretItem.folder
                    )
                }
            }
        }
    }
}

sealed class BottomSheetType {
    object Tutorial : BottomSheetType()
    class PasswordHistory(val history: History) : BottomSheetType()
    class SecureCode(val account: Account) : BottomSheetType()
    class ShareWith(val secretItem: SecretItem) : BottomSheetType()
}

// Swiping States
enum class SwipingStates {
    EXPANDED,
    COLLAPSED
}