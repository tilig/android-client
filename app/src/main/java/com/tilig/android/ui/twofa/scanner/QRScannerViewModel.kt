package com.tilig.android.ui.twofa.scanner

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.squareup.moshi.Moshi
import com.tilig.android.R
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.*
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.models.tilig.EncryptedItem
import com.tilig.android.data.models.tilig.EncryptedItemResponse
import com.tilig.android.data.qrscanner.MalformedTokenException
import com.tilig.android.data.qrscanner.Token
import com.tilig.android.data.repository.Repository
import com.tilig.android.ui.CryptoModel
import com.tilig.crypto.Crypto
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

data class ScanMessage(
    val website: String,
    val pattern: Int = R.string.scan_qr_code_on
)

sealed interface QRCodeScannerUiState {
    val account: Account?

    object Empty : QRCodeScannerUiState {
        override val account: Account? = null
    }

    data class ErrorFetchAccount(
        override val account: Account? = null,
        val errorMessage: UiError
    ) : QRCodeScannerUiState

    data class Idle(
        override val account: Account,
        val scanMessage: ScanMessage
    ) : QRCodeScannerUiState

    data class SuccessScan(
        val token: String,
        override val account: Account
    ) : QRCodeScannerUiState

    data class WrongQRCode(
        override val account: Account
    ) : QRCodeScannerUiState
}

data class QRScannerState(
    val response: ApiResponse<EncryptedItemResponse>? = null,
    val token: String? = null,
    val errorMessage: String? = null,
    val incorrectQRCode: Boolean = false
) {

    /**
     * Converts this [QRScannerState] into a more strongly typed [QRCodeScannerUiState] for driving
     * the ui.
     */
    fun toUiState(jsonConverter: Moshi, crypto: Crypto): QRCodeScannerUiState {
        if (response == null) {
            return QRCodeScannerUiState.Empty
        } else {
            when (response) {
                is ApiEmptyResponse,
                is ApiErrorResponse -> return QRCodeScannerUiState.ErrorFetchAccount(
                    errorMessage = UiError(
                        msgResource = R.string.error_loading_account
                    )
                )
                is ApiSuccessResponse -> {
                    val account = response.body.item.decrypt(
                        jsonConverter,
                        crypto,
                        includingDetails = true
                    ) as Account
                    return when {
                        token == null && !incorrectQRCode ->
                            QRCodeScannerUiState.Idle(
                                account = account,
                                scanMessage = ScanMessage(
                                    account.domain ?: account.name
                                    ?: "Untitled",
                                    R.string.scan_qr_code_on
                                )
                            )
                        incorrectQRCode -> QRCodeScannerUiState.WrongQRCode(account)
                        else -> QRCodeScannerUiState.SuccessScan(
                            token = token!!,
                            account = account
                        )
                    }
                }
            }
        }
    }
}

class QRScannerViewModel : ViewModel(), KoinComponent {
    private val repo by inject<Repository>()
    private val tracker by inject<Mixpanel>()
    private val viewModelState = MutableStateFlow(QRScannerState())
    private val cryptoModel: CryptoModel by inject()

    val crypto = cryptoModel.getInitializedCrypto()
    val uiState = viewModelState
        .map { it.toUiState(repo.jsonConverter, crypto) }
        .stateIn(
            viewModelScope,
            SharingStarted.Eagerly,
            viewModelState.value.toUiState(repo.jsonConverter, crypto)
        )

    fun getAccount(uuid: String) = viewModelScope.launch {
        val result = repo.getSecretItem(uuid)
        viewModelState.update {
            it.copy(response = result)
        }
    }

    fun handleManualCode(code: String): Boolean {
        val token = "otpauth://totp/${uiState.value.account?.name}?secret=${code}"
        return handleQRCode(qrCode = token)
    }

    private var previousCode: String? = null

    fun handleQRCode(qrCode: String): Boolean {
        val isValid = try {
            Token.parse(qrCode).code.isNotEmpty()
        } catch (e: MalformedTokenException) {
            false
        }
        if (previousCode != qrCode) {
            if (isValid) {
                tracker.trackEvent(Tracker.EVENT_2FA_ADD_QR_CODE_SCANNED)
                updateAccount(qrCode)
            } else {
                viewModelState.update {
                    tracker.trackEvent(Tracker.EVENT_2FA_INCORRECT_CODE_ENTERED)
                    it.copy(incorrectQRCode = true)
                }
            }
            previousCode = qrCode
        }
        return isValid
    }

    fun clearQRScannerState() {
        viewModelState.update {
            it.copy(token = null, errorMessage = null, incorrectQRCode = false)
        }
    }

    private fun updateAccount(qrCode: String?) =
        uiState.value.account?.let { account ->
            account.otp = qrCode
            val encryptedAccount = account.encrypt(
                repo.jsonConverter, crypto
            )
            repo.updateSecretItemCall(encryptedAccount).enqueue(object : Callback<EncryptedItemResponse> {
                override fun onResponse(
                    call: Call<EncryptedItemResponse>,
                    response: Response<EncryptedItemResponse>
                ) {
                    viewModelState.update {
                        it.copy(token = qrCode, incorrectQRCode = false)
                    }
                }

                override fun onFailure(call: Call<EncryptedItemResponse>, t: Throwable) {
                    //no-op
                }
            })
        }

    override fun onCleared() {
        if (uiState.value.account?.hasOtp() == false) {
            tracker.trackEvent(Tracker.EVENT_2FA_SETUP_CANCELLED)
        }
        super.onCleared()
    }
}
