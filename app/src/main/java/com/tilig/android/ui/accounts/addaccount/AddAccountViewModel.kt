package com.tilig.android.ui.accounts.addaccount

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.goterl.lazysodium.utils.Key
import com.goterl.lazysodium.utils.KeyPair
import com.tilig.android.R
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.data.models.*
import com.tilig.android.data.models.tilig.*
import com.tilig.android.data.repository.Repository
import com.tilig.android.ui.CryptoModel
import com.tilig.android.utils.getDomain
import com.tilig.android.utils.removePredefinedPrefixesFromDomain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.UnknownHostException

sealed interface AddAccountUiState {

    data class SelectBrand(
        override val isLoading: Boolean,
        override val errorMessages: List<UiError>? = null,
        override val account: Account? = null,
        val autocompleteBrands: List<Brand> = emptyList(),
        val currentSearchQuery: String?
    ) : AddAccountUiState

    data class CreateAccount(
        override val isLoading: Boolean,
        override val errorMessages: List<UiError>? = null,
        override val account: Account,
        val folders: List<Folder>? = null
    ) : AddAccountUiState

    val isLoading: Boolean
    val account: Account?
    val errorMessages: List<UiError>?
}

private data class AddAccountViewModelState(
    val isLoading: Boolean = false,
    val autocompleteBrands: ApiResponse<List<Brand>>? = null,
    val account: Account? = null,
    val accountError: Throwable? = null,
    val currentSearchQuery: String? = null,
    val folders: List<Folder>? = null
) {

    fun toUiError(): UiError =
        when (accountError) {
            is UnknownHostException -> UiError(null, R.string.error_autosaving_new_account)
            else -> UiError(accountError?.message, R.string.error_generic)
        }

    /**
     * Converts this [AddAccountViewModelState] into a more strongly typed [AddAccountUiState] for driving
     * the ui.
     */
    fun toUiState(): AddAccountUiState {
        val autocompleteData =
            if (autocompleteBrands is ApiSuccessResponse) autocompleteBrands.body else emptyList()

        val errorMessages = listOfNotNull(
            if (autocompleteBrands is ApiErrorResponse) autocompleteBrands.errorMessage else null,
            accountError
        ).map {
            when (it) {
                is String -> UiError(
                    it,
                    R.string.error_loading_account
                )
                is Throwable -> toUiError()
                else -> UiError(null, R.string.error_generic)
            }
        }
        return if (account == null) {
            AddAccountUiState.SelectBrand(
                isLoading = isLoading,
                errorMessages = errorMessages.ifEmpty { null },
                autocompleteBrands = autocompleteData,
                currentSearchQuery = currentSearchQuery
            )
        } else {
            AddAccountUiState.CreateAccount(
                isLoading = isLoading,
                errorMessages = errorMessages.ifEmpty { null },
                account = account,
                folders = folders
            )
        }
    }
}

class AddAccountViewModel : ViewModel(), KoinComponent {
    private val repo: Repository by inject()
    private val tracker: Mixpanel by inject()
    private val cryptoModel: CryptoModel by inject()

    private val _accountCreatedEvent = MutableStateFlow(false)
    val accountCreatedEvent: StateFlow<Boolean> = _accountCreatedEvent

    private val viewModelState = MutableStateFlow(AddAccountViewModelState())

    val uiState = viewModelState
        .map { it.toUiState() }
        .stateIn(
            viewModelScope,
            SharingStarted.Eagerly,
            AddAccountViewModelState().toUiState()
        )

    init {
        viewModelScope.launch(Dispatchers.IO) {
            val result = repo.getAllFolders()
            viewModelState.update {
                it.copy(
                    folders = if (result is ApiSuccessResponse) {
                        result.body.folders
                    } else null
                )
            }
        }
    }

    fun search(search: String?) {
        viewModelState.update {
            it.copy(isLoading = true, currentSearchQuery = search)
        }
        viewModelScope.launch(Dispatchers.IO) {
            if (search.isNullOrEmpty()) {
                viewModelState.update {
                    it.copy(isLoading = false, autocompleteBrands = ApiEmptyResponse())
                }
                return@launch
            }
            val brandsResult = repo.searchBrandsAndFetchBrandInfo(search, 20)
            viewModelState.update {
                it.copy(isLoading = false, autocompleteBrands = brandsResult)
            }
        }
    }

    fun accountPrefill(account: Account, isSuggested: Boolean) {
        viewModelState.update {
            it.copy(
                account = account,
                isLoading = false
            )
        }
        if (isSuggested && !account.website.isNullOrEmpty()) {
            loadBrand(account.website!!)
        }
    }

    private fun loadBrand(website: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val result = repo.getBrand(
                website.getDomain().removePredefinedPrefixesFromDomain() ?: website
            )
            val brand = if (result is ApiSuccessResponse) result.body else null
            viewModelState.update {
                it.copy(
                    account = it.account?.copy(
                        brand = brand
                    ).apply {
                        this?.website = website
                    }
                )
            }
        }
    }

    fun initWithSuggestedAccount(suggestedAccount: Account) {
        viewModelState.update {
            it.copy(
                account = suggestedAccount,
                isLoading = false
            )
        }
    }

    fun clearUIState() {
        clearShouldFetchAccounts()
        viewModelState.value = AddAccountViewModelState(folders = viewModelState.value.folders)
    }

    fun clearShouldFetchAccounts() {
        _accountCreatedEvent.value = false
    }

    fun onGoBack() {
        viewModelState.update {
            it.copy(account = null)
        }
    }

    fun createAccount(account: Account) {
        viewModelState.update {
            it.copy(isLoading = true, account = account)
        }
        cryptoModel.withInitializedCrypto { crypto ->
            if (account.folder != null) {
                account.decryptedDek = account.decryptedDek ?: crypto.generateDek()

                // User has a private key → used to decrypt the private KEK
                val usersKeypair = crypto.getKeyPairs().keypair!!
                val privateKek = crypto.open(
                    cipherText = account.folder!!.myEncryptedPrivateKey,
                    keyPair = usersKeypair
                )
                val publicKek = crypto.base64decode(account.folder!!.publicKey)

                // Private key of the KEK is used to decrypt the DEK
                val keyPair = KeyPair(
                    Key.fromBytes(publicKek),
                    Key.fromBytes(privateKek)
                )

                // Re-encrypt DEK using the new folder KEK for the current user
                account.encryptedFolderKey = crypto.encryptWithKeypair(
                    messageBytes = account.decryptedDek!!,
                    keyPair = keyPair
                )
            }
            val encryptedAccount =
                account.encrypt(repo.jsonConverter, crypto)

            repo.createSecretItemCall(encryptedAccount)
                .enqueue(object : Callback<EncryptedItemResponse> {
                    override fun onResponse(
                        call: Call<EncryptedItemResponse>,
                        response: Response<EncryptedItemResponse>
                    ) {
                        val responseAccount = response.body()
                        val decryptedAccount = responseAccount?.item?.decrypt(
                            repo.jsonConverter,
                            crypto
                        ) as Account
                        decryptedAccount.decryptDetails(repo.jsonConverter, crypto)
                        viewModelState.update {
                            it.copy(
                                account = decryptedAccount,
                                accountError = null
                            )
                        }
                        _accountCreatedEvent.value = true
                    }

                    override fun onFailure(call: Call<EncryptedItemResponse>, throwable: Throwable) {
                        _accountCreatedEvent.value = false
                        viewModelState.update {
                            it.copy(
                                isLoading = false,
                                account = account,
                                accountError = throwable
                            )
                        }
                    }
                })
            tracker.trackAccountCreation(account)
        }
    }

    fun updateWebsiteBrandData(website: String?) {
        val accountData = viewModelState.value.account
        if (website.isNullOrEmpty() || website == accountData?.website) {
            return
        }
        loadBrand(website)
    }
}