package com.tilig.android.ui.theme

import androidx.compose.ui.unit.sp

val FontSizeH1 = 36.sp
val FontSizeH2 = 28.sp

val FontSizeLarge = 25.sp
val FontSizeNormal = 14.sp
val FontSizeSmall = 12.sp
val FontSizeDefault = 14.sp
