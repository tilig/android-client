package com.tilig.android.ui.secrets

enum class DetailsMode {
    VIEW,
    EDIT
}