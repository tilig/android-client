package com.tilig.android.ui.secrets


import android.app.Activity
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.TabRowDefaults.tabIndicatorOffset
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.google.android.play.core.appupdate.AppUpdateManager
import com.tilig.android.MainRoute
import com.tilig.android.R
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.SecretItemType
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.models.tilig.SecretItem
import com.tilig.android.startup.RatingStatus
import com.tilig.android.startup.RatingViewModel
import com.tilig.android.startup.UpdateStatus
import com.tilig.android.startup.UpdateViewModel
import com.tilig.android.ui.SomethingWentWrongScreen
import com.tilig.android.ui.autofill.components.ImportantMessageBar
import com.tilig.android.ui.components.LoadingScreen
import com.tilig.android.ui.components.SearchView
import com.tilig.android.ui.components.StatusBarAndNavigationBarContrast
import com.tilig.android.ui.components.TiligHeader
import com.tilig.android.ui.onboarding.getstarted.GetStartedViewModel
import com.tilig.android.ui.onboarding.getstarted.components.GetStartedLabelComponent
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.safeNavigate
import org.koin.androidx.compose.get


@ExperimentalComposeUiApi
@Composable
@Preview
fun AccountsScreenPreview() {
    SecretItemsScreen(
        navController = rememberNavController(),
        showAutofillBar = remember { mutableStateOf(true) },
        searchState = remember { mutableStateOf(TextFieldValue()) },
        returnToAutofill = {},
        onAutofillEnableClicked = {},
        getStartedViewModel = get())
}

@OptIn(ExperimentalMaterialApi::class)
@ExperimentalComposeUiApi
@Composable
fun SecretItemsScreen(
    navController: NavHostController,
    isInSearchMode: Boolean = false,
    showAutofillBar: MutableState<Boolean>,
    tracker: Mixpanel = get(),
    viewModel: SecretItemsViewModel = viewModel(),
    updateHelper: UpdateViewModel = viewModel(),
    ratingHelper: RatingViewModel = viewModel(),
    updateManager: AppUpdateManager? = null,
    getStartedViewModel: GetStartedViewModel,
    lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current,
    returnToAutofill: (Account) -> Unit,
    searchState: MutableState<TextFieldValue>,
    onAutofillEnableClicked: (MutableState<Boolean>) -> Unit
) {

    val getStartedState = getStartedViewModel.uiState.collectAsState()

    LaunchedEffect(key1 = viewModel) {
        // Track this viewing event once
        tracker.trackEvent(Tracker.EVENT_VIEW_ACCOUNT_LIST)
        // Check for ratings once
        ratingHelper.checkForRating()
    }

    lifecycleOwner.lifecycle.addObserver(viewModel)
    val uiState by viewModel.uiState.collectAsState()
    val pullRefreshState = rememberPullRefreshState(
        (uiState as? SecretItemsUiState.SecretItemsSuccess)?.isRefreshing ?: false,
        { viewModel.refresh() })
    val updateStatus by updateHelper.uiState.collectAsState()
    val ratingStatus by ratingHelper.uiState.collectAsState()

    Column {
        StatusBarAndNavigationBarContrast(
            statusBarBright = false,
            navigationBarBright = true,
            navigationBarScrollBehind = true
        )

        ConstraintLayout(
            modifier = Modifier
        ) {
            val (header, title) = createRefs()

            TiligHeader(modifier = Modifier.constrainAs(header) {
                top.linkTo(parent.top)
                start.linkTo(parent.start)
            })

            Row(
                modifier = Modifier
                    .padding(top = DefaultPadding)
                    .constrainAs(title) {
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                        start.linkTo(parent.start)
                    },
                verticalAlignment = Alignment.Bottom
            ) {
                SearchView(
                    tracker = tracker,
                    state = searchState,
                    searchHint = stringResource(id = R.string.label_search_item),
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1f)
                        .padding(start = DefaultPadding)
                )

                IconButton(modifier = Modifier,
                    onClick = {
                        tracker.trackEvent(Tracker.EVENT_APP_SETTINGS)
                        navController.safeNavigate(
                            MainRoute.SecretItems.route,
                            MainRoute.Settings.route
                        )
                    }) {
                    Icon(
                        modifier = Modifier
                            .size(24.dp),
                        painter = painterResource(id = R.drawable.ic_settings),
                        contentDescription = "",
                        tint = White
                    )
                }
            }

        }

        if (getStartedState.value.displayGetStarted && getStartedState.value.getProgress() < 1f) {
            // Let the GettingStarted model know that we have items
            if(!uiState.isLoading && uiState.secretItems.any { it is Account }) {
                LaunchedEffect(uiState.secretItems) {
                    getStartedViewModel.attachLogins(true)
                }
            }
            LaunchedEffect(getStartedState.value.displayGetStarted) {
                tracker.trackEvent(Tracker.GETTING_STARTED_PROMPT_APPEARED)
            }
            GetStartedLabelComponent(
                navController,
                getStartedState.value.getProgress(),
                tracker
            )
        }

        // Only show the loading screen when we're loading *and* we are checking
        // for errors or empty screens. Don't show the loader if we already have
        // accounts, because that makes the screen jumpy
        if (uiState.isLoading && uiState.secretItems.isEmpty()) {
            LoadingScreen()
        } else {
            Box(
                modifier = Modifier
                    .pullRefresh(
                        state = pullRefreshState
                    )
                    .weight(1f)
                    .then(
                        // if allow imePadding with stick bottom view we will have next case:
                        // when keyboard is open items list goes up and only white screen is visible.
                        // Add imePadding only when there is no stick bottom view
                        if (showAutofillBar.value) {
                            Modifier
                        } else {
                            Modifier.imePadding()
                        }
                    )
            )
            {
                val onFilterDisplayType: (SecretItemType?) -> Unit = {
                    viewModel.filterByType(it)
                }

                when (uiState) {
                    is SecretItemsUiState.SecretItemsError -> {
                        SomethingWentWrongScreen(whatWasWrongText = stringResource(id = R.string.error_load_secret_items)) {
                            viewModel.secretItems()
                        }
                    }
                    is SecretItemsUiState.SecretItemsSuccess -> {
                        SecretItemListView(
                            state = uiState as SecretItemsUiState.SecretItemsSuccess,
                            viewModel = viewModel,
                            searchState = searchState,
                            onSecretItemClick = { item ->
                                if (item is Account && isInSearchMode) {
                                    returnToAutofill(item)
                                } else {
                                    navController.navigate(
                                        MainRoute.ItemDetails.createRouteForItemId(
                                            item
                                        )
                                    )
                                }
                            },
                            onFilterDisplayType = onFilterDisplayType,
                            displayFilters = true,
                        )
                    }
                }

                PullRefreshIndicator(
                    (uiState as? SecretItemsUiState.SecretItemsSuccess)?.isRefreshing ?: false,
                    pullRefreshState,
                    Modifier.align(Alignment.TopCenter),
                    scale = true
                )
            }

            // if return back to not empty list show the loader over previous fetched
            // list to prevent jump of ui
            if (uiState.isLoading) {
                // add empty click handler to prevent click on elements below loading screen (account list, filter)
                LoadingScreen(
                    modifier = Modifier.clickable(
                        interactionSource = MutableInteractionSource(),
                        indication = null,
                        onClick = {})
                )
            }
        }

        // Show a warning status if necessary
        if (updateStatus.message != null) {
            when (updateStatus.status) {
                UpdateStatus.UPDATE_AVAILABLE -> {
                    // We can show a custom message if we want, but currently we just open the system popup right away
                }
                UpdateStatus.DOWNLOADING -> {
                    // Same here. Currently it downloads in the background which is fine.
                }
                UpdateStatus.APPROVED -> {
                }
                UpdateStatus.DOWNLOADED -> {
                    ImportantMessageBar(
                        message = updateStatus.message!!,
                        buttonText = R.string.bt_install,
                        onClick = {
                            updateManager?.let {
                                updateHelper.completeUpdate(it)
                            }
                        }
                    )
                }
                UpdateStatus.ERROR -> {
                    ImportantMessageBar(
                        message = updateStatus.message!!,
                        buttonText = R.string.generic_ok,
                        onClick = {
                            updateHelper.tryLater()
                        }
                    )
                }
                else -> {}
            }
        } else if (showAutofillBar.value) {
            ImportantMessageBar(
                message = R.string.label_enable_autofill,
                buttonText = R.string.bt_enable,
                onClick = {
                    onAutofillEnableClicked(showAutofillBar)
                }
            )
        } else if (ratingStatus.status == RatingStatus.TIME_TO_ASK) {
            val activity = LocalContext.current as Activity
            LaunchedEffect(key1 = ratingStatus.status) {
                ratingHelper.askForRating(activity)
            }
        }
    }
}

@Composable
fun FilterTabs(
    currentDisplayType: SecretItemType?,
    onFilterDisplayType: (SecretItemType?) -> Unit,
    filteredSecretItemsByDisplayType: Map<SecretItemType?, List<SecretItem>>? = null,
    displayOthers: Boolean
) {
    var tabIndex by remember {
        mutableStateOf(
            when (currentDisplayType) {
                null -> 0
                SecretItemType.ACCOUNT -> 1
                SecretItemType.NOTE -> 2
                SecretItemType.CREDIT_CARD -> 3
                SecretItemType.WIFI -> 4
                SecretItemType.OTHER -> 5
            }
        )
    }
    val tabData =
        if (filteredSecretItemsByDisplayType == null) {
            if (displayOthers) {
                stringArrayResource(R.array.tab_secrets_items_with_other_type)
            } else {
                stringArrayResource(R.array.tab_secrets_items)
            }
        } else {
            val items = mutableListOf(
                stringResource(
                    id = R.string.tab_all_items,
                    filteredSecretItemsByDisplayType[null]?.size ?: 0
                ), stringResource(
                    id = R.string.tab_logins,
                    filteredSecretItemsByDisplayType[SecretItemType.ACCOUNT]?.size ?: 0
                ), stringResource(
                    id = R.string.tab_notes,
                    filteredSecretItemsByDisplayType[SecretItemType.NOTE]?.size ?: 0
                ), stringResource(
                    id = R.string.tab_cards,
                    filteredSecretItemsByDisplayType[SecretItemType.CREDIT_CARD]?.size ?: 0
                ), stringResource(
                    id = R.string.tab_wifi,
                    filteredSecretItemsByDisplayType[SecretItemType.WIFI]?.size ?: 0
                )
            )
            if (displayOthers) {
                items.add(stringResource(
                    id = R.string.tab_other,
                    filteredSecretItemsByDisplayType[SecretItemType.OTHER]?.size ?: 0
                ))
            }
            items.toTypedArray()
        }

    ScrollableTabRow(
        selectedTabIndex = tabIndex, backgroundColor = White,
        indicator = { tabPositions ->
            TabRowDefaults.Indicator(
                modifier = Modifier
                    .fillMaxWidth()
                    .tabIndicatorOffset(tabPositions[tabIndex]),
                height = 2.dp,
                color = LightBlue
            )
        },
        edgePadding = 0.dp,
    ) {
        tabData.forEachIndexed { index, text ->
            // on some devices(pixel 2, Nokia 7.2) tabs does not fill width. Set up min width manually
            Tab(modifier = Modifier.widthIn((LocalConfiguration.current.screenWidthDp / 4).dp),
                selected = tabIndex == index,
                onClick = {
                    tabIndex = index
                    onFilterDisplayType(
                        when (index) {
                            1 -> SecretItemType.ACCOUNT
                            2 -> SecretItemType.NOTE
                            3 -> SecretItemType.CREDIT_CARD
                            4 -> SecretItemType.WIFI
                            5 -> SecretItemType.OTHER
                            else -> null
                        }
                    )
                },
                text = {
                    Text(
                        text = text,
                        color = if (tabIndex == index) LightBlue else DisabledGrey,
                        style = Typography.h3
                    )
                })
        }
    }
}
