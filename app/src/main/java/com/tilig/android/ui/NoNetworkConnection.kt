package com.tilig.android.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tilig.android.R
import com.tilig.android.ui.theme.LightBlue
import com.tilig.android.ui.theme.StyleSpoofMediumDarkBlue20
import com.tilig.android.ui.theme.StyleStolzRegularDarkBlue14

@Composable
fun NoNetworkConnectionScreen(modifier: Modifier) {
    Box(
        modifier = modifier.padding(horizontal = 32.dp),
        contentAlignment = Alignment.Center
    ) {
        Box(
            modifier = Modifier
                .background(Color.White),
        ) {
            Column(
                modifier = Modifier
                    .background(Color.White)
                    .padding(horizontal = 32.dp)
            ) {
                Spacer(modifier = Modifier.height(140.dp))
                Row(modifier = Modifier.align(Alignment.CenterHorizontally)) {
                    Image(
                        modifier = Modifier.size(88.dp),
                        painter = painterResource(id = R.drawable.ic_squirrel_dark),
                        contentDescription = null
                    )
                    Spacer(modifier = Modifier.width(16.dp))
                    Image(
                        modifier = Modifier.size(88.dp),
                        painter = painterResource(id = R.drawable.ic_no_connection),
                        contentDescription = null
                    )
                }
                Spacer(modifier = Modifier.height(40.dp))
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = stringResource(id = R.string.you_are_offline_message),
                    textAlign = TextAlign.Center,
                    style = StyleSpoofMediumDarkBlue20.copy(fontSize = 24.sp)
                )
                Spacer(modifier = Modifier.height(20.dp))
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    textAlign = TextAlign.Center,
                    text = stringResource(id = R.string.offline_mode_explanation),
                    style = StyleStolzRegularDarkBlue14
                )
                Spacer(modifier = Modifier.height(80.dp))
                CircularProgressIndicator(
                    modifier = Modifier
                        .size(32.dp)
                        .align(Alignment.CenterHorizontally),
                    color = LightBlue,
                    strokeWidth = 4.dp
                )
                Spacer(modifier = Modifier.height(16.dp))
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    textAlign = TextAlign.Center,
                    text = stringResource(id = R.string.trying_to_connect),
                    style = StyleStolzRegularDarkBlue14.copy(color = LightBlue)
                )
                Spacer(modifier = Modifier.height(90.dp))
            }
        }
    }
}