package com.tilig.android.ui.components

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.relocation.BringIntoViewRequester
import androidx.compose.foundation.relocation.bringIntoViewRequester
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Info
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.focus.onFocusEvent
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tilig.android.R
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.components.ErrorHandler.Companion.displayErrorMessage
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.TextInputUtils
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/**
 * Custom Border input field without default android padding as in [TextField]
 */
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun BorderedBasicTextField(
    modifier: Modifier = Modifier,
    hint: String,
    errorHandler: ErrorHandler? = null,
    borderTextFieldColors: BorderTextFieldStyle = borderTextFieldColors(
        errorColor = errorHandler?.errorColor ?: DefaultErrorColor
    ),
    state: TextFieldState,
    textStyle: TextStyle = TextStyle(
        fontWeight = FontWeight.Normal,
        fontSize = 14.sp,
        color = if (state.isEditing) DefaultTextColor else Color.White,
        fontFamily = StolzlBook
    ),
    enabled: Boolean = true,
    singleLine: Boolean = true,
    maxLength: Int? = null,
    textModifier: Modifier = Modifier
        .fillMaxWidth()
        .padding(start = 12.dp, end = 12.dp),
    showRevealButton: Boolean = false,
    showContentInitially: Boolean = false,
    defaultShape: Shape = RoundedCornerShape(8.dp),
    contentAlignment: Alignment = Alignment.CenterStart,
    onValueChanged: ((String) -> Unit)? = null,
    onNext: (() -> Unit)? = null,
    onDone: (() -> Unit)? = null,
    onFocusLost: () -> Unit = {},
    onFocusGained: () -> Unit = {},
    keyboardType: KeyboardType = KeyboardType.Text,
    capitalization: KeyboardCapitalization = KeyboardCapitalization.None,
    defaultVisualTransformation: VisualTransformation = VisualTransformation.None,
    hideVisualTransformation: VisualTransformation = defaultVisualTransformation,
    extraButtons: @Composable (RowScope.() -> Unit)? = null
) {

    val isInErrorMode = errorHandler.displayErrorMessage() && !state.isValid
    val relocationRequestor = remember { BringIntoViewRequester() }
    val coroutineScope = rememberCoroutineScope()

    var contentVisibility by remember { mutableStateOf(showContentInitially) }
    var isContentVisibleInitially by remember { mutableStateOf(showContentInitially) }

    val shape = if (isInErrorMode) RoundedCornerShape(
        topStart = 8.dp,
        bottomStart = 8.dp,
        bottomEnd = 8.dp
    ) else defaultShape

    val borderColor =
        borderTextFieldColors.borderColor(
            focused = state.isFocused,
            showError = errorHandler != null && !state.isValid
        )

    val boxModifier = if (!singleLine) {
        Modifier
            .heightIn(min = DefaultButtonHeight)
    } else {
        Modifier
            .height(DefaultButtonHeight)
    }

    Column(modifier = modifier
        .bringIntoViewRequester(relocationRequestor)
        .onFocusEvent {
            if (it.isFocused && it.hasFocus) coroutineScope.launch {
                delay(300)
                relocationRequestor.bringIntoView()
            }
        }
        .onFocusChanged { focusState ->
            runBlocking {
                if (focusState.hasFocus) {
                    onFocusGained.invoke()
                } else {
                    onFocusLost.invoke()
                }
            }
        }) {
        if (isInErrorMode) {
            Text(
                modifier = Modifier
                    .align(Alignment.End)
                    .height(DefaultErrorMessageHeight)
                    .background(
                        color = errorHandler!!.errorColor,
                        shape = RoundedCornerShape(topStart = 8.dp, topEnd = 8.dp)
                    )
                    .padding(4.dp), text = errorHandler.errorMessage,
                style = errorHandler.errorStyle
            )
        }
        Row(
            modifier = boxModifier
                .border(width = DefaultSmallBorder, color = borderColor.value, shape = shape)
                .background(
                    if (state.isEditing) DefaultBackground else Color.Transparent,
                    shape = shape
                ),
            verticalAlignment = Alignment.CenterVertically
        ) {
            BasicTextField(
                value = state.text,
                onValueChange = {
                    if ((maxLength == null || it.length <= maxLength) && TextInputUtils.isInputTextSatisfyKeyboardType(
                            it,
                            keyboardType
                        )
                    ) {

                        state.text = it
                        coroutineScope.launch {
                            relocationRequestor.bringIntoView()
                        }
                        onValueChanged?.invoke(it)
                    }
                },
                visualTransformation = if (contentVisibility) defaultVisualTransformation else hideVisualTransformation,
                singleLine = singleLine,
                decorationBox = { innerTextField ->
                    // to center vertically
                    Box(
                        modifier = Modifier.fillMaxSize(),
                        contentAlignment = contentAlignment
                    ) {
                        if (state.text.isEmpty()) {
                            Text(
                                text = hint,
                                style = Typography.subtitle2,
                                modifier = Modifier.align(contentAlignment)
                            )
                        }
                        innerTextField()
                    }
                },
                enabled = enabled,
                textStyle = textStyle,
                modifier = textModifier
                    .weight(1f)
                    .align(Alignment.CenterVertically)
                    .onFocusChanged { focusState ->
                        state.onFocusChange(focusState.isFocused)
                        contentVisibility = focusState.isFocused && isContentVisibleInitially
                        if (focusState.isFocused) {
                            isContentVisibleInitially = false
                        }
                    },
                keyboardOptions = KeyboardOptions(
                    imeAction = when {
                        onNext != null -> ImeAction.Next
                        onDone != null -> ImeAction.Done
                        else -> ImeAction.Default
                    },
                    keyboardType = keyboardType,
                    capitalization = capitalization
                ),
                keyboardActions = KeyboardActions(
                    onNext = {
                        onNext?.invoke()
                    },
                    onDone = {
                        onDone?.invoke()
                    }
                )
            )
            if (showRevealButton) {
                val image = if (contentVisibility)
                    R.drawable.ic_revealed
                else
                    R.drawable.ic_concealed

                val action = {
                    contentVisibility = !contentVisibility
                }
                Icon(
                    modifier = Modifier
                        .padding(end = DefaultPadding)
                        .clickable { action.invoke() }
                        .size(24.dp),
                    painter = painterResource(id = image),
                    contentDescription = stringResource(id = R.string.cd_reveal_password)
                )
            }

            if (isInErrorMode) {
                Icon(
                    modifier = Modifier.padding(end = DefaultPadding),
                    imageVector = Icons.Outlined.Info,
                    tint = errorHandler!!.errorColor,
                    contentDescription = stringResource(id = R.string.error_content)
                )
            }
            extraButtons?.let {
                extraButtons()
            }
        }
    }
}

data class ErrorHandler(
    val errorMessage: String = "",
    val errorColor: Color = Grey,
    val errorStyle: TextStyle = StyleStolzRegularDarkBlue12
) {
    companion object {
        fun ErrorHandler?.displayErrorMessage() = this != null && this.errorMessage.isNotEmpty()
    }
}

@Stable
interface BorderTextFieldStyle {
    @Composable
    fun borderColor(focused: Boolean): State<Color>

    @Composable
    fun borderColor(focused: Boolean, showError: Boolean): State<Color>

    @Composable
    fun backgroundColor(): Color
}

@Immutable
private data class DefaultBorderTextFieldStyle(
    val defaultBorderColor: Color,
    val focusedBorderColor: Color,
    val errorColor: Color,
    val defaultBackgroundColor: Color
) : BorderTextFieldStyle {

    @Composable
    override fun borderColor(focused: Boolean): State<Color> {
        return rememberUpdatedState(if (focused) focusedBorderColor else defaultBorderColor)
    }

    @Composable
    override fun borderColor(focused: Boolean, showError: Boolean): State<Color> {
        return rememberUpdatedState(
            when {
                focused && showError -> errorColor
                !focused && showError -> errorColor
                focused -> focusedBorderColor
                else -> defaultBorderColor
            }
        )
    }

    @Composable
    override fun backgroundColor(): Color {
        return defaultBackgroundColor
    }

}

@Composable
fun borderTextFieldColors(
    defaultBorderColor: Color = DefaultBorderColor,
    focusedBorderColor: Color = LightBlue,
    errorColor: Color = DefaultErrorColor,
    defaultBackgroundColor: Color = White
): BorderTextFieldStyle = DefaultBorderTextFieldStyle(
    defaultBorderColor = defaultBorderColor,
    focusedBorderColor = focusedBorderColor,
    errorColor = errorColor,
    defaultBackgroundColor = defaultBackgroundColor
)