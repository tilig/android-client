package com.tilig.android.ui.secrets.add

import android.annotation.SuppressLint
import androidx.activity.compose.BackHandler
import androidx.annotation.StringRes
import androidx.compose.foundation.layout.*
import androidx.compose.material.Snackbar
import androidx.compose.material.SnackbarHost
import androidx.compose.material.SnackbarHostState
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.tilig.android.R
import com.tilig.android.data.models.UiError
import com.tilig.android.data.models.tilig.Folders
import com.tilig.android.data.models.tilig.SecretItem
import com.tilig.android.ui.components.LoadingScreen
import com.tilig.android.ui.components.ScreenHeader
import com.tilig.android.ui.notes.add.CreateNote
import com.tilig.android.ui.secrets.SecretItemsViewModel
import com.tilig.android.ui.theme.Black
import com.tilig.android.ui.theme.DefaultPadding
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@SuppressLint("CoroutineCreationDuringComposition")
@Composable
fun AddSecretItemBottomSheet(
    viewModel: BaseAddItemViewModel,
    secretItemsViewModel: SecretItemsViewModel,
    @StringRes modalTitleRes: Int,
    onCloseSheet: () -> Unit,
    onItemCreated: (item: SecretItem) -> Unit,
    content: @Composable ColumnScope.(Folders?) -> Unit
) {
    val screenState by viewModel.uiState.collectAsState()

    val coroutineScope = rememberCoroutineScope()
    val snackbarHostState = remember { SnackbarHostState() }
    val shouldFetchItems by viewModel.itemCreatedEvent.collectAsState()

    if (shouldFetchItems) {
        secretItemsViewModel.secretItems()
        LaunchedEffect(Unit) {
            coroutineScope.launch {
                secretItemsViewModel.uiState.collectLatest {
                    if (secretItemsViewModel.lookupItem(screenState.item!!.id) != null) {
                        screenState.item?.let { onItemCreated(it) }
                    }
                }
            }
        }
        viewModel.clearShouldFetchItems()
    }

    Box(
        modifier = Modifier
            .navigationBarsPadding()
            .height(LocalConfiguration.current.screenHeightDp.dp)
            .imePadding()
    ) {
        Column(
            modifier = Modifier.fillMaxSize()
        ) {
            ScreenHeader(
                headerTextRes = modalTitleRes,
                closeSheet = {
                    onCloseSheet()
                },
                leftActionIconRes = R.drawable.ic_close,
                headerBg = R.drawable.ic_coloured_header,
                tintColor = Color.White
            )

            if (!screenState.isLoading) {
                content(screenState.folders)
            } else {
                LoadingScreen()
            }

            screenState.itemError?.let {
                AddItemSnackbarErrorMessage(
                    scope = coroutineScope,
                    uiError = UiError(it.message),
                    snackbarHostState = snackbarHostState
                )
            }
        }
        SnackbarHost(
            hostState = snackbarHostState,
            Modifier
                .fillMaxWidth()
                .align(Alignment.BottomCenter)
                .padding(DefaultPadding)
        ) { data ->
            Snackbar(
                backgroundColor = Black,
                snackbarData = data,
            )
        }
    }
    BackHandler {
        onCloseSheet()
    }
}

@Preview
@Composable
fun AddSecretItemBottomSheetPreview() {
    AddSecretItemBottomSheet(
        viewModel = viewModel(),
        secretItemsViewModel = viewModel(),
        modalTitleRes = R.string.title_add_secure_note,
        onCloseSheet = {},
        onItemCreated = {},
        content = {
            CreateNote(
                folders = null,
                onSaveNote = {
                }
            )
        }
    )
}
