package com.tilig.android.ui.secrets.components

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.components.BorderedBasicTextField
import com.tilig.android.ui.theme.StyleStolzRegularDarkBlue16

@Composable
fun NoteViewEdit(
    modifier: Modifier = Modifier.fillMaxWidth(),
    notesState: TextFieldState,
    title: String,
    @StringRes contentHintRes: Int,
    maxLength: Int? = null,
    contentAlignment: Alignment = Alignment.CenterStart,
    onValueChanged: ((String) -> Unit)? = null,
    ) {
    TitledContent(
        modifier = modifier,
        title = title
    ) {
        BorderedBasicTextField(
            modifier = Modifier,
            textStyle = StyleStolzRegularDarkBlue16,
            hint = stringResource(id = contentHintRes),
            state = notesState,
            singleLine = false,
            maxLength = maxLength,
            textModifier = Modifier
                .fillMaxWidth()
                .padding(12.dp),
            keyboardType = KeyboardType.Text,
            capitalization = KeyboardCapitalization.Sentences,
            contentAlignment = contentAlignment,
            onValueChanged = onValueChanged
        )
    }
}