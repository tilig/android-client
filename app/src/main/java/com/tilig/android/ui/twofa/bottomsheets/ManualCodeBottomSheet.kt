package com.tilig.android.ui.twofa.bottomsheets

import android.annotation.SuppressLint
import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.animateScrollBy
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.relocation.BringIntoViewRequester
import androidx.compose.foundation.relocation.bringIntoViewRequester
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.insets.ProvideWindowInsets
import com.google.accompanist.insets.navigationBarsWithImePadding
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.components.ScreenHeader
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.theme.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@OptIn(androidx.compose.ui.ExperimentalComposeUiApi::class)
@Composable
fun ManualCodeBottomSheet(
    tracker: Tracker,
    closeSheet: () -> Unit,
    onManualCodeEntered: (code: String) -> Boolean
) {
    val codeState = remember { TextFieldState(fieldValue = null, validator = { it.isNotEmpty() }) }
    val scrollable = rememberScrollState()

    ProvideWindowInsets {
        Column(
            modifier = Modifier
                .verticalScroll(scrollable)
                .navigationBarsWithImePadding()
        ) {
            ScreenHeader(
                headerTextRes = R.string.add_code_manually,
                closeSheet = closeSheet
            )
            Image(
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(top = 14.dp),
                painter = painterResource(id = R.drawable.img_manual_code),
                contentDescription = null
            )
            Text(
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(top = 8.dp, bottom = 24.dp),
                textAlign = TextAlign.Center,
                text = stringResource(id = R.string.add_qr_code_manually),
                style = Typography.h2,
                fontSize = 18.sp,
                color = DarkBlue,
            )
            Text(
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(horizontal = 16.dp),
                text = stringResource(id = R.string.add_qr_code_manually_explanation),
                style = StyleStolzBookDarkBlue16,
                textAlign = TextAlign.Center
            )
            Spacer(modifier = Modifier.height(16.dp))
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .align(alignment = Alignment.CenterHorizontally)
                    .padding(horizontal = DefaultPadding)
            ) {
                FillCodeTextField(
                    hint = stringResource(id = R.string.hint_fill_in_the_code),
                    state = codeState,
                    textFieldModifier = Modifier.align(Alignment.Center),
                    scrollState = scrollable,
                    onDone = {
                        if (onManualCodeEntered(codeState.text)) {
                            tracker.trackEvent(Tracker.EVENT_2FA_SECRET_MANUALLY_ENTERED)
                            closeSheet()
                        }
                    }
                )
            }
            Spacer(modifier = Modifier.height(26.dp))
            TiligButton(
                modifier = Modifier
                    .align(alignment = Alignment.CenterHorizontally)
                    .padding(
                        start = DefaultPadding,
                        end = DefaultPadding,
                        bottom = 16.dp
                    )
                    .fillMaxWidth()
                    .height(48.dp),
                iconRes = null,
                text = stringResource(R.string.continue_with_this_code),
                enabled = codeState.isValid,
                isSizeConstrained = false,
                isDark = true,
                onClick = {
                    if (onManualCodeEntered(codeState.text)) {
                        tracker.trackEvent(Tracker.EVENT_2FA_SECRET_MANUALLY_ENTERED)
                        closeSheet()
                    }
                })
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun FillCodeTextField(
    hint: String,
    state: TextFieldState,
    fontSize: TextUnit = 15.sp,
    onFocusLost: () -> Unit = {},
    onFocusGained: () -> Unit = {},
    onDone: (() -> Unit)? = null,
    onValueChanged: (() -> Unit)? = null,
    @SuppressLint("ModifierParameter")
    textFieldModifier: Modifier = Modifier,
    scrollState: ScrollState
) {

    val bringIntoViewRequester = remember { BringIntoViewRequester() }
    val coroutineScope = rememberCoroutineScope()
    val scrollByValue = with(LocalDensity.current) { 156.dp.toPx() }
    val focusRequester = remember { FocusRequester() }

    val shape = RoundedCornerShape(8.dp)
    val borderColor = when {
        state.isFocused -> SearchBarBorderFocused
        state.isEditing -> SearchBarBorder
        else -> Color.Transparent
    }
    Box(
        modifier = Modifier
            .focusRequester(focusRequester)
            .border(1.dp, color = borderColor, shape = shape)
            .background(color = BackgroundGrey, shape = shape)
            .height(52.dp)
            .onFocusChanged { focusState ->
                if (focusState.hasFocus) {
                    onFocusGained.invoke()
                } else {
                    onFocusLost.invoke()
                }
            },
        contentAlignment = if (state.isEditing) Alignment.CenterStart else Alignment.Center
    ) {
        val textStyle = TextStyle(
            fontWeight = FontWeight.Normal,
            fontSize = fontSize,
            letterSpacing = 0.15.sp,
            color = if (state.isEditing) DefaultTextColor else Color.White,
            fontFamily = StolzlBook,
            textAlign = TextAlign.Center
        )
        TextField(
            value = state.text,
            onValueChange = {
                state.text = it
                onValueChanged?.invoke()
            },
            placeholder = {
                Text(
                    hint,
                    style = StyleStolzRegularDarkBlue16.copy(color = DimmedBlue),
                    textAlign = TextAlign.Center,
                    color = PlaceholderTextColor,
                    modifier = Modifier.fillMaxWidth()
                )
            },
            singleLine = !state.isGrowingMultiline,
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Color.Transparent,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                errorIndicatorColor = Color.Transparent,
                placeholderColor = PlaceholderTextColor
            ),
            textStyle = textStyle,
            modifier = textFieldModifier
                .fillMaxSize()
                .padding(0.dp)
                .align(Alignment.Center)
                .bringIntoViewRequester(bringIntoViewRequester)
                .onFocusChanged { focusState ->
                    state.onFocusChange(focusState.isFocused)
                    if (!focusState.isFocused) {
                        state.enableShowErrors()
                    } else {
                        coroutineScope.launch {
                            bringIntoViewRequester.bringIntoView()
                            // solution to make bottom buttons visible
                            delay(300)
                            scrollState.animateScrollBy(scrollByValue)
                        }
                    }
                },
            isError = state.showErrors(),
            keyboardOptions = KeyboardOptions(
                imeAction = when {
                    onDone != null -> ImeAction.Done
                    else -> ImeAction.Default
                }
            ),
            keyboardActions = KeyboardActions(
                onDone = {
                    onDone?.invoke()
                }
            )
        )
    }
}
