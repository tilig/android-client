package com.tilig.android.ui.autofill.components

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import com.tilig.android.R
import com.tilig.android.analytics.ConversionAnalytics
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.ui.accounts.addaccount.AddAccountUiState
import com.tilig.android.ui.accounts.addaccount.AddAccountViewModel
import com.tilig.android.ui.accounts.addaccount.components.CreateAccount
import com.tilig.android.ui.components.LoadingScreen
import com.tilig.android.utils.SharedPrefs
import org.koin.androidx.compose.inject

@Composable
fun AutofillCreateAccount(
    addAccountViewModel: AddAccountViewModel = viewModel(),
    account: Account,
    shouldAlsoAutofill: Boolean,
    action: (AutofillPopupActions) -> Unit
) {

    val tracker: Mixpanel by inject()
    val prefs: SharedPrefs by inject()
    val conversions: ConversionAnalytics by inject()
    val screenState by addAccountViewModel.uiState.collectAsState()
    val accountCreatedEvent by addAccountViewModel.accountCreatedEvent.collectAsState()

    LaunchedEffect(Unit) {
        addAccountViewModel.accountPrefill(account, true)
    }

    if (accountCreatedEvent) {
        tracker.trackAutofillEvent(prefs, conversions)
        action.invoke(
            AutofillPopupActions.FillSelected(
                account = (screenState as AddAccountUiState.CreateAccount).account
            )
        )
    } else {
        when {
            screenState.isLoading -> LoadingScreen()
            else -> {
                screenState.account?.let { acc ->
                    CreateAccount(
                        tracker = tracker,
                        account = acc,
                        folders = (screenState as AddAccountUiState.CreateAccount).folders,
                        buttonText = stringResource(id =
                            if (shouldAlsoAutofill) R.string.store_and_autofill else R.string.store
                        ),
                        onSaveAccount = { account ->
                            addAccountViewModel.createAccount(account)
                        }
                    ) { website ->
                        addAccountViewModel.updateWebsiteBrandData(website)
                    }
                }
            }
        }
    }
}