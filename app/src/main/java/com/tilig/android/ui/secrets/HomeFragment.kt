package com.tilig.android.ui.secrets

import RelatedDomainsHelper
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.animation.*
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.SpringSpec
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.*
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.view.WindowCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.navArgument
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.composable
import com.google.accompanist.navigation.animation.navigation
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import com.tilig.android.MainActivity
import com.tilig.android.MainRoute
import com.tilig.android.R
import com.tilig.android.analytics.Breadcrumb
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.autofill.AutofillHelper
import com.tilig.android.data.api.ConnectivityState
import com.tilig.android.data.models.SecretItemType
import com.tilig.android.data.models.tilig.*
import com.tilig.android.ui.BaseFragmentCompose
import com.tilig.android.ui.LaunchModeView
import com.tilig.android.ui.NoNetworkConnectionScreen
import com.tilig.android.ui.accounts.addaccount.AddAccountBottomSheet
import com.tilig.android.ui.accounts.addaccount.AddAccountViewModel
import com.tilig.android.ui.autofillsheets.AutofillSheet
import com.tilig.android.ui.cards.add.AddCardContent
import com.tilig.android.ui.components.LoadingScreen
import com.tilig.android.ui.components.LockedScreen
import com.tilig.android.ui.components.MultiFabState
import com.tilig.android.ui.components.MultiFloatingActionButton
import com.tilig.android.ui.notes.add.CreateNote
import com.tilig.android.ui.onboarding.getstarted.GetStartedScreen
import com.tilig.android.ui.onboarding.getstarted.GetStartedViewModel
import com.tilig.android.ui.secrets.add.AddSecretItemBottomSheet
import com.tilig.android.ui.secrets.add.BaseAddItemViewModel
import com.tilig.android.ui.secrets.details.SecretDetailsScreen
import com.tilig.android.ui.secrets.details.rememberDetailsScreenStateHolder
import com.tilig.android.ui.settings.ImportPasswordsScreen
import com.tilig.android.ui.settings.SettingsScreen
import com.tilig.android.ui.signin.SignInViewModel
import com.tilig.android.ui.theme.Black
import com.tilig.android.ui.theme.DefaultCornerRadius
import com.tilig.android.ui.theme.White
import com.tilig.android.ui.twofa.QRCodeScannerScreen
import com.tilig.android.ui.wifipass.add.AddWifiPassContent
import com.tilig.android.utils.VersionUtil
import com.tilig.android.utils.lock.LockViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.core.component.inject

/**
 * This Fragment is the Main Fragment. The Home Fragment. The Navigation Host. THE Fragment.
 */
@ExperimentalMaterialApi
@ExperimentalFoundationApi
@ExperimentalComposeUiApi
class HomeFragment : BaseFragmentCompose() {

    private val tracker: Mixpanel by inject()
    private val lockViewModel: LockViewModel by activityViewModels()
    private val authViewModel: SignInViewModel by activityViewModels()
    private val secretsViewModel: SecretItemsViewModel by viewModels()

    companion object {

        /**
         * Never completed functionality: when a user searches for app/url from the autofill,
         * then selects an account - should we update that account with the app ID or website url
         * that we know? Should we ask first?
         * We could simply wait for the autosave to kick in when the form is submitted.
         * Related ticket: https://gitlab.com/subshq/clients/tilig-android/-/issues/107
         * Note that Bitwarden offers (asks) to update the selected account with the new
         * information.
         */
        private const val UPDATE_ACCOUNT_AFTER_SEARCH = false
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        setStatusbarColorBright(false)
        Breadcrumb.drop(Breadcrumb.MainScreen)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        WindowCompat.setDecorFitsSystemWindows(requireActivity().window, false)

        // Tracking
        if (isInSearchMode()) {
            Breadcrumb.drop(Breadcrumb.SearchAccount)
        }

        // Load related domains into memory
        RelatedDomainsHelper.load(requireActivity())

        // Check if we are upgrading from 3.8.4 to 3.9.*, then we might need to
        // fetch or create the new libsodium keys
        authViewModel.cryptoModel.withInitializedCrypto {
            authViewModel.getOrGenerateKeyPairs(
                it.getKeyPairsAsStrings(),
                continueToFinishLogin = false
            ) { needsRefresh, _ ->
                // Refresh screen
                if (needsRefresh) {
                    Log.v("SignIn", "Refresh needed")
                    Breadcrumb.drop(Breadcrumb.CryptoTrail_HomeScreen_RequiresRefresh)
                    secretsViewModel.secretItems()
                } else {
                    Log.v("SignIn", "Refresh NOT needed")
                }
            }
        }

        return ComposeView(requireContext()).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )

            setContent {
                NavHostContainer(secretsViewModel)
            }
        }
    }

    @OptIn(ExperimentalAnimationApi::class)
    @Composable
    @Preview
    fun NavHostContainer(
        viewModel: SecretItemsViewModel = viewModel(),
        getStartedViewModel: GetStartedViewModel = viewModel(),
        addAccountViewModel: AddAccountViewModel = viewModel(),
        lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current
    ) {

        val isConnectedToTheNetwork = remember {
            mutableStateOf(true)
        }

        viewModel.connectivity.observe(viewLifecycleOwner) {
            isConnectedToTheNetwork.value = it == ConnectivityState.Connected
        }

        val scaffoldState = rememberScaffoldState()
        val navController = rememberAnimatedNavController()
        val context = LocalContext.current
        val uiState by viewModel.uiState.collectAsState()
        val fabState = remember { mutableStateOf(MultiFabState.COLLAPSED) }
        val uiLockState by lockViewModel.uiState.collectAsState()
        val keyboardManager = LocalSoftwareKeyboardController.current
        val getStartedUIState by getStartedViewModel.uiState.collectAsState()

        val modalBottomSheetState =
            rememberModalBottomSheetState(
                initialValue = ModalBottomSheetValue.Hidden,
                skipHalfExpanded = true
            )


        // we wrap this in a remember so that the bottom nav will recompose once the user enables autofill
        val shouldShowAutofillSnackbar = remember {
            mutableStateOf(AutofillHelper().shouldShowAutofillSnackbar(prefs, context))
        }
        val currentBottomSheet = remember {
            mutableStateOf<HomeBottomSheetType?>(null)
        }
        // clear current bottom sheet(during manually close or slide down)
        if (modalBottomSheetState.currentValue != ModalBottomSheetValue.Hidden) {
            DisposableEffect(Unit) {
                onDispose {
                    keyboardManager?.hide()
                    currentBottomSheet.value = null
                }
            }
        }

        val searchState = remember { mutableStateOf(TextFieldValue(getSearchQuery())) }
        val coroutineScope = rememberCoroutineScope()
        val addSecretItemViewModel: BaseAddItemViewModel = viewModel()

        ModalBottomSheetLayout(
            sheetState = modalBottomSheetState,
            scrimColor = Black.copy(alpha = 0.75f),
            sheetContent = {
                BottomSheetContent(
                    tracker = tracker,
                    addAccountViewModel = addAccountViewModel,
                    addSecretItemViewModel = addSecretItemViewModel,
                    secretItemsViewModel = viewModel,
                    onItemCreated = {
                        coroutineScope.launch {
                            navController.navigate(
                                MainRoute.ItemDetails.createRouteForItemId(it)
                            )
                            delay(300)
                            modalBottomSheetState.animateTo(ModalBottomSheetValue.Hidden)
                        }
                    },
                    bottomSheetType = currentBottomSheet.value,
                ) {
                    coroutineScope.launch {
                        modalBottomSheetState.animateTo(ModalBottomSheetValue.Hidden)
                    }
                }
            },
            sheetShape = RoundedCornerShape(
                topStart = DefaultCornerRadius,
                topEnd = DefaultCornerRadius
            )
        ) {
            Scaffold(
                scaffoldState = scaffoldState,
                snackbarHost = {
                    SnackbarHost(
                        it,
                        Modifier.fillMaxWidth()
                    ) { data ->
                        Snackbar(
                            snackbarData = data,
                        )
                    }
                },
                floatingActionButton = {
                    val navBackStackEntry by navController.currentBackStackEntryAsState()
                    val currentRoute = navBackStackEntry?.destination?.route

                    val itemClick: (SecretItemType) -> Unit = {
                        when (it) {
                            SecretItemType.ACCOUNT -> {
                                currentBottomSheet.value =
                                    HomeBottomSheetType.AddAccount(null)
                            }
                            SecretItemType.NOTE -> {
                                // We explicitly clear the UI state here, because otherwise the
                                // bottom sheet will appear prefilled with the last known info.
                                // This is more reliable than the clearUIState() done in
                                // AddAccountBottomSheet because that one is in a LaunchedEffect
                                // that is hard to trigger exactly and only at the right times.
                                addSecretItemViewModel.clearUIState()
                                currentBottomSheet.value =
                                    HomeBottomSheetType.AddNote
                            }
                            SecretItemType.CREDIT_CARD -> {
                                addSecretItemViewModel.clearUIState()
                                currentBottomSheet.value =
                                    HomeBottomSheetType.AddCard
                            }
                            SecretItemType.WIFI -> {
                                addSecretItemViewModel.clearUIState()
                                currentBottomSheet.value =
                                    HomeBottomSheetType.AddWifi
                            }
                            SecretItemType.OTHER -> {

                            }
                        }

                        coroutineScope.launch {
                            modalBottomSheetState.animateTo(
                                ModalBottomSheetValue.Expanded,
                                SpringSpec(
                                    dampingRatio = 1f,
                                    stiffness = Spring.StiffnessVeryLow,
                                    visibilityThreshold = null
                                )
                            )
                        }
                    }
                    // only show in accounts tab and if the accounts are not empty
                    if (currentRoute == MainRoute.SecretItems.route && uiState !is SecretItemsUiState.SecretItemsError) {
                        MultiFloatingActionButton(
                            // if show yellow banner, move button above not to block user's app usage
                            modifier = Modifier.padding(bottom = if (shouldShowAutofillSnackbar.value) 72.dp else 0.dp),
                            fabIcon = painterResource(id = R.drawable.ic_add),
                            onItemClick = itemClick,
                            displayType = uiState.displayType,
                            toState = fabState.value
                        ) { state ->
                            fabState.value = state
                        }
                    }
                },
                modifier = Modifier
                    .fillMaxSize()
                    .navigationBarsPadding()
            ) { innerPadding ->
                AnimatedNavHost(
                    navController,
                    startDestination = MainRoute.Home.route,
                    Modifier.padding(innerPadding)
                ) {
                    navigation(
                        route = MainRoute.Home.route,
                        startDestination = MainRoute.SecretItems.route
                    ) {
                        addHomeGraph(
                            fabState = fabState,
                            navController = navController,
                            showAutofillBar = shouldShowAutofillSnackbar,
                            searchState = searchState,
                            onAutofillEnableClicked = { state ->
                                currentBottomSheet.value =
                                    HomeBottomSheetType.Autofill(state)
                                coroutineScope.launch {
                                    modalBottomSheetState.animateTo(
                                        ModalBottomSheetValue.Expanded
                                    )
                                }
                                tracker.trackEvent(Tracker.EVENT_SHOW_AUTOFILL_1)
                            },
                            getStartedViewModel = getStartedViewModel,
                            viewModel = viewModel
                        )
                    }
                    composable(
                        MainRoute.ItemDetails.route,
                        arguments = listOf(
                            navArgument(MainRoute.ItemDetails.ARGUMENT_ITEM_ID) {
                                type = NavType.StringType
                            }
                        ),
                        enterTransition = {
                            if (this.initialState.destination.route == MainRoute.SecretItems.route) {
                                slideInHorizontally(initialOffsetX = { fullWidth -> fullWidth })
                            } else
                                null // use the defaults
                        },
                        exitTransition = {
                            if (this.targetState.destination.route == MainRoute.SecretItems.route) {
                                slideOutHorizontally(targetOffsetX = { fullWidth -> fullWidth })
                            } else
                                null // use the defaults
                        },
                    ) {
                        val item = viewModel.lookupItem(
                            it.arguments?.getString(MainRoute.ItemDetails.ARGUMENT_ITEM_ID)
                        )
                        if (item == null) {
                            // Level up again, because otherwise we'll crash.
                            // TODO show a spinner, fetch the account by its ID
                            // This is only an issue on old devices, or when the app has been in the
                            // background for a while. Test this by enabling 'Never keep Activities'
                            // in the system dev settings, then open an account, open the recent app
                            // switcher, and back to Tilig.
                            viewModel.secretItems()
                            LaunchedEffect(key1 = uiState.secretItems) {
                                val account = viewModel.lookupItem(
                                    it.arguments?.getString(MainRoute.ItemDetails.ARGUMENT_ITEM_ID)
                                ) as Account?
                                if (account != null) {
                                    navController.navigate(
                                        MainRoute.ItemDetails.createRouteForItemId(account)
                                    )
                                }
                            }
                        } else {
                            SecretDetailsScreen(
                                initialItem = item, stateHolder = rememberDetailsScreenStateHolder(
                                    navigator = navController
                                )
                            )
                        }
                    }
                    composable(MainRoute.QRCodeScanner.route) {
                        val accountId = it.arguments?.getString(
                            MainRoute.QRCodeScanner.ARGUMENT_ACCOUNT_ID,
                            null
                        )
                        if (accountId != null) {
                            QRCodeScannerScreen(
                                accountId = accountId,
                                navController = navController,
                                tracker = tracker
                            )
                        } else {
                            Log.w(
                                "ListFrag",
                                "Cannot be empty: " + it.arguments?.getString(MainRoute.QRCodeScanner.ARGUMENT_ACCOUNT_ID)
                            )
                        }
                    }
                }
            }
        }

        if (!isConnectedToTheNetwork.value) {
            NoNetworkConnectionScreen(
                modifier = Modifier
                    .fillMaxSize()
                    .background(White)
            )
        }

        // If biometric login is active, display lock screen
        if (uiLockState.isLocked) {
            LockedScreen(uiLockState.tooManyAttempts)
        }

        // on resume recheck the status of autofill
        DisposableEffect(lifecycleOwner) {
            val eventResumeObserver = LifecycleEventObserver { _, event ->
                if (event == Lifecycle.Event.ON_RESUME) {
                    shouldShowAutofillSnackbar.value =
                        AutofillHelper().shouldShowAutofillSnackbar(prefs, context)
                }
            }
            lifecycleOwner.lifecycle.addObserver(eventResumeObserver)
            onDispose {
                lifecycleOwner.lifecycle.removeObserver(eventResumeObserver)
            }
        }
    }

    /**
     * Updates the selected account with the new information from the autofill context (either
     * website url or android app ID) and returns the selected account to the autofill.
     */
    private fun updateAndReturnToAutofill(account: Account) {
        if (UPDATE_ACCOUNT_AFTER_SEARCH) {
            // TODO ask first - This functionality is proposed in the Product Ideas board on Notion

            // Update selected account
            val appId =
                requireActivity().intent.getStringExtra(MainActivity.EXTRA_ADD_APP_ID_STR)
            val website =
                requireActivity().intent.getStringExtra(MainActivity.EXTRA_ADD_URL_STR)
            if (appId != null) {
                Log.d("SEARCH", "Updating APP ID ${account.androidAppId} to $appId")
                account.androidAppId = appId
            }
            if (website != null) {
                Log.d("SEARCH", "Updating WEBSITE ${account.website} to $website")
                account.website = website
            }

            // TODO store in the backend - This functionality is proposed in the Product Ideas board on Notion
        }

        // Return it
        (activity as? LaunchModeView)?.returnAutofillWithAccount(account)
    }

    @OptIn(ExperimentalAnimationApi::class)
    private fun NavGraphBuilder.addHomeGraph(
        fabState: MutableState<MultiFabState>,
        navController: NavHostController,
        showAutofillBar: MutableState<Boolean>,
        searchState: MutableState<TextFieldValue>,
        onAutofillEnableClicked: (MutableState<Boolean>) -> Unit,
        viewModel: SecretItemsViewModel,
        getStartedViewModel: GetStartedViewModel
    ) {
        composable(MainRoute.SecretItems.route) {
            SecretItemsScreen(
                navController,
                isInSearchMode = isInSearchMode(),
                showAutofillBar = showAutofillBar,
                searchState = searchState,
                viewModel = viewModel,
                updateManager = getBaseActivity()?.getAppUpdateManager(),
                returnToAutofill = { account ->
                    updateAndReturnToAutofill(account)
                },
                onAutofillEnableClicked = onAutofillEnableClicked,
                getStartedViewModel = getStartedViewModel
            )
            // overlay
            if (fabState.value == MultiFabState.EXPANDED) {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(Black.copy(alpha = 0.5f))
                        .clickable {
                            fabState.value = MultiFabState.COLLAPSED
                        }
                )
            }
        }
        composable(MainRoute.Sharing.route) {
            // TODO placeholder until we implement sharing
            LoadingScreen()
        }
        composable(MainRoute.Settings.route) {
            SettingsScreen(navController)
        }
        composable(MainRoute.ImportPasswords.route) {
            ImportPasswordsScreen(navController)
        }
        composable(MainRoute.GetStarted.route) {
            GetStartedScreen(
                navController = navController,
                getStartedViewModel = getStartedViewModel,
                lockViewModel = lockViewModel
            )
        }
    }

}

private sealed class HomeBottomSheetType {
    class AddAccount(val suggestedAccount: Account?) : HomeBottomSheetType()
    object AddNote : HomeBottomSheetType()
    object AddCard : HomeBottomSheetType()
    object AddWifi : HomeBottomSheetType()
    class Autofill(val shouldShowAutofillSnackbar: MutableState<Boolean>) : HomeBottomSheetType()
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun BottomSheetContent(
    tracker: Mixpanel,
    addAccountViewModel: AddAccountViewModel,
    addSecretItemViewModel: BaseAddItemViewModel,
    secretItemsViewModel: SecretItemsViewModel,
    onItemCreated: (secretItem: SecretItem) -> Unit,
    bottomSheetType: HomeBottomSheetType?,
    closeSheet: () -> Unit,
) {
    Box(
        Modifier
            .navigationBarsPadding()
            .height(LocalConfiguration.current.screenHeightDp.dp)
            .imePadding()
    ) {
        LaunchedEffect(Unit) {
            addSecretItemViewModel.clearUIState()
        }
        when (bottomSheetType) {
            is HomeBottomSheetType.AddAccount -> {
                LaunchedEffect(Unit) {
                    addAccountViewModel.clearUIState()
                }
                AddAccountBottomSheet(
                    tracker = tracker,
                    viewModel = addAccountViewModel,
                    secretItemsViewModel = secretItemsViewModel,
                    suggestedAccount = bottomSheetType.suggestedAccount,
                    onCloseSheet = closeSheet
                ) {
                    onItemCreated(it)
                }
            }
            is HomeBottomSheetType.AddNote -> {
                AddSecretItemBottomSheet(
                    viewModel = addSecretItemViewModel,
                    secretItemsViewModel = secretItemsViewModel,
                    modalTitleRes = R.string.title_add_secure_note,
                    onCloseSheet = closeSheet,
                    onItemCreated = { onItemCreated(it) }
                ) {
                    CreateNote(folders = it, onSaveNote = {
                        addSecretItemViewModel.createItem(item = it)
                    })
                }
            }
            is HomeBottomSheetType.AddCard -> {
                AddSecretItemBottomSheet(
                    viewModel = addSecretItemViewModel,
                    secretItemsViewModel = secretItemsViewModel,
                    modalTitleRes = R.string.title_add_credit_card_note,
                    onCloseSheet = closeSheet,
                    onItemCreated = { onItemCreated(it) }
                ) {
                    AddCardContent(folders = it, onSaveCard = {
                        addSecretItemViewModel.createItem(item = it)
                    })
                }
            }
            is HomeBottomSheetType.AddWifi -> {
                AddSecretItemBottomSheet(
                    viewModel = addSecretItemViewModel,
                    secretItemsViewModel = secretItemsViewModel,
                    modalTitleRes = R.string.title_add_wifi_item,
                    onCloseSheet = closeSheet,
                    onItemCreated = { onItemCreated(it) }
                ) {
                    AddWifiPassContent(folders = it, onSaveWifiPass = {
                        addSecretItemViewModel.createItem(item = it)
                    })
                }
            }
            is HomeBottomSheetType.Autofill -> {
                if (VersionUtil.supportsAutofill()) {
                    AutofillSheet(
                        onCloseSheet = closeSheet,
                        shouldShowAutofillSnackbar = bottomSheetType.shouldShowAutofillSnackbar,
                        modifier = Modifier.navigationBarsPadding()
                    )
                } else {
                    closeSheet()
                }
            }
            null -> {
            }
        }
    }
}