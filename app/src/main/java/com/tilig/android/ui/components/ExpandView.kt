package com.tilig.android.ui.components

import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.LinearOutSlowInEasing
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForwardIos
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.tilig.android.ui.theme.*

@Composable
@Preview
fun ExpandViewPreview() {
    ExpandView(header = { Text("header") }, content = { Text("content") }, expanded = true)
}

@Composable
fun ExpandView(
    modifier: Modifier = Modifier,
    header: @Composable () -> Unit,
    content: @Composable () -> Unit,
    cardShape: Shape = MaterialTheme.shapes.medium,
    elevation: Dp = 1.dp,
    withBorders: Boolean = true,
    expanded: Boolean
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .animateContentSize(
                animationSpec = tween(
                    durationMillis = 300,
                    easing = LinearOutSlowInEasing
                )
            ),
        shape = cardShape,
        elevation = elevation
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .then(
                    if (withBorders) {
                        Modifier
                            .background(
                                color = if (expanded) White else BackgroundGrey,
                                shape = RoundedCornerShape(DefaultCornerRadius)
                            )
                            .border(
                                width = DefaultSmallBorder,
                                shape = RoundedCornerShape(DefaultCornerRadius),
                                color = if (expanded) LightBlue else Grey
                            )
                    } else {
                        Modifier
                    }
                )
                .then(modifier)
        ) {
            header()
            if (expanded) {
                content()
            }
        }
    }
}

@Composable
fun ExpandHeaderTitleWithArrow(
    modifier: Modifier = Modifier,
    title: String,
    textStyle: TextStyle = StyleStolzRegularDarkBlue14,
    expanded: Boolean,
    onHeaderClick: () -> Unit
) {
    val rotationState by animateFloatAsState(
        targetValue = if (expanded) -90f else 90f
    )

    Row(
        modifier = modifier
            .fillMaxWidth()
            .clickable {
                onHeaderClick.invoke()
            }
            .padding(16.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            modifier = Modifier
                .padding(end = 16.dp),
            text = title,
            style = textStyle,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )
        Icon(
            modifier = Modifier
                .size(16.dp)
                .rotate(rotationState),
            imageVector = Icons.Filled.ArrowForwardIos,
            contentDescription = "",
            tint = SettingsArrowColor
        )
    }
}