package com.tilig.android.ui.autofillsheets

import android.os.Build
import android.widget.Toast
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.annotation.RequiresApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import com.tilig.android.R
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.autofill.components.instructions.AutofillInstructionsContentComponent
import com.tilig.android.utils.SharedPrefs
import com.tilig.android.utils.VersionUtil
import com.tilig.android.utils.launchForResultSafely
import org.koin.androidx.compose.get
import org.koin.androidx.compose.inject

@ExperimentalMaterialApi
@Composable
@RequiresApi(Build.VERSION_CODES.O)
fun AutofillSheetInstructionsView(
    onClose: () -> Unit,
    systemSettingsLauncher: ManagedActivityResultLauncher<Void?, Boolean>?,
    tracker: Mixpanel = get()
) {
    val context = LocalContext.current
    val prefs: SharedPrefs by inject()

    AutofillInstructionsContentComponent {
        tracker.trackEvent(Tracker.EVENT_AUTOFILL_SETTINGS)
        if (VersionUtil.supportsAutofill()) {
            val success = systemSettingsLauncher?.launchForResultSafely(null)
            if (success != true) {
                // According to Sentry reports, this error can happen on Android 8.0
                // We'll handle it as a 'Skip' action, since that's the best we can do
                prefs.mayAskForAutofill = false
                onClose()
                // .. but let's show an error
                Toast.makeText(context, R.string.error_autofill_activity, Toast.LENGTH_LONG).show()
            }
        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@ExperimentalMaterialApi
@Composable
@Preview(showBackground = true)
fun AutofillSheetInstructionsPreview() {
    val tracker: Mixpanel by inject()
    AutofillSheetInstructionsView(
        systemSettingsLauncher = null,
        tracker = tracker,
        onClose = {}
    )
}
