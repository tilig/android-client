package com.tilig.android.ui.components

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.AlertDialog
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.theme.*

@Composable
fun TiligDialog(
    @StringRes title: Int? = null,
    @StringRes text: Int,
    onDismissRequest: () -> Unit,
    @StringRes confirmButtonText: Int,
    onConfirm: () -> Unit,
    @StringRes cancelButtonText: Int? = null,
    onCancel: (() -> Unit)? = null
) {
    if (onCancel == null || cancelButtonText == null) {
        AlertDialog(
            onDismissRequest = onDismissRequest,
            shape = RoundedCornerShape(DefaultCornerRadius),
            modifier = Modifier
                .fillMaxWidth()
                .padding(DefaultPadding),
            text = {
                TiligDialogContentView(title, text)
            },
            confirmButton = {
                TiligButton(
                    modifier = Modifier
                        .height(64.dp)
                        .padding(bottom = DefaultPadding, end = DefaultPadding),
                    text = stringResource(id = confirmButtonText),
                    enabled = true,
                    isSizeConstrained = false,
                    onClick = onConfirm,
                    isDark = true,
                    iconRes = null
                )
            }
        )
    } else {
        AlertDialog(
            onDismissRequest = onDismissRequest,
            shape = RoundedCornerShape(DefaultCornerRadius),
            modifier = Modifier
                .fillMaxWidth()
                .padding(DefaultPadding),
            text = {
                TiligDialogContentView(title, text)
            },
            confirmButton = {
                TiligButton(
                    modifier = Modifier
                        .height(64.dp)
                        .padding(bottom = DefaultPadding, end = MediumWithQuarter),
                    text = stringResource(id = confirmButtonText),
                    enabled = true,
                    isSizeConstrained = false,
                    onClick = onConfirm,
                    isDark = true,
                    iconRes = null
                )
            },
            dismissButton = {
                TiligButton(
                    modifier = Modifier
                        .height(64.dp)
                        .padding(bottom = DefaultPadding, end = HalfPadding),
                    text = stringResource(id = cancelButtonText),
                    enabled = true,
                    isSizeConstrained = false,
                    onClick = onCancel,
                    isDark = false,
                    iconRes = null
                )
            }
        )
    }
}

@Composable
private fun TiligDialogContentView(
    @StringRes title: Int? = null,
    @StringRes text: Int
) {
    when (title) {
        null -> TiligText(textRes = text)
        else -> {
            Column {
                TiligText(
                    text = stringResource(id = title),
                    textColor = LightBlue,
                    modifier = Modifier.padding(bottom = HalfPadding)
                )
                TiligText(textRes = text)
            }
        }
    }
}

@Composable
@Preview
fun TiligDialogPreview() {
    TiligDialog(
        title = R.string.app_name,
        text = R.string.error_autofill_activity,
        confirmButtonText = R.string.bt_enable,
        cancelButtonText = R.string.cancel,
        onConfirm = {},
        onCancel = {},
        onDismissRequest = {}
    )
}
