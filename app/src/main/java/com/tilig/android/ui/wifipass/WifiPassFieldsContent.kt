package com.tilig.android.ui.wifipass

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.components.*
import com.tilig.android.ui.secrets.components.AddCustomField
import com.tilig.android.ui.secrets.components.AddSecretItemNameAndLogoView
import com.tilig.android.ui.secrets.components.AllCustomFieldsEditComponent
import com.tilig.android.ui.secrets.components.TitledContent
import com.tilig.android.ui.theme.DefaultErrorColor
import com.tilig.android.ui.theme.DefaultErrorMessageHeight
import com.tilig.android.ui.theme.StyleStolzRegularDarkBlue16

@Composable
fun ColumnScope.WifiPassFieldsContent(
    fieldsState: WifiPassFieldsStateHolder,
    showWifiIcon: Boolean = false,
    supportCustomFields: Boolean = false
) {
    val context = LocalContext.current

    if (showWifiIcon) {
        AddSecretItemNameAndLogoView(
            icon = painterResource(id = R.drawable.ic_wifi),
            hintInputRes = R.string.hint_wifi_title,
            focusManager = fieldsState.focusManager,
            focusRequester = fieldsState.focusRequester,
            nameState = fieldsState.nameState,
            onValueChanged = {
                fieldsState.displayNameError.value = false
            })
    } else {
        TiligSpacerVerticalMedium()
        Box {
            BorderedBasicTextField(
                modifier = Modifier.padding(top = if (fieldsState.nameState.isValid) DefaultErrorMessageHeight else 0.dp),
                errorHandler = ErrorHandler(
                    errorMessage = stringResource(id = R.string.error_name),
                    errorColor = DefaultErrorColor
                ),
                singleLine = false,
                hint = stringResource(id = R.string.hint_wifi_title),
                state = fieldsState.nameState,
                textModifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 12.dp, vertical = 4.dp),
                keyboardType = KeyboardType.Text,
                capitalization = KeyboardCapitalization.Sentences,
                onNext = {
                    fieldsState.focusManager.moveFocus(FocusDirection.Down)
                },
                onValueChanged = {
                    fieldsState.displayNameError.value = false
                }
            )
            FieldTitle(
                title = stringResource(id = R.string.label_wifi_title),
                modifier = Modifier
            )
        }
    }

    TiligSpacerVerticalMedium()
    TitledContent(
        modifier = Modifier,
        title = stringResource(id = R.string.label_wifi_name_network)
    ) {
        BorderedBasicTextField(
            modifier = Modifier,
            hint = stringResource(id = R.string.hint_wifi_name_network),
            state = fieldsState.networkNameState,
            singleLine = false,
            onNext = {
                fieldsState.focusManager.moveFocus(FocusDirection.Next)
            },
        )
    }

    TiligSpacerVerticalMedium()
    TitledContent(
        modifier = Modifier,
        title = stringResource(id = R.string.label_wifi_password)
    ) {
        BorderedBasicTextField(
            modifier = Modifier,
            hint = stringResource(id = R.string.hint_wifi_password),
            state = fieldsState.passwordState,
            singleLine = false,
            hideVisualTransformation = PasswordVisualTransformation(),
            showRevealButton = fieldsState.passwordState.isFocused,
            onNext = {
                fieldsState.focusManager.moveFocus(FocusDirection.Next)
            },
            keyboardType = KeyboardType.Text,
        )
    }

    TiligSpacerVerticalMedium()
    TitledContent(
        modifier = Modifier,
        title = stringResource(id = R.string.label_extra_info)
    ) {
        BorderedBasicTextField(
            modifier = Modifier,
            textStyle = StyleStolzRegularDarkBlue16,
            hint = stringResource(id = R.string.hint_wifi_extra_info),
            state = fieldsState.extraInfoState,
            singleLine = false,
            textModifier = Modifier
                .fillMaxWidth()
                .padding(12.dp),
            keyboardType = KeyboardType.Text,
            capitalization = KeyboardCapitalization.Sentences,
        )
    }

    if (supportCustomFields) {
        AllCustomFieldsEditComponent(
            focusManager = fieldsState.focusManager,
            fieldsWrapper = fieldsState.wifiCustomFields.value,
            onFieldsChanged = {}) {
            fieldsState.deleteField(it)
        }

        TiligSpacerVerticalDefault()
        AddCustomField(modifier = Modifier.fillMaxWidth()) {
            fieldsState.addNewCustomField(context, it)
        }
    }
}