package com.tilig.android.ui.others.details.holders

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.Folders
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.others.details.OtherEditWrapper
import com.tilig.android.ui.secrets.CustomFieldStateWrapper

data class EditOtherStateHolder(
    val foldersList: Folders?,
    val nameState: TextFieldState,
    val customFields: List<CustomFieldStateWrapper>,
    val selectedFolder: Folder?
)

@Composable
fun rememberEditOtherStateHolder(
    foldersList: Folders?,
    otherEditWrapper: OtherEditWrapper
) =
    EditOtherStateHolder(
        foldersList = foldersList,
        nameState = remember {
            TextFieldState(
                otherEditWrapper.name,
                validator = { it.isNotEmpty() })
        },
        customFields =
        otherEditWrapper.customFields?.map {
            CustomFieldStateWrapper(it)
        } ?: emptyList(),
        selectedFolder = otherEditWrapper.selectedFolder
    )