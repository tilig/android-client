package com.tilig.android.ui.twofa.bottomsheets

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.insets.navigationBarsWithImePadding
import com.tilig.android.R
import com.tilig.android.ui.components.ScreenHeader
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.theme.DarkBlue
import com.tilig.android.ui.theme.DefaultPadding
import com.tilig.android.ui.theme.Typography

@Composable
fun WrongQRCodeBottomSheet(accountName: String, closeSheet: () -> Unit) {
    Column(modifier = Modifier.navigationBarsWithImePadding()) {
        ScreenHeader(
            headerTextRes = R.string.something_went_wrong,
            closeSheet = closeSheet
        )
        Image(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(top = 14.dp),
            painter = painterResource(id = R.drawable.ic_2fa_security_code),
            contentDescription = null
        )
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(top = 8.dp, bottom = 24.dp),
            textAlign = TextAlign.Center,
            text = stringResource(id = R.string.hm_something_went_wrong),
            style = Typography.h1,
            fontSize = 24.sp,
            color = DarkBlue,
        )
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(horizontal = 32.dp),
            text = stringResource(id = R.string.scan_error_message, accountName),
            style = Typography.h4,
            textAlign = TextAlign.Center,
            fontSize = 16.sp,
            color = DarkBlue,
        )
        TiligButton(
            modifier = Modifier
                .align(alignment = Alignment.CenterHorizontally)
                .padding(
                    start = DefaultPadding,
                    end = DefaultPadding,
                    top = 40.dp,
                    bottom = 32.dp
                )
                .fillMaxWidth()
                .height(48.dp),
            iconRes = null,
            text = stringResource(R.string.try_again),
            enabled = true,
            isSizeConstrained = false,
            isDark = true,
            onClick = {
                closeSheet()
            })
    }
}

@Composable
@Preview
fun WrongQRCodeBottomSheetPreview(){
    WrongQRCodeBottomSheet(accountName = "Account"){

    }
}
