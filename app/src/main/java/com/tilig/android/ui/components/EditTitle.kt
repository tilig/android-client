package com.tilig.android.ui.components

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text.selection.LocalTextSelectionColors
import androidx.compose.foundation.text.selection.TextSelectionColors
import androidx.compose.material.MaterialTheme
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.graphics.ColorUtils
import androidx.core.graphics.blue
import androidx.core.graphics.green
import androidx.core.graphics.red
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.theme.Spoof
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.math.abs

@Composable
fun EditTitle(
    selectionState: MutableState<TextFieldValue>,
    titleState: TextFieldState,
    textStyle: TextStyle,
    focusRequester: FocusRequester,
    onTitleChanged: (String, Boolean) -> Unit,
    modifier: Modifier,
    coroutineScope: CoroutineScope,
    cursorPointerColor: Color = MaterialTheme.colors.primary,
) {
    // The field starts with isFocused==false, so we can't respond to
    // only those events. We need to get the *second* event.
    val wasPreviouslyFocused = remember { mutableStateOf(false) }

    val cursorSelectionsColors = TextSelectionColors(
        handleColor = buildCursorColor(cursorPointerColor),
        backgroundColor = cursorPointerColor.copy(alpha = 0.4f)
    )
    CompositionLocalProvider(LocalTextSelectionColors provides cursorSelectionsColors) {
        TextField(
            value = selectionState.value,
            onValueChange = {
                titleState.text = it.text
                selectionState.value = it
                // We don't (only) wait for this TextField to lose focus,
                // but we update the viewmodel on every value change.
                // Because pressing the back button notifies the onPause
                // *before* notifying our onFocusChanged listener,
                // effectively reverting our changes.
                // We use 'false' those, because we're not done and want to
                // stay in edit mode.
                onTitleChanged(it.text, false)
            },
            singleLine = true,
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Color.Transparent,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                cursorColor = Color.White,
                errorCursorColor = Color.White
            ),
            textStyle = textStyle.copy(textAlign = TextAlign.Center),
            modifier = modifier
                .padding(start = 0.dp, end = 0.dp, top = 0.dp)
                .focusRequester(focusRequester)
                .onFocusChanged { focusState ->
                    coroutineScope.launch {
                        delay(100)
                        if (!focusState.isFocused && wasPreviouslyFocused.value) {
                            wasPreviouslyFocused.value = false
                            onTitleChanged(titleState.text, true)
                        } else {
                            wasPreviouslyFocused.value = true
                        }
                    }
                },
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
            keyboardActions = KeyboardActions(
                onDone = {
                    onTitleChanged(titleState.text, true)
                    titleState.exitEditMode()
                }
            )
        )
    }
}

@Composable
@Preview(showBackground = false)
fun EditTitlePreview() {
    val focusRequester = remember { FocusRequester() }
    val textStyle = TextStyle(
        fontSize = 20.sp,
        letterSpacing = 0.15.sp,
        color = Color.White,
        fontWeight = FontWeight.Medium,
        fontFamily = Spoof,
        textAlign = TextAlign.Center
    )
    EditTitle(
        selectionState = remember { mutableStateOf(TextFieldValue()) },
        titleState = TextFieldState(fieldValue = "Lorem Ipsum", startsInEditMode = false),
        textStyle = textStyle,
        focusRequester = focusRequester,
        onTitleChanged = { _, _ -> },
        modifier = Modifier,
        coroutineScope = rememberCoroutineScope()
    )
}

private fun buildCursorColor(colorOrigin: Color): Color{
    val hlsArray = floatArrayOf(0f, 0f, 0f)
    val arbgColor = colorOrigin.toArgb()
    ColorUtils.RGBToHSL(arbgColor.red, arbgColor.green, arbgColor.blue, hlsArray)
    val newLightness = if (abs(hlsArray[2]-0.1f) > abs(hlsArray[2]-0.9f)) 0.1f else 0.9f
    return Color.hsl(hlsArray[0], hlsArray[1], newLightness)
}
