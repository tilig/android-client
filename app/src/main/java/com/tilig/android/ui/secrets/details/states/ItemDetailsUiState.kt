package com.tilig.android.ui.secrets.details.states

import com.tilig.android.data.models.UiError
import com.tilig.android.data.models.tilig.CustomField
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.Folders
import com.tilig.android.data.models.tilig.SecretItem

interface ItemDetailsUiState {

    val isLoading: Boolean
    val errorMessage: UiError?
    val shareErrorMessage: UiError?

    data class ItemError(
        override val isLoading: Boolean,
        override val errorMessage: UiError,
        override val shareErrorMessage: UiError?,
        val debugMessage: String? = null
    ) : ItemDetailsUiState

    data class ItemView(
        val isInInitialState: Boolean,
        override val isLoading: Boolean,
        override val item: SecretItem,
        val folder: Folder?,
        override val errorMessage: UiError?,
        override val shareErrorMessage: UiError?,
        override val foldersList: Folders?,
    ) : ItemDetailsUiState, ItemDetailsSuccess

    data class ItemEdit(
        override val isLoading: Boolean,
        override val item: SecretItem,
        val itemEditWrapper: ItemEditWrapper,
        val displayConfirmChangesDialog: Boolean,
        override val errorMessage: UiError?,
        override val shareErrorMessage: UiError?,
        override val foldersList: Folders?,
    ) : ItemDetailsUiState, ItemDetailsSuccess
}

interface ItemDetailsSuccess {
    val item: SecretItem
    val foldersList: Folders?
}

interface ItemEditWrapper{
    val customFields: List<CustomField>?
    val selectedFolder: Folder?

    fun hadDataChanged(original: SecretItem): Boolean

    fun getItemName(): String?
}