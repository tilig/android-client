package com.tilig.android.ui.autofill.components.instructions

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tilig.android.R
import com.tilig.android.ui.components.LoopingVideo
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.theme.*

@Composable
fun AutofillInstructionsContentComponent(onOpenSettings: () -> Unit) {
    val scrollState = rememberScrollState()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .navigationBarsPadding()
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .weight(1f, true)
                .padding(top = 36.dp)
                .padding(horizontal = DefaultPadding)
                .verticalScroll(scrollState)
        ) {

            Text(
                text = stringResource(id = R.string.onboarding_autofill_instructions_title),
                style = StyleSpoofBoldDarkBlue28.copy(fontSize = 24.sp)
            )

            Column(
                modifier = Modifier
                    .padding(top = DefaultPadding)
                    .background(color = Blue4, shape = RoundedCornerShape(8.dp))
                    .fillMaxWidth()
                    .padding(
                        DefaultPadding
                    )
            ) {
                Text(
                    text = stringResource(id = R.string.onboarding_autofill_instructions_enjoy_autofill),
                    style = StyleStolzMediumDarkBlue14
                )
                Text(
                    modifier = Modifier.padding(top = HalfPadding),
                    text = stringResource(id = R.string.onboarding_autofill_instructions_enjoy_autofill_desc),
                    style = StyleStolzRegularDarkBlue14.copy(color = Neutral40)
                )
            }

            Column(
                modifier = Modifier
                    .padding(top = DefaultPadding)
                    .background(color = Blue4, shape = RoundedCornerShape(8.dp))
                    .padding(
                        DefaultPadding
                    )
            ) {
                Text(
                    text = stringResource(id = R.string.onboarding_autofill_instructions_enable_title),
                    style = StyleStolzMediumDarkBlue14
                )

                LoopingVideo(
                    id = R.raw.video_autofill,
                    modifier = Modifier
                        .padding(top = MediumPadding)
                        .fillMaxSize()
                        .heightIn(min = 600.dp)
                        .widthIn(min = 600.dp)
                        .background(color = Blue4)
                )
            }
        }

        TiligButton(
            modifier = Modifier,
            iconRes = null,
            text = stringResource(R.string.onboarding_autofill_instructions_bt),
            enabled = true,
            isDark = true,
            isSizeConstrained = true,
            horizontalPadding = 16.dp,
            onClick = onOpenSettings
        )

    }
}

@Preview
@Composable
fun AutofillInstructionsContentComponentPreview() {
    AutofillInstructionsContentComponent({})
}