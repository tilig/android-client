package com.tilig.android.ui.wifipass

import android.content.Context
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.platform.LocalFocusManager
import com.tilig.android.R
import com.tilig.android.data.models.tilig.CustomField
import com.tilig.android.data.models.tilig.ItemFieldType
import com.tilig.android.ui.accounts.detail.components.states.PasswordState
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.secrets.CustomFieldStateWrapper


class WifiPassFieldsStateHolder(
    val focusManager: FocusManager,
    val focusRequester: FocusRequester,
    val nameState: TextFieldState,
    val extraInfoState: TextFieldState,
    val passwordState: PasswordState,
    val networkNameState: TextFieldState,
    val displayNameError: MutableState<Boolean>,
    var wifiCustomFields: MutableState<List<CustomFieldStateWrapper>>
) {

    fun areAllRequiredFieldsSetup(): Boolean {
        var allFieldsEntered = true
        displayNameError.value = nameState.text.isEmpty()
        if (nameState.text.isEmpty()/*displayNameError.value*/) {
            allFieldsEntered = false
        }
        return allFieldsEntered
    }

    fun addNewCustomField(context: Context, fieldType: ItemFieldType) {
        val updatedList: MutableList<CustomFieldStateWrapper> =
            wifiCustomFields.value.toMutableList()
        val fieldName = when (fieldType) {
            ItemFieldType.TEXT -> R.string.custom_field_text
            ItemFieldType.PASSWORD -> R.string.custom_field_password
            ItemFieldType.SECRET -> R.string.custom_field_secret
            ItemFieldType.DATE -> R.string.custom_field_date
            ItemFieldType.TOTP -> R.string.custom_field_2fa
            else -> R.string.custom_field_secret
        }
        val fieldNameStr = context.getString(fieldName)
        val count = ((updatedList.filter { it.customField.kind == fieldType }.size).plus(1)).toString()
        val defaultName = "$fieldNameStr $count"
        updatedList.add(CustomFieldStateWrapper(CustomField(defaultName, fieldType, "")))
        wifiCustomFields.value = updatedList
    }

    fun deleteField(field: CustomFieldStateWrapper){
        val updatedList: MutableList<CustomFieldStateWrapper> =
            wifiCustomFields.value.toMutableList()
        updatedList.remove(field)
        wifiCustomFields.value = updatedList
    }
}

@Composable
fun rememberWifiPassFieldsStateHolder(
    focusManager: FocusManager = LocalFocusManager.current,
    focusRequester: FocusRequester = remember { FocusRequester() },
    displayNameError: MutableState<Boolean> = remember { mutableStateOf(false) },
    wifiName: String,
    extraInfo: String?,
    password: String?,
    networkName: String?,
    wifiCustomFields: List<CustomField>?
) = remember {
    WifiPassFieldsStateHolder(
        focusManager = focusManager,
        focusRequester = focusRequester,
        nameState = TextFieldState(wifiName, validator = {
            // on add flow show message only after click save button,
            // on edit page show immediately after losing focus if name is empty
            if (wifiName.isNotEmpty()) {
                it.isNotEmpty()
            } else {
                !displayNameError.value
            }
        }),
        passwordState = PasswordState(password),
        networkNameState = TextFieldState(networkName),
        extraInfoState = TextFieldState(extraInfo),
        displayNameError = displayNameError,
        wifiCustomFields = mutableStateOf(wifiCustomFields?.map {
            CustomFieldStateWrapper(it)
        } ?: emptyList())
    )
}