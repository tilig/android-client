package com.tilig.android.ui.autofill

import RelatedDomainsHelper
import Relation
import android.annotation.SuppressLint
import android.app.Activity
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.os.DeadObjectException
import android.service.autofill.FillRequest
import android.view.ViewGroup
import android.view.autofill.AutofillManager
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.autofill.HintConstants.AUTOFILL_HINT_SMS_OTP
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.platform.ComposeView
import androidx.core.os.bundleOf
import androidx.core.view.WindowCompat
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.tilig.android.LockActivity
import com.tilig.android.MainActivity
import com.tilig.android.R
import com.tilig.android.analytics.Breadcrumb
import com.tilig.android.analytics.ConversionAnalytics
import com.tilig.android.analytics.Tracker
import com.tilig.android.autofill.AuthLevel
import com.tilig.android.autofill.FillResponseFactory
import com.tilig.android.autofill.PackageVerifier
import com.tilig.android.autofill.TiligService
import com.tilig.android.autofill.google.StructureParser
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.ui.CryptoModel
import com.tilig.android.ui.LaunchModeView
import com.tilig.android.ui.autofill.components.AutofillActivityView
import com.tilig.android.ui.autofill.components.AutofillBottomSheetScreen
import com.tilig.android.ui.autofill.components.AutofillPopupActions
import com.tilig.android.ui.onboarding.OnboardingActivity
import com.tilig.android.utils.*
import io.sentry.Sentry
import io.sentry.SentryLevel
import org.koin.core.component.inject
import kotlin.random.Random

@RequiresApi(Build.VERSION_CODES.O)
class AutofillActivity : LockActivity(), LaunchModeView {

    private val prefs: SharedPrefs by inject()
    private val cryptoModel: CryptoModel by viewModels()
    private val autofillViewModel: AutofillViewModel by viewModels()

    private var fillRequest: FillRequest? = null
    private var parser: StructureParser? = null

    private val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            fetchAccountsAndFillParser()
        }
    }

    companion object {
        private const val REQUEST_CODE: Int = 7563

        fun createPendingIntentForCreatingNew(
            context: Context,
            fillRequest: FillRequest?,
            applicationId: String?,
            url: String? = null,
            login: String? = null,
            password: String? = null,
            username: String? = null
        ): PendingIntent {
            // Note that from the FillResponseFactory we can only set an Intent
            // for opening the app. What we want is for all these arguments
            // to end up in the EditFragment instead. So, in the startup flow
            // we pick all these Intent extras and repackage them into Fragment
            // arguments (in the shape of a Parcelable called NewAccountHint).
            // This happens in the StartFragment.
            val intent = Intent(context, AutofillActivity::class.java).apply {
                putExtra(MainActivity.EXTRA_FILL_REQUEST_PARC, fillRequest)
                putExtra(MainActivity.EXTRA_ADD_APP_ID_STR, applicationId)
                putExtra(MainActivity.EXTRA_ADD_URL_STR, url)
                putExtra(MainActivity.EXTRA_ADD_LOGIN_STR, login)
                putExtra(MainActivity.EXTRA_ADD_PASSWORD_STR, password)
                putExtra(MainActivity.EXTRA_ADD_USERNAME_STR, username)
                // This is a hack that enables us to create two PendingIntents at the same
                // time for the same Activity: one for search, one for creating a new secret
                action = Random.Default.nextInt().toString()
            }
            return PendingIntent.getActivity(
                context,
                REQUEST_CODE,
                intent,
                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
            )
        }
    }

    @OptIn(ExperimentalPagerApi::class, ExperimentalAnimationApi::class)
    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        WindowCompat.setDecorFitsSystemWindows(window, false)

        // Try to fix to portrait.
        // According to Sentry reports, some OS versions don't allow locking to portrait when
        // the Activity has a translucent background. Since it seems to work for most users,
        // we try it anyway.
        // More info: https://stackoverflow.com/questions/48072438/java-lang-illegalstateexception-only-fullscreen-opaque-activities-can-request-o
        if (Build.VERSION.SDK_INT != Build.VERSION_CODES.O) {
            try {
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            } catch (e: IllegalStateException) {
                Sentry.captureMessage("Not allowed to lock translucent activity to portrait")
            }
        }

        // Do some logging
        Breadcrumb.drop(
            if (prefs.isLoggedIn())
                Breadcrumb.CryptoTrail_LoggedIn
            else
                Breadcrumb.CryptoTrail_LoggedOut
        )

        // Track this event (User tapped the 'Autofill with Tilig' autofill entry)
        mixpanel.trackEvent(Tracker.EVENT_AUTOFILL_EXTENSION_OPENED)

        val username = intent.getStringExtra(MainActivity.EXTRA_ADD_USERNAME_STR)
        val appId = intent.getStringExtra(MainActivity.EXTRA_ADD_APP_ID_STR)
        val url = intent.getStringExtra(MainActivity.EXTRA_ADD_URL_STR)

        // Pick up the name, based on either the App name or the website URL
        val name = if (appId.isNullOrEmpty()) {
            url?.getDomain()?.removePredefinedPrefixesFromDomain()
        } else {
            PackageVerifier.getAppName(this, appId)
                ?: url?.getDomain()?.removePredefinedPrefixesFromDomain()
        }
        val account = Account.createEmpty().apply {
            this.name = name
            this.website = url
            this.androidAppId = appId
            this.username = username
        }

        // Fetch accounts
        fetchAccountsAndFillParser()

        goToOnboardingIfNotLoggedIn()

        setContentView(ComposeView(this).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )

            val chosenAccount = mutableStateOf<Account?>(null)
            setContent {
                AutofillActivityView(
                    autofillViewModel,
                    lockViewModel,
                    account,
                    chosenAccount.value,
                    startScreen = null,
                    action = { action ->
                        if (!isFinishing) {
                            when (action) {
                                is AutofillPopupActions.FillSelected -> {
                                    returnAutofillWithAccount(action.account)
                                }
                                is AutofillPopupActions.StartAddAccount -> {
                                    autofillViewModel.enterAddingMode(action.account)
                                }
                                AutofillPopupActions.ClosePopup -> {
                                    mixpanel.trackEvent(Tracker.EVENT_AUTOFILL_EXTENSION_CLOSED)
                                    finish()
                                }
                                is AutofillPopupActions.ChosenAccount -> {
                                    val relation: Relation? = if (parser?.isWebsite == true) {
                                        parser?.getWebsite()
                                            ?.removePredefinedPrefixesFromDomain()
                                            ?.let {
                                                RelatedDomainsHelper.lookupDomain(it)
                                            }
                                    } else {
                                        RelatedDomainsHelper.lookupApp(parser?.getStructPackageName())
                                    }
                                    if (autofillViewModel.isAccountMatchesWithAutofill(
                                            parser = parser,
                                            account = action.account,
                                            relation = relation
                                        )
                                    ) {
                                        returnAutofillWithAccount(action.account)
                                    } else {
                                        mixpanel.trackEvent(Tracker.EVENT_PHISHING_WARNING_APPEARED)
                                        chosenAccount.value = action.account
                                    }
                                }
                                AutofillPopupActions.ReturnToAccountsList -> {
                                    chosenAccount.value = null
                                    autofillViewModel.enterDefaultMode()
                                }
                                AutofillPopupActions.RefreshAutofillAccountsList -> {
                                    autofillViewModel.secretItems(parser = parser)
                                }
                            }
                        }
                    }
                )
            }
        })
    }

    private fun fetchAccountsAndFillParser(){
        initFillRequestAndParser()
        autofillViewModel.secretItems(parser = parser)
    }

    private fun initFillRequestAndParser() {
        if (fillRequest == null) {
            Breadcrumb.drop(Breadcrumb.AutofillTrail_FillRequest_Null)
            fillRequest =
                intent.getParcelableExtra(MainActivity.EXTRA_FILL_REQUEST_PARC)
            if (fillRequest == null) {
                Sentry.captureMessage(
                    "FillRequest missing from parcelable extra's in initFillRequestAndParser",
                    SentryLevel.INFO
                )
                return
            }
        } else {
            Breadcrumb.drop(Breadcrumb.AutofillTrail_FillRequest_Not_Null)
        }

        try {
            if (parser == null) {
                Breadcrumb.drop(Breadcrumb.AutofillTrail_Parser_Null)
                parser = StructureParser(applicationContext, fillRequest!!)
            } else {
                Breadcrumb.drop(Breadcrumb.AutofillTrail_Parser_Not_Null)
            }
            parser?.parse(forFill = true)
        } catch (e: DeadObjectException) {
            // This happens, and we don't know why. Docs don't say much, the internet suggests there's
            // a parcel too big somewhere, but that doesn't make sense either. I suspect the parser
            // tries to access the previous screen again, and the device has already cleared it
            // from memory. In that case, there's not much we can do: we can't parse it,
            // then we cant tell the system which value to put in which field. The rest of this
            // method will still execute, but the target app will not be filled with anything.
            // Original Sentry error: https://sentry.io/organizations/tilig/issues/3084436429/?environment=production&project=5237937
            // Note the 'DEVICE_STORAGE_LOW' event right before.
            Sentry.captureMessage(
                e.message ?: "DeadObjectException in initFillRequestAndParser",
                SentryLevel.DEBUG
            )
        } catch (e: RuntimeException) {
            // Samesies, but the DeadObjectException is wrapped in a RuntimeException
            Sentry.captureMessage(
                e.message ?: "RuntimeException(DeadObjectException) in initFillRequestAndParser",
                SentryLevel.DEBUG
            )
        }
    }

    override fun onResume() {
        super.onResume()

        // Track changes to autofill. We do that here, in the onResume, to make sure we also catch
        // the change when the user switched Autofill provider by following a competitor's
        // instructions.
        val isEnabled = autofill.isAutofillEnabled()
        if (isEnabled != prefs.isAutofillEnabled) {
            if (isEnabled) {
                mixpanel.trackEvent(Tracker.EVENT_AUTOFILL_ENABLED)
                conversions.trackEvent(ConversionAnalytics.EVENT_AUTOFILL_ENABLED)
            } else {
                mixpanel.trackEvent(Tracker.EVENT_AUTOFILL_DISABLED)
            }
            // Note that we leave prefs.mayAskForAutofill the way it is.
            // Since we have the yellow bar at the bottom now, we WON'T
            // start the autofill screen anymore, we'll use that bar instead.
            prefs.isAutofillEnabled = isEnabled
        }
    }

    override fun isInCreateNewMode(): Boolean = true
    override fun isInSearchMode(): Boolean = false
    override fun getSearchQuery(): String? = null

    override fun returnAutofillWithAccount(account: Account) {
        // Make sure the keypair is set. This usually already happened
        // when editing an account, but when adding a new one from the
        // autofill flow, we still need to:
        Breadcrumb.drop(Breadcrumb.CryptoTrail_ReturnToAutofill)
        val intent = Intent()

        initFillRequestAndParser()
        if (fillRequest == null && !isInCreateNewMode()) {
            // Should not be possible, but apparently the fillRequest can still
            // get lost from the extras. Just bail at that point.
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        cryptoModel.withInitializedCrypto { crypto ->
            val dataset = FillResponseFactory.createDataset(
                context = this,
                auth = AuthLevel.AUTHENTICATED_UNLOCKED,
                fillRequest = fillRequest!!,
                account = account,
                autofillFieldMetadata = parser!!.autofillFields,
                accounts = arrayListOf(account),
                crypto = crypto
            )
            intent.apply {
                // We return a dataset here to replace the
                // ResponseRequest that was created in the service.
                // The new dataset replaces the dataset that requires authentication and the views are filled out immediately
                putExtra(AutofillManager.EXTRA_AUTHENTICATION_RESULT, dataset)
                // We cannot set a clientState on a dataset, but we can pass this extra,
                // and, according to the docs, this takes precedence over the other thing when calling getClientState()
                putExtra(
                    AutofillManager.EXTRA_CLIENT_STATE, bundleOf(
                        TiligService.CLIENT_STATE_ACCOUNT to account,
                        TiligService.CLIENT_STATE_ACCOUNT_ID_STR to account.id,
                        TiligService.CLIENT_STATE_USERNAME_STR to account.username,
                        TiligService.CLIENT_STATE_PASSWORD_STR to account.password
                    )
                )
            }

            // Copy the OTP code (if any) to clipboard, but only if we didn't already autofill it
            if (account.hasOtp()) {
                val filledFields = parser!!.autofillFields.getFieldsForHint(AUTOFILL_HINT_SMS_OTP)
                if (filledFields.isNullOrEmpty()) {
                    // Copy to clipboard, and (on success) show a message to notify the user
                    if (account.otpTokenNow()?.copyToClipboard(this) == true
                        && !VersionUtil.showsSystemMessageOnCopyToClipboard()
                    ) {
                        Toast.makeText(
                            this,
                            R.string.notice_copied_to_clipboard_2fa,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }

            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mixpanel.onSessionEnd()
    }

    /**
    /Note that starting this activity does not close the current one;
    We do this: launch the OnboardingActivity on top of this one,
    and when it closes, catch in registerForActivityResult, updating
    accounts, parser information
     */
    @OptIn(ExperimentalPagerApi::class, ExperimentalAnimationApi::class)
    private fun goToOnboardingIfNotLoggedIn() {
        if (GoogleSignIn.getLastSignedInAccount(applicationContext)==null || !prefs.isLoggedIn()) {
            startForResult.launch(Intent(this, OnboardingActivity::class.java))
        }
    }
}
