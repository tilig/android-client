package com.tilig.android.ui.twofa.bottomsheets

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.insets.navigationBarsWithImePadding
import com.tilig.android.R
import com.tilig.android.ui.components.ScreenHeader
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.theme.DarkBlue
import com.tilig.android.ui.theme.DefaultPadding
import com.tilig.android.ui.theme.Typography

@Composable
fun GoToSettingsBottomSheet(closeSheet: () -> Unit) {

    val context = LocalContext.current

    Column(modifier = Modifier.navigationBarsWithImePadding()) {
        ScreenHeader(
            headerTextRes = R.string.camera_access_needed,
            closeSheet = closeSheet
        )
        Spacer(modifier = Modifier.height(32.dp))
        Icon(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(top = 14.dp),
            painter = painterResource(id = R.drawable.ic_camera),
            tint = DarkBlue,
            contentDescription = null
        )
        Spacer(modifier = Modifier.height(32.dp))
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(top = 8.dp, bottom = 24.dp),
            textAlign = TextAlign.Center,
            text = stringResource(id = R.string.camera_access_needed),
            style = Typography.h1,
            fontSize = 24.sp,
            color = DarkBlue,
        )
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(horizontal = 32.dp),
            text = stringResource(id = R.string.go_to_android_settings),
            style = Typography.h4,
            textAlign = TextAlign.Center,
            fontSize = 16.sp,
            color = DarkBlue,
        )
        TiligButton(
            modifier = Modifier
                .align(alignment = Alignment.CenterHorizontally)
                .padding(
                    start = DefaultPadding,
                    end = DefaultPadding,
                    top = 40.dp,
                    bottom = 32.dp
                )
                .fillMaxWidth()
                .height(48.dp),
            iconRes = null,
            text = stringResource(R.string.go_to_settings),
            enabled = true,
            isSizeConstrained = false,
            isDark = true,
            onClick = {
                closeSheet()
                context.openSettings()
            })
    }
}

private fun Context.openSettings() {
    val intent = Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    val uri = Uri.fromParts("package",packageName,null)
    intent.data = uri
    startActivity(intent)
}
