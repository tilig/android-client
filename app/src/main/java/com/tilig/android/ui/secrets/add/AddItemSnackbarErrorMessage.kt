package com.tilig.android.ui.secrets.add

import android.annotation.SuppressLint
import androidx.compose.material.SnackbarDuration
import androidx.compose.material.SnackbarHostState
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.tilig.android.R
import com.tilig.android.data.models.UiError
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@SuppressLint("CoroutineCreationDuringComposition")
@Composable
fun AddItemSnackbarErrorMessage(
    scope: CoroutineScope,
    uiError: UiError,
    snackbarHostState: SnackbarHostState
) {
    val genericError = stringResource(id = uiError.msgResource ?: R.string.error_generic)
    scope.launch {
        snackbarHostState.showSnackbar(
            message = uiError.message ?: genericError,
            duration = SnackbarDuration.Short
        )
    }
}