package com.tilig.android.ui.secrets.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.theme.DarkBlue
import com.tilig.android.ui.theme.StyleStolzRegularDarkBlue12

@Composable
fun TitledContent(
    modifier: Modifier = Modifier,
    loadingColor: Color? = null,
    title: String,
    titleTextStyle: TextStyle = StyleStolzRegularDarkBlue12.copy(color = DarkBlue.copy(alpha = 0.75f)),
    tracker: Tracker? = null,
    event: String? = null,
    onClick: (() -> Unit)? = null,
    content: @Composable () -> Unit,
) {
    TitledContent(
        modifier,
        loadingColor,
        AnnotatedString(title),
        titleTextStyle,
        tracker,
        event,
        onClick,
        content,
    )
}

@Composable
fun TitledContent(
    modifier: Modifier = Modifier,
    loadingColor: Color? = null,
    title: AnnotatedString,
    titleTextStyle: TextStyle = StyleStolzRegularDarkBlue12.copy(color = DarkBlue.copy(alpha = 0.75f)),
    tracker: Tracker? = null,
    event: String? = null,
    onClick: (() -> Unit)? = null,
    content: @Composable () -> Unit,
) {
    val modifierUpdated = if (onClick != null) {
        modifier
            .fillMaxWidth()
            .clickable {
                event?.let {
                    tracker?.trackEvent(it)
                }
                onClick.invoke()
            }
    } else {
        modifier
    }
    Column(
        modifier = modifierUpdated
            .fillMaxWidth()
    ) {
        Text(
            text = title,
            modifier = Modifier
                .padding(bottom = 4.dp)
                .then(
                    if (loadingColor != null) {
                        Modifier
                            .background(color = loadingColor, shape = RoundedCornerShape(4.dp))
                            .width(64.dp)
                            .height(14.dp)
                    } else {
                        Modifier
                    }
                ),
            style = titleTextStyle
        )
        content()
    }
}
