package com.tilig.android.ui.secrets.components

import androidx.annotation.StringRes
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBackIos
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.*
import com.tilig.android.BuildConfig
import com.tilig.android.R
import com.tilig.android.ui.components.AccountIcon
import com.tilig.android.ui.components.MeasureUnconstrainedViewWidth
import com.tilig.android.ui.theme.*
import kotlinx.coroutines.flow.MutableStateFlow


// When changing the Text to a TextField, the TextField has slightly more
// padding at the top. Unfortunately, we can't edit the TextField,
// so instead we reduce the parent padding, and add the padding to
// the Text to correct for this.
val ToolbarTopCorrection = 6.dp

@ExperimentalFoundationApi
@Composable
fun CollapsableToolbar(
    expandedHeight: Dp,
    progress: Float,
    title: String,
    isLoading: Boolean,
    @StringRes actionBtnTitleRes: Int,
    isActionBtnAvailable: Boolean,
    accountIcon: Any? = null,
    backgroundColor: Color?,
    shouldUpdateIcon: MutableStateFlow<Boolean>,
    callBack: SecretItemCollapsableContainerCallBack
) {

    val dynamicSize = lerp(
        AppBarCollapsedTotalHeight,
        expandedHeight - DefaultPadding,
        progress
    )

    TopAppBar(
        contentPadding = PaddingValues(),
        backgroundColor = Color.Transparent,
        modifier = Modifier
            .height(dynamicSize)
            .background(color = Color.Yellow),
        elevation = 0.dp
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(expandedHeight/*appBarExpandedHeightDp(mode)/* - DefaultPadding*/*/)
                    .background(color = backgroundColor ?: DarkBlue),
            ) {
                Image(
                    modifier = Modifier.align(Alignment.BottomStart),
                    painter = painterResource(id = R.drawable.img_blob_left),
                    contentDescription = ""
                )
                Image(
                    modifier = Modifier.align(Alignment.TopEnd),
                    painter = painterResource(id = R.drawable.img_blob_right),
                    contentDescription = ""
                )

                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(
                            top = StatusBarHeight + HalfPadding - ToolbarTopCorrection,
                            bottom = 0.dp,
                            start = 48.dp,
                            end = 48.dp
                        )
                        .align(alignment = Alignment.TopCenter),
                    horizontalAlignment = Alignment.CenterHorizontally,
                ) {

                    val roundedness = RoundedCornerShape(12.dp * progress + 4.dp)
                    val imageSize = calcImageSize(progress)

                    Box(
                        modifier = Modifier
                            .padding(top = lerp(0.dp, StatusBarHeight, progress))
                            .alpha(progress)
                            .clip(roundedness)
                            .border(1.dp * progress, Grey, roundedness)
                            .background(White)
                            .padding((8.dp * progress).coerceAtLeast(8.dp))
                    ) {
                        AccountIcon(
                            accountIcon = accountIcon,
                            shouldUpdateIcon = shouldUpdateIcon,
                            size = imageSize,
                            clipShape = roundedness,
                            modifier = Modifier
                                .alpha(progress)
                                .background(White)
                        )
                    }

                    // These properties are shared between 'normal' mode and edit mode
                    val maxWidth = LocalConfiguration.current.screenWidthDp.dp - 48.dp
                    val topPadding = lerp(0.dp, 8.dp, progress) + ToolbarTopCorrection
                    val textStyle = TextStyle(
                        fontSize = 24.sp,
                        color = Color.White,
                        fontWeight = FontWeight.Medium,
                        fontFamily = Spoof,
                        textAlign = TextAlign.Center
                    )
                    MeasureUnconstrainedViewWidth(
                        viewToMeasure = {
                            Text(title)
                        }
                    ) { measuredWidth ->
                        Text(
                            text = title,
                            maxLines = 1,
                            style = textStyle,
                            modifier = Modifier
                                .align(alignment = Alignment.CenterHorizontally)
                                .widthIn(0.dp, maxWidth)
                                .padding(top = topPadding, end = 0.dp, start = 0.dp)
                                .then(
                                    if (measuredWidth > maxWidth) {
                                        Modifier
                                            .graphicsLayer { alpha = 0.99F }
                                            .drawWithContent {
                                                val colors = listOf(
                                                    White,
                                                    White.copy(alpha = 0.99f),
                                                    White.copy(alpha = 0.95f),
                                                    Color.Transparent
                                                )
                                                drawContent()
                                                drawRect(
                                                    brush = Brush.horizontalGradient(colors),
                                                    blendMode = BlendMode.DstIn
                                                )
                                            }
                                    } else {
                                        Modifier
                                    }
                                )

                        )
                    }
                }
            }

            IconButton(
                onClick = { callBack.onBackClicked() },
                modifier = Modifier
                    .padding(
                        top = StatusBarHeight + DefaultPadding,
                        start = HalfPadding
                    ),
            ) {
                Icon(
                    modifier = Modifier
                        .size(24.dp),
                    imageVector = Icons.Filled.ArrowBackIos,
                    contentDescription = "",
                    tint = White
                )
            }

            if (BuildConfig.DEBUG) {
                TextButton(
                    modifier = Modifier
                        .align(Alignment.TopEnd)
                        .padding(
                            top = StatusBarHeight + DefaultPadding,
                            end = DoublePadding * 2
                        ),
                    onClick = { callBack.onDebugBtnClicked() }
                ) {

                    Text(
                        text = stringResource(id = R.string.debug),
                        style = StyleStolzRegularDarkBlue16.copy(color = if (title.isEmpty()) DisabledTextColor else White),
                        modifier = Modifier
                    )
                }
            }

            if (isLoading) {
                Box(
                    modifier = Modifier
                        .align(Alignment.TopEnd)
                        .padding(
                            top = StatusBarHeight + DefaultPadding,
                            end = HalfPadding
                        )
                        .padding(12.dp)
                        .size(24.dp)
                ) {
                    // background
                    CircularProgressIndicator(
                        modifier = Modifier
                            .fillMaxSize(),
                        color = White.copy(alpha = 0.75f),
                        strokeWidth = 4.dp,
                        progress = 1f
                    )
                    CircularProgressIndicator(
                        modifier = Modifier
                            .fillMaxSize(),
                        color = White,
                        strokeWidth = 4.dp
                    )
                }
            } else {
                if (isActionBtnAvailable) {
                    TextButton(
                        modifier = Modifier
                            .align(Alignment.TopEnd)
                            .padding(
                                top = StatusBarHeight + DefaultPadding,
                                end = HalfPadding
                            ),
                        onClick = {
                            callBack.onActionBtnClicked()
                        }) {

                        Text(
                            text = stringResource(actionBtnTitleRes),
                            style = StyleStolzRegularDarkBlue16.copy(color = if (title.isEmpty()) DisabledTextColor else White),
                            modifier = Modifier
                        )
                    }
                }
            }
        }
    }
}

private fun calcImageSize(collapseFraction: Float): Dp =
    lerp(0.dp, 64.dp, collapseFraction)