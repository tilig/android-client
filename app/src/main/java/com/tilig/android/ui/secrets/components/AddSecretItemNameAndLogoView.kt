package com.tilig.android.ui.secrets.components

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.components.*
import com.tilig.android.ui.theme.*


@Composable
fun AddSecretItemNameAndLogoView(
    icon: Any?,
    @StringRes hintInputRes: Int,
    focusManager: FocusManager,
    focusRequester: FocusRequester? = null,
    nameState: TextFieldState,
    onValueChanged: ((String) -> Unit)? = null,
) {
    Row(
        modifier = Modifier.padding(top = QuarterPadding),
        verticalAlignment = Alignment.CenterVertically
    ) {
        TiligImage(
            modifier = Modifier
                .padding(top = DefaultErrorMessageHeight)
                .size(32.dp)
                .clip(DefaultAccountClipShape),
            image = icon,
            contentDescription = null
        )
        TiligSpacerHorizontalSmall()
        BorderedBasicTextField(
            modifier = Modifier
                .weight(3f)
                .padding(top = if (nameState.isValid) DefaultErrorMessageHeight else 0.dp)
                .then(
                    if (focusRequester != null) Modifier.focusRequester(focusRequester) else Modifier
                ),
            textStyle = StyleSpoofBoldDarkBlue28,
            borderTextFieldColors = borderTextFieldColors(
                defaultBorderColor = SearchBarBorder,
                focusedBorderColor = SearchBarBorderFocused
            ),
            errorHandler = ErrorHandler(
                errorColor = DefaultErrorColor,
                errorMessage = stringResource(id = R.string.error_name)
            ),
            hint = stringResource(id = hintInputRes),
            state = nameState,
            singleLine = true,
            textModifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 12.dp, vertical = 4.dp),
            keyboardType = KeyboardType.Text,
            capitalization = KeyboardCapitalization.Sentences,
            onNext = {
                focusManager.moveFocus(FocusDirection.Down)
            },
            onValueChanged = onValueChanged
        )
    }
}