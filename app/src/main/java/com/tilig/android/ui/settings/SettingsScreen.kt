package com.tilig.android.ui.settings

import android.graphics.Bitmap
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.compose.BackHandler
import androidx.annotation.StringRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.NavHostController
import com.tilig.android.BuildConfig
import com.tilig.android.R
import com.tilig.android.ui.components.ScreenHeader
import com.tilig.android.ui.components.StatusBarAndNavigationBarContrast
import com.tilig.android.ui.components.TiligSpacerVerticalLarge
import com.tilig.android.ui.settings.components.*
import com.tilig.android.ui.theme.*
import org.koin.androidx.compose.get

enum class SettingsUrl(val url: String, @StringRes val titleRes: Int) {
    PRIVACY(url = "https://www.tilig.com/legal/privacy-policy", titleRes = R.string.privacy_policy),
    TERMS_AND_CONDITIONS(
        url = "https://www.tilig.com/legal/terms",
        titleRes = R.string.terms_and_conditions
    ),
    FAQ(url = "https://www.tilig.com/faq ", titleRes = R.string.label_faq)
}

@OptIn(ExperimentalMaterialApi::class, ExperimentalComposeUiApi::class)
@Composable
fun SettingsScreen(navController: NavHostController) {
    StatusBarAndNavigationBarContrast(
        statusBarBright = true,
        navigationBarBright = true,
        navigationBarScrollBehind = true
    )

    val settingsStateHolder = rememberSettingsStateHolder(navigator = navController)

    val context = LocalContext.current

    ModalBottomSheetLayout(
        modifier = Modifier.fillMaxSize(),
        sheetContent = {
            BottomSheetContent(
                closeSheet = {
                    if (settingsStateHolder.canCloseSheet()) {
                        settingsStateHolder.closeSheet()
                    } else {
                        settingsStateHolder.leaveScreen()
                    }
                 },
                settingsUrl = settingsStateHolder.getCurrentSettingsUrl()
            )
        },
        sheetState = settingsStateHolder.modalBottomSheetState,
        sheetShape = RoundedCornerShape(topStart = 16.dp, topEnd = 16.dp),
        sheetBackgroundColor = White,
    ) {
        Column(
            Modifier
                .fillMaxSize()
                .background(color = BackgroundGrey)
        ) {
            TopAppBar(
                backgroundColor = Color.Transparent,
                modifier = Modifier
                    .padding(top = StatusBarHeight),
                elevation = 0.dp,
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight(align = Alignment.CenterVertically)
                        .align(Alignment.CenterVertically)
                        .padding(horizontal = 8.dp)
                ) {
                    IconButton(
                        modifier = Modifier
                            .then(Modifier.size(32.dp))
                            .align(Alignment.CenterStart),
                        onClick = { settingsStateHolder.leaveScreen() }) {
                        Icon(
                            modifier = Modifier.size(16.dp),
                            painter = painterResource(id = R.drawable.ic_back),
                            contentDescription = null,
                            tint = DarkBlue
                        )
                    }
                    Text(
                        text = stringResource(id = R.string.title_settings),
                        style = StyleStolzRegularDarkBlue16,
                        modifier = Modifier
                            .align(Alignment.Center)
                            .fillMaxWidth(),
                        textAlign = TextAlign.Center
                    )
                }
            }
            Column(
                Modifier
                    .padding(horizontal = DefaultPadding)
                    .verticalScroll(settingsStateHolder.scrollState)
            ) {
                Spacer(modifier = Modifier.height(24.dp))
                LoggingInfoSection()
                TiligSpacerVerticalLarge()
                PreferencesSection()
                TiligSpacerVerticalLarge()
                YourDataSection(navController)
                TiligSpacerVerticalLarge()
                SupportAppsComponent()
                TiligSpacerVerticalLarge()
                SupportSection(
                    openLink = {
                        settingsStateHolder.openSheet.invoke(it)
                    },
                    sendMessageToSupport = {
                        settingsStateHolder.sendMessageToSupport(context)
                    }
                )
                TiligSpacerVerticalLarge()
                LegalSection {
                    settingsStateHolder.openSheet.invoke(it)
                }
                TiligSpacerVerticalLarge()
                DangerZone()

                Spacer(modifier = Modifier.height(72.dp))
                //BrowserExtensionView() TODO enable when the backend had implemented the browser extension link
                if (BuildConfig.DEBUG) {
                    DevTogglesView()
                }
                if (BuildConfig.ALLOW_KEY_RESET) {
                    KeyResetView()
                }

                Text(
                    modifier = Modifier.align(Alignment.CenterHorizontally),
                    text = stringResource(
                        id = R.string.current_version,
                        context.packageManager.getPackageInfo(context.packageName, 0).versionName
                    ),
                    style = StyleStolzBookDarkBlue12.copy(color = DimmedBlue)
                )

                Spacer(modifier = Modifier.height(40.dp))
            }
        }
    }
}

@Composable
private fun BottomSheetContent(
    settingsUrl: SettingsUrl?,
    closeSheet: () -> Unit,
) {
    val scrollState = rememberScrollState()
    val showProgress = remember { mutableStateOf(false) }

    Box(Modifier.defaultMinSize(minHeight = 1.dp)) {
        if (showProgress.value) {
            CircularProgressIndicator(
                modifier = Modifier
                    .size(84.dp)
                    .align(Alignment.Center),
                color = ProgressIndicatorColor,
                strokeWidth = 2.dp
            )
        }
        settingsUrl?.let { settingsUrl ->
            Column(
                modifier = Modifier
                    .height(LocalConfiguration.current.screenHeightDp.dp - 24.dp)
            ) {
                ScreenHeader(headerTextRes = settingsUrl.titleRes, closeSheet = closeSheet)
                AndroidView(
                    modifier = Modifier.verticalScroll(scrollState),
                    factory = {
                        WebView(it)
                            .apply {
                                layoutParams = ViewGroup.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT
                                )
                                webViewClient = object : WebViewClient() {
                                    override fun onPageStarted(
                                        view: WebView?,
                                        url: String?,
                                        favicon: Bitmap?
                                    ) {
                                        showProgress.value = true
                                        super.onPageStarted(view, url, favicon)
                                    }

                                    override fun onPageFinished(view: WebView?, url: String?) {
                                        showProgress.value = false
                                        super.onPageFinished(view, url)
                                    }
                                }
                                settings.javaScriptEnabled = true
                                loadUrl(settingsUrl.url)
                            }
                    },
                    update = {
                        it.loadUrl(settingsUrl.url)
                    })
            }
        }
        BackHandler {
            closeSheet.invoke()
        }
    }
}

@Preview
@Composable
fun SettingsScreenPreview() {
    SettingsScreen(navController = get())
}

