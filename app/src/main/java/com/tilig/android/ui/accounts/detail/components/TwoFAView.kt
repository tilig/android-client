package com.tilig.android.ui.accounts.detail.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.components.TiligSpacerVerticalDefault
import com.tilig.android.ui.secrets.components.TitledContent
import com.tilig.android.ui.theme.DefaultBackground
import com.tilig.android.ui.theme.DefaultButtonHeight
import com.tilig.android.ui.theme.DefaultSmallBorder
import com.tilig.android.ui.twofa.TwoFactorAuthEnabled
import com.tilig.android.ui.twofa.TwoFactorAuthNotEnabled

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun TwoFAEdit(
    otpValue: String?,
    accountName: String,
    support2FA: Boolean,
    has2FAToken: Boolean,
    tracker: Tracker,
    onOpenScanScreen: () -> Unit,
    onShowTutorial: () -> Unit,
    onNewCodeGenerated: (code: String?) -> Unit,
    onClick: (() -> Unit)? = null
) {
    if (has2FAToken) {
        TiligSpacerVerticalDefault()
        TitledContent(
            modifier = Modifier.fillMaxWidth(),
            title = stringResource(id = R.string.label_two_fa),
            event = Tracker.EVENT_QR_CODE_COPY,
            onClick = onClick,
        ) {
            TwoFactorAuthEnabled(modifier = Modifier
                .fillMaxSize()
                .alpha(0.4f)
                .height(DefaultButtonHeight)
                .border(
                    DefaultSmallBorder,
                    color = Color.Transparent,
                    shape = RoundedCornerShape(8.dp)
                )
                .background(
                    DefaultBackground,
                    shape = RoundedCornerShape(8.dp)
                )
                .padding(horizontal = 12.dp),
                otp = otpValue, onNewCodeGenerated = {
                    onNewCodeGenerated(it)
                })
        }
    } else if (support2FA) {
        TiligSpacerVerticalDefault()
        TitledContent(
            modifier = Modifier.fillMaxWidth(),
            title = stringResource(id = R.string.label_two_fa)
        ) {
            TwoFactorAuthNotEnabled(
                onOpenScanScreen = onOpenScanScreen,
                accountName = accountName,
                tracker = tracker,
                onShowTutorial = onShowTutorial
            )
        }
    }
}
