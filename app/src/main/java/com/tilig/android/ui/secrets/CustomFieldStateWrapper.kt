package com.tilig.android.ui.secrets

import com.tilig.android.data.models.tilig.CustomField
import com.tilig.android.data.models.tilig.ItemFieldType
import com.tilig.android.data.qrscanner.MalformedTokenException
import com.tilig.android.data.qrscanner.Token
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState

data class CustomFieldStateWrapper(
    val customField: CustomField,
    val textFieldState: TextFieldState,
    val labelFieldState: TextFieldState
) {
    constructor(customField: CustomField) : this(customField = customField,
        TextFieldState(customField.value, validator = {
            // check if is valid token
            if (customField.kind == ItemFieldType.TOTP){
                try {
                    Token.parse(customField.value).code.isNotEmpty()
                } catch (e: MalformedTokenException) {
                    false
                }
            }else{
                true
            }
        }),
        labelFieldState = TextFieldState(customField.name, validator = { it.isNotEmpty() })
    )
}