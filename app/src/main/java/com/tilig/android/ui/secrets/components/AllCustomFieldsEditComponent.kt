package com.tilig.android.ui.secrets.components

import android.app.DatePickerDialog
import android.widget.DatePicker
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.data.models.tilig.ItemFieldType
import com.tilig.android.ui.accounts.addaccount.components.CreatePasswordContent
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.components.BorderedBasicTextField
import com.tilig.android.ui.components.ErrorHandler
import com.tilig.android.ui.components.TiligSpacerVerticalDefault
import com.tilig.android.ui.secrets.CustomFieldStateWrapper
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.DateUtils
import java.text.SimpleDateFormat
import java.util.*

@Composable
fun ColumnScope.AllCustomFieldsEditComponent(
    focusManager: FocusManager,
    fieldsWrapper: List<CustomFieldStateWrapper>,
    onFieldsChanged: () -> Unit,
    onDeleteField: (CustomFieldStateWrapper) -> Unit
) {
    fieldsWrapper.forEach { field ->
        TiligSpacerVerticalDefault()
        CustomTextFieldEditComponent(
            fieldType = field.customField.kind,
            textState = field.textFieldState,
            titleState = field.labelFieldState,
            focusManager = focusManager,
            onDeleteField = {
                onDeleteField.invoke(field)
            },
            onValueChanged = onFieldsChanged
        )
    }
}

@Composable
private fun CustomTextFieldEditComponent(
    modifier: Modifier = Modifier,
    fieldType: ItemFieldType,
    textState: TextFieldState,
    titleState: TextFieldState,
    focusManager: FocusManager,
    titleTextStyle: TextStyle = StyleStolzRegularDarkBlue12.copy(color = DarkBlue.copy(alpha = 0.75f)),
    onFocusLost: () -> Unit = {},
    onValueChanged: () -> Unit,
    onDeleteField: () -> Unit
) {
    Box(modifier = modifier.fillMaxWidth()) {
        Row(modifier.fillMaxWidth()) {
            when (fieldType) {
                ItemFieldType.DATE -> DatePickerComponent(
                    textState,
                    onValueChanged = onValueChanged
                )
                ItemFieldType.PASSWORD -> {
                    CreatePasswordContent(
                        modifier = Modifier.weight(1f),
                        tracker = null,
                        passwordState = textState,
                        hint = R.string.hint_login_password,
                        onDone = {
                            onValueChanged.invoke()
                        },
                        onValueChanged = {
                            textState.text = it
                            onValueChanged.invoke()
                        }
                    )
                }
                else -> {
                    BorderedBasicTextField(
                        modifier = Modifier
                            .weight(1f)
                            .padding(top = if (textState.isValid) DefaultErrorMessageHeight else 0.dp),
                        textStyle = StyleStolzRegularDarkBlue16,
                        hint = stringResource(id = R.string.custom_field_text_hint),
                        state = textState,
                        singleLine = false,
                        enabled = true,
                        errorHandler = ErrorHandler(
                            errorMessage = stringResource(id = if (fieldType == ItemFieldType.TOTP) R.string.custom_field_error_invalid else R.string.custom_field_error_empty),
                            errorColor = DefaultErrorColor
                        ),
                        textModifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 12.dp, vertical = 4.dp),
                        keyboardType = KeyboardType.Text,
                        onNext = {
                            focusManager.moveFocus(FocusDirection.Down)
                        },
                        showRevealButton = fieldType == ItemFieldType.SECRET,
                        showContentInitially = true,
                        onFocusLost = onFocusLost,
                        onValueChanged = {
                            textState.text = it
                            onValueChanged.invoke()
                        },
                        hideVisualTransformation = if (fieldType.isSensitive()) PasswordVisualTransformation() else VisualTransformation.None
                    )
                }
            }

            Icon(
                modifier = Modifier
                    .padding(top = 25.dp, start = DefaultPadding)
                    .size(20.dp)
                    .align(Alignment.CenterVertically)
                    .clickable {
                        onDeleteField.invoke()
                    },
                painter = painterResource(id = R.drawable.ic_trash),
                contentDescription = stringResource(id = R.string.delete_share_link),
                tint = DarkGrey
            )
        }

        BasicTextField(
            modifier = Modifier,
            textStyle = titleTextStyle, value = titleState.text,
            onValueChange = {
                titleState.text = it
                onValueChanged.invoke()
            },
        )
    }
}

@Composable
private fun RowScope.DatePickerComponent(
    textState: TextFieldState,
    onValueChanged: () -> Unit,
) {
    val calendar = Calendar.getInstance()
    LaunchedEffect(Unit) {
        if (textState.text.isEmpty()) {
            textState.text =
                SimpleDateFormat(DateUtils.API_DATE_FORMAT, Locale.getDefault()).apply {
                    timeZone = TimeZone.getTimeZone("UTC")
                }.format(calendar.time)
        }
    }
    val context = LocalContext.current

    val dateState = TextFieldState(
        DateUtils.formatDate(context = context, originDate =
        textState.text, targetFormat = DateUtils.DATE_DOT_FORMAT, originalFormat =
        SimpleDateFormat(DateUtils.API_DATE_FORMAT, Locale.getDefault()).apply {
            timeZone = TimeZone.getTimeZone("UTC")
        }
        )
    )

    val datePickerDialog = DatePickerDialog(
        context,
        { _: DatePicker, year: Int, month: Int, dayOfMonth: Int ->
            textState.text =
                DateUtils.buildDateStr(context, year, month, dayOfMonth, DateUtils.API_DATE_FORMAT)
            onValueChanged.invoke()
        },
        calendar.get(Calendar.YEAR),
        calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DAY_OF_MONTH)
    )

    BorderedBasicTextField(
        modifier = Modifier
            .weight(1f)
            .padding(top = if (textState.isValid) DefaultErrorMessageHeight else 0.dp)
            .clickable {
                datePickerDialog.show()
            },
        textStyle = StyleStolzRegularDarkBlue16,
        hint = stringResource(id = R.string.custom_field_date_hint),
        state = dateState,
        enabled = false,
        errorHandler = ErrorHandler(
            errorMessage = stringResource(id = R.string.custom_field_error_empty),
            errorColor = DefaultErrorColor
        ),
        textModifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 12.dp, vertical = 4.dp),
        showContentInitially = true,
        extraButtons = {
            Icon(
                modifier = Modifier
                    .padding(end = DefaultPadding)
                    .size(24.dp),
                painter = painterResource(id = R.drawable.ic_calendar),
                contentDescription = stringResource(id = R.string.custom_field_date)
            )
        }
    )
}