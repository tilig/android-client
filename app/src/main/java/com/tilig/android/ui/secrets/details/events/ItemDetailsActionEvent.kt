package com.tilig.android.ui.secrets.details.events

import com.tilig.android.data.models.tilig.CustomField
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.ItemFieldType


interface ItemDetailsActionEvent{
    object DebugItem : ItemDetailsActionEvent
    object DeleteItem : ItemDetailsActionEvent
    object CancelChangesDialog : ItemDetailsActionEvent
    object ConfirmDiscardChanges : ItemDetailsActionEvent
    object OnGoBack : ItemDetailsActionEvent
    object SaveChanges : ItemDetailsActionEvent
    object EnterEditMode : ItemDetailsActionEvent
    data class Edit(val event: BaseEditEvent) : ItemDetailsActionEvent
    data class Share(val email: String) : ItemDetailsActionEvent
    data class ShareRevoke(val folderId: String, val memberId: String) : ItemDetailsActionEvent
}

interface BaseEditEvent {
    data class CustomFieldsChanged(val fields: List<CustomField>) : BaseEditEvent
    data class AddCustomField(val fieldType: ItemFieldType) : BaseEditEvent
    data class ChangeFolder(val newFolder: Folder?) : BaseEditEvent
}
