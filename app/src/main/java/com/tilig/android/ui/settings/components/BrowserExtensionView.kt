package com.tilig.android.ui.settings.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.components.FieldTitle
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.theme.DefaultTextColor
import com.tilig.android.ui.theme.Typography

@Preview
@Composable
fun BrowserExtensionView() {
    FieldTitle(title = stringResource(id = R.string.label_browser_extension))
    Text(
        text = stringResource(id = R.string.info_browser_extension),
        style = Typography.subtitle2,
        color = DefaultTextColor
    )
    TiligButton(
        modifier = Modifier
            .padding(top = 8.dp)
            .height(64.dp)
            .fillMaxWidth(),
        iconRes = null,
        text = stringResource(R.string.bt_browser_extension),
        enabled = true,
        isDark = true,
        isSizeConstrained = false,
        onClick = { /*TODO*/ })
}
