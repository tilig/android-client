package com.tilig.android.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import com.tilig.android.R

val StolzlBook = FontFamily(
    Font(R.font.stolzl_book)
)

val Stolzl = FontFamily(
    Font(R.font.stolzl_regular, FontWeight.Normal),
    Font(R.font.stolzl_bold, FontWeight.Bold),
    Font(R.font.stolzl_light, FontWeight.Light),
    Font(R.font.stolzl_medium, FontWeight.Medium),
    Font(R.font.stolzl_thin, FontWeight.Thin),
)


val Spoof = FontFamily(
    Font(R.font.spoof_regular, FontWeight.Normal),
    Font(R.font.spoof_regular, FontWeight.Normal, FontStyle.Italic),
    Font(R.font.spoof_black, FontWeight.Black),
    Font(R.font.spoof_black, FontWeight.Black, FontStyle.Italic),
    Font(R.font.spoof_bold_slanted, FontWeight.Bold, FontStyle.Italic),
    Font(R.font.spoof_bold, FontWeight.Bold),
    Font(R.font.spoof_light, FontWeight.Light),
    Font(R.font.spoof_light_slanted, FontWeight.Light, FontStyle.Italic),
    Font(R.font.spoof_medium, FontWeight.Medium),
    Font(R.font.spoof_medium_slanted, FontWeight.Medium, FontStyle.Italic),
    Font(R.font.spoof_thin, FontWeight.Thin),
    Font(R.font.spoof_thin_slanted, FontWeight.Thin, FontStyle.Italic)
)

val Space = FontFamily(
    Font(R.font.space_mono, FontWeight.Normal)
)

val RobotoMono = FontFamily(
    Font(R.font.roboto_medium, FontWeight.Medium),
)

// Set of Material typography styles to start with
val Typography = Typography(
    body1 = TextStyle(
        fontFamily = StolzlBook,
        fontWeight = FontWeight.Normal,
        fontSize = FontSizeNormal,
        color = DefaultTextColor
    ),
    h1 = TextStyle(
        fontFamily = Spoof,
        fontWeight = FontWeight.Bold,
        fontSize = FontSizeH1,
        color = White
    ),
    h2 = TextStyle(
        fontFamily = Stolzl,
        fontWeight = FontWeight.Medium,
        fontSize = FontSizeH2,
        color = White
    ),
    h3 = TextStyle(
        fontFamily = Stolzl,
        fontWeight = FontWeight.Normal,
        fontSize = FontSizeNormal,
        color = DefaultTextColor,
    ),
    h4 = TextStyle(
        fontFamily = Spoof,
        fontWeight = FontWeight.Normal,
        fontSize = FontSizeLarge,
        color = DefaultTextColor,
        lineHeight = FontSizeLarge * 1.2f
    ),
    caption = TextStyle(
        fontFamily = Stolzl,
        fontWeight = FontWeight.Normal,
        fontSize = FontSizeSmall,
        color = LightBlue
    ),
    button = TextStyle(
        fontFamily = Stolzl,
        fontWeight = FontWeight.Normal,
        fontSize = FontSizeNormal,
        color = DefaultTextColor,
        lineHeight = FontSizeNormal
    ),
    subtitle1 = TextStyle(
        fontFamily = StolzlBook,
        fontWeight = FontWeight.Normal,
        fontSize = FontSizeSmall,
        color = DarkGrey
    ),
    // used also for hints
    subtitle2 = TextStyle(
        fontFamily = StolzlBook,
        fontWeight = FontWeight.Normal,
        fontSize = FontSizeNormal,
        color = DarkGrey
    )
)

val TypographyDisabled = Typography(
    caption = TextStyle(
        fontFamily = Stolzl,
        fontWeight = FontWeight.Normal,
        fontSize = FontSizeSmall,
        color = DisabledTextColor
    )
)
