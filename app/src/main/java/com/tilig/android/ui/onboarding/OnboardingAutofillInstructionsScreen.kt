package com.tilig.android.ui.onboarding

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import androidx.navigation.findNavController
import com.tilig.android.R
import com.tilig.android.analytics.*
import com.tilig.android.ui.autofill.AutofillEnableContract
import com.tilig.android.ui.autofill.components.instructions.AutofillInstructionsContentComponent
import com.tilig.android.ui.components.*
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.*
import org.koin.androidx.compose.get

class OnboardingAutofillInstructionsFragment : Fragment() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        return ComposeView(requireContext()).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            setContent {
                OnboardingAutofillInstructionsScreen(findNavController())
            }
        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun OnboardingAutofillInstructionsScreen(
    navController: NavController,
    tracker: Mixpanel = get(),
    conversions: ConversionAnalytics = get(),
    preferences: SharedPrefs = get(),
) {
    val context = LocalContext.current
    val systemSettingsLauncher =
        rememberLauncherForActivityResult(contract = AutofillEnableContract()) {
            if (it) {
                tracker.trackEvent(Tracker.EVENT_AUTOFILL_ENABLED)
                conversions.trackEvent(ConversionAnalytics.EVENT_AUTOFILL_ENABLED)
                preferences.mayAskForAutofill = false
                preferences.isAutofillEnabled = true
                navController.safeNavigate(
                    R.id.onboardingAutofillInstructionsFragment,
                    R.id.onboardingAutofillDoneFragment
                )
            }
        }

    LaunchedEffect(Unit) {
        tracker.trackEvent(Tracker.EVENT_SHOW_AUTOFILL_2)
    }

    val scrollState = rememberScrollState()

    Surface(
        modifier = Modifier
            .fillMaxSize()
            .navigationBarsPadding(),
        color = MaterialTheme.colors.background
    ) {
        StatusBarAndNavigationBarContrast(
            statusBarBright = true,
            navigationBarBright = true,
            navigationBarScrollBehind = false
        )

        AutofillInstructionsContentComponent{
            if (VersionUtil.supportsAutofill()) {
                tracker.trackEvent(Tracker.EVENT_AUTOFILL_SETTINGS)
                val success = systemSettingsLauncher.launchForResultSafely(null)
                if (!success) {
                    // According to Sentry reports, this error can happen on Android 8.0
                    // We'll handle it as a 'Skip' action, since that's the best we can do
                    preferences.mayAskForAutofill = false
                    navController.catchNavigate(
                        OnboardingAutofillInstructionsFragmentDirections.actionOnboardingAutofillInstructionsFragmentToHomeFragment()
                    )
                    // .. but let's show an error
                    Toast.makeText(
                        context,
                        R.string.error_autofill_activity,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Preview
@Composable
fun OnboardingAutofillInstructionsScreenPreview() {
    OnboardingAutofillInstructionsScreen(navController = rememberNavController())
}
