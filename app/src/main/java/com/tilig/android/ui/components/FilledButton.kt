package com.tilig.android.ui.components

import androidx.annotation.DrawableRes
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.theme.LightBlue
import com.tilig.android.ui.theme.White

@Composable
fun FilledButton(
    text: String,
    @DrawableRes iconRes: Int,
    modifier: Modifier = Modifier
) {
    Button(modifier = modifier
        .height(52.dp),
        colors = ButtonDefaults.buttonColors(backgroundColor = LightBlue, contentColor = White),
        onClick = { }) {
        Icon(painterResource(id = iconRes), "", modifier = Modifier.size(16.dp))
        Text(text, color = White, modifier = Modifier.padding(4.dp))
    }
}

@Composable
@Preview(showBackground = true)
fun FilledButtonPreview() {
    FilledButton(
        text = "Lorem Ipsum",
        iconRes = R.drawable.ic_google,
        modifier = Modifier
    )
}
