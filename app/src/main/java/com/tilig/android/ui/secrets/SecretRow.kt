package com.tilig.android.ui.secrets

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tilig.android.R
import com.tilig.android.ui.components.TiligImage
import com.tilig.android.ui.components.TiligSpacerHorizontalDefault
import com.tilig.android.ui.theme.*

@Composable
fun SecretRow(
    name: String?,
    description: AnnotatedString?,
    icon: Any?,
    withChevron: Boolean,
    isShared: Boolean,
    onClick: () -> Unit,
) {
    Row(
        Modifier
            .fillMaxWidth()
            .height(64.dp)
            .clickable(onClick = onClick),
        verticalAlignment = Alignment.CenterVertically
    ) {
        TiligImage(
            image = icon,
            contentDescription = "",
            modifier = Modifier
                .padding(0.dp)
                .size(24.dp)
        )

        TiligSpacerHorizontalDefault()

        Column(Modifier.weight(1f)) {
            Text(
                text = name ?: stringResource(id = R.string.untitled_account),
                style = Typography.h3,
                fontSize = 14.sp
            )
            description?.let {
                Text(
                    text = it,
                    style = Typography.subtitle2,
                    color = DisabledGrey,
                    fontSize = 12.sp
                )
            }
        }

        if (isShared) {
            Icon(
                painter = painterResource(id = R.drawable.ic_shared),
                contentDescription = stringResource(id = R.string.shared_item),
                tint = IconColor,
                modifier = Modifier
                    .padding(DefaultPadding)
                    .size(DefaultPadding)
            )
        }
        if (withChevron) {
            Icon(
                painter = painterResource(id = R.drawable.ic_arrow_right),
                "",
                tint = Grey,
                modifier = Modifier
            )
        }
    }
}