package com.tilig.android.ui.secrets.details

import android.util.Log
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tilig.android.BuildConfig
import com.tilig.android.R
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.data.api.ApiHelper
import com.tilig.android.data.models.*
import com.tilig.android.data.models.tilig.*
import com.tilig.android.data.repository.Repository
import com.tilig.android.ui.CryptoModel
import com.tilig.android.ui.accounts.detail.AccountViewModel
import com.tilig.android.ui.secrets.DetailsMode
import com.tilig.android.ui.secrets.details.events.BaseEditEvent
import com.tilig.android.ui.secrets.details.events.ItemDetailsActionEvent
import com.tilig.android.ui.secrets.details.states.ItemDetailsSuccess
import com.tilig.android.ui.secrets.details.states.ItemEditWrapper
import com.tilig.android.ui.secrets.details.states.NewItemDetailsViewModelState
import com.tilig.android.utils.DebugLogger
import com.tilig.android.utils.SharedPrefs
import com.tilig.android.utils.isValidEmail
import com.tilig.crypto.Crypto
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.goterl.lazysodium.utils.Key
import com.goterl.lazysodium.utils.KeyPair

abstract class ItemDetailsViewModel : ViewModel(), DefaultLifecycleObserver, KoinComponent {

    protected val repo: Repository by inject()
    protected val cryptoModel: CryptoModel by inject()
    val tracker: Mixpanel by inject()
    private val prefs: SharedPrefs by inject()

    protected val viewModelState =
        MutableStateFlow(
            NewItemDetailsViewModelState(
                mode = DetailsMode.VIEW,
                isInitialState = true,
                itemEditWrapper = getItemEditWrapper(null),
                secretItemType = getSecretItemType()
            )
        )

    // UI state exposed to the UI
    val uiState = viewModelState
        .map { it.toUiState() }
        .stateIn(
            viewModelScope,
            SharingStarted.Eagerly,
            viewModelState.value.toUiState()
        )

    private val _leaveScreen = MutableSharedFlow<Boolean>()
    val leaveScreen = _leaveScreen.asSharedFlow()

    init {
        // fetch all available folders
        viewModelScope.launch(Dispatchers.IO) {
            val result = repo.getAllFolders()
            viewModelState.update {
                it.copy(
                    foldersList = if (result is ApiSuccessResponse) {
                        result.body.folders
                    } else null
                )
            }
        }
    }

    abstract fun getSecretItemType(): SecretItemType

    abstract fun getItemEditWrapper(item: SecretItem?): ItemEditWrapper

    // Update custom fields in appropriate wrappers after onEditEvent(BaseEditEvent)
    protected abstract fun updateCustomFields(customFields: List<CustomField>?): ItemEditWrapper

    // Update folder in appropriate wrappers after onEditEvent(BaseEditEvent)
    protected abstract fun updateFolder(folder: Folder?): ItemEditWrapper

    protected open fun onEditEvent(event: BaseEditEvent) {
        viewModelState.update {
            it.copy(
                itemEditWrapper = when (event) {
                    is BaseEditEvent.AddCustomField -> {
                        val updatedList: MutableList<CustomField> =
                            it.itemEditWrapper.customFields?.toMutableList() ?: mutableListOf()
                        val defaultName = when (event.fieldType) {
                            ItemFieldType.TEXT -> "Text"
                            ItemFieldType.PASSWORD -> "Password"
                            ItemFieldType.DATE -> "Date"
                            ItemFieldType.TOTP -> "2FA Code"
                            else -> "Secret"
                        } + " " + ((it.itemEditWrapper.customFields?.filter { it.kind == event.fieldType }?.size
                            ?: 0).plus(1)).toString()
                        updatedList.add(CustomField(defaultName, event.fieldType, ""))
                        updateCustomFields(updatedList)
                    }
                    is BaseEditEvent.CustomFieldsChanged -> updateCustomFields(event.fields)
                    is BaseEditEvent.ChangeFolder -> updateFolder(event.newFolder)
                    else -> it.itemEditWrapper
                }
            )
        }
    }

    fun getItemInitially(item: SecretItem) {
        viewModelScope.launch {
            getItem(item = item)
        }
    }

    open fun onActionEvent(event: ItemDetailsActionEvent) {
        when (event) {
            is ItemDetailsActionEvent.DeleteItem -> deleteItem()
            is ItemDetailsActionEvent.DebugItem -> debugItem()
            is ItemDetailsActionEvent.ConfirmDiscardChanges -> enterViewMode()
            is ItemDetailsActionEvent.Edit -> onEditEvent(event = event.event)
            is ItemDetailsActionEvent.EnterEditMode -> enterEditMode()
            is ItemDetailsActionEvent.CancelChangesDialog -> updateConfirmChangesDialog(false)
            is ItemDetailsActionEvent.OnGoBack -> {
                when (viewModelState.value.mode) {
                    DetailsMode.VIEW -> {
                        viewModelScope.launch {
                            _leaveScreen.emit(true)
                        }
                    }
                    DetailsMode.EDIT -> {
                        if (viewModelState.value.hasDataChanged()) {
                            updateConfirmChangesDialog(true)
                        } else {
                            enterViewMode()
                        }
                    }
                }
            }
            is ItemDetailsActionEvent.SaveChanges -> {
                if (viewModelState.value.hasDataChanged()) {
                    saveChanges()
                } else {
                    enterViewMode()
                }
            }
            is ItemDetailsActionEvent.Share -> {
                shareItem(event.email)
            }
            is ItemDetailsActionEvent.ShareRevoke -> {
                revokeShareItem(event.folderId, event.memberId)
            }
        }
    }

    protected suspend fun getItem(item: SecretItem) {
        if (item.isTestDriveItem()) {
            viewModelState.update {
                it.copy(
                    isLoading = false,
                    item = item,
                    isInitialState = false,
                    errorMessage = null,
                    shareErrorMessage = null
                )
            }
            return
        }
        viewModelState.update {
            it.copy(
                isLoading = true,
                item = item,
                errorMessage = null,
                shareErrorMessage = null
            )
        }

        // Force show loading state at least for some milliseconds
        delay(200L)

        cryptoModel.withInitializedCrypto { crypto ->
            // Note we're doing this in the IO dispatcher, so the API call is off the UI thread,
            // BUT ALSO THE DECRYPTION! Muy importanto.
            viewModelScope.launch(Dispatchers.IO) {
                val encryptedAccount = repo.getSecretItem(item.id!!)
                val decryptedAccount =
                    (encryptedAccount as? ApiSuccessResponse)?.body?.item?.decrypt(
                        repo.jsonConverter,
                        crypto,
                        includingDetails = true
                    )

                // Wrap it back into a response
                val decryptedResponse = ApiResponse.createCloneWithNewBody(
                    encryptedAccount,
                    decryptedAccount
                )

                viewModelState.update {
                    it.copy(
                        item = (decryptedResponse as? ApiSuccessResponse)?.body,
                        errorMessage = UiError((decryptedResponse as? ApiErrorResponse)?.errorMessage),
                        shareErrorMessage = null,
                        isLoading = false,
                        isInitialState = false,
                        folder = (decryptedResponse as? ApiSuccessResponse)?.body?.folder
                    )
                }
            }
        }
    }

    /**
     * Update item with item wrapper details
     */
    abstract fun buildItemWithUpdatedItems(): SecretItem?

    /**
     * If the folder has changed change folder for updated secret item. If folder is null, remove item from the folder.
     * If the folder has not changed we just ignore this step
     */
    protected fun updateFolderField(secretItem: SecretItem, newFolder: Folder?): SecretItem{
        if (secretItem.folder != newFolder) {
            if (newFolder != null) {
                cryptoModel.withInitializedCrypto { crypto ->
                    secretItem.decryptedDek = secretItem.decryptedDek ?: crypto.generateDek()

                    // User has a private key → used to decrypt the private KEK
                    val usersKeypair = crypto.getKeyPairs().keypair!!
                    val privateKek = crypto.open(
                        cipherText = newFolder.myEncryptedPrivateKey,
                        keyPair = usersKeypair
                    )
                    val publicKek = crypto.base64decode(newFolder.publicKey)

                    // Private key of the KEK is used to decrypt the DEK
                    val keyPair = KeyPair(
                        Key.fromBytes(publicKek),
                        Key.fromBytes(privateKek)
                    )

                    // Re-encrypt DEK using the new folder KEK for the current user
                    secretItem.encryptedFolderKey = crypto.encryptWithKeypair(
                        messageBytes = secretItem.decryptedDek!!,
                        keyPair = keyPair
                    )
                }
            } else {
                secretItem.encryptedFolderKey = null
            }
            secretItem.folder = newFolder
        }
        return secretItem
    }

    private fun saveChanges() {
        viewModelState.update {
            it.copy(isLoading = true)
        }
        buildItemWithUpdatedItems()?.let {
            updateItem(it) {
                enterViewMode()
            }
        }
    }

    private fun deleteItem() {
        val id = (uiState.value as? ItemDetailsSuccess)?.item?.id ?: return
        if (!viewModelState.value.isLoading) {
            viewModelState.update {
                it.copy(isLoading = true, errorMessage = null, shareErrorMessage = null)
            }
            viewModelScope.launch(Dispatchers.IO) {
                val deleteResult = repo.deleteSecretItem(id)
                if (deleteResult is ApiSuccessResponse) {
                    trackDeleteItem()
                    viewModelScope.launch {
                        _leaveScreen.emit(true)
                    }
                } else {
                    viewModelState.update {
                        it.copy(
                            errorMessage = UiError(
                                (deleteResult as? ApiErrorResponse)?.errorMessage,
                                R.string.something_went_wrong
                            ),
                            shareErrorMessage = null
                        )
                    }
                }
            }
        }
    }

    protected abstract fun trackDeleteItem()

    private fun debugItem() {
        val uuid = viewModelState.value.item?.id
        val logger = DebugLogger(logTimestamps = false)
        logger.collect("Fetching account with ID $uuid\n\n")

        viewModelState.update {
            it.copy(
                isLoading = true, errorMessage = null, shareErrorMessage = null
            )
        }

        val apiClient = ApiHelper.createTemporaryDebugApiClient(logger)

        uuid?.let {
            apiClient.getSecretItemCall(uuid).enqueue(object : Callback<EncryptedItemResponse> {
                private fun shareLogs() {
                    viewModelState.update {
                        it.copy(
                            isLoading = false,
                            debugMessage = logger.toString(),
                            errorMessage = null,
                            shareErrorMessage = null
                        )
                    }
                }

                override fun onResponse(
                    call: Call<EncryptedItemResponse>,
                    response: Response<EncryptedItemResponse>
                ) {
                    cryptoModel.withInitializedCrypto { crypto ->
                        val keys = crypto.getKeyPairs()
                        // Log key state
                        logger.collect("\n\nHas legacy keys? private = ${keys.legacy?.private != null}, pub = ${keys.legacy?.public != null} ")
                        logger.collect("Has new keys? private = ${keys.keypair?.secretKey != null}, pub = ${keys.keypair?.publicKey != null} ")
                        // Decrypt
                        response.body()?.item?.decrypt(
                            repo.jsonConverter,
                            crypto,
                            includingDetails = true,
                            debugLogger = logger
                        )
                        // Show result
                        shareLogs()
                    }
                }

                override fun onFailure(call: Call<EncryptedItemResponse>, t: Throwable) {
                    // Show error
                    logger.collect("Error: ${t.message}")
                    shareLogs()
                }
            })
        }
    }

    private fun enterViewMode() {
        viewModelState.update {
            it.copy(
                isLoading = false,
                mode = DetailsMode.VIEW,
                displayConfirmChangesDialog = false,
                itemEditWrapper = getItemEditWrapper(null)
            )
        }
    }

    private fun enterEditMode() {
        viewModelState.update {
            it.copy(
                mode = DetailsMode.EDIT,
                itemEditWrapper = getItemEditWrapper(it.item)
            )
        }
    }

    private fun updateConfirmChangesDialog(display: Boolean) {
        viewModelState.update {
            it.copy(
                displayConfirmChangesDialog = display
            )
        }
    }

    private fun shareItem(email: String) {
        cryptoModel.withInitializedCrypto { crypto ->
            viewModelState.value.item?.let { item ->
                if (item.id != null) {
                    if (item.isTestDriveItem()) {
                        // no-op, sharing is hidden for test drive items
                    } else if (!email.isValidEmail()) {
                        viewModelState.update {
                            val update = it.copy(
                                isLoading = false,
                                shareErrorMessage = UiError(
                                    null,
                                    R.string.error_share_invalid_email
                                ),
                                errorMessage = null
                            )
                            update.folder = null
                            update
                        }
                    } else if (prefs.email == email) {
                        viewModelState.update {
                            val update = it.copy(
                                isLoading = false,
                                shareErrorMessage = UiError(
                                    null,
                                    R.string.error_share_cannot_share_with_yourself
                                ),
                                errorMessage = null,
                            )
                            update.folder = null
                            update
                        }
                    } else if (item.containsMember(email)) {
                        viewModelState.update {
                            val update = it.copy(
                                isLoading = false,
                                shareErrorMessage = UiError(
                                    null,
                                    R.string.error_share_already_shared
                                ),
                                errorMessage = null,
                            )
                            update.item = item
                            update.folder = item.folder
                            update
                        }
                    } else {
                        viewModelScope.launch(Dispatchers.IO) {
                            val pubKey =
                                (repo.getPublicKey(email = email) as? ApiSuccessResponse)?.body?.user

                            if (item.isSharedFolderItem()) {
                                val folder = (viewModelState.value.folder ?: item.folder)
                                folder?.let {
                                    addMemberToFolder(
                                        crypto = crypto,
                                        folder = it,
                                        theirPublicKey = pubKey?.publicKey,
                                        theirUserId = pubKey?.id,
                                        theirEmail = email
                                    )
                                }
                            } else {
                                upgradeToFolder(
                                    crypto = crypto,
                                    item = item,
                                    theirPublicKey = pubKey?.publicKey,
                                    theirUserId = pubKey?.id,
                                    theirEmail = email
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    private suspend fun upgradeToFolder(
        crypto: Crypto,
        item: SecretItem,
        theirPublicKey: String?,
        theirUserId: String?,
        theirEmail: String
    ) {
        // TODO assert not null:
        //      prefs.mixpanelUserId
        //      account.decryptedDek

        val usersKeypair = crypto.getKeyPairs().keypair!!

        // Generate new keypair (kek)
        val folderKek = crypto.getNewGeneratedKey()

        // Re-encrypt DEK using the new folder KEK for the current user
        item.encryptedFolderKey = crypto.encryptWithKeypair(
            messageBytes = item.decryptedDek!!,
            keyPair = folderKek
        )

        // Encrypt folderKEK's private key for each user (using each user's public key)
        val privateKEK = folderKek.secretKey
        val ourEncryptedPrivateKEK = crypto.seal(
            messageBytes = privateKEK.asBytes,
            publicKeyRecipient = usersKeypair.publicKey.asBytes
        )
        val theirEncryptedPrivateKEK = theirPublicKey?.let {
            crypto.seal(
                messageBytes = privateKEK.asBytes,
                publicKeyRecipient = crypto.base64decode(it)
            )
        }

        // Invite the other user
        val response = repo.createFolder(
            itemId = item.id!!,
            name = item.overview.name ?: "",
            publicKek = crypto.base64encode(folderKek.publicKey.asBytes),
            ourUserId = prefs.mixpanelUserId!!,
            ourEncryptedKek = ourEncryptedPrivateKEK!!,
            theirEncryptedKek = theirEncryptedPrivateKEK,
            theirUserId = theirUserId,
            theirEmail = theirEmail,
            encryptedFolderDek = item.encryptedFolderKey!!
        )

        if (response is ApiSuccessResponse) {
            if (BuildConfig.DEBUG && AccountViewModel.AUTO_ACCEPT_INVITES_ENABLED) {
                // In debug builds, we can automatically accept outgoing invites
                (response.body as? FolderResponse)?.folder?.folderMemberships?.forEach { membership ->
                    if (membership.tempAcceptanceToken != null && membership.isMe != true) {
                        val acceptResponse = membership.id?.let {
                            repo.instantAcceptInvite(
                                folderId = response.body.folder.id,
                                membershipId = it,
                                token = membership.tempAcceptanceToken
                            )
                        }
                        Log.d("invite", acceptResponse.toString())
                    }
                }
            }

            viewModelState.update {
                it.copy(
                    isLoading = false,
                    errorMessage = null,
                    shareErrorMessage = null,
                    folder = response.body.folder
                )
            }
        } else {
            viewModelState.update {
                val update = it.copy(
                    isLoading = false,
                    shareErrorMessage = UiError(null, R.string.something_went_wrong),
                    errorMessage = null,
                )
                update.folder = null
                update
            }
        }
    }

    private suspend fun addMemberToFolder(
        crypto: Crypto,
        folder: Folder,
        theirPublicKey: String?,
        theirUserId: String?,
        theirEmail: String
    ) {
        val usersKeypair = crypto.getKeyPairs().keypair!!
        val decryptedPrivateKek = crypto.open(
            cipherText = folder.myEncryptedPrivateKey,
            keyPair = usersKeypair
        )

        // encrypt the items private KEK with the public key of user B
        val theirEncryptedPrivateKEK = theirPublicKey?.let {
            crypto.seal(
                messageBytes = decryptedPrivateKek!!,
                publicKeyRecipient = crypto.base64decode(it)
            )
        }

        // call /addmember with item id, shareeEncryptedPrivateKEK, theirEmail
        val response = repo.addUserToFolder(
            folderId = folder.id,
            theirEncryptedKek = theirEncryptedPrivateKEK,
            theirUserId = theirUserId,
            theirEmail = theirEmail
        )
        if (response is ApiSuccessResponse) {

            (response.body as? FolderMembershipResponse)?.getMembership()?.let { membership ->

                // In debug builds, we can automatically accept outgoing invites
                if (BuildConfig.DEBUG && AccountViewModel.AUTO_ACCEPT_INVITES_ENABLED && membership.tempAcceptanceToken != null) {
                    val acceptResponse = membership.id.let {
                        repo.instantAcceptInvite(
                            folderId = folder.id,
                            membershipId = it,
                            token = membership.tempAcceptanceToken
                        )
                    }
                    Log.d("invite", acceptResponse.toString())
                }

                viewModelState.update {
                    val update = it.copy(
                        isLoading = false,
                        shareErrorMessage = null,
                        errorMessage = null,
                    )
                    // non tilig user
                    if (membership.user.id == null) {
                        val memberships =
                            (it.folder?.folderInvitations ?: emptyArray()) + membership
                        update.folder = it.folder?.copy(
                            folderInvitations = memberships
                        )
                    } else {
                        val memberships =
                            (it.folder?.folderMemberships ?: emptyArray()) + membership
                        update.folder = it.folder?.copy(
                            folderMemberships = memberships
                        )
                    }
                    update
                }
            }
        } else {
            viewModelState.update {
                it.copy(
                    isLoading = false,
                    shareErrorMessage = UiError(null, R.string.something_went_wrong),
                    errorMessage = null,
                )
            }
        }
    }

    private fun revokeShareItem(itemId: String, membershipId: String) {
        viewModelState.value.item?.let { item ->
            if (item.id != null) {
                if (item.isTestDriveItem()) {
                    // no-op, sharing is hidden for test drive items
                } else {
                    viewModelScope.launch(Dispatchers.IO) {
                        val response = repo.revokeUser(
                            folderId = itemId,
                            membershipId = membershipId
                        )
                        if (response is ApiSuccessResponse) {
                            viewModelState.update {
                                val update = it.copy(
                                    isLoading = false,
                                    shareErrorMessage = null,
                                    errorMessage = null,
                                )
                                update.folder = (it.folder ?: item.folder)?.let { f ->
                                    f.copy(
                                        // Manually refresh the UI without reloading, by removing the membership from the object
                                        folderMemberships = f.folderMemberships
                                            ?.filter { membership -> membership.id != membershipId }
                                            ?.toTypedArray(),
                                        folderInvitations = f.folderInvitations
                                            ?.filter { membership -> membership.id != membershipId }
                                            ?.toTypedArray(),
                                    )
                                }
                                update
                            }
                        } else {
                            viewModelState.update {
                                it.copy(
                                    isLoading = false,
                                    shareErrorMessage = UiError(
                                        null,
                                        R.string.something_went_wrong
                                    ),
                                    errorMessage = null,
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    protected fun updateItem(item: SecretItem, doAfterUpdate: (SecretItem) -> Unit) {
        cryptoModel.withInitializedCrypto { crypto ->
            // Encrypt before sending
            val encryptedItem = item.encrypt(repo.jsonConverter, crypto)
            viewModelScope.launch(Dispatchers.IO) {
                val updateResult = repo.updateSecretItem(encryptedItem)
                val decryptedItem =
                    (updateResult as? ApiSuccessResponse)?.body?.item?.decrypt(
                        repo.jsonConverter,
                        crypto,
                        includingDetails = true
                    )

                viewModelState.update {
                    it.copy(
                        isLoading = false,
                        errorMessage = UiError((updateResult as? ApiErrorResponse)?.errorMessage),
                        shareErrorMessage = null,
                        item = decryptedItem,
                        folder = decryptedItem?.folder
                    )
                }
                if (updateResult is ApiSuccessResponse) {
                    doAfterUpdate(decryptedItem ?: item)
                }
            }
        }
    }
}