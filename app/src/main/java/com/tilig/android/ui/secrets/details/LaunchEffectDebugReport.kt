package com.tilig.android.ui.secrets.details

import android.content.Intent
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.platform.LocalContext

@Composable
fun LaunchEffectDebugReport(debugMessage: String?) {
    val context = LocalContext.current

    debugMessage?.let {message ->
        // Open share screen so testers can share debug info
        LaunchedEffect(message) {
            val sendIntent = Intent(Intent.ACTION_SEND).apply {
                putExtra(Intent.EXTRA_TEXT, message)
                type = "text/plain"
            }
            context.startActivity(
                Intent.createChooser(
                    sendIntent,
                    "Please send to a developer"
                )
            )
        }
    }
}