package com.tilig.android.ui.secrets.details.states

import com.tilig.android.R
import com.tilig.android.data.models.SecretItemType
import com.tilig.android.data.models.UiError
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.Folders
import com.tilig.android.data.models.tilig.SecretItem
import com.tilig.android.ui.secrets.DetailsMode

data class NewItemDetailsViewModelState(
    val isInitialState: Boolean,
    var item: SecretItem? = null,
    val itemEditWrapper: ItemEditWrapper,
    val displayConfirmChangesDialog: Boolean = false,
    var folder: Folder? = null,
    val isLoading: Boolean = false,
    val mode: DetailsMode = DetailsMode.VIEW,
    val errorMessage: UiError? = null,
    val shareErrorMessage: UiError? = null,
    val debugMessage: String? = null,
    val secretItemType: SecretItemType?,
    val foldersList: Folders? = null
) {

    fun toUiState(): ItemDetailsUiState =
        when {
            debugMessage != null -> ItemDetailsUiState.ItemError(
                isLoading = false,
                errorMessage = UiError(
                    "Please share the log with a developer",
                    R.string.error_loading_account
                ),
                shareErrorMessage = shareErrorMessage,
                debugMessage = debugMessage
            )
            item == null -> ItemDetailsUiState.ItemError(
                isLoading = isLoading,
                errorMessage = UiError(
                    errorMessage?.message,
                    when (secretItemType) {
                        SecretItemType.ACCOUNT -> R.string.error_loading_account
                        SecretItemType.NOTE -> R.string.error_loading_note
                        SecretItemType.CREDIT_CARD -> R.string.error_loading_credit_card
                        SecretItemType.WIFI -> R.string.error_loading_wifi
                        else -> R.string.error_loading_item
                    }
                ),
                shareErrorMessage = shareErrorMessage,
            )
            mode == DetailsMode.EDIT -> {
                ItemDetailsUiState.ItemEdit(
                    isLoading = isLoading,
                    item = item!!,
                    itemEditWrapper = itemEditWrapper,
                    displayConfirmChangesDialog = displayConfirmChangesDialog,
                    errorMessage = errorMessage,
                    shareErrorMessage = shareErrorMessage,
                    foldersList = foldersList
                )
            }
            else -> {
                ItemDetailsUiState.ItemView(
                    isLoading = isLoading,
                    item = item!!,
                    folder = folder ?: item!!.folder,
                    isInInitialState = isInitialState,
                    errorMessage = errorMessage,
                    shareErrorMessage = shareErrorMessage,
                    foldersList = foldersList
                )
            }
        }

    fun hasDataChanged(): Boolean = item?.let {
        if (mode == DetailsMode.VIEW) {
            return false
        }
        return itemEditWrapper.hadDataChanged(it)
    } ?: false
}