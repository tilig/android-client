package com.tilig.android.ui.notes.details

import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.SecretItemType
import com.tilig.android.data.models.tilig.CustomField
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.Note
import com.tilig.android.data.models.tilig.SecretItem
import com.tilig.android.ui.secrets.details.ItemDetailsViewModel
import com.tilig.android.ui.secrets.details.events.BaseEditEvent
import com.tilig.android.ui.secrets.details.states.ItemEditWrapper
import com.tilig.android.ui.secrets.details.states.ItemDetailsUiState
import kotlinx.coroutines.flow.update


data class NoteEditWrapper(
    val name: String? = null,
    val content: String? = null,
    override val customFields: List<CustomField>? = null,
    override val selectedFolder: Folder? = null
) : ItemEditWrapper {
    constructor(note: Note) : this(
        name = note.name,
        content = note.content,
        customFields = note.details.customFields,
        selectedFolder = note.folder
    )

    override fun hadDataChanged(original: SecretItem): Boolean {
        return (original as Note).name != name || original.content != content ||
                original.folder != selectedFolder ||
                original.details.customFields != customFields
    }

    override fun getItemName(): String? = name
}

sealed interface NoteEditEvents : BaseEditEvent {
    data class NameChanged(val name: String) : NoteEditEvents
    data class ContentChanged(val content: String) : NoteEditEvents
}

class NoteViewModel : ItemDetailsViewModel() {

    override fun getSecretItemType(): SecretItemType = SecretItemType.NOTE

    override fun getItemEditWrapper(item: SecretItem?): ItemEditWrapper {
        return if (item != null) NoteEditWrapper(item as Note) else NoteEditWrapper()
    }

    override fun updateCustomFields(customFields: List<CustomField>?): ItemEditWrapper {
        return (viewModelState.value.itemEditWrapper as NoteEditWrapper).copy(
            customFields = customFields
        )
    }

    override fun updateFolder(folder: Folder?): ItemEditWrapper {
        return (viewModelState.value.itemEditWrapper as NoteEditWrapper).copy(
            selectedFolder = folder
        )
    }

    override fun onEditEvent(event: BaseEditEvent) {
        if (event is NoteEditEvents) {
            viewModelState.update {
                it.copy(
                    itemEditWrapper =
                    when (event) {
                        is NoteEditEvents.ContentChanged -> {
                            (it.itemEditWrapper as NoteEditWrapper).copy(
                                content = event.content
                            )
                        }
                        is NoteEditEvents.NameChanged -> {
                            (it.itemEditWrapper as NoteEditWrapper).copy(
                                name = event.name
                            )
                        }
                    }
                )
            }
        } else {
            super.onEditEvent(event)
        }
    }

    override fun buildItemWithUpdatedItems(): SecretItem? {
        val currentState = uiState.value
        if (currentState is ItemDetailsUiState.ItemEdit) {
            val editable = currentState.itemEditWrapper as NoteEditWrapper
            return (currentState.item as Note).apply {
                this.name = editable.name
                this.content = editable.content
                // ignore fields with empty names
                editable.customFields?.filter { it.name.isNotEmpty() }?.let {
                    this.details.customFields = it.toMutableList()
                }
                updateFolderField(this, editable.selectedFolder)
            }
        }
        return null
    }

    override fun trackDeleteItem() {
        tracker.trackEvent(Tracker.EVENT_NOTE_DELETED)
    }
}