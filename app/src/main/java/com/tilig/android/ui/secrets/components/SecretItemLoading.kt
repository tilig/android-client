package com.tilig.android.ui.secrets.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.tilig.android.data.models.tilig.*
import com.tilig.android.ui.components.AnimatedItem
import com.tilig.android.ui.components.AnimatedPulseContent
import com.tilig.android.ui.components.AnimatedPulseField
import com.tilig.android.ui.components.TiligSpacerVerticalDefault
import com.tilig.android.ui.theme.DefaultPadding
import com.tilig.android.ui.theme.MediumCornerRadius
import com.tilig.android.ui.theme.White

@Composable
fun SecretItemLoading(modifier: Modifier = Modifier, secretItem: SecretItem) {
    val itemsList = buildLoadingItemList(secretItem)
    Column(
        modifier = modifier
            .fillMaxWidth()
            .padding(DefaultPadding)
    ) {
        itemsList.forEach { item ->
            when (item) {
                is LoadingType.Box -> {
                    if (item.dividerAbove) {
                        TiligSpacerVerticalDefault()
                    }
                    AnimatedPulseContent {
                        Box(
                            modifier = modifier.background(
                                color = White,
                                shape = RoundedCornerShape(MediumCornerRadius)
                            )
                        ) {
                            Column(
                                modifier = Modifier.padding(DefaultPadding),
                            ) {
                                for (animatedItemIndex in 0 until item.itemsAmount) {
                                    AnimatedItem(
                                        modifier = Modifier.heightIn(min = 48.dp),
                                        color = it
                                    )
                                    // if not last add a divider
                                    if (animatedItemIndex != item.itemsAmount - 1) {
                                        Divider(
                                            modifier = Modifier
                                                .fillMaxWidth()
                                                .padding(vertical = DefaultPadding)
                                        )
                                    }
                                }
                            }

                        }
                    }
                }
                LoadingType.Field -> {
                    TiligSpacerVerticalDefault()
                    AnimatedPulseField()
                }
            }
        }
    }
}

// Type of animated item: AnimatedPulseContent with AnimatedItem or AnimatedPulseField
private sealed class LoadingType() {
    // amount of AnimatedItems inside
    data class Box(val itemsAmount: Int, val dividerAbove: Boolean) : LoadingType()
    object Field : LoadingType()
}

private fun buildLoadingItemList(secretItem: SecretItem): List<LoadingType> {
    val list = mutableListOf<LoadingType>()
    when (secretItem) {
        is Account -> {
            list.add(LoadingType.Box(2, false))
            list.add(LoadingType.Field)
            if (secretItem.hasOtp()) {
                list.add(LoadingType.Field)
            }
            if (secretItem.notes.isNotEmpty()) {
                list.add(LoadingType.Field)
            }

        }
        is Note -> {
            list.add(LoadingType.Box(1, true))
        }
        is CreditCard -> {
            list.add(LoadingType.Box(if (!secretItem.securityCode.isNullOrEmpty()) 4 else 3, true))
            if (!secretItem.pinCode.isNullOrEmpty()) {
                list.add(LoadingType.Field)
            }
            if (!secretItem.extraInfo.isNullOrEmpty()) {
                list.add(LoadingType.Field)
            }
        }
        is WifiPassword -> {
            if (!secretItem.networkName.isNullOrEmpty()) {
                list.add(LoadingType.Field)
            }
            if (!secretItem.password.isNullOrEmpty()) {
                list.add(LoadingType.Field)
            }
            if (!secretItem.extraInfo.isNullOrEmpty()) {
                list.add(LoadingType.Field)
            }
        }
        else -> {
            list.add(LoadingType.Box(1, true))
        }
    }
    return list
}