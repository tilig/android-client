package com.tilig.android.ui.accounts.addaccount.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import coil.decode.SvgDecoder
import coil.request.ImageRequest
import com.tilig.android.R
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.addaccount.SuggestedBrand
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.ui.modifiers.dropShadow
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.UITestTags

@Composable
fun SuggestedBrands(
    tracker: Mixpanel,
    modifier: Modifier = Modifier,
    onAccountPrefill: (Account) -> Unit
) {
    val accountNames = stringArrayResource(R.array.first_accounts_names)
    val accountUrls = stringArrayResource(R.array.first_accounts_urls)
    val accountIcons =
        LocalContext.current.resources.obtainTypedArray(R.array.first_accounts_icon_res)
    val suggestedBrands = mutableListOf<SuggestedBrand>()
    accountNames.forEachIndexed { index, name ->
        suggestedBrands.add(
            SuggestedBrand(
                name,
                accountUrls[index],
                accountIcons.getResourceId(index, R.drawable.ic_website)
            )
        )
    }
    accountIcons.recycle()

    Column(
        modifier
            .testTag(UITestTags.suggestedBrandsScreenTestTag)
            .fillMaxSize()
            .background(BackgroundGrey)
    ) {
        Text(
            modifier = Modifier
                .padding(start = DefaultPadding, bottom = HalfPadding),
            text = stringResource(id = R.string.pick_a_suggestion),
            style = StyleStolzRegularDarkBlue12.copy(color = DimmedBlue)
        )
        LazyVerticalGrid(
            modifier = Modifier
                .testTag(UITestTags.suggestedBrandsListTestTag)
                .padding(
                    horizontal = DefaultPadding
                )
                .weight(1f)
                .fillMaxHeight(),
            columns = GridCells.Fixed(3),
            verticalArrangement = Arrangement.spacedBy(16.dp),
            horizontalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            items(suggestedBrands.size) { index ->
                SuggestedBrandItem(suggestedBrands[index]) {
                    onAccountPrefill(
                        Account.createBasic(
                            name = suggestedBrands[index].name,
                            website = suggestedBrands[index].url
                        )
                    )
                }
            }
        }
    }
}

@Composable
private fun SuggestedBrandItem(brand: SuggestedBrand, onClick: () -> Unit) {
    val modifier = Modifier
        .height(80.dp)
        .fillMaxWidth()
        .dropShadow()

    OutlinedButton(
        modifier = modifier,
        onClick = onClick,
        shape = RoundedCornerShape(8.dp),
        border = BorderStroke(1.dp, Grey),
        colors = ButtonDefaults.outlinedButtonColors(
            backgroundColor = DefaultBackground
        ),
    ) {

        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            val painter = rememberAsyncImagePainter(
                ImageRequest.Builder(LocalContext.current).data(data = brand.icon)
                    .decoderFactory(SvgDecoder.Factory()).build()
            )
            Image(
                painter = painter,
                contentDescription = brand.name,
                modifier = Modifier
                    .size(32.dp).clip(DefaultAccountClipShape)
            )
            Spacer(modifier = Modifier.height(4.dp))
            Text(
                text = brand.name,
                style = StyleStolzRegularDarkBlue12,
                textAlign = TextAlign.Center
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun BrandItemPreview() {
    SuggestedBrandItem(
        brand = SuggestedBrand(
            name = "Lorem ipsum",
            icon = R.drawable.ic_acorn_placeholder,
            url = "https://google.com"
        ),
        onClick = {}
    )
}
