package com.tilig.android.ui.settings.components

import android.content.Intent
import android.net.Uri
import androidx.annotation.StringRes
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForwardIos
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.tilig.android.MainRoute
import com.tilig.android.R
import com.tilig.android.ui.components.FieldTitle
import com.tilig.android.ui.theme.*
import org.koin.androidx.compose.get

@Composable
fun YourDataSection(navController: NavController) {
    val context = LocalContext.current
    val exportPassLink = stringResource(id = R.string.export_passwords_link)

    FieldTitle(
        title = stringResource(id = R.string.label_your_data),
        titlePadding = 8.dp,
        style = StyleSpoofMediumDarkBlue20
    )
    Column(
        modifier = Modifier
            .background(color = White, shape = RoundedCornerShape(MediumCornerRadius))
            .fillMaxWidth(),
    ) {
        YourDataRow(
            titleId = R.string.import_passwords,
            onClick = {
                navController.navigate(MainRoute.ImportPasswords.route)
            }
        )
        Divider(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = DefaultPadding)
        )
        YourDataRow(
            titleId = R.string.export_passwords,
            onClick = {
                context.startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(exportPassLink)
                    )
                )
            }
        )
    }
}

@Composable
private fun YourDataRow(
    @StringRes titleId: Int,
    onClick: () -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onClick.invoke() }
            .padding(16.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            modifier = Modifier
                .padding(end = 16.dp),
            text = stringResource(id = titleId),
            style = StyleStolzRegularDarkBlue14,
            color = DefaultTextColor,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )
        Icon(
            modifier = Modifier.size(16.dp),
            imageVector = Icons.Filled.ArrowForwardIos,
            contentDescription = "",
            tint = SettingsArrowColor
        )
    }
}

@Composable
@Preview
fun YourDataSectionPreview() {
    YourDataSection(get())
}
