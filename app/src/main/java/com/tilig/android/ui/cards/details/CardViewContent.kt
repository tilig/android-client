package com.tilig.android.ui.cards.details

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.UiError
import com.tilig.android.data.models.tilig.CreditCard
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.Membership
import com.tilig.android.ui.accounts.detail.components.ShareLinkComponent
import com.tilig.android.ui.accounts.detail.components.ShareLinkComponentCallback
import com.tilig.android.ui.components.TiligSpacerVerticalDefault
import com.tilig.android.ui.secrets.components.*
import com.tilig.android.ui.secrets.details.BottomSheetType
import com.tilig.android.ui.secrets.details.ItemDetailsCallback
import com.tilig.android.ui.secrets.details.events.ItemDetailsActionEvent
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.CardUtils
import com.tilig.android.utils.VersionUtil
import com.tilig.android.utils.copyToClipboard

@Composable
fun CardViewContent(
    card: CreditCard,
    folder: Folder?,
    isLoading: Boolean,
    shareErrorMessage: UiError? = null,
    tracker: Tracker,
    callBack: ItemDetailsCallback
) {
    val context = LocalContext.current
    val cardNumber =
        remember { CardUtils.formatCardNumber(AnnotatedString(card.cardNumber ?: "")).text.text }
    val cardHolder = remember { card.holderName }
    val cardExpire = remember { card.expireDate }
    val cardSecretCode = remember { card.securityCode }
    val cardPinCode = remember { card.pinCode }
    val extraInfo = remember { card.extraInfo }

    val cardNumberCopiedMessage = stringResource(id = R.string.copied_card_number)
    val cardHolderCopiedMessage = stringResource(id = R.string.copied_card_holder)
    val cardExpireCopiedMessage = stringResource(id = R.string.copied_card_expire)
    val cardSecurityCopiedMessage = stringResource(id = R.string.copied_card_security_code)
    val cardPinCopiedMessage = stringResource(id = R.string.copied_card_pin_code)
    val cardZipCopiedMessage = stringResource(id = R.string.copied_card_zip_code)
    val extraInfoCopiedMessage = stringResource(id = R.string.copied_extra_info)

    val copyAction: (String?, String) -> Unit = { message, copyMessage ->
        if (message?.copyToClipboard(context) == true
            && !VersionUtil.showsSystemMessageOnCopyToClipboard()
        ) {
            callBack.onShowSnackbarMessage(copyMessage)
        }
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(DefaultPadding)
    ) {
        TiligSpacerVerticalDefault()
        Box(
            modifier = Modifier.background(
                color = White,
                shape = RoundedCornerShape(MediumCornerRadius)
            )
        ) {
            Column(
                modifier = Modifier.padding(
                    start = DefaultPadding,
                    end = DefaultPadding,
                    top = DefaultPadding,
                    bottom = HalfPadding
                )
            ) {
                TitledContent(
                    modifier = Modifier.fillMaxWidth(),
                    title = stringResource(id = R.string.label_card_number),
                    tracker = tracker,
                    event = Tracker.EVENT_CARD_NUMBER_COPIED,
                    onClick = { copyAction(cardNumber, cardNumberCopiedMessage) }
                ) {
                    DetailsTextField(
                        value = cardNumber.ifEmpty { "-" },
                        showRevealButton = cardNumber.isNotEmpty(),
                        showContentInitially = false,
                        transformation = if (cardNumber.isNotEmpty()) CardUtils.CardNumberTransformation(
                            CardUtils.BULLET
                        ) else VisualTransformation.None
                    )
                }

                Divider(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = DefaultPadding)
                )

                TitledContent(
                    modifier = Modifier.fillMaxWidth(),
                    title = stringResource(id = R.string.label_card_holder_name),
                    tracker = tracker,
                    event = Tracker.EVENT_CARD_HOLDER_COPIED,
                    onClick = { copyAction(cardHolder, cardHolderCopiedMessage) }
                ) {
                    DetailsTextField(
                        value = if (!cardHolder.isNullOrEmpty()) cardHolder else "-"
                    )
                }

                Divider(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = DefaultPadding)
                )

                TitledContent(
                    modifier = Modifier.fillMaxWidth(),
                    title = stringResource(id = R.string.label_expire_date),
                    tracker = tracker,
                    event = Tracker.EVENT_CARD_EXPIRE_COPIED,
                    onClick = { copyAction(cardExpire, cardExpireCopiedMessage) }
                ) {
                    DetailsTextField(
                        value = if (!cardExpire.isNullOrEmpty()) cardExpire else "-"
                    )
                }

                if (!cardSecretCode.isNullOrEmpty()) {
                    Divider(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(vertical = DefaultPadding)
                    )
                    TitledContent(
                        modifier = Modifier.fillMaxWidth(),
                        title = stringResource(id = R.string.label_secret_code),
                        tracker = tracker,
                        event = Tracker.EVENT_CARD_SECRET_COPIED,
                        onClick = { copyAction(cardSecretCode, cardSecurityCopiedMessage) }
                    ) {
                        DetailsTextField(
                            value = cardSecretCode,
                            showRevealButton = true,
                            showContentInitially = false
                        )
                    }
                }
            }
        }

        if (!cardPinCode.isNullOrEmpty()) {
            TiligSpacerVerticalDefault()
            BoxWithTitledContent(
                modifier = Modifier.fillMaxWidth(),
                titleStr = stringResource(id = R.string.label_pin_code),
                tracker = tracker,
                event = Tracker.EVENT_CARD_PIN_CODE_COPIED,
                onClick = { copyAction(cardPinCode, cardPinCopiedMessage) }
            ) {
                DetailsTextField(
                    value = cardPinCode,
                    showRevealButton = true,
                    showContentInitially = false
                )
            }
        }

        if (!card.zipCode.isNullOrEmpty()) {
            TiligSpacerVerticalDefault()
            BoxWithTitledContent(
                modifier = Modifier.fillMaxWidth(),
                titleStr = stringResource(id = R.string.label_zip_code),
                tracker = tracker,
                event = Tracker.EVENT_CARD_ZIP_CODE_COPIED,
                onClick = { copyAction(card.zipCode, cardZipCopiedMessage) }
            ) {
                DetailsTextField(
                    value = card.zipCode
                )
            }
        }

        if (!extraInfo.isNullOrEmpty()) {
            TiligSpacerVerticalDefault()
            BoxWithTitledContent(
                modifier = Modifier.fillMaxWidth(),
                titleStr = stringResource(id = R.string.label_extra_info),
                tracker = tracker,
                event = Tracker.EVENT_EXTRA_INFO_COPY,
                onClick = { copyAction(extraInfo, extraInfoCopiedMessage) }
            ) {
                DetailsTextField(
                    value = extraInfo,
                    enabled = true
                )
            }
        }

        AllCustomFieldsViewComponent(card.details.customFields) {
            callBack.onShowSnackbarMessage(it)
        }

        if (card.isTestDriveItem()) {

            Spacer(modifier = Modifier.height(56.dp))
            TextButton(
                modifier = Modifier.align(Alignment.CenterHorizontally),
                onClick = { callBack.onActionEvent(ItemDetailsActionEvent.DeleteItem) },
            ) {
                Text(
                    stringResource(id = R.string.bt_delete_credit_card),
                    color = LightBlue,
                    style = StyleStolzRegularDarkBlue16
                )
            }
        } else {
            TiligSpacerVerticalDefault()
            ShareLinkComponent(
                item = card,
                folder = folder,
                isActionEnabled = !isLoading,
                shareErrorMessage = shareErrorMessage,
                callback = object : ShareLinkComponentCallback {
                    override fun onRevokeAccess(member: Membership) {
                        callBack.onActionEvent(
                            ItemDetailsActionEvent.ShareRevoke(
                                folderId = folder!!.id,
                                memberId = member.id
                            )
                        )
                    }

                    override fun onOpenShareScreen() {
                        callBack.onUpdateBottomSheet(BottomSheetType.ShareWith(card))
                    }
                })
        }
    }
}