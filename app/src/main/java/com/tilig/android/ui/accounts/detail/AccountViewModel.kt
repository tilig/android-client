package com.tilig.android.ui.accounts.detail

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.ApiErrorResponse
import com.tilig.android.data.models.ApiSuccessResponse
import com.tilig.android.data.models.SecretItemType
import com.tilig.android.data.models.UiError
import com.tilig.android.data.models.tilig.*
import com.tilig.android.ui.accounts.detail.events.AccountActionEvent
import com.tilig.android.ui.secrets.details.ItemDetailsViewModel
import com.tilig.android.ui.secrets.details.events.BaseEditEvent
import com.tilig.android.ui.secrets.details.events.ItemDetailsActionEvent
import com.tilig.android.ui.secrets.details.states.ItemEditWrapper
import com.tilig.android.ui.secrets.details.states.ItemDetailsUiState
import com.tilig.android.ui.wifipass.details.WifiEditWrapper
import com.tilig.android.utils.getDomain
import com.tilig.android.utils.removePredefinedPrefixesFromDomain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent


data class AccountEditWrapper(
    val name: String? = null,
    val username: String? = null,
    val password: String? = null,
    val website: String? = null,
    val brandData: Brand? = null,
    val extraInfo: String? = null,
    override val customFields: List<CustomField>? = null,
    override val selectedFolder: Folder? = null
) : ItemEditWrapper {
    constructor(account: Account) : this(
        name = account.name,
        username = account.username,
        password = account.password,
        website = account.website,
        brandData = account.brand,
        extraInfo = account.notes,
        customFields = account.details.customFields,
        selectedFolder = account.folder
    )

    override fun hadDataChanged(original: SecretItem): Boolean {
        return (original as Account).name != name
                || original.website?.ifEmpty { null } != website?.ifEmpty { null }
                || original.notes.ifEmpty { null } != extraInfo?.ifEmpty { null }
                || original.username?.ifEmpty { null } != username?.ifEmpty { null }
                || original.password?.ifEmpty { null } != password?.ifEmpty { null }
                || original.folder != selectedFolder
                || original.details.customFields.ifEmpty { null } != customFields?.ifEmpty { null }
    }

    override fun getItemName(): String? = name
}

sealed interface AccountEditEvent : BaseEditEvent {
    data class NameChanged(val name: String) : AccountEditEvent
    data class UserNameChanged(val username: String) : AccountEditEvent
    data class PasswordChanged(val password: String) : AccountEditEvent
    data class WebsiteChanged(val website: String) : AccountEditEvent
    data class ExtraInfoChanged(val extraInfo: String) : AccountEditEvent
}

class AccountViewModel : ItemDetailsViewModel(), KoinComponent {

    override fun getSecretItemType(): SecretItemType = SecretItemType.ACCOUNT

    override fun getItemEditWrapper(item: SecretItem?): ItemEditWrapper {
        return if (item != null) AccountEditWrapper(item as Account) else AccountEditWrapper()
    }

    override fun updateCustomFields(customFields: List<CustomField>?): ItemEditWrapper {
        return (viewModelState.value.itemEditWrapper as AccountEditWrapper).copy(
            customFields = customFields
        )
    }

    override fun updateFolder(folder: Folder?): ItemEditWrapper {
        return (viewModelState.value.itemEditWrapper as AccountEditWrapper).copy(
            selectedFolder = folder
        )
    }

    companion object {
        const val AUTO_ACCEPT_INVITES_ENABLED: Boolean = false
    }

    override fun onActionEvent(event: ItemDetailsActionEvent) {
        if (event is AccountActionEvent) {
            when (event) {
                AccountActionEvent.CreateShareLink -> createShareLink()
                AccountActionEvent.DeleteShareLink -> deleteShareLink()
                is AccountActionEvent.GetBrandInfoByWebsite -> updateWebsiteBrandData(event.website)
                is AccountActionEvent.RestorePassword -> restorePassword(event.secret)
            }
        } else {
            super.onActionEvent(event)
        }
    }

    override fun onEditEvent(event: BaseEditEvent) {
        if (uiState.value !is ItemDetailsUiState.ItemEdit){
            return
        }
        if (event is AccountEditEvent) {
            viewModelState.update {
                it.copy(
                    itemEditWrapper = when (event) {
                        is AccountEditEvent.ExtraInfoChanged -> {
                            (it.itemEditWrapper as AccountEditWrapper).copy(
                                extraInfo = event.extraInfo
                            )
                        }
                        is AccountEditEvent.NameChanged -> {
                            (it.itemEditWrapper as AccountEditWrapper).copy(
                                name = event.name
                            )
                        }
                        is AccountEditEvent.PasswordChanged -> {
                            (it.itemEditWrapper as AccountEditWrapper).copy(
                                password = event.password
                            )
                        }
                        is AccountEditEvent.UserNameChanged -> {
                            (it.itemEditWrapper as AccountEditWrapper).copy(
                                username = event.username
                            )
                        }
                        is AccountEditEvent.WebsiteChanged -> {
                            (it.itemEditWrapper as AccountEditWrapper).copy(
                                website = event.website
                            )
                        }
                    }
                )
            }
        } else {
            super.onEditEvent(event)
        }
    }

    override fun buildItemWithUpdatedItems(): SecretItem? {
        val currentState = uiState.value
        if (currentState is ItemDetailsUiState.ItemEdit) {
            val editable = currentState.itemEditWrapper as AccountEditWrapper
            return (currentState.item as Account).apply {
                this.username = editable.username
                this.website = editable.website
                val previousPassword = this.password
                if (this.password != editable.password) {
                    this.password = editable.password
                    // Update password history
                    if (previousPassword != null) {
                        val listHistory = this.details.history.toMutableList()
                        listHistory.add(
                            HistoryEntry(
                                kind = ItemFieldType.PASSWORD,
                                value = previousPassword,
                                replacedAt = HistoryEntry.formatNow()
                            )
                        )
                        this.details.history = listHistory
                    }
                }
                this.notes = editable.extraInfo ?: ""
                this.name = editable.name
                // ignore fields with empty names
                editable.customFields?.filter { it.name.isNotEmpty() }?.let {
                    this.details.customFields = it.toMutableList()
                }
                updateFolderField(this, editable.selectedFolder)
            }
        }
        return null
    }

    override fun trackDeleteItem() {
        tracker.trackEvent(Tracker.EVENT_LOGIN_DELETED)
    }

    private fun createShareLink() {
        viewModelState.value.item?.let { account ->
            cryptoModel.withInitializedCrypto { crypto ->
                if (account.id == null) {
                    Log.e(
                        "AccountViewModel",
                        "Create link failed: account.id is null"
                    )
                } else {
                    var decryptedDek = account.decryptedDek
                    if (decryptedDek == null) {
                        if (account.isSharedFolderItem()) {
                            val usersKeypair = crypto.getKeyPairs().keypair!!
                            decryptedDek = crypto.open(
                                cipherText = account.folder!!.myEncryptedPrivateKey,
                                keyPair = usersKeypair
                            )
                        } else {
                            Log.e(
                                "AccountViewModel",
                                "Create link failed: account.decryptedDek is null"
                            )
                            return@withInitializedCrypto
                        }
                    }

                    val derivedDataFromMasterSecret =
                        crypto.deriveDataFromMasterSecret(decryptedDek!!)
                    if (derivedDataFromMasterSecret == null) {
                        Log.e(
                            "AccountViewModel",
                            "Create link failed: cannot build share data"
                        )
                        return@withInitializedCrypto
                    }
                    viewModelState.update {
                        it.copy(isLoading = true, errorMessage = null, shareErrorMessage = null)
                    }
                    viewModelScope.launch(Dispatchers.IO) {
                        val linkResult = repo.createShareLink(
                            account.id!!,
                            derivedDataFromMasterSecret
                        )
                        if (linkResult is ApiSuccessResponse) {
                            getItem(item = account)
                        } else if (linkResult is ApiErrorResponse) {
                            // share already created, fetch account
                            if (linkResult.errorMessage == "{\"base\":\"Share already created\"}") {
                                getItem(item = account)
                            } else {
                                Log.e(
                                    "AccountViewModel",
                                    "Create link failed: ${linkResult.errorMessage}"
                                )
                                viewModelState.update {
                                    it.copy(
                                        errorMessage = UiError(null, R.string.something_went_wrong),
                                        shareErrorMessage = null,
                                        isLoading = false
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun deleteShareLink() {
        viewModelState.value.item?.let { account ->
            if (account.id != null) {
                viewModelState.update {
                    it.copy(
                        isLoading = true
                    )
                }
                viewModelScope.launch(Dispatchers.IO) {
                    val linkResult = repo.deleteShareLink(uuid = account.id!!)
                    if (linkResult is ApiSuccessResponse) {
                        getItem(item = account)
                    } else if (linkResult is ApiErrorResponse) {
                        // link has been already deleted on other device/web
                        if (linkResult.errorMessage == "{\"msg\":\"Not found\"}") {
                            getItem(item = account)
                        } else {
                            viewModelState.update {
                                it.copy(
                                    errorMessage = UiError(linkResult.errorMessage),
                                    shareErrorMessage = null,
                                    isLoading = false
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    private fun updateWebsiteBrandData(website: String) {
        if (uiState.value is ItemDetailsUiState.ItemEdit) {
            if (website.isEmpty()) {
                return
            }
            viewModelState.update {
                it.copy(
                    isLoading = true, errorMessage = null, shareErrorMessage = null
                )
            }
            viewModelScope.launch(Dispatchers.IO) {
                val result =
                    repo.getBrand(
                        website.getDomain().removePredefinedPrefixesFromDomain() ?: website
                    )
                val brand = if (result is ApiSuccessResponse) result.body else null
                viewModelState.update {
                    it.copy(
                        isLoading = false,
                        errorMessage = null,
                        shareErrorMessage = null,
                        itemEditWrapper = (it.itemEditWrapper as AccountEditWrapper).copy(
                            brandData = brand
                        )
                    )
                }
            }
        }
    }

    private fun restorePassword(secret: String) {
        viewModelState.value.item?.apply {
            if ((this as Account).password != secret && this.password != null) {
                // Update password history
                val listHistory = this.details.history.toMutableList()
                listHistory.add(
                    HistoryEntry(
                        kind = ItemFieldType.PASSWORD,
                        value = this.password!!,
                        replacedAt = HistoryEntry.formatNow()
                    )
                )
                this.details.history = listHistory
            }
            this.password = secret
        }
        viewModelState.value.item?.let {
            updateItem(it) { _ ->
                tracker.trackEvent(Tracker.EVENT_PASSWORD_RESTORED)
                viewModelState.update { state ->
                    state.copy(
                        itemEditWrapper = (state.itemEditWrapper as AccountEditWrapper).copy(
                            password = secret
                        ),
                        errorMessage = null,
                        shareErrorMessage = null
                    )
                }
            }
        }
    }
}