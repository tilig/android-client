package com.tilig.android.ui.onboarding

import android.app.Activity.RESULT_OK
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.TweenSpec
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.isSpecified
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.core.view.WindowCompat
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPagerIndicator
import com.google.accompanist.pager.PagerState
import com.google.accompanist.pager.rememberPagerState
import com.tilig.android.R
import com.tilig.android.analytics.*
import com.tilig.android.ui.components.ProgressButton
import com.tilig.android.ui.components.StatusBarAndNavigationBarContrast
import com.tilig.android.ui.onboarding.components.OnboardingPager
import com.tilig.android.ui.onboarding.components.OnboardingSignInView
import com.tilig.android.ui.onboarding.components.WarnSnackbar
import com.tilig.android.ui.signin.SignInUiState
import com.tilig.android.ui.signin.SignInViewModel
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.SharedPrefs
import com.tilig.android.utils.UITestTags
import kotlinx.coroutines.launch
import org.koin.androidx.compose.get

private const val AnimationDuration = 700

@ExperimentalPagerApi
@ExperimentalAnimationApi
class OnboardingActivity : ComponentActivity() {

    val viewModel by viewModels<SignInViewModel>()

    companion object {
        const val PAGES = 4

        val ARG_ACCOUNT_DELETED = "ARG_ACCOUNT_DELETED"
    }

    override fun onStart() {
        super.onStart()
        viewModel.attemptSilentLogin()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)

        setContent {
            TiligOnboardingTheme {
                OnboardingActivityScreen(
                    showAccountDeletedMessage = this.intent.extras?.getBoolean(
                        ARG_ACCOUNT_DELETED,
                        false
                    ) ?: false
                )
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@ExperimentalAnimationApi
@ExperimentalPagerApi
@Composable
fun OnboardingActivityScreen(
    showAccountDeletedMessage: Boolean,
    tracker: Mixpanel = get(),
    viewModel: SignInViewModel = viewModel(),
    preferences: SharedPrefs = get(),
) {
    StatusBarAndNavigationBarContrast(statusBarBright = true, navigationBarBright = true)
    Breadcrumb.drop(Breadcrumb.SignInOnboarding)

    val uiState by viewModel.uiState.collectAsState()
    val coroutineScope = rememberCoroutineScope()
    val bottomSheetScaffoldState = rememberBottomSheetScaffoldState(
        bottomSheetState = BottomSheetState(BottomSheetValue.Collapsed)
    )
    val context = LocalContext.current

    val snackState = remember { SnackbarHostState() }
    val snackScope = rememberCoroutineScope()
    val accountDeletedMessage = stringResource(id = R.string.msg_account_was_deleted)

    // If the user has finish onboarding before, we skip to the last page.
    // This is possible e.g. if they swiped to the signup, then exited.
    val pagerState = rememberPagerState(
        initialPage = if (preferences.hasFinishedOnboarding) OnboardingActivity.PAGES - 1 else 0
    )

    LaunchedEffect(pagerState) {
        if (pagerState.currentPage == 0) {
            tracker.trackEvent(Tracker.EVENT_WELCOME_STARTED)
        }
    }

    val animateProgressBtn = remember { mutableStateOf(true) }

    LaunchedEffect(key1 = showAccountDeletedMessage) {
        if (showAccountDeletedMessage) {
            snackScope.launch { snackState.showSnackbar(accountDeletedMessage) }
            animateProgressBtn.value = false
            pagerState.scrollToPage(OnboardingActivity.PAGES - 1)
        }
    }

    BottomSheetScaffold(
        scaffoldState = bottomSheetScaffoldState,
        sheetContent = {
            MoreInfoSignInBottomSheet {
                coroutineScope.launch {
                    bottomSheetScaffoldState.bottomSheetState.collapse()
                }
            }
        },
        sheetContentColor = Black,
        sheetPeekHeight = 0.dp,
        sheetShape = RoundedCornerShape(
            topStart = DefaultCornerRadius,
            topEnd = DefaultCornerRadius
        ),
        modifier = Modifier
            .fillMaxSize()
            .navigationBarsPadding()
            .imePadding(),
    ) {
        Box(
            Modifier
                .fillMaxSize()
        ) {
            when (uiState) {
                is SignInUiState.Onboarding -> {
                    OnboardingActivityView(
                        pagerState = pagerState,
                        onFinishOnBoarding = {
                            viewModel.finishOnboarding()
                        }
                    )
                }
                is SignInUiState.SignInSuccess -> {
                    val error = (uiState as SignInUiState.SignInSuccess).errorMessage
                    if (error != null) {
                        val genericError = stringResource(id = R.string.error_signin)
                        LaunchedEffect(uiState) {
                            Toast.makeText(
                                context,
                                error.message ?: genericError,
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                    viewModel.saveDebug(context)
                    (context as ComponentActivity).setResult(RESULT_OK)
                    context.finish()
                }
                is SignInUiState.SignInError,
                is SignInUiState.SignInLoading -> {
                    val genericError = stringResource(id = R.string.error_signin)
                    val errorMessage = uiState.errorMessage?.message
                        ?: uiState.errorMessage?.msgResource?.let { it -> stringResource(id = it) }
                        ?: genericError
                    LaunchedEffect(uiState) {
                        if (uiState is SignInUiState.SignInError) {
                            Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
                            viewModel.resetErrorState()
                        }
                    }
                    viewModel.setToLoading(false)
                    OnboardingSignInView(
                        bottomSheetScaffoldState = bottomSheetScaffoldState,
                    )
                }
            }
            ModalSheetShadow(
                color = Black.copy(alpha = 0.85f),
                visible = bottomSheetScaffoldState.bottomSheetState.targetValue != BottomSheetValue.Collapsed
            )
            SnackbarHost(
                hostState = snackState,
                Modifier
                    .wrapContentWidth()
                    .align(Alignment.BottomCenter)
            ) {
                WarnSnackbar(snackbarData = it)
            }

        }
    }
}

@ExperimentalAnimationApi
@ExperimentalPagerApi
@Composable
fun OnboardingActivityView(
    pagerState: PagerState,
    onFinishOnBoarding: () -> Unit
) {
    val coroutineScope = rememberCoroutineScope()

    BackHandler(pagerState.currentPage != 0) {
        coroutineScope.launch {
            // We coerce this value, since people might be able to tap faster than the
            // animation and get to a page number below zero
            val page = (pagerState.currentPage - 1).coerceIn(0 until OnboardingActivity.PAGES)
            pagerState.animateScrollToPage(page)
        }
    }

    Surface(color = MaterialTheme.colors.background) {
        ConstraintLayout(
            modifier = Modifier
                .fillMaxSize()
                .statusBarsPadding()
        ) {
            val (btSkip, content, pagerIndicator, indicator) = createRefs()

            TextButton(
                modifier = Modifier
                    .padding(DefaultPadding)
                    .constrainAs(btSkip) {
                        top.linkTo(parent.top)
                        end.linkTo(parent.end)
                    }, onClick = onFinishOnBoarding
            ) {
                Text(
                    stringResource(id = R.string.skip),
                    style = StyleStolzRegularDarkBlue14.copy(color = LightBlue),
                )
            }

            OnboardingPager(pagerState = pagerState,
                modifier = Modifier
                    .constrainAs(content) {
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        top.linkTo(btSkip.bottom)
                        bottom.linkTo(pagerIndicator.top)
                        width = Dimension.fillToConstraints
                        height = Dimension.fillToConstraints
                    })

            HorizontalPagerIndicator(
                pagerState = pagerState,
                inactiveColor = Grey,
                activeColor = DarkBlue,
                modifier = Modifier
                    .padding(bottom = 40.dp)
                    .constrainAs(pagerIndicator) {
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        bottom.linkTo(indicator.top)
                    },
            )

            AnimatedVisibility(
                modifier = Modifier
                    .constrainAs(indicator) {
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        bottom.linkTo(parent.bottom)
                    }
                    .testTag(UITestTags.onboardingProgressTestTag)
                    .padding(bottom = DefaultPadding),
                visible = true,
                exit = fadeOut(
                    animationSpec = tween(durationMillis = AnimationDuration)
                ),
            ) {
                ProgressButton(
                    modifier = Modifier
                        .testTag(UITestTags.onboardingNextButtonTag),
                    pagerState = pagerState,
                    onFinalPageNextClick = onFinishOnBoarding
                )
            }
        }
    }
}

/**
 * Show shadow behind bottom sheet
 */
@Composable
private fun ModalSheetShadow(
    color: Color,
    visible: Boolean
) {
    if (color.isSpecified) {
        val alpha by animateFloatAsState(
            targetValue = if (visible) 1f else 0f,
            animationSpec = TweenSpec()
        )
        Canvas(
            Modifier
                .fillMaxSize()
        ) {
            drawRect(color = color, alpha = alpha)
        }
    }
}

@ExperimentalAnimationApi
@ExperimentalPagerApi
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    TiligOnboardingTheme {
        OnboardingActivityScreen(false)
    }
}