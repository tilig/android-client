package com.tilig.android.ui.notes.add

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import com.tilig.android.R
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.Folders
import com.tilig.android.data.models.tilig.Note
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.components.TiligSpacerVerticalDefault
import com.tilig.android.ui.components.TiligSpacerVerticalMedium
import com.tilig.android.ui.secrets.add.AddToFolderComponent
import com.tilig.android.ui.secrets.components.AddSecretItemNameAndLogoView
import com.tilig.android.ui.secrets.components.NoteViewEdit
import com.tilig.android.ui.secrets.components.SaveSecretItemButton
import com.tilig.android.ui.theme.DefaultPadding
import com.tilig.android.ui.theme.HalfPadding

@Composable
fun CreateNote(folders: Folders?, onSaveNote: (Note) -> Unit) {

    val focusManager = LocalFocusManager.current
    val focusRequester = remember { FocusRequester() }

    val displayTitleError = remember { mutableStateOf(false) }

    val titleState = remember { TextFieldState("", validator = { !displayTitleError.value }) }
    val descriptionState = remember { TextFieldState("") }
    val selectedFolder = remember { mutableStateOf<Folder?>(null) }
    val scrollableState = rememberScrollState()

    val saveNoteCall = {
        val note = Note.createEmpty()
        note.name = titleState.text
        note.content = descriptionState.text
        note.folder = selectedFolder.value
        onSaveNote.invoke(
            note
        )
    }

    LaunchedEffect(Unit) {
        focusRequester.requestFocus()
    }

    Column(
        modifier = Modifier
            .padding(
                start = DefaultPadding,
                end = DefaultPadding,
                bottom = HalfPadding
            )
            .fillMaxSize()
            .verticalScroll(scrollableState)
            .imePadding()
    ) {
        AddSecretItemNameAndLogoView(
            icon = painterResource(id = R.drawable.ic_note),
            hintInputRes = R.string.hint_note_name,
            focusManager = focusManager,
            focusRequester = focusRequester,
            nameState = titleState,
            onValueChanged = {
                displayTitleError.value = false
            })
        TiligSpacerVerticalMedium()

        NoteViewEdit(
            modifier = Modifier
                .fillMaxHeight()
                .weight(1f),
            notesState = descriptionState,
            title = stringResource(id = R.string.label_note_content),
            contentHintRes = R.string.hint_note_extra_info,
            contentAlignment = Alignment.TopStart
        )

        TiligSpacerVerticalDefault()
        AddToFolderComponent(
            modifier = Modifier,
            folders = folders,
            selectedFolder = selectedFolder.value,
            onFolderChanged = { selectedFolder.value = it }
        )

        TiligSpacerVerticalDefault()
        SaveSecretItemButton(
            modifier = Modifier.align(alignment = Alignment.CenterHorizontally),
            text = stringResource(id = R.string.btn_save_note),
            onSave = {
                if (isSaveButtonEnabled(
                        titleState = titleState,
                        displayTitleError = displayTitleError,
                    )
                ) {
                    saveNoteCall()
                }
            }
        )
    }
}

private fun isSaveButtonEnabled(
    titleState: TextFieldState,
    displayTitleError: MutableState<Boolean>,
): Boolean {
    var allFieldsEntered = true
    displayTitleError.value = titleState.text.isEmpty()
    if (displayTitleError.value) {
        allFieldsEntered = false
    }
    return allFieldsEntered
}