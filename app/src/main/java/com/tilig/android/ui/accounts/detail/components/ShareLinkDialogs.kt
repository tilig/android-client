package com.tilig.android.ui.accounts.detail.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.tilig.android.R
import com.tilig.android.ui.components.AppendableText
import com.tilig.android.ui.components.AppendableTextView
import com.tilig.android.ui.components.TiligSpacerHorizontalDefault
import com.tilig.android.ui.components.TiligSpacerVerticalDefault
import com.tilig.android.ui.theme.*


@Composable
fun ShareRevokeAccessDialog(
    email: String,
    accountName: String,
    onConfirm: () -> Unit,
    onCancel: (() -> Unit)
) {
    BaseDialog(
        headerIconRes = R.drawable.ic_revoke_access_red,
        titleRes = R.string.revoke_access,
        confirmBtnTitleRes = R.string.revoke_access,
        onConfirm = onConfirm,
        onCancel = onCancel
    ) {
        AppendableTextView(
            appendableTexts = listOf(
                AppendableText(
                    stringResource(id = R.string.dialog_revoke_access_desc_start),
                    false,
                    addSpace = true,
                ),
                AppendableText(
                    email,
                    true
                ),
                AppendableText(
                    stringResource(id = R.string.dialog_revoke_access_desc_middle),
                    addSpace = true,
                    isStyled = false
                ),
                AppendableText(
                    accountName,
                    true
                ),
                AppendableText(
                    stringResource(id = R.string.dialog_revoke_access_desc_end),
                    addSpace = true,
                    isStyled = false
                )
            ),
            textStyle = StyleStolzRegularDarkBlue14,
            spanStyle = SpanStyle(
                fontWeight = FontWeight.Medium
            ),
            modifier = Modifier
        )
    }
}

@Composable
fun ShareLinkDeleteDialog(
    onConfirm: () -> Unit,
    onCancel: (() -> Unit)
) {
    BaseDialog(
        headerIconRes = R.drawable.ic_share_link_trash,
        titleRes = R.string.delete_the_share_link,
        confirmBtnTitleRes = R.string.yes_delete,
        onConfirm = onConfirm,
        onCancel = onCancel
    ) {
        Text(
            text = stringResource(id = R.string.delete_share_link_dialog_title),
            modifier = Modifier,
            style = StyleStolzBookDarkBlue16,
            textAlign = TextAlign.Center
        )
    }
}

@Composable
fun BaseDialog(
    headerIconRes: Int,
    titleRes: Int,
    confirmBtnTitleRes: Int,
    onConfirm: () -> Unit,
    onCancel: () -> Unit,
    content: @Composable () -> Unit,
) {
    Dialog(onDismissRequest = onCancel) {
        Column(
            modifier = Modifier
                .padding(DefaultPadding)
                .background(color = White, shape = RoundedCornerShape(MediumCornerRadius))
                .fillMaxWidth()
                .padding(DoublePadding),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(headerIconRes),
                contentDescription = "",
                modifier = Modifier.size(46.dp)
            )
            Text(
                text = stringResource(id = titleRes),
                modifier = Modifier,
                style = StyleStolzMediumDarkBlue20,
                textAlign = TextAlign.Center
            )

            content()

            Column(
                modifier = Modifier
                    .padding(top = DefaultPadding)
                    .fillMaxWidth()
            ) {

                TextButton(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(48.dp)
                        .background(
                            color = WarningButtonTextColor,
                            shape = RoundedCornerShape(MediumCornerRadius)
                        ),
                    elevation = ButtonDefaults.elevation(
                        defaultElevation = 2.dp,
                        focusedElevation = 0.dp,
                        hoveredElevation = 0.dp
                    ),
                    shape = RoundedCornerShape(8.dp),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = SettingsWarnColor,
                    ),
                    onClick = onConfirm
                ) {
                    Text(
                        stringResource(id = confirmBtnTitleRes),
                        style = StyleStolzRegularDarkBlue14,
                        color = White
                    )
                }

                TiligSpacerVerticalDefault()

                TextButton(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(48.dp),
                    elevation = ButtonDefaults.elevation(
                        defaultElevation = 2.dp,
                        focusedElevation = 0.dp,
                        hoveredElevation = 0.dp
                    ),
                    shape = RoundedCornerShape(8.dp),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = White,
                    ),
                    onClick = onCancel
                ) {
                    Text(
                        modifier = Modifier,
                        text = stringResource(id = R.string.cancel),
                        textAlign = TextAlign.Center,
                        style = StyleStolzRegularDarkBlue14,
                        color = DarkBlue.copy(alpha = 0.75f)
                    )
                }
            }
        }
    }
}

@Composable
@Preview
fun ShareLinkDeleteDialogPreview(
) {
    ShareLinkDeleteDialog(
        onCancel = {},
        onConfirm = {}
    )
}

@Composable
@Preview
fun ShareRevokeAccessDialogPreview(
) {
    ShareRevokeAccessDialog(
        email = "test@com.dd",
        accountName = "Test",
        onCancel = {},
        onConfirm = {}
    )
}