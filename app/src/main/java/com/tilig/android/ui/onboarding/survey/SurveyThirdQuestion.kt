package com.tilig.android.ui.onboarding.survey

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.tilig.android.ui.components.TiligCheckBox
import com.tilig.android.ui.theme.*

@Composable
fun SurveyThirdQuestion(viewModel: SurveyViewModel) {
    val thirdQuestion =
        remember { surveyQuestions[if (viewModel.showThirdQuestionAVariant()) THIRD_A_QUESTION_KEY else THIRD_B_QUESTION_KEY]!! }
    val checkedSet = remember { viewModel.getThirdQuestionAnswers() }

    Column(
        modifier = Modifier
            .padding(horizontal = 32.dp)
            .fillMaxSize()
    ) {
        Text(
            text = thirdQuestion.content,
            style = Typography.h1.copy(fontSize = 24.sp, lineHeight = 32.sp, color = DarkBlue),
            textAlign = TextAlign.Start
        )
        Spacer(modifier = Modifier.height(16.dp))
        thirdQuestion.answerOptions.forEach { item ->
            Spacer(modifier = Modifier.height(16.dp))
            SurveyThirdQuestionCard(item, checkedSet.contains(item), viewModel)
        }
        Spacer(modifier = Modifier.height(16.dp))
    }
}

@Composable
private fun SurveyThirdQuestionCard(
    title: String,
    isChecked: Boolean,
    viewModel: SurveyViewModel
) {
    val checked = remember {
        mutableStateOf(isChecked)
    }

    val onCheckChange: ((Boolean) -> Unit) = {
        checked.value = it
        if (checked.value) {
            viewModel.addAnswerToThirdQuestion(title)
        } else {
            viewModel.removeAnswerForThirdQuestion(title)
        }
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .heightIn(min = 64.dp)
            .background(
                color = BackgroundGrey,
                shape = RoundedCornerShape(DefaultCornerRadius)
            )
            .border(
                width = DefaultSmallBorder,
                shape = RoundedCornerShape(DefaultCornerRadius),
                color = if (checked.value) LightBlue else Grey
            )
            .padding(start = DefaultPadding)
            .clickable {
                onCheckChange(!checked.value)
            },
        verticalAlignment = Alignment.CenterVertically
    ) {
        TiligCheckBox(
            checked = checked.value,
            onCheckedChange = onCheckChange,
            modifier = Modifier
                .size(20.dp)
        )

        Text(
            modifier = Modifier
                .padding(horizontal = DefaultPadding, vertical = 20.dp)
                .weight(1f),
            text = title,
            style = Typography.h3.copy(
                fontSize = 16.sp,
                color = if (checked.value) LightBlue else DarkBlue
            )
        )
    }
}

@Preview
@Composable
fun SurveyThirdQuestionPreview() {
    SurveyThirdQuestion(viewModel = viewModel())
}