package com.tilig.android.ui.secrets.details

import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.SwipeableState
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import com.tilig.android.R
import com.tilig.android.ui.secrets.components.SecretItemCollapsableContainer
import com.tilig.android.ui.secrets.components.SecretItemCollapsableContainerCallBack
import com.tilig.android.ui.secrets.details.events.ItemDetailsActionEvent
import com.tilig.android.ui.secrets.details.states.ItemDetailsUiState
import com.tilig.android.ui.theme.appBarDetailsExpandedEditHeightDp
import com.tilig.android.utils.SecretItemExt.getBackgroundColor
import com.tilig.android.utils.SecretItemExt.getSecretIcon


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SecretEditViewModeComponent(
    editState: ItemDetailsUiState.ItemEdit,
    swipingState: SwipeableState<SwipingStates>,
    onActionEvent: (event: ItemDetailsActionEvent) -> Unit,
    content: @Composable () -> Unit
) {
    val toolbarCallback = object : SecretItemCollapsableContainerCallBack {
        override fun onBackClicked() {
            onActionEvent(ItemDetailsActionEvent.OnGoBack)
        }

        override fun onActionBtnClicked() {
            onActionEvent(ItemDetailsActionEvent.SaveChanges)
        }

        override fun onDebugBtnClicked() {
            onActionEvent(ItemDetailsActionEvent.DebugItem)
        }
    }

    SecretItemCollapsableContainer(
        swipingState = swipingState,
        accountIcon = editState.item.getSecretIcon(context = LocalContext.current),
        backgroundColor = editState.item.getBackgroundColor(),
        itemName = editState.itemEditWrapper.getItemName(),
        actionBtnTitleRes = R.string.save,
        isLoading = editState.isLoading,
        isActionBtnAvailable = true,
        callBack = toolbarCallback,
        expandedHeight = appBarDetailsExpandedEditHeightDp(),
        addExtraHeightShift = false,
        content = {
            content()
        }
    )
}