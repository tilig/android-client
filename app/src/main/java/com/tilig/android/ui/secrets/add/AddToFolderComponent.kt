package com.tilig.android.ui.secrets.add

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.ExposedDropdownMenuDefaults.TrailingIcon
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.Folders
import com.tilig.android.ui.secrets.components.TitledContent
import com.tilig.android.ui.theme.*

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun AddToFolderComponent(
    modifier: Modifier,
    folders: Folders?,
    selectedFolder: Folder?,
    onFolderChanged:(Folder?)-> Unit
) {
    // if folder is null or was shared don't allow operations with folder
    if (folders == null || selectedFolder?.isSharedWithIndividuals == true) {
        return
    }
    val noFolderTitle = stringResource(id = R.string.label_no_folder)
    TitledContent(
        modifier = modifier.fillMaxWidth(),
        title = stringResource(id = R.string.label_folder)
    ) {
        val expanded = remember { mutableStateOf(false) }

        ExposedDropdownMenuBox(
            expanded = expanded.value,
            onExpandedChange = {
                expanded.value = !expanded.value
            }
        ) {
            Row(
                modifier = Modifier
                    .border(
                        width = 1.dp,
                        color = DefaultBorderColor,
                        shape = Shapes.medium
                    )
                    .fillMaxWidth()
                    .padding(start = DefaultPadding),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Text(
                    text = if (selectedFolder != null) {
                        selectedFolder.name ?: ""
                    } else {
                        noFolderTitle
                    }
                )
                TrailingIcon(
                    expanded = expanded.value
                )
            }

            ExposedDropdownMenu(
                expanded = expanded.value,
                onDismissRequest = {
                    expanded.value = false
                }
            ) {
                // no folder option
                FolderOptionItem(
                    folder = null,
                    onClick = {
                        expanded.value = false
                        //   selectedFolder = null
                        onFolderChanged.invoke(null)
                    }
                )
                folders.forEach { folder ->
                    FolderOptionItem(
                        folder = folder,
                        onClick = {
                            expanded.value = false
                            //   selectedFolder = folder
                            onFolderChanged.invoke(folder)
                        }
                    )
                }
            }
        }
    }
}

@Composable
private fun FolderOptionItem(
    folder: Folder?,
    onClick: () -> Unit
) {
    DropdownMenuItem(
        onClick = onClick
    ) {
        Text(
            text = if (folder != null) {
                folder.name ?: ""
            } else {
                stringResource(id = R.string.label_no_folder)
            }
        )
    }
}