package com.tilig.android.ui.accounts.addaccount.components

import androidx.annotation.StringRes
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.accounts.detail.components.states.PasswordState
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.components.FieldTitle
import com.tilig.android.ui.components.PasswordBasicField
import com.tilig.android.ui.components.SlotText
import com.tilig.android.ui.theme.*
import com.tilig.crypto.Password
import org.koin.androidx.compose.get

private enum class PassState {
    Initial,
    PassGenerating,
    PassGeneratedEditable
}

@Composable
fun CreateAccountPasswordView(
    tracker: Tracker,
    passwordState: TextFieldState,
    onDone: () -> Unit,
    onValueChanged: ((String) -> Unit)? = null,
) {
    val state = remember { mutableStateOf(PassState.Initial) }
    val textStyle = StyleStolzBookDarkBlue16.copy(fontFamily = Space)

    val rowModifier = Modifier.height(DefaultButtonHeight)
    Box {
        val shape = RoundedCornerShape(8.dp)
        val borderColor =
            if (!passwordState.isValid) DefaultErrorColor else if (passwordState.isFocused) LightBlue else SearchBarBorder
        val bfColor =
            if (state.value == PassState.PassGenerating ||
                (state.value == PassState.PassGeneratedEditable && passwordState.text.isNotEmpty())
            ) {
                TiligYellow2
            } else White

        CreatePasswordContent(
            tracker = tracker,
            passwordState = passwordState,
            onDone = onDone,
            onValueChanged = onValueChanged,
            hint = R.string.hint_login_password
        )

        FieldTitle(
            title = stringResource(id = R.string.label_login_password),
            modifier = Modifier
        )
    }
}

@Composable
fun CreatePasswordContent(
    modifier: Modifier = Modifier,
    tracker: Tracker?,
    passwordState: TextFieldState,
    onDone: () -> Unit,
    onValueChanged: ((String) -> Unit)? = null,
    @StringRes hint: Int = R.string.hint_login_password_existing
) {
    val state = remember { mutableStateOf(PassState.Initial) }
    val textStyle = StyleStolzBookDarkBlue16.copy(fontFamily = Space)
    val rowModifier = Modifier.height(DefaultButtonHeight)
    val borderColor =
        if (!passwordState.isValid) DefaultErrorColor else if (passwordState.isFocused) LightBlue else SearchBarBorder
    val bfColor =
        if (state.value == PassState.PassGenerating ||
            (state.value == PassState.PassGeneratedEditable && passwordState.text.isNotEmpty())
        ) {
            TiligYellow2
        } else White
    val shape = RoundedCornerShape(8.dp)

    Column(modifier = modifier.padding(top = if (passwordState.isValid) DefaultErrorMessageHeight else 0.dp)) {
        if (!passwordState.isValid) {
            Text(
                modifier = Modifier
                    .align(Alignment.End)
                    .height(DefaultErrorMessageHeight)
                    .background(
                        color = DefaultErrorColor,
                        shape = RoundedCornerShape(topStart = 8.dp, topEnd = 8.dp)
                    )
                    .padding(4.dp), text = stringResource(id = R.string.error_password),
                style = StyleStolzRegularDarkBlue12
            )
        }
        Row(
            modifier = rowModifier
                .border(1.dp, color = borderColor, shape = shape)
                .background(
                    bfColor,
                    shape = shape
                )
                .fillMaxWidth(),
        ) {
            val passwordModifier =
                if (passwordState.text.isEmpty()) {
                    Modifier
                        .fillMaxSize()
                        .weight(1f)
                } else {
                    Modifier
                        .fillMaxSize()
                }
            if (state.value != PassState.PassGenerating) {
                PasswordBasicField(
                    modifier = passwordModifier.padding(end = DefaultPadding),
                    hint = stringResource(id = hint),
                    state = passwordState,
                    textStyle = textStyle,
                    onDone = { onDone() },
                    showRevealButton = passwordState.text.isNotEmpty(),
                    tracker = tracker,
                    onValueChange = {
                        if (it.isEmpty()) {
                            state.value = PassState.Initial
                        }
                        onValueChanged?.invoke(it)
                    }
                )
            }
            if (passwordState.text.isEmpty()) {
                TextButton(modifier = Modifier
                    .height(36.dp)
                    .padding(horizontal = 8.dp)
                    .align(Alignment.CenterVertically),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = White,
                        contentColor = White,
                        disabledBackgroundColor = White,
                        disabledContentColor = White
                    ),
                    elevation = ButtonDefaults.elevation(),
                    shape = RoundedCornerShape(8.dp),
                    onClick = {
                        state.value = PassState.PassGenerating
                        passwordState.text = Password.generate().toString()
                        tracker?.trackEvent(Tracker.EVENT_PASSWORD_GENERATED)
                    }) {
                    Text(
                        modifier = Modifier.align(Alignment.CenterVertically),
                        text = stringResource(id = R.string.generate),
                        style = StyleStolzRegularDarkBlue14.copy(
                            color = LightBlue,
                            lineHeight = 24.sp
                        )
                    )
                }
            } else if (state.value == PassState.PassGenerating) {
                SlotText(
                    text = passwordState.text, textStyle = textStyle, modifier = Modifier
                        .fillMaxSize()
                        .weight(1f)
                        .padding(start = DefaultPadding),
                    onSlottingEnd = {
                        state.value = PassState.PassGeneratedEditable
                        onValueChanged?.invoke(passwordState.text)
                    }
                )
            }
        }
    }
}

@Composable
@Preview
fun CreateAccountPasswordViewPreview() {
    CreateAccountPasswordView(
        tracker = get(),
        passwordState = PasswordState("valuepass"),
        onDone = {}
    )
}
