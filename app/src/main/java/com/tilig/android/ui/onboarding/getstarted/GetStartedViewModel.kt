package com.tilig.android.ui.onboarding.getstarted

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tilig.android.R
import com.tilig.android.analytics.Breadcrumb
import com.tilig.android.data.models.ApiSuccessResponse
import com.tilig.android.data.models.UiError
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.models.tilig.Profile
import com.tilig.android.data.models.tilig.UserSettings
import com.tilig.android.data.repository.Repository
import com.tilig.android.ui.CryptoModel
import com.tilig.android.ui.onboarding.getstarted.components.Step
import com.tilig.android.utils.DateUtils
import com.tilig.android.utils.SharedPrefs
import com.tilig.android.utils.TimerHelper
import com.tilig.android.utils.lock.LockUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

data class GetStartedState(
    val lastCreatedAccount: Account? = null,
    val userEmail: String? = null,
    val displayGetStarted: Boolean = false,
    val stepsStatus: Map<Step, Boolean> = mutableMapOf(),
    val errorMessage: UiError? = null,
) {
    fun getProgress(): Float =
        (stepsStatus.values.filter { it }.size / stepsStatus.size.toFloat()).takeIf { !it.isNaN() }
            ?: 0f
}

class GetStartedViewModel : ViewModel(), KoinComponent {

    private val prefs: SharedPrefs by inject()
    private val repo: Repository by inject()
    private val cryptoModel: CryptoModel by inject()
    private var lastCreatedAccount: Account? = null
    private var hasAccounts = false
    private var isPolling: Boolean = false
    private var timer: TimerHelper? = null

    private val _viewModelState =
        MutableStateFlow(
            GetStartedState()
        )

    val uiState: StateFlow<GetStartedState> = _viewModelState

    companion object {
        private const val POLLING_INTERVAL_MS = 3000L
    }

    init {
        fetchProfile()
    }

    fun getAutofillCount() = prefs.autofillCount

    /**
     * If the user creates an account through the Getting Started flow, we attach it here, so
     * we don't need to re-fetch accounts later for the 'Try Autofill' flow.
     */
    fun attachRecentlyCreatedAccount(account: Account) {
        lastCreatedAccount = account
    }

    fun attachLogins(hasAccounts: Boolean) {
        this.hasAccounts = hasAccounts
        updateStep(Step.LOGIN, true)
    }

    fun getLastCreatedAccount() {
        // Check the cache first
        if (lastCreatedAccount != null) {
            _viewModelState.update {
                it.copy(
                    lastCreatedAccount = lastCreatedAccount
                )
            }
            return
        }

        // Otherwise, fetch the items and look for the most recent one
        viewModelScope.launch(Dispatchers.IO) {
            val encryptedAccounts = repo.secretItems()
            cryptoModel.withInitializedCrypto { crypto ->
                var latestAccount = (encryptedAccounts as? ApiSuccessResponse)?.body?.items
                    ?.map {
                        it.decrypt(
                            repo.jsonConverter,
                            crypto
                        )
                    }
                    ?.filterIsInstance(Account::class.java)
                    ?.maxByOrNull { DateUtils.convertDateToTimeStamp(it.createdAt) }

                // If the user hasn't created anything yet, create a dummy
                if (latestAccount == null) {
                    // TODO move to resources
                    latestAccount = Account.createBasic(
                        name = "Example Login",
                        website = "https://tilig.com",
                        androidAppId = null
                    )
                }

                _viewModelState.update {
                    it.copy(
                        lastCreatedAccount = latestAccount
                    )
                }
            }
        }
    }

    fun lastCreatedAccountOpened() {
        _viewModelState.update {
            it.copy(
                lastCreatedAccount = null
            )
        }
    }

    fun dismissForever() {
        updateAllSteps(completed = true)
    }

    fun debugResetAll(completed: Boolean = false) {
        updateAllSteps(completed = completed)
    }

    private fun updateAllSteps(completed: Boolean) {
        // Update all steps
        val settings = UserSettings()
        val updatedStepsStatus = _viewModelState.value.stepsStatus.toMutableMap()
        updatedStepsStatus.keys.forEach {
            updatedStepsStatus[it] = completed
        }
        settings.setGetStartedInfo(repo.jsonConverter, updatedStepsStatus)
        // Update backend
        viewModelScope.launch(Dispatchers.IO) {
            repo.updateUserSettings(settings)
        }
        // Update UI
        _viewModelState.update {
            it.copy(
                stepsStatus = updatedStepsStatus,
                displayGetStarted = shouldDisplayGetStartedBar(updatedStepsStatus)
            )
        }
    }

    fun updateStep(step: Step, completed: Boolean) {
        val settings = UserSettings()
        val updatedStepsStatus = _viewModelState.value.stepsStatus.toMutableMap()
        updatedStepsStatus[step] = completed
        settings.setGetStartedInfo(repo.jsonConverter, updatedStepsStatus)
        // Update backend
        viewModelScope.launch(Dispatchers.IO) {
            repo.updateUserSettings(settings)
        }
        // Update UI
        _viewModelState.update {
            it.copy(
                stepsStatus = updatedStepsStatus
            )
        }
    }

    /**
     * Fetches the profile to read the settings
     * @param isBackgroundCheck Use `true` to do a lightweight check: no error messaging, and only check if the desktop is connected.
     */
    private fun fetchProfile(isBackgroundCheck: Boolean = false) {
        repo.fetchProfile().enqueue(object : Callback<Profile> {
            override fun onFailure(call: Call<Profile>, t: Throwable) {
                if (!isBackgroundCheck) {
                    Breadcrumb.drop(Breadcrumb.CryptoTrail_CannotFetchPublicKey)
                    _viewModelState.update {
                        it.copy(
                            errorMessage = UiError(t.message, R.string.something_went_wrong)
                        )
                    }
                }
            }

            override fun onResponse(
                call: Call<Profile>,
                response: Response<Profile>
            ) {
                val profile = response.body() ?: return
                val settings = profile.userSettings?.getGetStartedInfo(repo.jsonConverter)

                // Make sure we update the settings from the profile with the state of the app:
                val steps = mutableMapOf<Step, Boolean>()
                settings?.let { steps.putAll(it) }
                // Update biometrics status: don't suggest if it is already enabled
                if (LockUtil.isSupported()) {
                    steps[Step.BIOMETRIC_STEP] =
                        (steps[Step.BIOMETRIC_STEP] == true) || prefs.isLockEnabled
                } else {
                    steps.remove(Step.BIOMETRIC_STEP)
                }
                // Update 'add login' status: don't show if user has already added logins.
                // Note that this value might not be present yet (still loading) but in that case
                // the value will be update when the items come in.
                if (steps[Step.LOGIN] == false && hasAccounts) {
                    steps[Step.LOGIN] = true
                }
                // Update 'try autofill': don't suggest if user has autofilled before
                if (steps[Step.TRY_AUTOFILL] == false && prefs.autofillCount > 0) {
                    steps[Step.TRY_AUTOFILL] = true
                }
                // Update the desktop connection with the info we got from the profile during signin
                if (steps[Step.CONNECT_DESKTOP] == false && prefs.isDesktopConnected) {
                    steps[Step.CONNECT_DESKTOP] = true
                }
                // Hide GS completely if user is an existing user from before the GS implementation,
                // BUT only if the local value is null ('unknown'). Otherwise the Get Started disappears.
                if (profile.userSettings?.hasOnboardedBefore == true) {
                    if (prefs.hasOnboardedBefore == null) {
                        // If the previous status was unknown, then this is a fresh login on Android,
                        // but because the usersettings are true, the user has already onboarded elsewhere
                        // and we skip the onboarding on Android.
                        // This is the case for new users signing up on web first, and for older existing users.
                        prefs.hasOnboardedBefore = true
                    }
                } else {
                    // If the previous status was unknown, but the usersettings are blank, it means
                    // the user is signing up here with Android as their first client. We now know for
                    // sure that they did NOT have the onboarding before.
                    prefs.hasOnboardedBefore = prefs.hasOnboardedBefore ?: false
                }

                // If user signed in on desktop (Web or Ext.) but the user settings still indicate false; we must update the user settings
                var hasDesktopJustBeenConnected = false
                if (settings?.get(Step.CONNECT_DESKTOP) == false && profile.applicationSettings?.isDesktopConnected == true) {
                    hasDesktopJustBeenConnected = true
                    // silent call to update settings
                    val updatedSettings = UserSettings()
                    val updatedStepsStatus = settings.toMutableMap()
                    updatedStepsStatus[Step.CONNECT_DESKTOP] = true
                    updatedSettings.setGetStartedInfo(repo.jsonConverter, updatedStepsStatus)
                    viewModelScope.launch(Dispatchers.IO) {
                        repo.updateUserSettings(updatedSettings)
                    }
                    steps[Step.CONNECT_DESKTOP] = true
                    // Update the prefs as well
                    prefs.isDesktopConnected = true
                }

                // When doing background checks, only update the UI when the CONNECT_DESKTOP has actually changed from false to true.
                if (!isBackgroundCheck || hasDesktopJustBeenConnected) {
                    _viewModelState.update {
                        it.copy(
                            userEmail = profile.email,
                            displayGetStarted = shouldDisplayGetStartedBar(steps),
                            stepsStatus = steps.toSortedMap { a, b -> a.sortOrder.compareTo(b.sortOrder) }
                        )
                    }
                }
            }
        })
    }

    private fun shouldDisplayGetStartedBar(steps: MutableMap<Step, Boolean>) =
        steps.values.contains(false) && prefs.hasOnboardedBefore == false

    /**
     * Periodically re-fetches the profile to check if the 'connect desktop' step is completed
     */
    fun startPolling() {
        if (isPolling) {
            return
        }
        isPolling = true
        if (timer == null) {
            timer = TimerHelper(POLLING_INTERVAL_MS)
        }
        timer?.start()
        viewModelScope.launch {
            timer?.ticksFlow?.collect {
                if (_viewModelState.value.stepsStatus[Step.CONNECT_DESKTOP] == false) {
                    fetchProfile(isBackgroundCheck = true)
                } else {
                    stopPolling()
                }
            }
        }
    }

    fun stopPolling() {
        isPolling = false
        timer?.stop()
    }
}
