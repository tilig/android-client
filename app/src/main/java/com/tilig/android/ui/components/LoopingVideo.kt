package com.tilig.android.ui.components

import androidx.annotation.RawRes
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.viewinterop.AndroidView
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DataSpec
import com.google.android.exoplayer2.upstream.RawResourceDataSource
import com.tilig.android.R
import com.tilig.android.ui.theme.Blue4

@Composable
fun LoopingVideo(
    @RawRes id: Int,
    modifier: Modifier = Modifier
) {

    val context = LocalContext.current
    val exoPlayer = remember(context) {
        ExoPlayer.Builder(context).build().apply {

            val dataSpec = DataSpec(RawResourceDataSource.buildRawResourceUri(id))
            val dataSource = RawResourceDataSource(context)
            dataSource.open(dataSpec)
            val extractorsFactory = DefaultExtractorsFactory().setConstantBitrateSeekingEnabled(true)
            val dataFactory = DataSource.Factory { dataSource }
            dataSource.uri?.let {
                val mediaItem = MediaItem.Builder().setUri(it).build()
                val source = ProgressiveMediaSource
                    .Factory(dataFactory, extractorsFactory)
                    .createMediaSource(mediaItem)
                setMediaSource(source)
                prepare()
            }

            repeatMode = Player.REPEAT_MODE_ONE
        }
    }

    AndroidView(
        modifier = modifier,
        factory = {
            StyledPlayerView(context).apply {
                setShutterBackgroundColor(Blue4.toArgb())
                setBackgroundColor(Blue4.toArgb())
                player = exoPlayer
                exoPlayer.playWhenReady = true
                hideController()
                controllerAutoShow = false
            }
        }
    )
}

@Composable
@Preview
fun LoopingVideoPreview() {
    LoopingVideo(
        id = R.raw.video_autofill
    )
}
