package com.tilig.android.ui.onboarding.survey

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.tilig.android.ui.components.TiligRadioButton
import com.tilig.android.ui.theme.*

@Composable
fun SurveySecondQuestion(viewModel: SurveyViewModel) {
    val secondQuestion = remember { surveyQuestions[SECOND_QUESTION_KEY]!! }
    Column(
        modifier = Modifier
            .padding(horizontal = 32.dp)
            .fillMaxWidth()
    ) {
        Text(
            text = secondQuestion.content,
            style = Typography.h1.copy(fontSize = 24.sp, lineHeight = 32.sp, color = DarkBlue),
            textAlign = TextAlign.Start
        )
        val selection = remember {
            mutableStateOf(viewModel.getSecondQuestionAnswer())
        }
        SurveySecondQuestionsGroup(
            modifier = Modifier,
            secondQuestion.answerOptions,
            selection.value
        ) {
            viewModel.addAnswerToSecondQuestion(it)
            selection.value = it
        }
    }
}

@Composable
private fun SurveySecondQuestionCard(
    title: String,
    onItemClick: ((String) -> Unit),
    checked: Boolean
) {

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .heightIn(min = 64.dp)
            .background(
                color = BackgroundGrey,
                shape = RoundedCornerShape(DefaultCornerRadius)
            )
            .border(
                width = DefaultSmallBorder,
                shape = RoundedCornerShape(DefaultCornerRadius),
                color = if (checked) LightBlue else Grey
            )
            .clickable {
                onItemClick(title)
            },
        verticalAlignment = Alignment.CenterVertically
    ) {
        TiligRadioButton(
            selected = checked, onClick = { onItemClick(title) }, modifier = Modifier
                .size(20.dp)
                .padding(start = 24.dp, end = 12.dp)
        )
        Text(
            modifier = Modifier
                .padding(horizontal = 24.dp, vertical = 20.dp)
                .fillMaxWidth(),
            text = title,
            style = Typography.h3.copy(
                fontSize = 16.sp,
                color = if (checked) LightBlue else DarkBlue
            )
        )
    }
}

@Composable
private fun SurveySecondQuestionsGroup(
    modifier: Modifier,
    items: List<String>,
    selection: String?,
    onItemClick: ((String) -> Unit)
) {
    Column(modifier = modifier) {
        items.forEach { item ->
            Spacer(modifier = Modifier.height(16.dp))
            SurveySecondQuestionCard(
                onItemClick = onItemClick,
                title = item,
                checked = item == selection,
            )
        }
    }
}

@Preview
@Composable
fun SurveySecondQuestionPreview() {
    SurveySecondQuestion(viewModel = viewModel())
}