package com.tilig.android.ui.cards

import android.content.Context
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.platform.LocalFocusManager
import com.tilig.android.R
import com.tilig.android.data.models.tilig.CardType
import com.tilig.android.data.models.tilig.CustomField
import com.tilig.android.data.models.tilig.ItemFieldType
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.secrets.CustomFieldStateWrapper
import com.tilig.android.utils.CardUtils

class CardFieldsStateHolder(
    val focusManager: FocusManager,
    val focusRequester: FocusRequester,
    val nameState: TextFieldState,
    val cardNumberState: TextFieldState,
    val cardHolderState: TextFieldState,
    val cardExpireState: TextFieldState,
    val cardSecurityCodeState: TextFieldState,
    val cardPinState: TextFieldState,
    val cardZipCodeState: TextFieldState,
    val cardExtraInfoState: TextFieldState,
    val displayNameError: MutableState<Boolean>,
    val displayCardNumberError: MutableState<Boolean>,
    val displayCardExpireError: MutableState<Boolean>,
    var cardCustomFields: MutableState<List<CustomFieldStateWrapper>>
) {
    fun getCardNumberFieldMaxLength(): Int? {
        return when (CardUtils.identifyCardType(cardNumberState.text)) {
            CardType.AMEX -> 15
            CardType.DINERS_CLUB -> 14
            else -> null
        }
    }

    fun areAllRequiredFieldsSetup(
    ): Boolean {
        var allFieldsEntered = true
        displayNameError.value = nameState.text.isEmpty()
        displayCardNumberError.value =
            cardNumberState.text.isNotEmpty() && !CardUtils.isValidCardNumber(cardNumberState.text)
        displayCardExpireError.value =
            cardExpireState.text.isNotEmpty() && !CardUtils.isValidExpireDate(cardExpireState.text)
        if (displayNameError.value) {
            allFieldsEntered = false
        }
        return allFieldsEntered
    }

    fun validateCardNumber() {
        displayCardNumberError.value =
            cardNumberState.text.isNotEmpty() && !CardUtils.isValidCardNumber(cardNumberState.text)
    }

    fun addNewCustomField(context: Context, fieldType: ItemFieldType) {
        val updatedList: MutableList<CustomFieldStateWrapper> =
            cardCustomFields.value.toMutableList()
        val fieldName = when (fieldType) {
            ItemFieldType.TEXT -> R.string.custom_field_text
            ItemFieldType.PASSWORD -> R.string.custom_field_password
            ItemFieldType.SECRET -> R.string.custom_field_secret
            ItemFieldType.DATE -> R.string.custom_field_date
            ItemFieldType.TOTP -> R.string.custom_field_2fa
            else -> R.string.custom_field_secret
        }
        val fieldNameStr = context.getString(fieldName)
        val count = ((updatedList.filter { it.customField.kind == fieldType }.size).plus(1)).toString()
        val defaultName = "$fieldNameStr $count"
        updatedList.add(CustomFieldStateWrapper(CustomField(defaultName, fieldType, "")))
        cardCustomFields.value = updatedList
    }

    fun deleteField(field: CustomFieldStateWrapper){
        val updatedList: MutableList<CustomFieldStateWrapper> =
            cardCustomFields.value.toMutableList()
        updatedList.remove(field)
        cardCustomFields.value = updatedList
    }
}

@Composable
fun rememberCardFieldsStateHolder(
    focusManager: FocusManager = LocalFocusManager.current,
    focusRequester: FocusRequester = remember { FocusRequester() },
    displayNameError: MutableState<Boolean> = remember { mutableStateOf(false) },
    displayCardNumberError: MutableState<Boolean> = remember { mutableStateOf(false) },
    displayCardExpireError: MutableState<Boolean> = remember { mutableStateOf(false) },
    cardName: String,
    cardHolder: String,
    cardNumber: String,
    cardSecret: String,
    cardExpire: String,
    cardPin: String,
    cardZipCode: String,
    cardExtraInfo: String,
    cardCustomFields: List<CustomField>?
) = remember {
    CardFieldsStateHolder(
        focusManager = focusManager,
        focusRequester = focusRequester,
        nameState = TextFieldState(cardName, validator = {
            // on add flow show message only after click save button,
            // on edit page show immediately after losing focus if name is empty
            if (cardName.isNotEmpty()) {
                it.isNotEmpty()
            } else {
                !displayNameError.value
            }
        }),
        cardNumberState = TextFieldState(cardNumber, validator = { !displayCardNumberError.value }),
        cardHolderState = TextFieldState(cardHolder),
        cardExpireState = TextFieldState(cardExpire, validator = { !displayCardExpireError.value }),
        cardSecurityCodeState = TextFieldState(cardSecret),
        cardPinState = TextFieldState(cardPin),
        cardZipCodeState = TextFieldState(cardZipCode),
        cardExtraInfoState = TextFieldState(cardExtraInfo),
        displayNameError = displayNameError,
        displayCardNumberError = displayCardNumberError,
        displayCardExpireError = displayCardExpireError,
        cardCustomFields = mutableStateOf(
        cardCustomFields?.map { CustomFieldStateWrapper(it) } ?: emptyList()
        )
    )
}