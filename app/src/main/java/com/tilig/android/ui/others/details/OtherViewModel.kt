package com.tilig.android.ui.others.details

import com.tilig.android.data.models.SecretItemType
import com.tilig.android.data.models.tilig.CustomField
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.data.models.tilig.OtherItem
import com.tilig.android.data.models.tilig.SecretItem
import com.tilig.android.ui.notes.details.NoteEditWrapper
import com.tilig.android.ui.secrets.details.ItemDetailsViewModel
import com.tilig.android.ui.secrets.details.events.BaseEditEvent
import com.tilig.android.ui.secrets.details.states.ItemEditWrapper
import com.tilig.android.ui.secrets.details.states.ItemDetailsUiState
import kotlinx.coroutines.flow.update

data class OtherEditWrapper(
    val name: String? = null,
    override val customFields: List<CustomField>? = null,
    override val selectedFolder: Folder? = null
) : ItemEditWrapper {
    constructor(otherItem: OtherItem) : this(
        name = otherItem.name,
        customFields = otherItem.details.customFields,
        selectedFolder = otherItem.folder
    )

    override fun hadDataChanged(original: SecretItem): Boolean {
        return (original as OtherItem).name != name ||
                original.folder != selectedFolder ||
                original.details.customFields != customFields
    }

    override fun getItemName(): String? = name
}

sealed interface OtherEditEvents : BaseEditEvent {
    data class NameChanged(val name: String) : OtherEditEvents
}

class OtherViewModel : ItemDetailsViewModel() {

    override fun getSecretItemType(): SecretItemType = SecretItemType.OTHER

    override fun getItemEditWrapper(item: SecretItem?): ItemEditWrapper {
        return if (item != null) OtherEditWrapper(item as OtherItem) else OtherEditWrapper()
    }

    override fun updateCustomFields(customFields: List<CustomField>?): ItemEditWrapper {
        return (viewModelState.value.itemEditWrapper as OtherEditWrapper).copy(
            customFields = customFields
        )
    }

    override fun updateFolder(folder: Folder?): ItemEditWrapper {
        return (viewModelState.value.itemEditWrapper as OtherEditWrapper).copy(
            selectedFolder = folder
        )
    }

    override fun onEditEvent(event: BaseEditEvent) {
        if (event is OtherEditEvents) {
            viewModelState.update {
                it.copy(
                    itemEditWrapper =
                    when (event) {
                        is OtherEditEvents.NameChanged -> {
                            (it.itemEditWrapper as NoteEditWrapper).copy(
                                name = event.name
                            )
                        }
                    }
                )
            }
        } else {
            super.onEditEvent(event)
        }
    }

    override fun buildItemWithUpdatedItems(): SecretItem? {
        val currentState = uiState.value
        if (currentState is ItemDetailsUiState.ItemEdit) {
            val editable = currentState.itemEditWrapper as OtherEditWrapper
            return (currentState.item as OtherItem).apply {
                this.name = editable.name
                // ignore fields with empty names
                editable.customFields?.filter { it.name.isNotEmpty() }?.let {
                    this.details.customFields = it.toMutableList()
                }
                updateFolderField(this, editable.selectedFolder)
            }
        }
        return null
    }

    override fun trackDeleteItem() {
    }
}