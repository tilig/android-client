package com.tilig.android.ui.components

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.tilig.android.R

@ExperimentalAnimationApi
@Composable
fun SignInButton(
    modifier: Modifier,
    iconRes: Int,
    textRes: Int,
    enabled: Boolean,
    onClick: () -> Unit
) {
    AnimatedVisibility(
        visible = enabled,
        enter = fadeIn(),
        exit = fadeOut(),
        modifier = modifier) {

        TiligButton(
            modifier = Modifier,
            iconRes = iconRes,
            text = stringResource(textRes),
            enabled = enabled,
            onClick = onClick,
            isDark = false,
            extraPadding = false,
            isSizeConstrained = true
        )
    }
}

@ExperimentalAnimationApi
@Composable
@Preview
fun SignInButtonPreview() {
    SignInButton(
        modifier = Modifier,
        iconRes = R.drawable.ic_login_google,
        textRes = R.string.sign_in_with_google,
        enabled = true,
        onClick = {}
    )
}
