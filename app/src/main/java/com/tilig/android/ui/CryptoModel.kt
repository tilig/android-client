package com.tilig.android.ui

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tilig.android.data.livedata.ForcedLogoutLiveData
import com.tilig.android.utils.SharedPrefs
import com.tilig.crypto.Crypto
import com.tilig.crypto.KeyPairString
import com.tilig.crypto.KeyPairsString
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.component.inject


class CryptoModel(private val crypto: Crypto = Crypto()) : ViewModel(), KoinComponent {

    private val prefs: SharedPrefs by inject()
    private var isLoaded = false

    companion object {
        private const val TAG = "CryptoModel"
    }

    fun waitForKeys() = runBlocking {
        val keys = prefs.authKeys.first()
        Log.d(TAG, "Keys in this CryptoModel: $keys")
        return@runBlocking keys
    }

    /**
     * Gets the Crypto module, setting the keypair from the preferences.
     * This requires the keys to be already present in the SharedPrefs,
     * otherwise a stacktrace will be logged to Sentry.
     * Call [hasLoadedKeys] afterwards to check if the keys have successfully
     * loaded into the Crypto module
     */
    fun getInitializedCrypto() = crypto.apply {
        if (!isLoaded) {
            val authKeys = waitForKeys()
            isLoaded = setKeypair(authKeys)
        }
    }

    fun withInitializedCrypto(block: (crypto: Crypto) -> Unit) {
        val keys = waitForKeys()
        isLoaded = setKeypair(keys)
        val keyPairsCheck = crypto.getKeyPairs()
        Log.v(
            TAG,
            "Keypair check: \n  ${keyPairsCheck.keypair?.secretKey}\n  ${keyPairsCheck.keypair?.publicKey}\n  ${keyPairsCheck.legacy?.private}\n  ${keyPairsCheck.legacy?.public}"
        )
        if (crypto.hasKeys()) {
            block(crypto)
        } else {
            Log.e(TAG, "Somehow keys are gone, logging out (Crypto: ${crypto.hashCode()})", Throwable())
            val authStatus: ForcedLogoutLiveData = get()
            authStatus.forceLogout(true)
        }
    }

    fun uninitialize() {
        isLoaded = false
        crypto.uninitialize()
        viewModelScope.launch {
            prefs.clearAuthKeys()
        }
    }

    fun hasLoadedKeys() = isLoaded

    /**
     * Initializes the Crypto module with the given keypairs.
     * Either can be null, but for now (until we migrate and let go of legacy
     * completely) we will fallback to getting the legacy keypair from the old SharedPreferences
     */
    private fun setKeypair(keys: KeyPairsString): Boolean =
        if (keys.legacy?.private == null || keys.legacy?.public == null) {
            if (keys.keypair?.private != null && keys.keypair?.public != null) {
                Log.v(
                    TAG,
                    "setKeypair: (part of) legacy keypair is null (${keys.legacy?.private}, ${keys.legacy?.public}), but new keypair is okay. Checking old storage to be sure."
                )
                // Fetching from the old storage for just in case.
                val privKey = prefs.getString(SharedPrefs.AUTH_LEGACY_PRIVATE_KEY)
                val pubKey = prefs.getString(SharedPrefs.AUTH_LEGACY_PUBLIC_KEY)
                // Init the crypto with the modern keys from the params, (and possibly with old legacy keys as a bonus)
                SharedPrefs.initCryptoSafely(
                    legacyKeyPair = KeyPairString(privKey, pubKey),
                    keyPair = keys.keypair,
                    crypto = crypto
                )
            } else {
                Log.w(
                    TAG,
                    "setKeypair: (part of) legacy keypair is null (${keys.legacy?.private}, ${keys.legacy?.public}), and no new keypair is present",
                    Throwable()
                )
                setKeypairFromOldPrefs(keys.keypair)
            }
        } else {
            try {
                SharedPrefs.initCryptoSafely(
                    keyPair = keys.keypair,
                    legacyKeyPair = keys.legacy,
                    crypto
                )
            } catch (e: RuntimeException) {
                Log.e(TAG, "Cannot initCryptoSafely: ${e.message}", e)
                setKeypairFromOldPrefs(keys.keypair)
            }
        }

    private fun setKeypairFromOldPrefs(keyPair: KeyPairString?): Boolean {
        val privKey = prefs.getString(SharedPrefs.AUTH_LEGACY_PRIVATE_KEY)
        val pubKey = prefs.getString(SharedPrefs.AUTH_LEGACY_PUBLIC_KEY)
        Log.v(
            TAG,
            "setKeypairFromOldPrefs: logged in: ${prefs.isLoggedIn()}, keys in old prefs: ($privKey, $pubKey)"
        )
        return SharedPrefs.initCryptoSafely(
            legacyKeyPair = KeyPairString(privKey, pubKey),
            keyPair = keyPair,
            crypto = crypto
        )
    }

    fun generateNewKeys(includeLegacy: Boolean, overwrite: Boolean, callback: (Crypto) -> Unit) {
        viewModelScope.launch {
            crypto.apply {
                val keys = getKeyPairs()
                if (keys.keypair?.publicKey == null || keys.keypair?.secretKey == null || overwrite) {
                    generateKey()
                    prefs.updatePrivateKey(getKeyPairAsStrings()?.private)
                    prefs.updatePublicKey(getKeyPairAsStrings()?.public)
                }
                if (includeLegacy) {
                    if (keys.legacy?.public == null || keys.legacy?.private == null || overwrite) {
                        generateLegacyKey()
                        prefs.updateLegacyPrivateKey(legacyPrivateKeyAsString())
                        prefs.updateLegacyPublicKey(legacyPublicKeyAsString())
                    }
                }
            }
            callback(crypto)
        }
    }

    fun updateLegacyPrivateKey(key: String?, callback: ((KeyPairsString) -> Unit)? = null) {
        viewModelScope.launch {
            prefs.updateLegacyPrivateKey(legacyPrivateKey = key)
            callback?.invoke(waitForKeys())
        }
    }

    fun updateLegacyPublicKey(key: String?, callback: ((KeyPairsString) -> Unit)? = null) {
        viewModelScope.launch {
            prefs.updateLegacyPublicKey(legacyPublicKey = key)
            callback?.invoke(waitForKeys())
        }
    }

    fun updatePrivateKey(key: String?, callback: ((KeyPairsString) -> Unit)? = null) {
        viewModelScope.launch {
            prefs.updatePrivateKey(privateKey = key)
            callback?.invoke(waitForKeys())
        }
    }

    fun updatePublicKey(key: String?, callback: ((KeyPairsString) -> Unit)? = null) {
        viewModelScope.launch {
            prefs.updatePublicKey(publicKey = key)
            callback?.invoke(waitForKeys())
        }
    }
}
