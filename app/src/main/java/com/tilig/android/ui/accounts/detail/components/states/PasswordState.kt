package com.tilig.android.ui.accounts.detail.components.states

class PasswordState(val value: String?) :
    TextFieldState(
        fieldValue = value,
        validator = ::isPasswordValid,
        errorFor = ::passwordValidationError
    )

private fun isPasswordValid(password: String): Boolean {
    return password.length > 3
}

@Suppress("UNUSED_PARAMETER")
private fun passwordValidationError(password: String): String {
    return "Invalid password"
}
