package com.tilig.android.ui.secrets.details.states

import androidx.compose.runtime.MutableState
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.ui.secrets.CustomFieldStateWrapper

interface EditItemStateHolder {
    val customFields: List<CustomFieldStateWrapper>
    val selectedFolder: MutableState<Folder?>
}