@file:Suppress("IfThenToElvis")

package com.tilig.android.ui.onboarding.components

import android.app.Activity
import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.firebase.auth.OAuthProvider
import com.tilig.android.R
import com.tilig.android.ui.components.SignInButton
import com.tilig.android.ui.components.TiligButtonHeight
import com.tilig.android.ui.components.TiligSpacerVerticalDefault
import com.tilig.android.ui.signin.AuthClients
import com.tilig.android.ui.signin.SignInViewModel
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.UITestTags
import com.tilig.android.utils.elseLet
import com.tilig.android.utils.launchForResultSafely
import io.sentry.Sentry
import kotlinx.coroutines.launch
import org.koin.androidx.compose.get

private val MoreInfoRowHeight = 48.dp
private const val BUTTONS_AMOUNT = 3

@OptIn(ExperimentalMaterialApi::class, ExperimentalPagerApi::class, ExperimentalAnimationApi::class)
@Composable
fun OnboardingSignInView(
    bottomSheetScaffoldState: BottomSheetScaffoldState
) {

    ConstraintLayout(
        modifier = Modifier
            .fillMaxSize()
    ) {

        val (header, image, footer) = createRefs()

        Image(
            painter = painterResource(id = R.drawable.logo),
            contentDescription = stringResource(R.string.app_name),
            alignment = Alignment.Center,
            modifier = Modifier
                .padding(top = 48.dp)
                .fillMaxWidth()
                .height(48.dp)
                .constrainAs(header) {
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    top.linkTo(parent.top)
                },
        )

        Image(
            painter = painterResource(id = R.drawable.img_sign_in),
            contentDescription = "",
            modifier = Modifier
                .constrainAs(image) {
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    top.linkTo(header.bottom)
                    bottom.linkTo(footer.top)
                    width = Dimension.fillToConstraints
                    height = Dimension.fillToConstraints
                }
        )

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .constrainAs(footer) {
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    bottom.linkTo(parent.bottom)
                    width = Dimension.fillToConstraints
                }, verticalArrangement = Arrangement.Bottom
        ) {
            Text(
                text = stringResource(id = R.string.sign_in_title),
                style = TextStyle(
                    fontFamily = Spoof, fontSize = 28.sp, lineHeight = 32.sp,
                    color = DarkBlue, fontWeight = FontWeight.Bold
                ),
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = DefaultPadding)
            )
            Spacer(modifier = Modifier.height(12.dp))
            Text(
                text = stringResource(id = R.string.sign_in_desc),
                style = TextStyle(
                    fontFamily = StolzlBook, fontSize = 16.sp,
                    color = DarkBlue
                ),
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = DefaultPadding)
                    .padding(horizontal = DefaultPadding)
            )
            TiligSpacerVerticalDefault()

            SignInView(
                modifier = Modifier,
                bottomSheetScaffoldState = bottomSheetScaffoldState,
            )
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@ExperimentalAnimationApi
@ExperimentalPagerApi
@Composable
private fun SignInView(
    modifier: Modifier,
    viewModel: SignInViewModel = viewModel(),
    authClients: AuthClients = get(),
    bottomSheetScaffoldState: BottomSheetScaffoldState
) {
    val loadingState by viewModel.isLoading.collectAsState()
    val coroutineScope = rememberCoroutineScope()

    val googleAuthLauncher =
        rememberLauncherForActivityResult(contract = ActivityResultContracts.StartActivityForResult()) {
            val task = GoogleSignIn
                .getSignedInAccountFromIntent(it.data)
            viewModel.handleGoogleSignInResult(task)
        }

    val context = LocalContext.current

    Box(modifier = modifier) {
        Column(modifier = Modifier) {
            SignInButton(
                modifier = Modifier
                    .padding(horizontal = DoublePadding),
                iconRes = R.drawable.ic_login_google,
                textRes = R.string.sign_in_with_google,
                enabled = !loadingState
            ) {
                // sign in with Google
                val success =
                    googleAuthLauncher.launchForResultSafely(authClients.googleSignInClient.signInIntent)
                if (success) {
                    viewModel.setToLoading()
                } else {
                    viewModel.handleSignInError(R.string.error_login_firebase_activitynotfound)
                }
            }

            SignInButton(
                modifier = Modifier
                    .padding(top = DefaultPadding)
                    .padding(horizontal = DoublePadding),
                iconRes = R.drawable.ic_login_apple,
                textRes = R.string.sign_in_with_apple,
                enabled = !loadingState
            ) {
                signInWithApple(viewModel, authClients, context as? Activity?)
            }

            SignInButton(
                modifier = Modifier
                    .padding(vertical = DefaultPadding)
                    .padding(horizontal = DoublePadding),
                iconRes = R.drawable.ic_login_microsoft,
                textRes = R.string.sign_in_with_microsoft,
                enabled = !loadingState
            ) {
                signInWithMicrosoft(viewModel, authClients, context as? Activity?)
            }

            if (!loadingState) {
                Row(modifier = Modifier
                    .height(MoreInfoRowHeight)
                    .align(Alignment.CenterHorizontally)
                    .clickable {
                        coroutineScope.launch {
                            bottomSheetScaffoldState.bottomSheetState.expand()
                        }
                    }) {
                    Icon(
                        modifier = Modifier
                            .size(14.dp)
                            .align(Alignment.CenterVertically),
                        painter = painterResource(id = R.drawable.ic_question_mark),
                        tint = DimmedBlue,
                        contentDescription = null
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                    Text(
                        modifier = Modifier
                            .testTag(UITestTags.onboardingMoreInfoBtnTestTag)
                            .align(Alignment.CenterVertically),
                        text = stringResource(id = R.string.more_info_sign_in_header),
                        style = TextStyle(color = DimmedBlue, fontFamily = Stolzl, fontSize = 14.sp)
                    )
                }
            }
        }
        if (loadingState) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(TiligButtonHeight * BUTTONS_AMOUNT + MoreInfoRowHeight + DefaultPadding * BUTTONS_AMOUNT)
                    .background(color = White),
                contentAlignment = Alignment.BottomCenter
            ) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .width(DoublePadding),
                    color = ProgressIndicatorColor,
                    strokeWidth = 2.dp
                )
            }
        }
    }
}

fun signInWithApple(
    viewModel: SignInViewModel,
    authClients: AuthClients,
    activity: Activity?
) {
    activity?.let {
        viewModel.setToLoading()
        val provider = OAuthProvider.newBuilder("apple.com").apply {
            scopes = arrayListOf("email", "name")
        }

        val pending = authClients.auth.pendingAuthResult
        if (pending == null) {
            try {
                authClients.auth.startActivityForSignInWithProvider(activity, provider.build())
                    .addOnCompleteListener { task ->
                        viewModel.handleFirebaseLoginResult(task)
                    }
            } catch (e: Exception) {
                Sentry.captureMessage("Apple sign-in failed, error caught: ${e.message}")
                viewModel.handleSignInError(R.string.error_login_firebase_activitynotfound)
            }
        } else {
            pending.addOnCompleteListener { task ->
                viewModel.handleFirebaseLoginResult(task)
            }
        }
    }.elseLet {
        Log.e("OnboardingSignInView", "signInWithApple: Context was null")
    }
}

fun signInWithMicrosoft(
    viewModel: SignInViewModel,
    authClients: AuthClients,
    activity: Activity?
) {
    activity?.let {
        viewModel.setToLoading()
        val provider = OAuthProvider.newBuilder("microsoft.com")
        provider.addCustomParameter("prompt", "select_account")

        val pending = authClients.auth.pendingAuthResult
        if (pending == null) {
            try {
                authClients.auth.startActivityForSignInWithProvider(activity, provider.build())
                    .addOnCompleteListener {
                        viewModel.handleMicrosoftLoginResult(it)
                    }
            } catch (e: Exception) {
                Sentry.captureMessage("Microsoft sign-in failed, error caught: ${e.message}")
                viewModel.handleSignInError(R.string.error_login_firebase_activitynotfound)
            }
        } else {
            pending.addOnCompleteListener {
                viewModel.handleMicrosoftLoginResult(it)
            }
        }
    }.elseLet {
        Log.e("OnboardingSignInView", "signInWithMicrosoft: Context was null")
    }
}

@OptIn(ExperimentalMaterialApi::class)
@ExperimentalAnimationApi
@ExperimentalPagerApi
@Composable
@Preview
fun OnboardingSignInViewPreview() {
    OnboardingSignInView(
        bottomSheetScaffoldState = rememberBottomSheetScaffoldState(),
    )
}
