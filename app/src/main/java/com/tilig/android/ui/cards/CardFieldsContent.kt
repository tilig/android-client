package com.tilig.android.ui.cards

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.components.*
import com.tilig.android.ui.secrets.components.AddCustomField
import com.tilig.android.ui.secrets.components.AddSecretItemNameAndLogoView
import com.tilig.android.ui.secrets.components.AllCustomFieldsEditComponent
import com.tilig.android.ui.secrets.components.TitledContent
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.CardUtils

@Composable
fun ColumnScope.CardFieldsContent(
    fieldsState: CardFieldsStateHolder,
    showExtraInfoField: Boolean = true,
    showCardIcon: Boolean = false,
    supportCustomFields: Boolean = false
) {

    val context = LocalContext.current
    if (showCardIcon) {
        AddSecretItemNameAndLogoView(
            icon = painterResource(id = R.drawable.ic_credit_card),
            hintInputRes = R.string.hint_card_card_name,
            focusManager = fieldsState.focusManager,
            focusRequester = fieldsState.focusRequester,
            nameState = fieldsState.nameState,
            onValueChanged = {
                fieldsState.displayNameError.value = false
            })
    } else {
        TiligSpacerVerticalMedium()
        Box {
            BorderedBasicTextField(
                modifier = Modifier.padding(top = if (fieldsState.nameState.isValid) DefaultErrorMessageHeight else 0.dp),
                errorHandler = ErrorHandler(
                    errorMessage = stringResource(id = R.string.error_name),
                    errorColor = DefaultErrorColor
                ),
                singleLine = false,
                hint = stringResource(id = R.string.hint_card_card_name),
                state = fieldsState.nameState,
                textModifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 12.dp, vertical = 4.dp),
                keyboardType = KeyboardType.Text,
                capitalization = KeyboardCapitalization.Sentences,
                onNext = {
                    fieldsState.focusManager.moveFocus(FocusDirection.Down)
                },
                onValueChanged = {
                    fieldsState.displayNameError.value = false
                }
            )
            FieldTitle(
                title = stringResource(id = R.string.label_card_name),
                modifier = Modifier
            )
        }
    }

    TiligSpacerVerticalMedium()
    Box {
        BorderedBasicTextField(
            modifier = Modifier.padding(top = if (fieldsState.cardNumberState.isValid) DefaultErrorMessageHeight else 0.dp),
            hint = stringResource(id = R.string.hint_card_number),
            state = fieldsState.cardNumberState,
            showContentInitially = true,
            keyboardType = KeyboardType.Number,
            onNext = {
                fieldsState.focusManager.moveFocus(FocusDirection.Down)
            },
            onValueChanged = {
                fieldsState.displayCardNumberError.value = false
            },
            onFocusLost = {
                fieldsState.validateCardNumber()
            },
            hideVisualTransformation = CardUtils.CardNumberTransformation(CardUtils.BULLET),
            defaultVisualTransformation = CardUtils.CardNumberTransformation(null),
            showRevealButton = fieldsState.cardNumberState.isFocused,
            maxLength = fieldsState.getCardNumberFieldMaxLength(),
            errorHandler = ErrorHandler(
                errorMessage = stringResource(id = R.string.error_card_number_invalid),
                errorColor = DefaultWarnColor
            )
        )
        FieldTitle(
            title = stringResource(id = R.string.label_card_number),
            modifier = Modifier
        )
    }

    TiligSpacerVerticalMedium()
    Box {
        BorderedBasicTextField(
            modifier = Modifier.padding(top = if (fieldsState.cardHolderState.isValid) DefaultErrorMessageHeight else 0.dp),
            hint = stringResource(id = R.string.hint_card_cardholder_name),
            state = fieldsState.cardHolderState,
            onNext = {
                fieldsState.focusManager.moveFocus(FocusDirection.Next)
            },
            capitalization = KeyboardCapitalization.Words,
        )
        FieldTitle(
            title = stringResource(id = R.string.label_card_holder_name),
            modifier = Modifier
        )
    }

    TiligSpacerVerticalMedium()
    Box(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        BorderedBasicTextField(
            modifier = Modifier.padding(top = if (fieldsState.cardExpireState.isValid) DefaultErrorMessageHeight else 0.dp),
            hint = stringResource(id = R.string.hint_card_expire_date),
            state = fieldsState.cardExpireState,
            keyboardType = KeyboardType.Number,
            defaultVisualTransformation = { date ->
                CardUtils.formatExpireDate(date)
            },
            showContentInitially = true,
            onValueChanged = {
                fieldsState.displayCardExpireError.value = false
            },
            onNext = {
                fieldsState.focusManager.moveFocus(FocusDirection.Next)
            },
            errorHandler = ErrorHandler(
                errorMessage = stringResource(id = R.string.error_expire_date),
                errorColor = DefaultWarnColor
            ),
            maxLength = 4
        )
        FieldTitle(
            title = stringResource(id = R.string.label_expire_date),
            modifier = Modifier
        )
    }

    TiligSpacerVerticalMedium()
    Box(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        BorderedBasicTextField(
            modifier = Modifier.padding(top = if (fieldsState.cardSecurityCodeState.isValid) DefaultErrorMessageHeight else 0.dp),
            hint = stringResource(id = R.string.hint_card_secret_code),
            state = fieldsState.cardSecurityCodeState,
            hideVisualTransformation = PasswordVisualTransformation(),
            showRevealButton = fieldsState.cardSecurityCodeState.isFocused,
            onNext = {
                fieldsState.focusManager.moveFocus(FocusDirection.Next)
            },
            keyboardType = KeyboardType.Number,
            maxLength = 4,
        )
        FieldTitle(
            title = stringResource(id = R.string.label_secret_code),
            modifier = Modifier
        )
    }

    TiligSpacerVerticalMedium()
    Box(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        BorderedBasicTextField(
            modifier = Modifier.padding(top = DefaultErrorMessageHeight),
            hint = stringResource(id = R.string.hint_card_pin_code),
            state = fieldsState.cardPinState,
            hideVisualTransformation = PasswordVisualTransformation(),
            showRevealButton = fieldsState.cardPinState.isFocused,
            keyboardType = KeyboardType.Number,
            maxLength = 4,
            onNext = {
                fieldsState.focusManager.moveFocus(FocusDirection.Next)
            }
        )
        FieldTitle(
            title = stringResource(id = R.string.label_pin_code),
            modifier = Modifier
        )
    }

    TiligSpacerVerticalMedium()
    Box(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        BorderedBasicTextField(
            modifier = Modifier.padding(top = DefaultErrorMessageHeight),
            hint = stringResource(id = R.string.hint_card_zip_code),
            state = fieldsState.cardZipCodeState,
            onNext = {
                fieldsState.focusManager.moveFocus(FocusDirection.Next)
            }
        )
        FieldTitle(
            title = stringResource(id = R.string.label_zip_code),
            modifier = Modifier
        )
    }

    if (showExtraInfoField) {
        TiligSpacerVerticalMedium()
        TitledContent(
            modifier = Modifier,
            title = stringResource(id = R.string.label_extra_info),
            titleTextStyle = Typography.h3,
        ) {
            BorderedBasicTextField(
                modifier = Modifier,
                textStyle = StyleStolzRegularDarkBlue16,
                hint = stringResource(id = R.string.hint_card_extra_info),
                state = fieldsState.cardExtraInfoState,
                singleLine = false,
                textModifier = Modifier
                    .fillMaxWidth()
                    .padding(12.dp),
                keyboardType = KeyboardType.Text,
                capitalization = KeyboardCapitalization.Sentences,
            )
        }
    }

    if (supportCustomFields) {
        AllCustomFieldsEditComponent(
            focusManager = fieldsState.focusManager,
            fieldsWrapper = fieldsState.cardCustomFields.value,
            onFieldsChanged = {}) {
            fieldsState.deleteField(it)
        }

        TiligSpacerVerticalDefault()
        AddCustomField(modifier = Modifier.fillMaxWidth()) {
            fieldsState.addNewCustomField(context, it)
        }
    }
}