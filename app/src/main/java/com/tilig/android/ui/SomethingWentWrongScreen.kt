package com.tilig.android.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.components.AppendableText
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.components.buildAnnotatedString
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.IntentsBuilder

@Composable
fun SomethingWentWrongScreen(
    modifier: Modifier = Modifier,
    whatWasWrongText: String,
    onTryAgainClick: () -> Unit
) {
    val context = LocalContext.current
    val descriptionAppendable = listOf(
        AppendableText(
            stringResource(
                id = R.string.could_not_load,
                whatWasWrongText
            ),
            false
        ),
        AppendableText(
            stringResource(
                id = R.string.contact_support_dot,
            ), true, tag = "contact us", annotation = "contact us"
        )
    )
    val annotatedString = buildAnnotatedString(
        appendableTexts = descriptionAppendable,
        spanStyle = SpanStyle(
            LightBlue,
            fontWeight = FontWeight.Bold,
            textDecoration = TextDecoration.Underline
        )
    )

    Column(
        modifier = modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Image(
            modifier = Modifier,
            painter = painterResource(id = R.drawable.ic_free_your_mind),
            contentDescription = "",
            contentScale = ContentScale.FillWidth,
        )
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp),
            text = stringResource(id = R.string.smth_went_wrong),
            style = Typography.h4,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center,
            fontSize = FontSizeLarge,
            color = DefaultTextColor
        )
        ClickableText(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = DefaultPadding),
            text = annotatedString,
            style = Typography.body1.copy(color = DarkGrey, textAlign = TextAlign.Center),
            onClick = {
                annotatedString.getStringAnnotations(it, it).firstOrNull()?.let {
                    if (it.tag == descriptionAppendable[1].tag) {
                        context.startActivity(IntentsBuilder.getEmailSendIntent())
                    }
                }
            }
        )
        TiligButton(
            modifier = Modifier
                .align(alignment = Alignment.CenterHorizontally)
                .padding(
                    start = DefaultPadding,
                    end = DefaultPadding,
                    top = 32.dp,
                    bottom = 32.dp
                )
                .height(48.dp),
            iconRes = R.drawable.ic_repeat,
            text = stringResource(R.string.btn_try_again),
            enabled = true,
            isSizeConstrained = false,
            isDark = false,
            onClick = onTryAgainClick
        )
    }
}