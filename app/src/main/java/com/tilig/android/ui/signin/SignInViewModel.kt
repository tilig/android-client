package com.tilig.android.ui.signin

import android.content.Context
import android.util.Log
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.*
import com.google.firebase.functions.FirebaseFunctionsException
import com.tilig.android.BuildConfig
import com.tilig.android.R
import com.tilig.android.analytics.Breadcrumb
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.*
import com.tilig.android.data.models.firebase.DecryptKeypairRequest
import com.tilig.android.data.models.firebase.EncryptKeypair
import com.tilig.android.data.models.firebase.EncryptKeypairRequest
import com.tilig.android.data.models.firebase.KeypairMeta
import com.tilig.android.data.models.tilig.EncryptedKeyPair
import com.tilig.android.data.models.tilig.OkResponse
import com.tilig.android.data.models.tilig.Profile
import com.tilig.android.data.models.tilig.ProfileData
import com.tilig.android.data.repository.Repository
import com.tilig.android.featureflag.FeatureFlagClient
import com.tilig.android.ui.CryptoModel
import com.tilig.android.utils.DebugLogger
import com.tilig.android.utils.SharedPrefs
import com.tilig.crypto.Crypto
import com.tilig.crypto.KeyPairString
import com.tilig.crypto.KeyPairsString
import io.sentry.Sentry
import io.sentry.SentryLevel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

sealed interface SignInUiState {

    val errorMessage: UiError?

    data class SignInError(
        override val errorMessage: UiError? = null
    ) : SignInUiState

    data class SignInSuccess(
        override val errorMessage: UiError? = null
    ) : SignInUiState

    data class Onboarding(
        override val errorMessage: UiError? = null
    ) : SignInUiState

    data class SignInLoading(
        override val errorMessage: UiError? = null
    ) : SignInUiState
}

data class SignInViewModelState(
    val response: ApiResponse<Profile>? = null,
    val errorMessage: Int? = null,
    val onboardingFinished: Boolean
) {

    /**
     * Converts this [SignInViewModelState] into a more strongly typed [SignInUiState] for driving
     * the ui.
     */
    fun toUiState(): SignInUiState =
        when {
            errorMessage != null -> SignInUiState.SignInError(UiError(msgResource = errorMessage))
            else -> {
                when (response) {
                    null -> {
                        if (onboardingFinished) SignInUiState.SignInLoading() else SignInUiState.Onboarding()
                    }
                    is ApiEmptyResponse -> SignInUiState.SignInError(
                        UiError(msgResource = R.string.error_login_empty_response)
                    )
                    is ApiErrorResponse -> SignInUiState.SignInError(
                        UiError(msgResource = R.string.error_login_error_response)
                    )
                    is ApiSuccessResponse -> {
                        SignInUiState.SignInSuccess()
                    }
                }
            }
        }
}

open class SignInViewModel : ViewModel(), KoinComponent {

    private var onKeysCreatedOrFetched: ((Boolean, KeyPairsString) -> Unit)? = null
    private val repo by inject<Repository>()
    private val authClients by inject<AuthClients>()
    private val prefs by inject<SharedPrefs>()
    private val tracker by inject<Mixpanel>()
    private val featureFlagClient by inject<FeatureFlagClient>()
    private val viewModelState =
        MutableStateFlow(SignInViewModelState(onboardingFinished = prefs.hasFinishedOnboarding))
    private var firebaseUser: FirebaseUser? = null
    private var firebaseUserPlus: AdditionalUserInfo? = null
    val isLoading = MutableStateFlow(false)
    val cryptoModel: CryptoModel by inject()
    private val debugLogger = DebugLogger()

    // UI state exposed to the UI
    val uiState = viewModelState
        .map { it.toUiState() }
        .stateIn(
            viewModelScope,
            SharingStarted.Eagerly,
            viewModelState.value.toUiState()
        )

    companion object {
        const val FIRESTORE_COLLECTION = "keys"
        const val TAG = "SignIn"
    }

    fun debug(message: String) {
        debugLogger.collect(message)
    }

    fun attemptSilentLogin() {
        // Check if Firebase is already authenticated
        val currentUser = authClients.auth.currentUser
        if (currentUser != null && prefs.firebaseUserId != null) {
            debug("Silent login not needed, already logged in")
            fetchJWT()
        } else {
            debug("Trying silent login")
            // Check if we can silently log the user in
            authClients.googleSignInClient
                .silentSignIn()
                .addOnCompleteListener { task ->
                    handleGoogleSignInResult(task, silent = true)
                }
        }
    }

    fun setToLoading(enabled: Boolean = true) {
        isLoading.value = enabled
    }

    fun finishOnboarding() {
        if (!viewModelState.value.onboardingFinished) {
            tracker.trackEvent(Tracker.EVENT_WELCOME_COMPLETED)
            prefs.hasFinishedOnboarding = true
            viewModelState.update {
                it.copy(
                    onboardingFinished = true
                )
            }
        }
    }

    // MutableStateFlow won't emit an update if the old and the new values are the same.
    // So if we don't call this method, and the same error happens 2 consecutive times, the snackbar won't show up the second time.
    fun resetErrorState() {
        viewModelState.update {
            it.copy(errorMessage = null)
        }
    }

    private fun fetchJWT() {
        debug("fetching JWT")
        authClients.functions
            .getHttpsCallable("generateTiligToken")
            .call()
            .continueWith { task ->
                if (task.isSuccessful) {
                    val json = task.result?.data as Map<*, *>
                    try {
                        val token = json["token"] as String
                        debug("fetching JWT successful: $token")
                        authenticate(token)
                    } catch (e: Exception) {
                        Log.w(TAG, "Unable to read JWT", e)
                        debug("fetching JWT successful, but cannot read the JWT")
                        viewModelState.update {
                            it.copy(
                                errorMessage = R.string.error_login_unable_to_read_jwt_1
                            )
                        }
                    }
                } else {
                    val e = task.exception
                    if (e is FirebaseFunctionsException) {
                        val code = e.code
                        val details = e.details
                        Log.w(TAG, "Unable to read JWT ($code - $details)", e)
                        debug("fetching JWT failed ($code - $details)")
                        viewModelState.update {
                            it.copy(
                                errorMessage = R.string.error_login_unable_to_read_jwt_2
                            )
                        }
                    } else {
                        Log.w(TAG, "Unable to fetch JWT", e)
                        debug("fetching JWT failed (${e?.message})")
                        viewModelState.update {
                            it.copy(
                                errorMessage = R.string.error_login_unable_to_read_jwt_3
                            )
                        }
                    }
                }
            }
    }

    private fun authenticate(jwt: String) {
        viewModelScope.launch {
            // Set it in the prefs so the [AuthorizationInterceptor] picks it up
            prefs.refreshToken = null
            prefs.jwtToken = jwt
            repo.authenticateCall().enqueue(object : Callback<TokenExchangeResponse> {
                override fun onResponse(
                    call: Call<TokenExchangeResponse>,
                    response: Response<TokenExchangeResponse>
                ) {
                    if (response.isSuccessful) {
                        debug("authenticate successful")
                        response.body()?.let { res ->
                            prefs.accessToken = res.accessToken
                            prefs.refreshToken = res.refreshToken
                            prefs.jwtToken = null
                            val keys = cryptoModel.waitForKeys()
                            debug("legacy keys present in cryptomodel? priv: ${keys.legacy?.private != null}, pub: ${keys.legacy?.public != null}")
                            getOrGenerateKeyPairs(keys)
                        }
                    } else {
                        debug("authenticate failed")
                        Breadcrumb.drop(Breadcrumb.CryptoTrail_LoginFailure_1)
                        Log.w(TAG, "Authenticate call failed, error response")
                        viewModelState.update {
                            it.copy(errorMessage = R.string.error_login_backend_response_1)
                        }
                    }
                }

                override fun onFailure(call: Call<TokenExchangeResponse>, t: Throwable) {
                    Breadcrumb.drop(Breadcrumb.CryptoTrail_LoginFailure_2)
                    Log.w(TAG, "Authenticate call failed")
                    debug("authenticate failed with error ${t.message}")
                    viewModelState.update {
                        it.copy(errorMessage = R.string.error_login_backend_response_2)
                    }
                }
            })
        }
    }

    /**
     * Kicks off a flow that ensures all keys (legacy or new) are either generated or fetched from
     * the backend. Does nothing if keys are already in memory.
     * @param existingKeys The keys as found in local storage. If those are good, we don't need to do anything
     * @param continueToFinishLogin Set to `false` if we're not in a login flow. Set to `true` when we are, so we can take the proper steps once login is finished.
     * @param callback Only needed when we are not in the login flow. This is called once all keys are present. Useful for upgrading existing users to the new keypairs without having them log in again.
     */
    fun getOrGenerateKeyPairs(
        existingKeys: KeyPairsString,
        continueToFinishLogin: Boolean = true,
        callback: ((Boolean, KeyPairsString) -> Unit)? = null
    ) {
        val hasLegacyKeys = existingKeys.legacy?.private != null
                && existingKeys.legacy?.public != null
        val hasNewKeys = existingKeys.keypair?.public != null
                && existingKeys.keypair?.private != null
        onKeysCreatedOrFetched = callback
        if (hasLegacyKeys && hasNewKeys) {
            debug("not generating keys, there are existing keys")
            if (continueToFinishLogin) {
                finishLogin(existingKeys)
            }
            onKeysCreatedOrFetched?.invoke(false, existingKeys)
        } else if (hasLegacyKeys) {
            debug("generating NEW keys, there are existing LEGACY keys")
            Breadcrumb.drop(Breadcrumb.CryptoTrail_FetchingPrivateKey_NewOnly)
            fetchFromTiligServer(firebaseId = null)
        } else {
            debug("generating BOTH keys, legacy and new")
            Breadcrumb.drop(Breadcrumb.CryptoTrail_FetchingPrivateKey_Both)

            // Check Firestore for existing LEGACY private key
            val firebaseId = prefs.firebaseUserId!!
            authClients.db
                .collection(FIRESTORE_COLLECTION)
                .document(firebaseId)
                .get()
                .addOnSuccessListener { document ->
                    // Make sure we're online. If we have a key, but it's from the cache,
                    // we might actually be logged out and start using an old key from a previous session
                    if (document.metadata.isFromCache) {
                        debug("prevented loading legacy keys from firebase cache")
                        Breadcrumb.drop(Breadcrumb.CryptoTrail_FirestoreError_FetchingKey)
                        viewModelState.update {
                            it.copy(
                                errorMessage = R.string.error_login_firebase_failure
                            )
                        }
                    } else if (document?.data != null) {
                        // If we have a keys: great! We update the cryptomodel to use this one,
                        // then we'll fetch the associated public key from the Tilig server
                        Log.d(TAG, "DocumentSnapshot data: ${document.data}")
                        debug("found legacy private key in firebase")
                        val key = document.data?.get("privateKey") as? String
                        if (key != null) {
                            Breadcrumb.drop(Breadcrumb.CryptoTrail_FetchingPublicKey)
                        }
                        cryptoModel.updateLegacyPrivateKey(key)
                        fetchFromTiligServer(firebaseId)
                    } else {
                        // If we have no keys: no problem. We'll make some.
                        // We'll also create the new keypair and send it to the cloudfunctions and to the backend
                        Log.d(TAG, "No such document")
                        debug("no (legacy) keys found in firebase")
                        Breadcrumb.drop(Breadcrumb.CryptoTrail_FirestoreSuccess_CreatingKeypair)
                        createKeyPair(firebaseId)
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d(TAG, "get failed with ", exception)
                    debug("fetching from firebase failed ${exception.message}")
                    Breadcrumb.drop(Breadcrumb.CryptoTrail_FirestoreError_CreatingKeypair)
                    viewModelState.update {
                        it.copy(
                            errorMessage = R.string.error_login_firebase_failure
                        )
                    }
                }
        }
    }

    private fun sendKeyPairToCloudFunctionForEncryption(
        keys: KeyPairString,
        onSuccess: (String) -> Unit
    ) {
        debug("sending encrypted keypair")
        val request = EncryptKeypairRequest(
            EncryptKeypair(
                private_key = keys.private!!,
                public_key = keys.public!!
            ),
            KeypairMeta("android", BuildConfig.VERSION_NAME)
        ).toFirebaseModel()
        authClients.functions
            .getHttpsCallable("encryptKeypair")
            .call(request)
            .continueWith { task ->
                if (task.isSuccessful) {
                    val json = task.result?.data as Map<*, *>
                    try {
                        val token = json["token"] as String
                        debug("fetching keypair successful: token = $token")
                        onSuccess(token)
                    } catch (e: Exception) {
                        Log.w(TAG, "Unable to read keypair", e)
                        debug("fetching JWT successful, but cannot read the keypair")
                        viewModelState.update {
                            it.copy(
                                errorMessage = R.string.error_login_unable_to_read_keypair_1
                            )
                        }
                    }
                } else {
                    val e = task.exception
                    if (e is FirebaseFunctionsException) {
                        val code = e.code
                        val details = e.details
                        debug("fetching keypair failed ($code - $details): ${e.message}")
                        viewModelState.update {
                            it.copy(
                                errorMessage = R.string.error_login_unable_to_read_keypair_2
                            )
                        }
                    } else {
                        debug("fetching keypair failed: ${e?.message}")
                        viewModelState.update {
                            it.copy(
                                errorMessage = R.string.error_login_unable_to_read_keypair_3
                            )
                        }
                    }
                }
            }
    }

    private fun fetchDecryptedKeyPairFromCloudFunctions(
        encryptedKeyPair: EncryptedKeyPair,
        callback: (KeyPairsString) -> Unit
    ) {
        debug("fetching decrypted keypair from cloud function")
        val request = DecryptKeypairRequest(encryptedKeyPair.encryptedPrivateKey).toFirebaseModel()
        authClients.functions
            .getHttpsCallable("decryptPrivateKey")
            .call(request)
            .continueWith { task ->
                if (task.isSuccessful) {
                    val json = task.result?.data as Map<*, *>
                    try {
                        val privateKey = json["private_key"] as String
                        debug("fetching decrypted keypair successful: $privateKey")
                        // Store the private key locally without overwriting the legacy one
                        cryptoModel.updatePrivateKey(privateKey, callback)
                    } catch (e: Exception) {
                        debug("fetching decrypted keypair successful, but cannot read the keypair: ${e.message}")
                        viewModelState.update {
                            it.copy(
                                errorMessage = R.string.error_login_unable_to_read_keypair_1
                            )
                        }
                    }
                } else {
                    val e = task.exception
                    if (e is FirebaseFunctionsException) {
                        val code = e.code
                        val details = e.details
                        debug("fetching decrypted keypair failed ($code - $details): ${e.message}")
                        Log.e(
                            TAG,
                            "fetching decrypted keypair failed ($code - $details): ${e.message}"
                        )
                        viewModelState.update {
                            it.copy(
                                errorMessage = R.string.error_login_unable_to_read_keypair_2
                            )
                        }
                    } else {
                        debug("fetching keypair failed: ${e?.message}")
                        Log.e(TAG, "fetching keypair failed: ${e?.message}")
                        viewModelState.update {
                            it.copy(
                                errorMessage = R.string.error_login_unable_to_read_keypair_3
                            )
                        }
                    }
                }
            }
    }

    // This is the final call to update the state, unless an error has occurred
    private fun finishLogin(authKeys: KeyPairsString) {
        debug("finished login")

        if (BuildConfig.DEBUG) {
            Log.i(TAG, "=== AUTH SUMMARY ========================")
            Log.i(TAG, "Access token:  ${prefs.accessToken}")
            Log.i(TAG, "Refresh token: ${prefs.refreshToken}")
            Log.i(TAG, "JWT:           ${prefs.jwtToken}")
            Log.i(TAG, "Firebase UID:  ${prefs.firebaseUserId}")
            Log.i(
                TAG,
                "Legacy keys?   ${authKeys.legacy?.private != null}, ${authKeys.legacy?.public != null}"
            )
            Log.i(
                TAG,
                "New keys?      ${authKeys.keypair?.private != null}, ${authKeys.keypair?.public != null}"
            )
            Log.i(TAG, "=========================================")
        }

        // Debug logging
        when {
            authKeys.legacy?.private.isNullOrEmpty() && authKeys.legacy?.public.isNullOrEmpty() ->
                Breadcrumb.drop(Breadcrumb.CryptoTrail_PostLogin_BothKeysEmpty)
            authKeys.legacy?.private.isNullOrEmpty() ->
                Breadcrumb.drop(Breadcrumb.CryptoTrail_PostLogin_PrivKeyEmpty)
            authKeys.legacy?.public.isNullOrEmpty() ->
                Breadcrumb.drop(Breadcrumb.CryptoTrail_PostLogin_PubKeyEmpty)
            else ->
                Breadcrumb.drop(Breadcrumb.CryptoTrail_PostLogin_KeysOk)
        }
        if (authKeys.keypair?.public != null && authKeys.keypair?.private != null) {
            Breadcrumb.drop(Breadcrumb.CryptoTrail_PostLogin_NewKeysArePresent)
        }

        val request = ProfileData(
            display_name = firebaseUser?.displayName,
            given_name = firebaseUserPlus?.profile?.get("given_name") as? String,
            family_name = firebaseUserPlus?.profile?.get("family_name") as? String,
            picture = firebaseUserPlus?.profile?.get("picture") as? String,
            locale = Locale.getDefault().toLanguageTag(),
            provider_id = firebaseUserPlus?.providerId,
            public_key = authKeys.legacy?.public
        )

        // sync user profile
        viewModelScope.launch {
            val result = repo.createProfile(request)

            if (result is ApiSuccessResponse) {
                val userId = result.body.id
                prefs.mixpanelUserId = userId
                tracker.setIdentity(userId)
                withContext(Dispatchers.IO) {
                    featureFlagClient.setIdentity(userId)
                }
                Breadcrumb.identify(prefs)

                // Remember this for later. Yes, we can fetch it from teh API, but having it in
                // the preferences is much faster and works offline:
                prefs.email = result.body.email
            } else {
                Log.w("ANALYTICS", "Unable to sync user profile. Mixpanel.identity will not work!")
            }

            viewModelState.update {
                it.copy(response = result)
            }
        }
    }

    fun saveDebug(context: Context) {
        debugLogger.logCollection(context)
    }

    /**
     * Fetches the profile from the backend. The profile contains the
     * legacy public key (`public_key`) and the new (encrypted) libsodium keypair.
     *
     * Omit the firebaseId to focus only on the new libsodium keys.
     */
    private fun fetchFromTiligServer(firebaseId: String? = null) {
        val includeLegacy = firebaseId != null
        repo.fetchProfile().enqueue(object : Callback<Profile> {
            override fun onFailure(call: Call<Profile>, t: Throwable) {
                Breadcrumb.drop(Breadcrumb.CryptoTrail_CannotFetchPublicKey)
                debug("unable to fetch public keys from backend: ${t.message}")
                Sentry.captureMessage("unable to fetch public keys from backend: ${t.message}")
                viewModelState.update {
                    it.copy(
                        errorMessage = R.string.error_login_key_from_backend
                    )
                }
            }

            override fun onResponse(
                call: Call<Profile>,
                response: Response<Profile>
            ) {
                val profile = response.body()
                // While were here fetching the profile, we should update some prefs according to the
                // user settings in the profile
                prefs.isSurveyFinished = profile?.userSettings?.isSurveyFinished ?: false
                prefs.isDesktopConnected = profile?.applicationSettings?.isDesktopConnected ?: false
                prefs.hasOnboardedBefore = profile?.userSettings?.hasOnboardedBefore ?: false

                // Update the keys in the crypto module with the one in the profile (if any)
                val encryptedKeyPair = profile?.keypair
                if (encryptedKeyPair != null) {
                    debug("new keys found in backend")
                    Breadcrumb.drop(Breadcrumb.CryptoTrail_ProfileResponse_NewKeysPresent)
                    // Store new pub key locally, and use the cloud functions to decrypt 'em
                    cryptoModel.updatePublicKey(encryptedKeyPair.publicKey) {
                        fetchDecryptedKeyPairFromCloudFunctions(encryptedKeyPair) {
                            if (includeLegacy) {
                                updateLegacyPublicKey(profile.public_key, firebaseId!!)
                            }
                            onKeysCreatedOrFetched?.invoke(true, it)
                        }
                    }
                } else if (includeLegacy) {
                    Breadcrumb.drop(Breadcrumb.CryptoTrail_ProfileResponse_NoKeysPresent)
                    // No new keys? Let's store the legacy public key locally first. updateLegacyPublicKey
                    // will take care of the rest of the flow (creating and encrypting the new keys)
                    updateLegacyPublicKey(profile?.public_key, firebaseId!!)
                } else {
                    // No new keys? If we're ignoring the legacy code, we'll create and store
                    // the libsodium keys
                    Breadcrumb.drop(Breadcrumb.CryptoTrail_ProfileResponse_OnlyLegacyKeysPresent)
                    createKeyPair(firebaseId = null, includeLegacy = false)
                }
            }
        })
    }

    private fun updateLegacyPublicKey(publicKey: String?, firebaseId: String) {
        // Update legacy public key in the crypto module with the one in the profile
        cryptoModel.updateLegacyPublicKey(publicKey) { keys ->
            if (keys.legacy?.public.isNullOrBlank()) {
                Breadcrumb.drop(Breadcrumb.CryptoTrail_CreatingKeypair)
                debug("found null or empty legacy public key on backend")
                createKeyPair(firebaseId)
            } else {
                debug("found legacy public key")
                finishLogin(keys)
            }
        }
    }

    private fun createKeyPair(firebaseId: String?, includeLegacy: Boolean = true) {
        // If includeLegacy is true, we MUST have the firebaseId
        assert(firebaseId != null || !includeLegacy)
        // This creates legacy and/or new keys, depending on which ones are already known locally
        cryptoModel.generateNewKeys(includeLegacy = includeLegacy, overwrite = false) {
            if (includeLegacy) {
                debug("created new keys, including legacy")
                storeLegacyPrivateKeyInFirestore(it, firebaseId!!)
                storeLegacyPublicKeyInTiligServer(
                    it.getKeyPairsAsStrings()
                )
            } else {
                debug("created new keys, only the new libsodium ones")
            }
            it.getKeyPairAsStrings()?.let { keypair ->
                sendKeyPairToCloudFunctionForEncryption(keypair) { token ->
                    // Note: we don't continue to finish login if includeLegacy is true, because the legacy block
                    // above will already take care of continuing to the finishLogin
                    storeEncryptedKeypairInTiligServer(
                        token,
                        continueToFinishLogin = !includeLegacy,
                        authKeys = it.getKeyPairsAsStrings()
                    )
                }
            }
        }
    }

    private fun storeLegacyPrivateKeyInFirestore(crypto: Crypto, firebaseId: String) {
        // Sure, the prefs should already be set at this point, but of course
        // there can be timing issues when user logs in and out. So, we pass on
        // an immutable firebaseId in the arguments, and re-set the prefs here.
        // Should fix the !!-related NPE we used to have (Sentry: ANDROID-APP-96)
        prefs.firebaseUserId = firebaseId
        authClients.db
            .collection(FIRESTORE_COLLECTION)
            .document(firebaseId)
            .set(
                hashMapOf(
                    "uid" to prefs.firebaseUserId,
                    "privateKey" to crypto.legacyPrivateKeyAsString(),
                    "platform" to "android",
                    "version" to BuildConfig.VERSION_NAME,
                    "algorithm" to crypto.getAlgorithmName()
                )
            )
            .addOnSuccessListener {
                debug("Legacy key successfully saved in Firebase")
                viewModelScope.launch {
                    prefs.updateLegacyPrivateKey(crypto.legacyPrivateKeyAsString())
                }
            }
            .addOnFailureListener { e ->
                debug("Unable to store key in Firebase: ${e.message}")
                viewModelState.update {
                    it.copy(errorMessage = R.string.error_login_storing_key_in_firebase)
                }
            }
    }

    private fun storeEncryptedKeypairInTiligServer(
        token: String,
        authKeys: KeyPairsString?,
        continueToFinishLogin: Boolean
    ) {
        debug("storing encrypted keypair in backend")
        repo.storeEncryptedKeypair(token).enqueue(object : Callback<OkResponse> {
            override fun onFailure(call: Call<OkResponse>, t: Throwable) {
                debug("Unable to store encrypted keypair in backend: ${t.message}")
                viewModelState.update {
                    it.copy(errorMessage = R.string.error_login_storing_key_in_backend)
                }
            }

            override fun onResponse(
                call: Call<OkResponse>,
                response: Response<OkResponse>
            ) {
                debug("Successfully stored encrypted keypair in backend: ${response.code()} ${response.message()}")
                if (authKeys != null && continueToFinishLogin) {
                    finishLogin(authKeys)
                }
                authKeys?.let {
                    onKeysCreatedOrFetched?.invoke(true, it)
                    viewModelScope.launch {
                        prefs.updatePrivateKey(it.keypair?.private)
                        prefs.updatePublicKey(it.keypair?.public)
                    }
                }
            }
        })
    }

    private fun storeLegacyPublicKeyInTiligServer(authKeys: KeyPairsString) {
        debug("Storing legacy public key in backend")
        authKeys.legacy?.public?.let { key ->
            repo
                .updatePublicKey(key)
                .enqueue(object : Callback<OkResponse> {
                    override fun onFailure(call: Call<OkResponse>, t: Throwable) {
                        debug("Unable to store legacy public key in backend")
                        viewModelState.update {
                            it.copy(errorMessage = R.string.error_login_storing_legacy_key_in_backend)
                        }
                    }

                    override fun onResponse(
                        call: Call<OkResponse>,
                        response: Response<OkResponse>
                    ) {
                        debug("Legacy public key stored in backend")
                        viewModelScope.launch {
                            prefs.updateLegacyPublicKey(authKeys.legacy?.public)
                        }
                        finishLogin(authKeys)
                    }
                })
        } ?: finishLogin(authKeys)
    }

    private fun loginToFirebase(idToken: String?) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        authClients.auth.signInWithCredential(credential)
            .addOnCompleteListener {
                handleFirebaseLoginResult(it)
            }
    }

    fun handleMicrosoftLoginResult(task: Task<AuthResult>) {
        if (task.isSuccessful && task.result.user != null && task.result.user?.isEmailVerified == false) {
            Log.w(TAG, "MicrosoftLogin: verify email")
            task.result.user!!.sendEmailVerification()
                .addOnCompleteListener {
                    authClients.auth.signOut()
                    viewModelState.update {
                        it.copy(errorMessage = R.string.error_login_firebase_verify_email)
                    }
                }
        } else {
            handleFirebaseLoginResult(task)
        }
    }

    fun handleSignInError(@StringRes message: Int) {
        viewModelState.update {
            it.copy(errorMessage = message)
        }
    }

    fun handleFirebaseLoginResult(task: Task<AuthResult>) {
        if (task.isSuccessful) {
            // Sign in success, update UI with the signed-in user's information
            if (task.result != null) {
                prefs.firebaseUserId = task.result.user?.uid
                firebaseUser = task.result.user
                firebaseUserPlus = task.result.additionalUserInfo
                fetchJWT()
            } else {
                // If sign in fails, display a message to the user.
                debug("signInWithCredential: task successful but result is null")
                handleSignInError(R.string.error_login_firebase_login_result_1)
            }
        } else {
            // If sign in fails, display a message to the user.
            debug("signInWithCredential: failure ${task.exception}")
            handleSignInError(
                if (task.exception is FirebaseAuthUserCollisionException)
                    R.string.error_login_firebase_login_collision else R.string.error_login_firebase_login_result_2
            )
        }
    }

    fun handleGoogleSignInResult(
        task: Task<GoogleSignInAccount?>,
        silent: Boolean = false
    ): Boolean {
        try {
            task.getResult(ApiException::class.java)?.let { account ->
                debug("handleGoogleSigninResult: task contains account ${account.idToken}")
                loginToFirebase(account.idToken)
                return true
            }
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult: failed code=" + e.statusCode)
            Sentry.captureMessage(
                "Failed to google sign in: failed code = : ${e.statusCode}",
                SentryLevel.WARNING
            )
            if (silent) {
                debug("handleGoogleSigninResult: task threw error ${e.statusCode}, silently ignoring")
            } else {
                debug("handleGoogleSigninResult: task threw error ${e.statusCode}")
                Breadcrumb.drop(Breadcrumb.CryptoTrail_GoogleResultFailure)
                // getStatusCodeString does not always return a human-readable error,
                // like DEVELOPER_ERROR or NETWORK_ERROR
                val message = when (e.statusCode) {
                    // This is an inexplicable error that happened once. The meaning of
                    // status code 4 is nowhere to be found in the docs
                    4 -> R.string.error_login_google_generic
                    7 -> R.string.error_login_google_connection
                    10 -> R.string.error_login_google_developer_error
                    else -> {
                        //Sentry.captureMessage(GoogleSignInStatusCodes.getStatusCodeString(e.statusCode), SentryLevel.INFO)
                        R.string.error_login_google_generic
                    }
                }
                viewModelState.update {
                    it.copy(errorMessage = message)
                }
            }
        }
        return false
    }
}
