package com.tilig.android.ui.autofill.components

import com.tilig.android.data.models.tilig.Account

sealed class AutofillPopupActions {
    data class FillSelected(val account: Account) : AutofillPopupActions()
    data class ChosenAccount(val account: Account) : AutofillPopupActions()
    object ClosePopup : AutofillPopupActions()
    object ReturnToAccountsList : AutofillPopupActions()
    object RefreshAutofillAccountsList: AutofillPopupActions()
    data class StartAddAccount(val account: Account) : AutofillPopupActions()
}
