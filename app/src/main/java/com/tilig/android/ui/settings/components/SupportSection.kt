package com.tilig.android.ui.settings.components

import androidx.annotation.StringRes
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForwardIos
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.components.FieldTitle
import com.tilig.android.ui.settings.SettingsUrl
import com.tilig.android.ui.theme.*

@Composable
@Preview
fun SupportSectionPreview() {
    SupportSection({}, {})
}

@Composable
fun SupportSection(openLink: (SettingsUrl) -> Unit, sendMessageToSupport: () -> Unit) {
    FieldTitle(
        title = stringResource(id = R.string.label_support),
        titlePadding = 8.dp,
        style = StyleSpoofMediumDarkBlue20
    )
    Column(
        modifier = Modifier
            .background(color = White, shape = RoundedCornerShape(MediumCornerRadius))
            .fillMaxWidth(),
    ) {
        SupportRow(titleId = R.string.label_faq, descriptionId = R.string.faq_desc, onClick = {openLink.invoke(SettingsUrl.FAQ)})

        Divider(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = DefaultPadding)
        )

        SupportRow(
            titleId = R.string.label_get_in_touch,
            descriptionId = R.string.get_in_touch_desc,
            onClick = sendMessageToSupport)
    }
}

@Composable
private fun SupportRow(
    @StringRes titleId: Int,
    @StringRes descriptionId: Int,
    onClick: () -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onClick.invoke() }
            .padding(16.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Column(
            modifier = Modifier
                .padding(end = 16.dp)
                .weight(1f)
        ) {
            Text(
                text = stringResource(id = titleId),
                style = StyleStolzRegularDarkBlue14,
                color = DefaultTextColor,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
            Text(
                modifier = Modifier.alpha(0.5f),
                text = stringResource(id = descriptionId),
                style = StyleStolzBookDarkBlue12,
                color = DefaultTextColor,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        }
        Icon(
            modifier = Modifier.size(16.dp),
            imageVector = Icons.Filled.ArrowForwardIos,
            contentDescription = "",
            tint = SettingsArrowColor
        )
    }
}
