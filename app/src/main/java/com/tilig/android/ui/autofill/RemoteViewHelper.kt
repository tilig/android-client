package com.tilig.android.ui.autofill

import android.content.Context
import android.graphics.*
import android.net.Uri
import android.view.View
import android.widget.RemoteViews
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.compose.ui.graphics.toArgb
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.AppWidgetTarget
import com.tilig.android.R
import com.tilig.android.ui.theme.DarkBlue
import com.tilig.android.ui.theme.DefaultIconColor

object RemoteViewHelper {

    private const val REMOTE_FAVICONS = false

    fun newRemoteViews(
        context: Context,
        @StringRes remoteViewsTextRes: Int,
        remoteSubText: String?,
        @DrawableRes drawableId: Int? = null,
        imageUrl: String? = null
    ): RemoteViews = newRemoteViews(
        context,
        context.getString(remoteViewsTextRes),
        remoteSubText,
        drawableId,
        imageUrl
    )

    fun newRemoteFooter(
        context: Context
    ): RemoteViews = RemoteViews(
        context.packageName,
        R.layout.li_autofill_footer
    )

    fun newRemoteViews(
        context: Context,
        remoteViewsText: String,
        remoteSubText: String?,
        @DrawableRes drawableId: Int? = null,
        imageUrl: String? = null
    ): RemoteViews {

        val presentation: RemoteViews
        if (!remoteSubText.isNullOrBlank()) {
            presentation = RemoteViews(
                context.packageName,
                R.layout.li_autofill_subtext
            )
            presentation.setTextViewText(R.id.tv_subtitle, remoteSubText)
        } else {
            presentation = RemoteViews(
                context.packageName,
                R.layout.li_autofill
            )
        }

        presentation.setTextViewText(R.id.tv_title, remoteViewsText)

        // Load the image, either from the resources,
        // or use Glide to load it from the net. Or both! In which case
        // the one in the resources is a placeholder
        if (drawableId != null) {
            val color = DefaultIconColor.toArgb()
            //setIconWithTint(context, presentation, R.id.iv_icon, drawableId, color)
            presentation.setImageViewResource(R.id.iv_icon, drawableId)
            presentation.setViewVisibility(R.id.iv_icon, View.VISIBLE)
            presentation.setInt(R.id.iv_icon, "setColorFilter", color)
        }

        // Load image through Glide
        if (imageUrl != null && REMOTE_FAVICONS) {
            val uri: Uri = Uri.parse(imageUrl)
            // This fails. The only RemoteViews that Glide supports are
            // widgets (abused here) or notifications. Both of those can be
            // refreshed after loading the image. The Autofill popup can not!
            // So while Glide happily loads the image and puts it in the RemoteView,
            // there is no way to refresh it, and the icon will not appear until
            // after the user closes the popup and reopens it.
            // Which is more confusing than anything, so REMOTE_FAVICONS is set to `false`
            // until we find a way.
            val remoteTarget = AppWidgetTarget(
                context,
                R.id.iv_icon,
                presentation,
                0
            )
            Glide.with(context)
                .asBitmap()
                .load(uri)
                .into(remoteTarget)
        }

        return presentation
    }

    private fun setIconWithTint(
        context: Context,
        view: RemoteViews,
        @IdRes viewId: Int,
        @DrawableRes imageRes: Int,
        @ColorInt color: Int
    ) {

        val source = BitmapFactory.decodeResource(context.resources, imageRes)
        // Make a mutable copy. Since we use the same properties, this will not actually be a
        // copy of the whole bitmap, it will just be a mutable one sharing the same memory
        val result = Bitmap.createBitmap(
            source,
            0,
            0,
            source.width,
            source.height
        )
        val p = Paint()
        val filter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN)
        p.colorFilter = filter

        val canvas = Canvas(result)
        canvas.drawBitmap(result, 0f, 0f, p)

        view.setBitmap(viewId, "setImageBitmap", result)
    }
}
