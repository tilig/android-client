package com.tilig.android.ui.cards.add

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.tilig.android.R
import com.tilig.android.data.models.tilig.CreditCard
import com.tilig.android.data.models.tilig.Folders
import com.tilig.android.ui.cards.CardFieldsContent
import com.tilig.android.ui.components.TiligSpacerVerticalDefault
import com.tilig.android.ui.secrets.add.AddToFolderComponent
import com.tilig.android.ui.secrets.components.SaveSecretItemButton
import com.tilig.android.ui.theme.DefaultPadding
import com.tilig.android.ui.theme.HalfPadding

@Composable
fun AddCardContent(
    folders: Folders?,
    state: AddCardStateHolder = rememberAddCardStateHolder(),
    onSaveCard: (CreditCard) -> Unit
) {

    val fieldState = state.cardFieldsStateHolder

    Column(
        modifier = Modifier
            .padding(
                start = DefaultPadding,
                end = DefaultPadding,
                bottom = HalfPadding
            )
            .fillMaxSize()
            .verticalScroll(state.scrollableState)
            .imePadding()
    ) {
        CardFieldsContent(fieldState, showExtraInfoField = false, showCardIcon = true)

        TiligSpacerVerticalDefault()
        AddToFolderComponent(
            modifier = Modifier,
            folders = folders,
            selectedFolder = state.selectedFolder.value,
            onFolderChanged = { state.selectedFolder.value = it }
        )

        TiligSpacerVerticalDefault()
        SaveSecretItemButton(
            modifier = Modifier.align(alignment = Alignment.CenterHorizontally),
            text = stringResource(id = R.string.btn_save_card),
            onSave = {
                if (state.isSaveButtonEnabled()) {
                    onSaveCard(
                        state.buildCreditCardModel()
                    )
                }
            }
        )
    }
}