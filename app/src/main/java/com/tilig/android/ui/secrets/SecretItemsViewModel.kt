package com.tilig.android.ui.secrets

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.viewModelScope
import com.tilig.android.data.livedata.ConnectivityLiveData
import com.tilig.android.data.models.*
import com.tilig.android.data.models.tilig.*
import com.tilig.android.data.repository.Repository
import com.tilig.android.testdrive.TestDrive
import com.tilig.android.ui.CryptoModel
import com.tilig.android.utils.getDomain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.koin.core.component.inject
import java.util.*

sealed interface SecretItemsUiState {

    val isLoading: Boolean
    val secretItems: List<SecretItem>
    val displayType: SecretItemType?

    data class SecretItemsError(
        override val isLoading: Boolean,
        val errorMessages: List<UiError>,
        override val secretItems: List<SecretItem>,
        override val displayType: SecretItemType?
    ) : SecretItemsUiState

    data class SecretItemsSuccess(
        override val isLoading: Boolean,
        override val secretItems: List<SecretItem>,
        override val displayType: SecretItemType?,
        val filteredSecretItemsByDisplayType: Map<SecretItemType?, List<SecretItem>>,
        // when no matches need to know amount of existing items for the current display type
        val totalNoSearchSecretItemsAmount: Int,
        // indicates if user exported custom items
        val hasCustomItems: Boolean,
        val isRefreshing: Boolean
    ) : SecretItemsUiState
}

data class SecretItemsViewModelState(
    val secretItems: ApiResponse<List<SecretItem>>? = null,
    // Filter secrets query
    val searchFilter: String? = null,
    // if null display all
    val displayType: SecretItemType? = null,
    val hasCustomItems: Boolean = false,
    val isLoading: Boolean = false,
    val isRefreshing: Boolean = false
) {

    /**
     *
     * Converts this [SecretItemsViewModelState] into a more strongly typed [SecretItemsUiState] for driving
     * the ui.
     */
    fun toUiState(): SecretItemsUiState =
        when (secretItems) {
            null,
            is ApiEmptyResponse -> SecretItemsUiState.SecretItemsSuccess(
                isLoading = secretItems?.let { isLoading } ?: true,
                secretItems = listOf(),
                displayType = displayType,
                totalNoSearchSecretItemsAmount = 0,
                filteredSecretItemsByDisplayType = emptyMap(),
                isRefreshing = isRefreshing,
                hasCustomItems = hasCustomItems
            )
            is ApiErrorResponse -> SecretItemsUiState.SecretItemsError(
                isLoading = isLoading,
                secretItems = listOf(),
                errorMessages = listOf(
                    secretItems.toUiError()
                ),
                displayType = displayType
            )
            is ApiSuccessResponse -> {
                if (secretItems.body.isEmpty()) {
                    SecretItemsUiState.SecretItemsSuccess(
                        isLoading = isLoading,
                        secretItems = listOf(),
                        displayType = displayType,
                        totalNoSearchSecretItemsAmount = 0,
                        filteredSecretItemsByDisplayType = emptyMap(),
                        isRefreshing = isRefreshing,
                        hasCustomItems = hasCustomItems
                    )
                } else {
                    val sortedItems =
                        secretItems.body.sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER) { it.overview.name ?: "" })
                    val searchTextLowerCase = searchFilter
                        ?.lowercase(Locale.getDefault())

                    val allItems = mutableListOf<SecretItem>()
                    val allAccounts = mutableListOf<SecretItem>()
                    val allNotes = mutableListOf<SecretItem>()
                    val allCards = mutableListOf<SecretItem>()
                    val allWifi = mutableListOf<SecretItem>()
                    val allCustoms = mutableListOf<SecretItem>()

                    val filteredItems = mutableListOf<SecretItem>()
                    val filteredAccounts = mutableListOf<SecretItem>()
                    val filteredNotes = mutableListOf<SecretItem>()
                    val filteredCards = mutableListOf<SecretItem>()
                    val filteredWifi = mutableListOf<SecretItem>()
                    val filteredCustoms = mutableListOf<SecretItem>()

                    if (searchTextLowerCase == null) {
                        allItems.addAll(sortedItems)
                        allAccounts.addAll(sortedItems.filterIsInstance(Account::class.java))
                        allNotes.addAll(sortedItems.filterIsInstance(Note::class.java))
                        allCards.addAll(sortedItems.filterIsInstance(CreditCard::class.java))
                        allWifi.addAll(sortedItems.filterIsInstance(WifiPassword::class.java))
                        allCustoms.addAll(sortedItems.filterIsInstance(OtherItem::class.java))

                        filteredItems.addAll(allItems)
                        filteredAccounts.addAll(allAccounts)
                        filteredNotes.addAll(allNotes)
                        filteredCards.addAll(allCards)
                        filteredWifi.addAll(allWifi)
                        filteredCustoms.addAll(allCustoms)

                    } else {
                        // go over each item, add it to grouped by type maps and if it satisfies search
                        // add to grouped by type search map results (needed for correctly setup of
                        // empty screens and tabs items count indicators)
                        sortedItems.forEach { item ->
                            allItems.add(item)
                            when (item) {
                                is Account -> {
                                    allAccounts.add(item)
                                    if (item.name
                                            ?.lowercase(Locale.getDefault())
                                            ?.contains(searchTextLowerCase) != false
                                        || item.website.getDomain()
                                            ?.lowercase(Locale.getDefault())
                                            ?.contains(searchTextLowerCase) == true
                                    ) {
                                        filteredItems.add(item)
                                        filteredAccounts.add(item)
                                    }
                                }
                                is Note -> {
                                    allNotes.add(item)
                                    if (item.name?.lowercase(Locale.getDefault())
                                            ?.contains(searchTextLowerCase) != false
                                    ) {
                                        filteredItems.add(item)
                                        filteredNotes.add(item)
                                    }
                                }
                                is CreditCard -> {
                                    allCards.add(item)
                                    if (item.name?.lowercase(Locale.getDefault())
                                            ?.contains(searchTextLowerCase) != false
                                    ) {
                                        filteredItems.add(item)
                                        filteredCards.add(item)
                                    }
                                }
                                is WifiPassword -> {
                                    allWifi.add(item)
                                    if (item.name?.lowercase(Locale.getDefault())
                                            ?.contains(searchTextLowerCase) != false
                                    ) {
                                        filteredItems.add(item)
                                        filteredWifi.add(item)
                                    }
                                }
                                is OtherItem -> {
                                    allCustoms.add(item)
                                    if (item.name?.lowercase(Locale.getDefault())
                                            ?.contains(searchTextLowerCase) != false
                                    ) {
                                        filteredItems.add(item)
                                        filteredCustoms.add(item)
                                    }
                                }
                            }
                        }
                    }

                    val allItemsByDisplayType = mutableMapOf(
                        null to allItems.size,
                        SecretItemType.ACCOUNT to allAccounts.size,
                        SecretItemType.NOTE to allNotes.size,
                        SecretItemType.CREDIT_CARD to allCards.size,
                        SecretItemType.WIFI to allWifi.size,
                        SecretItemType.OTHER to allCustoms.size,
                    )
                    val filteredItemsByDisplayType = mutableMapOf(
                        null to filteredItems.toList(),
                        SecretItemType.ACCOUNT to filteredAccounts.toList(),
                        SecretItemType.NOTE to filteredNotes.toList(),
                        SecretItemType.CREDIT_CARD to filteredCards.toList(),
                        SecretItemType.WIFI to filteredWifi.toList(),
                        SecretItemType.OTHER to filteredCustoms.toList()
                    )

                    SecretItemsUiState.SecretItemsSuccess(
                        isLoading = isLoading,
                        secretItems = filteredItemsByDisplayType[displayType]!!,
                        displayType = displayType,
                        filteredSecretItemsByDisplayType = filteredItemsByDisplayType.toMap(),
                        totalNoSearchSecretItemsAmount = allItemsByDisplayType[displayType]!!,
                        hasCustomItems = (allItemsByDisplayType[SecretItemType.OTHER] ?: 0) > 0,
                        isRefreshing = isRefreshing
                    )
                }
            }
        }

    fun toList(): List<SecretItem> =
        when (secretItems) {
            is ApiEmptyResponse -> listOf()
            is ApiErrorResponse -> listOf()
            is ApiSuccessResponse -> {
                secretItems.body.sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER) { it.overview.name ?: "" })
                    .ifEmpty {
                        listOf()
                    }
            }
            else -> listOf()
        }
}

class SecretItemsViewModel : BaseSecretsListViewModel(), DefaultLifecycleObserver {

    private val repo by inject<Repository>()
    private val cryptoModel: CryptoModel by inject()

    val connectivity by inject<ConnectivityLiveData>()

    private val viewModelState = MutableStateFlow(SecretItemsViewModelState())

    // UI state exposed to the UI
    val uiState = viewModelState
        .map { it.toUiState() }
        .stateIn(
            viewModelScope,
            SharingStarted.Eagerly,
            viewModelState.value.toUiState()
        )

    fun myAccount() = repo.fetchProfile()

    fun filterByType(type: SecretItemType?) {
        viewModelState.update {
            it.copy(displayType = type)
        }
    }

    fun refresh() {
        viewModelState.update {
            it.copy(isRefreshing = true)
        }
        secretItems()
    }

    /**
     * Fetches items and updates the uiState upon return.
     * The list of items can be complete (both arguments null) or filtered by a search query.
     * @param query when given, is used as a text search filter on the list of items
     */
    fun secretItems(query: String? = null) {
        viewModelState.update {
            it.copy(isLoading = true)
        }
        viewModelScope.launch(Dispatchers.IO) {
            val encryptedAccounts = repo.secretItems()
            cryptoModel.withInitializedCrypto { crypto ->
                // why not to move this into repository ?
                val decryptedAccounts =
                    (encryptedAccounts as? ApiSuccessResponse)?.body?.items?.map {
                        it.decrypt(
                            repo.jsonConverter,
                            crypto
                        )
                    }
                val decryptedResponse = ApiResponse.createCloneWithNewBody(
                    encryptedAccounts,
                    decryptedAccounts
                )
                viewModelState.update {
                    it.copy(
                        secretItems = decryptedResponse,
                        searchFilter = query,
                        isLoading = false,
                        isRefreshing = false
                    )
                }
                runMigrations {
                    secretItems(query)
                }
            }
        }
    }

    override fun onResume(owner: LifecycleOwner) {
        super.onResume(owner)
        secretItems()
    }

    fun lookupItem(id: String?): SecretItem? {
        val item = viewModelState.value.toList().firstOrNull { it.id == id }
        item?.decryptDetails(jsonConverter = repo.jsonConverter, cryptoModel.getInitializedCrypto())
        return item
    }

    fun searchForSecretItems(searchText: String) {
        viewModelScope.launch(Dispatchers.IO) {
            viewModelState.update {
                it.copy(
                    searchFilter = searchText,
                )
            }
        }
    }
}
