package com.tilig.android.ui.onboarding.survey

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.lifecycle.viewmodel.compose.viewModel
import com.tilig.android.R
import com.tilig.android.ui.components.TiligCheckBox
import com.tilig.android.ui.theme.*

@Composable
fun SurveyFinished(viewModel: SurveyViewModel) {
    ConstraintLayout(
        modifier = Modifier
    ) {
        val (content, fineForInterview) = createRefs()

        ShortChartCheckBoxView(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 32.dp)
                .constrainAs(fineForInterview) {
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }, viewModel = viewModel
        )

        Column(
            Modifier
                .constrainAs(content) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    bottom.linkTo(fineForInterview.top)
                    height = Dimension.preferredWrapContent
                }
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Image(
                modifier = Modifier
                    .width(270.dp)
                    .height(204.dp),
                painter = painterResource(id = R.drawable.first_account_success),
                contentDescription = "",
                contentScale = ContentScale.FillWidth
            )
            Text(
                modifier = Modifier
                    .padding(top = 8.dp, bottom = 24.dp),
                textAlign = TextAlign.Center,
                text = stringResource(id = R.string.survey_thanks),
                style = Typography.h1.copy(
                    color = DarkBlue,
                    fontSize = 28.sp,
                    lineHeight = 32.sp
                ),
            )
            Text(
                modifier = Modifier
                    .padding(horizontal = 32.dp),
                text = stringResource(id = R.string.survey_finished_desc),
                style = Typography.body1.copy(color = DarkBlue, fontSize = 16.sp),
                textAlign = TextAlign.Center,
            )
        }


    }
}

@Composable
private fun ShortChartCheckBoxView(modifier: Modifier, viewModel: SurveyViewModel) {
    val checked = remember {
        mutableStateOf(true)
    }
    viewModel.agreeOnChart(checked.value)

    Column(
        modifier = modifier
            .background(
                color = BackgroundGrey,
                shape = RoundedCornerShape(DefaultCornerRadius)
            )
            .border(
                width = DefaultSmallBorder,
                shape = RoundedCornerShape(DefaultCornerRadius),
                color = Grey
            )
            .padding(DefaultPadding)
    ) {
        Row(modifier = Modifier.height(24.dp), verticalAlignment = Alignment.CenterVertically) {
            TiligCheckBox(
                checked = checked.value,
                onCheckedChange = {
                    checked.value = it
                },
                modifier = Modifier.size(16.dp)
            )
            Text(
                modifier = Modifier
                    .padding(start = 8.dp),
                text = stringResource(id = R.string.survey_available_for_chat),
                style = Typography.h2.copy(color = DarkBlue, fontSize = 14.sp),
                textAlign = TextAlign.Left,
            )
        }
        Text(
            modifier = Modifier.padding(top = 4.dp),
            text = stringResource(id = R.string.survey_available_for_chat_desc),
            style = Typography.h3.copy(color = DimmedBlue, fontSize = 12.sp, lineHeight = 16.sp),
            textAlign = TextAlign.Left,
        )
    }
}

@Composable
@Preview
fun SurveyFinishedPreview() {
    SurveyFinished(viewModel = viewModel())
}