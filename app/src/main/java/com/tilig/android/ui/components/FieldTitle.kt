package com.tilig.android.ui.components

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.tilig.android.ui.theme.Typography

@Composable
fun FieldTitle(
    modifier: Modifier = Modifier,
    title: String,
    style: TextStyle = Typography.h3,
    titlePadding: Dp = 4.dp
) {
    Text(
        text = title,
        modifier.padding(bottom = titlePadding),
        style = style
    )
}

@Composable
@Preview(showBackground = true)
fun FieldTitlePreview() {
    FieldTitle(
        title = "Lorem Ipsum"
    )
}
