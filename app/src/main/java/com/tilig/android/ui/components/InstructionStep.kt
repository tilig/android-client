package com.tilig.android.ui.components

import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.httpify

@Composable
fun InstructionStep(
    index: Int,
    annotatedString: List<AppendableText>,
    tag: String? = null
) {

    val context = LocalContext.current

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = HalfPadding),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Box(
            modifier = Modifier
                .background(color = Blue4, shape = CircleShape)
                .size(24.dp),
            contentAlignment = Alignment.Center
        ) {
            Text(
                modifier = Modifier,
                text = index.toString(),
                style = StyleStolzMediumDarkBlue14.copy(
                    fontSize = 12.sp,
                    textAlign = TextAlign.Center,
                    color = Neutral40
                )
            )
        }

        ClickableText(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = DefaultPadding),
            text = buildAnnotatedString(
                appendableTexts = annotatedString,
                spanStyle = SpanStyle(fontWeight = FontWeight.Bold)
            ),
            style = StyleStolzRegularDarkBlue14,
            onClick = {
                tag?.let {
                    annotatedString.forEach {
                        if (it.tag == tag) {
                            context.startActivity(
                                Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse(it.text.httpify())
                                )
                            )
                        }
                    }
                }
            }
        )
    }
}

@Composable
@Preview
fun InstructionStepPreview() {
    InstructionStep(0, listOf(AppendableText(text = "Text", isStyled = false)))
}