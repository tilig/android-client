package com.tilig.android.ui.accounts.detail.components

import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tilig.android.R
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.models.tilig.HistoryEntry
import com.tilig.android.data.models.tilig.History
import com.tilig.android.ui.components.ScreenHeader
import com.tilig.android.ui.modifiers.dropShadow
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.VersionUtil
import com.tilig.android.utils.copyToClipboard

@Composable
fun PasswordHistoryBottomSheet(
    history: History,
    tracker: Tracker,
    closeSheet: () -> Unit,
    restorePassword: (String) -> Unit
) {
    val bottomSheetHeight = LocalConfiguration.current.screenHeightDp.dp

    Column(
        modifier = Modifier
            .heightIn(max = bottomSheetHeight)
            .navigationBarsPadding()
    ) {
        ScreenHeader(
            headerTextRes = R.string.password_history,
            closeSheet = closeSheet
        )
        LazyColumn(modifier = Modifier) {
            items(history, key = { item ->
                item
            }) { item ->
                PasswordHistoryItemNew(
                    passwordVersion = item,
                    tracker = tracker,
                    restorePassword = restorePassword,
                    closeSheet = closeSheet
                )
            }
        }
    }

    BackHandler {
        closeSheet.invoke()
    }
}

@Composable
private fun PasswordHistoryItemNew(
    passwordVersion: HistoryEntry,
    tracker: Tracker,
    restorePassword: (String) -> Unit,
    closeSheet: () -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(64.dp)
            .padding(horizontal = DefaultPadding),
        verticalAlignment = Alignment.CenterVertically
    ) {
        val context = LocalContext.current
        val decryptedSecret = remember { passwordVersion.value }

        Column(Modifier.weight(1f)) {
            Row(modifier = Modifier.clickable {
                tracker.trackEvent(Tracker.EVENT_PASSWORD_COPY)
                if (decryptedSecret.copyToClipboard(context)
                    && !VersionUtil.showsSystemMessageOnCopyToClipboard()
                ) {
                    Toast.makeText(context, R.string.notice_copied_to_clipboard, Toast.LENGTH_SHORT)
                        .show()
                }
            }) {
                Text(
                    text = decryptedSecret,
                    style = StyleStolzRegularDarkBlue14,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
                Spacer(modifier = Modifier.width(8.dp))
                Image(
                    painter = painterResource(id = R.drawable.ic_copy),
                    contentDescription = stringResource(
                        id = R.string.bt_copy
                    )
                )
            }
            Spacer(modifier = Modifier.width(4.dp))
            Text(
                text = passwordVersion.formatDate(context = context),
                style = StyleStolzBookDarkBlue12.copy(color = DimmedBlue)
            )
        }
        Button(modifier = Modifier
            .height(38.dp)
            .dropShadow(),
            border = BorderStroke(0.5.dp, DefaultBorderColor),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = White,
                contentColor = White,
                disabledBackgroundColor = White,
                disabledContentColor = White
            ),
            elevation = ButtonDefaults.elevation(
                defaultElevation = 0.dp,
                pressedElevation = 0.dp,
            ),
            shape = RoundedCornerShape(8.dp),
            onClick = {
                restorePassword(passwordVersion.value)
                closeSheet()
            }) {
            Text(
                text = stringResource(id = R.string.restore),
                style = TextStyle(
                    color = LightBlue,
                    fontSize = 14.sp,
                    fontFamily = Stolzl
                )
            )
        }
    }
}
