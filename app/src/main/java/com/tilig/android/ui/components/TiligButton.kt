package com.tilig.android.ui.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.TiligApp
import com.tilig.android.ui.modifiers.dropShadow
import com.tilig.android.ui.theme.*

val TiligButtonHeight = 64.dp

@Composable
fun TiligButton(
    modifier: Modifier,
    iconRes: Int?,
    text: String,
    enabled: Boolean,
    isSizeConstrained: Boolean, // true for constraint layouts
    onClick: () -> Unit,
    isDark: Boolean = false,
    textColor: Color? = null,
    // Only relevant if isSizeConstrained. If true, adds bottom padding and horizontalPadding
    extraPadding: Boolean = true,
    // Only relevant if sizeIsConstrained and extraPadding is true
    horizontalPadding: Dp = 32.dp,
    iconSize: Dp = 24.dp,
    withShadow: Boolean = true
) {
    val _textColor = textColor ?: when {
        isDark && enabled -> InverseTextColor
        isDark && !enabled -> InverseDisabledTextColor
        !isDark && enabled -> DefaultTextColor
        else -> DisabledTextColor
    }
    val bgColor = if (isDark) LightBlue else DefaultBackground
    val borderColor = if (isDark) Color.Transparent else DefaultBorderColor

    // if the size is constraint set height & width manually
    val buttonModifier = if (isSizeConstrained) {
        if (extraPadding) {
            modifier
                .height(TiligButtonHeight)
                .fillMaxWidth()
                .padding(horizontal = horizontalPadding)
                .padding(bottom = 16.dp)
        } else {
            modifier
                .height(TiligButtonHeight)
                .fillMaxWidth()
        }
    } else modifier

    Button(
        modifier = buttonModifier
            .dropShadow(enabled = enabled && withShadow),
        border = BorderStroke(0.5.dp, borderColor),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = bgColor,
            disabledBackgroundColor = bgColor.copy(alpha = 0.5f)
        ),
        shape = RoundedCornerShape(DefaultCornerRadius),
        enabled = enabled,
        // Disable elevation, because we'll make our own dropshadow
        elevation = null,
        onClick = {
            TiligApp.debounceClicks {
                onClick.invoke()
            }
        }
    )
    {
        iconRes?.let {
            Image(
                painter = painterResource(id = iconRes),
                contentDescription = "",
                Modifier
                    .size(iconSize)
                    .align(Alignment.CenterVertically)
            )
            Spacer(modifier = Modifier.width(8.dp))
        }
        TiligText(
            modifier = Modifier.align(Alignment.CenterVertically),
            text = text,
            textColor = _textColor,
            overflow = TextOverflow.Ellipsis,
        )
    }
}

@Preview
@Composable
fun TiligButtonPreview() {
    Column {
        TiligButton(
            modifier = Modifier,
            iconRes = null,
            text = stringResource(R.string.first_account_bt_done),
            enabled = true,
            isDark = true,
            isSizeConstrained = false,
            onClick = { })
        TiligButton(
            modifier = Modifier,
            iconRes = null,
            text = stringResource(R.string.first_account_bt_done),
            enabled = false,
            isDark = true,
            isSizeConstrained = false,
            onClick = { })
        TiligButton(
            modifier = Modifier,
            iconRes = null,
            text = stringResource(R.string.first_account_bt_done),
            enabled = true,
            isDark = false,
            isSizeConstrained = false,
            onClick = { })
        TiligButton(
            modifier = Modifier,
            iconRes = null,
            text = stringResource(R.string.first_account_bt_done),
            enabled = false,
            isDark = false,
            isSizeConstrained = false,
            onClick = { })
    }
}
