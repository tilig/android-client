package com.tilig.android.ui.secrets

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tilig.android.migration.MigrationLegacyAccounts
import com.tilig.android.migration.MigrationLegacyTestDriveItems
import com.tilig.android.ui.CryptoModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

open class BaseSecretsListViewModel : ViewModel(), KoinComponent {

    private val migrationLegacyAccounts by inject<MigrationLegacyAccounts>()
    private val migrationLegacyTestDriveItems by inject<MigrationLegacyTestDriveItems>()
    private val cryptoModel: CryptoModel by inject()

    fun runMigrations(onMigrationFinished: () -> Unit) {
        viewModelScope.launch(Dispatchers.IO) {
            migrationLegacyAccounts.runMigrationLegacyAccounts(
                cryptoModel.getInitializedCrypto(),
                onActionAfterAccountsMigrated = {
                    onMigrationFinished.invoke()
                }
            )
            migrationLegacyTestDriveItems.runMigrationLegacyTestDriveItems(cryptoModel.getInitializedCrypto()) {
                onMigrationFinished.invoke()
            }
        }
    }
}