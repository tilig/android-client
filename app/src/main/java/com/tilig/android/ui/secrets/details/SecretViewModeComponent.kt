package com.tilig.android.ui.secrets.details

import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.SwipeableState
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import com.tilig.android.R
import com.tilig.android.data.models.tilig.SecretItem
import com.tilig.android.ui.secrets.components.SecretItemCollapsableContainer
import com.tilig.android.ui.secrets.components.SecretItemCollapsableContainerCallBack
import com.tilig.android.ui.secrets.components.SecretItemLoading
import com.tilig.android.ui.secrets.details.events.ItemDetailsActionEvent
import com.tilig.android.ui.theme.appBarDetailsExpandedViewHeightDp
import com.tilig.android.utils.SecretItemExt.getBackgroundColor
import com.tilig.android.utils.SecretItemExt.getSecretIcon

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SecretViewModeComponent(
    swipingState: SwipeableState<SwipingStates>,
    secretItem: SecretItem,
    isInInitialState: Boolean,
    isLoading: Boolean,
    onActionEvent: (event: ItemDetailsActionEvent) -> Unit,
    content: @Composable () -> Unit
) {
    val toolbarCallback = object : SecretItemCollapsableContainerCallBack {
        override fun onBackClicked() {
            onActionEvent(ItemDetailsActionEvent.OnGoBack)
        }

        override fun onActionBtnClicked() {
            onActionEvent(ItemDetailsActionEvent.EnterEditMode)
        }

        override fun onDebugBtnClicked() {
            onActionEvent(ItemDetailsActionEvent.DebugItem)
        }
    }

    SecretItemCollapsableContainer(
        swipingState = swipingState,
        accountIcon = secretItem.getSecretIcon(context = LocalContext.current),
        backgroundColor = secretItem.getBackgroundColor(),
        itemName = secretItem.name,
        actionBtnTitleRes = R.string.edit,
        isLoading = isLoading,
        isActionBtnAvailable = true,
        callBack = toolbarCallback,
        expandedHeight = appBarDetailsExpandedViewHeightDp(),
        addExtraHeightShift = true,
        content = {
            if (isInInitialState) {
                SecretItemLoading(secretItem = secretItem)
            } else {
                content()
            }
        }
    )
}