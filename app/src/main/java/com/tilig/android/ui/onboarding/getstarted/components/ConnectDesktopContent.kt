package com.tilig.android.ui.onboarding.getstarted.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.components.AppendableText
import com.tilig.android.ui.components.InstructionStep
import com.tilig.android.ui.theme.*

@Composable
fun ConnectDesktopContent(
    email: String,
    onSkip: () -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = White, shape = Shapes.medium)
            .padding(DefaultPadding)
    ) {
        Text(
            modifier = Modifier.padding(bottom = MediumPadding),
            text = stringResource(id = R.string.get_started_step_connect_desktop_desc),
            style = StyleStolzRegularNeutral14
        )
        Icon(
            modifier = Modifier
                .height(43.dp)
                .width(74.dp)
                .align(Alignment.CenterHorizontally),
            painter = painterResource(id = R.drawable.ic_desktop),
            tint = DarkBlue,
            contentDescription = null
        )
        TextButton(
            modifier = Modifier
                //.fillMaxWidth()
                .padding(top = DefaultPadding, bottom = DoublePadding)
                .background(color = DarkBlue, shape = RoundedCornerShape(32.dp))
                .padding(horizontal = 40.dp)
                .height(36.dp)
                .align(Alignment.CenterHorizontally),
            elevation = ButtonDefaults.elevation(
                defaultElevation = 0.dp,
                focusedElevation = 0.dp,
                hoveredElevation = 0.dp
            ),
            shape = RoundedCornerShape(32.dp),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = DarkBlue,
            ),
            onClick = { }) {
            Text(
                text = stringResource(id = R.string.connect_desktop_extension),
                style = StyleStolzBookDarkBlue14.copy(fontWeight = FontWeight.Bold, color = White)
            )
        }
        InstructionStep(
            index = 1, listOf(
                AppendableText(
                    stringResource(
                        id = R.string.connect_desktop_step_1,
                    ),
                    false
                )
            )
        )
        InstructionStep(
            index = 2, listOf(
                AppendableText(
                    stringResource(
                        id = R.string.connect_desktop_step_2,
                    ),
                    false
                ),
                AppendableText(
                    email,
                    true,
                    addSpace = true,
                )
            )
        )
        InstructionStep(
            index = 3, listOf(
                AppendableText(
                    stringResource(
                        id = R.string.connect_desktop_step_3,
                    ),
                    false
                )
            )
        )
        DoItLater(onSkip)
    }
}
