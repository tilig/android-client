package com.tilig.android.ui.autofillsheets

import android.os.Build
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.dp
import com.tilig.android.analytics.ConversionAnalytics
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.ui.autofill.AutofillEnableContract
import com.tilig.android.ui.components.StatusBarAndNavigationBarContrast
import com.tilig.android.utils.SharedPrefs
import org.koin.androidx.compose.get


enum class HOME_AUTOFILL_STATE {
    INSTRUCTIONS,
    DONE
}

@RequiresApi(Build.VERSION_CODES.O)
@ExperimentalMaterialApi
@Composable
fun AutofillSheet(
    onCloseSheet: () -> Unit,
    shouldShowAutofillSnackbar: MutableState<Boolean>,
    modifier: Modifier,
    prefs: SharedPrefs = get(),
    tracker: Mixpanel = get(),
    conversions: ConversionAnalytics = get()
) {
    StatusBarAndNavigationBarContrast(
        statusBarBright = false,
        navigationBarBright = false,
        navigationBarScrollBehind = true
    )
    val progress = remember { mutableStateOf(HOME_AUTOFILL_STATE.INSTRUCTIONS) }
    Box(
        modifier
            .fillMaxWidth()
            .height(LocalConfiguration.current.screenHeightDp.dp)
    ) {
        val systemSettingsLauncher =
            rememberLauncherForActivityResult(contract = AutofillEnableContract()) {
                if (it) {
                    shouldShowAutofillSnackbar.value = false
                    tracker.trackEvent(Tracker.EVENT_AUTOFILL_ENABLED)
                    conversions.trackEvent(ConversionAnalytics.EVENT_AUTOFILL_ENABLED)
                    prefs.mayAskForAutofill = false
                    prefs.isAutofillEnabled = true
                    progress.value = HOME_AUTOFILL_STATE.DONE
                }
            }

        when (progress.value) {
            HOME_AUTOFILL_STATE.INSTRUCTIONS -> AutofillSheetInstructionsView(
                onClose = onCloseSheet,
                systemSettingsLauncher = systemSettingsLauncher,
                tracker = tracker
            )
            HOME_AUTOFILL_STATE.DONE -> AutofillSheetDoneView(
                onClose = onCloseSheet
            )
        }
    }
}


