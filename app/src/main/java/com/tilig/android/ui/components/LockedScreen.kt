package com.tilig.android.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Fingerprint
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tilig.android.LockActivity
import com.tilig.android.R
import com.tilig.android.ui.theme.DarkBlue
import com.tilig.android.ui.theme.StyleStolzBookDarkBlue14
import com.tilig.android.ui.theme.StyleStolzRegularDarkBlue16
import com.tilig.android.ui.theme.White
import com.tilig.android.utils.UITestTags

@Composable
fun LockedScreen(
    tooManyAttempts: Boolean,
    modifier: Modifier = Modifier
) {

    val context = LocalContext.current

    Box(
        modifier = modifier
            .fillMaxSize()
            .background(color = White)
            .testTag(UITestTags.lockedStateTestTag)
    ) {
        Image(
            painter = painterResource(id = R.drawable.logo),
            contentDescription = "",
            modifier = Modifier
                .padding(top = 48.dp)
                .align(Alignment.TopCenter)
        )

        Column(
            modifier = Modifier
                .fillMaxSize()
                .align(Alignment.Center),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = stringResource(
                    id = R.string.lock_title,
                    stringResource(id = R.string.app_name)
                ),
                style = StyleStolzRegularDarkBlue16
            )
            Icon(
                modifier = Modifier
                    .padding(top = 32.dp)
                    .size(64.dp)
                    .clickable {
                        (context as? LockActivity)?.tryToUnlock()
                    },
                imageVector = Icons.Filled.Fingerprint,
                contentDescription = null,
                tint = DarkBlue
            )
            if (tooManyAttempts) {
                Text(
                    text = stringResource(
                        id = R.string.lock_description_too_many_attempts
                    ),
                    style = StyleStolzBookDarkBlue14
                )
            }
        }
    }
}

@Preview
@Composable
fun LockedScreenPreview() {
    LockedScreen(tooManyAttempts = true)
}
