package com.tilig.android.ui.twofa.bottomsheets.tutorial

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.PagerState
import com.tilig.android.R
import com.tilig.android.ui.theme.DarkBlue
import com.tilig.android.ui.theme.DefaultPadding
import com.tilig.android.ui.theme.StyleStolzBookDarkBlue16
import com.tilig.android.ui.theme.Typography

@ExperimentalAnimationApi
@ExperimentalPagerApi
@Composable
internal fun TwoFATutorialPager(
    pagerState: PagerState,
    modifier: Modifier
) {
    HorizontalPager(
        modifier = modifier
            .fillMaxWidth()
            .wrapContentHeight(),
        verticalAlignment = Alignment.Top,
        count = PAGES_COUNT, state = pagerState
    ) { position ->
        when (position) {
            0 -> TwoFATutorialPage(
                imageRes = R.drawable.two_fa_tutorial_0,
                title = stringResource(id = R.string.two_fa_tutorial_title_1),
                descriptionRes = R.string.two_fa_tutorial_desc_1
            )
            1 -> TwoFATutorialPage(
                imageRes = R.drawable.two_fa_tutorial_1,
                title = stringResource(id = R.string.two_fa_tutorial_title_2, "hh"),
                descriptionRes = R.string.two_fa_tutorial_desc_2
            )
            2 -> TwoFATutorialPage(
                imageRes = R.drawable.two_fa_tutorial_2,
                title = stringResource(id = R.string.two_fa_tutorial_title_3),
                descriptionRes = R.string.two_fa_tutorial_desc_3
            )
            3 -> TwoFATutorialPage(
                imageRes = R.drawable.two_fa_tutorial_3,
                title = stringResource(id = R.string.two_fa_tutorial_title_4),
                descriptionRes = R.string.two_fa_tutorial_desc_4
            )
        }
    }
}

@ExperimentalPagerApi
@Composable
fun TwoFATutorialPage(
    imageRes: Int,
    title: String,
    descriptionRes: Int,
) {
    val scroll = rememberScrollState(0)

    Column(
        modifier = Modifier
            .padding(horizontal = 32.dp)
            .fillMaxHeight(),
    ) {
        Image(
            painter = painterResource(id = imageRes),
            contentDescription = "",
            modifier = Modifier
                .padding(vertical = DefaultPadding)
                .width(175.dp)
                .align(Alignment.CenterHorizontally)
            ,
            contentScale = ContentScale.FillWidth
        )
        Text(
            modifier = Modifier
                .padding(bottom = DefaultPadding),
            text = title,
            style = Typography.h2,
            fontSize = 18.sp,
            color = DarkBlue,
        )
        Text(
            stringResource(id = descriptionRes),
            modifier = Modifier.verticalScroll(scroll),
            style = StyleStolzBookDarkBlue16.copy(lineHeight = 24.sp)
        )
    }
}

@ExperimentalAnimationApi
@ExperimentalPagerApi
@Composable
@Preview
fun TwoFATutorialPagerPreview() {
    TwoFATutorialPager(
        PagerState(0),
        Modifier
    )
}

@ExperimentalPagerApi
@Composable
@Preview
fun OnboardingPagePreview() {
    TwoFATutorialPage(
        imageRes = R.drawable.two_fa_tutorial_1,
        title = stringResource(id = R.string.two_fa_tutorial_title_1, "Google.com"),
        descriptionRes = R.string.two_fa_tutorial_desc_1
    )
}
