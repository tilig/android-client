package com.tilig.android.ui.accounts.addaccount

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.*
import androidx.compose.material.Snackbar
import androidx.compose.material.SnackbarHost
import androidx.compose.material.SnackbarHostState
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.tilig.android.R
import com.tilig.android.analytics.Breadcrumb
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.ui.accounts.addaccount.components.CreateAccount
import com.tilig.android.ui.accounts.addaccount.components.SelectBrand
import com.tilig.android.ui.components.LoadingScreen
import com.tilig.android.ui.components.ScreenHeader
import com.tilig.android.ui.secrets.SecretItemsViewModel
import com.tilig.android.ui.secrets.add.AddItemSnackbarErrorMessage
import com.tilig.android.ui.theme.Black
import com.tilig.android.ui.theme.DefaultPadding
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.compose.get

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun AddAccountBottomSheet(
    tracker: Mixpanel,
    viewModel: AddAccountViewModel,
    secretItemsViewModel: SecretItemsViewModel,
    suggestedAccount: Account?,
    onCloseSheet: () -> Unit,
    onAccountCreated: (account: Account) -> Unit,
) {
    suggestedAccount?.let {
        viewModel.initWithSuggestedAccount(it)
        Breadcrumb.drop(Breadcrumb.AddSuggestedAccount)
    }
    val coroutineScope = rememberCoroutineScope()
    val screenState by viewModel.uiState.collectAsState()
    val shouldFetchAccounts by viewModel.accountCreatedEvent.collectAsState()
    val snackbarHostState = remember { SnackbarHostState() }
    val keyboardManager = LocalSoftwareKeyboardController.current

    val updateScreenSheet = {
        if (screenState is AddAccountUiState.CreateAccount) {
            viewModel.onGoBack()
        } else {
            viewModel.clearUIState()
        }
        keyboardManager?.hide()
    }
    LaunchedEffect(shouldFetchAccounts) {
        coroutineScope.launch {
            if (shouldFetchAccounts) {
                secretItemsViewModel.secretItems()
                secretItemsViewModel.uiState.collectLatest {
                    if ((screenState is AddAccountUiState.CreateAccount)
                        && !it.isLoading
                        && secretItemsViewModel.lookupItem(
                            (screenState as AddAccountUiState.CreateAccount).account.id
                        ) != null
                    ) {
                        onAccountCreated((screenState as AddAccountUiState.CreateAccount).account)
                    }
                }
                viewModel.clearShouldFetchAccounts()
            }
        }
    }

    Box(
        modifier = Modifier
            .navigationBarsPadding()
            .height(LocalConfiguration.current.screenHeightDp.dp)
            .imePadding()
    ) {
        Column(
            modifier = Modifier.fillMaxSize()
        ) {
            ScreenHeader(
                headerTextRes = R.string.bt_add_login,
                closeSheet = {
                    updateScreenSheet.invoke()
                    if (screenState is AddAccountUiState.SelectBrand) {
                        onCloseSheet()
                    }
                },
                leftActionIconRes = if (screenState is AddAccountUiState.SelectBrand) R.drawable.ic_close else R.drawable.ic_back,
                headerBg = R.drawable.ic_coloured_header,
                tintColor = Color.White
            )
            when (screenState) {
                is AddAccountUiState.SelectBrand -> {
                    SelectBrand(
                        tracker = tracker,
                        uiState = screenState as AddAccountUiState.SelectBrand,
                        onAccountPrefill = { account, suggested ->
                            keyboardManager?.hide()
                            viewModel.accountPrefill(account = account, isSuggested = suggested)
                        },
                        onSearch = {
                            viewModel.search(it)
                        })
                }
                is AddAccountUiState.CreateAccount -> {
                    if (!screenState.isLoading) {
                        val account = (screenState as AddAccountUiState.CreateAccount).account
                        val folders = (screenState as AddAccountUiState.CreateAccount).folders

                        Box(modifier = Modifier.fillMaxSize()) {
                            CreateAccount(
                                tracker = tracker,
                                account = account,
                                folders = folders,
                                onSaveAccount = {
                                    keyboardManager?.hide()
                                    viewModel.createAccount(it)
                                }
                            ) {
                                viewModel.updateWebsiteBrandData(it)
                            }
                        }
                    } else {
                        LoadingScreen()
                    }
                }
            }
            screenState.errorMessages.let {
                it?.forEach {
                    AddItemSnackbarErrorMessage(
                        scope = coroutineScope,
                        uiError = it,
                        snackbarHostState = snackbarHostState
                    )
                }
            }
        }
        SnackbarHost(
            hostState = snackbarHostState,
            Modifier
                .fillMaxWidth()
                .align(Alignment.BottomCenter)
                .padding(DefaultPadding)
        ) { data ->
            Snackbar(
                backgroundColor = Black,
                snackbarData = data,
            )
        }
    }
    BackHandler {
        updateScreenSheet.invoke()
        if (screenState is AddAccountUiState.SelectBrand) {
            onCloseSheet()
        }
    }
}

@Preview
@Composable
fun AddAccountBottomSheetPreview() {
    AddAccountBottomSheet(
        tracker = get(),
        viewModel = viewModel(),
        secretItemsViewModel = viewModel(),
        suggestedAccount = null,
        onCloseSheet = {},
    ) {}
}
