package com.tilig.android.ui.onboarding.survey

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.fragment.app.Fragment
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPagerIndicator
import com.google.accompanist.pager.rememberPagerState
import com.tilig.android.R
import com.tilig.android.ui.components.AppendableText
import com.tilig.android.ui.components.AppendableTextView
import com.tilig.android.ui.components.TiligButton
import com.tilig.android.ui.modifiers.dropShadow
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.catchNavigate


private val IndicatorHorizontalPadding = 32.dp
private val IndicatorSpacing = 8.dp
private val IndicatorHeight = 3.dp
private val BottomBoxHeight = 80.dp

class SurveyFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        return ComposeView(requireContext()).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            setContent {
                SurveyScreen(findNavController())
            }
        }
    }
}

@OptIn(
    ExperimentalPagerApi::class, androidx.compose.animation.ExperimentalAnimationApi::class,
    kotlinx.coroutines.InternalCoroutinesApi::class
)
@Composable
private fun SurveyScreen(navController: NavController, viewModel: SurveyViewModel = viewModel()) {

    val goNextScreen = viewModel.finishSurvey.collectAsState()
    if (goNextScreen.value) {
        navController.catchNavigate(SurveyFragmentDirections.actionSurveyFragmentToHomeFragment())
    }
    val pagerState = rememberPagerState(initialPage = 0)

    val currentPage by viewModel.currentPage.collectAsState()

    val actionButtonEnabledState by viewModel.actionButtonEnabled.collectAsState()

    LaunchedEffect(currentPage) {
        pagerState.animateScrollToPage(page = currentPage)
    }

    BackHandler(pagerState.currentPage != 0) {
        viewModel.goToPreviousScreen()
    }

    val state = rememberScrollState()

    val scrollableContentPxHeight = remember { mutableStateOf(0f) }
    val scrollableContentDpHeight = with(LocalDensity.current) {
        scrollableContentPxHeight.value.toDp()
    }
    val applyBoxDropShadow = scrollableContentDpHeight < BottomBoxHeight

    ConstraintLayout(
        modifier = Modifier
            .fillMaxSize()
            .background(color = White)
            .navigationBarsPadding()
            .statusBarsPadding()
    ) {
        val (content, bottomView) = createRefs()
        Column(
            modifier = Modifier
                .constrainAs(content) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    bottom.linkTo(bottomView.top)
                    height = Dimension.fillToConstraints
                }
                .fillMaxSize()
                .verticalScroll(state)
                .onGloballyPositioned { coordinates ->
                    coordinates.parentLayoutCoordinates?.let {
                        scrollableContentPxHeight.value =
                            (it.size.height - coordinates.size.height).toFloat()
                    }
                }
        ) {
            val indicatorWidth =
                (LocalConfiguration.current.screenWidthDp.dp - IndicatorHorizontalPadding * 2 - IndicatorSpacing * (pagerState.pageCount - 1)) / SurveyPages.SURVEY_PAGES

            HorizontalPagerIndicator(
                pagerState = pagerState,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 32.dp, end = 32.dp, top = 0.dp)
                    .statusBarsPadding(),
                indicatorWidth = indicatorWidth,
                spacing = IndicatorSpacing,
                indicatorHeight = IndicatorHeight,
                activeColor = LightBlue,
                inactiveColor = TwoFATutorialIndicatorInactive,
                indicatorShape = RoundedCornerShape(2.dp)
            )

            Spacer(
                modifier = Modifier
                    .height(40.dp)
            )

            QuestionLabel(
                modifier = Modifier
                    .padding(start = 32.dp, end = 32.dp, bottom = 32.dp),
                currentPage = pagerState.currentPage
            )

            SurveyPager(
                viewModel = viewModel,
                pagerState = pagerState,
                modifier = if (pagerState.currentPage == SurveyPages.SURVEY_FINISHED) {
                    Modifier.weight(1f)
                } else {
                    Modifier.wrapContentHeight(unbounded = true)
                }
            )

        }

        val styleModifier = if (applyBoxDropShadow) {
            Modifier
                .background(color = White)
                .dropShadow()
        } else {
            Modifier
        }

        Card(modifier = Modifier
            .constrainAs(bottomView) {
                start.linkTo(parent.start)
                end.linkTo(parent.end)
                bottom.linkTo(parent.bottom)
            }
            .background(color = White)
            .then(styleModifier), shape = RectangleShape
        ) {


            TiligButton(
                modifier = Modifier
                    .padding(horizontal = 32.dp, vertical = 16.dp)
                    .fillMaxWidth()
                    .height(48.dp),
                iconRes = null,
                text = stringResource(if (pagerState.currentPage != SurveyPages.SURVEY_FINISHED) R.string.next else R.string.finish),
                enabled = actionButtonEnabledState,
                isSizeConstrained = false,
                isDark = true,
                onClick = {
                    viewModel.goToNextScreen()
                })
        }
    }
}

@Composable
private fun QuestionLabel(modifier: Modifier, currentPage: Int) {
    if (currentPage == SurveyPages.SURVEY_FINISHED) {
        return
    }
    val appendableTexts = mutableListOf<AppendableText>()
    appendableTexts.add(
        AppendableText(
            stringResource(id = R.string.question_1_spannable, currentPage + 1),
            false,
            addSpace = true
        )
    )
    appendableTexts.add(AppendableText(stringResource(id = R.string.question_3), true))

    AppendableTextView(
        modifier = modifier,
        appendableTexts = appendableTexts,
        spanStyle = SpanStyle(DimmedLightBlue, fontSize = 14.sp),
        textStyle = Typography.h3.copy(
            color = LightBlue,
            fontSize = 14.sp,
            lineHeight = 24.sp
        )
    )
}
