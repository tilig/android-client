package com.tilig.android.ui.onboarding.getstarted.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.components.SmallWarningBar
import com.tilig.android.ui.secrets.components.SaveSecretItemButton
import com.tilig.android.ui.theme.*

@Composable
fun TryAutofillContent(
    enabled: Boolean,
    onSkip: () -> Unit,
    onTryAutofill: () -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = White, shape = Shapes.medium)
            .padding(DefaultPadding)
    ) {
        Text(
            text = stringResource(id = R.string.get_started_step_try_autofill_desc),
            style = StyleStolzRegularNeutral14
        )
        Image(
            modifier = Modifier
                .padding(vertical = MediumPadding)
                .align(alignment = Alignment.CenterHorizontally),
            painter = painterResource(id = R.drawable.img_try_autofill),
            contentDescription = null
        )
        if (!enabled) {
            Spacer(modifier = Modifier.height(12.dp))
            SmallWarningBar(text = R.string.warning_add_a_login)
            Spacer(modifier = Modifier.height(12.dp))
        }
        SaveSecretItemButton(
            modifier = Modifier
                .align(alignment = Alignment.CenterHorizontally),
            text = stringResource(id = R.string.get_started_step_try_autofill),
            enabled = enabled,
            onSave = onTryAutofill
        )
        DoItLater(onSkip)
    }
}
