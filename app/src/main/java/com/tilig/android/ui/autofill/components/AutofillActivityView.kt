package com.tilig.android.ui.autofill.components

import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.SpringSpec
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ModalBottomSheetValue
import androidx.compose.material.rememberModalBottomSheetState
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.input.pointer.util.VelocityTracker
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.tilig.android.R
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.data.api.ConnectivityState
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.ui.NoNetworkConnectionScreen
import com.tilig.android.ui.SomethingWentWrongScreen
import com.tilig.android.ui.autofill.AutofillUiState
import com.tilig.android.ui.autofill.AutofillViewModel
import com.tilig.android.ui.autofill.components.AutofillBottomSheetScreen.Companion.header
import com.tilig.android.ui.autofill.components.AutofillBottomSheetScreen.Companion.isSupportBackToAccountList
import com.tilig.android.ui.autofill.components.modalsheet.AutofillModalBottomSheetLayout
import com.tilig.android.ui.components.LoadingScreen
import com.tilig.android.ui.components.LockedScreen
import com.tilig.android.ui.components.ScreenHeader
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.lock.LockViewModel
import kotlinx.coroutines.launch
import org.koin.androidx.compose.inject

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun AutofillActivityView(
    viewModel: AutofillViewModel,
    lockViewModel: LockViewModel = viewModel(),
    account: Account,
    currentlyChosenAccount: Account?,
    startScreen: AutofillBottomSheetScreen?,
    action: (AutofillPopupActions) -> Unit
) {

    val modalBottomSheetState =
        rememberModalBottomSheetState(
            initialValue = ModalBottomSheetValue.Hidden,
            skipHalfExpanded = true
        )

    LaunchedEffect(Unit) {
        modalBottomSheetState.animateTo(
            ModalBottomSheetValue.Expanded, SpringSpec(
                dampingRatio = 1f,
                stiffness = Spring.StiffnessLow,
                visibilityThreshold = null
            )
        )
    }

    if (modalBottomSheetState.currentValue != ModalBottomSheetValue.Hidden) {
        DisposableEffect(Unit) {
            onDispose {
                action.invoke(AutofillPopupActions.ClosePopup)
            }
        }
    }

    val uiState by viewModel.uiState.collectAsState()
    val uiLockState by lockViewModel.uiState.collectAsState()
    val tracker: Mixpanel by inject()

    val connectivity by viewModel.connectivity.observeAsState()

    val searchState = remember { mutableStateOf("") }

    val screenType = remember {
        mutableStateOf(startScreen ?: AutofillBottomSheetScreen.AccountListScreen)
    }
    val coroutineScope = rememberCoroutineScope()

    val navigateBack: () -> Unit = {
        if (screenType.value.isSupportBackToAccountList()) {
            action.invoke(AutofillPopupActions.ReturnToAccountsList)
        } else {
            action.invoke(AutofillPopupActions.ClosePopup)
        }
    }

    // search for items by search query
    viewModel.searchForSecretItems(searchState.value)

    screenType.value =
        when {
            startScreen != null -> startScreen
            connectivity == ConnectivityState.Disconnected -> AutofillBottomSheetScreen.NoConnectionScreen
            currentlyChosenAccount != null -> AutofillBottomSheetScreen.PhishingScreen
            uiLockState.isLocked -> AutofillBottomSheetScreen.LockScreen
            uiState.isLoading -> AutofillBottomSheetScreen.LoadingScreen
            uiState is AutofillUiState.AddNewAccount -> AutofillBottomSheetScreen.AddAccountScreen
            uiState is AutofillUiState.AutofillAccountsError -> AutofillBottomSheetScreen.ErrorScreen
            else -> AutofillBottomSheetScreen.AccountListScreen
        }

    AutofillModalBottomSheetLayout(
        modifier = Modifier.navigationBarsPadding(),
        sheetState = modalBottomSheetState,
        scrimColor = Black.copy(alpha = 0.75f),
        sheetContent = {
            Column(
                modifier = Modifier
                    .fillMaxHeight(0.9f)
                    .imePadding()
            ) {
                if (screenType.value == AutofillBottomSheetScreen.NoConnectionScreen) {
                    NoNetworkConnectionScreen(
                        modifier = Modifier.background(color = White)
                    )
                } else {
                    Box {
                        ScreenHeader(
                            modifier = Modifier
                                .background(
                                    DefaultBackground,
                                    shape = RoundedCornerShape(
                                        topStart = DefaultCornerRadius,
                                        topEnd = DefaultCornerRadius
                                    )
                                )
                                .clip(
                                    RoundedCornerShape(
                                        topStart = DefaultCornerRadius,
                                        topEnd = DefaultCornerRadius
                                    )
                                ),
                            closeSheet = {
                                navigateBack()
                            },
                            headerTextRes = screenType.value.header(),
                            headerBg = R.drawable.ic_coloured_header,
                            leftActionIconRes = if (screenType.value.isSupportBackToAccountList()) R.drawable.ic_back else R.drawable.ic_close,
                            textColor = White
                        )
                        // detect gesture events
                        Box(modifier = Modifier
                            // prevent click overlay of the left action button
                            .padding(start = 42.dp)
                            .fillMaxWidth()
                            .height(56.dp)
                            .pointerInput(Unit) {
                                val velocityTracker = VelocityTracker()
                                detectDragGestures(
                                    onDragEnd = {
                                        coroutineScope.launch {
                                            val velocity = velocityTracker.calculateVelocity().y
                                            // this is needed to update anchors and hide or left expanded modal sheet
                                            modalBottomSheetState.performFling(velocity)
                                        }
                                    }
                                ) { change, amount ->
                                    // only when header is touched, a user can swipe modal sheet
                                    change.consume()
                                    velocityTracker.addPosition(
                                        change.uptimeMillis,
                                        change.position
                                    )
                                    modalBottomSheetState.performDrag(amount.y)
                                }
                            })
                    }

                    when (screenType.value) {
                        AutofillBottomSheetScreen.AccountListScreen -> {
                            // We show the Autofill Popup when there are no matching accounts,
                            // but NOT if that happens due to a search query that didn't yield results.
                            LaunchedEffect(uiState) {
                                if ((uiState as AutofillUiState.AutofillAccountsList).matchedAccounts.isNotEmpty()) {
                                    tracker.trackEvent(Tracker.EVENT_AUTOFILL_SUGGESTED)
                                } else {
                                    // Note: this will also fire when the user taps the (+) icon manually,
                                    // which it shouldn't, but that'll be fixed once that icon will lead into the new
                                    // add account flow instead of this one.
                                    tracker.trackEvent(Tracker.EVENT_AUTOFILL_NO_SUGGESTIONS)
                                }
                            }

                            AutofillMainScreen(
                                account = account,
                                matchedAccounts = (uiState as AutofillUiState.AutofillAccountsList).matchedAccounts,
                                allAccounts = (uiState as AutofillUiState.AutofillAccountsList).allAccounts,
                                totalAccounts = (uiState as AutofillUiState.AutofillAccountsList).totalCount,
                                searchState = searchState,
                                isDemoMode = (uiState as AutofillUiState.AutofillAccountsList).isDemoMode,
                                action = action
                            )
                        }
                        AutofillBottomSheetScreen.AddAccountScreen -> {
                            AutofillCreateAccount(
                                account = (uiState as AutofillUiState.AddNewAccount).prefilledAccount,
                                action = action,
                                shouldAlsoAutofill = startScreen == null
                            )
                        }
                        AutofillBottomSheetScreen.ErrorScreen -> {
                            val context = LocalContext.current
                            LaunchedEffect(uiState) {
                                Toast.makeText(context, R.string.no_accounts, Toast.LENGTH_LONG)
                                    .show()
                            }
                            SomethingWentWrongScreen(whatWasWrongText = stringResource(id = R.string.error_load_secret_items)) {
                                action.invoke(AutofillPopupActions.RefreshAutofillAccountsList)
                            }
                        }
                        AutofillBottomSheetScreen.LoadingScreen -> {
                            LoadingScreen(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(64.dp)
                            )
                        }
                        AutofillBottomSheetScreen.LockScreen -> {
                            LockedScreen(
                                tooManyAttempts = uiLockState.tooManyAttempts,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(64.dp)
                            )
                        }
                        AutofillBottomSheetScreen.NoConnectionScreen -> {
                            NoNetworkConnectionScreen(
                                modifier = Modifier.background(color = White)
                            )
                        }
                        AutofillBottomSheetScreen.PhishingScreen -> {
                            if (currentlyChosenAccount != null) {
                                AutofillPhishingView(
                                    chosenAccount = currentlyChosenAccount,
                                    visitingAccount = account,
                                    onAction = action
                                )
                            }
                        }
                    }
                }
            }
        },
        sheetShape = RoundedCornerShape(
            topStart = DefaultCornerRadius,
            topEnd = DefaultCornerRadius
        )
    ) {
        // no content for activity
    }

    BackHandler {
        navigateBack()
    }
}


sealed class AutofillBottomSheetScreen {
    object AccountListScreen : AutofillBottomSheetScreen()
    object AddAccountScreen : AutofillBottomSheetScreen()
    object PhishingScreen : AutofillBottomSheetScreen()
    object NoConnectionScreen : AutofillBottomSheetScreen()
    object LockScreen : AutofillBottomSheetScreen()
    object LoadingScreen : AutofillBottomSheetScreen()
    object ErrorScreen : AutofillBottomSheetScreen()

    companion object {
        fun AutofillBottomSheetScreen.isSupportBackToAccountList() =
            this is AddAccountScreen || this is PhishingScreen

        fun AutofillBottomSheetScreen.header() = when (this) {
            AccountListScreen -> R.string.title_autofill_logins
            PhishingScreen -> R.string.label_autofill_header_choose_account
            AddAccountScreen -> R.string.add_a_new_login
            LoadingScreen,
            LockScreen,
            ErrorScreen,
            NoConnectionScreen -> R.string.label_autofill_toolbar_header_loading
        }
    }
}