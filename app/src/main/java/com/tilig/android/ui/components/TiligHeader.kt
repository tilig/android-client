package com.tilig.android.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.theme.DarkBlue

private val _headerHeight = 96.dp
private val _statusBarHeight = 24.dp
val HeaderHeight = _headerHeight + _statusBarHeight

@Composable
@Preview
fun TiligHeader(modifier: Modifier = Modifier) {
    Box(
        modifier
            .fillMaxWidth()
            .height(HeaderHeight)
            .background(DarkBlue)
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_coloured_header),
            contentDescription = "",
            contentScale = ContentScale.FillBounds,
        )
    }
}
