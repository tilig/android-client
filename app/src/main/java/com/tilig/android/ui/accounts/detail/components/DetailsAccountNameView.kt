package com.tilig.android.ui.accounts.detail.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.tilig.android.R
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.components.BorderedBasicTextField
import com.tilig.android.ui.components.ErrorHandler
import com.tilig.android.ui.theme.*

@Composable
fun DetailsAccountNameView(
    modifier: Modifier = Modifier,
    nameState: TextFieldState,
    onValueChanged: ((String) -> Unit)? = null,
    onNext: (() -> Unit)? = null,
) {
    Box(modifier = modifier) {
        BorderedBasicTextField(
            modifier = Modifier.padding(top = if (nameState.isValid) DefaultErrorMessageHeight else 0.dp),
            textStyle = StyleStolzRegularDarkBlue16,
            errorHandler = ErrorHandler(
                errorMessage = stringResource(id = R.string.error_name),
                errorColor = DefaultErrorColor
            ),
            singleLine = false,
            hint = stringResource(id = R.string.hint_login_name),
            state = nameState,
            textModifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 12.dp, vertical = 4.dp),
            keyboardType = KeyboardType.Text,
            onValueChanged = onValueChanged,
            onNext = onNext
        )
        Text(
            text = stringResource(id = R.string.label_edit_login_name),
            modifier = Modifier,
            style = StyleStolzRegularDarkBlue12.copy(color = DarkBlue.copy(alpha = 0.75f))
        )
    }
}