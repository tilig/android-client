package com.tilig.android.ui.components

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.ArrowForwardIos
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithCache
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.PagerState
import com.tilig.android.ui.onboarding.OnboardingActivity
import com.tilig.android.ui.theme.LightBlue
import com.tilig.android.ui.theme.ProgressColorInactive
import com.tilig.android.ui.theme.White
import com.tilig.android.utils.UITestTags
import kotlinx.coroutines.launch

@ExperimentalAnimationApi
@ExperimentalPagerApi
@Composable
fun ProgressButton(modifier: Modifier, pagerState: PagerState, onFinalPageNextClick: () -> Unit) {
    val initialPageProgress =
        (pagerState.currentPage + (if (pagerState.currentPage == 0) 0 else 1)) / OnboardingActivity.PAGES.toFloat()
    var progress by remember { mutableStateOf(initialPageProgress) }
    val animatedProgress = animateFloatAsState(
        targetValue = progress,
        animationSpec = ProgressIndicatorDefaults.ProgressAnimationSpec
    ).value

    Box(modifier = modifier) {
        ConstraintLayout {
            val (button, indicator) = createRefs()
            val coroutineScope = rememberCoroutineScope()

            Button(
                modifier = Modifier
                    .size(58.dp)
                    .constrainAs(button) {
                        start.linkTo(indicator.start)
                        end.linkTo(indicator.end)
                        top.linkTo(indicator.top)
                        bottom.linkTo(indicator.bottom)
                    },
                shape = CircleShape,
                colors = ButtonDefaults.buttonColors(backgroundColor = LightBlue),
                onClick = {
                    if (pagerState.currentPage == OnboardingActivity.PAGES - 1) {
                        onFinalPageNextClick.invoke()
                    }
                    coroutineScope.launch {
                        // We coerce this value, since people might be able to tap faster than the
                        // animation and get to a page number too high to exist
                        val page =
                            (pagerState.currentPage + 1).coerceIn(0 until OnboardingActivity.PAGES)
                        pagerState.animateScrollToPage(page)
                    }
                }) {
                Icon(Icons.Outlined.ArrowForwardIos, "", tint = White)
            }
            CircularProgressIndicator(
                progress = animatedProgress,
                color = LightBlue,
                modifier = Modifier
                    .testTag(UITestTags.circularProgressIndicatorTestTag)
                    .size(80.dp)
                    .padding(2.dp)
                    .constrainAs(indicator) {
                        bottom.linkTo(parent.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
                    .drawWithCache {
                        onDrawWithContent {
                            drawArc(
                                color = ProgressColorInactive,
                                startAngle = 0f,
                                sweepAngle = 360f,
                                useCenter = false,
                                style = Stroke(width = 3.dp.toPx(), cap = StrokeCap.Round)
                            )
                            drawArc(
                                color = LightBlue,
                                startAngle = -90f,
                                sweepAngle = animatedProgress * 360,
                                useCenter = false,
                                style = Stroke(width = 3.dp.toPx(), cap = StrokeCap.Round)
                            )
                        }
                    }
            )
        }
    }

    // Update progress value for the current displayed page
    progress = (pagerState.currentPage + 1f) / OnboardingActivity.PAGES.toFloat()
}

@ExperimentalAnimationApi
@ExperimentalPagerApi
@Composable
@Preview(showBackground = true)
fun ProgressButtonPreview() {
    Column {
        ProgressButton(modifier = Modifier, pagerState = PagerState(0), {})
        ProgressButton(modifier = Modifier, pagerState = PagerState(1), {})
        ProgressButton(
            modifier = Modifier,
            pagerState = PagerState(OnboardingActivity.PAGES - 1),
            {})
    }
}
