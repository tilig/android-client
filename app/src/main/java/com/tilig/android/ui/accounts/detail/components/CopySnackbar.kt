package com.tilig.android.ui.accounts.detail.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.tilig.android.ui.theme.StatusBarHeight
import com.tilig.android.ui.theme.StyleStolzRegularDarkBlue14
import com.tilig.android.ui.theme.White

@Composable
fun CopySnackbar(
    snackbarData: SnackbarData,
    modifier: Modifier = Modifier,
    shape: Shape = RoundedCornerShape(28.dp),
    backgroundColor: Color = White,
    contentColor: Color = White,
    actionColor: Color = SnackbarDefaults.primaryActionColor,
    elevation: Dp = 6.dp,
    textStyle: TextStyle = StyleStolzRegularDarkBlue14
) {
    Surface(
        modifier = modifier.wrapContentWidth().padding(top = StatusBarHeight),
        shape = shape,
        elevation = elevation,
        color = backgroundColor,
        contentColor = contentColor
    ) {
        CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.high) {
            Box(
                modifier = Modifier.padding(horizontal = 8.dp)
            ) {
                TextButton(
                    colors = ButtonDefaults.textButtonColors(contentColor = actionColor),
                    onClick = { snackbarData.performAction() },
                    content = { Text(snackbarData.message, style = textStyle) }
                )
            }
        }
    }
}