package com.tilig.android.ui.twofa.scanner

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.tilig.android.ui.theme.Black
import com.tilig.android.ui.theme.DefaultCornerRadius
import com.tilig.android.ui.theme.Stolzl
import com.tilig.android.ui.theme.White

@Composable
internal fun ScannerMessage(modifier: Modifier, message: String) {
    Box(
        modifier = modifier
            .background(
                color = Black.copy(alpha = 0.7f),
                shape = RoundedCornerShape(DefaultCornerRadius)
            )
            .padding(16.dp)
    ) {
        Text(
            modifier = Modifier.fillMaxWidth(),
            text = message,
            style = TextStyle(
                color = White,
                fontFamily = Stolzl,
                fontSize = 16.sp,
                textAlign = TextAlign.Center
            )
        )
    }
}

@Preview
@Composable
fun ScannerMessagePreview() {
    ScannerMessage(
        modifier = Modifier,
        message = "Lorem Ipsum"
    )
}
