package com.tilig.android.ui.secrets.components

import SingleLetterIcon
import android.icu.text.CaseMap.Fold
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.tilig.android.R
import com.tilig.android.data.models.tilig.Contact
import com.tilig.android.data.models.tilig.Folder
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.components.LoadingScreen
import com.tilig.android.ui.components.ScreenHeader
import com.tilig.android.ui.components.TiligSpacerVerticalMediumLarge
import com.tilig.android.ui.modifiers.dropShadow
import com.tilig.android.ui.secrets.ContactsViewModel
import com.tilig.android.ui.theme.*
import com.tilig.android.utils.isValidEmail

@Composable
fun ShareWithBottomSheet(
    viewModel: ContactsViewModel = viewModel(),
    folder: Folder?,
    closeSheet: () -> Unit,
    onShare: (String) -> Unit
) {
    LaunchedEffect(Unit) {
        viewModel.fetchContacts(folder)
    }
    val bottomSheetHeight = LocalConfiguration.current.screenHeightDp.dp
    val uiState by viewModel.uiState.collectAsState()

    val emailFieldState =
        remember { TextFieldState(uiState.searchFilter, validator = { it.isValidEmail() }) }

    Column(
        modifier = Modifier
            .height(bottomSheetHeight)
            .navigationBarsPadding()
    ) {
        ScreenHeader(
            headerTextRes = R.string.share_with,
            closeSheet = {
                viewModel.clearState()
                closeSheet.invoke()
            },
            textColor = DarkBlue
        )
        TiligSpacerVerticalMediumLarge()

        SearchContactInputField(emailFieldState, onShareClick = {
            val email = emailFieldState.text
            viewModel.clearState()
            onShare.invoke(email)
        }, onValueChange = {
            viewModel.search(it.trim())
        })

        TiligSpacerVerticalMediumLarge()

        if (uiState.isLoading) {
            LoadingScreen(modifier = Modifier.weight(1f))
        } else if (!uiState.filteredContacts.isNullOrEmpty()) {
            Text(
                modifier = Modifier.padding(horizontal = DefaultPadding),
                text = stringResource(id = R.string.suggested_contacts).uppercase(),
                style = StyleStolzRegularDarkBlue12.copy(
                    color = Neutral40,
                    fontSize = 10.sp,
                    lineHeight = 14.sp
                ),
            )
            LazyColumn(
                modifier = Modifier
                    .padding(horizontal = DefaultPadding)
                    .weight(1f)
            ) {
                uiState.filteredContacts?.let {
                    items(it, key = { item ->
                        item
                    }) { item ->
                        ContactRow(
                            contact = item,
                            onClick = { contact ->
                                emailFieldState.text = contact.email
                            }
                        )
                    }
                }
            }
        } else {
            NonContactView(emailFieldState)
            Spacer(modifier = Modifier.weight(1f))
        }

        TextButton(
            modifier = Modifier
                .imePadding()
                .padding(bottom = DefaultPadding)
                .fillMaxWidth()
                .padding(horizontal = DefaultPadding)
                .height(48.dp)
                .dropShadow(enabled = true),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = LightBlue,
                disabledBackgroundColor = Neutral10,
            ),
            shape = RoundedCornerShape(8.dp),
            enabled = emailFieldState.isValid,
            onClick = {
                val email = emailFieldState.text
                viewModel.clearState()
                onShare.invoke(email) },
        ) {
            Text(
                text = stringResource(R.string.send_invite),
                style = StyleStolzRegularDarkBlue14.copy(color = if (emailFieldState.isValid) White else Neutral30)
            )
        }
    }

    BackHandler {
        viewModel.clearState()
        closeSheet.invoke()
    }
}

@Composable
private fun ContactRow(contact: Contact, onClick: (Contact) -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = HalfPadding)
            .clickable {
                onClick.invoke(contact)
            }
    ) {
        if (contact.picture != null) {
            Image(
                painter = rememberAsyncImagePainter(contact.picture),
                contentDescription = null,
                modifier = Modifier
                    .size(40.dp)
                    .background(color = LightBlue, shape = CircleShape)
                    .clip(CircleShape)
            )
        } else {
            SingleLetterIcon(
                text = contact.getNameOrEmail(),
                backgroundColor = LightBlue,
                size = 40.dp
            )
        }
        Column(
            modifier = Modifier
                .padding(start = HalfPadding)
                .weight(1f)
                .align(Alignment.CenterVertically),
            horizontalAlignment = Alignment.Start,
        ) {
            Text(
                modifier = Modifier,
                text = contact.getNameOrEmail(),
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = StyleStolzRegularBookDarkBlue13
            )
        }
    }
}

@Composable
private fun SearchContactInputField(
    inputFieldState: TextFieldState,
    onValueChange: (String) -> Unit,
    onShareClick: () -> Unit
) {
    Row(
        modifier = Modifier
            .height(DefaultButtonHeight)
            .padding(horizontal = DefaultPadding)
            .border(
                width = DefaultSmallBorder,
                color = if (inputFieldState.isFocused) LightBlue else DefaultBorderColor,
                shape = Shapes.medium
            )
            .background(
                if (inputFieldState.isEditing) DefaultBackground else Color.Transparent,
                shape = Shapes.medium
            ),
        verticalAlignment = Alignment.CenterVertically
    ) {
        BasicTextField(
            value = inputFieldState.text,
            onValueChange = {
                inputFieldState.text = it
                onValueChange.invoke(it)
            },
            singleLine = true,
            decorationBox = { innerTextField ->
                // to center vertically
                Box(
                    modifier = Modifier.fillMaxSize(),
                    contentAlignment = Alignment.CenterStart
                ) {
                    if (inputFieldState.text.isEmpty()) {
                        Text(
                            text = stringResource(id = R.string.share_hint),
                            style = Typography.subtitle2,
                            modifier = Modifier.align(Alignment.CenterStart)
                        )
                    }
                    innerTextField()
                }
            },
            enabled = true,
            textStyle = StyleStolzRegularDarkBlue14,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 12.dp, end = 12.dp)
                .weight(1f)
                .align(Alignment.CenterVertically)
                .onFocusChanged { focusState ->
                    inputFieldState.onFocusChange(focusState.isFocused)
                },
        )
        Image(
            modifier = Modifier
                .padding(end = DefaultPadding)
                .clickable {
                    onShareClick.invoke()
                }
                .size(24.dp),
            painter = painterResource(id = if (inputFieldState.isValid) R.drawable.ic_share_focused else R.drawable.ic_share_default),
            contentDescription = stringResource(id = R.string.send_invite)
        )
    }
}

@Composable
private fun NonContactView(textState: TextFieldState) {
    if (textState.text.isNotEmpty()) {
        Text(
            modifier = Modifier.padding(horizontal = DefaultPadding),
            text = stringResource(id = if (textState.isValid) R.string.select_a_person_title else R.string.keep_typing_title).uppercase(),
            style = StyleStolzRegularDarkBlue12.copy(
                color = Neutral40,
                fontSize = 10.sp,
                lineHeight = 14.sp
            ),
        )

        Row(
            modifier = Modifier
                .padding(DefaultPadding)
                .fillMaxWidth()
                .padding(vertical = HalfPadding)
        ) {
            SingleLetterIcon(
                text = textState.text,
                backgroundColor = Blue3,
                size = 40.dp,
                textStyle = StyleStolzRegularDarkBlue14.copy(
                    color = Neutral40
                ),
                applyBorder = true
            )

            Text(
                modifier = Modifier
                    .padding(start = HalfPadding)
                    .weight(1f)
                    .align(Alignment.CenterVertically),
                text = textState.text,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = StyleStolzRegularDarkBlue12.copy(color = Neutral30)
            )
        }
    }
}