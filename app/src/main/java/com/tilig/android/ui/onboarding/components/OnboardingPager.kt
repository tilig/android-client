package com.tilig.android.ui.onboarding.components

import androidx.annotation.StringRes
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.PagerState
import com.tilig.android.R
import com.tilig.android.ui.onboarding.OnboardingActivity
import com.tilig.android.ui.theme.*

@ExperimentalAnimationApi
@ExperimentalPagerApi
@Composable
fun OnboardingPager(
    pagerState: PagerState,
    modifier: Modifier
) {
    Box(modifier = modifier.padding(top = HalfPadding)) {
        HorizontalPager(count = OnboardingActivity.PAGES, state = pagerState) { position ->
            when (position) {
                0 -> OnboardingPage(
                    imageRes = R.drawable.img_onboarding_1,
                    titleRes = R.string.onboarding_body_1_title,
                    descriptionRes = R.string.onboarding_body_1_desc
                )
                1 -> OnboardingPage(
                    imageRes = R.drawable.img_onboarding_2,
                    titleRes = R.string.onboarding_body_2_title,
                    descriptionRes = R.string.onboarding_body_2_desc
                )
                2 -> OnboardingPage(
                    imageRes = R.drawable.img_onboarding_3,
                    titleRes = R.string.onboarding_body_3_title,
                    descriptionRes = R.string.onboarding_body_3_desc
                )
                3 -> OnboardingPage(
                    imageRes = R.drawable.img_onboarding_4,
                    titleRes = R.string.onboarding_body_4_title,
                    descriptionRes = R.string.onboarding_body_4_desc
                )
            }
        }
    }
}

@ExperimentalPagerApi
@Composable
fun OnboardingPage(
    @StringRes
    titleRes: Int,
    @StringRes
    descriptionRes: Int,
    imageRes: Int,
) {

    ConstraintLayout(
        modifier = Modifier
            .fillMaxSize()
            .statusBarsPadding()
    ) {
        val (image, footer) = createRefs()

        Image(
            painter = painterResource(id = imageRes),
            contentDescription = "",
            modifier = Modifier
                .padding(top = 24.dp)
                .constrainAs(image) {
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    top.linkTo(parent.top)
                    bottom.linkTo(footer.top)
                    width = Dimension.fillToConstraints
                },
            contentScale = ContentScale.FillWidth,
        )

        Column(modifier = Modifier
            .padding(vertical = MediumPadding)
            .fillMaxWidth()
            .constrainAs(footer) {
                start.linkTo(parent.start)
                end.linkTo(parent.end)
                bottom.linkTo(parent.bottom)
                width = Dimension.fillToConstraints
            }) {
            Text(
                text = stringResource(id = titleRes),
                style = TextStyle(
                    fontFamily = Spoof, fontSize = 24.sp, lineHeight = 36.sp,
                    color = DarkBlue, fontWeight = FontWeight.Bold
                ),
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
            )
            Spacer(modifier = Modifier.height(12.dp))
            Text(
                text = stringResource(id = descriptionRes),
                style = TextStyle(
                    fontFamily = Stolzl, fontSize = 16.sp,
                    color = Neutral40
                ),
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .padding(horizontal = DefaultPadding)
            )
        }
    }
}

@ExperimentalAnimationApi
@ExperimentalPagerApi
@Composable
@Preview
fun OnboardingPagerPreview() {
    OnboardingPager(
        pagerState = PagerState(0),
        modifier = Modifier
    )
}

@ExperimentalPagerApi
@Composable
@Preview
fun OnboardingPagePreview() {
    OnboardingPage(
        imageRes = R.drawable.img_onboarding_1,
        titleRes = R.string.onboarding_body_1_title,
        descriptionRes = R.string.onboarding_body_1_desc,
    )
}
