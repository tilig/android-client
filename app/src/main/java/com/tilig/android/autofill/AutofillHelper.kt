package com.tilig.android.autofill

import android.content.Context
import android.util.Log
import com.tilig.android.utils.SharedPrefs
import com.tilig.android.utils.VersionUtil
import com.tilig.android.utils.getDomain

class AutofillHelper {

    // TODO Delete the companion object once migration to Compose is done.
    companion object {

        /*
        // These browsers work natively with the Autofill Framework.
        val TRUSTED_BROWSERS = arrayOf(
            "com.duckduckgo.mobile.android",
            "com.google.android.googlequicksearchbox",
            "org.mozilla.focus",
            "org.mozilla.focus.beta",
            "org.mozilla.focus.nightly",
            "org.mozilla.klar"
        )

        // These browsers work using the compatibility shim for the Autofill Framework
        // This list must always match the @xml/service_configuration list exactly
        val COMPAT_BROWSERS = arrayOf(
            "alook.browser",
            "alook.browser.google",
            "com.amazon.cloud9",
            "com.android.browser",
            "com.android.chrome",
            "com.android.htmlviewer",
            "com.avast.android.secure.browser",
            "com.avg.android.secure.browser",
            "com.brave.browser",
            "com.brave.browser_beta",
            "com.brave.browser_default",
            "com.brave.browser_dev",
            "com.brave.browser_nightly",
            "com.chrome.beta",
            "com.chrome.canary",
            "com.chrome.dev",
            "com.cookiegames.smartcookie",
            "com.cookiejarapps.android.smartcookieweb",
            "com.ecosia.android",
            "com.google.android.apps.chrome",
            "com.google.android.apps.chrome_dev",
            "com.google.android.captiveportallogin",
            "com.jamal2367.styx",
            "com.kiwibrowser.browser",
            "com.kiwibrowser.browser.dev",
            "com.microsoft.emmx",
            "com.microsoft.emmx.beta",
            "com.microsoft.emmx.canary",
            "com.microsoft.emmx.dev",
            "com.mmbox.browser",
            "com.mmbox.xbrowser",
            "com.mycompany.app.soulbrowser",
            "com.naver.whale",
            "com.opera.browser",
            "com.opera.browser.beta",
            "com.opera.mini.native",
            "com.opera.mini.native.beta",
            "com.opera.touch",
            "com.qflair.browserq",
            "com.qwant.liberty",
            "com.sec.android.app.sbrowser",
            "com.sec.android.app.sbrowser.beta",
            "com.stoutner.privacybrowser.free",
            "com.stoutner.privacybrowser.standard",
            "com.vivaldi.browser",
            "com.vivaldi.browser.snapshot",
            "com.vivaldi.browser.sopranos",
            "com.yandex.browser",
            "com.z28j.feel",
            "idm.internet.download.manager",
            "idm.internet.download.manager.adm.lite",
            "idm.internet.download.manager.plus",
            "io.github.forkmaintainers.iceraven",
            "mark.via",
            "mark.via.gp",
            "net.slions.fulguris.full.download",
            "net.slions.fulguris.full.download.debug",
            "net.slions.fulguris.full.playstore",
            "net.slions.fulguris.full.playstore.debug",
            "org.adblockplus.browser",
            "org.adblockplus.browser.beta",
            "org.bromite.bromite",
            "org.bromite.chromium",
            "org.chromium.chrome",
            "org.codeaurora.swe.browser",
            "org.gnu.icecat",
            "org.mozilla.fenix",
            "org.mozilla.fenix.nightly",
            "org.mozilla.fennec_aurora",
            "org.mozilla.fennec_fdroid",
            "org.mozilla.firefox",
            "org.mozilla.firefox_beta",
            "org.mozilla.reference.browser",
            "org.mozilla.rocket",
            "org.torproject.torbrowser",
            "org.torproject.torbrowser_alpha",
            "org.ungoogled.chromium.extensions.stable",
            "org.ungoogled.chromium.stable",
            "us.spotco.fennec_dos",
        )
        */

        val BLACKLISTED_APP_IDS = arrayOf(
            "android",
            "com.android.settings",
//            "com.tilig.android",
//            "com.tilig.android.debug",
            "com.oneplus.applocker",
        )

        private val BLACKLISTED_WEBSITES = arrayOf(
            "app.tilig.com",
            "tilig-staging.netlify.app"
        )

        fun getAutofill(context: Context) : Autofill = if (VersionUtil.supportsAutofill()) {
            AutofillOreo(context)
        } else {
            AutofillCompat()
        }

        fun isBlacklistedWebsite(website: String?) = BLACKLISTED_WEBSITES.contains(
            website?.getDomain()
        )
    }

    private fun isSupported(): Boolean = VersionUtil.supportsAutofill()

    private fun isEnabled(context: Context): Boolean {
        val afm = getAutofill(context = context)
        Log.v("AutofillHelper", "isEnabled() : afm.isAutofillSupported() = ${afm.isAutofillSupported()}, afm.isAutofillEnabled() = ${afm.isAutofillEnabled()}")
        return afm.isAutofillSupported() && afm.isAutofillEnabled()
    }

    fun shouldShowAutofill(prefs: SharedPrefs, context: Context): Boolean {
        return if (prefs.isAutofillEnabled == null) {
            prefs.mayAskForAutofill && isSupported() && !isEnabled(context = context)
        } else {
            prefs.mayAskForAutofill && isSupported() && !prefs.isAutofillEnabled!!
        }
    }

    fun shouldShowAutofillSnackbar(prefs: SharedPrefs, context: Context): Boolean {
        Log.v("AutofillHelper", "shouldShowSnackbar(): isSupported() = ${isSupported()}, prefs.isEnabled = ${prefs.isAutofillEnabled}")
        return isSupported() && !isEnabled(context = context)
    }
}
