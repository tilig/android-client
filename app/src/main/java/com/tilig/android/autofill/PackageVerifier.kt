package com.tilig.android.autofill

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import com.tilig.android.utils.VersionUtil
import java.io.ByteArrayInputStream
import java.security.MessageDigest
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.util.*
import android.content.pm.PackageManager.NameNotFoundException

import android.graphics.Bitmap

import android.graphics.drawable.Drawable


/**
 * Similar to the one found in Google's samples, but:
 * - updated to grab signatures on SDK 28+ without deprecated methods
 * - can lookup an app's name by its package name
 */
object PackageVerifier {

    private const val TAG = "PackageVerifier"

    /**
     * Attempts to get the app name from the package name.
     * Note that this is not always possible, and no longer possible
     * at all since SDK 30. Luckily we found a clever way of re-enabling
     * it for SDK 30+, however, it is not a guarantee that it works.
     * For sure, it only works if the app is installed.
     * This method also checks if the app is a browser or not. If it
     * is, it will return `null`, because logging in to facebook.com in your Chrome
     * browser should not match 'Chrome'.
     */
    fun getAppName(context: Context, packageName: String?): String? {
        if (packageName == null) {
            return null
        }
        return try {
            val info =
                context.packageManager.getApplicationInfo(packageName, PackageManager.GET_META_DATA)
            context.packageManager.getApplicationLabel(info) as String
        } catch (e: NameNotFoundException) {
            // An exception is normal; it just means the app is not installed
            null
        }
    }

    /**
     * Since Android 11, we can no longer check for packages without
     * requesting intrusive permissions. So we need to skip this.
     */
    private fun isCertificateCheckPossible(): Boolean {
        return VersionUtil.supportsCertificateCheck()
    }

    /**
     * Verifies if a package is valid by matching its certificate with the previously stored
     * certificate.
     */
    fun isValidPackage(context: Context, packageName: String?): Boolean {
        if (!isCertificateCheckPossible() || packageName == null) {
            return true
        }

        val hash: String
        try {
            hash = getCertificateHash(context, packageName)
            Log.d(TAG, "Hash for $packageName: $hash")
        } catch (e: Exception) {
            Log.w(TAG, "Error getting hash for $packageName: $e")
            return false
        }

        return verifyHash(context, packageName, hash)
    }

    @Suppress("DEPRECATION")
    @SuppressLint("PackageManagerGetSignatures")
    private fun getCertificateHash(context: Context, packageName: String): String {
        val pm = context.packageManager
        val cert: ByteArray

        // Android Pie supports certificate history
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            val packageInfo =
                pm.getPackageInfo(packageName, PackageManager.GET_SIGNING_CERTIFICATES)
            val infos = packageInfo.signingInfo
            val signatures = infos.signingCertificateHistory
            // TODO: check all instead of only one?
            cert = signatures.last().toByteArray()
        } else {
            val packageInfo = pm.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)
            val signatures = packageInfo.signatures
            cert = signatures[0].toByteArray()
        }
        ByteArrayInputStream(cert).use { input ->
            val factory = CertificateFactory.getInstance("X509")
            val x509 = factory.generateCertificate(input) as X509Certificate
            val md = MessageDigest.getInstance("SHA256")
            val publicKey = md.digest(x509.encoded)
            return toHexFormat(publicKey)
        }
    }

    private fun toHexFormat(bytes: ByteArray): String {
        val builder = StringBuilder(bytes.size * 2)
        for (i in bytes.indices) {
            var hex = Integer.toHexString(bytes[i].toInt())
            val length = hex.length
            if (length == 1) {
                hex = "0" + hex
            }
            if (length > 2) {
                hex = hex.substring(length - 2, length)
            }
            builder.append(hex.toUpperCase(Locale.ROOT))
            if (i < bytes.size - 1) {
                builder.append(':')
            }
        }
        return builder.toString()
    }

    private fun verifyHash(context: Context, packageName: String, hash: String): Boolean {
        val prefs = context.applicationContext.getSharedPreferences(
            "package-hashes", Context.MODE_PRIVATE
        )
        if (!prefs.contains(packageName)) {
            Log.d(TAG, "Creating initial hash for $packageName")
            prefs.edit().putString(packageName, hash).apply()
            return true
        }

        val existingHash = prefs.getString(packageName, null)
        if (hash != existingHash) {
            Log.w(
                TAG, "hash mismatch for " + packageName + ": expected " + existingHash
                        + ", got " + hash
            )
            return false
        }
        return true
    }

    fun getIcon(context:Context, packageName: String) : Drawable? =
        try {
            context.packageManager.getApplicationIcon(packageName)
        } catch (e: NameNotFoundException) {
            // An exception is normal; it just means the app is not installed
            null
        }
}