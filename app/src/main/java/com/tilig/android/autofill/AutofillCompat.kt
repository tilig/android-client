package com.tilig.android.autofill

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import android.view.autofill.AutofillManager
import androidx.annotation.RequiresApi
import com.tilig.android.utils.VersionUtil
import io.sentry.Sentry

/**
 * Abstract interface that 'hides' the AutofillManager references
 * on incompatible (pre-Oreo) devices
 */
interface Autofill {

    fun isAutofillSupported(): Boolean

    fun isAutofillEnabled(): Boolean

    fun commit()

}

class AutofillCompat : Autofill {

    override fun isAutofillSupported(): Boolean = false

    override fun isAutofillEnabled(): Boolean = false

    override fun commit() {
        // no-op
    }
}

@RequiresApi(Build.VERSION_CODES.O)
class AutofillOreo(val context: Context) : Autofill {

    private val autofillManager: AutofillManager? = try {
            context.getSystemService(AutofillManager::class.java)
        } catch (e: ClassNotFoundException) {
            null
        } catch (e: NoClassDefFoundError) {
            null
        }

    override fun isAutofillSupported(): Boolean =
        if (autofillManager != null
            && VersionUtil.supportsAutofill()
        ) {
            val hasFeature =
                context.packageManager.hasSystemFeature(PackageManager.FEATURE_AUTOFILL)
            hasFeature && autofillManager.isAutofillSupported
        } else {
            false
        }

    override fun isAutofillEnabled(): Boolean =
        if (VersionUtil.supportsAutofill()
            && autofillManager is AutofillManager
        ) {
            try {
                autofillManager.hasEnabledAutofillServices()
            } catch (e : RuntimeException) {
                // This seems to be a bug in Android 13? It doesn't happen a lot and there
                // is nothing we can do about it, unfortunately. We used to log it to Sentry.
                // Sentry.captureMessage("Fail to get enabled autofill services status: ${e.message}")
                Log.e("AutofillOreo", e.message, e)
                // Return true here, otherwise we'd be nagging the user while autofill might already be enabled
                true
            }
        } else {
            false
        }

    override fun commit() {
        autofillManager?.commit()
    }
}
