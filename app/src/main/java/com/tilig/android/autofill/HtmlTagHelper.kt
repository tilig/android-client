package com.tilig.android.autofill

import android.app.assist.AssistStructure
import android.os.Build
import android.view.ViewStructure
import androidx.annotation.RequiresApi

@RequiresApi(Build.VERSION_CODES.O)
object HtmlTagHelper {

    private val FORBIDDEN_INPUT_TYPES = arrayOf(
        // "button",
        "checkbox", "color", "date", "datetime-local",
        //"email",
        "file",
        //"hidden",
        "image", "month",
        //"number", "password",
        "radio", "range", "reset", "search",
        // "submit", "tel", "text",
        "time", "url", "week"
    )

    fun getId(htmlInfo: ViewStructure.HtmlInfo?) : String? = getAttribute(htmlInfo, "id")

    fun getHint(htmlInfo: ViewStructure.HtmlInfo?) = getAttribute(htmlInfo, "placeholder")

    private fun hasAttributeWithValue(htmlInfo: ViewStructure.HtmlInfo?, key: String, value: String) : Boolean {
        return htmlInfo?.attributes?.any { p ->
            (p.first == key && p.second == value)
        } == true
    }

    private fun getAttribute(htmlInfo: ViewStructure.HtmlInfo?, key: String) : String? {
        return htmlInfo?.attributes?.firstOrNull { p ->
            p.first == key
        }?.second
    }

    private fun idLooksLike(htmlInfo: ViewStructure.HtmlInfo?, value: String) : Boolean {
        return htmlInfo?.attributes?.any { p ->
            (p.first == "id" && p.second.contains(value))
        } == true
    }

    private fun isInputOfType(htmlInfo: ViewStructure.HtmlInfo?, type: String) : Boolean {
        return htmlInfo?.tag == "input"
                && hasAttributeWithValue(htmlInfo, "type", type)
    }

    fun isTextField(htmlInfo: ViewStructure.HtmlInfo?) : Boolean {
        return htmlInfo?.tag == "input"
                && !FORBIDDEN_INPUT_TYPES.contains(getAttribute(htmlInfo, "type"))
    }

    fun isPasswordField(viewNode: AssistStructure.ViewNode) =
        isInputOfType(viewNode.htmlInfo, "password")

    fun isSubmitButton(viewNode: AssistStructure.ViewNode) =
        isInputOfType(viewNode.htmlInfo, "submit")
                || (isInputOfType(viewNode.htmlInfo, "button")
                    && idLooksLike(viewNode.htmlInfo, "submit"))
                || (viewNode.htmlInfo?.tag == "button"
                    && idLooksLike(viewNode.htmlInfo, "submit"))

}
