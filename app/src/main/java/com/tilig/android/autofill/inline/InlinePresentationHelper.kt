package com.tilig.android.autofill.inline

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Icon
import android.os.Build
import android.service.autofill.InlinePresentation
import android.widget.inline.InlinePresentationSpec
import androidx.annotation.DrawableRes
import androidx.annotation.RequiresApi
import androidx.annotation.StringRes
import androidx.autofill.inline.UiVersions
import androidx.autofill.inline.v1.InlineSuggestionUi
import com.tilig.android.R

@RequiresApi(Build.VERSION_CODES.R)
object InlinePresentationHelper {

    fun newInlineViews(
        context: Context,
        inlinePresentationSpec: InlinePresentationSpec,
        remoteViewsText: String,
        remoteSubText: String?,
        @DrawableRes drawableId: Int?
    ): InlinePresentation? {
        // InlinePresentation requires nonNull pending intent (even though we only utilize one for
        // the "my vault" presentation) so we're including an empty one here
        val pendingIntent = PendingIntent.getService(
            context,
            0,
            Intent(),
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_ONE_SHOT or PendingIntent.FLAG_UPDATE_CURRENT
        )
        return newInlineViews(
            context,
            inlinePresentationSpec,
            pendingIntent,
            remoteViewsText,
            remoteSubText,
            drawableId
        )
    }

    fun newInlineViews(
        context: Context,
        inlinePresentationSpec: InlinePresentationSpec,
        pendingIntent: PendingIntent,
        @StringRes remoteViewsTextRes: Int,
        remoteSubText: String?,
        @DrawableRes drawableId: Int? = null
    ): InlinePresentation? = newInlineViews(
        context,
        inlinePresentationSpec,
        pendingIntent,
        context.getString(remoteViewsTextRes),
        remoteSubText,
        drawableId
    )

    // Not sure why this is needed, but the IDE demanded it. Both the Android source and the
    // official examples do not consider calling `.slice` to be a restricted API.
    @SuppressLint("RestrictedApi")
    fun newLogo(
        context: Context,
        inlinePresentationSpec: InlinePresentationSpec,
        pendingIntent: PendingIntent
    ): InlinePresentation? {

        // Make sure that the IME spec claims support for v1 UI template.
        val imeStyle = inlinePresentationSpec.style
        if (!UiVersions.getVersions(imeStyle).contains(UiVersions.INLINE_UI_VERSION_1)) {
            return null
        }
        val contentBuilder = InlineSuggestionUi.newContentBuilder(pendingIntent)
//            .setContentDescription(remoteViewsText)
//            .setTitle("")
        Icon.createWithResource(context, R.drawable.ic_squirrel)?.let {
            contentBuilder.setStartIcon(it)
        }

        val content = contentBuilder.build()
        val slice = content.slice
        if (slice != null) {
            return InlinePresentation(slice, inlinePresentationSpec, true)
        }
        return null
    }

    // Not sure why this is needed, but the IDE demanded it. Both the Android source and the
    // official examples do not consider calling `.slice` to be a restricted API.
    @SuppressLint("RestrictedApi")
    private fun newInlineViews(
        context: Context,
        inlinePresentationSpec: InlinePresentationSpec,
        pendingIntent: PendingIntent,
        remoteViewsText: String,
        remoteSubText: String?,
        @DrawableRes drawableId: Int? = null
    ): InlinePresentation? {

        // Make sure that the IME spec claims support for v1 UI template.
        val imeStyle = inlinePresentationSpec.style
        if (!UiVersions.getVersions(imeStyle).contains(UiVersions.INLINE_UI_VERSION_1)) {
            return null
        }
        val contentBuilder = InlineSuggestionUi.newContentBuilder(pendingIntent)
//            .setContentDescription(remoteViewsText)
            .setTitle(remoteViewsText)
        if (remoteSubText != null) {
            contentBuilder.setSubtitle(remoteSubText)
        }
        if (drawableId != null) {
            Icon.createWithResource(context, drawableId)?.let {
                contentBuilder.setStartIcon(it)
            }
        }

        val content = contentBuilder.build()
        val slice = content.slice
        if (slice != null) {
            return InlinePresentation(slice, inlinePresentationSpec, false)
        }
        return null
    }
}
