package com.tilig.android.autofill.google

import android.app.assist.AssistStructure
import android.content.Context
import android.os.Build
import android.os.DeadObjectException
import android.service.autofill.FillRequest
import android.service.autofill.SaveRequest
import android.text.InputType
import android.text.InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD
import android.util.Log
import android.view.View
import android.view.autofill.AutofillId
import androidx.annotation.RequiresApi
import androidx.autofill.HintConstants
import com.google.android.autofillframework.multidatasetservice.AutofillFieldMetadataCollection
import com.google.android.autofillframework.multidatasetservice.model.FilledAutofillField
import com.google.android.autofillframework.multidatasetservice.model.FilledAutofillFieldCollection
import com.tilig.android.BuildConfig
import com.tilig.android.autofill.HtmlTagHelper
import com.tilig.android.autofill.PackageVerifier
import com.tilig.android.autofill.TiligService
import com.tilig.android.utils.elseLet
import org.koin.core.component.KoinComponent
import java.util.*


@RequiresApi(Build.VERSION_CODES.O)
open class StructureParser : KoinComponent {

    // If the parser is targeting a website and not a native app,
    // this field will be set with the URL of the website.
    // Note: even this is set, it does not mean we are filling a website!
    // it can be a WebView-based app with a file:// url
    private var website: String? = null
    private var hasWebView = false
    var isWebsite = false
        private set
    var isCompatBrowser = false
        private set

    @Suppress("UNCHECKED_CAST")
    val ignoreIds: Array<out AutofillId>
        get() = autofillFields.ignoreIds.toTypedArray()

    var hasParsedForSave = false
    val isValid: Boolean
    private val structure: AssistStructure
    val autofillFields = AutofillFieldMetadataCollection()
    var filledAutofillFieldCollection: FilledAutofillFieldCollection =
        FilledAutofillFieldCollection()
        private set

    private val debugLogger = StringBuilder()

    constructor(appContext: Context, request: FillRequest) {
        structure = request.fillContexts.last().structure
        isValid = isValidPackage(appContext)
    }

    constructor(appContext: Context, request: SaveRequest) {
        structure = request.fillContexts.last().structure
        isValid = isValidPackage(appContext)
    }

    companion object {
        private const val TAG = "Autofill"

        // When we're parsing input fields fields on a website instead of
        // native app, we check the webScheme. However, it may be null,
        // also, on < SDK 28 it is unavailable. In
        // that case we pick a default.
        // The web scheme is used when finding the matching account
        // by its website (technically this value could be bogus,
        // the [AccountMatcher] will still check the main domain, but we
        // need a scheme to parse it to a Uri later).
        private const val DEFAULT_WEB_SCHEME = "https"

        // These lists help to match EditText fields that have no Autofill settings.
        // By looking at the hint and id name we can figure out if those are
        // email, username or password fields
        // (This one might be inspired by Bitwarden)
        private val IGNORE_SEARCH_HINTS = arrayOf(
            "search", "find", "recipient", "edit"
        )
        private val USERNAME_HINTS = arrayOf(
            "username", "user name"
        )

        // E.g. Twitter uses the idEntry "login_identifier"
        private val USERNAME_ID_ENTRIES = arrayOf(
            "username", "user name", "user_name", "identifier"
        )
        private val PASSWORD_HINTS = arrayOf(
            "password", "pass phrase", "passphrase"
        )
        private val PASSWORD_ID_ENTRIES = arrayOf(
            "password", "passwd", "pass phrase", "passphrase"
        )
        private val EMAIL_HINTS = arrayOf(
            "email"
        )
        private val EMAIL_ID_ENTRIES = arrayOf(
            "email"
        )

        /**
         * This list is created by observing a bunch of 2FA forms on popular websites.
         * Some values are common (or parts of common IDs), others are specifically targeted
         * for certain websites.
         * Examples:
         * - Amazon: "auth-mfa-otpcode"
         * - Facebook: "approvals_code"
         * - LinkedIn: "input__phone_verification_pin"
         * - Paypal: "ci-otpCode-0" (0-5)
         * - Reddit: "otp"
         * - Roblox: "two-step-verification-code-input"
         */
        private val OTP_ID_ENTRIES = arrayOf(
            // Common
            "mfa", "2fa", "twofa", "two-fa", "otp", "pin", "two-step", "verification",
            // Facebook
            "approvals_code",
        )

        /**
         * List of inputTypes that we accept as password fields.
         * Note that we do only exact matches since recently. Even though
         * inputTypes are build up of combined flags, checking if the flag for Password
         * is part of the inputType is not enough. Because, there are combinations that DO include
         * the password but ARE NOT actually password fields. Good example is 177, which we commonly
         * found on search bars (YouTube e.g.) which is:
         *
         * TYPE_TEXT_VARIATION_PASSWORD
         *  & TYPE_TEXT_VARIATION_EMAIL_ADDRESS
         *  & TYPE_TEXT_VARIATION_PASSWORD
         *  & TYPE_CLASS_TEXT
         *
         * This includes 2 password indicators, but it is a search bar!
         * See the sheet 'inputTypes' here for more information
         */
        private val INPUT_TYPES_PASSWORD = arrayOf(
            // Disabled number_variation_password, it triggers the Samsung browser's address bar
            //InputType.TYPE_NUMBER_VARIATION_PASSWORD,
            InputType.TYPE_TEXT_VARIATION_PASSWORD,
            // This is just TYPE_TEXT_VARIATION_PASSWORD + TYPE_TEXT_VARIATION_URI
            InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD + InputType.TYPE_CLASS_TEXT,
            // 129 = password
            InputType.TYPE_TEXT_VARIATION_PASSWORD + InputType.TYPE_CLASS_TEXT,
            // 145 = visible password field in our Test Lab app. Should NOT be included in the _WEB fields!
            InputType.TYPE_TEXT_VARIATION_PASSWORD + InputType.TYPE_TEXT_VARIATION_URI + InputType.TYPE_CLASS_TEXT
        )
        private val INPUT_TYPES_PASSWORD_WEB = arrayOf(
            // These two are not included in the web browser filters, because, as we discovered,
            // regular input fields (search, email) ALSO include BOTH of those flags, even though
            // they are not passwords at all. As far as we know, VARIATION_WEB_PASSWORD IS save to use.

            //InputType.TYPE_TEXT_VARIATION_PASSWORD,
            //InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD,
            TYPE_TEXT_VARIATION_WEB_PASSWORD,
            TYPE_TEXT_VARIATION_WEB_PASSWORD + InputType.TYPE_CLASS_TEXT
        )
        // We ignore this one for browsers in compat mode, but ONLY when AUTOSAVING, NOT when AUTOFILLING.
        // It is okay to autofill, but when saving it could otherwise save it with masked values.
        private const val IGNORE_INPUTTYPE_FOR_AUTOSAVE = TYPE_TEXT_VARIATION_WEB_PASSWORD + InputType.TYPE_CLASS_TEXT
        private val INPUT_TYPES_EMAIL = arrayOf(
            //InputType.TYPE_TEXT_VARIATION_EMAIL_SUBJECT,
            // 33 = email input
            InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS + InputType.TYPE_CLASS_TEXT,
            InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS + InputType.TYPE_CLASS_TEXT
        )
        private val INPUT_TYPES_USERNAME = arrayOf(
            InputType.TYPE_TEXT_VARIATION_PERSON_NAME + InputType.TYPE_CLASS_TEXT,
            InputType.TYPE_CLASS_PHONE
        )
    }

    fun debug(message: String) {
        if (TiligService.DEBUG_LOG_ALL_FIELDS) {
            Log.v(TAG, message)
            debugLogger.append(message).append("\n")
        }
    }
    fun getDebugLog(): String = debugLogger.toString()

    fun parseForSave(): FilledAutofillFieldCollection {
        parse(false)
        return filledAutofillFieldCollection
    }

    fun parse(forFill: Boolean = true): AutofillFieldMetadataCollection {

        // Safety check #1: check if certificate of app is not changed
        if (!isValid && !BuildConfig.DEBUG) {
            return autofillFields
        }

        // Recursive 1st pass to find out if we're autofilling a website or an app
        val nodes = structure.windowNodeCount
        hasWebView = (0 until nodes).any {
            recursiveScanForWebviews(structure.getWindowNodeAt(it).rootViewNode)
        }
        if (TiligService.DEBUG_LOG_ALL_FIELDS) {
            Log.v(TAG, "First pass completed. Has WebView? $hasWebView. Is website? $isWebsite. URL is $website.")
            if (isWebsite) {
                debug("Scanning website for URL $website for autofillable fields")
            } else {
                debug("Scanning ${if (hasWebView) "webview-based" else "native"} app for autofillable fields")
            }
        }

        // Recursive 2nd pass for parsing
        filledAutofillFieldCollection = FilledAutofillFieldCollection()
        for (i in 0 until nodes) {
            parseRecursiveNodes(forFill, structure.getWindowNodeAt(i).rootViewNode)
        }
        if (!forFill) {
            hasParsedForSave = true
        }

        return autofillFields
    }

    /**
     * Checks if we're parsing HTML elements or a native app view
     */
    private fun recursiveScanForWebviews(viewNode: AssistStructure.ViewNode): Boolean {
        if (!viewNode.webDomain.isNullOrEmpty()) {
            var scheme = DEFAULT_WEB_SCHEME
            if (Build.VERSION.SDK_INT >= 28) {
                scheme = viewNode.webScheme ?: DEFAULT_WEB_SCHEME
            }
            isWebsite = true
            website = "${scheme}://${viewNode.webDomain}"
            Log.v(TAG, "WebView found for url $website (by finding web domain)")
            return true
        // We used to look for web-based apps and treat them as apps instead of websites.
        // This code is now disabled because it triggers on Chrome as well. Instead, we
        // finish the search tree now and use the web-app detection only as a fallback when
        // we are really sure that no web domains are found.
        /* } else if (viewNode.className == "android.webkit.WebView") {
            var scheme = DEFAULT_WEB_SCHEME
            if (Build.VERSION.SDK_INT >= 28) {
                scheme = viewNode.webScheme ?: DEFAULT_WEB_SCHEME
            }
            // We did find a WebView but the webDomain is empty, so
            // we don't think this is a browser. Just an HTML-based app.
            isWebsite = false
            website = "${scheme}://"
            Log.v(TAG, "WebView found for url $website (by finding WebView instance)")
            return true */
        } else {
            val childrenSize = viewNode.childCount
            for (i in 0 until childrenSize) {
                val foundWebview = recursiveScanForWebviews(viewNode.getChildAt(i))
                if (foundWebview) {
                    return true
                }
            }
            return false
        }
    }

    fun getWebsite() = if (isWebsite) website else null

    /**
     * Parses the viewtree recursively, looking for all autofillable fields.
     * @param forFill Indicates if we are parsing with the purpose of autoFILLING later (true) or autoSAVING (false).
     * @param viewNode The root note to check. This method recurses to parse all this viewNode's children.
     */
    private fun parseRecursiveNodes(forFill: Boolean, viewNode: AssistStructure.ViewNode) {

        // When autofillHints are set by the target app, we use those and don't need to resort to heuristics.
        // Only consider valid values though.
        val hints = viewNode.autofillHints?.filter { h -> AutofillFieldMetadata.isValidHint(h) }?.toTypedArray()

        if (hints.isNullOrEmpty()) {
            // New: if the filters don't matter, we'll do the manual thing.
            // Reason: autofill attribute in WebViews is often something like "chrome-off" or "on" or "off"
            // and not "username" or "password". If the value is "off" we STILL want to autofill
            // it since the instruction is for browsers and not for us.
            // Even if the value is "new-password" (commonly used to shut down PMs) but since Tilig is
            // capable of generating new passwords, it is still relevant for us to trigger autofill
            // on these fields.
            parseNodeWithoutAutofillProps(forFill, viewNode)
        } else {
            if (forFill) {
                if (TiligService.DEBUG_LOG_ALL_FIELDS) {
                    debug("Field with autofillHints (fill): ${hints.joinToString(",")}")
                }
                autofillFields.add(AutofillFieldMetadata(viewNode, hints))
            } else {
                if (TiligService.DEBUG_LOG_ALL_FIELDS) {
                    debug("Field with autofillHints (save): ${hints.joinToString(",")}")
                }
                filledAutofillFieldCollection.add(FilledAutofillField(viewNode, hints))
            }
        }

        val childrenSize = viewNode.childCount
        for (i in 0 until childrenSize) {
            parseRecursiveNodes(forFill, viewNode.getChildAt(i))
        }
    }

    /**
     * Part of the recursive cycle. This method is used for the nodes that
     * do NOT have autofill properties, or where those properties are not sufficient.
     * Instead, it will look at input types, hints, and the htmlInfo.
     * It recurses back into [parseRecursiveNodes], not into itself!
     */
    private fun parseNodeWithoutAutofillProps(
        forFill: Boolean,
        viewNode: AssistStructure.ViewNode
    ) {
        // Don't do smart matching for accounts specifically marked to not autofill
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P
            && (viewNode.importantForAutofill == View.IMPORTANT_FOR_AUTOFILL_NO
                    || viewNode.importantForAutofill == View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS)
        ) {
            return
        }

        val isRelevant: Boolean = isRelevantForAutofill(viewNode)
        val passwordInputFilter = if (hasWebView) {
            INPUT_TYPES_PASSWORD_WEB
        } else {
            INPUT_TYPES_PASSWORD
        }

        if (isRelevant) {
            val hints: Array<String>
            val hint = viewNode.hint?.lowercase(Locale.ROOT) ?: HtmlTagHelper.getHint(viewNode.htmlInfo)
            val idEntry = viewNode.idEntry?.lowercase(Locale.ROOT) ?: HtmlTagHelper.getId(viewNode.htmlInfo)
            val inputType = viewNode.inputType
            var ignoreForAutosave = false
            when {
                passwordInputFilter.contains(inputType) -> {
                    if (TiligService.DEBUG_LOG_ALL_FIELDS) {
                        debug("Field detected with password inputType $inputType, ID: $idEntry, and hint: '$hint'")
                        if (!hasWebView) {
                            val varVisPass =
                                (inputType and InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                            if (varVisPass) {
                                    Log.v(
                                        TAG,
                                        "    this field would've been rejected in previous build - probably a search query field (hint: $hint, inputType: $inputType, ID: $idEntry)"
                                    )
//                                viewNode.autofillId?.let { autofillFields.ignoreIds.add(it) }
//                                return
                            }
                        }
                    }

                    hints = arrayOf(View.AUTOFILL_HINT_PASSWORD)

                    if (inputType == IGNORE_INPUTTYPE_FOR_AUTOSAVE) {
                        debug("Field might be masked when autosaving because of inputtype $inputType")
                        // Note: we used to ignore it for autosave, but we shouldn't do that here!
                        // We should still *trigger* the autofill, and then ask for the password again when the user hits 'yes'.
                        // Oftentimes the password is not even masked, so it actually works fine.
                        //ignoreForAutosave = true
                        isCompatBrowser = true
                    } else {
                        debug("    ($inputType is not on the block list ($IGNORE_INPUTTYPE_FOR_AUTOSAVE) so that's good)")
                    }
                }
                PASSWORD_HINTS.any { hint?.contains(it) == true } -> {
                    if (TiligService.DEBUG_LOG_ALL_FIELDS) {
                        debug("Field detected with password'ish hint: '$hint', inputType: $inputType, and ID: $idEntry")
                    }
                    hints = arrayOf(View.AUTOFILL_HINT_PASSWORD)
                }
                PASSWORD_ID_ENTRIES.any { idEntry?.contains(it) == true } -> {
                    if (TiligService.DEBUG_LOG_ALL_FIELDS) {
                        debug("Field detected with password'ish ID: $idEntry, and hint: '$hint'")
                    }
                    hints = arrayOf(View.AUTOFILL_HINT_PASSWORD)
                }
                INPUT_TYPES_EMAIL.contains(inputType) -> {
                    if (TiligService.DEBUG_LOG_ALL_FIELDS) {
                        val varEmail =
                            (inputType and InputType.TYPE_TEXT_VARIATION_EMAIL_SUBJECT) == InputType.TYPE_TEXT_VARIATION_EMAIL_SUBJECT
                        val varWebEmail =
                            (inputType and InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS) == InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS
                        debug("Field detected with email inputType ($inputType includes VAR_EMAIL_SUBJECT=$varEmail, VAR_WEB_EMAIL_ADDRESS=$varWebEmail), and hint: '$hint'")
                    }
                    hints = arrayOf(View.AUTOFILL_HINT_EMAIL_ADDRESS)
                }
                OTP_ID_ENTRIES.any { idEntry?.contains(it) == true } -> {
                    debug("Field detected with OTP'ish ID: $idEntry, inputType: $inputType, and hint: '$hint'")
                    hints = arrayOf(HintConstants.AUTOFILL_HINT_SMS_OTP)
                    ignoreForAutosave = true
                }
                INPUT_TYPES_USERNAME.contains(inputType) -> {
                    if (TiligService.DEBUG_LOG_ALL_FIELDS) {
                        val varPersName =
                            (inputType and InputType.TYPE_TEXT_VARIATION_PERSON_NAME) == InputType.TYPE_TEXT_VARIATION_PERSON_NAME
                        val clsPhone =
                            (inputType and InputType.TYPE_CLASS_PHONE) == InputType.TYPE_CLASS_PHONE
                        debug("Field detected with name/phone inputType ($inputType includes VAR_PERSON_NAME=$varPersName, CLASS_PHONE=$clsPhone), and hint: '$hint'")
                    }
                    hints = arrayOf(View.AUTOFILL_HINT_USERNAME)
                }
                EMAIL_HINTS.any { hint?.contains(it) == true } -> {
                    debug("Field detected with email'ish hint: '$hint', inputType: $inputType, and ID: $idEntry")
                    hints = arrayOf(View.AUTOFILL_HINT_EMAIL_ADDRESS)
                }
                EMAIL_ID_ENTRIES.any { idEntry?.contains(it) == true } -> {
                    debug("Field detected with email'ish ID: $idEntry, and hint: '$hint'")
                    hints = arrayOf(View.AUTOFILL_HINT_EMAIL_ADDRESS)
                }
                USERNAME_HINTS.any { hint?.contains(it) == true } -> {
                    debug("Field detected with username'ish hint: '$hint', inputType: $inputType, and ID: $idEntry")
                    hints = arrayOf(View.AUTOFILL_HINT_USERNAME)
                }
                USERNAME_ID_ENTRIES.any { idEntry?.contains(it) == true } -> {
                    debug("Field detected with email'ish ID: $idEntry, inputType: $inputType, and hint: '$hint'")
                    hints = arrayOf(View.AUTOFILL_HINT_USERNAME)
                }
                // Check the html info. This'll trigger for apps that use a WebView but are not a browser.
                HtmlTagHelper.isPasswordField(viewNode) -> {
                    debug("HTML password field detected (hint: $hint, inputType: $inputType, ID: $idEntry)")
                    hints = arrayOf(View.AUTOFILL_HINT_PASSWORD)
                }
                HtmlTagHelper.isSubmitButton(viewNode) -> {
                    debug("HTML submit button detected (hint: $hint, inputType: $inputType, ID: $idEntry)")
                    viewNode.autofillId?.let { autofillFields.submitButtonIds.add(it) }
                    return
                }
                else -> {
                    debug("Field rejected by default (hint: $hint, inputType: $inputType, ID: $idEntry)")
                    viewNode.autofillId?.let { autofillFields.ignoreIds.add(it) }
                    return
                }
            }

            if (forFill) {
                autofillFields.add(AutofillFieldMetadata(viewNode, hints, ignoreForAutosave))
            } else {
                filledAutofillFieldCollection.add(FilledAutofillField(viewNode, hints))
            }
        }
    }

    private fun isRelevantForAutofill(viewNode: AssistStructure.ViewNode): Boolean {
        return viewNode.isEnabled
                && (isRelevantAppFieldForAutofill(viewNode) || isRelevantWebsiteFieldForAutofill(
            viewNode
        ))
    }

    private fun isRelevantAppFieldForAutofill(viewNode: AssistStructure.ViewNode): Boolean {
        return viewNode.className?.contains("EditText") == true
    }

    private fun isRelevantWebsiteFieldForAutofill(viewNode: AssistStructure.ViewNode): Boolean {
        // We look for input fields (as long as they resemble text -- no checkboxes or date fields!).
        // We also look for submit buttons, we can use those to trigger autocapture.
        return (viewNode.htmlInfo?.tag == "input" && HtmlTagHelper.isTextField(viewNode.htmlInfo))
                || viewNode.htmlInfo?.tag == "button"
                || viewNode.htmlInfo?.tag == "submit"
    }

    fun getStructPackageName() = try {
        structure.activityComponent?.packageName
    } catch (e: DeadObjectException) {
        null
    }

    private fun isValidPackage(context: Context): Boolean =
        PackageVerifier.isValidPackage(context, getStructPackageName())

    fun findValueForHint(hint: String): String? {
        if (filledAutofillFieldCollection.isEmpty()) {
            return null
        }
        return filledAutofillFieldCollection.getFirstNonNullOrBlank(hint)
    }

}
