package com.tilig.android.autofill.autosave

//import org.koin.core.component.inject

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.service.autofill.SaveInfo
import android.util.Log
import android.view.autofill.AutofillId
import androidx.annotation.RequiresApi
import com.google.android.autofillframework.multidatasetservice.AutofillFieldMetadataCollection
import com.tilig.android.MainActivity
import com.tilig.android.R
import com.tilig.android.autofill.TiligService
import com.tilig.android.utils.SharedPrefs
import io.sentry.Sentry
import io.sentry.SentryLevel
import org.koin.core.component.KoinComponent
import kotlin.random.Random

object AutoSaveHelper : KoinComponent {

    private const val TAG = "AutoSaveHelper"

    // TODO don't open the mainactivity, use a broadcast instead, only for tracking the event.
    // TODO mayyyybe show a dialog asking for 'never ask again' but then we need to implement that as well
    private fun createCancelIntent(
        context: Context,
        website: String?,
        androidAppId: String?
    ): PendingIntent? {
        val intent = Intent(context, MainActivity::class.java).apply {
            putExtra(MainActivity.EXTRA_ADD_APP_ID_STR, androidAppId)
            putExtra(MainActivity.EXTRA_ADD_URL_STR, website)
            // This is a hack that enables us to create two PendingIntents at the same
            // time for the same Activity: one for search, one for creating a new secret
            action = Random.Default.nextInt().toString()
        }
        return PendingIntent.getActivity(
            context,
            123123,
            intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
        )
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun createSaveInfo(
        context: Context,
        prefs: SharedPrefs,
        website: String?,
        androidAppId: String?,
        autofillFieldMetadata: AutofillFieldMetadataCollection
    ): SaveInfo {

        val builder =
            SaveInfo.Builder(
                autofillFieldMetadata.saveType,
                autofillFieldMetadata.getRequiredAutosaveIds()
            )
        autofillFieldMetadata.optionalAutosaveIds.toTypedArray().let {
            if (it.isNotEmpty()) {
                builder.setOptionalIds(it)
            }
        }

        if (autofillFieldMetadata.submitButtonIds.isNotEmpty()) {
            Log.v(
                TAG,
                "Setting ${autofillFieldMetadata.submitButtonIds.first()} as the SUBMIT button ID"
            )
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    builder.setTriggerId(autofillFieldMetadata.submitButtonIds.first())
                }
            } catch (e: NoSuchMethodError) {
                // Note: this is weird, setTriggerId is supported since SDK 26 (8.0 Oreo) but of course
                // this error occurs (according to Sentry) on an 8.1 Samsung. Only once for one user,
                // and this is a non-essential functionality anyway, so let's just catch it.
                Sentry.captureMessage(
                    e.message ?: "builder.setTriggerId is not supported in AutoSaveHelper",
                    SentryLevel.INFO
                )
            }
        } else if (website != null || prefs.devToggleEnableAutosaveFlag) {
            // Only use this when we're on a website. In native apps,
            // it is better to simply wait for the activity to finish,
            // otherwise we might fire too eagerly.
            Log.v(TAG, "Setting flag FLAG_SAVE_ON_ALL_VIEWS_INVISIBLE")
            builder.setFlags(SaveInfo.FLAG_SAVE_ON_ALL_VIEWS_INVISIBLE)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            // We can create an intent in case user cancels. We could offer 'never ask again for this website'
            // functionality, for example.
            //        val cancelIntentSender = createCancelIntent(context, website, androidAppId)?.intentSender

            builder
                .setDescription(context.getString(R.string.autosave_description))
                .setPositiveAction(SaveInfo.POSITIVE_BUTTON_STYLE_SAVE)
                .setNegativeAction(SaveInfo.NEGATIVE_BUTTON_STYLE_CANCEL, null)
        }
        Log.v(TAG,"Added ${(autofillFieldMetadata.requiredAutosaveIds.size + autofillFieldMetadata.optionalAutosaveIds.size)} fields as SaveInfo (save type ${autofillFieldMetadata.saveType})")
        return builder.build()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun hasSaveInfo(autofillFieldMetadata: AutofillFieldMetadataCollection): Boolean {
        return autofillFieldMetadata.saveType != 0
                && (autofillFieldMetadata.requiredAutosaveIds.size + autofillFieldMetadata.optionalAutosaveIds.size) > 0
    }
}
