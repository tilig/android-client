package com.tilig.android.autofill

import android.content.Context
import android.content.Intent
import android.os.*
import android.service.autofill.FillCallback
import android.service.autofill.FillRequest
import android.service.autofill.SaveCallback
import android.service.autofill.SaveRequest
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.autofill.HintConstants
import androidx.fragment.app.activityViewModels
import com.google.android.autofillframework.multidatasetservice.model.FilledAutofillFieldCollection
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.tilig.android.BuildConfig
import com.tilig.android.DevActivity
import com.tilig.android.R
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.autofill.google.StructureParser
import com.tilig.android.data.models.tilig.*
import com.tilig.android.data.repository.Repository
import com.tilig.android.ui.autofill.AutofillActivity
import com.tilig.android.ui.autofill.AutosaveActivity
import com.tilig.android.ui.signin.SignInViewModel
import com.tilig.android.utils.DebugLogger
import com.tilig.android.utils.SharedPrefs
import com.tilig.android.utils.bundleToString
import com.tilig.android.utils.getDomain
import com.tilig.crypto.Crypto
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.CoroutineContext

/**
 * The Big Autofill Service.
 *
 * This service is triggered by the OS when a user is focusing on a form field.
 * It handles:
 * - The autofill
 * - Autocapture
 *
 * The autofill currently shows only one entry: "Autofill with Tilig".
 * We used to show a list of matching accounts, but chose to create a simpler UI.
 * The functionality was still available through developer toggles, but the code was finally
 * removed in commit <92ae24e04ba8394dbfc5be035095fae4840938fc> on Mon Jan 2nd, 2023, by Mathijs.
 *
 * Note that one reason to remove the list of accounts was that the request could take too long to
 * autofill within the allotted window. In case we ever want the functionality back, we need to
 * use local caching first.
 *
 * To improve autofill response times, we also do:
 * - Launch this service as a sticky service, so it lingers a little bit longer after autofilling (for the next time)
 * - Pre-launch it when the user taps the URL in the account detail screen, so that when the autofill, the service is already in memory.
 */
@RequiresApi(Build.VERSION_CODES.O)
open class TiligService : LifecycleAutofillService(), KoinComponent {

    protected val prefs: SharedPrefs by inject()
    private val repo by inject<Repository>()
    private val tracker by inject<Mixpanel>()
    private val debugLogger = DebugLogger()

    private val coroutineContext: CoroutineContext
        get() = Job() + Dispatchers.IO

    val coroutineScope = CoroutineScope(coroutineContext)

    private val _crypto = Crypto()

    private fun waitForKeys(block: (crypto: Crypto) -> Unit) {
        if (_crypto.hasAllKeys()) {
            block(_crypto)
        } else {
            coroutineScope.launch {
                prefs.authKeys.collect { authKeys ->
                    SharedPrefs.initCryptoSafely(
                        keyPair = authKeys.keypair,
                        legacyKeyPair = authKeys.legacy,
                        crypto = _crypto
                    )
                    // Extra safety check to make sure we don't continue without keys
                    if (_crypto.hasAllKeys()) {
                        block(_crypto)
                    } else if (_crypto.hasNewKeys()) {
                        // No old keys present. Is that even possible? It used to happen (at most once)
                        // for some users, but after we do the extra .collect, it should be impossible.
                        // to reach this point.
                        Log.e(TAG, "No OLD keys present")
                        debug("No OLD keys present after .collect()")
                        // We can't continue to the callback, because we'll be trying to save
                        // an account without having keys. The dance ends here.
                    } else if (_crypto.hasLegacyKeys()) {
                        // No NEW keys present
                        // TODO  we can enter the fetch/create flow here if we refactor the SignInViewModel
                        //  (so we can use the getOrGenerateKeyPairs directly without using a ViewModel in a Service),
                        //  orrrrr we can simply take the hit here and let the key upgrade happen when the user
                        //  opens the HomeFragment
                        block(_crypto)
                    }
                }
            }
        }
    }

    companion object {
        const val CLIENT_STATE_ACCOUNT_ID_STR: String = "account_id"
        const val CLIENT_STATE_ACCOUNT: String = "account"
        const val CLIENT_STATE_USERNAME_STR: String = "username"
        const val CLIENT_STATE_PASSWORD_STR: String = "password"

        private const val TAG = "TiligService"
        val DEBUG_LOG_ALL_FIELDS = BuildConfig.DEBUG

        /**
         * Preloads the service in memory.
         * Also triggers the [onStartCommand] method, which returns
         * START_STICKY so we can stay in memory a little bit longer than usual.
         */
        fun preload(context: Context) {
            try {
                context.startService(
                    Intent(
                        context,
                        TiligService::class.java
                    )
                )
            } catch (e: NoClassDefFoundError) {
                Log.e(TAG, "Cannot preload TiligService", e)
            }
        }
    }

    override fun debug(message: String) {
        super.debug(message)
        debugLogger.log(this, prefs, message)
    }

    private fun isAuthenticated(): Boolean {
        val account = GoogleSignIn.getLastSignedInAccount(applicationContext)
        return account != null || prefs.isLoggedIn()
    }

    override fun onFillRequest(
        request: FillRequest,
        cancellationSignal: CancellationSignal,
        callback: FillCallback
    ) {
        debug("===================================== Request received to autoFILL")
        // Cancellations happen if Tilig takes too long to respond
        cancellationSignal.setOnCancelListener {
            debug("Autofill canceled")
            Handler(Looper.getMainLooper()).post {

                // Track this
                tracker.trackEvent(Tracker.EVENT_AUTOFILL_CANCELED)

                if (BuildConfig.DEBUG) {
                    Toast.makeText(
                        applicationContext,
                        R.string.error_canceled,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }

        val parser = StructureParser(applicationContext, request)

        // Security check #1: Check we're not autofilling apps that have changed package signature
        if (!parser.isValid) {
            Toast.makeText(
                applicationContext,
                R.string.error_invalid_package_signature,
                Toast.LENGTH_LONG
            ).show()
            Log.e(
                TAG,
                "App ID ${parser.getStructPackageName()} has changed signatures - not autofilling"
            )
            debug("Not autofilling, invalid package signature detected")
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                // According to the docs, < Q should not use onFailure because it might not work and
                // it might show the message to the user instead of just logging it to Logcat
                callback.onSuccess(null)
            } else {
                callback.onFailure(getString(R.string.error_invalid_package_signature))
            }
            return
        }

        // Security check #2: Check if we're not autofilling blacklisted apps
        // (also convenient for skipping irrelevant apps like system screens)
        if (AutofillHelper.BLACKLISTED_APP_IDS.contains(parser.getStructPackageName())) {
            Log.e(TAG, "App ID ${parser.getStructPackageName()} is blacklisted from Autofilling")
            debug("App ID ${parser.getStructPackageName()} is blacklisted from Autofilling")
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                // According to the docs, < Q should not use onFailure because it might not work and
                // it might show the message to the user instead of just logging it to Logcat
                callback.onSuccess(null)
            } else {
                callback.onFailure(getString(R.string.error_invalid_app))
            }
            return
        }

        //Log.d(TAG, "onFillRequest(): data=" + data.bundleToString())
        try {
            debug("Optional clientState: ${request.clientState.bundleToString(Account::class.java)}")
        } catch (e: BadParcelableException) {
            debug("Unable to log clientState: ${e.message}")
        }
        val fields = parser.parse()
        debug(parser.getDebugLog())

        if (parser.isWebsite && AutofillHelper.isBlacklistedWebsite(parser.getWebsite())) {
            debug("Not autofilling our own website")
            callback.onSuccess(null)
        } else if (!fields.isEmpty()) {
            val response = FillResponseFactory.createSingleTiligResponse(
                this,
                request,
                parser
            )
            debug("Autofill response successfully created")
            callback.onSuccess(response)

            // Track
            tracker.trackEvent(Tracker.EVENT_AUTOFILL_TRIGGERED)
        } else {
            debug("No (relevant) fields to autofill")
            callback.onSuccess(null)
        }
    }

    override fun onSaveRequest(request: SaveRequest, callback: SaveCallback) {
        debug("===================================== Request received to autoSAVE")
        val parser = StructureParser(applicationContext, request)
        val filledFields = parser.parseForSave()

        waitForKeys { crypto ->
            // If we can get accounts, it USUALLY means we have logged in
            // and we can set the keys. Unfortunately, there's always 3 users
            // in a 1000 that encounter an impossible state.
            val isOk = isAuthenticated()
            if (isOk) {
                handleSaveRequest(request, callback, parser, filledFields, crypto)
            } else {
                debug("onSaveRequest: Impossible state: has keys but is not authenticated!")
            }
        }
    }

    private fun handleSaveRequest(
        request: SaveRequest,
        callback: SaveCallback,
        parser: StructureParser,
        filledFields: FilledAutofillFieldCollection,
        crypto: Crypto
    ) {

        if (!filledFields.isEmpty()) {
            val packageName = parser.getStructPackageName()

            val username = parser.findValueForHint(View.AUTOFILL_HINT_USERNAME)
                ?: parser.findValueForHint(HintConstants.AUTOFILL_HINT_NEW_USERNAME)
                ?: parser.findValueForHint(View.AUTOFILL_HINT_EMAIL_ADDRESS)
            var password = parser.findValueForHint(View.AUTOFILL_HINT_PASSWORD)
                ?: parser.findValueForHint(HintConstants.AUTOFILL_HINT_NEW_PASSWORD)

            // If the password comes from a browser that is in compat mode (all of them are),
            // then the password might be masked. If the password contains 2 or more human-readable
            // characters, we consider it unmasked. Yes, this is not a perfect match, but we want to
            // err on the side of caution, so as not to accidentally overwrite existing passwords
            // with "*******d"
            val isMasked = password?.contains("[a-zA-Z0-9]{2,}".toRegex()) != true
            if (!parser.isCompatBrowser || !isMasked) {
                Log.v(
                    TAG,
                    "    onSaveRequest: not a compat browser, or password appears to be unmasked, so we allow to autosave it"
                )
            } else {
                password = null
            }

            if (username != null || password != null) {

                val originalAccount: Account? =
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                        request.clientState?.getParcelable(
                            CLIENT_STATE_ACCOUNT,
                            Account::class.java
                        )
                    } else {
                        request.clientState?.classLoader = Account::class.java.classLoader
                        @Suppress("DEPRECATION")
                        request.clientState?.getParcelable(CLIENT_STATE_ACCOUNT)
                    }
                val filledAccount = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P
                    && originalAccount != null
                    && !originalAccount.isTestDriveItem()
                ) {
                    request.datasetIds?.firstOrNull()
                } else {
                    null
                }

                when {
                    // If username is null, we are at the second step of a 2-step login and we need to ask the username again
                    filledAccount == null && username == null -> showSaveDialog(
                        parser,
                        username = null,
                        password = password
                    )
                    // If we know the username but the password is masked, ask it again
                    filledAccount == null && isMasked -> showSaveDialog(
                        parser,
                        username = username,
                        password = null
                    )
                    // If filledAccount id is null, we're adding a new Account.
                    // If not, we're updating.
                    filledAccount == null -> autosaveNewLogin(
                            parser,
                            packageName,
                            username,
                            password,
                            crypto,
                            callback
                        )
                    else -> autoUpdateExistingAccount(
                            originalAccount,
                            filledAccount,
                            username,
                            password,
                            crypto,
                            callback
                        )
                }
            }

            if (prefs.devToggleEnableDebugAutosave && BuildConfig.DEBUG) {
                // Show edit screen:
                val pending = DevActivity.createPendingIntentForAlertText(
                    this,
                    debugText = """
Autosave triggered with data:
Target app: ${parser.getStructPackageName()}
Target website: ${parser.getWebsite()}
Account ID by clientState: ${request.clientState?.getString(CLIENT_STATE_ACCOUNT_ID_STR, null)}
Target is compat browser: ${parser.isCompatBrowser}

Username by hint: ${parser.findValueForHint(View.AUTOFILL_HINT_USERNAME)}
..  by new hint: ${parser.findValueForHint("newPassword")}
..  by email: ${parser.findValueForHint(View.AUTOFILL_HINT_EMAIL_ADDRESS)}
..  by clientState: ${request.clientState?.getString(CLIENT_STATE_USERNAME_STR, null)}

Password by hint: ${parser.findValueForHint(View.AUTOFILL_HINT_PASSWORD)}
..  by new hint: ${parser.findValueForHint("newPassword")}
..  by clientState: ${request.clientState?.getString(CLIENT_STATE_PASSWORD_STR, null)}
Password is masked? $isMasked
"""
                )
                pending.send()
            }
        }
    }

    private fun autoUpdateExistingAccount(
        originalAccount: Account?,
        filledAccount: String?,
        username: String?,
        password: String?,
        crypto: Crypto,
        callback: SaveCallback
    ) {
        // Check if it is different from the original
        // because otherwise we're tracking too much
        if (username != null && username != originalAccount?.username) {
            tracker.trackEvent(Tracker.EVENT_AUTOSAVE_USERNAME_CAPTURED)
        }
        if (password != null && password != originalAccount?.password) {
            tracker.trackEvent(Tracker.EVENT_AUTOSAVE_PASSWORD_CAPTURED)
        }

        // Update the account with the new information
        originalAccount!!.apply {
            decryptDetails(repo.jsonConverter, crypto)
            this.id = this.id ?: filledAccount
            if (username != null) {
                this.username = username
            }
            if (password != null) {
                this.password = password
            }
        }
        val encryptedAccount = originalAccount.encrypt(
            repo.jsonConverter, crypto
        )

        repo.updateSecretItemCall(encryptedAccount)
            .enqueue(object : Callback<EncryptedItemResponse> {
                override fun onFailure(call: Call<EncryptedItemResponse>, t: Throwable) {
                    Log.e(TAG, "Error saving accounts")
                    callback.onFailure(getString(R.string.error_autosaving_account_changes))
                }

                override fun onResponse(
                    call: Call<EncryptedItemResponse>,
                    response: Response<EncryptedItemResponse>
                ) {
                    response.body()?.let { account ->
                        try {
                            callback.onSuccess()
                        } catch (e: IllegalStateException) {
                            // This can only happen on Sentry, of course.
                            // https://sentry.io/share/issue/94ac5e6b867843d988ba2098cff9f1db/
                            // Happened to one user, once. Maybe doing things very quickly?
                            // Unfortunately we can't check for this, we can only catch
                            // it. There is no proper way of handling this.
                            // The account is already saved so we can show the Toast.
                            Log.e(TAG, "Error calling the onSuccess callback: $e.message")
                        }

                        Toast.makeText(
                            this@TiligService,
                            getString(
                                R.string.toast_autosave_new,
                                originalAccount.name
                            ),
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.v(TAG, "Account saved: ${account.item.id}")
                    }
                }
            })
    }

    private fun autosaveNewLogin(
        parser: StructureParser,
        packageName: String?,
        username: String?,
        password: String?,
        crypto: Crypto,
        callback: SaveCallback
    ) {
        val login = if (parser.isWebsite) {
            parser.getWebsite() ?: getString(R.string.untitled_account)
        } else {
            PackageVerifier.getAppName(this, packageName)
                ?: getString(R.string.untitled_account)
        }
        val newAccount = Account.createBasic(
            name = login,
            website = parser.getWebsite(),
            androidAppId = if (parser.isWebsite) null else packageName
        ).apply {
            this.username = username
            if (password != null) {
                this.password = password
            }
        }
        val encryptedItem =
            EncryptedItem.encrypt(newAccount, repo.jsonConverter, crypto)

        repo.createSecretItemCall(encryptedItem)
            .enqueue(object : Callback<EncryptedItemResponse> {
                override fun onFailure(call: Call<EncryptedItemResponse>, t: Throwable) {
                    Log.e(TAG, "Error saving accounts")
                    callback.onFailure(getString(R.string.error_autosaving_new_account))
                }

                override fun onResponse(
                    call: Call<EncryptedItemResponse>,
                    response: Response<EncryptedItemResponse>
                ) {
                    response.body()?.let { account ->
                        try {
                            callback.onSuccess()
                        } catch (e: IllegalStateException) {
                            // This can only happen on Sentry, of course.
                            // https://sentry.io/share/issue/94ac5e6b867843d988ba2098cff9f1db/
                            // Happened to one user, once. Maybe doing things very quickly?
                            // Unfortunately we can't check for this, we can only catch
                            // it. There is no proper way of handling this.
                            // The account is alreayd saved so we can show the Toast.
                            Log.e(TAG, "Error calling the onSuccess callback: $e.message")
                        }

                        Toast.makeText(
                            this@TiligService,
                            getString(R.string.toast_autosave_new, newAccount.name),
                            Toast.LENGTH_SHORT
                        ).show()

                        Log.v(TAG, "Account saved: ${account.item.id}")
                        tracker.trackAccountCreation(newAccount)
                        if (username != null) {
                            tracker.trackEvent(Tracker.EVENT_AUTOSAVE_USERNAME_CAPTURED)
                        }
                        if (password != null) {
                            tracker.trackEvent(Tracker.EVENT_AUTOSAVE_PASSWORD_CAPTURED)
                        }
                    }
                }
            })
    }

    private fun showSaveDialog(
        parser: StructureParser,
        username: String?,
        password: String?) {
        val showEditScreen = true
        if (showEditScreen) {
            val pending = if (parser.isWebsite) {
                AutosaveActivity.createPendingIntentForCreatingNew(
                    this,
                    applicationId = null,
                    url = parser.getWebsite(),
                    login = parser.getWebsite().getDomain(),
                    username = username,
                    password = password
                )
            } else {
                AutosaveActivity.createPendingIntentForCreatingNew(
                    this,
                    applicationId = parser.getStructPackageName(),
                    login = PackageVerifier.getAppName(
                        this@TiligService,
                        parser.getStructPackageName()
                    ),
                    username = username,
                    password = password
                )
            }
            pending.send()
        }
    }

}
