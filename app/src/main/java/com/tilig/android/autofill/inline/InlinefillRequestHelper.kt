package com.tilig.android.autofill.inline

import android.service.autofill.FillRequest
import android.widget.inline.InlinePresentationSpec
import com.tilig.android.utils.VersionUtil

/**
 * On Android R and up, we can do inline autofill responses.
 * This is similar to the 'regular' datasets but they appear above the keyboard.
 * This object helps creating those datasets, and takes away the need to
 * check for compatibility
 */
object InlineFillRequestHelper {

    /**
     * Checks if inline autofill is supported, and if there is room for at least one
     * response in the [FillRequest].
     */
    fun isInlineSupported(fillRequest: FillRequest) : Boolean {
        if (VersionUtil.supportsInlineAutofill()) {
            val inlineRequest = fillRequest.inlineSuggestionsRequest
            if (inlineRequest != null && inlineRequest.maxSuggestionCount > 0) {
                return true
            }
        }
        return false
    }

    /**
     * Returns the number of slots available in the inline suggestion box, or 0 if
     * the system does not support inline autofill
     */
    fun getSlotCount(fillRequest: FillRequest) : Int = if (!VersionUtil.supportsInlineAutofill()) {
        0
    } else {
        fillRequest.inlineSuggestionsRequest?.inlinePresentationSpecs?.count() ?: 0
    }

    /**
     * We consider the following cases when filling the inline autofill slots:
     * 0 slots - no suggestions
     * 1 slot - Search
     * 2 slots - Best match, Search
     * 3 slots - Best match, Search, Add new account
     * >= 4 slots - Matches 1...n-2, Search, Add new account
     */
    fun shouldShowSearch(fillRequest: FillRequest) : Boolean = getSlotCount(fillRequest) >= 1

    /**
     * We consider the following cases when filling the inline autofill slots:
     * 0 slots - no suggestions
     * 1 slot - Search
     * 2 slots - Best match, Search
     * 3 slots - Best match, Search, Add new account
     * >= 4 slots - Matches 1...n-2, Search, Add new account
     */
    fun shouldShowAdd(fillRequest: FillRequest) : Boolean = getSlotCount(fillRequest) >= 3

    /**
     * We consider the following cases when filling the inline autofill slots:
     * 0 slots - no suggestions
     * 1 slot - Search
     * 2 slots - Best match, Search
     * 3 slots - Best match, Search, Add new account
     * >= 4 slots - Matches 1...n-2, Search, Add new account
     */
    fun getRemainingSlotCount(fillRequest: FillRequest) : Int {
        val slots = getSlotCount(fillRequest)
        return when {
            slots < 2 -> 0
            slots == 2 -> 1
            else -> slots - 2
        }
    }

}
