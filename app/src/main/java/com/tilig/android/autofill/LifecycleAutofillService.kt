package com.tilig.android.autofill

import android.content.Intent
import android.os.Build
import android.service.autofill.AutofillService
import android.util.Log
import androidx.annotation.CallSuper
import androidx.annotation.RequiresApi
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ServiceLifecycleDispatcher
import com.tilig.android.BuildConfig
import com.tilig.android.utils.DebugLogger

/**
 * A LifecycleService that is also an AutofillService
 */
@RequiresApi(Build.VERSION_CODES.O)
abstract class LifecycleAutofillService : AutofillService(), LifecycleOwner {

    private val mDispatcher = ServiceLifecycleDispatcher(this)

    @CallSuper
    override fun onCreate() {
        mDispatcher.onServicePreSuperOnCreate()
        super.onCreate()
        debug("onCreate")
    }

    override fun onConnected() {
        super.onConnected()
        debug("onConnected")
    }

    @Deprecated("Deprecated in Java")
    @CallSuper
    override fun onStart(intent: Intent, startId: Int) {
        mDispatcher.onServicePreSuperOnStart()
        super.onStart(intent, startId)
        debug("onStart")
    }

    // this method is added only to annotate it with @CallSuper.
    // In usual service super.onStartCommand is no-op, but in LifecycleService
    // it results in mDispatcher.onServicePreSuperOnStart() call, because
    // super.onStartCommand calls onStart().
    @CallSuper
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        debug("onStartCommand")
        if (intent != null) {
            super.onStartCommand(intent, flags, startId)
        }
        return START_STICKY
    }

    override fun onDisconnected() {
        debug("onDisconnected")
        super.onDisconnected()
    }

    @CallSuper
    override fun onDestroy() {
        debug("onDestroyed")
        mDispatcher.onServicePreSuperOnDestroy()
        super.onDestroy()
    }

    override fun getLifecycle(): Lifecycle {
        return mDispatcher.lifecycle
    }

    protected open fun debug(message: String) {
        if (BuildConfig.DEBUG) {
            val instanceHash = this.toString().replace("com.tilig.android.autofill.TiligService", "")
            Log.i("SERVICE", "Process instance $instanceHash - $message")
        }
    }
}
