package com.tilig.android.autofill

import android.content.Context
import android.os.Build
import android.service.autofill.Dataset
import android.service.autofill.FillRequest
import android.service.autofill.FillResponse
import android.util.Log
import android.view.View
import android.view.autofill.AutofillValue
import androidx.annotation.DrawableRes
import androidx.annotation.RequiresApi
import androidx.annotation.StringRes
import androidx.core.os.bundleOf
import com.google.android.autofillframework.multidatasetservice.AutofillFieldMetadataCollection
import com.tilig.android.MainActivity
import com.tilig.android.R
import com.tilig.android.autofill.autosave.AutoSaveHelper
import com.tilig.android.autofill.google.StructureParser
import com.tilig.android.autofill.inline.InlineFillRequestHelper
import com.tilig.android.autofill.inline.InlinePresentationHelper
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.repository.Repository
import com.tilig.android.ui.autofill.AutofillActivity
import com.tilig.android.ui.autofill.RemoteViewHelper
import com.tilig.android.utils.*
import com.tilig.crypto.Crypto
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

enum class AuthLevel {
    AUTHENTICATED_UNLOCKED,

    //AUTHENTICATED_LOCKED,
    NOT_AUTHENTICATED
}

@RequiresApi(Build.VERSION_CODES.O)
object FillResponseFactory : KoinComponent {

    private val prefs: SharedPrefs by inject()
    private val repo: Repository by inject()
    private const val TAG = "FillRespFactory"

    fun createSingleTiligResponse(
        context: Context,
        fillRequest: FillRequest,
        parser: StructureParser
    ): FillResponse {

        val responseBuilder = FillResponse.Builder()

        // Show a single entry
        val datasetAdd = createNoMatchesDataset(
            context,
            parser,
            fillRequest,
            title = R.string.autofill_tilig,
            icon = R.drawable.ic_squirrel
        )
        responseBuilder.addDataset(datasetAdd)
        addSafeInfo(
            context,
            parser.getWebsite(),
            parser.getStructPackageName(),
            parser.autofillFields,
            responseBuilder
        )

        // One more dataset that is a sidebar icon
        if (prefs.devToggleSideIconAutofill
            && prefs.devToggleEnableInlineAutofill
        ) {
            val datasetLogo = createLogoDataset(
                context,
                parser,
                fillRequest
            )
            responseBuilder.addDataset(datasetLogo)
        }

        responseBuilder.setIgnoredIds(*parser.ignoreIds)
        return responseBuilder.build()
    }

    /**
     * Creates a FillResponse that contains no datasets. The response will have an authentication
     * intent that opens the MainActivity in SEARCH mode (after login, the user can search and select the right account).
     * The MainActivity can return a new FillResponse in the result Intent, with
     * extra [android.view.autofill.AutofillManager.EXTRA_AUTHENTICATION_RESULT] to replace the current response.
     */
    fun createAuthResponse(
        context: Context,
        fillRequest: FillRequest,
        parser: StructureParser
    ): FillResponse {

        val responseBuilder = FillResponse.Builder()
        val remoteView = RemoteViewHelper.newRemoteViews(
            context,
            R.string.autofill_sign_in,
            null,
            R.mipmap.ic_launcher_foreground
        )

        // Ignore irrelevant IDs
        responseBuilder.setIgnoredIds(*parser.ignoreIds)

        val packageName = parser.getStructPackageName()
        val intent = createSearchIntent(parser, context, fillRequest, packageName)
        responseBuilder.setAuthentication(
            parser.autofillFields.autofillIds.toTypedArray(),
            intent.intentSender,
            remoteView
        )
        responseBuilder.setIgnoredIds(*parser.ignoreIds)
        // Note: setFooter is not allowed when the response has called.
        // setAuthentication. According to the docs: "this method should only
        // be used on FillResponses that do not require authentication
        // (as the footer could have been set directly in the main presentation
        // in these cases)".
        /* if (VersionUtil.supportsFootersInAutofill()) {
            responseBuilder.setFooter(RemoteViewHelper.newRemoteFooter(context))
        } */
        return responseBuilder.build()
    }

    /**
     * Creates a FillResponse that contains no datasets. The response will have an authentication
     * intent that opens the LockActivity.
     * The LockActivity can return a new FillResponse in the result Intent, with
     * extra [android.view.autofill.AutofillManager.EXTRA_AUTHENTICATION_RESULT] to replace the current response.
     */
    /*
    fun createLockResponse(
        context: Context,
        fillRequest: FillRequest,
        parser: StructureParser
    ): FillResponse {

        val responseBuilder = FillResponse.Builder()
        val remoteView = RemoteViewHelper.newRemoteViews(
            context, R.string.autofill_unlock,
            null,
            R.drawable.ic_lock
        )

        // Ignore irrelevant IDs
        responseBuilder.setIgnoredIds(*parser.ignoreIds)

        val sender = LockActivity.createPendingIntent(
            context,
            fillRequest,
            null
        ).intentSender
        responseBuilder.setAuthentication(
            parser.autofillFields.autofillIds.toTypedArray(),
            sender,
            remoteView
        )
        // Note: setFooter is not allowed when the response has called
        // setAuthentication. According to the docs: "this method should only
        // be used on FillResponses that do not require authentication
        // (as the footer could have been set directly in the main presentation
        // in these cases)".
//        if (VersionUtil.supportsFootersInAutofill()) {
//            responseBuilder.setFooter(RemoteViewHelper.newRemoteFooter(context))
//        }
        return responseBuilder.build()
    } */

    /**
     * If the user is authenticated but has no matching accounts, the FillResponse created
     * by this method can open the app in search-mode, so the user can select an account to autofill
     * with.
     */
    fun createSearchDataset(
        context: Context,
        parser: StructureParser,
        fillRequest: FillRequest
    ): Dataset {

        val packageName = parser.getStructPackageName()

        val pendingIntent = createSearchIntent(parser, context, fillRequest, packageName)

        val rv = RemoteViewHelper.newRemoteViews(
            context,
            R.string.label_search,
            null,
            R.drawable.ic_search_autofill
        )
        val datasetBuilder = Dataset.Builder(rv).apply {
            // At least one value must be set
            parser.autofillFields.autofillIds.forEach {
                setValue(it, AutofillValue.forText("PLACEHOLDER3"))
            }
            setAuthentication(pendingIntent.intentSender)

            // Support inline?
            if (InlineFillRequestHelper.shouldShowSearch(fillRequest)
                && VersionUtil.supportsInlineAutofill()
                && prefs.devToggleEnableInlineAutofill
            ) {
                fillRequest.inlineSuggestionsRequest?.inlinePresentationSpecs?.get(0)?.let { spec ->
                    val rvInline = InlinePresentationHelper.newInlineViews(
                        context,
                        spec,
                        pendingIntent,
                        R.string.label_search,
                        null,
                        R.drawable.ic_search
                    )
                    rvInline?.let {
                        setInlinePresentation(it)
                    }
                }
            }
        }
        return datasetBuilder.build()
    }

    private fun createSearchIntent(
        parser: StructureParser,
        context: Context,
        fillRequest: FillRequest,
        packageName: String?
    ) = if (parser.isWebsite) {
        MainActivity.createPendingIntentForSearch(
            context,
            fillRequest,
            applicationId = null,
            url = parser.getWebsite(),
            login = parser.getWebsite()
        )
    } else {
        MainActivity.createPendingIntentForSearch(
            context,
            fillRequest,
            applicationId = packageName,
            url = null,
            login = PackageVerifier.getAppName(context, packageName)
        )
    }

    /**
     * This method creates 'normal' FillResponse with a list of accounts that match the target app
     */
    fun createResponse(
        context: Context,
        auth: AuthLevel,
        fillRequest: FillRequest,
        parser: StructureParser,
        accounts: ArrayList<Account>?,
        crypto: Crypto
    ): FillResponse.Builder {

        // Add all matching accounts to the dataset
        val responseBuilder = FillResponse.Builder()
        accounts?.forEach {
            // Add
            createDataset(
                context,
                fillRequest,
                parser.autofillFields,
                it,
                accounts,
                auth,
                crypto
            )?.let(responseBuilder::addDataset)
        }

        when (auth) {
            /* AuthLevel.AUTHENTICATED_LOCKED -> {
                // FIXME remove this? Is it used? Will the AUTHENTICATED_LOCKED functionality ever return?
                responseBuilder.setClientState(
                    AbstractAutofillActivity.getBundle(
                        fillRequest = fillRequest,
                        matchingAccounts = accounts
                    )
                )
            } */
            AuthLevel.AUTHENTICATED_UNLOCKED -> {
                // Decrypt details before we can access the username and password
                val account = accounts?.firstOrNull()
                account?.decryptDetails(repo.jsonConverter, crypto)

                responseBuilder.setClientState(
                    bundleOf(
                        TiligService.CLIENT_STATE_ACCOUNT
                                to account,
                        TiligService.CLIENT_STATE_ACCOUNT_ID_STR
                                to account?.id,
                        TiligService.CLIENT_STATE_USERNAME_STR
                                to account?.username,
                        TiligService.CLIENT_STATE_PASSWORD_STR
                                to account?.password
                    )
                )
                addSafeInfo(
                    context,
                    parser.getWebsite(),
                    parser.getStructPackageName(),
                    parser.autofillFields,
                    responseBuilder
                )
            }
            else -> {
            }
        }

        // Ignore irrelevant IDs
        responseBuilder.setIgnoredIds(*parser.ignoreIds)
        return responseBuilder
    }

    private fun addSafeInfo(
        context: Context,
        website: String?,
        androidAppId: String?,
        autofillFieldMetadata: AutofillFieldMetadataCollection,
        responseBuilder: FillResponse.Builder
    ) {
        if (AutoSaveHelper.hasSaveInfo(autofillFieldMetadata)) {
            responseBuilder.setSaveInfo(
                AutoSaveHelper.createSaveInfo(
                    context,
                    prefs,
                    website,
                    androidAppId,
                    autofillFieldMetadata
                )
            )
        } else {
            Log.v(TAG, "These fields are not meant to be saved by autofill. Save type: ${autofillFieldMetadata.saveType}")
        }
    }

    /**
     * Wraps autofill data in a [Dataset] object which can then be sent back to the
     * client View.
     *
     * @param context The service context.
     * @param fillRequest The original fill request
     * @param autofillFieldMetadata Meta data
     * @param account The account to create the dataset for
     * @param accounts The complete list of matching accounts. Used to update the response if the app is locked and shows locked accounts at first
     * @param auth How locked the app is. User signed in? App locked? Or neither?
     */
    fun createDataset(
        context: Context,
        fillRequest: FillRequest,
        autofillFieldMetadata: AutofillFieldMetadataCollection,
        account: Account,
        accounts: ArrayList<Account>,
        auth: AuthLevel,
        crypto: Crypto
    ): Dataset? {

        var datasetBuilder: Dataset.Builder? = null
        account.name.let { loginName ->
            when (auth) {
                AuthLevel.NOT_AUTHENTICATED -> {
                    // no-op; this situation is impossible. If the user is not authenticated,
                    // [createAuthResponse] is called instead.
                    // datasetBuilder = createAuthDatasetBuilder(context, fillRequest, parser)
                }
                /* AuthLevel.AUTHENTICATED_LOCKED -> {
                    datasetBuilder = Dataset.Builder(
                        RemoteViewHelper.newRemoteViews(
                            context,
                            loginName,
                            account.decryptedUsername(crypto),
                            R.drawable.ic_lock,
                            IconUrlUtil.getIconUrl(account)
                        )
                    ).apply {
                        val sender = LockActivity.createPendingIntent(
                            context,
                            fillRequest = fillRequest,
                            matchingAccounts = accounts
                        ).intentSender

                        setAuthentication(sender)
                    }
                } */
                AuthLevel.AUTHENTICATED_UNLOCKED -> {
                    datasetBuilder = Dataset.Builder(
                        RemoteViewHelper.newRemoteViews(
                            context,
                            loginName!!,
                            account.username,
                            R.drawable.ic_username,
                            IconUrlUtil.getIconUrl(account)
                        )
                    ).apply {
                        // Saving the account ID for later. We use this on the onSaveRequest to determine if the account is new or not
                        this.setId(account.id)
                        // Support inline?
                        if (InlineFillRequestHelper.shouldShowAdd(fillRequest)
                            && VersionUtil.supportsInlineAutofill()
                            && prefs.devToggleEnableInlineAutofill
                        ) {
                            fillRequest.inlineSuggestionsRequest?.inlinePresentationSpecs?.get(0)
                                ?.let { spec ->
                                    val rvInline = InlinePresentationHelper.newInlineViews(
                                        context,
                                        spec,
                                        loginName,
                                        "",
                                        R.drawable.ic_squirrel
                                    )
                                    rvInline?.let {
                                        setInlinePresentation(it)
                                    }
                                }
                        }
                    }
                }
            }
        }

        datasetBuilder?.let {
            val setValueAtLeastOnce = applyToFields(account, autofillFieldMetadata, it, crypto)
            if (setValueAtLeastOnce) {
                return it.build()
            }
        }
        return null
    }

    /**
     * Tells a [Dataset.Builder] how it should apply the dataset to a form field.
     * That is, which attribute of the [Account] object should be put in an
     * [android.widget.EditText] with the hint 'password'
     * and which attribute goes into the one with the hint 'username' or 'email'.
     */
    private fun applyToFields(
        account: Account,
        autofillFieldMetadata: AutofillFieldMetadataCollection,
        datasetBuilder: Dataset.Builder,
        crypto: Crypto
    ): Boolean {
        var setValueAtLeastOnce = false
        for (hint in autofillFieldMetadata.allAutofillHints) {
            val autofillFields = autofillFieldMetadata.getFieldsForHint(hint) ?: continue
            for (autofillField in autofillFields) {
                when (autofillField.autofillType) {
                    View.AUTOFILL_TYPE_LIST -> {
                    }
                    View.AUTOFILL_TYPE_DATE -> {
                    }
                    View.AUTOFILL_TYPE_TEXT -> {
                        val fieldValue = account.fieldValueForHint(hint, repo.jsonConverter, crypto)
                        datasetBuilder.setValue(
                            autofillField.autofillId,
                            AutofillValue.forText(fieldValue)
                        )
                        datasetBuilder.setId(account.id)
                        setValueAtLeastOnce = true
                    }
                    View.AUTOFILL_TYPE_TOGGLE -> {
                    }
                    else -> Log.w(
                        TAG,
                        "Invalid autofill type - ${autofillField.autofillType}"
                    )
                }
            }
        }
        return setValueAtLeastOnce
    }

    /**
     * Creates a [FillResponse] that contains one item: a dataset that offers to create
     * a new account in the app.
     * Note: the app should return a new [FillResponse] in the return intent extra
     * [android.view.autofill.AutofillManager.EXTRA_AUTHENTICATION_RESULT]  to replace
     * this FillResponse with a new one that actually autofills.
     */
    @RequiresApi(Build.VERSION_CODES.O)
    fun createNoMatchesResponse(
        context: Context,
        parser: StructureParser,
        fillRequest: FillRequest
    ): FillResponse {

        val responseBuilder = FillResponse.Builder()

        // Add a line to search
        if (prefs.devToggleShowSearchInAutofill) {
            val datasetSearch = createSearchDataset(
                context,
                parser,
                fillRequest
            )
            responseBuilder.addDataset(datasetSearch)
        }
        // Add one more line to add an account
        if (prefs.devToggleShowAddNewInAutofill) {
            val datasetAdd = createNoMatchesDataset(
                context,
                parser,
                fillRequest
            )
            responseBuilder.addDataset(datasetAdd)
        }

        // And one more dataset that is a sidebar icon
        if (prefs.devToggleSideIconAutofill
            && prefs.devToggleEnableInlineAutofill
        ) {
            val datasetLogo = createLogoDataset(
                context,
                parser,
                fillRequest
            )
            responseBuilder.addDataset(datasetLogo)
        }

        return responseBuilder.build()
    }

    /**
     * Creates a dataset that offers to create a new account in the app.
     * Note: the app should return a new [FillResponse] in the return intent extra
     * [android.view.autofill.AutofillManager.EXTRA_AUTHENTICATION_RESULT]  to replace
     * this FillResponse with a new one that actually autofills.
     */
    @RequiresApi(Build.VERSION_CODES.O)
    fun createNoMatchesDataset(
        context: Context,
        parser: StructureParser,
        fillRequest: FillRequest,
        @StringRes title: Int = R.string.create_new_login,
        @DrawableRes icon: Int = R.drawable.ic_add_black
    ): Dataset {

        // Don't set applicationId if we're autofilling a browser, and don't set url if we're
        // autofilling an app
        val pendingIntent = if (parser.isWebsite) {
            AutofillActivity.createPendingIntentForCreatingNew(
                context,
                fillRequest,
                applicationId = null,
                url = parser.getWebsite(),
                login = parser.getWebsite()?.getDomain()?.removePredefinedPrefixesFromDomain(),
                password = parser.findValueForHint(View.AUTOFILL_HINT_PASSWORD)
                    ?: parser.findValueForHint("newPassword"),
                username = parser.findValueForHint(View.AUTOFILL_HINT_USERNAME)
                    ?: parser.findValueForHint("newUsername")
                    ?: parser.findValueForHint(View.AUTOFILL_HINT_EMAIL_ADDRESS)
            )
        } else {
            AutofillActivity.createPendingIntentForCreatingNew(
                context,
                fillRequest,
                applicationId = parser.getStructPackageName(),
                url = null,
                login = PackageVerifier.getAppName(context, parser.getStructPackageName()),
                password = parser.findValueForHint(View.AUTOFILL_HINT_PASSWORD)
                    ?: parser.findValueForHint("newPassword"),
                username = parser.findValueForHint(View.AUTOFILL_HINT_USERNAME)
                    ?: parser.findValueForHint("newUsername")
                    ?: parser.findValueForHint(View.AUTOFILL_HINT_EMAIL_ADDRESS)
            )
        }

        val rv = RemoteViewHelper.newRemoteViews(
            context,
            title,
            null,
            icon
        )
        val datasetBuilder = Dataset.Builder(rv).apply {
            // At least one value must be set
            parser.autofillFields.autofillIds.forEach {
                setValue(it, AutofillValue.forText("PLACEHOLDER"))
            }
            setAuthentication(pendingIntent.intentSender)

            // Support inline?
            if (InlineFillRequestHelper.shouldShowAdd(fillRequest)
                && VersionUtil.supportsInlineAutofill()
                && prefs.devToggleEnableInlineAutofill
            ) {
                fillRequest.inlineSuggestionsRequest?.inlinePresentationSpecs?.get(0)?.let { spec ->
                    val rvInline = InlinePresentationHelper.newInlineViews(
                        context,
                        spec,
                        pendingIntent,
                        title,
                        null,
                        icon
                    )
                    rvInline?.let {
                        setInlinePresentation(it)
                    }
                }
            }
        }
        return datasetBuilder.build()
    }

    /**
     * Creates a dataset that offers to create a new account in the app.
     * Note: the app should return a new [FillResponse] in the return intent extra
     * [android.view.autofill.AutofillManager.EXTRA_AUTHENTICATION_RESULT]  to replace
     * this FillResponse with a new one that actually autofills.
     */
    @RequiresApi(Build.VERSION_CODES.O)
    fun createLogoDataset(
        context: Context,
        parser: StructureParser,
        fillRequest: FillRequest
    ): Dataset {

        // Don't set applicationId if we're autofilling a browser, and don't set url if we're
        // autofilling an app
        val pendingIntent = if (parser.isWebsite) {
            AutofillActivity.createPendingIntentForCreatingNew(
                context,
                fillRequest,
                applicationId = null,
                url = parser.getWebsite(),
                login = parser.getWebsite()?.getDomain()?.removePredefinedPrefixesFromDomain(),
                password = parser.findValueForHint(View.AUTOFILL_HINT_PASSWORD)
                    ?: parser.findValueForHint("newPassword"),
                username = parser.findValueForHint(View.AUTOFILL_HINT_USERNAME)
                    ?: parser.findValueForHint("newUsername")
                    ?: parser.findValueForHint(View.AUTOFILL_HINT_EMAIL_ADDRESS)
            )
        } else {
            AutofillActivity.createPendingIntentForCreatingNew(
                context,
                fillRequest,
                applicationId = parser.getStructPackageName(),
                url = null,
                login = PackageVerifier.getAppName(context, parser.getStructPackageName()),
                password = parser.findValueForHint(View.AUTOFILL_HINT_PASSWORD)
                    ?: parser.findValueForHint("newPassword"),
                username = parser.findValueForHint(View.AUTOFILL_HINT_USERNAME)
                    ?: parser.findValueForHint("newUsername")
                    ?: parser.findValueForHint(View.AUTOFILL_HINT_EMAIL_ADDRESS)
            )
        }

        val rv = RemoteViewHelper.newRemoteViews(
            context,
            R.string.autofill_tilig,
            null,
            R.drawable.ic_acorn
        )
        val datasetBuilder = Dataset.Builder(rv).apply {
            // At least one value must be set
            parser.autofillFields.autofillIds.forEach {
                setValue(it, AutofillValue.forText("PLACEHOLDER"))
            }
            setAuthentication(pendingIntent.intentSender)

            // Support inline?
            if (VersionUtil.supportsInlineAutofill()
                && prefs.devToggleEnableInlineAutofill
            ) {
                fillRequest.inlineSuggestionsRequest?.inlinePresentationSpecs?.get(0)?.let { spec ->
                    val rvInline = InlinePresentationHelper.newLogo(
                        context,
                        spec,
                        pendingIntent
                    )
                    rvInline?.let {
                        setInlinePresentation(it)
                    }
                }
            }
        }
        return datasetBuilder.build()
    }
}
