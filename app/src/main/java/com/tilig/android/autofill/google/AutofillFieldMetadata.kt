/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tilig.android.autofill.google

import android.app.assist.AssistStructure.ViewNode
import android.os.Build
import android.service.autofill.SaveInfo
import android.util.Log
import android.view.View
import android.view.autofill.AutofillId
import androidx.annotation.RequiresApi
import androidx.autofill.HintConstants
import androidx.autofill.HintConstants.AUTOFILL_HINT_SMS_OTP
import com.tilig.android.autofill.TiligService

/**
 * A stripped down version of a [ViewNode] that contains only autofill-relevant metadata. It also
 * contains a `saveType` flag that is calculated based on the [ViewNode]'s autofill hints.
 */
@RequiresApi(Build.VERSION_CODES.O)
class AutofillFieldMetadata(view: ViewNode, val autofillHints: Array<String>, val ignoreForAutosave: Boolean = false) {

    val saveType = getSaveTypeFromHints(autofillHints)

    val autofillId: AutofillId = view.autofillId!!
    val autofillType: Int = view.autofillType
    val isFocused: Boolean = view.isFocused

    private val autofillOptions: Array<CharSequence>? = view.autofillOptions

    companion object {
        private val CC_FIELDS = arrayOf(
            HintConstants.AUTOFILL_HINT_CREDIT_CARD_EXPIRATION_DATE,
            HintConstants.AUTOFILL_HINT_CREDIT_CARD_EXPIRATION_DAY,
            HintConstants.AUTOFILL_HINT_CREDIT_CARD_EXPIRATION_MONTH,
            HintConstants.AUTOFILL_HINT_CREDIT_CARD_EXPIRATION_YEAR,
            HintConstants.AUTOFILL_HINT_CREDIT_CARD_NUMBER,
            HintConstants.AUTOFILL_HINT_CREDIT_CARD_SECURITY_CODE
        )
        private val ADDRESS_FIELDS = arrayOf(
            View.AUTOFILL_HINT_POSTAL_ADDRESS,
            View.AUTOFILL_HINT_POSTAL_CODE,
            HintConstants.AUTOFILL_HINT_POSTAL_ADDRESS_EXTENDED_ADDRESS,
            HintConstants.AUTOFILL_HINT_POSTAL_ADDRESS_STREET_ADDRESS,
            HintConstants.AUTOFILL_HINT_POSTAL_ADDRESS_COUNTRY,
            HintConstants.AUTOFILL_HINT_POSTAL_ADDRESS_EXTENDED_POSTAL_CODE,
            HintConstants.AUTOFILL_HINT_POSTAL_ADDRESS_LOCALITY,
            HintConstants.AUTOFILL_HINT_POSTAL_ADDRESS_REGION,
        )
        private val PHONE_FIELDS = arrayOf(
            View.AUTOFILL_HINT_PHONE,
            HintConstants.AUTOFILL_HINT_PHONE_COUNTRY_CODE,
            HintConstants.AUTOFILL_HINT_PHONE_NATIONAL,
            HintConstants.AUTOFILL_HINT_PHONE_NUMBER,
            HintConstants.AUTOFILL_HINT_PHONE_NUMBER_DEVICE,
        )
        private val OTP_FIELDS = arrayOf(
            AUTOFILL_HINT_SMS_OTP
        )
        private val DEFAULT_FIELDS = arrayOf(
            View.AUTOFILL_HINT_EMAIL_ADDRESS,
            View.AUTOFILL_HINT_NAME,
            View.AUTOFILL_HINT_PASSWORD,
            View.AUTOFILL_HINT_USERNAME,
            HintConstants.AUTOFILL_HINT_NEW_USERNAME,
            HintConstants.AUTOFILL_HINT_NEW_PASSWORD,
        )

        private const val CC_SUPPORT_ENABLED = false
        private const val ADDRESS_SUPPORT_ENABLED = false
        private const val PHONE_SUPPORT_ENABLED = false
        private const val OTP_SUPPORT_ENABLED = true

        fun isValidHint(hint: String?): Boolean {
            if (CC_SUPPORT_ENABLED && CC_FIELDS.contains(hint)) {
                return true
            }
            if (ADDRESS_SUPPORT_ENABLED && ADDRESS_FIELDS.contains(hint)) {
                return true
            }
            if (PHONE_SUPPORT_ENABLED && PHONE_FIELDS.contains(hint)) {
                return true
            }
            if (OTP_SUPPORT_ENABLED && OTP_FIELDS.contains(hint)) {
                return true
            }
            return DEFAULT_FIELDS.contains(hint)
        }

        private fun getSaveTypeFromHints(autofillHints: Array<String>): Int {
            var saveType = 0
            for (hint in autofillHints) {
                when (hint) {
                    View.AUTOFILL_HINT_CREDIT_CARD_EXPIRATION_DATE,
                    View.AUTOFILL_HINT_CREDIT_CARD_EXPIRATION_DAY,
                    View.AUTOFILL_HINT_CREDIT_CARD_EXPIRATION_MONTH,
                    View.AUTOFILL_HINT_CREDIT_CARD_EXPIRATION_YEAR,
                    View.AUTOFILL_HINT_CREDIT_CARD_NUMBER,
                    View.AUTOFILL_HINT_CREDIT_CARD_SECURITY_CODE -> {
                        if (CC_SUPPORT_ENABLED) {
                            saveType = saveType or SaveInfo.SAVE_DATA_TYPE_CREDIT_CARD
                        }
                    }
                    View.AUTOFILL_HINT_EMAIL_ADDRESS -> {
                        saveType = saveType or SaveInfo.SAVE_DATA_TYPE_EMAIL_ADDRESS
                    }
                    View.AUTOFILL_HINT_PHONE,
                    HintConstants.AUTOFILL_HINT_PHONE_COUNTRY_CODE,
                    HintConstants.AUTOFILL_HINT_PHONE_NATIONAL,
                    HintConstants.AUTOFILL_HINT_PHONE_NUMBER,
                    HintConstants.AUTOFILL_HINT_PHONE_NUMBER_DEVICE -> {
                        if (PHONE_SUPPORT_ENABLED) {
                            saveType = saveType or SaveInfo.SAVE_DATA_TYPE_GENERIC
                        }
                    }
                    View.AUTOFILL_HINT_NAME -> {
                        saveType = saveType or SaveInfo.SAVE_DATA_TYPE_GENERIC
                    }
                    View.AUTOFILL_HINT_PASSWORD,
                    HintConstants.AUTOFILL_HINT_NEW_PASSWORD -> {
                        saveType = saveType or SaveInfo.SAVE_DATA_TYPE_PASSWORD
                        saveType = saveType and SaveInfo.SAVE_DATA_TYPE_EMAIL_ADDRESS.inv()
                        saveType = saveType and SaveInfo.SAVE_DATA_TYPE_USERNAME.inv()
                    }
                    View.AUTOFILL_HINT_POSTAL_ADDRESS,
                    View.AUTOFILL_HINT_POSTAL_CODE,
                    HintConstants.AUTOFILL_HINT_POSTAL_ADDRESS_COUNTRY,
                    HintConstants.AUTOFILL_HINT_POSTAL_ADDRESS_LOCALITY,
                    HintConstants.AUTOFILL_HINT_POSTAL_ADDRESS_EXTENDED_ADDRESS,
                    HintConstants.AUTOFILL_HINT_POSTAL_ADDRESS_REGION,
                    HintConstants.AUTOFILL_HINT_POSTAL_ADDRESS_EXTENDED_POSTAL_CODE,
                    HintConstants.AUTOFILL_HINT_POSTAL_ADDRESS_STREET_ADDRESS -> {
                        if (ADDRESS_SUPPORT_ENABLED) {
                            saveType = saveType or SaveInfo.SAVE_DATA_TYPE_ADDRESS
                        }
                    }
                    View.AUTOFILL_HINT_USERNAME,
                    HintConstants.AUTOFILL_HINT_NEW_USERNAME -> {
                        saveType = saveType or SaveInfo.SAVE_DATA_TYPE_USERNAME
                    }
                }
            }
            Log.v("AutoSaveHelper", " Savetype from hints is $saveType")
            return saveType
        }
    }

    init {
        if (TiligService.DEBUG_LOG_ALL_FIELDS) {
            Log.v(
                "Autofill",
                " Adding detected field inputType=${view.inputType}, htmlInfo=${view.htmlInfo}, hints=${autofillHints.joinToString(",")}"
            )
        }
    }

    /**
     * When the [ViewNode] is a list that the user needs to choose a string from (i.e. a spinner),
     * this is called to return the index of a specific item in the list.
     */
    fun getAutofillOptionIndex(value: CharSequence): Int = autofillOptions?.indexOf(value) ?: -1

}
