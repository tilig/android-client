package com.tilig.android.autofill

import RelatedDomainsHelper
import Relation
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.tilig.android.BuildConfig
import com.tilig.android.autofill.google.StructureParser
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.utils.getDomain
import com.tilig.android.utils.removePredefinedPrefixesFromDomain
import com.tilig.android.utils.toArrayList

/**
 * Static helper methods for matching accounts to apps
 */
object AccountMatcher {

    private const val DEBUG_ALWAYS_MATCH_ALL_ACCOUNTS = false
    private const val DEBUG_NEVER_MATCH_ANY_ACCOUNTS = false

    const val PERFECT_MATCH = 1f
    const val NO_MATCH = 0f
    const val SAME_SUBDOMAIN_SCORE = .8f
    const val SAME_MAIN_DOMAIN_SCORE = .7f
    const val RELATED_DOMAIN_SCORE = .6f
    const val RELATED_APP_SCORE = .6f

    /**
     * Checks if the given account is a good suggestion for autofilling in the given app.
     * @param account The Account for which to check if it matches the app
     * @param parser The parser that parsed the target app. Helpful for finding the package name, website, etc.
     * @return A matching value from 0f to 1f, where 0f means the app is no match and should not be included, and 1f is a perfect match.
     */
    @RequiresApi(Build.VERSION_CODES.O)
    fun accountMatchesContexts(
        account: Account,
        parser: StructureParser,
        relations: Relation?
    ): Float {
        return accountMatchesUrlOrApp(
            accountFullUrl = account.website,
            accountHost = account.website?.getDomain(),
            accountMainDomain = account.website?.removePredefinedPrefixesFromDomain(),
            accountAppId = account.androidAppId,
            targetFullUrl = parser.getWebsite(),
            targetHost = parser.getWebsite()?.getDomain(),
            targetMainDomain = parser.getWebsite()?.removePredefinedPrefixesFromDomain(),
            targetAppId = parser.getStructPackageName(),
            targetIsWebsite = parser.isWebsite,
            targetRelations = relations
        )
    }

    private fun accountMatchesUrlOrApp(
        accountFullUrl: String?,
        accountHost: String?,
        accountMainDomain: String?,
        accountAppId: String?,
        targetFullUrl: String?,
        targetHost: String?,
        targetMainDomain: String?,
        targetAppId: String?,
        targetIsWebsite: Boolean,
        targetRelations: Relation?
    ): Float {

        // We use a ranking system instead of binary matching, to make sure we can match:
        // - similar domains but not quite (subdomains, e.g.)
        // - rank high: a login to a website in Chrome, rank lower the login for Chrome itself
        // - match app ID's to websites
        // - match higher when path and query params match as well, rank lower if only domain matches
        return when {

            BuildConfig.DEBUG && DEBUG_NEVER_MATCH_ANY_ACCOUNTS -> NO_MATCH

            BuildConfig.DEBUG && DEBUG_ALWAYS_MATCH_ALL_ACCOUNTS -> PERFECT_MATCH

            // Match by app id
            !targetIsWebsite && accountAppId == targetAppId -> {
                Log.v("AccountMatcher", "App matched on android_app_id: $targetAppId")
                PERFECT_MATCH
            }

            // Check if the URL is a perfect match
            targetIsWebsite && targetFullUrl == accountFullUrl -> PERFECT_MATCH

            // Check if the web domains match
            targetIsWebsite && targetHost != null && targetHost == accountHost -> {
                Log.v(
                    "AccountMatcher",
                    "Website matched on (sub)domain: $targetHost == $accountHost"
                )
                SAME_SUBDOMAIN_SCORE
            }

            targetIsWebsite && targetMainDomain != null && targetMainDomain == accountMainDomain -> {
                Log.v(
                    "AccountMatcher",
                    "Website matched on main domain: $targetMainDomain ends with $accountMainDomain"
                )
                SAME_MAIN_DOMAIN_SCORE
            }
//                    else -> {
//                        Log.v("AccountMatcher", "No match for $targetHost, $accountHost, $targetAppId")
//                        NO_MATCH
//                    }
            targetRelations?.matchesApp(accountAppId) == true -> {
                Log.v(
                    "AccountMatcher",
                    "App matched on related android_app_id: $targetMainDomain or $targetAppId is related to ${accountAppId}"
                )
                RELATED_APP_SCORE
            }
            targetRelations?.matchesDomain(accountMainDomain) == true -> {
                Log.v(
                    "AccountMatcher",
                    "Website matched on related domain: $targetMainDomain or $targetAppId is related to $accountMainDomain"
                )
                RELATED_DOMAIN_SCORE
            }
            else -> {
                Log.v(
                    "AccountMatcher",
                    "No match for $targetHost ($targetMainDomain), $targetAppId. Relations: ${targetRelations.toString()}"
                )
                NO_MATCH
            }
        }
    }

    /**
     * Filters the given list of accounts and sorts them by best match in descending order
     */
    @RequiresApi(Build.VERSION_CODES.O)
    fun filterAccounts(accounts: List<Account>, parser: StructureParser): ArrayList<Account> {
        val targetFullUrl = parser.getWebsite()
        val targetHost = targetFullUrl?.getDomain()
        val targetMainDomain = targetFullUrl?.removePredefinedPrefixesFromDomain()
        val targetAppId = parser.getStructPackageName()
        var targetRelations: Relation? = null

        if (parser.isWebsite) {
            targetMainDomain?.let {
                targetRelations = RelatedDomainsHelper.lookupDomain(it)
            }
        } else {
            targetAppId?.let {
                targetRelations = RelatedDomainsHelper.lookupApp(it)
            }
        }

        return accounts
            .map { account ->
                account.matchingScore = accountMatchesUrlOrApp(
                    accountFullUrl = account.website,
                    accountHost = account.website?.getDomain(),
                    accountMainDomain = account.website?.removePredefinedPrefixesFromDomain(),
                    accountAppId = account.androidAppId,
                    targetFullUrl = targetFullUrl,
                    targetHost = targetHost,
                    targetMainDomain = targetMainDomain,
                    targetAppId = targetAppId,
                    targetIsWebsite = parser.isWebsite,
                    targetRelations = targetRelations
                )
                account
            }
            .filter {
                it.matchingScore > NO_MATCH
            }
            .sortedByDescending {
                it.matchingScore
            }
            .toArrayList()
    }

}
