package com.tilig.android.analytics

import android.util.Log
import com.tilig.android.utils.SharedPrefs
import io.sentry.Sentry
import io.sentry.protocol.User


/**
 * Enriches the crashreports with breadcrumbs
 */
object Breadcrumb {

    const val SignInOnboarding = "signin_onboarding"
    const val MainScreen = "main"
    const val Detail = "detail"
    const val AddAccount = "add_account"
    const val SearchAccount = "search_account"
    const val AddSuggestedAccount = "add_suggested_account"
    const val Settings = "settings"

    const val LoggingOut = "logging_out"
    const val CryptoTrail_ReturnToAutofill = "crypto.returnToAutofill"
    const val CryptoTrail_LoggedOut = "crypto.isLoggedOut"
    const val CryptoTrail_LoggedIn = "crypto.isLoggedIn"
    const val CryptoTrail_ClearPrefs = "crypto.clear_prefs"
    const val CryptoTrail_PostLogin_BothKeysEmpty = "crypto.post_login.both_keys_empty"
    const val CryptoTrail_PostLogin_PrivKeyEmpty = "crypto.post_login.priv_key_empty"
    const val CryptoTrail_PostLogin_PubKeyEmpty = "crypto.post_login.pub_key_empty"
    const val CryptoTrail_PostLogin_KeysOk = "crypto.post_login.keys_ok"
    const val CryptoTrail_PostLogin_NewKeysArePresent = "crypto.post_login.new_keys_are_present"

    const val CryptoTrail_Setting_Priv_To_Null = "crypto.priv_set.empty"
    const val CryptoTrail_Setting_Priv_To_Something = "crypto.priv_set.has_value"
    const val CryptoTrail_Setting_Pub_To_Null = "crypto.pub_set.empty"
    const val CryptoTrail_Setting_Pub_To_Something = "crypto.pub_set.has_value"

    const val CryptoTrail_GoogleResultFailure = "crypto.loginflow.google_result_failure"
    const val CryptoTrail_CreatingKeypair = "crypto.loginflow.creating_keypair"
    const val CryptoTrail_FirestoreError_CreatingKeypair = "crypto.loginflow.creating_keypair.firestore_err"
    const val CryptoTrail_FirestoreError_FetchingKey = "crypto.loginflow.creating_keypair.firestore_connection_err"
    const val CryptoTrail_FirestoreSuccess_CreatingKeypair = "crypto.loginflow.creating_keypair.firestore_ok"
    const val CryptoTrail_CannotFetchPublicKey = "crypto.loginflow.fetch_pub_failure"
    const val CryptoTrail_FetchingPublicKey = "crypto.loginflow.fetching_pub"
    const val CryptoTrail_FetchingPrivateKey_Both = "crypto.loginflow.fetching_priv.both"
    const val CryptoTrail_FetchingPrivateKey_NewOnly = "crypto.loginflow.fetching_priv.new"
    const val CryptoTrail_ProfileResponse_NewKeysPresent = "crypto.profile_response.has_new_keys"
    const val CryptoTrail_ProfileResponse_NoKeysPresent = "crypto.profile_response.has_no_keys"
    const val CryptoTrail_ProfileResponse_OnlyLegacyKeysPresent = "crypto.profile_response.only_legacy_keys"
    const val CryptoTrail_HomeScreen_RequiresRefresh = "crypto.homescreen.requires_refresh"
    const val CryptoTrail_LoginFailure_1 = "crypto.loginflow.login_fail_1"
    const val CryptoTrail_LoginFailure_2 = "crypto.loginflow.login_fail_2"

    const val AutofillTrail_FillRequest_Null = "autofill.fillrequest_null"
    const val AutofillTrail_FillRequest_Not_Null = "autofill.fillrequest_not_null"
    const val AutofillTrail_Parser_Null = "autofill.parser_null"
    const val AutofillTrail_Parser_Not_Null = "autofill.parser_not_null"

    const val DeleteAccountTrail_FirebaseSuccess = "deleteaccount.firebase_success"
    const val DeleteAccountTrail_FirebaseFailure = "deleteaccount.firebase_failure"
    const val DeleteAccountTrail_BackendSuccess = "deleteaccount.backend_success"
    const val DeleteAccountTrail_BackendFailure = "deleteaccount.backend_failure"

    fun drop(crumb: String) {
        Log.i("Breadcrumb", crumb)
        Sentry.addBreadcrumb(crumb)
    }

    fun identify(prefs: SharedPrefs) {
        if (!prefs.email.isNullOrEmpty()) {
            Sentry.setUser(User().apply {
                if (!prefs.mixpanelUserId.isNullOrEmpty()) {
                    id = prefs.mixpanelUserId
                }
                email = prefs.email
            })
        }
    }
}
