package com.tilig.android.analytics

import com.tilig.android.utils.SharedPrefs
import kotlin.random.Random

// Mixpanel experiments
private const val EXPERIMENT_SURVEY_ENABLED = "androidSurveyEnabled"
// Survey disabled entirely from 4.4 onwards
private const val IS_EXPERIMENT_SURVEY_ENABLED = false

enum class FlagVariant(val value: Int) {
    // disable identifier_name
    DISABLED(0),

    // enable identifier_name
    ENABLED(1)
}

/**
 * Mixpanel experiments
 */
enum class ExperimentalKey(val key: String) {
    SurveyEnabled(EXPERIMENT_SURVEY_ENABLED)
}

class Experiments(private val mixpanel: Mixpanel, private val preferences: SharedPrefs) {
    /// Decide if this user should get variant A or B for the given key
    /// Also stores the chosen variant in SharedPrefs and Mixpanel profile
    fun flagVariant(key: ExperimentalKey): FlagVariant {
        val value = getOrCreateVariant(preferences = preferences, key = key)

        assert(value < 2)

        val flagVariant = if (value == 0) FlagVariant.DISABLED else FlagVariant.ENABLED

        // Set variant on Mixpanel property
        mixpanel.mixpanel.people.set(key.key, flagVariant.value)

        return flagVariant
    }

    private fun hasPersistedValue(preferences: SharedPrefs, key: ExperimentalKey): Boolean =
        preferences.getExperimentalIntPref(key) != null

    private fun storeRandomValue(preferences: SharedPrefs, key: ExperimentalKey) {
        val value = Random.nextInt(0, 2)
        preferences.setExperimentalIntPref(key, value)
    }

    private fun storeValue(preferences: SharedPrefs, key: ExperimentalKey, value: Int) {
        preferences.setExperimentalIntPref(key, value)
    }

    private fun getOrCreateVariant(preferences: SharedPrefs, key: ExperimentalKey): Int {
        when(key) {
            ExperimentalKey.SurveyEnabled -> {
                if (!IS_EXPERIMENT_SURVEY_ENABLED) {
                    // If the survey is disabled for all, we still store the flag and continue as usual,
                    // because we want this to be reflected in Mixpanel as well.
                    storeValue(preferences = preferences, key = key, FlagVariant.DISABLED.value)
                    return preferences.getExperimentalIntPref(key)!!
                }
            }
        }

        // If no value has been stored, calculate a new one
        if (!hasPersistedValue(preferences = preferences, key = key)) {
            storeRandomValue(preferences = preferences, key = key)
        }
        return preferences.getExperimentalIntPref(key)!!
    }
}