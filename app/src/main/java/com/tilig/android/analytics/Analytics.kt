package com.tilig.android.analytics

import android.content.Context
import android.net.Uri
import android.util.Log
import androidx.core.os.bundleOf
import com.android.installreferrer.api.ReferrerDetails
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.mixpanel.android.mpmetrics.MixpanelAPI
import com.tilig.android.BuildConfig
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.utils.SharedPrefs
import com.tilig.android.utils.json

abstract class Tracker {

    /**
     * All the events below are described in this sheet:
     * https://docs.google.com/spreadsheets/d/16BKhUkFjEckjtpTUbV06yGFUJ5P5y6lPGKyNPOVuv28/edit#gid=0
     */
    companion object {

        const val EVENT_SHOW_AUTOFILL_1 = "AutoFill Explanation (step 1) Appeared"
        const val EVENT_SHOW_AUTOFILL_2 = "AutoFill Explanation (step 2) Appeared"
        const val EVENT_AUTOFILL_ENABLED = "AutoFill Enabled"
        const val EVENT_AUTOFILL_DISABLED = "AutoFill Disabled"
        const val EVENT_SKIPPED_AUTOFILL_EXPLANATION = "AutoFill Explanation Skipped"

        //const val EVENT_SKIPPED_AUTOFILL_SUGGESTION = "AutoFill Suggestion Skipped"
        const val EVENT_WELCOME_STARTED = "Welcome Instructions Appeared"
        const val EVENT_WELCOME_COMPLETED = "Welcome Instructions Completed"

        //const val EVENT_SETTINGS = "Settings Opened"
        const val EVENT_AUTOFILL_SETTINGS = "System Settings Opened"
        const val EVENT_APP_SETTINGS = "App Settings Viewed"
        const val EVENT_VIEW_ACCOUNT = "Account Viewed"
        const val EVENT_USERNAME_COPY = "Username Copied"
        const val EVENT_PASSWORD_COPY = "Password Copied"
        const val EVENT_OPEN_WEBSITE = "Website Opened"
        const val EVENT_VIEW_ACCOUNT_LIST = "Account List Viewed"
        const val EVENT_QR_CODE_COPY = "OTP Copied"
        const val EVENT_EXTRA_INFO_COPY = "Extra Info Copied"


        // New events since 1.3.1
        const val EVENT_FIRST_PASSWORD_APPEARED = "First Password Message Appeared"
        const val EVENT_FIRST_PASSWORD_COMPLETED = "First Password Message Completed"
        const val EVENT_FIRST_PASSWORD_SKIPPED = "First Password Message Skipped"
        const val EVENT_AUTOFILL_TRIGGERED = "AutoFill Triggered"
        const val EVENT_AUTOFILL_SUGGESTED = "AutoFill Suggested"
        const val EVENT_AUTOFILL_NO_SUGGESTIONS = "AutoFill No Suggestions"
        const val EVENT_AUTOFILL_ADD_ACCOUNT_TAPPED = "AutoFill Add Account Tapped"

        // Removed after the redesign (2.0), returned in 3.3.20:
        const val EVENT_PASSWORD_GENERATED =
            "Password Generated"

        // New event in 1.3.7
        const val EVENT_AUTOFILL_CANCELED = "AutoFill Canceled"

        // New in 1.4.0
        const val EVENT_AUTOSAVE_USERNAME_CAPTURED = "Username Captured"
        const val EVENT_AUTOSAVE_PASSWORD_CAPTURED = "Password Captured"

        // New since 1.4.1
        const val EVENT_PASSWORD_REVEALED = "Password Revealed"

        // New since 2.0.0, removed in 3.4.8
        //const val EVENT_WEBSITE_COPY = "Website Copied"

        // New in 3.3.2
        const val EVENT_ACCOUNT_CREATED_WITH_WEBSITE = "Account Created With Website"
        const val EVENT_ACCOUNT_CREATED_WITH_APP_ID = "Account Created With Application ID"

        // Not yet implemented (sharing)
        const val EVENT_VIEW_SHARING = "Sharing Opened"

        // New since 3.1.4
        const val EVENT_2FA_READ_MORE_TAPPED = "2FA Read More Tapped"
        const val EVENT_2FA_SCAN_QR_CODE_TAPPED = "2FA Scan QR Code Tapped"
        const val EVENT_2FA_CAMERA_ENABLED = "2FA Camera Enabled"
        const val EVENT_2FA_ADD_QR_MANUALLY_TAPPED = "2FA Add QR Manually Tapped"
        const val EVENT_2FA_SECRET_MANUALLY_ENTERED = "2FA Secret Manually Entered"
        const val EVENT_2FA_ADD_QR_CODE_SCANNED = "2FA QR Code Scanned"

        // renamed in 3.11: 2FA Setup Cancelled -> 2FA Setup Canceled
        const val EVENT_2FA_SETUP_CANCELLED = "2FA Setup Canceled"
        const val EVENT_2FA_INCORRECT_CODE_ENTERED = "2FA Incorrect Code Entered"

        // New since 3.3.11
        const val EVENT_AUTOFILL_SELECTED = "AutoFill Selected"
        const val EVENT_AUTOFILL_EXTENSION_OPENED = "AutoFill Extension Opened"
        const val EVENT_AUTOFILL_EXTENSION_CLOSED = "AutoFill Extension Closed"
        const val EVENT_AUTOFILL_SEARCH_APPEARED = "AutoFill Search Appeared"
        const val EVENT_PHISHING_WARNING_APPEARED = "Phishing Warning Appeared"
        const val EVENT_PHISHING_WARNING_ACCEPTED = "Phishing Warning Accepted"
        const val EVENT_PHISHING_WARNING_CANCELED = "Phishing Warning Canceled"

        // New since 3.3.20 (when Lockscreen was re-introduced)
        // Renamed in 3.11.0
        const val EVENT_BIOMETRICS_ENABLED = "Biometrics Enabled"
        const val EVENT_BIOMETRICS_DISABLED = "Biometrics Disabled"
        const val EVENT_BIOMETRICS_INFO_TAPPED = "Biometrics Information Tapped"
        const val EVENT_BIOMETRICS_NOT_SET_UP = "Biometrics Not Set Up"
        const val EVENT_BIOMETRICS_UNLOCK_CANCELED = "Biometrics Unlock Canceled"
        const val EVENT_BIOMETRICS_UNLOCKED = "Biometrics Unlocked"

        //New since 3.4.0
        const val EVENT_SIGN_OUT = "Sign Out"

        // New since 3.4.8
        const val EVENT_OPEN_TEST_WEBSITE = "Test Website Opened"
        const val EVENT_VIEW_TEST_ACCOUNT = "Test Account Viewed"

        // New since 3.5.0 (added notes)
        const val EVENT_VIEW_NOTE = "Note Viewed"
        const val EVENT_NOTE_CREATED = "Note Created"
        const val EVENT_NOTES_DESCRIPTION_COPIED = "Notes Description Copied"

        // New since 3.7.0 (added credit card)
        const val EVENT_VIEW_CREDIT_CARD = "Credit Card Viewed"
        const val EVENT_CREDIT_CARD_CREATED = "Credit Card Created"
        const val EVENT_CARD_NUMBER_COPIED = "Credit Card Number Copied"
        const val EVENT_CARD_HOLDER_COPIED = "Credit Cardholder Copied"
        const val EVENT_CARD_EXPIRE_COPIED = "Credit Card Expiration Date Copied"
        const val EVENT_CARD_SECRET_COPIED = "Credit Card Security Code Copied"
        const val EVENT_CARD_PIN_CODE_COPIED = "Credit Card Pin Code Copied"
        const val EVENT_CARD_ZIP_CODE_COPIED = "Credit Card ZIP Copied"
        const val EVENT_NOTE_DELETED = "Note Deleted"
        const val EVENT_LOGIN_DELETED = "Login Deleted"
        const val EVENT_CREDIT_CARD_DELETED = "Credit Card Deleted"

        // New since 3.8.0 (added wifi type)
        const val EVENT_VIEW_WIFI = "Wifi Viewed"
        const val EVENT_WIFI_CREATED = "Wifi Created"
        const val EVENT_WIFI_DELETED = "Wifi Deleted"
        const val EVENT_WIFI_PASSWORD_COPIED = "Wifi Password Copied"
        const val EVENT_WIFI_NETWORK_NAME_COPIED = "Wifi Network Name Copied"

        const val PROPERTY_UTM_SOURCE = "UTM Source"
        const val PROPERTY_UTM_MEDIUM = "UTM Medium"
        const val PROPERTY_UTM_CAMPAIGN = "UTM Campaign"
        const val PROPERTY_UTM_TERM = "UTM Term"
        const val PROPERTY_UTM_CONTENT = "UTM Content"

        // Only on Android
        // Updated in 3.11: added a property where it appeared. "Autofill Extension" or "App Item List".
        const val EVENT_SEARCH_ITEM_TAPPED = "Item Searched"
        const val EVENT_SEARCH_ITEM_TAPPED_SOURCE = "source"
        const val EVENT_SEARCH_ITEM_TAPPED_IN_AUTOFILL = "Autofill Extension"
        const val EVENT_SEARCH_ITEM_TAPPED_ITEM_LIST = "App Item List"
        
        // New since 3.11.0
        const val EVENT_VIEW_PASSWORD_HISTORY = "Password History Viewed"
        const val EVENT_PASSWORD_RESTORED = "Password Restored"
        
        const val PROPERTY_TEST_DRIVE_ENABLED = "android-test_drive"

        // New in 4.3
        const val GETTING_STARTED_PROMPT_CLICKED = "Gettings Started Prompt Clicked"
        const val GETTING_STARTED_BIOMETRICS_EXPLANATION_APPEARED = "Biometrics Explanation Appeared"
        const val GETTING_STARTED_ADD_LOGIN_EXPLANATION_APPEARED = "Add Login Explanation Appeared"
        const val GETTING_STARTED_AUTOFILL_EXPLANATION_APPEARED = "Autofill Explanation Appeared"
        const val GETTING_STARTED_CONNECT_DESKTOP_APPEARED = "Connect Desktop Explanation Appeared"
        // When the user dismisses the getting started and the warning dialog is shown
        const val DISMISS_GETTING_STARTED_APPEARED = "Getting Started Dismiss Appeared"
        // When the user dismisses that dialog
        const val GETTING_STARTED_DISMISSED = "Getting Started Dismissed"
        // When the user has completed all tasks and the 'Explore Tilig' button appears
        const val GETTING_STARTED_COMPLETED_APPEARED = "Getting Started Completed Appeared"
        const val GETTING_STARTED_PROMPT_APPEARED = "Getting Started Prompt Appeared"
        //const val GETTING_STARTED_APPEARED = "Gettings Started Appeared"
        //const val IMPORT_EXPLANATION_APPEARED = "Import Explanation Appeared"
        //const val AUTOSAVE_EXPLANATION_APPEARED = "Autosave Explanation Appeared"

        // New since 4.4
        const val EVENT_ASKED_FOR_RATING = "Rating Asked"
        
        // Upcoming (4.4?)
        const val EVENT_AUTOSAVE_EXTENSION_OPENED = "AutoCapture Extension Opened"
        const val EVENT_AUTOSAVE_EXTENSION_CLOSED = "AutoCapture Extension Closed"
    }

    abstract fun trackEvent(key: String)

    open fun trackEvent(key: String, properties: Map<String, Any>?) { }

    open fun trackAutofillEvent(prefs: SharedPrefs, conversions: ConversionAnalytics) { }

    abstract fun setIdentity(identifier: String)

    abstract fun onSessionEnd()

    abstract fun trackAccountCreation(account: Account)

    abstract fun trackUtmReferrer(response: ReferrerDetails)

}

class ConversionAnalytics : Tracker() {

    private val firebase: FirebaseAnalytics = Firebase.analytics

    companion object {
        const val TAG = "Conversions"
        const val EVENT_AUTOFILL_ENABLED = "autofill_enabled"
        const val EVENT_FIRST_AUTOFILL = "first_autofill"
        const val EVENT_THIRD_AUTOFILL = "third_autofill"
    }

    override fun trackEvent(key: String) {
        firebase.logEvent(key, null)
    }

    override fun trackUtmReferrer(response: ReferrerDetails) {
        val referrerUrl: String = response.installReferrer
        if (referrerUrl.isNotEmpty()) {
            // Prefix a dummy domain, because the 'url' is often only the query string
            val completeUrl =
                if (referrerUrl.startsWith("utm")) "protocol://domain/?$referrerUrl" else referrerUrl
            val uri = Uri.parse(completeUrl)
            val utmSource = uri.getQueryParameter("utm_source")
            val utmMedium = uri.getQueryParameter("utm_medium")
            val utmCampaign = uri.getQueryParameter("utm_campaign")
            val utmTerm = uri.getQueryParameter("utm_term")
            val utmContent = uri.getQueryParameter("utm_content")

            Log.v(TAG, "Referrer Url: $completeUrl")

            firebase.logEvent(
                FirebaseAnalytics.Event.CAMPAIGN_DETAILS, bundleOf(
                    FirebaseAnalytics.Param.SOURCE to utmSource,
                    FirebaseAnalytics.Param.MEDIUM to utmMedium,
                    FirebaseAnalytics.Param.CAMPAIGN to utmCampaign,
                    FirebaseAnalytics.Param.TERM to utmTerm,
                    FirebaseAnalytics.Param.CONTENT to utmContent
                )
            )
        } else {
            Log.w(TAG, "Referrer is empty")
        }
    }

    override fun setIdentity(identifier: String) {
        firebase.setUserId(identifier)
    }

    override fun onSessionEnd() {
        // no-op
    }

    override fun trackAccountCreation(account: Account) {
        // no-op
    }
}

@Suppress("JoinDeclarationAndAssignment")
class Mixpanel(appContext: Context) : Tracker() {

    val mixpanel: MixpanelAPI
    var hasProperties = false

    companion object {

        private const val TAG = "ANALYTICS"

        private fun getSuperProperties() = json {
            "x-tilig-version" to "Android ${BuildConfig.VERSION_NAME}"
            "x-tilig-platform" to "Android"
        }
    }

    init {
        mixpanel = MixpanelAPI
            .getInstance(appContext, BuildConfig.MIXPANEL_TOKEN, getSuperProperties())
        hasProperties = true
        ensureSuperProperties()
    }

    fun ensureSuperProperties() {
        if (!hasProperties || mixpanel.superProperties.length() == 0) {
            mixpanel.registerSuperProperties(getSuperProperties())
            Log.w(TAG, "Mixpanel superProperties set")
        }
    }

    override fun trackEvent(key: String) {
        trackEvent(key, null)
    }

    override fun trackEvent(key: String, properties: Map<String, Any>?) {
        // These seem to get lost without proper care
        ensureSuperProperties()
        Log.i(TAG, "Event: $key (distinctId: ${mixpanel.distinctId})")
        // Note that we also track events in the debug app,
        // because we initialise Mixpanel with a special debug token
        if (properties != null) {
            mixpanel.trackMap(key, properties)
        } else {
            mixpanel.track(key)
        }
    }

    override fun trackAutofillEvent(prefs: SharedPrefs, conversions: ConversionAnalytics) {
        trackEvent(EVENT_AUTOFILL_SELECTED, null)
        prefs.autofillCount++
        if (prefs.autofillCount == 1) {
            conversions.trackEvent(ConversionAnalytics.EVENT_FIRST_AUTOFILL)
        } else if (prefs.autofillCount == 3) {
            conversions.trackEvent(ConversionAnalytics.EVENT_THIRD_AUTOFILL)
        }
    }

    override fun onSessionEnd() {
        Log.i(TAG, "Flushing Mixpanel events")
        mixpanel.flush()
    }

    override fun setIdentity(identifier: String) {
        val distinctId = identifier.lowercase()
        ensureSuperProperties()
        if (distinctId != mixpanel.distinctId) {
            Log.i(
                TAG,
                "Mixpanel identifying. Previous distinctId ${mixpanel.distinctId}, new: $distinctId "
            )
            mixpanel.alias(distinctId, mixpanel.distinctId)
        }
        mixpanel.identify(distinctId)
        mixpanel.people.identify(distinctId)
    }

    override fun trackUtmReferrer(response: ReferrerDetails) {
        val referrerUrl: String = response.installReferrer
        if (referrerUrl.isNotEmpty()) {
            // Prefix a dummy domain, because the 'url' is often only the query string
            val completeUrl =
                if (referrerUrl.startsWith("utm")) "protocol://domain/?$referrerUrl" else referrerUrl
            val uri = Uri.parse(completeUrl)
            val utmSource = uri.getQueryParameter("utm_source")
            val utmMedium = uri.getQueryParameter("utm_medium")
            val utmCampaign = uri.getQueryParameter("utm_campaign")
            val utmTerm = uri.getQueryParameter("utm_term")
            val utmContent = uri.getQueryParameter("utm_content")

            Log.v(ConversionAnalytics.TAG, "Referrer Url: $completeUrl")

            mixpanel.people.apply {
                set(PROPERTY_UTM_SOURCE, utmSource)
                set(PROPERTY_UTM_MEDIUM, utmMedium)
                set(PROPERTY_UTM_CAMPAIGN, utmCampaign)
                set(PROPERTY_UTM_TERM, utmTerm)
                set(PROPERTY_UTM_CONTENT, utmContent)
            }
        } else {
            Log.w(ConversionAnalytics.TAG, "Referrer is empty")
        }
    }

    fun logout() {
        Log.i(TAG, "Mixpanel logging out")
        mixpanel.flush()
        mixpanel.reset()
        hasProperties = false
        // Adding the properties right away, so we have them ready before the next event.
        // Even though we also set them on every call to trackEvent, we might miss automatic
        // events like App Session, which is why we do it here too:
        ensureSuperProperties()
    }

    override fun trackAccountCreation(account: Account) {
        // The backend tracks the "Account Created" event, but we also
        // want to know the balance between website/android_app_id fields.
        // This is a feature unique for Android
        if (!account.androidAppId.isNullOrEmpty()) {
            trackEvent(EVENT_ACCOUNT_CREATED_WITH_APP_ID)
        } else if (!account.website.isNullOrEmpty()) {
            trackEvent(EVENT_ACCOUNT_CREATED_WITH_WEBSITE)
        }
    }

}
