package com.tilig.android.utils

import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class TimerHelper(val intervalMs: Long) {

    private val scope = MainScope()
    private var job: Job? = null
    private var ticks = 0
    val ticksFlow = MutableStateFlow(0)

    fun start() {
        ticks = 0
        job = scope.launch {
            while (true) {
                ticks += 1
                ticksFlow.value = ticks
                delay(intervalMs)
            }
        }
    }

    fun stop() {
        job?.cancel()
        job = null
    }

}
