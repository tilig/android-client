package com.tilig.android.utils.lock

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.fragment.app.FragmentActivity
import com.tilig.android.R
import com.tilig.android.utils.SharedPrefs
import io.sentry.Breadcrumb.debug

@RequiresApi(Build.VERSION_CODES.P)
class LockUtilPie internal constructor(prefs: SharedPrefs) : LockUtil(prefs) {

    private val authenticators = if (DEFAULT_ALLOW_KEYCODE_FALLBACK) {
        BiometricManager.Authenticators.BIOMETRIC_STRONG or BiometricManager.Authenticators.BIOMETRIC_WEAK or BiometricManager.Authenticators.DEVICE_CREDENTIAL
    } else {
        BiometricManager.Authenticators.BIOMETRIC_STRONG or BiometricManager.Authenticators.BIOMETRIC_WEAK
    }
    private val retryAuthenticators = BiometricManager.Authenticators.DEVICE_CREDENTIAL

    override fun areBiometricsSupported(context: Context): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            return context.packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)
        } else {
            val bm = BiometricManager.from(context)
            val status = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                bm.canAuthenticate(authenticators)
            } else {
                @Suppress("DEPRECATION")
                bm.canAuthenticate()
            }
            return when (status) {
                BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE,
                BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE,
                BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED -> false
                BiometricManager.BIOMETRIC_SUCCESS,
                BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> true
                else -> true
            }
        }
    }

    override fun canAuthorize(context: Context): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            return context.packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)
        } else {
            val bm = BiometricManager.from(context)
            val status = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                bm.canAuthenticate(authenticators)
            } else {
                @Suppress("DEPRECATION")
                bm.canAuthenticate()
            }
            return when (status) {
                BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE,
                BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE,
                BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED,
                BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED -> false
                BiometricManager.BIOMETRIC_SUCCESS -> true
                else -> true
            }
        }
    }

    /**
     * Starts the verification flow if needed.
     */
    override fun authorize(
        activity: FragmentActivity,
        listener: LockEventListener,
        requestCode: Int,
        retryWithPincode: Boolean
    ) {
        val prompt =
            BiometricPrompt(activity, activity.mainExecutor, getAuthenticationCallback(listener))
        val builder = BiometricPrompt.PromptInfo.Builder()
            .setTitle(
                activity.getString(
                    R.string.lock_title,
                    activity.getString(R.string.app_name)
                )
            )
            .setConfirmationRequired(false)
            .setNegativeButtonText(activity.getString(R.string.cancel))

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            builder.setAllowedAuthenticators(if (retryWithPincode) retryAuthenticators else authenticators)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            @Suppress("DEPRECATION")
            builder.setDeviceCredentialAllowed(retryWithPincode || DEFAULT_ALLOW_KEYCODE_FALLBACK)
        }
        prompt.authenticate(builder.build())
    }

    private fun getAuthenticationCallback(listener: LockEventListener): BiometricPrompt.AuthenticationCallback {
        return object : BiometricPrompt.AuthenticationCallback() {
            override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errorCode, errString)
                Log.w(
                    LockUtilPie::class.java.name,
                    "Failed biometric login, errorCode = $errorCode"
                )
                debug("Failed biometric login, errorCode = $errorCode")
                listener.onAppUnlockFailed(errorCode)
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                isTempAuthorized = true
                listener.onAppUnlocked()
            }
        }
    }
}
