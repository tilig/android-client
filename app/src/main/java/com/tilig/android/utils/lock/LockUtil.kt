package com.tilig.android.utils.lock

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Build
import android.util.Log
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.tilig.android.utils.SharedPrefs
import java.util.concurrent.TimeUnit


/**
 * Helps with the screen unlock feature (pattern/pin/fingerprint/etc).
 *
 * Call [authorize] to initiate the unlock. Will skip
 * and return true when already authorized, or when not set up
 * or not supported.
 */
abstract class LockUtil internal constructor(protected val prefs: SharedPrefs) : LifecycleObserver {

    companion object {

        private const val TAG = "LockUtil"
        private const val DEBUG = true
        private const val FORCE_LOCK_DISABLED = false
        internal const val DEFAULT_ALLOW_KEYCODE_FALLBACK = false

        /**
         * Re-lock after 3 minutes
         * (or 10 seconds in debug builds)
         */
        private val TEMP_AUTH_TIMEOUT_MILLIS = TimeUnit.SECONDS.toMillis(10)

        // I'm keeping (and suppressing) this version check, because
        // it makes this LockUtil a very re-usable snippet.
        @SuppressLint("ObsoleteSdkInt")
        fun isSupported(): Boolean =
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
                    && !FORCE_LOCK_DISABLED
    }

    /**
     * Keeps track of the last time the user was verified
     */
    private var lastExitMillis: Long
        get() = prefs.lastAppExit
        set(value) {
            prefs.lastAppExit = value
        }

    /**
     * Should be true while the user is verified and still
     * using the app. We can't rely on the lastExitMillis alone,
     * because it would expire while the app is still open.
     */
    internal var isTempAuthorized: Boolean = false

    fun log(message: String) {
        if (DEBUG) {
            Log.v(TAG, message)
        }
    }

    /**
     * Resets the authorization. Call this e.g. when the main screen
     * is exited by the user.
     */
    fun unauthorize() {
        log("Forcing screenlock")
        lastExitMillis = 0
        isTempAuthorized = false
    }

    /**
     * Checks if the user is still authorized, so we don't ask again too often
     */
    private fun isTempAuthorized(): Boolean {
        val now = System.currentTimeMillis()
        val hasExpired = (now - lastExitMillis) > TEMP_AUTH_TIMEOUT_MILLIS
        log("isTempAuthorized: isTempAuthorized=$isTempAuthorized || !isLockEnabled=${!prefs.isLockEnabled} || !isExpired=${!hasExpired}")
        return isTempAuthorized
                || !prefs.isLockEnabled
                || !hasExpired
    }

    fun shouldShowLock(): Boolean {
        val isSupported = isSupported()
        val isTempAuthorized = isTempAuthorized()
        log("shouldShowLock: isSupported=$isSupported && !isTempAuthorized=${!isTempAuthorized} && !isLockEnabled=${!prefs.isLockEnabled} && isLoggedIn=${prefs.isLoggedIn()}")
        return isSupported
                && !isTempAuthorized
                && prefs.isLockEnabled
                && prefs.isLoggedIn()
    }

    fun isLockEnabled() = prefs.isLockEnabled

    @Suppress("unused")
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppMovedToForeground() {
        log("App is back to foreground, last seen = $lastExitMillis")
        if (isTempAuthorized()) {
            isTempAuthorized = true
        }
    }

    @Suppress("unused")
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppMovedToBackground() {
        lastExitMillis = if (isTempAuthorized) System.currentTimeMillis() else 0
        isTempAuthorized = false
        log("App moved to background, timestamp = $lastExitMillis")
    }

    fun onAuthorizationEvent(resultCode: Int): Boolean {
        isTempAuthorized = (resultCode == Activity.RESULT_OK)
        return isTempAuthorized
    }

    /**
     * If biometrics are supported, doesn't mean the user has set it up.
     * This method checks if we actually can authorize with the settings that we want.
     * (i.e.: biometrics only, not only device credentials)
     */
    abstract fun canAuthorize(context: Context): Boolean

    abstract fun areBiometricsSupported(context: Context): Boolean

    /**
     * Starts the verification flow if needed.
     */
    abstract fun authorize(
        activity: FragmentActivity,
        listener: LockEventListener,
        requestCode: Int,
        retryWithPincode: Boolean = false
    )
}
