package com.tilig.android.utils

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.util.Log

fun Context.openLink(url: String?): Boolean =
    if (url.isNullOrBlank()) {
        Log.w("Context.openLink", "Cannot open null or blank url")
        false
    } else {
        val intent = Intent(Intent.ACTION_VIEW).apply {
            // We 'httpify' the link first, otherwise Uri.parse does not accept it
            data = Uri.parse(url.httpify())
        }
        // Even though we httpify the URL, it may still fail. Example (from the wild) is
        // a user with 'loremhttps://ipsum'. Which is a valid URI, but not something
        // Android knows how to handle.
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
            true
        } else {
            false
        }
    }

fun Context.tryOpenSpecificApp(appId: String?): Boolean = try {
    if (appId.isNullOrBlank()) {
        Log.w("Context.tryOpenSpecificApp", "Cannot open null or blank app Id")
        false
    } else {
        val intent = this.packageManager.getLaunchIntentForPackage(appId)
        this.startActivity(intent)
        true
    }
} catch (e: PackageManager.NameNotFoundException) {
    false
} catch (e: NullPointerException) {
    // if not valid package name (can happen if device does not have a specific app any more)
    false
}
