package com.tilig.android.utils

import android.content.Context
import android.util.Log
import com.tilig.android.BuildConfig
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.text.SimpleDateFormat
import java.util.*

class DebugLogger(
    val logTimestamps: Boolean = true
) {

    private val filename = "debug_log.txt"
    private val format = SimpleDateFormat.getTimeInstance()
    private val collection = StringBuilder()

    fun collect(message: String) {
        if (BuildConfig.DEBUG) {
            Log.v("DebugLogger", message)
            if (logTimestamps) {
                collection.appendLine(format.format(Date()))
            }
            collection.appendLine(message)
        }
    }

    fun logCollection(context: Context) {
        synchronized(context) {
            if (BuildConfig.DEBUG) {
                val file = context.openFileOutput(filename, Context.MODE_APPEND)
                val osw = file.writer()
                osw.write(collection.toString())
                osw.flush()
                osw.close()
                collection.clear()
            }
        }
    }

    fun log(context: Context, prefs: SharedPrefs, message: String) {
        synchronized(context) {
            Log.v("DebugLogger", message)
            if (BuildConfig.DEBUG && prefs.devToggleEnableDebugLogsAutofill) {
                val file = context.openFileOutput(filename, Context.MODE_APPEND)
                val osw = file.writer()
                osw.write("${format.format(Date())}\n$message\n")
                osw.flush()
                osw.close()
            }
        }
    }

    override fun toString(): String {
        return collection.toString()
    }

    fun read(context: Context) : String {
        synchronized(context) {
            try {
                val file = context.openFileInput(filename)
                val text = StringBuilder()
                val br = BufferedReader(InputStreamReader(file))
                var line: String?
                while (br.readLine().also { line = it } != null) {
                    text.append(line)
                    text.append('\n')
                }
                br.close()
                return text.toString()
            } catch (e: IOException) {
                Log.e("DebugLogger", "Error reading debug log", e)
            }
            return "<no logs>"
        }
    }

    fun clear(context: Context) {
        synchronized(context) {
            if (BuildConfig.DEBUG) {
                val file = context.openFileOutput(filename, Context.MODE_PRIVATE)
                val osw = file.writer()
                osw.write("")
                osw.flush()
                osw.close()
            }
        }
    }

}
