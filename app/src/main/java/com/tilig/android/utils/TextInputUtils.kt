package com.tilig.android.utils

import androidx.compose.ui.text.input.KeyboardType

object TextInputUtils {

    /**
     * It is not enough to setup only KeyboardType to prevent unexpected input.
     * Unexpected input can be still added via copy/paste action.
     * Method helps to check if the input satisfies selected keyboard type
     */
    fun isInputTextSatisfyKeyboardType(input: String, type: KeyboardType): Boolean {
        return when (type) {
            KeyboardType.Number -> {
                return input.all { char -> char.isDigit() }
            }
            KeyboardType.Decimal -> {
                val regex = "-?[0-9]+(\\.[0-9]+)?".toRegex()
                return input.matches(regex)
            }
            // other keyboard types can be handle if it's needed
            else -> true
        }
    }

}