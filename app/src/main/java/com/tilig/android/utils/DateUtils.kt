package com.tilig.android.utils

import android.content.Context
import android.os.Build
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    const val API_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    const val DATE_SLASH_FORMAT = "dd/MM/yyyy"
    const val DATE_DOT_FORMAT = "dd.MM.yyyy"

    fun formatDate(
        context: Context,
        originDate: String?,
        originalFormat: SimpleDateFormat,
        targetFormat: String,
    ): String? {
        return formatDate(context, originDate, arrayOf(originalFormat), targetFormat)
    }

    /**
     * Format date string to another date format
     */
    fun formatDate(
        context: Context,
        originDate: String?,
        originalFormats: Array<SimpleDateFormat>,
        targetFormat: String,
    ): String? {
        val date = parseDateFormats(originDate, originalFormats)
        return date?.let { SimpleDateFormat(targetFormat, getLocale(context)).format(it) }
    }

    /**
     * Build beginning of the day string based on year, month and day in selected format
     */
    fun buildDateStr(
        context: Context,
        year: Int,
        month: Int,
        day: Int,
        format: String,
        timeZone: TimeZone = TimeZone.getTimeZone("UTC")
    ): String {
        val calendar = Calendar.getInstance()
        calendar.timeZone = timeZone
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, day)
        calendar.set(Calendar.HOUR, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)

        val utcTime = calendar.time
        val sdf = SimpleDateFormat(format, getLocale(context))
        sdf.timeZone = timeZone
        return sdf.format(utcTime)
    }

    /**
     * Attempts to parse a string-formatted date using the given
     * formats. Starts with the first, uses the consecutive ones
     * as fallbacks.
     */
    fun parseDateFormats(date: String?, formats: Array<SimpleDateFormat>): Date? {
        if (date == null) {
            return null
        }
        formats.forEach {
            try {
                return it.parse(date)
            } catch (_: ParseException) {
            }
        }
        return null
    }

    private fun getLocale(context: Context) = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        context.resources.configuration.locales[0]
    } else {
        @Suppress("DEPRECATION")
        context.resources.configuration.locale
    }

    fun convertDateToTimeStamp(date: String?): Long {
        date?.let {
            try {
                val dateValue = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH).parse(it)
                return dateValue?.time?:0L
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }
        return 0L
    }
}