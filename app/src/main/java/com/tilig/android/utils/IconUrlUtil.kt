package com.tilig.android.utils

import android.util.Log
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.tilig.android.R
import com.tilig.android.autofill.PackageVerifier
import com.tilig.android.data.addaccount.SuggestedBrand
import com.tilig.android.data.models.tilig.Account

object IconUrlUtil {

//    private const val URL = "https://logo.clearbit.com/%s"
//    private const val FALLBACK_URL = "https://logo.uplead.com/%s"
    private const val URL = "https://icon.tilig.com/%s"
    private const val ALLOW_LEGACY_ICONSERVER_FALLBACK = false

    fun apply(iconView: ImageView, account: Account) {
        val iconUrl = getIconUrl(account)
        when {
            iconUrl != null -> {
                Glide.with(iconView.context)
                    .load(iconUrl)
                    .error(R.drawable.ic_acorn_placeholder)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(iconView)
            }
            account.hasAppId() -> {
                val drawable = PackageVerifier.getIcon(iconView.context, account.androidAppId!!)
                Glide.with(iconView.context)
                    .load(drawable)
                    .error(R.drawable.ic_acorn_placeholder)
                    .into(iconView)
            }
            else -> {
                iconView.setImageResource(R.drawable.ic_acorn_placeholder)
            }
        }
    }

    fun apply(iconView: ImageView, account: SuggestedBrand) {
        val iconUrl = getIconUrl(account)
        if (iconUrl != null) {
            Log.i("ICON", iconUrl)
            Glide.with(iconView.context)
                .load(iconUrl)
                .error(R.drawable.ic_acorn_placeholder)
                .transition(DrawableTransitionOptions.withCrossFade())
                .transform(RoundedCorners(16))
                .into(iconView)
        } else {
            iconView
                .setImageResource(R.drawable.ic_add_black)
        }
    }

    private fun getIconUrl(url: String) = URL.format(url.getDomain())

    private fun getIconUrl(account: SuggestedBrand): String? = if (account.url.isBlank()) {
        null
    } else {
        getIconUrl(account.url)
    }

    fun getIconUrl(account: Account): String? =
        when {
            account.brand?.getBrandImage() != null -> {
                account.brand?.getBrandImage()
            }
            account.domain != null && ALLOW_LEGACY_ICONSERVER_FALLBACK -> {
                if (account.domain.isNullOrBlank()) {
                    null
                } else {
                    URL.format(account.domain?.removePredefinedPrefixesFromDomain())
                }
            }
            account.website?.isValidIP() == true && ALLOW_LEGACY_ICONSERVER_FALLBACK -> null
            else -> null
        }

}
