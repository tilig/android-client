package com.tilig.android.utils

import android.content.Context
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import com.tilig.android.R
import com.tilig.android.data.models.tilig.*
import com.tilig.android.ui.components.getIcon
import com.tilig.android.ui.theme.DarkBlue
import com.tilig.android.ui.theme.DarkGreen
import com.tilig.android.ui.theme.WifiBgColor

object SecretItemExt {

    @Composable
    fun SecretItem.getSecretIcon(context: Context) = when (this) {
        is Account -> this.getIcon(context)
        is Note -> painterResource(id = R.drawable.ic_note)
        is CreditCard -> painterResource(id = R.drawable.ic_credit_card)
        is WifiPassword -> painterResource(id = R.drawable.ic_wifi)
        else -> painterResource(id = R.drawable.ic_other_dots)
    }

    @Composable
    fun SecretItem.getBackgroundColor(): Color = when(this){
        is Account -> this.brand?.getBrandColor()?: DarkBlue
        is Note -> DarkBlue
        is CreditCard -> DarkGreen
        is WifiPassword -> WifiBgColor
        else -> DarkBlue
    }
}