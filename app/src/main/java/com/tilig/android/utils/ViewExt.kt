@file:Suppress("DEPRECATION")

package com.tilig.android.utils

import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.ClipboardManager
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.core.text.HtmlCompat
import com.tilig.android.R
import com.tilig.android.data.models.tilig.Account
import java.io.BufferedReader
import java.util.*


fun View.enable(): View {
    this.isEnabled = true
    return this
}

fun View.show(): View {
    this.visibility = View.VISIBLE
    return this
}

@Suppress("DEPRECATION")
fun String.copyToClipboard(context: Context): Boolean =
    try {
        val sdk = Build.VERSION.SDK_INT
        if (sdk < Build.VERSION_CODES.HONEYCOMB) {
            val clipboard = context
                .getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            clipboard.text = this
        } else {
            val clipboard = context
                .getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
            val clip = ClipData
                .newPlainText(
                    context.resources.getString(
                        R.string.clipboard_label
                    ), this
                )
            clipboard.setPrimaryClip(clip)
        }
        true
    } catch (e: Exception) {
        false
    }

fun Bundle?.bundleToString(clz: Class<*>?, builder: StringBuilder? = null): String {
    if (this == null) {
        return "N/A"
    }
    clz?.let {
        this.classLoader = clz.classLoader
    }
    if (builder == null) {
        val stringBuilder = StringBuilder()
        return this.bundleToString(clz, stringBuilder)
    } else {
        val keySet = try {
            this.keySet()
        } catch (e : IllegalArgumentException) {
            emptySet<String>()
        }
        builder.append("[Bundle with ").append(keySet.size).append(" keys:\n")
        for (key in keySet) {
            builder.append(' ').append(key).append('=')
            val value = this.get(key)
            if (value is Bundle) {
                value.bundleToString(null, builder)
            } else {
                val string = if (value is Array<*>) Arrays.toString(value) else value
                builder.append(string)
                builder.append("\n")
            }
        }
        builder.append(']')
    }
    return builder.toString()
}

fun Intent?.debugIntent(clz: Class<*>? = null) {
    if (this == null) {
        Log.v("INTENT", "Intent is null")
        return
    }
    Log.v("INTENT", "Intent action: ${this.action}")
    if (this.extras == null) {
        Log.v("INTENT", "Intent has no extras")
    } else {
        Log.v("INTENT", "Intent extras:\n${this.extras.bundleToString(clz)}")
    }
}

class OnDebouncedClickListener : View.OnClickListener {

    private val onClickListener: View.OnClickListener

    constructor(listener: View.OnClickListener) {
        onClickListener = listener
    }

    constructor(listener: (View) -> Unit) {
        onClickListener = View.OnClickListener { listener.invoke(it) }
    }

    override fun onClick(v: View) {
        val currentTimeMillis = System.currentTimeMillis()

        if (currentTimeMillis >= previousClickTimeMillis + DELAY_MILLIS) {
            previousClickTimeMillis = currentTimeMillis
            onClickListener.onClick(v)
        }
    }

    companion object {
        // Tweak this value as you see fit. In my personal testing this
        // seems to be good, but you may want to try on some different
        // devices and make sure you can't produce any crashes.
        private const val DELAY_MILLIS = 200L

        private var previousClickTimeMillis = 0L
    }

}

fun View.setOnDebouncedClickListener(l: View.OnClickListener) {
    setOnClickListener(OnDebouncedClickListener(l))
}

fun View.setOnDebouncedClickListener(l: (View) -> Unit) {
    setOnClickListener(OnDebouncedClickListener(l))
}
