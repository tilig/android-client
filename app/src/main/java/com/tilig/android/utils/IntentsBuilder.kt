package com.tilig.android.utils

import android.content.Intent
import android.net.Uri

object IntentsBuilder {
    fun getEmailSendIntent(): Intent {
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse("mailto:") // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf("support@tilig.com"))
        return intent
    }
}