package com.tilig.android.utils

import android.app.Application
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.tilig.android.BuildConfig
import com.tilig.android.analytics.ConversionAnalytics
import com.tilig.android.analytics.Experiments
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.config.AppExecutors
import com.tilig.android.data.api.ConnectivityMonitor
import com.tilig.android.data.livedata.ConnectivityLiveData
import com.tilig.android.data.livedata.ForcedLogoutLiveData
import com.tilig.android.data.repository.Repository
import com.tilig.android.data.repository.RepositoryImpl
import com.tilig.android.data.repository.RepositoryMockImpl
import com.tilig.android.featureflag.FeatureFlagClient
import com.tilig.android.migration.MigrationLegacyAccounts
import com.tilig.android.migration.MigrationLegacyTestDriveItems
import com.tilig.android.testdrive.TestDrive
import com.tilig.android.ui.CryptoModel
import com.tilig.android.ui.signin.AuthClients
import com.tilig.android.utils.lock.LockUtilFactory
import okhttp3.Cache
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import org.koin.dsl.module

object KoinHelper {

    @JvmStatic
    fun start(application: Application) {

        val appModules: Module = module {

            fun provideRepository(prefs: SharedPrefs): Repository {
                return if (prefs.devToggleSimulateResponses && BuildConfig.DEBUG) {
                    RepositoryMockImpl(prefs)
                } else {
                    RepositoryImpl()
                }
            }

            single { ForcedLogoutLiveData() }
            single { Cache(application.cacheDir, (5 * 1024 * 1024).toLong()) }
            single { ConnectivityMonitor.getInstance(application) }
            single { ConnectivityLiveData() }
            single { AppExecutors.instance }
            single { SharedPrefs(application) }
            single { CryptoModel() }
            single { Mixpanel(application) }
            single { ConversionAnalytics() }
            single { FeatureFlagClient(application) }
            single { AuthClients(application) }
            single { Experiments(get(), get()) }
            single { LockUtilFactory.create(get()) }
            single { Moshi.Builder().addLast(KotlinJsonAdapterFactory()).build() }
            single { provideRepository(get()) }
            single { MigrationLegacyAccounts(get()) }
            single { MigrationLegacyTestDriveItems(get(), get(), get()) }
        }

        startKoin {
            androidContext(application)
            modules(appModules)
        }
    }
}
