package com.tilig.android.utils.lock

interface LockEventListener {
    fun onAppUnlocked()
    fun onAppStartLocked()
    fun onAppUnlockFailed(errorCode: Int)
}
