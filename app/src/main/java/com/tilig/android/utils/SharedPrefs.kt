package com.tilig.android.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.util.Log
import androidx.datastore.preferences.SharedPreferencesMigration
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.tilig.android.analytics.Breadcrumb
import com.tilig.android.analytics.ExperimentalKey
import com.tilig.crypto.Crypto
import com.tilig.crypto.KeyPairString
import com.tilig.crypto.KeyPairsString
import io.sentry.Sentry
import io.sentry.SentryLevel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.FileNotFoundException
import java.io.IOException
import java.security.GeneralSecurityException
import java.security.KeyStoreException

class SharedPrefs(context: Context) {

    private val Context.dataStore by preferencesDataStore(
        name = pref_name
    )

    private object PreferencesKeys {
        val KEY_LEGACY_AUTH_PRIVATE = stringPreferencesKey(AUTH_LEGACY_PRIVATE_KEY)
        val KEY_LEGACY_AUTH_PUBLIC = stringPreferencesKey(AUTH_LEGACY_PUBLIC_KEY)
        val KEY_AUTH_PRIVATE = stringPreferencesKey(AUTH_LIBSODIUM_PRIVATE_KEY)
        val KEY_AUTH_PUBLIC = stringPreferencesKey(AUTH_LIBSODIUM_PUBLIC_KEY)
    }

    private val dataStore = context.dataStore

    companion object {

        const val pref_name = "tilig_preferences"
        private const val TAG = "prefs"

        private const val EMAIL_PREF_KEY = "email"
        private const val USER_ID_PREF_KEY = "user_id"
        private const val MIXPANEL_USER_ID_PREF_KEY = "mixpanel_user_id"
        private const val GOOGLE_ID_TOKEN_PREF_KEY = "google_id_token"
        private const val GOOGLE_SERVER_CODE_PREF_KEY = "google_server_code"
        private const val AUTOFILL_DONT_ASK_AGAIN = "autofill_dont_ask"
        private const val BIO_LOCK_ENABLED = "lock_enabled"
        private const val BIO_LOCK_TEMP_UNLOCKED = "temp_unlocked"
        private const val BIO_LOCK_LAST_EXIT = "last_exit"
        private const val LAST_APP_VERSION = "last_versioncode"
        private const val HAS_FINISHED_ONBOARDING = "finished_onboarding"
        private const val SKIP_FIRST_ACCOUNT = "skip_first_account"
        private const val SKIP_SURVEY = "skip_survey"
        private const val DESKTOP_CONNECTED = "desktop_connected"
        private const val HAS_ONBOARDED_BEFORE = "has_onboarded_before"
        private const val HAS_ASKED_FOR_RATING = "has_asked_for_rating"

        private const val DEV_TOGGLE_INLINE_AUTOFILL = "inline_autofill"
        private const val DEV_TOGGLE_SINGLE_AUTOFILL_ENTRY = "single_autofill_entry"
        private const val DEV_TOGGLE_SHOW_SEARCH_IN_AUTOFILL = "autofill_search"
        private const val DEV_TOGGLE_SHOW_ADD_NEW_IN_AUTOFILL = "autofill_add_new"
        private const val DEV_TOGGLE_SHOW_SIDE_ICON_IN_AUTOFILL = "autofill_side_icon"
        private const val DEV_TOGGLE_AUTOSAVE_FLAG = "autosave_flag"
        private const val DEV_TOGGLE_DEBUG_AUTOSAVE = "show_debug_autosave"
        private const val DEV_TOGGLE_DEBUG_AUTOFILL = "show_debug_autofill"
        private const val DEV_SIMULATE_RESPONSES = "simulate_responses"
        private const val DEV_SIMULATE_ERROR_RESPONSE_ITEMS_LIST =
            "simulate_error_response_items_list"
        private const val DEV_SIMULATE_ERROR_RESPONSE_ITEM_DETAILS =
            "simulate_error_response_item_details"

        private const val AUTH_JWT = "jwt_token_v3"
        private const val AUTH_ACCESS_TOKEN = "access_token_v3"
        private const val AUTH_REFRESH_TOKEN = "refresh_token_v3"
        const val AUTH_LEGACY_PRIVATE_KEY = "private_key"
        const val AUTH_LEGACY_PUBLIC_KEY = "public_key"
        const val AUTH_LIBSODIUM_PRIVATE_KEY = "ls_private_key"
        const val AUTH_LIBSODIUM_PUBLIC_KEY = "ls_public_key"

        private const val SKIP_SECURE_PREFS = "skip_secure_prefs"

        // For analytics only
        private const val IS_AUTOFILL_ENABLED = "autofill_enabled"
        private const val HAS_AUTOFILLED = "has_autofilled"
        private const val AUTOFILL_COUNT = "autofill_count"
        private const val IS_FIRST_LAUNCH = "is_first_launch"

        // For test drive only. Legacy fields (since 3.13)
        const val LOCAL_TEST_DRIVE_ACCOUNT = "local_test_drive_account"
        const val LOCAL_TEST_DRIVE_NOTE = "local_test_drive_note"
        const val LOCAL_TEST_DRIVE_CREDIT_CARD = "local_test_drive_credit_card"
        const val LOCAL_TEST_DRIVE_WIFI = "local_test_drive_wifi"

        // For test drive only
        const val TEST_DRIVE_MIGRATION_PASSED = "test_drive_migration_passed"

        fun initCryptoSafely(
            keyPair: KeyPairString?,
            legacyKeyPair: KeyPairString?,
            crypto: Crypto
        ): Boolean {
            var ok = false
            // Set libsodium keypair if present
            if (keyPair?.public != null && keyPair.private != null) {
                crypto.setKeypairFromString(
                    secret = keyPair.private!!,
                    pub = keyPair.public!!
                )
                ok = true
            }
            // Set legacy keypair if present
            if (!legacyKeyPair?.private.isNullOrEmpty() && !legacyKeyPair?.public.isNullOrEmpty()) {
                crypto.setLegacyKeypairFromString(
                    priv = legacyKeyPair?.private!!,
                    pub = legacyKeyPair.public!!
                )
                ok = true
            }

            if (!ok) {
                // According to Sentry, this status is (or used to be?) possible.
                // We need to get to the bottom of it:
                val privStatus = when {
                    legacyKeyPair?.private == null -> "null"
                    legacyKeyPair.private?.isEmpty() == true -> "empty"
                    else -> "normal"
                }
                val pubStatus = when {
                    legacyKeyPair?.public == null -> "null"
                    legacyKeyPair.public?.isEmpty() == true -> "empty"
                    else -> "normal"
                }
                Sentry.captureMessage(
                    "Trying to .initCryptoSafely with null or empty keys: priv=$privStatus and pub=$pubStatus",
                    SentryLevel.DEBUG
                )
            }
            return ok
        }
    }

    private val preferences: SharedPreferences =
        if (VersionUtil.supportsEncryptedPreferences()
            || !context.getSharedPreferences(pref_name, Context.MODE_PRIVATE)
                .getBoolean(SKIP_SECURE_PREFS, false)
        ) {
            try {
                EncryptedSharedPreferences.create(
                    pref_name,
                    MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
                    context,
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                )
            } catch (e: KeyStoreException) {
                skipSecurePrefs(context)
            } catch (e: GeneralSecurityException) {
                skipSecurePrefs(context)
            } catch (e: IOException) {
                skipSecurePrefs(context)
            } catch (e: FileNotFoundException) {
                skipSecurePrefs(context)
            }
        } else {
            skipSecurePrefs(context)
        }

    /**
     * Some devices (Samsung!) just don't seem to be able to play well with the secure prefs.
     * For those devices, we catch the exception once, then store an (unencrypted) flag that
     * we'll stop using the secure prefs.
     * Regular prefs are secure anyway, just not encrypted. Which should only matter for
     * rooted devices.
     */
    private fun skipSecurePrefs(context: Context): SharedPreferences {
        val prefs = context.getSharedPreferences(pref_name, Context.MODE_PRIVATE)
        prefs.edit().putBoolean(SKIP_SECURE_PREFS, true).apply()
        return prefs
    }

    fun removePrefByKey(key: String) {
        preferences.edit().remove(key).apply()
    }

    @SuppressLint("ApplySharedPref")
    fun clear(logoutOnly: Boolean) {
        Breadcrumb.drop(Breadcrumb.CryptoTrail_ClearPrefs)

        // It is easier to clean all, and copy a few, then to clean all-but-a-few
        val lastAppVersionBackup = appVersion
        val finishedOnboardingBackup = hasFinishedOnboarding

        // We force a commit here instead of apply, after observing that the logout went
        // haywire (it checks if the firebaseUserId is null, clears everything and
        // restarts, but the restarting was faster than clearing the firebaseUserId,
        // resulting in an interesting visual mess.
        preferences.edit().clear().commit()

        // Restore backup
        if (logoutOnly) {
            appVersion = lastAppVersionBackup
            hasFinishedOnboarding = finishedOnboardingBackup
        }
    }

    fun isLoggedIn(): Boolean = !accessToken.isNullOrEmpty()

    /**
     * Set experimental mixpanel variable
     */
    fun setExperimentalIntPref(experimentalKey: ExperimentalKey, value: Int) {
        preferences.edit().putInt(experimentalKey.key, value).apply()
    }

    fun getExperimentalIntPref(experimentalKey: ExperimentalKey): Int? {
        val result = preferences.getInt(experimentalKey.key, -1)
        if (result == -1) {
            return null
        }
        return result
    }

    var appVersion: Int
        get() = preferences.getInt(LAST_APP_VERSION, 0)
        set(value) = preferences.edit().putInt(LAST_APP_VERSION, value).apply()

    var accessToken: String?
        get() = preferences.getString(AUTH_ACCESS_TOKEN, null)
        set(value) = preferences.edit().putString(AUTH_ACCESS_TOKEN, value).apply()
    var refreshToken: String?
        get() = preferences.getString(AUTH_REFRESH_TOKEN, null)
        set(value) = preferences.edit().putString(AUTH_REFRESH_TOKEN, value).apply()
    var jwtToken: String?
        get() = preferences.getString(AUTH_JWT, null)
        set(value) = preferences.edit().putString(AUTH_JWT, value).apply()
    var firebaseUserId: String?
        get() = preferences.getString(USER_ID_PREF_KEY, null)
        set(value) = preferences.edit().putString(USER_ID_PREF_KEY, value).apply()
    var mixpanelUserId: String?
        get() = preferences.getString(MIXPANEL_USER_ID_PREF_KEY, null)
        set(value) = preferences.edit().putString(MIXPANEL_USER_ID_PREF_KEY, value).apply()

    // Added in 3.1.6, null for everyone who logged in before that version
    var email: String?
        get() = preferences.getString(EMAIL_PREF_KEY, null)
        set(value) = preferences.edit().putString(EMAIL_PREF_KEY, value).apply()

    var mayAskForAutofill: Boolean
        get() = !preferences.getBoolean(AUTOFILL_DONT_ASK_AGAIN, false)
        set(value) = preferences.edit().putBoolean(AUTOFILL_DONT_ASK_AGAIN, !value).apply()

    var isAutofillEnabled: Boolean?
        get() = if (preferences.contains(IS_AUTOFILL_ENABLED)) {
            preferences.getBoolean(IS_AUTOFILL_ENABLED, false)
        } else null
        set(value) = preferences.edit().putBoolean(IS_AUTOFILL_ENABLED, value ?: false).apply()

    var autofillCount: Int
        get() {
            // We use this construction as a migration from the previously
            // used boolean HAS_AUTOFILLED
            if (preferences.contains(AUTOFILL_COUNT)) {
                return preferences.getInt(AUTOFILL_COUNT, 0)
            } else if (preferences.getBoolean(HAS_AUTOFILLED, false)) {
                return 1
            }
            return 0
        }
        set(value) {
            preferences.edit().putInt(AUTOFILL_COUNT, value).apply()
        }
    var isFirstLaunch: Boolean
        get() = preferences.getBoolean(IS_FIRST_LAUNCH, true)
        set(value) = preferences.edit().putBoolean(IS_FIRST_LAUNCH, value).apply()

    var localTestDriveLegacyMigrationFinished: Boolean
        get() = preferences.getBoolean(TEST_DRIVE_MIGRATION_PASSED, false)
        set(value) = preferences.edit().putBoolean(TEST_DRIVE_MIGRATION_PASSED, value).apply()

    /**
     * Checks if the biometric lock is enabled. This is a user setting.
     */
    var isLockEnabled: Boolean
        get() = preferences.getBoolean(BIO_LOCK_ENABLED, false)
        set(value) = preferences.edit().putBoolean(BIO_LOCK_ENABLED, value).apply()

    /**
     * Timestamp when the app was last exited. The LockUtil uses this to determine if
     * the app should be locked again
     */
    var lastAppExit: Long
        get() = preferences.getLong(BIO_LOCK_LAST_EXIT, 0L)
        set(value) = preferences.edit().putLong(BIO_LOCK_LAST_EXIT, value).apply()

    var hasFinishedOnboarding: Boolean
        get() = preferences.getBoolean(HAS_FINISHED_ONBOARDING, false)
        set(value) = preferences.edit().putBoolean(HAS_FINISHED_ONBOARDING, value).apply()

    /**
     * The user has selected to move further without adding the details of one of the
     * suggested accounts.
     */
//    var skipFirstAccount: Boolean
//        get() = preferences.getBoolean(SKIP_FIRST_ACCOUNT, false)
//        set(value) = preferences.edit().putBoolean(SKIP_FIRST_ACCOUNT, value).apply()

    /**
     * The user has selected to move further not answering the survey.
     */
    var isSurveyFinished: Boolean
        get() = preferences.getBoolean(SKIP_SURVEY, false)
        set(value) = preferences.edit().putBoolean(SKIP_SURVEY, value).apply()
    var isDesktopConnected: Boolean
        get() = preferences.getBoolean(DESKTOP_CONNECTED, false)
        set(value) = preferences.edit().putBoolean(DESKTOP_CONNECTED, value).apply()

    /**
     * This field is nullable, because we also want to consider the state where it is not known yet.
     */
    var hasOnboardedBefore: Boolean?
        get() = if (preferences.contains(HAS_ONBOARDED_BEFORE))
                    preferences.getBoolean(HAS_ONBOARDED_BEFORE, false)
                else
                    null
        set(value) = preferences.edit().putBoolean(HAS_ONBOARDED_BEFORE, value ?: true).apply()

    /**
     * We ask to give a rating once and only once
     */
    var hasAskedForRating: Boolean
        get() = preferences.getBoolean(HAS_ASKED_FOR_RATING, false)
        set(value) = preferences.edit().putBoolean(HAS_ASKED_FOR_RATING, value).apply()



    var devToggleEnableInlineAutofill: Boolean
        get() = preferences.getBoolean(DEV_TOGGLE_INLINE_AUTOFILL, true)
        set(value) = preferences.edit().putBoolean(DEV_TOGGLE_INLINE_AUTOFILL, value).apply()
    var devToggleShowSearchInAutofill: Boolean
        get() = preferences.getBoolean(DEV_TOGGLE_SHOW_SEARCH_IN_AUTOFILL, false)
        set(value) = preferences.edit().putBoolean(DEV_TOGGLE_SHOW_SEARCH_IN_AUTOFILL, value)
            .apply()
    var devToggleShowAddNewInAutofill: Boolean
        get() = preferences.getBoolean(DEV_TOGGLE_SHOW_ADD_NEW_IN_AUTOFILL, false)
        set(value) = preferences.edit().putBoolean(DEV_TOGGLE_SHOW_ADD_NEW_IN_AUTOFILL, value)
            .apply()
    var devToggleSideIconAutofill: Boolean
        get() = preferences.getBoolean(DEV_TOGGLE_SHOW_SIDE_ICON_IN_AUTOFILL, true)
        set(value) = preferences.edit().putBoolean(DEV_TOGGLE_SHOW_SIDE_ICON_IN_AUTOFILL, value)
            .apply()
    var devToggleEnableAutosaveFlag: Boolean
        get() = preferences.getBoolean(DEV_TOGGLE_AUTOSAVE_FLAG, false)
        set(value) = preferences.edit().putBoolean(DEV_TOGGLE_AUTOSAVE_FLAG, value).apply()
    var devToggleEnableDebugAutosave: Boolean
        get() = preferences.getBoolean(DEV_TOGGLE_DEBUG_AUTOSAVE, false)
        set(value) = preferences.edit().putBoolean(DEV_TOGGLE_DEBUG_AUTOSAVE, value).apply()
    var devToggleEnableDebugLogsAutofill: Boolean
        get() = preferences.getBoolean(DEV_TOGGLE_DEBUG_AUTOFILL, false)
        set(value) = preferences.edit().putBoolean(DEV_TOGGLE_DEBUG_AUTOFILL, value).apply()
    var devToggleSimulateResponses: Boolean
        get() = preferences.getBoolean(DEV_SIMULATE_RESPONSES, false)
        set(value) = preferences.edit().putBoolean(DEV_SIMULATE_RESPONSES, value).apply()
    var devToggleSimulateErrorResponseItemsList: Boolean
        get() = preferences.getBoolean(DEV_SIMULATE_ERROR_RESPONSE_ITEMS_LIST, false)
        set(value) = preferences.edit().putBoolean(DEV_SIMULATE_ERROR_RESPONSE_ITEMS_LIST, value)
            .apply()
    var devToggleSimulateErrorResponseItemDetails: Boolean
        get() = preferences.getBoolean(DEV_SIMULATE_ERROR_RESPONSE_ITEM_DETAILS, false)
        set(value) = preferences.edit().putBoolean(DEV_SIMULATE_ERROR_RESPONSE_ITEM_DETAILS, value)
            .apply()

    suspend fun updateLegacyPrivateKey(legacyPrivateKey: String?) {
        dataStore.edit { preferences ->
            Breadcrumb.drop(
                if (legacyPrivateKey == null)
                    Breadcrumb.CryptoTrail_Setting_Priv_To_Null
                else
                    Breadcrumb.CryptoTrail_Setting_Priv_To_Something
            )
            preferences[PreferencesKeys.KEY_LEGACY_AUTH_PRIVATE] = legacyPrivateKey ?: ""
        }
    }

    suspend fun updatePrivateKey(privateKey: String?) {
        dataStore.edit { preferences ->
            preferences[PreferencesKeys.KEY_AUTH_PRIVATE] = privateKey ?: ""
        }
    }

    suspend fun updateLegacyPublicKey(legacyPublicKey: String?) {
        dataStore.edit { preferences ->
            Breadcrumb.drop(
                if (legacyPublicKey == null)
                    Breadcrumb.CryptoTrail_Setting_Pub_To_Null
                else
                    Breadcrumb.CryptoTrail_Setting_Pub_To_Something
            )
            preferences[PreferencesKeys.KEY_LEGACY_AUTH_PUBLIC] = legacyPublicKey ?: ""
        }
    }

    suspend fun updatePublicKey(publicKey: String?) {
        dataStore.edit { preferences ->
            preferences[PreferencesKeys.KEY_AUTH_PUBLIC] = publicKey ?: ""
        }
    }

    suspend fun clearAuthKeys() {
        dataStore.edit { preferences ->
            // Delete
            preferences.clear()
            // Extra delete
            preferences.remove(PreferencesKeys.KEY_AUTH_PRIVATE)
            preferences.remove(PreferencesKeys.KEY_AUTH_PUBLIC)
            preferences.remove(PreferencesKeys.KEY_LEGACY_AUTH_PRIVATE)
            preferences.remove(PreferencesKeys.KEY_LEGACY_AUTH_PUBLIC)
        }
    }

    fun getString(key: String): String? = preferences.getString(key, null)

    fun debug() {
        Log.i(TAG, "=== SharedPrefs Details ========================")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Log.i(TAG, "Shared preferences:")
            preferences.all.forEach { (key, value) ->
                Log.i(TAG, "  $key (${value?.javaClass?.name}) = $value")
            }
            Log.i(TAG, "Datastore:")
            dataStore.data.map { prefs ->
                prefs.asMap().forEach { (key, value) ->
                    Log.i(TAG, "  $key (${value.javaClass.name}) = $value")
                }
            }
        }
        Log.i(TAG, "================================================")
    }

    val authKeys: Flow<KeyPairsString> = dataStore.data.catch { exception ->
        if (exception is IOException) {
            emit(emptyPreferences())
        } else {
            throw exception
        }
    }.map { preferences ->
        val legacyKeyPriv = preferences[PreferencesKeys.KEY_LEGACY_AUTH_PRIVATE]
        val legacyKeyPub = preferences[PreferencesKeys.KEY_LEGACY_AUTH_PUBLIC]
        val keyPriv = preferences[PreferencesKeys.KEY_AUTH_PRIVATE]
        val keyPub = preferences[PreferencesKeys.KEY_AUTH_PUBLIC]
        KeyPairsString(
            legacy = KeyPairString(
                if (!legacyKeyPriv.isNullOrEmpty()) {
                    legacyKeyPriv
                } else {
                    null
                }, if (!legacyKeyPub.isNullOrEmpty()) {
                    legacyKeyPub
                } else {
                    null
                }
            ),
            keypair = KeyPairString(
                if (!keyPriv.isNullOrEmpty()) {
                    keyPriv
                } else {
                    null
                }, if (!keyPub.isNullOrEmpty()) {
                    keyPub
                } else {
                    null
                }
            )
        )
    }
}
