package com.tilig.android.utils.lock

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.biometric.BiometricManager
import com.tilig.android.utils.SharedPrefs

object LockUtilFactory {

    fun create(prefs: SharedPrefs): LockUtil =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            LockUtilPie(prefs)
        } else {
            LockUtilCompat(prefs)
        }

    /**
     * Checks if biometrics are done using fingerprint or face id.
     * Might be wrong.
     * Returns null if the answer is unknown.
     */
    // Note that we didn't put this method in the LockUtil or the LockUtilCompat.
    // That is so we don't have to instantiate an object (with a reference to SharedPrefs)
    // just to look at what the system supports.
    fun isBiometricsUsingFingerprint(context: Context): Boolean? {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            if (context.packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)) {
                return true
            } else if (
                context.packageManager.hasSystemFeature(PackageManager.FEATURE_FACE)
                || context.packageManager.hasSystemFeature(PackageManager.FEATURE_IRIS)
            ) {
                return false
            }
            return null
        } else {
            val bm = BiometricManager.from(context)

            // TODO in the future we might look at these localized strings.
            // On Samsung SDK 30 without fingerprint the values are null,null,null; but on Pixel 2 SDK 31 with fingerprint they are
            // "Use fingerprint"
            // "Use fingerprint to continue"
            // "Use fingerprint"
//            val strStrong = bm.getStrings(BiometricManager.Authenticators.BIOMETRIC_STRONG)
//            val strWeak = bm.getStrings(BiometricManager.Authenticators.BIOMETRIC_WEAK)
//            Log.d("BiometricvsString", "Biometrics STRONG: ${strStrong?.buttonLabel}, ${strStrong?.promptMessage}, ${strStrong?.settingName}")
//            Log.d("BiometricvsString", "Biometrics WEAK: ${strWeak?.buttonLabel}, ${strWeak?.promptMessage}, ${strWeak?.settingName}")

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val fingerprintStatus = bm.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG)
                val hasFingerprint = when (fingerprintStatus) {
                    BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE,
                    BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE,
                    BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED -> false
                    BiometricManager.BIOMETRIC_SUCCESS,
                    BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> true
                    else -> true
                }
                if (hasFingerprint) {
                    return true
                }
                val faceIdStatus = bm.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_WEAK)
                val hasFaceId = when (faceIdStatus) {
                    BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE,
                    BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE,
                    BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED -> false
                    BiometricManager.BIOMETRIC_SUCCESS,
                    BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> true
                    else -> true
                }
                if (hasFaceId) {
                    return false
                }
                return null
            }
        }
        return null
    }
}
