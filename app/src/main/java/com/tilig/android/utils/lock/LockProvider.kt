package com.tilig.android.utils.lock

import androidx.fragment.app.FragmentActivity

interface LockProvider {
    fun shouldShowScreenUnlock(): Boolean
    fun maybeShowScreenUnlock(
        activity: FragmentActivity,
        listener: LockEventListener,
        requestCode: Int
    )

    fun onAuthorizationEvent(resultCode: Int): Boolean
    fun isLockEnabled(): Boolean
}
