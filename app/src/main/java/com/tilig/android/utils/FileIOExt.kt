package com.tilig.android.utils

import java.io.BufferedReader
import java.io.InputStream

/**
 * Opens an InputStream to the specified path.
 * Pro tip: By prefixing `raw/` or `res/raw/` to the String you can load files from res/raw without a Context.
 */
fun String.openStream(clz: Any) = clz.javaClass.classLoader?.getResourceAsStream(this)

fun InputStream.readFile() = this.bufferedReader().use(BufferedReader::readText)

fun String.readFile(clz: Any) = this.openStream(clz)?.readFile()
