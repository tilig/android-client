package com.tilig.android.utils.lock

import androidx.biometric.BiometricPrompt
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import kotlinx.coroutines.flow.*
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

data class LockViewModelState(
    val isLocked: Boolean = false,
    val tooManyAttempts: Boolean = false
) {
    fun toUiState() = this
}

class LockViewModel : ViewModel(), KoinComponent, LockEventListener {

    private val lockState = MutableStateFlow(LockViewModelState(true))
    val lockUtil: LockUtil by inject()
    val tracker: Mixpanel by inject()

    // UI state exposed to the UI
    val uiState = lockState
        .map { it.toUiState() }
        .stateIn(
            viewModelScope,
            SharingStarted.Eagerly,
            lockState.value.toUiState()
        )

    override fun onAppUnlocked() {
        lockState.update {
            it.copy(isLocked = false, tooManyAttempts = false)
        }
    }

    override fun onAppStartLocked() {
        lockState.update {
            it.copy(isLocked = true, tooManyAttempts = false)
        }
    }

    override fun onAppUnlockFailed(errorCode: Int) {
        val tooManyAttempts = errorCode == BiometricPrompt.ERROR_LOCKOUT || errorCode == BiometricPrompt.ERROR_LOCKOUT_PERMANENT
        tracker.trackEvent(Tracker.EVENT_BIOMETRICS_UNLOCK_CANCELED)
        lockState.update {
            it.copy(
                isLocked = true,
                tooManyAttempts = tooManyAttempts
            )
        }
    }
}
