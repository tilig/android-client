package com.tilig.android.utils.lock

import android.app.KeyguardManager
import android.content.Context
import androidx.fragment.app.FragmentActivity
import com.tilig.android.R
import com.tilig.android.utils.SharedPrefs

class LockUtilCompat internal constructor(prefs: SharedPrefs) : LockUtil(prefs) {

    override fun canAuthorize(context: Context): Boolean = false

    override fun areBiometricsSupported(context: Context): Boolean = false

    /**
     * Starts the verification flow if needed.
     */
    @Suppress("DEPRECATION")
    override fun authorize(
        activity: FragmentActivity,
        listener: LockEventListener,
        requestCode: Int,
        retryWithPincode: Boolean
    ) {
        val km = activity.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager?
        val keyguardIntent = km?.createConfirmDeviceCredentialIntent(
            activity.getString(R.string.app_name), ""
        )
        keyguardIntent?.let { activity.startActivityForResult(it, requestCode) }
    }
}
