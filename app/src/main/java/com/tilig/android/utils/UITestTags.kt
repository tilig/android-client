package com.tilig.android.utils

object UITestTags {
    const val onboardingProgressTestTag = "onboardingProgress"
    const val onboardingNextButtonTag = "onboardingNextBtn"
    const val onboardingSignInTestTag = "onboardingSignIn"
    const val onboardingMoreInfoBtnTestTag = "onboardingMoreInfoBtn"
    const val onboardingMoreInfoModalTestTag = "onboardingMoreInfoModal"

    const val accountsListTestTag = "accountsList"
    const val lockedStateTestTag = "lockedState"

    const val fabButtonTestTag = "fabButton"

    const val suggestedBrandsScreenTestTag = "suggestedBrands"
    const val suggestedBrandsListTestTag = "suggestedBrandsList"
    const val createAccountScreenTestTag = "createAccountScreen"

    const val circularProgressIndicatorTestTag = "circularProgressIndicator"
    const val passwordFieldTestTag = "passwordField"
    const val passwordInputFieldTestTag = "passwordInputField"
    const val slotTextTestTag = "slotText"

}