package com.tilig.android.utils

import android.content.ActivityNotFoundException
import android.util.Log
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import androidx.navigation.NavOptionsBuilder
import io.sentry.Sentry
import io.sentry.SentryLevel

inline fun <T, R> T?.elseLet(block: () -> R) {
    if (this == null) {
        block()
    }
}

/**
 * Use this method wisely!
 * It catches a certain, random navigation error, one that seems
 * to occurs only once in a while, and always on the client's phone
 * and never on ours:
 * IllegalArgumentException, navigation destination <x> is unknown to this NavController
 *
 * According to [the internet](https://blog.jakelee.co.uk/resolving-crash-illegalargumentexception-x-is-unknown-to-this-navcontroller/)
 * this is most likely to occur when a user taps a navigation button twice
 * rapidly. We tried to prevent this using the OnDebounceClickListener,
 * but it still happened.
 *
 * Therefore we catch the error and log it.
 * We also check if the source of the navigation event is still
 * the one we're expecting. If not, apparently the navigation is already
 * in progress and we're about to make the mistake of doing it a second time.
 *
 * @param directions directions that describe this navigation operation
 */
fun NavController.safeNavigate(sourceId: Int, directions: NavDirections) {
    if (sourceId == this.currentDestination?.id) {
        try {
            navigate(directions.actionId, directions.arguments)
        } catch (e: IllegalArgumentException) {
            Log.e("NavError", "Impossible navigation detected", e)
        } catch (e: IllegalStateException) {
            Log.e("NavError", "Impossible navigation detected", e)
        }
    } else {
        val e = RuntimeException("safeNavigate detected mismatching source")
        Log.e(
            "NavError",
            "Impossible navigation prevented: source is no longer the expected one",
            e
        )
    }
}

fun NavController.safeNavigate(sourceId: Int, targetId: Int) {
    if (sourceId == this.currentDestination?.id) {
        try {
            navigate(targetId)
        } catch (e: IllegalArgumentException) {
            Log.e("NavError", "Impossible navigation detected", e)
        } catch (e: IllegalStateException) {
            Log.e("NavError", "Impossible navigation detected", e)
        }
    } else {
        val e = RuntimeException("safeNavigate detected mismatching source")
        Log.e(
            "NavError",
            "Impossible navigation prevented: source is no longer the expected one",
            e
        )
    }
}

fun NavController.safeNavigate(source: String, route: String) {
    if (source == this.currentDestination?.route) {
        try {
            navigate(route)
        } catch (e: IllegalArgumentException) {
            Log.e("NavError", "Impossible navigation detected", e)
        } catch (e: IllegalStateException) {
            Log.e("NavError", "Impossible navigation detected", e)
        }
    } else {
        val e = RuntimeException("safeNavigate detected mismatching source")
        Log.e(
            "NavError",
            "Impossible navigation prevented: source is no longer the expected one",
            e
        )
    }
}

fun NavController.catchNavigate(route: String, builder: NavOptionsBuilder.() -> Unit) {
    try {
        navigate(route, builder)
    } catch (e: IllegalArgumentException) {
        Log.e("NavError", "Impossible navigation detected", e)
    } catch (e: IllegalStateException) {
        Log.e("NavError", "Impossible navigation detected", e)
    }
}

fun NavController.catchNavigate(route: String) {
    try {
        navigate(route)
    } catch (e: IllegalArgumentException) {
        Log.e("NavError", "Impossible navigation detected", e)
    } catch (e: IllegalStateException) {
        Log.e("NavError", "Impossible navigation detected", e)
    }
}

fun NavController.catchNavigate(directions: NavDirections) {
    try {
        navigate(directions)
    } catch (e: IllegalArgumentException) {
        Log.e("NavError", "Impossible navigation detected", e)
    } catch (e: IllegalStateException) {
        Log.e("NavError", "Impossible navigation detected", e)
    }
}

fun NavController.catchNavigate(directions: NavDirections, options: NavOptions) {
    try {
        navigate(directions, options)
    } catch (e: IllegalArgumentException) {
        Log.e("NavError", "Impossible navigation detected", e)
    } catch (e: IllegalStateException) {
        Log.e("NavError", "Impossible navigation detected", e)
    }
}

/**
 * This method exists to catch error ANDROID-APP-7J,
 * which might be a bug in Compose, since we're doing everything by the book
 * and the error appears to be meant for 'old school' Android only.
 */
fun <I, O> ManagedActivityResultLauncher<I, O>.launchForResultSafely(input: I) : Boolean {
    try {
        this.launch(input)
        return true
    } catch (e: java.lang.IllegalStateException) {
        Sentry.captureMessage(
            "ActivityResultLauncher.launch failed for ${this::class.java.simpleName}: ${e.message}",
            SentryLevel.INFO
        )
    } catch (e: ActivityNotFoundException) {
        // This sometimes happens according to Sentry.
        Log.e("X", "launchForResultSafely failed with: ${e.message}", e)
    }
    return false
}
