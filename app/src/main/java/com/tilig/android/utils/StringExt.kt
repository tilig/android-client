package com.tilig.android.utils

import android.net.Uri
import android.util.Patterns
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull

fun String?.ellipsize(maxLength: Int, cropLength: Int): String? {
    if (cropLength > maxLength) {
        throw IllegalArgumentException("cropLength should be shorter than maxLength")
    }
    if (cropLength < 1) {
        throw IllegalArgumentException("cropLength should be at least 1")
    }
    return when {
        this.isNullOrEmpty() -> {
            this
        }
        this.length < maxLength -> {
            this
        }
        else -> {
            this.substring(0, cropLength) + "…"
        }
    }
}

fun String.isValidIP(): Boolean =
    this.isNotEmpty() && Patterns.IP_ADDRESS.matcher(this).matches()

fun String?.getDomain(): String? = if (this.isNullOrBlank()) {
    null
} else {
    // We 'httpify' the link first, otherwise Uri.parse does not accept it
    Uri.parse(this.httpify())?.host
}

fun String?.removePredefinedPrefixesFromDomain(): String? =
        this?.httpify()?.toHttpUrlOrNull()?.topPrivateDomain()

/*
fun String?.removePredefinedPrefixesFromDomain(): String? {
    if (this == null) {
        return null
    }
//    listOf("www.", "m.").forEach {
         avoid removing possible second prefix( like www.m.domain)
//        if (this.startsWith(it)) {
//            return this.removePrefix(it)
//        }
//    }
//    return this
    return PublicSuffixDatabase.get().getEffectiveTldPlusOne(this)
} */

fun String?.httpify(): String? = when {
    this.isNullOrBlank() -> {
        null
    }
    this.contains("://") -> {
        this
    }
    else -> {
        "https://$this"
    }
}

fun String?.isValidEmail() = !this.isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun String?.isValidUrl(): Boolean {
    if (this.isNullOrBlank()) {
        return false
    }
    if (this.contains(" ")) {
        return false
    }
    val host = try {
        java.net.URI(this.httpify()).host
    } catch (e: Exception) {
        null
    }
    return host != null
}