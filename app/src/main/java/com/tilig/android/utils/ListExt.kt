package com.tilig.android.utils

import org.json.JSONObject
import java.util.*

fun <T> List<T>.toArrayList(): ArrayList<T> =
    if (this is ArrayList) {
        this
    } else {
        ArrayList(this)
    }

fun <T> List<T>?.toArrayListOrEmpty(): ArrayList<T> =
    when {
        this == null -> ArrayList()
        this is ArrayList -> this
        else -> ArrayList(this)
    }

/**
 * Can turn ordinary data into a JSONObject in a simple way.
 * Source: https://stackoverflow.com/a/41863640
 * Usage:
 * ```
 * json {
 *   "name" to "Mario"
 *   "age" to 37
 *   "contact" to json {
 *    "city" to "istanbul"
 *    "email" to "xxx@yyy.com"
 *   }
 * }
 * ```
 */
fun json(build: JsonObjectBuilder.() -> Unit): JSONObject {
    return JsonObjectBuilder().json(build)
}

class JsonObjectBuilder {
    private val deque: Deque<JSONObject> = ArrayDeque()

    fun json(build: JsonObjectBuilder.() -> Unit): JSONObject {
        deque.push(JSONObject())
        this.build()
        return deque.pop()
    }

    infix fun <T> String.to(value: T) {
        deque.peek()?.put(this, value)
    }
}
