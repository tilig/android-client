package com.tilig.android.utils

import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.input.OffsetMapping
import androidx.compose.ui.text.input.TransformedText
import androidx.compose.ui.text.input.VisualTransformation
import com.tilig.android.data.models.tilig.CardType

object CardUtils {

    private const val SPACE = " "
    const val BULLET = '\u2022'

    fun identifyCardType(cardNumber: String): CardType {
        val jcbRegex = Regex("^(?:2131|1800|35)[0-9]{0,}$")
        val ameRegex = Regex("^3[47][0-9]{0,}\$")
        val dinersRegex = Regex("^3(?:0[0-59]{1}|[689])[0-9]{0,}\$")
        val visaRegex = Regex("^4[0-9]{0,}\$")
        val masterCardRegex = Regex("^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[01]|2720)[0-9]{0,}\$")
        val maestroRegex = Regex("^(5[06789]|6)[0-9]{0,}\$")
        val discoverRegex =
            Regex("^(6011|65|64[4-9]|62212[6-9]|6221[3-9]|622[2-8]|6229[01]|62292[0-5])[0-9]{0,}\$")

        val trimmedCardNumber = cardNumber.replace(" ", "")

        return when {
            trimmedCardNumber.matches(jcbRegex) -> CardType.JCB
            trimmedCardNumber.matches(ameRegex) -> CardType.AMEX
            trimmedCardNumber.matches(dinersRegex) -> CardType.DINERS_CLUB
            trimmedCardNumber.matches(visaRegex) -> CardType.VISA
            trimmedCardNumber.matches(masterCardRegex) -> CardType.MASTERCARD
            trimmedCardNumber.matches(discoverRegex) -> CardType.DISCOVER
            trimmedCardNumber.matches(maestroRegex) -> if (cardNumber[0] == '5') CardType.MASTERCARD else CardType.MAESTRO
            else -> CardType.UNKNOWN
        }
    }

    fun formatCardNumber(cardNumber: AnnotatedString): TransformedText =
        when (identifyCardType(cardNumber.text)) {
            CardType.AMEX -> {
                formatAmex(cardNumber)
            }
            CardType.DINERS_CLUB -> {
                formatDinnersClub(cardNumber)
            }
            else -> {
                formatOtherCardNumbers(cardNumber)
            }
        }

    class CardNumberTransformation(_mask: Char?) : VisualTransformation {
        private val mask = _mask

        override fun filter(text: AnnotatedString): TransformedText {
            return maskCardNumber(text.text, mask)
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is androidx.compose.ui.text.input.PasswordVisualTransformation) return false
            if (mask != other.mask) return false
            return true
        }

        override fun hashCode(): Int {
            return mask.hashCode()
        }
    }

    fun getLast4CardDigits(cardNumber: String): String =
        if (cardNumber.length < 4) cardNumber else cardNumber.substring(
            cardNumber.length - 4,
            cardNumber.length
        )

    fun emptyNumber(): TransformedText = formatOtherCardNumbers(AnnotatedString(""))

    fun maskCardNumber(value: String, mask: Char?): TransformedText {
        val onlyNumbers = value.replace(Regex(SPACE), "")
        val offset = if (onlyNumbers.length < 4) 0 else 4
        return formatCardNumber(
            AnnotatedString(
                mask?.toString()?.repeat(onlyNumbers.length - offset)?.plus(
                    onlyNumbers.subSequence(
                        onlyNumbers.length - offset,
                        onlyNumbers.length
                    )
                ) ?: onlyNumbers
            )
        )
    }

    fun isValidCardNumber(value: String): Boolean {
        var checksum = 0
        for (i in value.length - 1 downTo 0 step 2) {
            checksum += value[i] - '0'
        }
        for (i in value.length - 2 downTo 0 step 2) {
            val n: Int = (value[i] - '0') * 2
            checksum += if (n > 9) n - 9 else n
        }
        return checksum % 10 == 0
    }

    fun isValidExpireDate(value: String) = formatExpireDate(AnnotatedString(value)).text.length == 5

    fun addSeparatorIfCanToCardExpireDate(expireDate: String): String =
        if (expireDate.length == 4) StringBuilder().append(expireDate).insert(2, "/")
            .toString() else expireDate

    fun formatExpireDate(expireDate: AnnotatedString): TransformedText {
        val trimmed = if (expireDate.text.replace("/", "").length >= 4)
            expireDate.text.substring(0..3) else expireDate.text.replace("/", "")
        var out = ""

        for (i in trimmed.indices) {
            out += trimmed[i]
            //put - character at 2nd and
            if (i == 1) out += "/"
        }
        val expireOffsetTranslator = object : OffsetMapping {
            override fun originalToTransformed(offset: Int): Int {
                if (offset <= 2) return offset
                if (offset <= 4) return offset + 1
                return 5
            }

            override fun transformedToOriginal(offset: Int): Int {
                if (offset <= 1) return offset
                if (offset <= 4) return offset - 1
                return 4
            }
        }
        return TransformedText(AnnotatedString(out), expireOffsetTranslator)
    }

    private fun formatAmex(text: AnnotatedString): TransformedText {
        val trimmed = if (text.text.length >= 15) text.text.substring(0..14) else text.text
        var out = ""

        for (i in trimmed.indices) {
            out += trimmed[i]
            //        put - character at 3rd and 9th indicies
            if (i == 3 || i == 9 && i != 14) out += SPACE
        }
        //    original - 345678901234564
        //    transformed - 3456 7890123 4564
        //    xxxx xxxxxx xxxxx
        /**
         * The offset translator should ignore the hyphen characters, so conversion from
         *  original offset to transformed text works like
         *  - The 4th char of the original text is 5th char in the transformed text. (i.e original[4th] == transformed[5th]])
         *  - The 11th char of the original text is 13th char in the transformed text. (i.e original[11th] == transformed[13th])
         *  Similarly, the reverse conversion works like
         *  - The 5th char of the transformed text is 4th char in the original text. (i.e  transformed[5th] == original[4th] )
         *  - The 13th char of the transformed text is 11th char in the original text. (i.e transformed[13th] == original[11th])
         */
        val creditCardOffsetTranslator = object : OffsetMapping {
            override fun originalToTransformed(offset: Int): Int {
                if (offset <= 3) return offset
                if (offset <= 9) return offset + 1
                if (offset <= 15) return offset + 2
                return 17
            }

            override fun transformedToOriginal(offset: Int): Int {
                if (offset <= 4) return offset
                if (offset <= 11) return offset - 1
                if (offset <= 17) return offset - 2
                return 15
            }
        }
        return TransformedText(AnnotatedString(out), creditCardOffsetTranslator)
    }

    private fun formatDinnersClub(text: AnnotatedString): TransformedText {
        val trimmed = if (text.text.length >= 14) text.text.substring(0..13) else text.text
        var out = ""

        for (i in trimmed.indices) {
            out += trimmed[i]
            if (i == 3 || i == 9 && i != 13) out += SPACE
        }

        //    xxxx xxxxxx xxxx
        val creditCardOffsetTranslator = object : OffsetMapping {
            override fun originalToTransformed(offset: Int): Int {
                if (offset <= 3) return offset
                if (offset <= 9) return offset + 1
                if (offset <= 14) return offset + 2
                return 16
            }

            override fun transformedToOriginal(offset: Int): Int {
                if (offset <= 4) return offset
                if (offset <= 11) return offset - 1
                if (offset <= 16) return offset - 2
                return 14
            }
        }

        return TransformedText(AnnotatedString(out), creditCardOffsetTranslator)
    }

    private fun formatOtherCardNumbers(text: AnnotatedString): TransformedText {
        val stringWithSeparators = AnnotatedString.Builder().run {
            for (i in text.text.indices) {
                append(text.text[i])
                if (i % 4 == 3) {
                    append(" ")
                }
            }
            toAnnotatedString()
        }

        val creditCardOffsetTranslator = object : OffsetMapping {

            override fun originalToTransformed(offset: Int): Int {
                return offset + (offset / 4)
            }

            override fun transformedToOriginal(offset: Int): Int {
                return offset - (offset / 4)
            }
        }

        return TransformedText(stringWithSeparators, creditCardOffsetTranslator)
    }
}