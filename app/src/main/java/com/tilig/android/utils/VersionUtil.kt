package com.tilig.android.utils

import android.annotation.SuppressLint
import android.os.Build

object VersionUtil {

    /**
     * Checks if the device's OS version supports Autofill.
     * Make sure to, if you use the autofillManager in the BaseActivity or the BaseFragment,
     * to also check if it is actually an instance the AutofillManager class.
     * We've seen many issues in Sentry of devices that do check for this method,
     * and still crash on the AutofillManager references, so we made those references
     * more generic.
     */
    fun supportsAutofill(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O

    fun supportsFootersInAutofill(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.P

    fun supportsCertificateCheck(): Boolean = Build.VERSION.SDK_INT < Build.VERSION_CODES.P

    @SuppressLint("ObsoleteSdkInt")
    fun supportsEncryptedPreferences(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M

    fun supportsInlineAutofill(): Boolean  = Build.VERSION.SDK_INT >= Build.VERSION_CODES.R

    /**
     * Android 13+ shows a clipboard notice automatically. We don't need a custom message in that case
     */
    fun showsSystemMessageOnCopyToClipboard() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU

}
