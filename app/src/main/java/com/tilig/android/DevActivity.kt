package com.tilig.android

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.service.autofill.FillRequest
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.tilig.android.utils.DebugLogger
import kotlin.random.Random

class DevActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_DEBUG_TEXT = "debug_text"

        fun createPendingIntentForAlertText(context: Context, debugText: String): PendingIntent {
            return PendingIntent.getActivity(
                context,
                123,
                createIntent(context, debugText),
                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
            )
        }

        fun createIntent(context: Context, debugText: String): Intent?
            = Intent(context, DevActivity::class.java).apply {
                putExtra(EXTRA_DEBUG_TEXT, debugText)
                // This is a hack that enables us to create two PendingIntents at the same
                // time for the same Activity: one for search, one for creating a new secret
                action = Random.Default.nextInt().toString()
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dev)

        findViewById<TextView>(R.id.tv_alert).text = intent.getStringExtra(EXTRA_DEBUG_TEXT)
        findViewById<ImageButton>(R.id.bt_share).setOnClickListener {
            val sendIntent = Intent(Intent.ACTION_SEND).apply {
                putExtra(Intent.EXTRA_TEXT, intent.getStringExtra(EXTRA_DEBUG_TEXT))
                type = "text/plain"
            }
            startActivity(Intent.createChooser(sendIntent, "Share log with a dev"))
        }
        findViewById<Button>(R.id.bt_clear).setOnClickListener {
            DebugLogger().clear(this@DevActivity)
            findViewById<TextView>(R.id.tv_alert).text = ""
        }
    }
}
