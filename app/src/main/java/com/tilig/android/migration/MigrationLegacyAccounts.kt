package com.tilig.android.migration

import com.tilig.android.data.models.ApiErrorResponse
import com.tilig.android.data.models.ApiSuccessResponse
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.models.tilig.DEFAULT_ACCOUNT_TYPE
import com.tilig.android.data.repository.Repository
import com.tilig.crypto.Crypto
import io.sentry.Sentry
import io.sentry.SentryLevel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MigrationLegacyAccounts(private val repo: Repository) {

    private var migrationInProgress = false

    // migration should be called once for a session
    private var migrationPassed = false

    suspend fun runMigrationLegacyAccounts(
        crypto: Crypto,
        onActionAfterAccountsMigrated: () -> Unit
    ) = withContext(Dispatchers.IO) {
        if (migrationInProgress || migrationPassed) {
            return@withContext
        }
        migrationInProgress = true
//        Sentry.captureMessage(
//            "Logins encryption migration from v1 to v2 started",
//            SentryLevel.INFO
//        )
        val itemsResponse = repo.secretItems()
        if (itemsResponse is ApiSuccessResponse) {
            val legacyItems = itemsResponse.body.items
                .map {
                    it.decrypt(
                        jsonConverter = repo.jsonConverter,
                        crypto,
                        includingDetails = false
                    )
                }
                .filterIsInstance(Account::class.java)
                .filter {
                    it.encryptionVersion == 1 || it.template == null
                }
            legacyItems.forEach { legacyAccount ->
                if (legacyAccount.id == null) {
                    return@forEach
                }

                legacyAccount.decryptDetails(
                    jsonConverter = repo.jsonConverter,
                    crypto
                )
                val passHistoryResponse = repo.fetchPasswordVersionsAsync(legacyAccount.id!!)
                if (passHistoryResponse is ApiSuccessResponse) {
                    passHistoryResponse.body.let {
                        val history = it.mapNotNull { legacyEntry ->
                            legacyEntry.toHistoryEntry(crypto)
                        }
                        // Update the account model with the freshly fetched password history
                        legacyAccount.details.history = history
                    }
                } else if (passHistoryResponse is ApiErrorResponse) {
                    Sentry.captureMessage(
                        "Error occurred during logins encryption migration from v1 to v2. Error password history fetching: ${passHistoryResponse.errorMessage}",
                        SentryLevel.ERROR
                    )
                }

                val migratedEncryptedItem = legacyAccount.encrypt(
                    jsonConverter = repo.jsonConverter,
                    crypto
                )
                migratedEncryptedItem.template = DEFAULT_ACCOUNT_TYPE
                val updateItemResult = repo.updateSecretItem(migratedEncryptedItem)
                if (updateItemResult is ApiErrorResponse) {
                    Sentry.captureMessage(
                        "Error occurred during logins encryption migration from v1 to v2. Error login item update: ${updateItemResult.errorMessage}",
                        SentryLevel.ERROR
                    )
                }
            }
            migrationPassed = true
            if (legacyItems.isNotEmpty()) {
                onActionAfterAccountsMigrated()
            }
        }
        migrationInProgress = false
    }
}