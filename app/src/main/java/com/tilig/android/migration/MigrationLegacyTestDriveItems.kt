package com.tilig.android.migration

import android.util.Log
import com.tilig.android.data.models.ApiErrorResponse
import com.tilig.android.data.models.ApiSuccessResponse
import com.tilig.android.data.models.tilig.*
import com.tilig.android.data.repository.Repository
import com.tilig.android.featureflag.FeatureFlagClient
import com.tilig.android.testdrive.TestDrive
import com.tilig.android.utils.SharedPrefs
import com.tilig.crypto.Crypto
import io.sentry.Sentry
import io.sentry.SentryLevel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

private const val TAG = "MigrationTestDrive"

/**
 * Migrate previous stored test drive items
 * If a user updated it, then send it to the server and convert to a regular item, otherwise - delete
 */
class MigrationLegacyTestDriveItems(
    private val repo: Repository,
    private val pref: SharedPrefs,
    private val featureFlagClient: FeatureFlagClient
) {

    private var migrationInProgress = false

    // migration should be called once for a session
    private var migrationPassed = false

    suspend fun runMigrationLegacyTestDriveItems(
        crypto: Crypto,
        onFinishMigration: () -> Unit
    ) = withContext(Dispatchers.IO) {
        if (migrationInProgress || migrationPassed || pref.localTestDriveLegacyMigrationFinished) {
            return@withContext
        }
        Log.w(TAG, "Started test drive migration")
        migrationInProgress = true
        Sentry.captureMessage(
            "Test Drive legacy migration started",
            SentryLevel.INFO
        )
        val accountItem = pref.getString(SharedPrefs.LOCAL_TEST_DRIVE_ACCOUNT)?.let {
            repo.jsonConverter.adapter(EncryptedItem::class.java).fromJson(it)
        }
        val noteItem = pref.getString(SharedPrefs.LOCAL_TEST_DRIVE_NOTE)?.let {
            repo.jsonConverter.adapter(EncryptedItem::class.java).fromJson(it)
        }
        val cardItem = pref.getString(SharedPrefs.LOCAL_TEST_DRIVE_CREDIT_CARD)?.let {
            repo.jsonConverter.adapter(EncryptedItem::class.java).fromJson(it)
        }
        val wifiItem = pref.getString(SharedPrefs.LOCAL_TEST_DRIVE_WIFI)?.let {
            repo.jsonConverter.adapter(EncryptedItem::class.java).fromJson(it)
        }

        val legacyTestItems = listOfNotNull(accountItem, noteItem, cardItem, wifiItem).map {
            it.decrypt(
                jsonConverter = repo.jsonConverter,
                crypto,
                includingDetails = true
            )
        }
        legacyTestItems.forEach { item ->
            if (TestDrive.isLegacyTestItemWasUpdated(item)) {
                val updatedItem = when (item) {
                    is Account -> item.copy(id = null)
                    is Note -> item.copy(id = null)
                    is WifiPassword -> item.copy(id = null)
                    is CreditCard -> item.copy(id = null)
                    else -> item
                }
                val createResult = repo.createSecretItem(updatedItem, crypto)
                if (createResult is ApiSuccessResponse) {
                    removeLegacyItem(item)
                } else if (createResult is ApiErrorResponse) {
                    Log.w(
                        TAG,
                        "Test Drive legacy migration: failed to create an item based on previously updated legacy item"
                    )
                    Sentry.captureMessage(
                        "Test Drive legacy migration: failed to create an item based on previously updated legacy item",
                        SentryLevel.WARNING
                    )
                }
            } else {
                removeLegacyItem(item)
            }
        }

        val allLegacyItemsMigrated =
            pref.getString(SharedPrefs.LOCAL_TEST_DRIVE_ACCOUNT) == null &&
                    pref.getString(SharedPrefs.LOCAL_TEST_DRIVE_NOTE) == null &&
                    pref.getString(SharedPrefs.LOCAL_TEST_DRIVE_CREDIT_CARD) == null &&
                    pref.getString(SharedPrefs.LOCAL_TEST_DRIVE_WIFI) == null

        // run migration once per app launch, try again later if error
        migrationPassed = true
        Log.d(TAG, "all legacy test drive items were migrated = $allLegacyItemsMigrated")
        pref.localTestDriveLegacyMigrationFinished = allLegacyItemsMigrated
        onFinishMigration.invoke()
        migrationInProgress = false
    }

    private fun removeLegacyItem(item: SecretItem) {
        when (item) {
            is Account -> pref.removePrefByKey(SharedPrefs.LOCAL_TEST_DRIVE_ACCOUNT)
            is Note -> pref.removePrefByKey(SharedPrefs.LOCAL_TEST_DRIVE_NOTE)
            is WifiPassword -> pref.removePrefByKey(SharedPrefs.LOCAL_TEST_DRIVE_WIFI)
            is CreditCard -> pref.removePrefByKey(SharedPrefs.LOCAL_TEST_DRIVE_CREDIT_CARD)
            else -> {}
        }
    }
}