package com.tilig.android.data.models.tilig

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

data class OkResponse(
    val msg: String = ""
)

data class DeleteResponse(
    val msg: String = ""
)

@Parcelize
data class SurveyAnswerResponse(
    val id: String,
    val question: SurveyQuestion,
    @Json(name = "chosen_options")
    val chosenOptions: List<String>
) : Parcelable

@Parcelize
data class SurveyQuestion(
    val id: String,
    val content: String,
    @Json(name = "survey_token")
    val surveyToken: String,
    val token: String,
    @Json(name = "answer_options")
    val answerOptions: List<String>,
    @Json(name = "created_at")
    val createdAt: String,
    @Json(name = "updated_at")
    val updatedAt: String
) : Parcelable