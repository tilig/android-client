package com.tilig.android.data.models

enum class SecretItemType {
    ACCOUNT,
    NOTE,
    CREDIT_CARD,
    WIFI,
    OTHER
}