package com.tilig.android.data.qrscanner

class MalformedTokenException : Exception {
    constructor(message: String?) : super(message) {}
    constructor(message: String?, cause: Throwable?) : super(message, cause) {}
}