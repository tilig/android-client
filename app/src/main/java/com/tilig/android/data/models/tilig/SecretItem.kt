package com.tilig.android.data.models.tilig

import android.os.Parcelable
import android.util.Log
import com.goterl.lazysodium.utils.Key
import com.goterl.lazysodium.utils.KeyPair
import com.squareup.moshi.Json
import com.squareup.moshi.Moshi
import com.tilig.android.data.qrscanner.MalformedTokenException
import com.tilig.android.data.qrscanner.Token
import com.tilig.android.testdrive.TestDrive
import com.tilig.android.utils.DebugLogger
import com.tilig.crypto.Crypto
import kotlinx.parcelize.IgnoredOnParcel

// supported types: Account, Note
interface SecretItem : Parcelable {
    val id: String?
    val createdAt: String?
    val updatedAt: String?
    val encryptionVersion: Int

    var template: String?
    val overview: ItemOverview
    var details: ItemDetails
    var encryptedDetails: String?
    var legacyEncryptionDisabled: Boolean?

    var areDetailsDecrypted: Boolean
    var decryptedDek: ByteArray?
    var encryptedFolderKey: String?
    var folder: Folder?
    var share: ShareLink?

    fun isTestDriveItem() = TestDrive.isTestDriveItem(id)

    /**
     * Checks if this item is actually a shared folder, and not a 'regular' item.
     * Note that this does NOT check if the folder is actually shared with anyone!
     * All other members could have been revoked. Use [hasSharees] for that.
     */
    fun isSharedFolderItem() = encryptedFolderKey != null

    fun hasSharees() = encryptedFolderKey != null && ((folder?.folderInvitations?.size ?: 0) > 1 || (folder?.folderMemberships?.size ?: 0) > 1)

    fun encrypt(jsonConverter: Moshi, crypto: Crypto) =
        EncryptedItem.encrypt(this, jsonConverter, crypto)

    fun decryptDetails(jsonConverter: Moshi, crypto: Crypto, debugLogger: DebugLogger? = null) {
        if (details.isEmpty()) {
            if (encryptedFolderKey != null) {
                debugLogger?.collect("Decrypting using folder dek")
                // User has a private key → used to decrypt the private KEK
                val keyPair = KeyPair(
                    Key.fromBytes(crypto.base64decode(folder!!.publicKey)),
                    Key.fromBytes(crypto.open(folder?.myEncryptedPrivateKey!!, crypto.getKeyPairs().keypair!!))
                )
                // Private key of the KEK is used to decrypt the DEK
                val folderKek = crypto.decryptWithKeypair(encryptedFolderKey!!, keyPair)
                // DEK is used to decrypt the payload itself.
                encryptedDetails?.let {
                    crypto.decryptUsingDek(folderKek!!, it)?.let { decryptedDetails ->
                        Log.d("Crypto", "    DECRYPTED DETAILS: $decryptedDetails")
                        debugLogger?.collect("Decrypted details: \n$decryptedDetails")
                        val detailsObj =
                            jsonConverter.adapter(ItemDetails::class.java)
                                .fromJson(decryptedDetails)
                        details = detailsObj ?: ItemDetails.createEmpty()
                    }
                }
            } else if (decryptedDek == null && !encryptedDetails.isNullOrEmpty()) {
                // Sentry.captureMessage("Broken keypair")
                Log.e(
                    "Secret Item",
                    "Cannot decrypt details, broken keypair? ${Log.getStackTraceString(Throwable())}"
                )
                debugLogger?.collect("Cannot decrypt details, broken keypair?")
                encryptedDetails = null
            } else {
                encryptedDetails?.let {
                    crypto.decryptUsingDek(decryptedDek!!, it)?.let { decryptedDetails ->
                        Log.d("Crypto", "    DECRYPTED DETAILS: $decryptedDetails")
                        debugLogger?.collect("Decrypted details: \n$decryptedDetails")
                        val detailsObj =
                            jsonConverter.adapter(ItemDetails::class.java)
                                .fromJson(decryptedDetails)
                        details = detailsObj ?: ItemDetails.createEmpty()
                    }
                }
            }
        }
        areDetailsDecrypted = true
    }

    fun toDebugString(): String

    fun containsMember(email: String): Boolean = folder?.folderInvitations?.any { it.user.email.equals(email, ignoreCase = true) } == true ||
            folder?.folderMemberships?.any { it.user.email.equals(email, ignoreCase = true) } == true

    fun hasOtp(): Boolean = !otp.isNullOrEmpty()

    @IgnoredOnParcel
    @Json(ignore = true)
    var name: String?
        set(value) {
            overview.name = value ?: ""
        }
        get() = overview.name ?: ""

    @IgnoredOnParcel
    @Json(ignore = true)
    var otp: String?
        set(value) {
            assert(areDetailsDecrypted)
            if (value == null) {
                details.clearCustomField(ItemFieldType.TOTP)
            } else {
                details.setCustomField(
                    // TODO Decide on title? Is it used somewhere?
                    "OTP",
                    value,
                    ItemFieldType.TOTP
                )
            }
        }
        get() {
            assert(areDetailsDecrypted)
            return details.findField(ItemFieldType.TOTP)
        }

    fun otpTokenNow(): String? {
        if (hasOtp()) {
            return try {
                val token = Token.parse(otp)
                token.code
            } catch (e: MalformedTokenException) {
                null
            }
        }
        return null
    }

}
