package com.tilig.android.data.api.services

import com.tilig.android.data.models.TokenExchangeResponse
import retrofit2.Call
import retrofit2.http.POST

interface TokenAuthService {

    @POST("v3/authenticate")
    fun authenticateCall(): Call<TokenExchangeResponse>

    @POST("v3/refresh")
    fun refresh(): Call<TokenExchangeResponse>

}
