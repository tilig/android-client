package com.tilig.android.data.api

import okhttp3.Request
import okio.Timeout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MockErrorResponseCall<S : Any>(
    private val delegate: Call<S>,
    private val error: String
) : Call<S> {

    override fun enqueue(callback: Callback<S>) {
        return delegate.enqueue(object : Callback<S> {
            override fun onResponse(call: Call<S>, response: Response<S>) {
                callback.onFailure(
                    this@MockErrorResponseCall,
                    Throwable(error)
                )
            }

            override fun onFailure(call: Call<S>, throwable: Throwable) {
                callback.onFailure(
                    this@MockErrorResponseCall,
                    Throwable(error)
                    //call.execute()
                    //  Response.success(ApiResponse.create(throwable))
                )
            }
        })
    }

    override fun isExecuted() = delegate.isExecuted

    override fun clone() = MockErrorResponseCall(delegate.clone(), "")

    override fun isCanceled() = delegate.isCanceled

    override fun cancel() = delegate.cancel()

    override fun execute(): Response<S> {
        throw UnsupportedOperationException("NetworkResponseCall doesn't support execute")
    }

    override fun request(): Request = delegate.request()

    override fun timeout(): Timeout = delegate.timeout()
}