package com.tilig.android.data.api

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import androidx.annotation.RequiresApi
import com.tilig.android.data.livedata.ConnectivityLiveData

internal sealed class ConnectivityMonitor(
    protected val connectivityManager: ConnectivityManager
) {

    protected var callbackFunction: ((Boolean) -> Unit) = {}
    protected var isListening: Boolean = false

    abstract fun startListening(callback: (Boolean) -> Unit)
    abstract fun stopListening()

    @RequiresApi(Build.VERSION_CODES.N)
    private class NougatConnectivityMonitor(connectivityManager: ConnectivityManager) :
        ConnectivityMonitor(connectivityManager) {

        private val networkCallback = object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                callbackFunction(true)
            }

            override fun onLost(network: Network) {
                super.onLost(network)
                callbackFunction(false)
            }
        }

        override fun startListening(callback: (Boolean) -> Unit) {
            if (!isListening) {
                isListening = true
                callbackFunction = callback
                callbackFunction(false)
                // Note: the TooManyRequestsException thrown here is private so we can't catch
                // it specifically, but the new `if (!isListenering)` should prevent it from
                // happening altogether. If Sentry still reports this issue > 3.3.22, we need
                // a try-catch after all.
                connectivityManager.registerDefaultNetworkCallback(networkCallback)
            }
        }

        override fun stopListening() {
            connectivityManager.unregisterNetworkCallback(networkCallback)
            callbackFunction = {}
            isListening = false
        }
    }

    @Suppress("Deprecation")
    private class LegacyConnectivityMonitor(
        private val context: Context,
        connectivityManager: ConnectivityManager
    ) : ConnectivityMonitor(connectivityManager) {

        private val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        private val isNetworkConnected: Boolean
            get() = connectivityManager.activeNetworkInfo?.isConnected == true

        override fun startListening(callback: (Boolean) -> Unit) {
            if (!isListening) {
                isListening = true
                callbackFunction = callback
                callbackFunction(isNetworkConnected)
                context.registerReceiver(receiver, filter)
            }
        }

        override fun stopListening() {
            context.unregisterReceiver(receiver)
            callbackFunction = {}
            isListening = false
        }

        private val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                callbackFunction(isNetworkConnected)
            }
        }
    }

    companion object {
        fun getInstance(context: Context): ConnectivityMonitor {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                NougatConnectivityMonitor(
                    connectivityManager
                )
            } else {
                LegacyConnectivityMonitor(
                    context,
                    connectivityManager
                )
            }
        }
    }
}
