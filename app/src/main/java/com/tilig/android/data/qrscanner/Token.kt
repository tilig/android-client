package com.tilig.android.data.qrscanner

import android.net.Uri
import com.google.authenticator.blackberry.Base32String
import java.nio.ByteBuffer
import java.security.NoSuchAlgorithmException
import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

/**
 * Model for a single HOTP or TOTP token
 */
class Token {
    private var issuerInt: String? = null
    private var issuerExt: String? = null
    private var label: String? = null
    var type: TokenType? = null
    var algorithm: String? = null
        private set
    private var secret: ByteArray? = null
    var digits: Int = 0
    var count: Long = 0
        private set
    var intervalSec = 0
        private set

    @Transient
    private var mLastCode: Long = 0

    @Transient
    private var mCacheCounter: Long = -1

    @Transient
    private var mCacheCode: String? = null

    private constructor(uri: Uri?) {
        if (uri?.scheme == null || uri.scheme != URI_SCHEME) {
            throw MalformedTokenException("Invalid URI scheme (must be $URI_SCHEME)")
        }

        // HOTP / TOTP
        val authority = uri.authority
        if (URI_AUTHORITY_TOTP == authority) {
            type = TokenType.TOTP
        } else if (URI_AUTHORITY_HOTP == authority) {
            type = TokenType.HOTP
        } else {
            throw MalformedTokenException("Invalid authority $authority (must be $URI_AUTHORITY_HOTP or $URI_AUTHORITY_TOTP)")
        }

        // PATH
        var path = uri.path ?: throw MalformedTokenException("Path is null")

        // Strip the path of its leading '/'
        run {
            var i = 0
            while (path.isNotEmpty() && path[i] == '/') {
                path = path.substring(1)
                i++
            }
        }
//        if (path.isEmpty()) {
//            throw MalformedTokenException("0-length path")
//        }
        val i = path.indexOf(':')
        issuerExt = if (i < 0) "" else path.substring(0, i)
        issuerInt = uri.getQueryParameter(QUERY_PARAM_ISSUER)
        label = path.substring(if (i >= 0) i + 1 else 0)
        algorithm = uri.getQueryParameter(QUERY_PARAM_ALGORITHM)
        if (algorithm == null) {
            algorithm = DEFAULT_ALGORITHM
        }
        algorithm = algorithm!!.uppercase()
        try {
            Mac.getInstance("Hmac$algorithm")
        } catch (e1: NoSuchAlgorithmException) {
            throw MalformedTokenException("No such algorithm: 'Hmac$algorithm'", e1)
        }
        try {
            var d = uri.getQueryParameter(QUERY_PARAM_DIGITS)
            if (d == null) {
                d = DEFAULT_TOKEN_LENGTH
            }
            digits = d.toInt()
            if (digits != 6 && digits != 8) {
                throw MalformedTokenException("Invalid value for parameter '$QUERY_PARAM_DIGITS' (must be 6 or 8)")
            }
        } catch (e: NumberFormatException) {
            throw MalformedTokenException(
                "Unable to parse valid number for parameter '$QUERY_PARAM_DIGITS'",
                e
            )
        }
        when (type) {
            TokenType.HOTP -> try {
                var c = uri.getQueryParameter(QUERY_PARAM_COUNTER)
                if (c == null) {
                    c = "0"
                }
                count = c.toLong()
            } catch (e: NumberFormatException) {
                throw MalformedTokenException(
                    "Unable to parse valid number for parameter '$QUERY_PARAM_COUNTER'",
                    e
                )
            }
            // Default to TOTP if parameter is missing
            else -> try {
                var p = uri.getQueryParameter(QUERY_PARAM_PERIOD)
                if (p == null) {
                    p = DEFAULT_PERIOD
                }
                intervalSec = p.toInt()
            } catch (e: NumberFormatException) {
                throw MalformedTokenException(
                    "Unable to parse valid number for parameter '$QUERY_PARAM_PERIOD'",
                    e
                )
            }
        }

        val s = uri.getQueryParameter(QUERY_PARAM_SECRET)
        secret = try {
            Base32String.decode(s)
        } catch (e: Base32String.DecodingException) {
            throw MalformedTokenException(
                "Unable to decode value for parameter '$QUERY_PARAM_SECRET' value '$s'",
                e
            )
        }
    }

    private fun getHOTP(counter: Long): String {
        if (mCacheCounter == counter && mCacheCode != null) {
            return mCacheCode!!
        }

        // Encode counter in network byte order
        val bb = ByteBuffer.allocate(8)
        bb.putLong(counter)

        // Create digits divisor
        var div = 1
        for (i in digits downTo 1) {
            div *= 10
        }

        // Create the HMAC
        try {
            val algorithm = "Hmac$algorithm"
            val mac = Mac.getInstance(algorithm)
            // Throws IllegalArgumentException if the key is empty or invalid
            mac.init(SecretKeySpec(secret, algorithm))

            // Do the hashing
            val digest = mac.doFinal(bb.array())

            // Truncate
            var binary: Int
            val off = digest[digest.size - 1].toInt() and 0xf
            binary = digest[off].toInt() and 0x7f shl 0x18
            binary = binary or (digest[off + 1].toInt() and 0xff shl 0x10)
            binary = binary or (digest[off + 2].toInt() and 0xff shl 0x08)
            binary = binary or (digest[off + 3].toInt() and 0xff)
            binary %= div

            // Zero pad
            var hotp = binary.toString()
            while (hotp.length != digits) {
                hotp = "0$hotp"
            }

            // Cache
            mCacheCounter = counter
            mCacheCode = hotp
            return hotp
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    fun increment() {
        if (type === TokenType.HOTP) {
            count++
            mLastCode = System.currentTimeMillis()
        }
    }

    val identifier: String
        get() {
            val id: String = if (issuerInt != null && issuerInt != "") {
                "$issuerInt:$label"
            } else if (issuerExt != null && issuerExt != "") {
                "$issuerExt:$label"
            } else {
                label!!
            }
            return id
        }
    val secretEncoded: String
        get() = Base32String.encode(secret)
    val code: String
        get() = getCodeForTime(System.currentTimeMillis())

    private fun getCodeForTime(timestamp: Long): String {
        if (type === TokenType.TOTP) {
            return getHOTP(timestamp / 1000 / intervalSec)
        }
        if (AUTO_HIDE_HOTP && timestamp - mLastCode > HOTP_SHOWING_TIMEOUT_MS) {
            val sb = StringBuilder(digits)
            for (i in 0..digits) {
                sb.append('-')
            }
            return sb.toString()
        }
        return getHOTP(count)
    }

    private fun toUri(): Uri {
        val issuerLabel = if (issuerExt != "") "$issuerExt:$label" else label!!
        val builder = Uri.Builder()
            .scheme(URI_SCHEME)
            .path(issuerLabel)
            .appendQueryParameter(QUERY_PARAM_SECRET, Base32String.encode(secret))
            .appendQueryParameter(QUERY_PARAM_ISSUER, issuerInt ?: issuerExt)
            .appendQueryParameter(QUERY_PARAM_ALGORITHM, algorithm)
            .appendQueryParameter(
                QUERY_PARAM_DIGITS, digits.toString()
            )
        when (type) {
            TokenType.HOTP -> {
                builder.authority(URI_AUTHORITY_HOTP)
                builder.appendQueryParameter(
                    QUERY_PARAM_COUNTER, (count + 1).toString()
                )
            }
            TokenType.TOTP -> {
                builder.authority(URI_AUTHORITY_TOTP)
                builder.appendQueryParameter(
                    QUERY_PARAM_PERIOD, intervalSec.toString()
                )
            }
            else -> {}
        }
        return builder.build()
    }

    override fun toString(): String {
        return toUri().toString()
    }

    /**
     * Used to find duplicates in the list
     *
     * @param other Object to compare to
     * @return `true` if both are Tokens with the same properties
     */
    override fun equals(other: Any?): Boolean {
        if (other is Token) {
            return other.algorithm == algorithm
                    && other.digits == digits
                    && other.issuerExt == issuerExt
                    && other.issuerInt == issuerInt
                    && other.label == label
                    && other.intervalSec == intervalSec
                    && other.type === type
                    && Arrays.equals(
                other.secret,
                secret
            )
        }
        return super.equals(other)
    }

    override fun hashCode(): Int {
        var result = issuerInt?.hashCode() ?: 0
        result = 31 * result + (issuerExt?.hashCode() ?: 0)
        result = 31 * result + (label?.hashCode() ?: 0)
        result = 31 * result + (type?.hashCode() ?: 0)
        result = 31 * result + (algorithm?.hashCode() ?: 0)
        result = 31 * result + (secret?.contentHashCode() ?: 0)
        result = 31 * result + digits
        result = 31 * result + count.hashCode()
        result = 31 * result + intervalSec
        return result
    }

    companion object {
        private const val DEFAULT_ALGORITHM = "sha1"
        private const val DEFAULT_TOKEN_LENGTH = "6"
        private const val DEFAULT_PERIOD = "30"
        private const val URI_SCHEME = "otpauth"
        private const val URI_AUTHORITY_TOTP = "totp"
        private const val URI_AUTHORITY_HOTP = "hotp"
        private const val QUERY_PARAM_ISSUER = "issuer"
        private const val QUERY_PARAM_ALGORITHM = "algorithm"
        private const val QUERY_PARAM_DIGITS = "digits"
        private const val QUERY_PARAM_COUNTER = "counter"
        private const val QUERY_PARAM_PERIOD = "period"
        private const val QUERY_PARAM_SECRET = "secret"
        private const val HOTP_SHOWING_TIMEOUT_MS: Long = 60000
        private const val AUTO_HIDE_HOTP = false


        fun parse(value: String?) : Token =
            if (value == null) {
                throw MalformedTokenException("Uri is null")
            } else if (value.contains("$URI_SCHEME://")) {
                Token(Uri.parse(value))
            } else {
                // Remove all non-base32 characters before using the value as a secret
                val re = Regex("[^A-Za-z1-7]")
                val cleanedSecret = re.replace(value, "").uppercase()
                Token(Uri.parse("$URI_SCHEME://totp/issuer%3Alabel/username?secret=$cleanedSecret&algorithm=$DEFAULT_ALGORITHM&digits=$DEFAULT_TOKEN_LENGTH&"))
            }
    }
}
