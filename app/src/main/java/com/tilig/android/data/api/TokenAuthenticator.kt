package com.tilig.android.data.api

import android.util.Log
import com.tilig.android.data.livedata.ForcedLogoutLiveData
import com.tilig.android.utils.SharedPrefs
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

/**
 * If we get a 401 response it means that the authentication token has expired
 * With this class we will refresh the token and then continue with the original request
 * If the refresh of the token was not successful, we log out the user
 */
class TokenAuthenticator : Authenticator, KoinComponent {

    private val prefs: SharedPrefs by inject()
    private val authStatus: ForcedLogoutLiveData by inject()

    override fun authenticate(route: Route?, response: Response): Request? {
        synchronized(this) {
            return if (response.code == 401) {
                Log.d("TokenAuthenticator", "401 response code for request ${response.request.url}")

                val authorizationRetry =
                    response.request.header("AuthorizationRetry")?.equals("1") ?: false
                return if (
                    (prefs.jwtToken.isNullOrBlank() && prefs.refreshToken.isNullOrBlank())
                    || authorizationRetry
                ) {
                    null
                } else {
                    Log.w("TokenAuthenticator", "Retrying login")
                    val refreshResponse =
                        Api.tokenService.refresh().execute().body()
                    val newAccessToken = refreshResponse?.accessToken
                    if (newAccessToken != null) {
                        prefs.accessToken = newAccessToken
                        prefs.refreshToken = refreshResponse.refreshToken
                        prefs.jwtToken = null
                        authStatus.forceLogout(false)
                        response.request.newBuilder()
                            .header(
                                "Authorization",
                                "Bearer $newAccessToken"
                            )
                            .header("AuthorizationRetry", "1")
                            .build()
                    } else {
                        prefs.accessToken = null
                        authStatus.forceLogout(true)
                        null
                    }
                }
            } else
                null
        }
    }
}
