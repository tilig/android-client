package com.tilig.android.data.models.tilig

import android.content.Context
import android.icu.text.CaseMap.Fold
import android.os.Build
import android.os.Parcelable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import com.squareup.moshi.Json
import com.tilig.android.BuildConfig
import com.tilig.android.ui.theme.DarkBlue
import com.tilig.android.utils.DateUtils
import com.tilig.crypto.Crypto
import com.tilig.crypto.ShareMasterKeyData
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import java.text.SimpleDateFormat
import java.util.*


typealias Brands = List<Brand>
typealias Folders = List<Folder>

@Parcelize
data class HistoryEntry(
    var value: String,
    var kind: ItemFieldType = ItemFieldType.PASSWORD,
    @Json(name = "replaced_at")
    var replacedAt: String
) : Parcelable {

    companion object {
        // We've observed 2 formats coming from the API
        val API_FORMATS =
            arrayOf(SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US).apply {
                timeZone = TimeZone.getTimeZone("GMT")
            }, SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US).apply {
                timeZone = TimeZone.getTimeZone("GMT")
            })

        fun formatNow(): String =
            API_FORMATS[0].format(Calendar.getInstance().time)
    }

    fun formatDate(context: Context): String {
        val locale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.resources.configuration.locales[0]
        } else {
            @Suppress("DEPRECATION")
            context.resources.configuration.locale
        }

        val date = DateUtils.parseDateFormats(replacedAt, API_FORMATS)
        val targetFormat = SimpleDateFormat("MMM dd yyyy, HH:mm", locale)
        return date?.let { targetFormat.format(it) } ?: ""
    }
}

@Deprecated("This is a legacy encryption model", ReplaceWith("HistoryEntry"))
@Parcelize
data class LegacyHistoryEntry(
    val id: String,
    @Json(name = "password")
    val encryptedSecret: String,
    @Json(name = "created_at")
    val createdAt: String
) : Parcelable {

    fun toHistoryEntry(crypto: Crypto): HistoryEntry? {
        val secret = decryptedSecret(crypto)
        return if (secret == null) {
            null
        } else {
            HistoryEntry(
                kind = ItemFieldType.PASSWORD,
                value = secret,
                replacedAt = createdAt
            )
        }
    }

    private fun decryptedSecret(crypto: Crypto): String? {
        return crypto.legacyDecrypt(encryptedSecret)
    }
}

typealias History = List<HistoryEntry>

@Parcelize
data class EncryptedKeyPair(
    val kid: String,
    @Json(name = "encrypted_private_key")
    val encryptedPrivateKey: String,
    @Json(name = "public_key")
    val publicKey: String,
    @Json(name = "key_type")
    val keyType: String
) : Parcelable {
    fun toFirebaseModel() = mapOf(
        "kid" to kid,
        "encrypted_private_key" to encryptedPrivateKey,
        "public_key" to publicKey,
        "key_type" to keyType
    )
}

@Parcelize
data class Profile(
    val id: String,
    val email: String,
    @Json(name = "picture")
    val profilePic: String?,
    @Json(name = "family_name")
    val familyName: String?,
    @Json(name = "given_name")
    val givenName: String?,
    @Json(name = "display_name")
    val displayName: String?,
    var provider_id: String?,
    // RSA public key, will be phased out
    var public_key: String?,
    // New libsodium key
    var keypair: EncryptedKeyPair?,
    var locale: String?,
    @Json(name = "user_settings")
    var userProfileSettingsMap: UserProfileSettings?,
    @Json(name = "application_settings")
    var applicationProfileSettingsMap: ApplicationProfileSettings?,
) : Parcelable {

    fun getFullName(): String =
        if (givenName.isNullOrEmpty() && familyName.isNullOrEmpty()) {
            displayName ?: ""
        } else {
            // FIXME "Firstname Lastname" is not the proper way for many Asian countries.
            listOfNotNull(givenName, familyName).joinToString(" ")
        }

    // FIXME this wrapper around the usersettings field is no longer needed
    @IgnoredOnParcel
    @Json(ignore = true)
    var userSettings: UserSettings?
        set(value) {
            userProfileSettingsMap = value?.userProfileSettings
        }
        get() = UserSettings(userProfileSettingsMap ?: UserProfileSettings())

    @IgnoredOnParcel
    @Json(ignore = true)
    var applicationSettings: ApplicationSettings?
        set(value) {
            applicationProfileSettingsMap = value?.allSettingsMap
        }
        get() = ApplicationSettings(applicationProfileSettingsMap ?: emptyMap())
}

@Parcelize
data class SurveyAnswer(
    val skipped: Boolean,
    @Json(name = "chosen_options")
    val chosenOptions: List<String>,
    @Json(name = "question_attributes")
    val questionAttributes: SurveyQuestionAttribute,
) : Parcelable

@Parcelize
data class SurveyQuestionAttribute(
    val content: String,
    @Json(name = "survey_token")
    val surveyToken: String,
    val token: String,
    @Json(name = "answer_options")
    val answerOptions: List<String>,
) : Parcelable

@Parcelize
data class Brand(
    val id: String,
    val name: String?,
    val domain: String,
    @Json(name = "public_suffix_domain")
    val publicSuffixDomain: String?,
    val totp: Boolean,
    @Json(name = "main_color_hex")
    val mainColorHex: String?,
    @Json(name = "main_color_brightness")
    val mainColorBrightness: String?,
    @Json(name = "logo_source")
    val logoSource: String?,
    @Json(name = "logo_icon_source")
    val iconSource: String?,
    @Json(name = "is_fetched")
    val isFetched: Boolean? = false,
    @Json(name = "images")
    val imageDetails: BrandImageDetails? = null,
) : Parcelable {

    fun getBrandColor(): Color =
        Color(color = if (mainColorHex != null) android.graphics.Color.parseColor(mainColorHex) else DarkBlue.toArgb())

    fun getBrandImage(): String? = if (imageDetails?.icon?.png?.original != null) {
        imageDetails.icon.png.original
    } else iconSource
}

@Parcelize
data class BrandImageDetails(
    val icon: BrandImageIcon?
) : Parcelable

@Parcelize
data class BrandImageIcon(
    val svg: BrandImageExtension?,
    val png: BrandImageExtension?
) : Parcelable

@Parcelize
data class BrandImageExtension(
    val original: String?
) : Parcelable

@Parcelize
data class ShareLinkRequest(
    @Json(name = "share_link")
    val shareLink: ShareLinkRequestData
) : Parcelable

@Parcelize
data class ShareLinkRequestData(
    val uuid: String,
    @Json(name = "access_token")
    val accessToken: String,
    @Json(name = "encrypted_dek")
    val encryptedKey: String,
    @Json(name = "encrypted_master_key")
    val encryptedMasterKey: String,
) : Parcelable {

    constructor(shareData: ShareMasterKeyData) : this(
        uuid = shareData.uuid,
        accessToken = shareData.accessToken,
        encryptedKey = shareData.encryptedKey,
        encryptedMasterKey = shareData.encryptMasterSecret
    )
}

@Parcelize
data class ShareLink(
    val id: String,
    @Json(name = "times_viewed")
    val timesViewed: Int,
    @Json(name = "encrypted_master_key")
    val encryptedMasterKey: String,
) : Parcelable {

    @IgnoredOnParcel
    @Json(ignore = true)
    var decryptedShareLinkKey: String? = null

    fun getFullShareLink(): String? {
        return if (decryptedShareLinkKey != null) {
            if (BuildConfig.DEBUG) {
                "https://deploy-preview-473--tilig-staging.netlify.app/s#${decryptedShareLinkKey}"
            } else {
                "https://app.tilig.com/s#${decryptedShareLinkKey}"
            }
        } else {
            null
        }
    }

}

@Parcelize
data class ShareLinkFolderResponse(
    @Json(name = "share_link")
    val shareLink: ShareLink
) : Parcelable

// {
//  "user": {
//    "id": "c09aff96-4e51-44d7-870e-82be7b7a852e",
//    "email": "tili@tilig.com",
//    "public_key": "FzNlvsh_yPNcugjo_VsE55uj_0UpvnGW7PFLLnl3WB0"
//  }
//}
@Parcelize
data class PublicKeyUser(
    val id: String,
    val email: String,
    val kid: String,
    @Json(name = "public_key")
    val publicKey: String,
    @Json(name = "key_type")
    val keyType: String
) : Parcelable

@Parcelize
data class PublicKeyResponse(
    @Json(name = "user")
    val user: PublicKeyUser
) : Parcelable

@Parcelize
data class FolderResponse(
    val folder: Folder
) : Parcelable

@Parcelize
data class FoldersResponse(
    val folders: Folders
) : Parcelable

@Parcelize
data class FolderMembershipResponse(
    @Json(name = "folder_membership")
    val folderMembership: Membership?,
    // for non tilig users
    @Json(name = "folder_invitation")
    val folderInvitation: Membership?,
) : Parcelable {
    fun getMembership(): Membership? = folderMembership ?: folderInvitation
}

@Parcelize
data class ContactsResponse(
    val contacts: List<Contact>
) : Parcelable

@Parcelize
data class Contact(
    val id: String,
    val email: String,
    @Json(name = "display_name")
    val name: String?,
    @Json(name = "public_key")
    val publicKey: String,
    val picture: String?
) : Parcelable {

    fun getNameOrEmail() = if (!name.isNullOrEmpty()) {
        name
    } else {
        email
    }
}
