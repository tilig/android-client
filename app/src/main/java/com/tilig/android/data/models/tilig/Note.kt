package com.tilig.android.data.models.tilig

import com.squareup.moshi.Json
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class Note(
    override var id: String?,
    override var createdAt: String?,
    override var updatedAt: String?,
    override var encryptionVersion: Int = ENCRYPTION_VERSION_NEW,

    override var template: String? = DEFAULT_NOTE_TYPE,
    override var overview: ItemOverview,
    override var details: ItemDetails,
    override var encryptedDetails: String? = null,

    // We include those in the constructor, otherwise Kotlin operations like `.copy` will not include them.
    @Transient @Json(ignore = true)
    override var areDetailsDecrypted: Boolean = false,
    @Transient @Json(ignore = true)
    override var decryptedDek: ByteArray? = null,
    @Transient @Json(ignore = true)
    override var legacyEncryptionDisabled: Boolean? = false,

    override var encryptedFolderKey: String? = null,
    override var folder: Folder? = null,
    override var share: ShareLink? = null,
) : SecretItem {

    companion object {
        fun createEmpty() = Note(
            id = null,
            createdAt = null,
            updatedAt = null,
            encryptionVersion = ENCRYPTION_VERSION_NEW,
            overview = ItemOverview.createEmpty(),
            details = ItemDetails.createEmpty(),
            encryptedDetails = null,
        ).apply {
            areDetailsDecrypted = true
        }
    }

    @IgnoredOnParcel
    @Json(ignore = true)
    var content: String?
        set(value) {
            assert(areDetailsDecrypted)
            details.notes = value ?: ""
        }
        get() {
            assert(areDetailsDecrypted)
            return details.notes
        }

    override fun toDebugString(): String = """
        name = $name
        content = $content
        dek = ${decryptedDek?.joinToString { ", " }}
    """.trimIndent()
}
