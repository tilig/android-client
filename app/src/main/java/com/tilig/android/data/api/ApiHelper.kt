package com.tilig.android.data.api

import android.util.Log
import com.tilig.android.BuildConfig
import com.tilig.android.data.api.services.ApiService
import com.tilig.android.data.livedata.ApiResponseCallAdapterFactory
import com.tilig.android.data.livedata.ConnectivityLiveData
import com.tilig.android.data.livedata.LiveDataCallAdapterFactory
import com.tilig.android.utils.DebugLogger
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

object ApiHelper {

    private const val ENABLE_CACHE_INTERCEPTOR = false


    private fun customDebugInterceptor(callback: (String) -> Unit): Interceptor =
        HttpLoggingInterceptor(logger = { message ->
            callback.invoke(message)
            Log.i("OkHttpClient", message)
        }).apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

    /**
     * Creates a Retrofit instance based on the given client. Automatically adds
     * the base url from the BuildConfig and sets all relevant Moshi adapters and converters.
     */
    fun createRetrofitForClient(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create(Api.moshi).withNullSerialization())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .addCallAdapterFactory(ApiResponseCallAdapterFactory())
            .build()

    /**
     * Bundles [createRetrofitForClient] and [createOkHttpClient] and [customDebugInterceptor]
     * to quickly create an [ApiService] instance that logs all requests and responses to a
     * [DebugLogger]
     */
    fun createTemporaryDebugApiClient(debugLogger: DebugLogger): ApiService {
        val okHttpClient = createOkHttpClient(
            cache = null,
            connectivity = null,
            useBearerToken = true,
            enableTokenAuthenticator = true,
            debugInterceptor = customDebugInterceptor {
                debugLogger.collect(it)
            }
        )
        val retrofit = createRetrofitForClient(okHttpClient)
        return retrofit.create(ApiService::class.java)
    }

    /**
     * Creates a new OkHttpClient instance
     * @param cache Optional Cache
     * @param connectivity Optional connectivity indicator. Not used if cache is disabled
     * @param useBearerToken Indicates the type of authentication: Bearer token or Type token
     * @param enableTokenAuthenticator If enabled, the TokenAuthenticator is added
     * @param debugInterceptor An interceptor that can help debugging. Only added in debuggable builds.
     */
    fun createOkHttpClient(
        cache: Cache?,
        connectivity: ConnectivityLiveData?,
        useBearerToken: Boolean,
        enableTokenAuthenticator: Boolean,
        debugInterceptor: Interceptor?,
    ) =
        OkHttpClient.Builder().apply {
            // Cache is enabled for the regular requests, but not for login requests
            if (cache != null) {
                cache(cache)

                if (ENABLE_CACHE_INTERCEPTOR && connectivity != null) {
                    val cacheInterceptor = Interceptor { chain ->
                        var request = chain.request()
                        request = if (connectivity.value == ConnectivityState.Connected)
                            request.newBuilder().header("Cache-Control", "public, max-age=" + 5)
                                .build()
                        else
                            request.newBuilder().header(
                                "Cache-Control",
                                "public, only-if-cached, max-stale=${TimeUnit.DAYS.toSeconds(7)}"
                            ).build()
                        chain.proceed(request)
                    }

                    addInterceptor(cacheInterceptor)
                }
            }

            // During login flow, we switch from Bearer token to Type token
            if (useBearerToken) {
                addInterceptor(AuthorizationInterceptor(AuthorizationInterceptor.TOKEN_TYPE_BEARER))
            } else {
                addInterceptor(AuthorizationInterceptor(AuthorizationInterceptor.TOKEN_TYPE_TOKEN))
            }

            // Adds the X-Tilig headers
            addInterceptor(CustomHeaderInterceptor())

            if (enableTokenAuthenticator) {
                authenticator(TokenAuthenticator())
            }

            if (BuildConfig.DEBUG && debugInterceptor != null) {
                addInterceptor(debugInterceptor)
            }
        }.build()
}