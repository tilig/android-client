package com.tilig.android.data.livedata

import androidx.lifecycle.MutableLiveData
import com.tilig.android.data.api.ConnectivityMonitor
import com.tilig.android.data.api.ConnectivityState
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class ConnectivityLiveData :
    MutableLiveData<ConnectivityState>(), KoinComponent {

    private val connectionMonitor: ConnectivityMonitor by inject()

    override fun onActive() {
        super.onActive()
        connectionMonitor.startListening(::setConnected)
    }

    override fun onInactive() {
        connectionMonitor.stopListening()
        super.onInactive()
    }

    private fun setConnected(isConnected: Boolean) =
        postValue(if (isConnected) ConnectivityState.Connected else ConnectivityState.Disconnected)
}
