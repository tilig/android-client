package com.tilig.android.data.models.tilig

import android.os.Parcelable
import androidx.annotation.VisibleForTesting
import com.squareup.moshi.Json
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.tilig.android.ui.onboarding.getstarted.components.Step
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue

enum class SettingsSection(val key:String) {
    ANDROID("android"),
    IOS("ios"),
    ONBOARDING("onboarding")
}

// example:  { android: { survey_finished: true }, ios : {} }
@Parcelize
data class UserProfileSettings(
    val accountType: String? = null,
    var android: Map<String, @RawValue Any?>? = null,
    var ios: Map<String, @RawValue Any?>? = null,
    var onboarding: Map<String, @RawValue Any?>? = null,
) : Parcelable {
    fun getSectionMap(section: SettingsSection): Map<String, @RawValue Any?> =
        when(section) {
            SettingsSection.ANDROID -> android
            SettingsSection.IOS -> ios
            SettingsSection.ONBOARDING -> onboarding
        } ?: emptyMap()

    /**
     * Replaces the section; does NOT merge it. SO make sure you copied the existing
     * values into the `values` param beforehand!
     */
    fun setSection(section: SettingsSection, values: Map<String, Any?>?) {
        when(section) {
            SettingsSection.ANDROID -> android = values
            SettingsSection.IOS -> ios = values
            SettingsSection.ONBOARDING -> onboarding = values
        }
    }
}

private enum class UserSettingsItemFieldType(val key: String) {
    @Json(name = "survey_finished")
    SURVEY_FINISHED(key = "survey_finished"),

    @Json(name = "get_started_finished")
    GET_STARTED_INFO(key = "get_started_info"),

    @Json(name = "completed")
    COMPLETED(key = "completed")
}

@Parcelize
data class UserSettings(
    @Json(name = "user_settings")
    var userProfileSettings: UserProfileSettings = UserProfileSettings()
) : Parcelable {

    @IgnoredOnParcel
    @Json(ignore = true)
    var isSurveyFinished: Boolean
        set(value) {
            setField(SettingsSection.ANDROID, UserSettingsItemFieldType.SURVEY_FINISHED.key, value)
        }
        get() {
            return findField(
                SettingsSection.ANDROID,
                UserSettingsItemFieldType.SURVEY_FINISHED.key
            ) as? Boolean ?: false
        }

    /**
     * Reads the settings that existing users have on web. If present, the user
     * has used the app before and we don't show the getting Started
     */
    @IgnoredOnParcel
    @Json(ignore = true)
    var hasOnboardedBefore: Boolean
        set(value) {
            // TODO not sure if we need this setter at all?
            //setField(SettingsSection.ONBOARDING, UserSettingsItemFieldType.COMPLETED.key, value)
        }
        get() {
//            return findField(
//                SettingsSection.ONBOARDING,
//                UserSettingsItemFieldType.SURVEY_FINISHED.key
//            ) as? Boolean ?: false
            return userProfileSettings.onboarding != null
        }

    fun getGetStartedInfo(moshi: Moshi): Map<Step, Boolean> {
        val type = Types.newParameterizedType(
            Map::class.java,
            Step::class.java,
            Boolean::class.javaObjectType
        )
        val textValue = findField(
            SettingsSection.ANDROID,
            UserSettingsItemFieldType.GET_STARTED_INFO.key
        ) as String?
        if (textValue == null) {
            return Step.values().filter { it != Step.UNKNOWN }.associateWith { false }
        } else {
            return moshi.adapter<Map<Step, Boolean>>(type)
                .fromJson(textValue)
                ?.filter { it.key != Step.UNKNOWN }
                ?: emptyMap()
        }
    }

    fun setGetStartedInfo(moshi: Moshi, getStartedInfo: Map<Step, Boolean>) {
        val type = Types.newParameterizedType(
            Map::class.java,
            Step::class.java,
            Boolean::class.javaObjectType
        )

        val stringRepresentation = moshi.adapter<Map<Step, Boolean>>(type)
            .toJson(getStartedInfo)
        setField(
            SettingsSection.ANDROID,
            UserSettingsItemFieldType.GET_STARTED_INFO.key,
            stringRepresentation
        )
    }

    /**
     * Write the value into appropriate settings section
     */
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun setField(section: SettingsSection, type: String, value: Any) {
        val settingsSection = userProfileSettings.getSectionMap(section)
        val fields = hashMapOf<String, Any?>()
        settingsSection.let {
            fields.putAll(it)
        }
        fields[type] = value
        userProfileSettings.setSection(section, fields.toMap())
    }

    /**
     * @return the value of the field with the requested type in the requested section, or null.
     */
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun findField(section: SettingsSection, type: String): Any? {
        val s = userProfileSettings.getSectionMap(section)
        return s[type]
    }
}