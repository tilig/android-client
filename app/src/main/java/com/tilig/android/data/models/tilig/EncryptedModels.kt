package com.tilig.android.data.models.tilig

import android.os.Parcelable
import android.util.Log
import com.goterl.lazysodium.utils.Key
import com.goterl.lazysodium.utils.KeyPair
import com.squareup.moshi.Json
import com.squareup.moshi.Moshi
import com.tilig.android.utils.DebugLogger
import com.tilig.crypto.BuildConfig
import com.tilig.crypto.Crypto
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import kotlin.random.Random

typealias EncryptedItems = List<EncryptedItem>

data class EncryptedItemsResponse(
    val items: EncryptedItems
)

data class EncryptedItemResponse(
    val item: EncryptedItem
)

const val ENCRYPTION_VERSION_NEW = 2
const val DEFAULT_ACCOUNT_TYPE = "login/1"
const val DEFAULT_NOTE_TYPE = "securenote/1"
const val DEFAULT_CREDIT_CARD_TYPE = "creditcard/1"
const val DEFAULT_WIFI_PASSWORD_TYPE = "wifi/1"
const val DEFAULT_CUSTOM_TYPE = "custom/1"

@Parcelize
data class User(
    @Json(name = "created_at")
    val createdAt: String? = null,
    @Json(name = "updated_at")
    val updatedAt: String? = null,
    @Json(name = "display_name")
    val displayName: String? = null,
    @Json(name = "id")
    val id: String?,
    val email: String,
    val picture: String? = null,
    @Json(name = "public_key")
    val publicKey: String?
) : Parcelable {
    companion object {
        fun createDummy(): User = User(
            createdAt = "2022-11-10T15:42:36.326Z",
            updatedAt = "2022-11-10T15:42:36.326Z",
            displayName = "Hidde-Jan",
            email = "random-email@test.test",
            id = Random.nextInt().toString(),
            picture = "https://lh3.googleusercontent.com/a-/AOh14GhSH-osBbTeLQbho4yumDHfi4ZQSr5a9I0Q8b_9iA=s96-c",
            publicKey = "NEFybavfz8KyoHPUQxenISNXsu5FXz34tkwQ7fdgelI"
        )
    }

    /**
     * For privacy reasons, the displayName is null until the user accepted the invite. We fallback to the email in that case.
     */
    fun getADisplayName() = displayName ?: email
}

@Parcelize
data class Membership(
    // Not using those:
//    val accepted: Boolean? = false,
//    @Json(name = "accepted_at")
//    val acceptedAt: String? = null,
//    @Json(name = "created_at")
//    val createdAt: String? = null,
//    @Json(name = "updated_at")
//    val updatedAt: String? = null,
//    val role: String? = null,

    @Json(name = "pending")
    val isPending: Boolean? = false,
    @Json(name = "id")
    val id: String,
    val user: User,
    @Json(name = "is_me")
    val isMe: Boolean?,
    @Json(name = "temporary_field__acceptance_token")
    val tempAcceptanceToken: String? = null
) : Parcelable {

    companion object {
        fun createDummy(): Membership = Membership(
            isPending = Random.nextBoolean(),
            id = Random.nextInt().toString(),
            user = User.createDummy(),
            isMe = false
        )
    }
}

@Parcelize
data class Folder(
    @Json(name = "created_at")
    val createdAt: String? = null,
    @Json(name = "updated_at")
    val updatedAt: String? = null,

    @Json(name = "folder_memberships")
    val folderMemberships: Array<Membership>?,
    // non tilig users
    @Json(name = "folder_invitations")
    val folderInvitations: Array<Membership>?,

    val id: String,
    @Json(name = "my_encrypted_private_key")
    val myEncryptedPrivateKey: String,
    val name: String? = null,
    @Json(name = "public_key")
    val publicKey: String,
    // if false it means that it's a created folder and not shared
    @Json(name = "single_item")
    val isSharedWithIndividuals: Boolean = false
) : Parcelable {

    fun findMyMembership(): Membership? = folderMemberships?.first { it.isMe == true }

    /**
     * @return all memberships , including tilig and non tilig users
     */
    fun getAllMemberships(): Array<Membership> =
        (folderMemberships ?: emptyArray()).plus(folderInvitations ?: emptyArray())

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Folder

        if (createdAt != other.createdAt) return false
        if (updatedAt != other.updatedAt) return false
        if (folderMemberships != null) {
            if (other.folderMemberships == null) return false
            if (!folderMemberships.contentEquals(other.folderMemberships)) return false
        } else if (other.folderMemberships != null) return false
        if (folderInvitations != null) {
            if (other.folderInvitations == null) return false
            if (!folderInvitations.contentEquals(other.folderInvitations)) return false
        } else if (other.folderInvitations != null) return false
        if (id != other.id) return false
        if (myEncryptedPrivateKey != other.myEncryptedPrivateKey) return false
        if (name != other.name) return false
        if (isSharedWithIndividuals != other.isSharedWithIndividuals) return false
        if (publicKey != other.publicKey) return false

        return true
    }

    override fun hashCode(): Int {
        var result = createdAt?.hashCode() ?: 0
        result = 31 * result + (updatedAt?.hashCode() ?: 0)
        result = 31 * result + (folderMemberships?.contentHashCode() ?: 0)
        result = 31 * result + (folderInvitations?.contentHashCode() ?: 0)
        result = 31 * result + id.hashCode()
        result = 31 * result + myEncryptedPrivateKey.hashCode()
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + publicKey.hashCode()
        result = 31 * result + isSharedWithIndividuals.hashCode()
        return result
    }

    companion object {
        fun createDummy() = Folder(
            createdAt = "2023-02-07T09:20:40.082Z",
            updatedAt = "2023-02-07T09:20:40.082Z",
            folderMemberships = arrayOf(
                Membership.createDummy(),
                Membership.createDummy(),
                Membership.createDummy(),
            ),
            folderInvitations = arrayOf(
                Membership.createDummy()
            ),
            id = Random.nextInt().toString(),
            myEncryptedPrivateKey = Random.nextInt().toString(),
            name = null,
            publicKey = Random.nextInt().toString()
        )
    }
}

/**
 * Account model as it arrives from the API, completely encrypted. This model does not hold unencrypted
 * data ever, instead it returns a different model ([Account]) as a result of decryption.
 * There is one exception: the DEK can stay in memory after decrypting, for efficiency.
 * This model can also be sent to the API for create/update operations.
 */
@Parcelize
data class EncryptedItem(
    var id: String?,
    @Json(name = "created_at")
    val createdAt: String? = null,
    @Json(name = "updated_at")
    val updatedAt: String? = null,
    @Json(name = "encryption_version")
    var encryptionVersion: Int = ENCRYPTION_VERSION_NEW,

    // This is what we use to match w/ brands.
    // NOT what is used in the AccountMatcher to match websites or apps
    var domain: String? = null,
    var brand: Brand? = null,
    @Json(name = "share_link")
    var share: ShareLink? = null,

    @Json(name = "template")
    var template: String? = null,
    @Json(name = "rsa_encrypted_dek")
    var encryptedDek: String? = null,
    @Json(name = "encrypted_dek")
    var encryptedKey: String? = null,
    @Json(name = "encrypted_folder_dek")
    var encryptedFolderKey: String? = null,
    @Json(name = "legacy_encryption_disabled")
    var legacyEncryptionDisabled: Boolean? = true,

    @Json(name = "encrypted_overview")
    var encryptedOverview: String? = null,
    @Json(name = "encrypted_details")
    var encryptedDetails: String? = null,

    // Legacy fields.
    // We use the API fields here only once. They are used when decrypting into the Account model,
    // where they are put in the new fields. When encrypting an Account to an EncryptedAccount, they
    // are once again put back into the old fields, for backwards compatibility.
    // it is not pretty, but it is a way that allows itself to be removed easily in the future.
    @Json(name = "name")
    override var legacyName: String? = null,
    @Json(name = "website")
    var website: String? = null,
    @Json(name = "username")
    override var legacyEncryptedUsername: String? = null,
    @Json(name = "notes")
    override var legacyEncryptedNotes: String? = null,
    @Json(name = "password")
    override var legacyEncryptedSecret: String? = null,
    @Json(name = "android_app_id")
    override var legacyAndroidAppId: String? = null,
    @Json(name = "otp")
    override var legacyEncryptedOtp: String? = null,

    @Suppress("ArrayInDataClass")
    val folder: Folder? = null,
    // only for creating a new item and add it to a folder
    @Json(name = "folder_id")
    val folderId: String? = null,
) : LegacyFields(), Parcelable {

    @IgnoredOnParcel
    @Transient
    var decryptedDek: ByteArray? = null

    /**
     * @param includingDetails if true the details will be decrypted. For more secure on the list screen
     * we decrypt only overview data, on details screen decrypt details
     */
    fun decrypt(
        jsonConverter: Moshi,
        crypto: Crypto,
        includingDetails: Boolean = false,
        debugLogger: DebugLogger? = null
    ): SecretItem {

        // Force legacy encryption disabled:
        legacyEncryptionDisabled = true

        var overviewObj: ItemOverview? = null

        // Decrypt the DEK using Crypto module and either of 2 keypairs
        val dek: ByteArray? = when {
            !encryptedFolderKey.isNullOrEmpty() -> {
                val result = folder?.let {
                    // Shared folders have a separate folder key
                    Log.v("Crypto", "Using the FOLDER keypair to decrypt the dek")
                    debugLogger?.collect("Using the FOLDER keypair to decrypt the dek")

                    // User has a private key → used to decrypt the private KEK
                    val usersKeypair = crypto.getKeyPairs().keypair!!
                    val privateKek = crypto.open(
                        cipherText = folder.myEncryptedPrivateKey,
                        keyPair = usersKeypair
                    )
                    val publicKek = crypto.base64decode(folder.publicKey)

                    // Private key of the KEK is used to decrypt the DEK
                    val keyPair = KeyPair(
                        Key.fromBytes(publicKek),
                        Key.fromBytes(privateKek)
                    )
                    crypto.decryptWithKeypair(
                        encryptedFolderKey!!,
                        keyPair
                    )
                }
                if (result != null && !result.all { it == 0.toByte() }) {
                    result
                } else {
                    debugLogger?.collect("  Correction: we are unable to decrypt using the folder key")
                    null
                }
            }
            !encryptedKey.isNullOrEmpty() -> {
                // Latest method uses the encryptedKey encrypted with the libsodium keypair
                //Log.v("Crypto", "Using the new keypair to decrypt the dek")
                debugLogger?.collect("Using the new keypair to decrypt the dek")
                val result = crypto.decryptDek(encryptedKey!!)
                if (result != null) {
                    result
                } else {
                    // If encryptedKey is null, we fall back to the encryptedDek, which is the same DEK but
                    // encrypted with the old asymmetrical keypair
                    Log.v(
                        "Crypto",
                        "  Correction: we don't have the key yet, so we're using the legacy keypair to decrypt the dek"
                    )
                    debugLogger?.collect("  Correction: we don't have the key yet, so we're using the legacy keypair to decrypt the dek")
                    decryptedDek ?: crypto.legacyDecryptDek(encryptedDek!!)
                }
            }
            !encryptedDek.isNullOrEmpty() -> {
                // If encryptedKey is null, we fall back to the encryptedDek, which is the same DEK but
                // encrypted with the old asymmetrical keypair
                Log.v("Crypto", "Using the legacy keypair to decrypt the dek")
                debugLogger?.collect("Using the legacy keypair to decrypt the dek")
                decryptedDek ?: crypto.legacyDecryptDek(encryptedDek!!)
            }
            else -> {
                // Note: for *very* old accounts (v1 encryption), there is no dek, and overview/detail will be null or empty.
                null
            }
        }

        // Not possible in practice, but possible when you are a developer and messed up your keys
        if (dek != null) {
            // Decrypt overview and details using decrypted dek
            try {
                crypto.decryptUsingDek(dek, encryptedOverview!!)
                    ?.let { decryptedOverview ->
                        debugLogger?.collect("Decrypted overview: \n$decryptedOverview")
                        overviewObj =
                            jsonConverter.adapter(ItemOverview::class.java)
                                .fromJson(decryptedOverview)
                    }
            } catch (e: RuntimeException) {
                debugLogger?.collect("Decrypted overview: Broken keypair? ${e.message}")
                Log.w("decrypt", "Broken keypair?", e)
            }
        } else if (!BuildConfig.DEBUG) {
            debugLogger?.collect("Decrypted overview: Broken keypair?")
            Log.w("decrypt", "Broken keypair?")
        }

        return if (encryptedOverview == null || template == DEFAULT_ACCOUNT_TYPE) {
            Account(
                id = id,
                createdAt = createdAt,
                updatedAt = updatedAt,
                encryptionVersion = encryptionVersion,
                domain = domain,
                overview = overviewObj ?: ItemOverview.createEmpty(),
                details = ItemDetails.createEmpty(),
                encryptedDetails = encryptedDetails,
                brand = brand,
                share = share,
                legacyEncryptionDisabled = legacyEncryptionDisabled,
                encryptedFolderKey = encryptedFolderKey,
                folder = folder,
            ).apply {
                decryptedDek = dek
                areDetailsDecrypted = false

                // Legacy support.
                // If there is no DEK, then this is a legacy account, and we'll force the
                // details decryption. Otherwise we have nowhere to put the details.
                // Note that this won't actually decrypt anything (because there's nothing to decrypt)
                // but it'll create the object we need.
                val legacyEncryptionEnabled = legacyEncryptionDisabled != true
                val includeDetailsToUpgradeFromLegacy =
                    dek == null && encryptedDek.isNullOrEmpty() && legacyEncryptionEnabled

                if (includingDetails || includeDetailsToUpgradeFromLegacy) {
                    decryptDetails(jsonConverter, crypto, debugLogger)
                }

                // Backwards-compat support.
                // Since (for the time being) the legacy fields are the ground truth,
                // we ALWAYS overwrite the data from the new blob with the values from the
                // legacy fields. This is safe, because whenever we make an edit in the new blob,
                // we make the same edit in the legacy fields.
                website = domain ?: this@EncryptedItem.website
//                if (legacyEncryptionEnabled) {
//                    name = this@EncryptedItem.legacyName
//                    username = crypto.legacyDecrypt(this@EncryptedItem.legacyEncryptedUsername)
//                    androidAppId = this@EncryptedItem.legacyAndroidAppId
//                    if (areDetailsDecrypted) {
//                        password = crypto.legacyDecrypt(this@EncryptedItem.legacyEncryptedSecret)
//                        otp = crypto.legacyDecrypt(this@EncryptedItem.legacyEncryptedOtp)
//                        notes = crypto.legacyDecrypt(this@EncryptedItem.legacyEncryptedNotes) ?: ""
//                    }
//                }
                share = share?.apply {
                    decryptedShareLinkKey = encryptedMasterKey.let {
                        crypto.decryptMasterSecret(
                            it
                        )
                    }
                }
            }
        } else if (template == DEFAULT_NOTE_TYPE) {
            Note(
                id = id,
                createdAt = createdAt,
                updatedAt = updatedAt,
                encryptionVersion = encryptionVersion,
                overview = overviewObj ?: ItemOverview.createEmpty(),
                details = ItemDetails.createEmpty(),
                encryptedDetails = encryptedDetails,
                legacyEncryptionDisabled = legacyEncryptionDisabled,
                encryptedFolderKey = encryptedFolderKey,
                folder = folder,
            ).apply {
                decryptedDek = dek
                areDetailsDecrypted = false
                if (includingDetails) {
                    decryptDetails(jsonConverter, crypto, debugLogger)
                }
                // Support the legacy name field set by the webapp if it's not test drive item
                if (legacyEncryptionDisabled != true) {
                    name = legacyName
                }
            }
        } else if (template == DEFAULT_CREDIT_CARD_TYPE) {
            CreditCard(
                id = id,
                createdAt = createdAt,
                updatedAt = updatedAt,
                encryptionVersion = encryptionVersion,
                overview = overviewObj ?: ItemOverview.createEmpty(),
                details = ItemDetails.createEmpty(),
                encryptedDetails = encryptedDetails,
                legacyEncryptionDisabled = legacyEncryptionDisabled,
                encryptedFolderKey = encryptedFolderKey,
                folder = folder,
            ).apply {
                decryptedDek = dek
                areDetailsDecrypted = false
                if (includingDetails) {
                    decryptDetails(jsonConverter, crypto, debugLogger)
                }
            }
        } else if (template == DEFAULT_WIFI_PASSWORD_TYPE) {
            WifiPassword(
                id = id,
                createdAt = createdAt,
                updatedAt = updatedAt,
                encryptionVersion = encryptionVersion,
                overview = overviewObj ?: ItemOverview.createEmpty(),
                details = ItemDetails.createEmpty(),
                encryptedDetails = encryptedDetails,
                legacyEncryptionDisabled = legacyEncryptionDisabled,
                encryptedFolderKey = encryptedFolderKey,
                folder = folder,
            ).apply {
                decryptedDek = dek
                areDetailsDecrypted = false
                if (includingDetails) {
                    decryptDetails(jsonConverter, crypto, debugLogger)
                }
            }
        } else {
            OtherItem(
                id = id,
                createdAt = createdAt,
                updatedAt = updatedAt,
                encryptionVersion = encryptionVersion,
                overview = overviewObj ?: ItemOverview.createEmpty(),
                details = ItemDetails.createEmpty(),
                encryptedDetails = encryptedDetails,
                legacyEncryptionDisabled = legacyEncryptionDisabled,
                encryptedFolderKey = encryptedFolderKey,
                folder = folder,
            ).apply {
                decryptedDek = dek
                areDetailsDecrypted = false
                if (includingDetails) {
                    decryptDetails(jsonConverter, crypto, debugLogger)
                }
            }
        }
    }

    companion object {

        fun encrypt(item: SecretItem, jsonConverter: Moshi, crypto: Crypto): EncryptedItem {

            // Handling of legacy encryption
            val legacyEncryptionEnabled = item.legacyEncryptionDisabled != true

            // Decrypt the DEK using Crypto
            val dek = item.decryptedDek ?: crypto.generateDek()
            val legacyEncryptedDek = crypto.legacyEncryptDek(dek)

            // For now, we accept encryptedDek to be null, which is possible
            // if the user hasn't loaded the keys fast enough. They'll have to really quickly
            // create an account to trigger that state, or be in the very briefly published
            // 3.9.10 release.
            val encryptedDek = crypto.encryptDek(dek)
            item.decryptedDek = dek

            // Decrypt first, just in case we didn't do that yet.
            // Okay, that scenario is highly unlikely, but it is harmless to check.
            if (!item.areDetailsDecrypted) {
                item.decryptDetails(jsonConverter, crypto)
            }

            val overviewString =
                jsonConverter.adapter(ItemOverview::class.java).toJson(item.overview)
            val detailsString =
                jsonConverter.adapter(ItemDetails::class.java).toJson(item.details)

            val baseEncryptedItem = EncryptedItem(
                id = item.id,
                createdAt = item.createdAt,
                updatedAt = item.updatedAt,
                encryptionVersion = ENCRYPTION_VERSION_NEW,
                encryptedDek = legacyEncryptedDek,
                encryptedFolderKey = item.encryptedFolderKey,
                encryptedKey = encryptedDek,
                encryptedOverview = crypto.encryptUsingDek(dek, overviewString),
                encryptedDetails = crypto.encryptUsingDek(dek, detailsString),
                folderId = item.folder?.id
            ).apply {
                decryptedDek = dek
            }

            return when (item) {
                is Account -> if (legacyEncryptionEnabled) {
                    baseEncryptedItem.copy(
                        domain = item.domain ?: item.website ?: "",
                        brand = item.brand,
                        legacyAndroidAppId = item.androidAppId,
                        legacyEncryptedNotes = crypto.legacyEncryptIfNotBlank(item.notes),
                        legacyEncryptedOtp = crypto.legacyEncryptIfNotBlank(item.otp),
                        legacyEncryptedSecret = crypto.legacyEncryptIfNotBlank(item.password),
                        legacyEncryptedUsername = crypto.legacyEncryptIfNotBlank(item.username),
                        legacyName = item.name,
                        website = item.website,
                        // agreed with the server side that it should be null for accounts
                        template = item.template
                    )
                } else {
                    baseEncryptedItem.copy(
                        domain = item.domain ?: item.website ?: "",
                        brand = item.brand,
                        website = item.website,
                        // agreed with the server side that it should be null for accounts
                        template = item.template
                    )
                }
                is Note,
                is CreditCard,
                is WifiPassword,
                is OtherItem -> baseEncryptedItem.copy(
                    template = item.template,
                )
                else -> {
                    throw Exception("Not yet supported item type")
                }
            }
        }
    }
}

/**
 * Sensitive properties of the Account model.
 *
 * These are only decrypted JUST IN TIME, e.g. when an account
 * is selected for autofill, or when it is displayed.
 */
@Parcelize
data class ItemDetails(
    var notes: String? = "",
    var main: MutableList<ItemField>,
    @Json(name = "custom_fields")
    var customFields: MutableList<CustomField>,
    @Json(name = "history")
    var history: History
) : Parcelable {

    companion object {
        fun createEmpty() = ItemDetails(
            notes = "",
            main = mutableListOf(),
            customFields = mutableListOf(),
            history = mutableListOf()
        )
    }

    fun isEmpty(): Boolean =
        notes.isNullOrEmpty() && main.isEmpty() && customFields.isEmpty() && history.isEmpty()

    fun isNotEmpty(): Boolean = !isEmpty()

    /**
     * Sets the password. This is not encrypted until we send it to the API
     * (because more fields might be incoming)
     */
    fun setField(value: String, kind: ItemFieldType) {
        val index = main.indexOfFirst { it.kind == kind }
        if (index == -1) {
            // Add new
            main.add(
                ItemField(
                    value = value,
                    kind = kind
                )
            )
        } else {
            // Update existing
            main[index].value = value
        }
    }

    /**
     * This method returns the value of the first field with the requested type, or null.
     */
    fun findField(kind: ItemFieldType): String? {
        return main.firstOrNull { it.kind == kind }?.value
    }

    fun clearField(kind: ItemFieldType) {
        if (kind.isPrimary) {
            main.removeAll {
                it.kind == kind
            }
        } else {
            customFields.removeAll {
                it.kind == kind
            }
        }
    }

    fun setCustomField(name: String, value: String, kind: ItemFieldType) {
        val index = customFields.indexOfFirst { it.kind == kind }
        if (index == -1) {
            // Add new
            customFields.add(
                CustomField(
                    name = name,
                    value = value,
                    kind = kind
                )
            )
        } else {
            // Update existing
            customFields[index].value = value
        }
    }

    /**
     * This method returns the value of the first CUSTOM field with the requested type, or null.
     */
    fun findCustomField(kind: ItemFieldType): String? {
        return customFields.firstOrNull { it.kind == kind }?.value
    }

    fun clearCustomField(kind: ItemFieldType) {
        customFields.removeAll {
            it.kind == kind
        }
    }

    fun getSortedHistory() = history.sortedByDescending { it.replacedAt }.toMutableList()
}

/**
 * Low sensitivity properties in the Account model.
 *
 * These properties can/should be decrypted as soon as
 * data is received from the API or loaded from memory.
 * This data is used for sorting, displaying, and matching
 * (for things like autofill).
 */
@Parcelize
data class ItemOverview(
    var name: String,
    var info: String? = null,
    var urls: MutableList<UrlField>,
    @Json(name = "android_app_ids")
    var androidAppIds: MutableList<String>,
    // Semi-legacy support, used for parsing only. This field is now 'info'
    var username: String? = null,
) : Parcelable {
    companion object {
        fun createEmpty() = ItemOverview(
            name = "",
            info = "",
            urls = mutableListOf(),
            androidAppIds = mutableListOf()
        )
    }
}

@Parcelize
data class UrlField(
    var name: String? = null,
    var url: String
) : Parcelable

@Parcelize
data class MetaField(
    var key: String,
    var value: String?
) : Parcelable

/**
 * Type of field. Fields are divided into 2 sections: MAIN and FIELDS.
 * The MAIN ones are part of the account type template, the other ones
 * are considered secondary.
 */
enum class ItemFieldType(val key: String, val isPrimary: Boolean) {
    @Json(name = "unknown")
    UNKNOWN(key = "", isPrimary = false),

    // This field is part of: Login, and one the 4 distinguished custom fields for Other items
    @Json(name = "password")
    PASSWORD(key = "password", isPrimary = true),

    // This field is only part of Other items
    @Json(name = "secret")
    SECRET(key = "secret", isPrimary = false),

    // This field is only part of Other items
    @Json(name = "date")
    DATE(key = "date", isPrimary = false),

    // This field is part of: Login, and one the 4 distinguished custom fields for Other items
    @Json(name = "totp")
    TOTP(key = "totp", isPrimary = true),

    // This field is part of: Login, Card, WiFi, and one the 4 distinguished custom fields for Other items
    @Json(name = "text")
    TEXT(key = "text", isPrimary = false),

    @Json(name = "username")
    USERNAME(key = "username", isPrimary = true),

    @Json(name = "ccexp")
    CARD_EXPIRES(key = "ccexp", isPrimary = false),

    @Json(name = "ccholder")
    CARD_HOLDER_NAME(key = "ccholder", isPrimary = false),

    @Json(name = "ccnumber")
    CARD_NUMBER(key = "ccnumber", isPrimary = false),

    @Json(name = "cvv")
    CARD_SECURITY_CODE(key = "cvv", isPrimary = false),

    @Json(name = "pin")
    CARD_PIN_CODE(key = "pin", isPrimary = false),

    @Json(name = "zipcode")
    CARD_ZIPCODE(key = "zipcode", isPrimary = false),

    @Json(name = "ssid")
    SSID(key = "ssid", isPrimary = false);

    // true if item contains sensitive information like pass, pin
    fun isSensitive(): Boolean =
        this == PASSWORD || this == CARD_ZIPCODE || this == CARD_PIN_CODE || this == CARD_SECURITY_CODE || this == SECRET
}

/**
 * An ItemField holds a single field of information. A password, OTP secret, username, etc.
 */
@Parcelize
data class ItemField(
    var kind: ItemFieldType,
    var value: String?
) : Parcelable

/**
 * An CustomField holds a single field of information. A password, OTP secret, username, etc.
 */
@Parcelize
data class CustomField(
    var name: String,
    var kind: ItemFieldType,
    var value: String?
) : Parcelable
