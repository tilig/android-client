package com.tilig.android.data.repository

import androidx.lifecycle.LiveData
import com.squareup.moshi.Moshi
import com.tilig.android.data.api.Api
import com.tilig.android.data.livedata.ResourceLiveData
import com.tilig.android.data.livedata.ResponseLiveData
import com.tilig.android.data.models.ApiResponse
import com.tilig.android.data.models.ApiSuccessResponse
import com.tilig.android.data.models.NetworkBoundResource
import com.tilig.android.data.models.tilig.*
import com.tilig.crypto.Crypto
import com.tilig.crypto.ShareMasterKeyData
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Response

/**
 * Base repository functionality
 */
abstract class Repository {

    val jsonConverter: Moshi
        get() = Api.moshi

    fun authenticateCall() = Api.tokenService.authenticateCall()

    fun updatePublicKey(publicKey: String) = Api.service.updateProfile(
        UpdateProfileRequest(publicKey)
    )

    fun storeEncryptedKeypair(token: String) =
        Api.service.storeEncryptedKeyPair(StoreKeyPairRequest(token))

    suspend fun createProfile(profile: ProfileData) = Api.service.createProfile(
        CreateProfileRequest(
            profile
        )
    )

    open fun fetchProfile() = Api.service.fetchProfile()

    open fun deleteProfile() = Api.service.deleteProfile()

    open suspend fun fetchProfileAsync() = Api.service.fetchProfileAsync()

    open suspend fun secretItems(): ApiResponse<EncryptedItemsResponse> =
        Api.service.getSecretItems()

    open suspend fun getAllFolders(): ApiResponse<FoldersResponse> = Api.service.folders()

    open fun createSecretItemCall(item: EncryptedItem) = Api.service.createSecretItemCall(item)

    open suspend fun createSecretItem(
        item: SecretItem,
        crypto: Crypto
    ): ApiResponse<EncryptedItemResponse> {
        val encryptedItem = item.encrypt(jsonConverter, crypto)
        return Api.service.createSecretItem(encryptedItem)
    }

    open fun deleteSecretItemCall(id: String) = Api.service.deleteSecretItemCall(id)

    open suspend fun getSecretItem(uuid: String): ApiResponse<EncryptedItemResponse> =
        Api.service.getSecretItem(uuid)

    open fun getSecretItemCall(uuid: String) = Api.service.getSecretItemCall(uuid)

    open fun updateSecretItemCall(item: EncryptedItem) = Api.service.updateSecretItemCall(
        item.id!!,
        item
    )

    open suspend fun updateSecretItem(item: EncryptedItem) = Api.service.updateSecretItem(
        item.id!!,
        item
    )

    open suspend fun deleteSecretItem(id: String): ApiResponse<DeleteResponse> =
        Api.service.deleteSecretItem(id)

    open suspend fun sendSurveyAnswer(answer: SurveyAnswer) = Api.service.sendSurveyAnswer(answer)

    @Deprecated(message = "This is a legacy call. V2 encryption keeps its own history in the details object")
    open suspend fun fetchPasswordVersionsAsync(secretId: String) =
        Api.service.fetchPasswordVersionsAsync(secretId)

    open suspend fun getBrand(url: String) = Api.service.getBrand(url)

    suspend fun searchBrands(prefix: String, pageSize: Int) =
        Api.service.searchBrands(prefix, pageSize)

    // The Client should wait 0.5 seconds when he received the response
    // (so server has time to retrieve the values with brandfetch, takes 0,35-0,45S).
    // 1. do the "search request" and handle results (these can contain brands without logo information)
    //2.  0.5 second and make a separate call to the "request brand" API-endpoint in order to retreive the brand details.
    open suspend fun searchBrandsAndFetchBrandInfo(
        prefix: String,
        pageSize: Int
    ): ApiResponse<List<Brand>> {
        val brands = searchBrands(prefix, pageSize)
        if (brands is ApiSuccessResponse) {
            delay(500)
            return coroutineScope {
                val asyncBrandFetchers = brands.body.map { brand ->
                    async(Dispatchers.IO) {
                        if (brand.isFetched != true) {
                            val newBrand = getBrand(brand.domain)
                            if (newBrand is ApiSuccessResponse) {
                                return@async newBrand.body
                            } else {
                                return@async brand
                            }
                        } else {
                            return@async brand
                        }
                    }
                }
                val results: List<Brand> = asyncBrandFetchers.awaitAll()
                return@coroutineScope ApiResponse.create(
                    response = Response.success(results)
                )
            }
        } else {
            return brands
        }
    }

    open suspend fun createShareLink(id: String, data: ShareMasterKeyData) =
        Api.service.createShareFolderLink(id, ShareLinkRequest(ShareLinkRequestData(data)))

    open suspend fun deleteShareLink(uuid: String) = Api.service.deleteShareFolderLink(uuid)

    open suspend fun updateUserSettings(settings: UserSettings) =
        Api.service.updateUserSettings(settings)

    open suspend fun getPublicKey(email: String) =
        Api.service.getPublicKey(email)

    open suspend fun createFolder(
        itemId: String,
        name: String,
        publicKek: String,
        ourUserId: String,
        ourEncryptedKek: String,
        theirUserId: String?,
        theirEncryptedKek: String?,
        theirEmail: String,
        encryptedFolderDek: String,
    ) = Api.service.createFolder(
        data = CreateFolderRequest.create(
            itemId = itemId,
            name = name,
            encryptedFolderDek = encryptedFolderDek,
            ourUserId = ourUserId,
            ourEncryptedPrivateKey = ourEncryptedKek,
            theirUserId = theirUserId,
            theirEncryptedPrivateKey = theirEncryptedKek,
            theirEmail = theirEmail,
            publicKey = publicKek
        )
    )

    open suspend fun addUserToFolder(
        folderId: String,
        theirUserId: String?,
        theirEncryptedKek: String?,
        theirEmail: String
    ) = Api.service.addUserToFolder(
        folderId,
        data = AddToFolderRequest.create(
            theirUserId = theirUserId,
            theirEncryptedPrivateKey = theirEncryptedKek,
            theirEmail = theirEmail
        )
    )

    open suspend fun revokeUser(folderId: String, membershipId: String) =
        Api.service.revokeUser(folderId, membershipId)

    open suspend fun instantAcceptInvite(folderId: String, membershipId: String, token: String) =
        Api.service.instantAcceptInvite(
            folderId,
            membershipId,
            AcceptanceRequest(token)
        )

    open suspend fun getContacts() = Api.service.getContacts()

}

/**/
fun <T> createNetworkCall(call: ResponseLiveData<T>): ResourceLiveData<T> {
    return object : NetworkBoundResource<T>() {
        override fun createCall(): LiveData<ApiResponse<T>> {
            return call
        }
    }.asLiveData()
}