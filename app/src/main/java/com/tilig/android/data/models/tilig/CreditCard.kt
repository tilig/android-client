package com.tilig.android.data.models.tilig

import androidx.compose.ui.text.input.TransformedText
import com.squareup.moshi.Json
import com.tilig.android.utils.CardUtils
import com.tilig.android.utils.CardUtils.BULLET
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

enum class CardType {
    JCB, AMEX, DINERS_CLUB, VISA, MASTERCARD, DISCOVER, MAESTRO, UNKNOWN
}

@Parcelize
data class CreditCard(

    override var id: String?,
    override var createdAt: String?,
    override var updatedAt: String?,
    override var encryptionVersion: Int = ENCRYPTION_VERSION_NEW,

    override var template: String? = DEFAULT_CREDIT_CARD_TYPE,
    override var overview: ItemOverview,
    override var details: ItemDetails,
    override var encryptedDetails: String? = null,

    // We include those in the constructor, otherwise Kotlin operations like `.copy` will not include them.
    @Transient @Json(ignore = true)
    override var areDetailsDecrypted: Boolean = false,
    @Transient @Json(ignore = true)
    override var decryptedDek: ByteArray? = null,
    @Transient @Json(ignore = true)
    override var legacyEncryptionDisabled: Boolean? = false,

    override var encryptedFolderKey: String? = null,
    override var folder: Folder? = null,
    override var share: ShareLink? = null,
) : SecretItem {

    companion object {
        fun createEmpty() = CreditCard(
            id = null,
            createdAt = null,
            updatedAt = null,
            encryptionVersion = ENCRYPTION_VERSION_NEW,
            overview = ItemOverview.createEmpty(),
            details = ItemDetails.createEmpty(),
            encryptedDetails = null,
        ).apply {
            areDetailsDecrypted = true
        }
    }

    @IgnoredOnParcel
    @Json(ignore = true)
    var holderName: String?
        set(value) {
            assert(areDetailsDecrypted)
            if (value == null) {
                details.clearField(ItemFieldType.CARD_HOLDER_NAME)
            } else {
                details.setField(
                    value,
                    ItemFieldType.CARD_HOLDER_NAME
                )
            }
        }
        get() {
            assert(areDetailsDecrypted)
            return details.findField(ItemFieldType.CARD_HOLDER_NAME)
        }

    @IgnoredOnParcel
    @Json(ignore = true)
    var expireDate: String?
        set(value) {
            assert(areDetailsDecrypted)
            if (value == null) {
                details.clearField(ItemFieldType.CARD_EXPIRES)
            } else {
                details.setField(
                    value,
                    ItemFieldType.CARD_EXPIRES
                )
            }
        }
        get() {
            assert(areDetailsDecrypted)
            return details.findField(ItemFieldType.CARD_EXPIRES)
        }

    @IgnoredOnParcel
    @Json(ignore = true)
    var cardNumber: String?
        set(value) {
            overview.info = value?.let { CardUtils.getLast4CardDigits(it) }
            assert(areDetailsDecrypted)
            if (value == null) {
                details.clearField(ItemFieldType.CARD_NUMBER)
            } else {
                details.setField(
                    value,
                    ItemFieldType.CARD_NUMBER
                )
            }
        }
        get() {
            assert(areDetailsDecrypted)
            return details.findField(ItemFieldType.CARD_NUMBER)
        }

    @IgnoredOnParcel
    @Json(ignore = true)
    var securityCode: String?
        set(value) {
            assert(areDetailsDecrypted)
            if (value == null) {
                details.clearField(ItemFieldType.CARD_SECURITY_CODE)
            } else {
                details.setField(
                    value,
                    ItemFieldType.CARD_SECURITY_CODE
                )
            }
        }
        get() {
            assert(areDetailsDecrypted)
            return details.findField(ItemFieldType.CARD_SECURITY_CODE)
        }

    @IgnoredOnParcel
    @Json(ignore = true)
    var extraInfo: String?
        set(value) {
            assert(areDetailsDecrypted)
            details.notes = value ?: ""
        }
        get() {
            assert(areDetailsDecrypted)
            return details.notes
        }

    @IgnoredOnParcel
    @Json(ignore = true)
    var pinCode: String?
        set(value) {
            assert(areDetailsDecrypted)
            if (value == null) {
                details.clearField(ItemFieldType.CARD_PIN_CODE)
            } else {
                details.setField(
                    value,
                    ItemFieldType.CARD_PIN_CODE
                )
            }
        }
        get() {
            assert(areDetailsDecrypted)
            return details.findField(ItemFieldType.CARD_PIN_CODE)
        }

    @IgnoredOnParcel
    @Json(ignore = true)
    var zipCode: String?
        set(value) {
            assert(areDetailsDecrypted)
            if (value == null) {
                details.clearField(ItemFieldType.CARD_ZIPCODE)
            } else {
                details.setField(
                    value,
                    ItemFieldType.CARD_ZIPCODE
                )
            }
        }
        get() {
            assert(areDetailsDecrypted)
            return details.findField(ItemFieldType.CARD_ZIPCODE)
        }

    /**
     * on items list always display card number in format
     * **** **** **** ****, even if it is invalid
     */
    fun getCardNumberForOverview(): TransformedText {
        var number = overview.info
        if (number.isNullOrBlank()) {
            number = cardNumber
        }
        if (number.isNullOrBlank()) {
            return CardUtils.emptyNumber()
        }
        return CardUtils.maskCardNumber(
            BULLET.toString().repeat(16 - number.length).plus(number),
            BULLET
        )
    }

    override fun toDebugString(): String = """
        name = $name
        holderName = $holderName
        expireDate = $expireDate
        cardNumber = $cardNumber
        securityCode = $securityCode
        extraInfo = $extraInfo
        pinCode = $pinCode
        dek = ${decryptedDek?.joinToString { ", " }}
    """
}
