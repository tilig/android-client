package com.tilig.android.data.api

import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.EnumJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.tilig.android.data.api.services.ApiService
import com.tilig.android.data.api.services.TokenAuthService
import com.tilig.android.data.models.tilig.ItemFieldType
import com.tilig.android.ui.onboarding.getstarted.components.Step
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

object Api : KoinComponent {

    private val httpLoggingInterceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    // Removed, because it was only used if ApiHelper.ENABLE_CACHE_INTERCEPTOR was enabled, which it isn't
    //private val connectivity: ConnectivityLiveData by inject()
    private val cache: Cache by inject()

    private val okHttpClient: OkHttpClient = ApiHelper.createOkHttpClient(
        cache = cache,
        connectivity = null, // connectivity
        useBearerToken = true,
        enableTokenAuthenticator = true,
        debugInterceptor = httpLoggingInterceptor
    )
    private val okHttpClientLogin: OkHttpClient = ApiHelper.createOkHttpClient(
        cache = null,
        connectivity = null, // connectivity
        useBearerToken = false,
        enableTokenAuthenticator = false,
        debugInterceptor = httpLoggingInterceptor
    )


    /**
     * Moshi, our trusted JSON parser.
     */
    val moshi: Moshi = Moshi.Builder().addLast(KotlinJsonAdapterFactory())
        // Note: add all response enums here
        .add(
            ItemFieldType::class.java,
            EnumJsonAdapter.create(ItemFieldType::class.java)
                .withUnknownFallback(ItemFieldType.UNKNOWN)
                .nullSafe()
        )
        .add(
            Step::class.java,
            EnumJsonAdapter.create(Step::class.java)
                .withUnknownFallback(Step.UNKNOWN)
                .nullSafe()
        )
        .build()

    private val retrofit = ApiHelper.createRetrofitForClient(okHttpClient)

    private val retrofitNoCache = ApiHelper.createRetrofitForClient(okHttpClientLogin)

    val service: ApiService = retrofit.create(ApiService::class.java)

    val tokenService: TokenAuthService = retrofitNoCache.create(TokenAuthService::class.java)


    // In case of mocking responses, break glass:
//    val mockInterceptor = MockInterceptor()
//    val mockClient = OkHttpClient.Builder()
//        .cache(cache)
//        .addInterceptor(httpLoggingInterceptor)
//        .addInterceptor(mockInterceptor)
//        .build()
//    val mockService = ApiHelper.createRetrofitForClient(mockClient).create(ApiService::class.java)
}
