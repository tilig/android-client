package com.tilig.android.data.models.tilig

import com.squareup.moshi.Json
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class WifiPassword(
    override var id: String?,
    override var createdAt: String?,
    override var updatedAt: String?,
    override var encryptionVersion: Int = ENCRYPTION_VERSION_NEW,

    override var template: String? = DEFAULT_WIFI_PASSWORD_TYPE,
    override var overview: ItemOverview,
    override var details: ItemDetails,
    override var encryptedDetails: String? = null,

    // We include those in the constructor, otherwise Kotlin operations like `.copy` will not include them.
    @Transient @Json(ignore = true)
    override var areDetailsDecrypted: Boolean = false,
    @Transient @Json(ignore = true)
    override var decryptedDek: ByteArray? = null,
    @Transient @Json(ignore = true)
    override var legacyEncryptionDisabled: Boolean? = false,

    override var encryptedFolderKey: String? = null,
    override var folder: Folder? = null,
    override var share: ShareLink? = null,
) : SecretItem {

    companion object {
        fun createEmpty() = WifiPassword(
            id = null,
            createdAt = null,
            updatedAt = null,
            encryptionVersion = ENCRYPTION_VERSION_NEW,
            overview = ItemOverview.createEmpty(),
            details = ItemDetails.createEmpty(),
            encryptedDetails = null,
        ).apply {
            areDetailsDecrypted = true
        }
    }

    @IgnoredOnParcel
    @Json(ignore = true)
    var password: String?
        set(value) {
            assert(areDetailsDecrypted)
            if (value == null) {
                details.clearField(ItemFieldType.PASSWORD)
            } else {
                details.setField(
                    value,
                    ItemFieldType.PASSWORD
                )
            }
        }
        get() {
            assert(areDetailsDecrypted)
            return details.findField(ItemFieldType.PASSWORD)
        }

    @IgnoredOnParcel
    @Json(ignore = true)
    var extraInfo: String?
        set(value) {
            assert(areDetailsDecrypted)
            details.notes = value ?: ""
        }
        get() {
            assert(areDetailsDecrypted)
            return details.notes
        }

    @IgnoredOnParcel
    @Json(ignore = true)
    var networkName: String?
        set(value) {
            assert(areDetailsDecrypted)
            if (value == null) {
                details.clearField(ItemFieldType.SSID)
            } else {
                details.setField(
                    value,
                    ItemFieldType.SSID
                )
            }
        }
        get() {
            assert(areDetailsDecrypted)
            return details.findField(ItemFieldType.SSID)
        }

    override fun toDebugString(): String = """
        name = $name
        password = $password
        networkName = $networkName
        extraInfo = $extraInfo
        dek = ${decryptedDek?.joinToString { ", " }}
    """
}