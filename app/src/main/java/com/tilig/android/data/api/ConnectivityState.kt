package com.tilig.android.data.api

enum class ConnectivityState {
    Connected,
    Disconnected
}
