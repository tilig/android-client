package com.tilig.android.data.models.tilig

import android.os.Parcelable
import androidx.annotation.VisibleForTesting
import com.squareup.moshi.Json
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue

typealias ApplicationProfileSettings = Map<String, @RawValue Any?>

private enum class ApplicationSettingsItemFieldType(val key: String) {
    @Json(name = "clients_signed_in")
    CLIENTS_SIGNED_IN(key = "clients_signed_in"),
}

@Parcelize
data class ApplicationSettings(
    @Json(name = "application_settings")
    var allSettingsMap: ApplicationProfileSettings = emptyMap()
) : Parcelable {

    @IgnoredOnParcel
    @Json(ignore = true)
    val clientsSignedIn: List<String>
        get() {
            return findField(
                ApplicationSettingsItemFieldType.CLIENTS_SIGNED_IN.key
            ) as? List<String> ?: emptyList()
        }

    @IgnoredOnParcel
    val isDesktopConnected = clientsSignedIn.contains("Web")

    /**
     * Write the value into appropriate settings section
     */
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun setField(type: String, value: Any) {
        val mutableSettings = mutableMapOf<String, @RawValue Any?>()
        mutableSettings.putAll(allSettingsMap)
        mutableSettings[type] = value
        allSettingsMap = mutableSettings
    }

    /**
     * @return the value of the field with the requested type in the requested section, or null.
     */
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun findField(type: String): Any? {
        return allSettingsMap[type]
    }
}