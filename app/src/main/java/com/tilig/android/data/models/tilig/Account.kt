package com.tilig.android.data.models.tilig

import android.content.Context
import android.view.View
import androidx.autofill.HintConstants
import com.squareup.moshi.Json
import com.squareup.moshi.Moshi
import com.tilig.android.BuildConfig
import com.tilig.android.autofill.PackageVerifier
import com.tilig.android.data.qrscanner.MalformedTokenException
import com.tilig.android.data.qrscanner.Token
import com.tilig.android.utils.getDomain
import com.tilig.crypto.Crypto
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import java.util.*

abstract class LegacyFields {
    @Deprecated(message = "Deprecated working towards v2 migration")
    abstract var legacyName: String?
    abstract var legacyEncryptedUsername: String?
    abstract var legacyEncryptedNotes: String?
    abstract var legacyEncryptedSecret: String?
    abstract var legacyAndroidAppId: String?
    abstract var legacyEncryptedOtp: String?
}

@Parcelize
data class Account(
    override var id: String?,
    override var createdAt: String?,
    override var updatedAt: String?,
    override var encryptionVersion: Int = ENCRYPTION_VERSION_NEW,

    // This is what we use to match w/ brands.
    // NOT what is used in the AccountMatcher to match websites or apps
    var domain: String?,
    var brand: Brand?,
    override var share: ShareLink?,

    override var template: String? = DEFAULT_ACCOUNT_TYPE,
    override var overview: ItemOverview,
    override var details: ItemDetails,
    override var encryptedDetails: String? = null,

    // We include those in the constructor, otherwise Kotlin operations like `.copy` will not include them.
    @Transient @Json(ignore = true)
    override var areDetailsDecrypted: Boolean = false,
    @Transient @Json(ignore = true)
    override var decryptedDek: ByteArray? = null,
    @Transient @Json(ignore = true)
    override var legacyEncryptionDisabled: Boolean? = false,

    override var encryptedFolderKey: String? = null,
    override var folder: Folder? = null,
) : SecretItem {

    /**
     * By default, an account is saved when it comes from the API.
     * Any modifications made sets this flag to FALSE so we remember to save it.
     */
    @IgnoredOnParcel
    @Json(ignore = true)
    var isSaved = true

    /**
     * Used when matching accounts to apps or websites
     */
    @IgnoredOnParcel
    @Json(ignore = true)
    var matchingScore: Float = 0f

//    init {
//        // FIXME remove this debug code
//        if (BuildConfig.DEBUG) {
//            folder = Folder.createDummy()
//        }
//    }

    companion object {
        fun createEmpty() = Account(
            id = null,
            createdAt = null,
            updatedAt = null,
            encryptionVersion = ENCRYPTION_VERSION_NEW,
            domain = null,
            overview = ItemOverview.createEmpty(),
            details = ItemDetails.createEmpty(),
            encryptedDetails = null,
            brand = null,
            share = null,
        ).apply {
            isSaved = false
            areDetailsDecrypted = true
        }

        fun createBasic(name: String, website: String? = null, androidAppId: String? = null) =
            createEmpty().apply {
                this.website = website
                this.androidAppId = androidAppId
                this.name = name
                isSaved = false
                areDetailsDecrypted = true
            }
    }

    @IgnoredOnParcel
    @Json(ignore = true)
    var website: String?
        set(value) {
            if (value != null) {
                domain = value.getDomain()
                // Only add if it is not already added
                if (!overview.urls.any { it.url == value }) {
                    overview.urls.add(UrlField(name = null, url = value))
                } else {
                    // if url was added before we move it to the end of the list, as we use last added urls for the website field
                    Collections.swap(
                        overview.urls,
                        overview.urls.indexOfFirst { it.url == value },
                        overview.urls.lastIndex
                    )
                }
                isSaved = false
            }
        }
        get() = overview.urls.lastOrNull()?.url

    @Json(ignore = true)
    var androidAppId: String?
        set(value) {
            if (value != null) {
                // Only add if it is not already added
                if (value !in overview.androidAppIds) {
                    overview.androidAppIds.add(value)
                }
                isSaved = false
            }
        }
        get() = overview.androidAppIds.firstOrNull()

    fun getNonBlankDisplayNameOrNull(): String? = if (!name.isNullOrBlank()) {
        name
    } else {
        // .getDomain() includes a nullOrBlank check itself:
        website.getDomain()
    }

    @Json(ignore = true)
    var username: String?
        set(value) {
            overview.info = value
            if (areDetailsDecrypted) {
                if (value == null) {
                    details.clearField(ItemFieldType.USERNAME)
                } else {
                    details.setField(
                        value,
                        ItemFieldType.USERNAME
                    )
                }
            }
            isSaved = false
        }
        get() = overview.info ?: overview.username

    /**
     * Overriding the default, because SecretItem stores the otp in the custom fields,
     * but for accounts it is a main field.
     */
    @IgnoredOnParcel
    @Json(ignore = true)
    override var otp: String?
        set(value) {
            assert(areDetailsDecrypted)
            if (value == null) {
                details.clearField(ItemFieldType.TOTP)
            } else {
                details.setField(
                    value,
                    ItemFieldType.TOTP
                )
            }
        }
        get() {
            assert(areDetailsDecrypted)
            return details.findField(ItemFieldType.TOTP)
        }

    @IgnoredOnParcel
    @Json(ignore = true)
    var notes: String
        set(value) {
            assert(areDetailsDecrypted)
            details.notes = value
        }
        get() {
            assert(areDetailsDecrypted)
            return details.notes ?: ""
        }

    @IgnoredOnParcel
    @Json(ignore = true)
    var password: String?
        set(value) {
            assert(areDetailsDecrypted)
            if (value == null) {
                details.clearField(ItemFieldType.PASSWORD)
            } else {
                details.setField(
                    value,
                    ItemFieldType.PASSWORD
                )
            }
        }
        get() {
            assert(areDetailsDecrypted)
            return details.findField(ItemFieldType.PASSWORD)
        }

    /**
     * Returns a suggested name for this account, based on the
     * domain or the android app id
     */
    fun getHostOrAppName(context: Context): String = when {
        domain != null -> domain
        overview.androidAppIds.isEmpty() -> null
        else -> PackageVerifier.getAppName(context, overview.androidAppIds.first())
    } ?: ""

    fun hasAppId(): Boolean = !androidAppId.isNullOrEmpty()

    /**
     * Takes an Autofill hint and returns the appropriate field value.
     */
    fun fieldValueForHint(hint: String, jsonConverter: Moshi, crypto: Crypto): String {
        // Details if needed
        if (!areDetailsDecrypted) {
            decryptDetails(jsonConverter, crypto)
        }
        // See https://developer.android.com/reference/androidx/autofill/HintConstants
        // for a list of constants.
        return when {
            hint.contains(View.AUTOFILL_HINT_EMAIL_ADDRESS)
                    || hint.contains(View.AUTOFILL_HINT_USERNAME)
                    || hint.contains("newUsername")
            -> this.username?.trim()
            hint.contains(View.AUTOFILL_HINT_PASSWORD)
                    || hint.contains("newPassword")
                    || hint.contains("textPassword")
            -> this.password
            hint.contains(HintConstants.AUTOFILL_HINT_SMS_OTP)
            -> this.otpTokenNow()
            else -> null
        } ?: ""
    }

    fun getNotesLimit(): Int? = if (legacyEncryptionDisabled == true) null else 190

    override fun toDebugString(): String = """
        name = $name
        website = $website
        androidAppId = $androidAppId
        password = $password
        username = $username
        notes = $notes
        dek = ${decryptedDek?.joinToString { ", " }}
    """
}
