package com.tilig.android.data.api.services

import com.tilig.android.data.models.ApiResponse
import com.tilig.android.data.models.TrackingEvent
import com.tilig.android.data.models.tilig.*
import retrofit2.Call
import retrofit2.http.*

/**
 * Authorized methods for the Secrets API
 */
interface ApiService {

    /******* V3 ************/

    @PUT("v3/profile")
    fun updateProfile(@Body profile: UpdateProfileRequest): Call<OkResponse>

    @POST("v3/keypairs")
    fun storeEncryptedKeyPair(@Body keyPair: StoreKeyPairRequest): Call<OkResponse>

    @GET("v3/profile")
    fun fetchProfile(): Call<Profile>

    @DELETE("v3/profile")
    fun deleteProfile(): Call<OkResponse>

    @GET("v3/profile")
    suspend fun fetchProfileAsync(): ApiResponse<Profile>

    @PUT("v3/profile")
    suspend fun createProfile(@Body profile: CreateProfileRequest): ApiResponse<Profile>

    @GET("v3/items/{uuid}")
    suspend fun getSecretItem(@Path("uuid") uuid: String): ApiResponse<EncryptedItemResponse>

    @GET("v3/items/{uuid}")
    fun getSecretItemCall(@Path("uuid") uuid: String): Call<EncryptedItemResponse>

    @PUT("v3/items/{uuid}")
    suspend fun updateSecretItem(
        @Path("uuid") uuid: String,
        @Body data: AccountRequest
    ): ApiResponse<EncryptedItemResponse>

    @PUT("v3/items/{uuid}")
    fun updateSecretItemCall(
        @Path("uuid") uuid: String,
        @Body data: AccountRequest
    ): Call<EncryptedItemResponse>

    @DELETE("v3/items/{uuid}")
    suspend fun deleteSecretItem(@Path("uuid") uuid: String): ApiResponse<DeleteResponse>

    @DELETE("v3/items/{uuid}")
    fun deleteSecretItemCall(@Path("uuid") uuid: String): Call<DeleteResponse>

    @POST("v3/items")
    fun createSecretItemCall(@Body data: EncryptedItem): Call<EncryptedItemResponse>

    @POST("v3/secrets")
    suspend fun createSecretItem(@Body data: EncryptedItem): ApiResponse<EncryptedItemResponse>

    @GET("v3/items")
    suspend fun getSecretItems(
        @Query("include_template") includeTemplate: String = listOf(
            DEFAULT_NOTE_TYPE,
            DEFAULT_CREDIT_CARD_TYPE,
            DEFAULT_WIFI_PASSWORD_TYPE,
            DEFAULT_CUSTOM_TYPE
        ).joinToString(",")
    ): ApiResponse<EncryptedItemsResponse>

    @POST("v3/track")
    fun trackEvent(@Body data: TrackingEvent): Call<OkResponse>

    @POST("v3/answers")
    suspend fun sendSurveyAnswer(@Body data: SurveyAnswer): ApiResponse<SurveyAnswerResponse>

    @Deprecated(message = "This is a legacy call. V2 encryption keeps its own history in the details object")
    @GET("v3/secrets/{secret_id}/password_versions")
    suspend fun fetchPasswordVersionsAsync(@Path("secret_id") secretId: String): ApiResponse<List<LegacyHistoryEntry>>

    @GET("v3/brands")
    suspend fun getBrand(@Query("url") url: String): ApiResponse<Brand>

    @GET("v3/brands/search")
    suspend fun searchBrands(
        @Query("prefix") prefix: String,
        @Query("page_size") pageSize: Int
    ): ApiResponse<Brands>

    @POST("v3/items/{secret_id}/share_link")
    suspend fun createShareFolderLink(
        @Path("secret_id") uuid: String,
        @Body data: ShareLinkRequest
    ): ApiResponse<ShareLinkFolderResponse>

    @DELETE("v3/items/{secret_id}/share_link")
    suspend fun deleteShareFolderLink(@Path("secret_id") uuid: String): ApiResponse<OkResponse>

    @PUT("v3/profile/user_settings")
    suspend fun updateUserSettings(@Body data: UserSettings): ApiResponse<OkResponse>

    @GET("v3/users/public_key")
    suspend fun getPublicKey(
        @Query("email") email: String
    ): ApiResponse<PublicKeyResponse>

    @GET("v3/folders/")
    suspend fun folders(): ApiResponse<FoldersResponse>

    @POST("v3/folders/")
    suspend fun createFolder(@Body data: CreateFolderRequest): ApiResponse<FolderResponse>

    @POST("v3/folders/{folder_id}/folder_memberships")
    suspend fun addUserToFolder(@Path("folder_id") folderId: String, @Body data: AddToFolderRequest): ApiResponse<FolderMembershipResponse>

    @POST("v3/folders/{folder_id}/folder_memberships/{member_id}/acceptance")
    suspend fun instantAcceptInvite(@Path("folder_id") folderId: String, @Path("member_id") memberId: String, @Body data: AcceptanceRequest): ApiResponse<OkResponse>

    @DELETE("v3/folders/{folder_id}/folder_memberships/{member_id}")
    suspend fun revokeUser(@Path("folder_id") folderId: String, @Path("member_id") memberId: String): ApiResponse<OkResponse>

    @GET("v3/contacts")
    suspend fun getContacts(): ApiResponse<ContactsResponse>
}