package com.tilig.android.data.addaccount

data class SuggestedBrand(
    val name: String,
    val url: String,
    val icon: Int
)