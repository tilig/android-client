package com.tilig.android.data.models.tilig

enum class GetStartedStep {
    BIOMETRIC,
    ADD_LOGIN,
    TRY_AUTOFILL,
    CONNECT_DESKTOP
}