package com.tilig.android.data.repository

import org.koin.core.component.KoinComponent

/**
 * Default implementation of the repository.
 */
class RepositoryImpl :
    Repository(), KoinComponent