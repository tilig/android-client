package com.tilig.android.data.models

import com.tilig.android.R
import retrofit2.Response
import java.net.UnknownHostException

/**
 * Common class used by API responses.
 * @param <T> the type of the response object
</T> */
@Suppress("unused") // T is used in extending classes
sealed class ApiResponse<T> {
    companion object {
        fun <T> create(error: Throwable): ApiErrorResponse<T> {
            return ApiErrorResponse.fromThrowable(error)
        }

        fun <T> create(response: Response<T>): ApiResponse<T> {
            return if (response.isSuccessful) {
                val body = response.body()
                if (body == null || response.code() == 204) {
                    ApiEmptyResponse()
                } else {
                    ApiSuccessResponse(
                        body = body
                    )
                }
            } else {
                val msg = response.errorBody()?.string()
                val errorMsg: String? = when {
                    msg.isNullOrEmpty() -> {
                        response.message()
                    }
                    else -> {
                        msg
                    }
                }
                ApiErrorResponse(ApiErrorResponse.TYPE_API, errorMsg ?: "server returned an error")
            }
        }

        fun <T> createCloneWithNewBody(response: ApiResponse<*>, newBody: T?) : ApiResponse<T> {
            return when (response) {
                is ApiEmptyResponse -> {
                    ApiEmptyResponse()
                }
                is ApiErrorResponse -> {
                    ApiErrorResponse(response.errorCode, response.errorMessage)
                }
                else -> {
                    ApiSuccessResponse(
                        body = newBody!!
                    )
                }
            }
        }
    }
}

/**
 * separate class for HTTP 204 responses so that we can make ApiSuccessResponse's body non-null.
 */
class ApiEmptyResponse<T> : ApiResponse<T>()

data class ApiSuccessResponse<T>(
    val body: T
) : ApiResponse<T>()

data class ApiErrorResponse<T>(val errorCode: Int, val errorMessage: String) : ApiResponse<T>() {

    fun toUiError(): UiError =
        when(errorCode) {
            TYPE_UNKNOWN_HOST -> UiError(null, R.string.error_connection)
            TYPE_API -> UiError(errorMessage, R.string.error_api)
            else -> UiError(errorMessage, R.string.error_generic)
        }

    companion object {

        const val TYPE_UNKNOWN = 0
        const val TYPE_UNKNOWN_HOST = 1
        const val TYPE_API = 2

        fun <T> fromThrowable(error: Throwable): ApiErrorResponse<T> = when(error) {
            is UnknownHostException -> ApiErrorResponse(TYPE_UNKNOWN_HOST,  error.message ?: "unknown host")
            else -> ApiErrorResponse(TYPE_UNKNOWN, error.message ?: "unknown error")
        }
    }
}

inline fun <reified T> ApiResponse<*>.isOfType(): Boolean =
    if (this is T)
        @Suppress("UNCHECKED_CAST")
        true else false

data class UiError(
    val message: String? = null,
    val msgResource: Int? = null
)
