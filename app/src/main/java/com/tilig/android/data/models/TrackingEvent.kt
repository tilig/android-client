package com.tilig.android.data.models

data class TrackingEvent(
    val event:String,
    val properties: HashMap<String, String>
)
