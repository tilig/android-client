package com.tilig.android.data.repository

import android.util.Log
import com.tilig.android.data.api.MockErrorResponseCall
import com.tilig.android.data.models.ApiResponse
import com.tilig.android.data.models.tilig.*
import com.tilig.android.utils.SharedPrefs
import com.tilig.crypto.Crypto
import com.tilig.crypto.ShareMasterKeyData
import org.koin.core.component.KoinComponent
import retrofit2.Call

private const val MOCK_ERROR_TAG = "Mock Repository"

/**
 * This class will help to mock responses for the test purposes.
 * Use in combination with dev menu
 */
class RepositoryMockImpl(private val prefs: SharedPrefs) :
    Repository(), KoinComponent {

    override fun fetchProfile(): Call<Profile> {
        Log.d(MOCK_ERROR_TAG, "Error fetching profile")
        return MockErrorResponseCall(super.fetchProfile(), "Error fetching profile")
    }

    override fun deleteProfile(): Call<OkResponse> {
        Log.d(MOCK_ERROR_TAG, "Error delete profile")
        return MockErrorResponseCall(super.deleteProfile(), "Error delete profile")
    }

    override suspend fun fetchProfileAsync(): ApiResponse<Profile> {
        Log.d(MOCK_ERROR_TAG, "Error fetching profile")
        return ApiResponse.create(Throwable("Error fetching profile"))
    }

    override suspend fun secretItems(): ApiResponse<EncryptedItemsResponse> {
        if (prefs.devToggleSimulateErrorResponseItemsList) {
            Log.d(MOCK_ERROR_TAG, "Error fetching secret items")
            return ApiResponse.create(Throwable("Error fetching secret items"))
        } else {
            return super.secretItems()
        }
    }

    override fun createSecretItemCall(item: EncryptedItem): Call<EncryptedItemResponse> {
        Log.d(MOCK_ERROR_TAG, "Failed to create an item")
        return MockErrorResponseCall(super.createSecretItemCall(item), "Failed to create an item")
    }

    override suspend fun createSecretItem(
        item: SecretItem,
        crypto: Crypto
    ): ApiResponse<EncryptedItemResponse> {
        Log.d(MOCK_ERROR_TAG, "Failed to create an item")
        return ApiResponse.create(Throwable("Failed to create an item"))
    }

    override fun deleteSecretItemCall(id: String): Call<DeleteResponse> {
        Log.d(MOCK_ERROR_TAG, "Failed to delete an item")
        return MockErrorResponseCall(super.deleteSecretItemCall(id), "Failed to delete an item")
    }

    override suspend fun getSecretItem(uuid: String): ApiResponse<EncryptedItemResponse> {
        if (prefs.devToggleSimulateErrorResponseItemDetails) {
            Log.d(MOCK_ERROR_TAG, "Failed to fetch an item details")
            return ApiResponse.create(Throwable("Failed to fetch an item details"))
        } else {
            return super.getSecretItem(uuid)
        }
    }

    override fun getSecretItemCall(uuid: String): Call<EncryptedItemResponse> {
        if (prefs.devToggleSimulateErrorResponseItemDetails) {
            Log.d(MOCK_ERROR_TAG, "Failed to fetch an item details")
            return MockErrorResponseCall(
                super.getSecretItemCall(uuid),
                "Failed to fetch an item details"
            )
        } else {
            return super.getSecretItemCall(uuid)
        }
    }

    override fun updateSecretItemCall(item: EncryptedItem): Call<EncryptedItemResponse> {
        Log.d(MOCK_ERROR_TAG, "Failed to update an item")
        return MockErrorResponseCall(super.updateSecretItemCall(item), "Failed to update an item")
    }

    override suspend fun updateSecretItem(item: EncryptedItem): ApiResponse<EncryptedItemResponse> {
        Log.d(MOCK_ERROR_TAG, "Failed to update an item")
        return ApiResponse.create(Throwable("Failed to update an item"))
    }

    override suspend fun deleteSecretItem(id: String): ApiResponse<DeleteResponse> {
        Log.d(MOCK_ERROR_TAG, "Failed to delete an item")
        return ApiResponse.create(Throwable("Failed to delete an item"))
    }

    override suspend fun getBrand(url: String): ApiResponse<Brand> {
        Log.d(MOCK_ERROR_TAG, "Failed to fetch a brand")
        return ApiResponse.create(Throwable("Failed to fetch a brand"))
    }

    override suspend fun searchBrandsAndFetchBrandInfo(
        prefix: String,
        pageSize: Int
    ): ApiResponse<List<Brand>> {
        Log.d(MOCK_ERROR_TAG, "Failed to search for a brand")
        return ApiResponse.create(Throwable("Failed to search for a brand"))
    }

    override suspend fun updateUserSettings(settings: UserSettings): ApiResponse<OkResponse> {
        Log.d(MOCK_ERROR_TAG, "Failed to update a user settings")
        return ApiResponse.create(Throwable("Failed to update a user settings"))
    }
}