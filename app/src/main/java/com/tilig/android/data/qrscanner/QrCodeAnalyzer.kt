package com.tilig.android.data.qrscanner

import android.graphics.ImageFormat
import android.util.Log
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.google.zxing.*
import com.google.zxing.common.HybridBinarizer
import com.google.zxing.qrcode.QRCodeReader
import java.lang.Boolean
import java.nio.ByteBuffer
import java.util.*
import kotlin.Any
import kotlin.ByteArray
import kotlin.Exception
import kotlin.String
import kotlin.Unit
import kotlin.also
import kotlin.apply


class QrCodeAnalyzer(
    private val onQrCodeScanned: (String) -> Unit
): ImageAnalysis.Analyzer {

    private val supportedImageFormats = listOf(
        ImageFormat.YUV_420_888,
        ImageFormat.YUV_422_888,
        ImageFormat.YUV_444_888,
    )

    private val qrCodeReader = QRCodeReader()

    private val hintsMap = EnumMap<DecodeHintType, Any>(
        DecodeHintType::class.java
    )

    init {
//        hintsMap[DecodeHintType.TRY_HARDER] = Boolean.TRUE
        hintsMap[DecodeHintType.POSSIBLE_FORMATS] = listOf(
            BarcodeFormat.QR_CODE
        )
    }

    override fun analyze(image: ImageProxy) {
        if(image.format in supportedImageFormats) {
            val bytes = image.planes.first().buffer.toByteArray()
            val source = PlanarYUVLuminanceSource(
                bytes,
                image.width,
                image.height,
                0,
                0,
                image.width,
                image.height,
                false
            )
            val binaryBmp = BinaryBitmap(HybridBinarizer(source))
            try {
                val result = qrCodeReader.decode(binaryBmp, hintsMap)
                onQrCodeScanned(result.text)
            } catch(e: Exception) {
                Log.e("QR", "No QR found", e)
            } finally {
                image.close()
            }
        }
    }

    private fun ByteBuffer.toByteArray(): ByteArray {
        rewind()
        return ByteArray(remaining()).also {
            get(it)
        }
    }
}
