package com.tilig.android.data.livedata

import androidx.lifecycle.LiveData
import com.tilig.android.data.models.ApiResponse
import com.tilig.android.data.models.Resource

typealias ResponseLiveData<T> = LiveData<ApiResponse<T>>

typealias ResourceLiveData<T> = LiveData<Resource<T>>
