package com.tilig.android.data.models.tilig

import com.squareup.moshi.Json

data class ProfileData(
    val display_name: String?,
    val given_name: String?,
    val family_name: String?,
    val picture: String?,
    val locale: String?,
    val provider_id: String?,
    val public_key: String?
)

data class ProfileDataKeyOnly(
    val public_key: String
)

data class CreateProfileRequest(
    val user: ProfileData
)

data class UpdateProfileRequest(
    val user: ProfileDataKeyOnly
) {
    constructor(publicKey: String) : this(ProfileDataKeyOnly(public_key = publicKey))
}

data class StoreKeyPairRequest(
    val encrypted_keypair: String
)

typealias AccountRequest = EncryptedItem

///// Folder creation //////
data class CreateMembership(
    @Json(name = "encrypted_private_key")
    val encryptedPrivateKey: String?,
    @Json(name = "user_id")
    val userId: String?,
    val email: String? = null
)

data class CreateItem(
    @Json(name = "encrypted_folder_dek")
    val encryptedFolderDek: String,
    val id: String
)

@Suppress("ArrayInDataClass")
data class CreateFolder(
    val name: String,
    @Json(name = "folder_memberships")
    val memberships: Array<CreateMembership>,
    val items: Array<CreateItem>,
    @Json(name = "public_key")
    val publicKey: String
)

data class CreateFolderRequest(
    val folder: CreateFolder
) {
    companion object {
        fun create(
            itemId: String,
            name: String,
            encryptedFolderDek: String,
            ourUserId: String,
            ourEncryptedPrivateKey: String,
            theirUserId: String?,
            theirEncryptedPrivateKey: String?,
            theirEmail: String,
            publicKey: String
        ): CreateFolderRequest {
            val ourMembership = CreateMembership(
                encryptedPrivateKey = ourEncryptedPrivateKey,
                userId = ourUserId
            )
            val theirMembership = if (theirUserId == null || theirEncryptedPrivateKey == null) {
                CreateMembership(encryptedPrivateKey = null,userId = null, email = theirEmail)
            } else {
                CreateMembership(
                    encryptedPrivateKey = theirEncryptedPrivateKey,
                    userId = theirUserId
                )
            }
            return CreateFolderRequest(
                folder = CreateFolder(
                    name = name,
                    publicKey = publicKey,
                    memberships = arrayOf(
                        ourMembership,
                        theirMembership
                    ),
                    items = arrayOf(
                        CreateItem(
                            encryptedFolderDek = encryptedFolderDek,
                            id = itemId
                        )
                    ),
                )
            )
        }
    }
}

data class AddToFolderRequest(
    @Json(name = "folder_membership")
    val membership: CreateMembership
) {
    companion object {
        fun create(
            theirUserId: String?,
            theirEncryptedPrivateKey: String?,
            theirEmail: String
        ) = AddToFolderRequest(
            membership =
            if (theirUserId == null || theirEncryptedPrivateKey == null) {
                CreateMembership(userId = null, encryptedPrivateKey = null, email = theirEmail)
            } else {
                CreateMembership(
                    encryptedPrivateKey = theirEncryptedPrivateKey,
                    userId = theirUserId
                )
            }
        )
    }
}

data class AcceptanceRequestToken(
    @Json(name = "acceptance_token")
    val acceptanceToken: String
)

data class AcceptanceRequest(
    val acceptance: AcceptanceRequestToken
) {
    constructor(token: String) : this(AcceptanceRequestToken(token))
}
