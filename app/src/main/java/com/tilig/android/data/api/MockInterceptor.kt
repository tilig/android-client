package com.tilig.android.data.api

import android.util.Log
import com.tilig.android.utils.readFile
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.Protocol
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody

class MockInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val encodedPath = chain.request().url.encodedPath
        Log.d("MockInterceptor", "Requesting $encodedPath")
        val path = when (encodedPath) {
            // You can add files here for paths that need to be mocked:
            // "/api/v1/user-profile/" -> "res/raw/user_profile_success.json"
            else -> encodedPath
        }
        val response = path.readFile(this)

        return if (response.isNullOrEmpty()) {
            Log.d("MockInterceptor", "JSON file for $path should exist and not be empty")
            chain.proceed(chain.request())
        } else {
            Response.Builder()
                .code(200)
                .message(response)
                .request(chain.request())
                .protocol(Protocol.HTTP_1_1)
                .body(response.toResponseBody("application/json".toMediaTypeOrNull()))
                .addHeader("content-type", "application/json")
                .build()
        }
    }
}