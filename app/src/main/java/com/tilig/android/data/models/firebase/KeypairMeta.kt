package com.tilig.android.data.models.firebase

data class KeypairMeta(
    val app_platform: String,
    val app_version: String
) {
    fun toFirebaseModel() = mapOf(
        "app_platform" to app_platform,
        "app_version" to app_version,
    )
}

data class EncryptKeypair(
    val private_key: String,
    val public_key: String,
    val key_type: String = "x25519"
) {
    fun toFirebaseModel() = mapOf(
        "private_key" to private_key,
        "public_key" to public_key,
        "key_type" to key_type,
    )
}

data class EncryptKeypairRequest(
    val key: EncryptKeypair,
    val meta: KeypairMeta
) {
    fun toFirebaseModel() = mapOf(
        "key" to key.toFirebaseModel(),
        "meta" to meta.toFirebaseModel(),
    )
}

data class DecryptKeypairRequest(
    val encryptedPrivateKey: String
) {
    fun toFirebaseModel() = mapOf(
        "encrypted_private_key" to encryptedPrivateKey,
    )
}
