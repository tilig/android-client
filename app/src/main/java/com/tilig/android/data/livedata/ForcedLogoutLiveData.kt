package com.tilig.android.data.livedata

import androidx.lifecycle.MutableLiveData

data class ForcedLogoutData(
    val forceLogout: Boolean,
    // was user deleted or not
    val userExists: Boolean = true
)
/**
 * This is used for global sign out if the API calls return a 401
 */
class ForcedLogoutLiveData : MutableLiveData<ForcedLogoutData>() {

    fun forceLogout(forceLogout: Boolean, userExists: Boolean = true) =
        postValue(ForcedLogoutData(forceLogout, userExists))

    /**
     * The app logs out when `true` is posted. However, the
     * LiveData lingers, so on any app open, it can keep logging
     * out. Therefore we post a `false` after a successful logout
     * so it doesn't keep happening
     */
    fun preventLoop() = postValue(ForcedLogoutData(false))
}
