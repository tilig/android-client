package com.tilig.android.data.api

import com.tilig.android.utils.SharedPrefs
import okhttp3.Interceptor
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

/**
 * Request interceptor for OkHttp, which
 * adds the Bearer token in the headers of the request
 */
class AuthorizationInterceptor(
    private val tokenType: String
) : Interceptor, KoinComponent {

    companion object {
        const val TOKEN_TYPE_BEARER = "Bearer"
        const val TOKEN_TYPE_TOKEN = "Token"
    }

    private val prefs: SharedPrefs by inject()

    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        var request = chain.request()
        var token : String? = null
        when(tokenType) {
            TOKEN_TYPE_BEARER -> {
                prefs.accessToken?.let {
                    token = it
                }
            }
            TOKEN_TYPE_TOKEN -> {
                token = prefs.refreshToken ?: prefs.jwtToken
            }
        }

        token?.let {
            val headers =
                request.headers.newBuilder().add("Authorization", "$tokenType $token").build()
            request = request.newBuilder().headers(headers).build()
        }

        return chain.proceed(request)
    }
}
