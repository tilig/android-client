import android.content.Context
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

typealias RelatedDomains = Array<String>
typealias RelatedApps = Array<String>

@Suppress("ArrayInDataClass")
data class Relation(
    val u: RelatedDomains?,
    val a: RelatedApps?
) {
    fun matchesDomain(domain: String?) = domain != null && u?.contains(domain) == true
    fun matchesApp(app: String?) = app != null && a?.contains(app) == true

    override fun toString(): String =
        "urls: ${u?.joinToString(",")} and apps: ${a?.joinToString(",")}"
}

object RelatedDomainsHelper {

    private const val ASSETS_FILE = "related-domains-and-app-ids.json"

    private var relations: List<Relation>? = null

    fun load(context: Context) {
        if (relations == null) {
            val moshi = Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()
            val listType = Types.newParameterizedType(List::class.java, Relation::class.java)
            val converter = moshi.adapter<List<Relation>>(listType)
            val json = context.assets.open(ASSETS_FILE).bufferedReader().use {
                it.readText()
            }

            relations = converter.fromJson(json)!!
        }
    }

    fun lookupDomain(domain: String?): Relation? = if (domain == null)
        null
    else
        relations?.find { group ->
            group.matchesDomain(domain)
        }

    fun lookupApp(app: String?): Relation? = if (app == null)
        null
    else
        relations?.find { group ->
            group.matchesApp(app)
        }

}
