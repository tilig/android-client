package com.tilig.android.data.models

data class TokenExchangeResponse(
    val refreshToken: String? = null,
    val accessToken: String? = null
)
