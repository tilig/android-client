package com.tilig.android.data.api

import com.tilig.android.BuildConfig
import okhttp3.Interceptor

/**
 * Request interceptor for OkHttp, which
 * adds 2 headers that are meant for statistics
 */
class CustomHeaderInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        var request = chain.request()

        val headers = request.headers.newBuilder()
            .add("x-tilig-version", "Android ${BuildConfig.VERSION_NAME}")
            .add("x-tilig-platform", "Android")
            .build()

        request = request.newBuilder().headers(headers).build()
        return chain.proceed(request)
    }
}
