package com.tilig.android.data.qrscanner

enum class TokenType {
    /**
     * HMAC-based One-Time Password (Hash-based Message Auth Code)
     */
    HOTP,

    /**
     * Time-based One-Time Password
     */
    TOTP
}