package com.tilig.android

import android.content.Intent
import android.hardware.biometrics.BiometricPrompt
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.activity.viewModels
import androidx.annotation.LayoutRes
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.analytics.Tracker
import com.tilig.android.utils.lock.LockEventListener
import com.tilig.android.utils.lock.LockProvider
import com.tilig.android.utils.lock.LockViewModel
import org.koin.core.component.inject

/**
 * Faceless Activity that launches the applicable screen lock
 */
abstract class LockActivity : BaseActivity, LockEventListener {

    private lateinit var delayedHandler: Handler
    internal val lockViewModel: LockViewModel by viewModels()
    private val tracker: Mixpanel by inject()

    companion object {

        const val LOCK_REQUEST_CODE: Int = 123
    }

    constructor() : super()

    constructor(@LayoutRes layout: Int) : super(layout)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        delayedHandler = Handler()
    }

    override fun onResume() {
        super.onResume()

        // Possibly show the screen lock
        if ((application as LockProvider).shouldShowScreenUnlock()) {
            lockViewModel.onAppStartLocked()
            showScreenUnlock()
        } else {
            onAppUnlocked()
        }
        if (!lockViewModel.lockUtil.canAuthorize(this)) {
            Toast.makeText(
                this,
                getString(R.string.alert_set_up_biometric_on_device),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    fun tryToUnlock() {
        // Possibly show the screen lock
        if ((application as LockProvider).shouldShowScreenUnlock()) {
            lockViewModel.onAppStartLocked()
            showScreenUnlock()
        } else {
            onAppUnlocked()
        }
    }

    private fun showScreenUnlock() {
        (application as LockProvider).apply {

            // Delaying the lock slightly because if might be a
            // fix for a NullPointerException reported in BiometricPrompt
            // (Sentry issue ANDROID-APP-8)
            // Workaround implemented for a bug in the androidx library,
            // that was supposedly fixed in lib update 1.0.1.
            // More info:
            // https://stackoverflow.com/questions/58089766/biometric-prompt-crashing-on-android-9-and-10-on-some-devices
            // https://issuetracker.google.com/issues/141838014
            delayedHandler.postDelayed({
                onAppStartLocked()
                if (shouldShowScreenUnlock()) {
                    maybeShowScreenUnlock(this@LockActivity, this@LockActivity, LOCK_REQUEST_CODE)
                } else {
                    onAppUnlocked()
                }
            }, 20)
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // This is executed when LockUtil is used on Oreo or below
        if (requestCode == LOCK_REQUEST_CODE) {
            delayedHandler.postDelayed({
                val ok = (application as LockProvider).onAuthorizationEvent(resultCode)
                if (ok) {
                    onAppUnlocked()
                } else {
                    onAppUnlockFailed(errorCode = 0)
                }
            }, 20)
        }
    }

    override fun onAppUnlocked() {
        // track only if biometric lock is enabled
        if ((application as LockProvider).isLockEnabled()) {
            tracker.trackEvent(Tracker.EVENT_BIOMETRICS_UNLOCKED)
        }
        lockViewModel.onAppUnlocked()
    }

    override fun onAppStartLocked() {
        lockViewModel.onAppStartLocked()
    }

    override fun onAppUnlockFailed(errorCode: Int) {
        lockViewModel.onAppUnlockFailed(errorCode)
    }

}
