package com.tilig.android

import com.tilig.android.data.models.tilig.SecretItem

sealed class MainRoute(val route: String) {
    object Home : MainRoute("home")

    object ItemDetails : MainRoute("details?item_id={item_id}") {
        const val ARGUMENT_ITEM_ID: String = "item_id"

        fun createRouteForItemId(item: SecretItem): String =
            "details?$ARGUMENT_ITEM_ID=${item.id}"
    }

    object QRCodeScanner : MainRoute("qr_code_scanner?account_id={account_id}") {
        const val ARGUMENT_ACCOUNT_ID: String = "account_id"

        fun createRouteForAccountId(accountId: String?) =
            "qr_code_scanner?${ARGUMENT_ACCOUNT_ID}=${accountId}"
    }

    object SecretItems : MainRoute("home/secrets")
    object Settings : MainRoute("home/settings")
    object ImportPasswords : MainRoute("home/import_pass")
    object Sharing : MainRoute("home/sharing")
    object GetStarted : MainRoute("home/get_started")
}
