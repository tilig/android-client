package com.tilig.android

import android.app.Application
import androidx.fragment.app.FragmentActivity
import androidx.startup.AppInitializer
import com.tilig.android.startup.TiligAppInitializer
import com.tilig.android.utils.lock.LockEventListener
import com.tilig.android.utils.lock.LockProvider
import com.tilig.android.utils.lock.LockUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent


@Suppress("unused") // It is referenced in the manifest you dummy
class TiligApp : Application(), KoinComponent, LockProvider {

    lateinit var lockUtil: LockUtil

    companion object {

        private val debounceState = MutableStateFlow { }
        private const val DEBOUNCE_TIMEOUT_MILLIS = 300L

        init {
            GlobalScope.launch(Dispatchers.Main) {
                debounceState
                    .debounce(DEBOUNCE_TIMEOUT_MILLIS)
                    .collect { onClick ->
                        // This should be null-safe, but Sentry reports don't lie! Better
                        // do a null-safe operator just to be sure.
                        // https://sentry.io/organizations/tilig/issues/3320701499/?environment=production&project=5237937&query=is%3Aunresolved+release.version%3A3.3.12&statsPeriod=7d
                        @Suppress("SAFE_CALL_WILL_CHANGE_NULLABILITY", "UNNECESSARY_SAFE_CALL")
                        onClick?.invoke()
                    }
            }
        }

        fun debounceClicks(onClick: () -> Unit) {
            debounceState.value = onClick
        }
    }

    override fun onCreate() {
        super.onCreate()

        AppInitializer
            .getInstance(this)
            .initializeComponent(TiligAppInitializer::class.java)
    }

    override fun shouldShowScreenUnlock() = lockUtil.shouldShowLock()

    override fun maybeShowScreenUnlock(
        activity: FragmentActivity,
        listener: LockEventListener,
        requestCode: Int
    ) =
        lockUtil.authorize(activity, listener, requestCode)

    override fun onAuthorizationEvent(resultCode: Int): Boolean =
        lockUtil.onAuthorizationEvent(resultCode)

    override fun isLockEnabled(): Boolean = lockUtil.isLockEnabled()
}