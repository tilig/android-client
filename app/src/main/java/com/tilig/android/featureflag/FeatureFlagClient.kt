package com.tilig.android.featureflag

import android.content.Context
import com.tilig.android.BuildConfig
import com.tilig.android.R
import io.getunleash.UnleashClient
import io.getunleash.UnleashConfig
import io.getunleash.UnleashContext
import kotlin.random.Random

/**
 * Class is used to control new feature rollouts.
 * Get feature flags from server side
 */
class FeatureFlagClient(private val appContext: Context) {

    companion object {
        // since 3.13 renamed (old name = clients-test-drive)
        val FLAG_TEST_DRIVE = "android-test_drive"
    }

    private val unleashClient: UnleashClient

    init {
        val config = UnleashConfig.newBuilder()
            .proxyUrl(BuildConfig.UNLEASH_API_PROXY)
            .clientKey(BuildConfig.UNLEASH_API_PROXY_SECRET)
            // prevent listening flag updates. Get result only once for a session. Could be updated in future if need
            // to listen flags updates in realtime
            .metricsInterval(0)
            .environment(if (BuildConfig.DEBUG) "stage" else "prod")
            .build()

        unleashClient =
            UnleashClient(unleashConfig = config, unleashContext = createUnleashContext(null))
    }

    fun setIdentity(identifier: String){
        val newContext = createUnleashContext(identifier)
        unleashClient.updateContext(newContext).get()
    }

    fun isFlagEnabled(flag: String) = unleashClient.isEnabled(flag, false)

    private fun createUnleashContext(userId: String?): UnleashContext {
        val contextBuilder = UnleashContext.newBuilder()
            .appName(appContext.getString(R.string.app_name))
            .sessionId("Android Session: ${Random.nextLong()}")
            .environment(if (BuildConfig.DEBUG) "stage" else "prod")
        if (userId != null) {
            contextBuilder.userId(userId)
        }
        return contextBuilder.build()
    }
}