package com.tilig.android

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.ui.graphics.toArgb
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateManager
import com.tilig.android.analytics.ConversionAnalytics
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.autofill.Autofill
import com.tilig.android.autofill.AutofillHelper
import com.tilig.android.startup.UpdateStatus
import com.tilig.android.startup.UpdateViewModel
import com.tilig.android.ui.theme.DefaultBackground
import com.tilig.android.ui.theme.StatusBarColor
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

abstract class BaseActivity : AppCompatActivity, KoinComponent {

    constructor() : super()

    constructor(@LayoutRes contentLayoutId: Int) : super(contentLayoutId)

    val autofill: Autofill
        get() = AutofillHelper.getAutofill(this)

    internal val mixpanel: Mixpanel by inject()
    internal val conversions: ConversionAnalytics by inject()
    private val updateHelper: UpdateViewModel by viewModels()
    private var updateManager: AppUpdateManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Force updates
        updateManager = updateHelper.checkForUpdates(this)

        lifecycleScope.launchWhenStarted {
            updateHelper.uiState.collect { state ->
                when (state.status ) {
                    UpdateStatus.DONE,
                    UpdateStatus.IDLE -> {}
                    UpdateStatus.UPDATE_AVAILABLE -> {
                        updateManager?.let {
                            updateHelper.startUpdate(
                                activity = this@BaseActivity,
                                appUpdateManager = it,
                                appUpdateInfo = state.info!!,
                                force = state.isHighPriority
                            )
                        }
                    }
                    UpdateStatus.APPROVED -> {
                        updateManager?.completeUpdate()
                    }
                    UpdateStatus.DOWNLOADING -> {
                        // no-op, can be used for custom UI
                    }
                    UpdateStatus.INSTALLING -> {
                        // no-op, can be used for custom UI
                    }
                    UpdateStatus.DOWNLOADED -> {
                        // This'll show an ImportantMessageBar in the SecretItemsScreen (HomeFragment)
                        // Or we'll update right away if this is not a flexible flow
                        if (state.isHighPriority) {
                            updateManager?.let {
                                updateHelper.startUpdate(
                                    activity = this@BaseActivity,
                                    appUpdateManager = it,
                                    appUpdateInfo = state.info!!,
                                    force = true
                                )
                            }
                        }
                    }
                    UpdateStatus.ERROR -> {

                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        // Copied from the Mixpanel sample app,
        // we ensure adding the superProperties here as well:
        mixpanel.ensureSuperProperties()

        // Listen for update updates
        updateManager?.let { updateHelper.registerListener(it) }
    }

    override fun onPause() {
        super.onPause()
        updateManager?.let { updateHelper.unregisterListener(it) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        updateHelper.handleActivityResult(requestCode, resultCode)
    }

    /**
     * This toggles the statusbar background color between
     * two states: dark and bright, and switches the icon colors
     * to be the contrasting color
     */
    @Deprecated(
        "Not to be used in Compose views",
        replaceWith = ReplaceWith("BaseFragmentCompose.setStatusbarColorBright()")
    )
    fun setStatusbarColorBright(bright: Boolean) {
        window?.apply {
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            statusBarColor = if (bright) {
                DefaultBackground.toArgb()
            } else {
                StatusBarColor.toArgb()
            }

            // And toggle bright/dark icons
            decorView.let {
                it.systemUiVisibility = if (bright)
                    it.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                else
                    it.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
            }
        }
    }

    /**
     * Gets the AppUpdateManager (if any).
     * I'm not a big fan of this method (calling a public method from the Activity) but the AppUpdateManager
     * is tied to the Activity Context and we only want one of it. We cannot inject it, and keeping it in the
     * ViewModel would create a reference from the ViewModel to the Context.
     */
    fun getAppUpdateManager() = updateManager

    internal fun snackbar(
        container: View,
        @StringRes msg: Int,
        duration: Int = Snackbar.LENGTH_LONG
    ) = snackbar(
        container,
        getString(msg),
        duration
    )

    internal fun snackbar(
        container: View,
        msg: String,
        duration: Int = Snackbar.LENGTH_LONG
    ) = Snackbar.make(
        container,
        msg,
        duration
    ).apply {
        show()
    }
}
