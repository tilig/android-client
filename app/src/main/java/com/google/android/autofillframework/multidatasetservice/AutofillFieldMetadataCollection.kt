/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.android.autofillframework.multidatasetservice

import android.os.Build
import android.service.autofill.SaveInfo
import android.util.Log
import android.view.autofill.AutofillId
import androidx.annotation.RequiresApi
import androidx.autofill.HintConstants
import com.tilig.android.autofill.google.AutofillFieldMetadata

/**
 * Data structure that stores a collection of `AutofillFieldMetadata`s. Contains all of the client's `View`
 * hierarchy autofill-relevant metadata.
 */
@RequiresApi(Build.VERSION_CODES.O)
data class AutofillFieldMetadataCollection @JvmOverloads constructor(
    val autofillIds: ArrayList<AutofillId> = ArrayList(),
    val requiredAutosaveIds: ArrayList<AutofillId> = ArrayList(),
    val optionalAutosaveIds: ArrayList<AutofillId> = ArrayList(),
    val allAutofillHints: ArrayList<String> = ArrayList(),
    val focusedAutofillHints: ArrayList<String> = ArrayList(),
    val ignoreIds: ArrayList<AutofillId> = ArrayList(),
    val submitButtonIds: ArrayList<AutofillId> = ArrayList()
) {

    private var otpFieldCount = 0
    private val autofillHintsToFieldsMap = HashMap<String, MutableList<AutofillFieldMetadata>>()
    var saveType = 0
        private set

    fun add(autofillFieldMetadata: AutofillFieldMetadata) {
        // We allow only 1 OTP field to be autofilled. Some websites (like PayPal) contain
        // multiple fields and use Javascript to autofocus to the next field every time
        // a character is typed (or pasted). We therefore only consider the first field,
        // otherwise the same code would end up in duplicated into all fields.
        if (autofillFieldMetadata.autofillHints.contains(HintConstants.AUTOFILL_HINT_SMS_OTP)) {
            if (otpFieldCount > 0) {
                Log.v("Autofill", "Ignoring consecutive OTP field")
                return
            }
            otpFieldCount++
        }

        // Update the general save type
        saveType = saveType or autofillFieldMetadata.saveType

        // Add to the list of IDs meant for autofilling
        autofillIds.add(autofillFieldMetadata.autofillId)

        // and add most of them also to the list of IDs meant for autosaving.
        if (!autofillFieldMetadata.ignoreForAutosave) {
            // Add only password fields as required, and only the first one.
            // We want to have a little fields required as possible, but we need at least one.
            if (requiredAutosaveIds.isEmpty()
                && (autofillFieldMetadata.saveType and SaveInfo.SAVE_DATA_TYPE_PASSWORD != 0)
            ) {
                requiredAutosaveIds.add(autofillFieldMetadata.autofillId)
                Log.v(
                    "Autofill",
                    "\tAdded id ${autofillFieldMetadata.autofillId} for REQUIRED autosave"
                )
            } else {
                optionalAutosaveIds.add(autofillFieldMetadata.autofillId)
                Log.v(
                    "Autofill",
                    "\tAdded id ${autofillFieldMetadata.autofillId} for OPTIONAL autosave"
                )
            }
        }

        // Update the map of autofill hints
        val hintsList = autofillFieldMetadata.autofillHints
        allAutofillHints.addAll(hintsList)
        if (autofillFieldMetadata.isFocused) {
            focusedAutofillHints.addAll(hintsList)
        }
        autofillFieldMetadata.autofillHints.forEach {
            val fields = autofillHintsToFieldsMap[it] ?: ArrayList()
            autofillHintsToFieldsMap[it] = fields
            fields.add(autofillFieldMetadata)
        }
    }

    fun getFieldsForHint(hint: String): MutableList<AutofillFieldMetadata>? {
        return autofillHintsToFieldsMap[hint]
    }

    fun isEmpty(): Boolean = autofillIds.isEmpty() || allAutofillHints.isEmpty()

    fun getRequiredAutosaveIds(): Array<AutofillId> {
        if (requiredAutosaveIds.isEmpty()) {
            val first = optionalAutosaveIds.removeFirst()
            return arrayOf(first)
        }
        return requiredAutosaveIds.toTypedArray()
    }
}
