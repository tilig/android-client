//package com.tilig.android.addaccount
//
//import androidx.compose.ui.test.*
//import androidx.compose.ui.test.junit4.createComposeRule
//import com.tilig.android.Mocks
//import com.tilig.android.analytics.Mixpanel
//import com.tilig.android.data.repository.Repository
//import com.tilig.android.ui.accounts.AccountsViewModel
//import com.tilig.android.ui.addaccount.AddAccountBottomSheet
//import com.tilig.android.ui.addaccount.AddAccountViewModel
//import com.tilig.android.ui.detail.CryptoViewModel
//import com.tilig.android.utils.UITestTags
//import io.mockk.MockKAnnotations
//import io.mockk.coEvery
//import io.mockk.mockk
//import org.junit.Before
//import org.junit.FixMethodOrder
//import org.junit.Rule
//import org.junit.Test
//import org.junit.runners.MethodSorters
//import org.koin.core.context.loadKoinModules
//import org.koin.dsl.module
//
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//class AddAccountBottomSheetTest {
//
////    @get:Rule
////    val composeTestRule = createComposeRule()
////
////    val tracker: Mixpanel = mockk()
////    val repo = mockk<Repository>()
////
////    lateinit var addAccountsViewModel: AddAccountViewModel
////    lateinit var cryptoViewModel: CryptoViewModel
////    lateinit var accountsViewModel: AccountsViewModel
////
////    @Before
////    fun setup() {
////        MockKAnnotations.init(this, relaxUnitFun = true)
////
////        coEvery { repo.getBrand(any()) } returns Mocks.successResponseBrand
////        coEvery { tracker.trackEvent(any()) } returns Unit
////
////        loadKoinModules(module {
////            single { tracker }
////            single { repo }
////        })
////        addAccountsViewModel = AddAccountViewModel()
////        cryptoViewModel = CryptoViewModel()
////        accountsViewModel = AccountsViewModel()
////
////        with(composeTestRule) {
////            setContent {
////                AddAccountBottomSheet(
////                    tracker = tracker,
////                    accountsViewModel = accountsViewModel,
////                    viewModel = addAccountsViewModel,
////                    cryptoModel = cryptoViewModel,
////                    suggestedAccount = null,
////                    onCloseSheet = {},
////                    onAccountCreated = {},
////                )
////            }
////        }
////    }
////
////    @Test
////    fun addAccountSelectBrandStateTest_A() {
////        with(composeTestRule) {
////            onNodeWithTag(UITestTags.suggestedBrandsScreenTestTag).assertIsDisplayed()
////            onNodeWithTag(UITestTags.suggestedBrandsListTestTag).onChildren().assertCountEquals(14)
////
////            // test create account is visible after suggested brand was selected
////            onNodeWithTag(UITestTags.suggestedBrandsListTestTag).onChildAt(0).performClick()
////            onNodeWithTag(UITestTags.suggestedBrandsScreenTestTag).assertDoesNotExist()
////            onNodeWithTag(UITestTags.createAccountScreenTestTag, true).assertIsDisplayed()
////            onNodeWithText("brand").assertIsDisplayed()
////            onNodeWithText("domain").assertIsDisplayed()
////        }
////    }
////
////    @Test
////    fun addAccountCreateAccountInteractionTest_B() {
////        with(composeTestRule) {
////            onNodeWithTag(UITestTags.suggestedBrandsListTestTag).onChildAt(0).performClick()
////            onNodeWithText("domain").performTextClearance()
////            onNodeWithText("Note: Invalid URL").assertIsDisplayed()
////
////            onNodeWithText("Enter a website address").performTextInput("text")
////            onNodeWithText("Note: Invalid URL").assertDoesNotExist()
////        }
////    }
//}