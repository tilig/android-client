//package com.tilig.android.addaccount
//
//import androidx.compose.ui.test.*
//import androidx.compose.ui.test.junit4.createComposeRule
//import com.tilig.android.TestHelpers
//import com.tilig.android.TestHelpers.assertTextColor
//import com.tilig.android.analytics.Mixpanel
//import com.tilig.android.ui.addaccount.components.CreateAccountPasswordView
//import com.tilig.android.ui.detail.components.states.TextFieldState
//import com.tilig.android.ui.theme.LightBlue
//import com.tilig.android.utils.UITestTags
//import io.mockk.MockKAnnotations
//import io.mockk.coEvery
//import io.mockk.mockk
//import org.junit.Before
//import org.junit.Rule
//import org.junit.Test
//import org.koin.core.context.loadKoinModules
//import org.koin.dsl.module
//import org.koin.test.AutoCloseKoinTest
//
//class CreateAccountPasswordViewTest : AutoCloseKoinTest() {
//
//    @get:Rule
//    val composeTestRule = createComposeRule()
//
//    val tracker: Mixpanel = mockk()
//
//    @Before
//    fun setup() {
//        MockKAnnotations.init(this, relaxUnitFun = true)
//        coEvery { tracker.trackEvent(any()) } returns Unit
//        loadKoinModules(module {
//            single { tracker }
//        })
//    }
//
//    @Test
//    fun createAccountPasswordViewStatesTest() {
//        with(composeTestRule) {
//            setContent {
//                CreateAccountPasswordView(
//                    tracker = tracker,
//                    passwordState = TextFieldState(""),
//                    onValueChanged = {},
//                    onDone = {}
//                )
//            }
//
//            onNodeWithText("Generate").assertIsDisplayed()
//
//            onNodeWithText("Generate").performClick()
//            onNodeWithText("Generate").assertDoesNotExist()
//            onNodeWithTag(UITestTags.slotTextTestTag, true).assertIsDisplayed()
//            onNodeWithTag(UITestTags.passwordFieldTestTag, true).assertDoesNotExist()
//
//
//            TestHelpers.AsyncTimer.start(5000)
//            composeTestRule.waitUntil(
//                condition = { TestHelpers.AsyncTimer.expired },
//                timeoutMillis = 100+5000
//            )
//            onNodeWithTag(UITestTags.slotTextTestTag, true).assertDoesNotExist()
//            onNodeWithTag(UITestTags.passwordFieldTestTag, true).assertIsDisplayed()
//            onNodeWithTag(UITestTags.passwordInputFieldTestTag, true).performTextClearance()
//            onNodeWithText("Generate").assertIsDisplayed().assertTextColor(LightBlue)
//
//        }
//    }
//}