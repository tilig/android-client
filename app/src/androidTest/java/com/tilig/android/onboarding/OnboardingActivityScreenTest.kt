package com.tilig.android.onboarding

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.rememberBottomSheetScaffoldState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.semantics.ProgressBarRangeInfo
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createComposeRule
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.rememberPagerState
import com.tilig.android.ui.onboarding.OnboardingActivityScreen
import com.tilig.android.ui.onboarding.OnboardingActivityView
import com.tilig.android.ui.theme.TiligTheme
import com.tilig.android.utils.UITestTags
import org.junit.Rule
import org.junit.Test

@OptIn(
    ExperimentalAnimationApi::class,
    ExperimentalMaterialApi::class,
    ExperimentalPagerApi::class
)
class OnboardingActivityScreenTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun onBoardingPagerSwipeTest() {
        with(composeTestRule) {
            setContent {
                TiligTheme {
                    OnboardingActivityView(
                        pagerState = rememberPagerState()
                    ) {}
                }
            }

            onNodeWithTag(UITestTags.onboardingProgressTestTag).assertIsDisplayed()
            onNodeWithTag(UITestTags.onboardingSignInTestTag).assertDoesNotExist()

            onRoot().performTouchInput {
                swipeLeft()
                swipeLeft()
                swipeLeft()
            }
            onNodeWithTag(UITestTags.onboardingNextButtonTag).performClick()
            // According to the test, this doesn't display, but according to logic, it should:
//            mainClock.advanceTimeBy(700)
//            onNodeWithTag(UITestTags.onboardingSignInTestTag).assertIsDisplayed()
        }
    }

    @Test
    fun onBoardingProgressIndicatorTest() {
        val expectedProgressBarRange = 0f..1f

        with(composeTestRule) {
            setContent {
                TiligTheme {
                    OnboardingActivityView(
                        pagerState = rememberPagerState()
                    ) {}
                }
            }
            onNodeWithTag(UITestTags.circularProgressIndicatorTestTag, true)
                .assertIsDisplayed()
                .assertRangeInfoEquals(ProgressBarRangeInfo(0.0f, expectedProgressBarRange))


            onRoot().performTouchInput {
                swipeLeft()
            }
            mainClock.advanceTimeBy(700)
            onNodeWithTag(UITestTags.circularProgressIndicatorTestTag, true)
                .assertIsDisplayed()
                .assertRangeInfoEquals(ProgressBarRangeInfo(0.5f, expectedProgressBarRange))
        }
    }

    @Test
    fun onBoardingMoreInfoTest() {
        with(composeTestRule) {
            setContent {
                TiligTheme {
                    OnboardingActivityScreen(showAccountDeletedMessage = false)
                }
            }
            onRoot(true).performTouchInput {
                swipeLeft()
                swipeLeft()
                swipeLeft()
            }
            onNodeWithTag(UITestTags.onboardingNextButtonTag).performClick()
            onNodeWithTag(UITestTags.onboardingMoreInfoModalTestTag, true).assertIsNotDisplayed()
            onNodeWithTag(UITestTags.onboardingMoreInfoBtnTestTag, true).assertIsDisplayed()
                .performClick()
            onNodeWithTag(UITestTags.onboardingMoreInfoModalTestTag, true).assertIsDisplayed()
            onNodeWithTag(UITestTags.onboardingMoreInfoModalTestTag, true).performTouchInput {
                swipeDown()
            }
            onNodeWithTag(UITestTags.onboardingMoreInfoModalTestTag, true).assertIsNotDisplayed()
        }
    }
}
