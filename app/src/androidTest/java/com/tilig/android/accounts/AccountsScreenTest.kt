package com.tilig.android.accounts

import android.annotation.SuppressLint
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.test.*
import org.junit.*
import org.junit.runners.MethodSorters
import org.koin.test.AutoCloseKoinTest

@OptIn(ExperimentalComposeUiApi::class)
@SuppressLint("UnrememberedMutableState")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class AccountsScreenTest : AutoCloseKoinTest() {

//    @get:Rule
//    val composeTestRule = createComposeRule()
//
//    val repo = mockk<Repository>()
//
//    lateinit var viewmodel: AccountsViewModel
//    lateinit var lockViewModel: LockViewModel
//
//    @Before
//    fun setup() {
//        MockKAnnotations.init(this, relaxUnitFun = true)
//        loadKoinModules(module {
//            single { repo }
//        })
//
//        lockViewModel = LockViewModel()
//
//        coEvery { repo.getAllAccounts() } returns ApiResponse.create(Response.success(emptyList())) andThen ApiResponse.create(
//            Response.success(Mocks.mockAccounts)
//        ) andThen ApiResponse.create(Response.success(Mocks.mockAccounts)) andThen ApiResponse.create(
//            Response.success(emptyList())
//        )
//        viewmodel = AccountsViewModel()
//
//        composeTestRule.setContent {
//            TiligTheme {
//                AccountsScreen(
//                    scaffoldState = rememberScaffoldState(),
//                    navController = rememberNavController(),
//                    showAutofillBar = mutableStateOf(false),
//                    viewModel = viewmodel,
//                    lockViewModel = lockViewModel,
//                    returnToAutofill = {},
//                    onAddAccountClick = {},
//                    onAutofillEnableClicked = {}
//                )
//            }
//        }
//
//        lockViewModel.onAppUnlocked()
//
//    }
//
//    @Test
//    fun A_OnAccountsEmptyListTest() {
//        with(composeTestRule) {
//            onNodeWithText("Add first account").assertIsDisplayed()
//        }
//    }
//
//    @Test
//    fun B_OnAccountsListTest() {
//        viewmodel.accounts()
//        with(composeTestRule) {
//            onNodeWithText("Add first account").assertDoesNotExist()
//            onNodeWithTag(UITestTags.accountsListTestTag).onChildren().assertCountEquals(4)
//        }
//    }
//
//    @Test
//    fun C_OnLockScreenTest() {
//        lockViewModel.onAppStartLocked()
//        with(composeTestRule) {
//            onNodeWithTag(UITestTags.lockedStateTestTag).assertIsDisplayed()
//        }
//    }
//
//    @After
//    fun afterTest() {
//        unloadKoinModules(module {
//            single { repo }
//        })
//    }
}
