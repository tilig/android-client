package com.tilig.android.accounts

import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import com.tilig.android.MainRoute
import com.tilig.android.Mocks
//import com.tilig.android.ui.accounts.AccountsUiState
//import com.tilig.android.ui.accounts.UiStateSearchMode
//import com.tilig.android.ui.accounts.components.FabButton
import com.tilig.android.utils.UITestTags
import org.junit.Rule
import org.junit.Test

class FabButtonTest {

//    @get:Rule
//    val composeTestRule = createComposeRule()
//
//    @Test
//    fun fabButtonDisplayedTest() {
//        with(composeTestRule) {
//            setContent {
//                FabButton(
//                    currentRoute = MainRoute.Accounts.route,
//                    uiState = AccountsUiState.AccountsSuccess(
//                        isLoading = false,
//                        searchMode = UiStateSearchMode.DEFAULT,
//                        accounts = Mocks.mockAccounts
//                    ),
//                    reserveSpace = mutableStateOf(false),
//                    closeSheet = {}
//                )
//            }
//            onNodeWithTag(UITestTags.fabButtonTestTag, true).assertIsDisplayed()
//        }
//    }
//
//    @Test
//    fun fabButtonNotDisplayedTest() {
//        with(composeTestRule) {
//            setContent {
//                FabButton(
//                    currentRoute = MainRoute.Accounts.route,
//                    uiState = AccountsUiState.AccountsSuccess(
//                        isLoading = false,
//                        searchMode = UiStateSearchMode.DEFAULT,
//                        accounts = emptyList()
//                    ),
//                    reserveSpace = mutableStateOf(false),
//                    closeSheet = {}
//                )
//            }
//            onNodeWithTag(UITestTags.fabButtonTestTag).assertDoesNotExist()
//        }
//    }
//
//    @Test
//    fun fabButtonNotDisplayedNotMainRouteTest() {
//        with(composeTestRule) {
//            setContent {
//                FabButton(
//                    currentRoute = MainRoute.Details.route,
//                    uiState = AccountsUiState.AccountsSuccess(
//                        isLoading = false,
//                        searchMode = UiStateSearchMode.DEFAULT,
//                        accounts = Mocks.mockAccounts
//                    ),
//                    reserveSpace = mutableStateOf(false),
//                    closeSheet = {}
//                )
//            }
//            onNodeWithTag(UITestTags.fabButtonTestTag).assertDoesNotExist()
//        }
//    }
}