package com.tilig.android

import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.EnumJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.models.tilig.ItemFieldType
import com.tilig.crypto.Crypto
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import kotlin.test.assertNotEquals

@RunWith(JUnit4::class)
class MigrationAccountLegacyTest {

    private val crypto: Crypto = Crypto().apply {
        generateKey()
        generateLegacyKey()
    }

    /**
     * Moshi, our trusted JSON parser.
     */
    private val moshi = Moshi.Builder().addLast(KotlinJsonAdapterFactory())
        // Note: add all response enums here
        .add(
            ItemFieldType::class.java,
            EnumJsonAdapter.create(ItemFieldType::class.java)
                .withUnknownFallback(ItemFieldType.UNKNOWN)
                .nullSafe()
        )
        .build()

    @Test
    fun testDoesDecryptFullAccountWithLegacy() {
        val account = Account.createEmpty().apply {
            // account.legacyEncryptionDisabled = false
            username = "Bono2000"
            password = "U2"
            notes = "I love Ireland"
            otp = "123"
        }

        val encryptedItem = account.encrypt(moshi, crypto)
        val decryptedAccount = encryptedItem.decrypt(moshi, crypto, true) as Account

        assertEquals("Bono2000", decryptedAccount.username)
        assertEquals("U2", decryptedAccount.password)
        assertEquals("I love Ireland", decryptedAccount.notes)
        assertEquals("123", decryptedAccount.otp)
    }

    // This test is removed because legacy encryption is being phased out.
    // Encrypting and decrypting the item, with the boolean set to 'true', will
    // actually WORK now because the legacy encryption is being ignored.
    /*
    @Test
    fun testDoesNotDecryptLegacyFieldsWhenDisabled() {
        val account = Account.createEmpty().apply {
            legacyEncryptionDisabled = true
            username = "Bono2000"
            password = "U2"
            notes = "I love Ireland"
            otp = "123"
        }

        val encryptedItem = account.encrypt(moshi, crypto)
        val decryptedAccount = encryptedItem.decrypt(moshi, crypto, true) as Account

        assertNotEquals("Bono2000", decryptedAccount.username)
        assertNotEquals("U2", decryptedAccount.password)
        assertNotEquals("I love Ireland", decryptedAccount.notes)
        assertNotEquals("123", decryptedAccount.otp)
    }
    */

}