package com.tilig.android.crypto

import android.security.keystore.KeyProperties
import androidx.compose.ui.test.junit4.createComposeRule
import com.tilig.crypto.Crypto
import org.junit.Assert.assertArrayEquals
import org.junit.Rule
import org.junit.Test
import kotlin.test.*

class CryptoTest {

    companion object {

        /* *** Android *** */

        // Old keypair generated on Android
        const val TEST_LEGACY_PRIVATE_KEY_ANDROID = "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDitqRwXko9Y+to\ncr+oWLOVsOdFP78GCoBIkekhmqQSeSRSPw4gKjw0yX4R3LqmD6N1O4UW8IqlukEG\nqRUUujS65IV2Wnb4JaQzVOoly+xOAhT3vmjaHaLmi+uBY05+xLzh+QyPksiFRACK\n3UWi+FQde0IOu+i1CDeX8G0c1VwvK54kLO8/N+wcyg4HyJfzV/fdNAddHiYA6o+X\nqKX4emdNoGP7EfMtrgywEJmjV9c4C8beIPB9BBYd/8yGTT5BVeGvZgDd3wfG80wl\n5wSFv81qJsYbj9QLnTKIFxAxVYIHWcKydMt9tkFl6z7l3cNlZh4Kz+e9ke6mWfNv\ngSlc3IC3AgMBAAECggEABUxGWHfjnT6mBClWUD1OaQuaw/QC50W8jLWYMJhYCDR7\nhE6D6n2CLaudvhYd6H6KD9XFvOz/GHNbTKgL3ZGiZNGtSsA/cnoStb5gwGRdD8+d\npNJ2yc4Sffe8F6h176FyVoeG4x4HgJG/v37/Pxk1ggDc4NJEVGKuRecw7ZEQ1+tO\nQCS4m5CgEwkLBzK8KMMqbNjMDq+dsyESCWgY1pO/pUHZ1TVwds3RvV3UiHkCG5fY\nO2hwEd2fnq8vkzmIe5JqV4yukyDSn4Wujrz/ONkrWuLx08kWYOJj4jN8bSwJ6/qy\nZtvzUPRbYZtp8qlRAcmrPTznLT7fDjc/l3VpYxst6QKBgQD7c9esTXjspKfgr9k+\nn6OYxXUhHmwtdhL3Vd7NTU6CDxhQx6dTZIN+lJ00IJrx4fsuYkEBRaRlvILnp9/a\niB86Gjw+jLzBLkXozM6Y9CPRs8mm0thpP2CaRmOv0gucnHUVAW6QorMHFXPuqvLB\nLAqKP4h+n58GO6R4fCKSo3AWKQKBgQDm0EPAblJ9enQ9hHAqsNcGagh21ocdRpji\nJNkZqdHYNIC0fy0qUVnvx3FoTT2tJ5px/uWfGQCpugeXJ/VAxS6lP5M2xejmLE8Z\nOj701H1xdvYePh3F5xqeAh6WAMR9XBSBTVcasZzJ0vdDLetQmlzTlG2JWnORZbob\n+KJlfmf73wKBgAs4/148UvJU4v0/O/X0kAuofHLO9csNowkQ9xG4qpFZdR9d2dT5\n/nhuz5lc/y1ehaRrVDVnU1ALGtiWAIKzYploRFoSU/ZXR119jpb7BoI8gPqGHIyN\n/JTGIXreaBBEV2Fpn/KqoEznozX6wTzn50yGS2RDYYiYrt2FxNFApCcJAoGACdKR\nH45SWl6stbV+JIL0E6TNiWklxNwtQ2p4BryQXpgVZhkHbgaaQjcFn3+yAT1vOCy1\nzuh8e7yb+BXOPRxnGMc0157ig3EdSKCO71JfqEV8wdq436QvIMxMSuvrcL5LbRlC\nfLNUTc55kVVR71MyYhqt8qtzwZtHqUsSdilD+ZcCgYEA6FJDif1GTb3ZkvvVyoWj\naRs5GbZwJ0xAWCSMK06t4RClJI9QP1R96w5fTqV6wRDJEzeFO+aQ5i38H23ttAvV\nil0VgopYGI2HqXlkCVj5Cg/9ZzU15NnOn9FxovskvZxqvpfz7otpf+G4o7neP+zj\n0+EWG2K/8C/7HaQuK3J6gbw=\n-----END PRIVATE KEY-----\n"
        const val TEST_LEGACY_PUBLIC_KEY_ANDROID = "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4rakcF5KPWPraHK/qFiz\nlbDnRT+/BgqASJHpIZqkEnkkUj8OICo8NMl+Edy6pg+jdTuFFvCKpbpBBqkVFLo0\nuuSFdlp2+CWkM1TqJcvsTgIU975o2h2i5ovrgWNOfsS84fkMj5LIhUQAit1FovhU\nHXtCDrvotQg3l/BtHNVcLyueJCzvPzfsHMoOB8iX81f33TQHXR4mAOqPl6il+Hpn\nTaBj+xHzLa4MsBCZo1fXOAvG3iDwfQQWHf/Mhk0+QVXhr2YA3d8HxvNMJecEhb/N\naibGG4/UC50yiBcQMVWCB1nCsnTLfbZBZes+5d3DZWYeCs/nvZHuplnzb4EpXNyA\ntwIDAQAB\n-----END PUBLIC KEY-----\n"
        // New libsodium keypair generated on Android
        const val TEST_PRIVATE_LIBSODIUM_KEY_ANDROID = "HVJt_MxHlE1T7JK9nQO9Z5uwumsOEaMYYYKSChmag7s"
        const val TEST_PUBLIC_LIBSODIUM_KEY_ANDROID = "3imoVUEjSEB_jr48iCivmYem2fIuxxdGWmMw9QmUz3k"
        // Libsodium keys that work, but are twice base64'd or something?
        // Keeping them here until I figured out WHY they work.
//        const val TEST_PRIVATE_LIBSODIUM_KEY_ANDROID = "SFZKdF9NeEhsRTFUN0pLOW5RTzlaNXV3dW1zT0VhTVlZWUtTQ2htYWc3cw"
//        const val TEST_PUBLIC_LIBSODIUM_KEY_ANDROID = "M2ltb1ZVRWpTRUJfanI0OGlDaXZtWWVtMmZJdXh4ZEdXbU13OVFtVXozaw"
//        val TEST_DEK = byteArrayOf(111, -97, 73, 127, 110, -75, -25, 9, -6, -17, 17, 77, 89, -16, 69, 36, -73, -46, 73, 54, 126, -2, 125, 15, -32, 73, -50, 42, -12, 101, -70, 98)
//        val TEST_DEK_STR = TEST_DEK.toString(Charsets.UTF_8)
//        const val TEST_ENCRYPTED_KEY_ANDROID = "pV-v0IK_Y5zT2JZNkeJU9iB2XxmubhVdCTMhZNC3fWMg6gycPq4gm1jyh2gqlN4b5-Svi5X6t1U"
//        const val TEST_ENCRYPTED_KEY_ANDROID = "2ecS9wkOjjNmbSKjqEjcwZUbuE3ZP-RFFvg5_jtMNBCodAqwL_VBjxXelvyrWY-TbCTKavvLgYPp-NYkTxTiHruzvmF7EfzQ"
//        const val TEST_ENCRYPTED_DEK_ANDROID = "HPvMGC_NXerihaAepXDuqyzFuqKDF_VND6rlEwSmWmH1pzoGFBWLlLMA9xGjDekUcjqqboylA91s_WsjR8xLUpXbJ9e_uuHh7RE59G9eNWYJaxQr4Wmp6gFXyh6GxAiovnDJjFpWrZ8PhPlRIyvn797rm8-vhUn0X0J89GgOWnGI7N3AaZHo-XW0gDnevmKWdhUJwYh5tB8_uQzwpHjx4Dgf97AxqrWeyJ3WZI_CNkLHdBfJAcPd12Gbr6nUXJQknZ_fY8VLBeoD5Aw-Ak-xTo0llG2hMN-0bJfD_gtn5HpuxKwFDiFxc_Q1w8Tc29pdzdeD3XEo4b0LND03xT33Kg"
//        const val TEST_PAYLOAD_ENCRYPTED = "eX0sefzE1yKmDLIwgxtPFb_OLd0501FbBnbjXf87H8_igqzcwenXXfm706ZU_JPjfQ6QjumkWikGq_1u"
//        const val TEST_ENCRYPTED_DEK_ANDROID = "HPvMGC_NXerihaAepXDuqyzFuqKDF_VND6rlEwSmWmH1pzoGFBWLlLMA9xGjDekUcjqqboylA91s_WsjR8xLUpXbJ9e_uuHh7RE59G9eNWYJaxQr4Wmp6gFXyh6GxAiovnDJjFpWrZ8PhPlRIyvn797rm8-vhUn0X0J89GgOWnGI7N3AaZHo-XW0gDnevmKWdhUJwYh5tB8_uQzwpHjx4Dgf97AxqrWeyJ3WZI_CNkLHdBfJAcPd12Gbr6nUXJQknZ_fY8VLBeoD5Aw-Ak-xTo0llG2hMN-0bJfD_gtn5HpuxKwFDiFxc_Q1w8Tc29pdzdeD3XEo4b0LND03xT33Kg"

        // DEK generated on Android
        val TEST_DEK = byteArrayOf(91, -27, 38, 13, 22, 33, -42, -125, -86, -68, 98, -68, 90, 38, -33, 32, -95, -14, 64, 14, 23, 83, 45, 113, 96, 124, 10, -73, -110, -87, -80, -121)
        val TEST_DEK_STR = TEST_DEK.toString(Charsets.UTF_8)
        const val TEST_KEY_ANDROID_ENCODED = "W-UmDRYh1oOqvGK8WibfIKHyQA4XUy1xYHwKt5KpsIc"
        // Payload
        const val TEST_PAYLOAD_ANDROID = "payload from android"
        // Payload encrypted using the DEK
        const val TEST_PAYLOAD_ENCRYPTED = "bGW5_96EP4rCHYJw94Vgzb2-RGFqrkg4vUa82BVynb5KaL-Yja_iAxupFm3cm-i00HhvRNBXiJk1P0v6"
        // The above DEK, encrypted using the old keypair on Android
        const val TEST_ENCRYPTED_DEK_ANDROID = "jWg+5jBTeYBotVJ9edMS+3aYHTlLSCRg1PPFXVdg7e9fbywemJb+/7RkTQqk9xICpE6mjoJ0xPUC4Fw+wqraL33ma/gVlH+qaGRJ6X6nvv7iyivGj7FSqd7aDKGxivIhoUvCWg+bRwac6P9iKJuECkZGMHQE5rSHMeWdPNBMucQ/Irt4yWX9iTovIGJoiNAH7I5nV8ZsWE2ZavC1w9az+fSAuvAkYXS5vFy3EdNoUy548Fw2awE6bYyEQfH4ecXMtSDahS3HRQfQPO+lAaWXs2Emy31/FrB4QXxoVK0iS4Ss1LSjYdwqTf7KCLoc/SFJyQOu9vEUREN2suKccWXHaw=="
        // The above DEK, encrypted using the new libsodium keypair on Android
        const val TEST_ENCRYPTED_KEY_ANDROID = "yuy1J6bgEYEaG0cWAqzsoFmUB5uSdB7H1UWUyj5Ut6IdojKna4Jz370zv5-REvhCI7jDmTQ3_mbV2j4hZVJlJx5DLSQVXX6z"

        /* *** iOS *** */
        // DEK generated on iOS
        val TEST_DEK_IOS = byteArrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
        const val TEST_PAYLOAD_IOS = "my payload"
        // Payload encrypted using the DEK
        const val TEST_PAYLOAD_ENCRYPTED_IOS = "WwpjH6gGd_77-TfwOyrycnx05EPNMkdVDSNCv1NfG0Fy0ddYb20d_XiUjIuRW-Wj22w"
        // Old private key generated on iOS
        const val TEST_LEGACY_PRIVATE_KEY_IOS = "-----BEGIN PRIVATE KEY-----MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDDTKo7JmPCeZRvQ+iumxFz/4S244syFXxEGhs8w2kmhI8r7ijE/thI8p8ZFpAVfFTHXTWbDT2/IW3nGTuAe2kipwBoiMEQVg2kAcDJdNPN4g4ECVRSYpqL+YyZtvqNnkGM7fixZ7epfVBHwvbrMrF35ErmqpuizN6Wp5d4HJfriBZ3oko21cWMzYbsFd73Bvm/c/nw7RmTmpx9aq4TfeO/KZFb5czyq7Hf930H/2h+VSXrNLPFCfjbxAxChfcKvR1FKcP890nf2T3dvf2kyHzBHYHpAi0Kyq8VKYH6BJB8oXIiv3rHbF7TfMjV/2PKfhZ4koq3W1KSyJ7dhCcBB5+JAgMBAAECggEAFrx3AHgbis1P6ySlIuetYFtdylNi9feTnhOKenhW5luuFcuN/kIbcKGjRT7P0xqEfamzpwuDddv40RvKRi7H2neF+AV8sW/NOnvUQ3fI/SndHxntccJmMtfAZzhKwsAq6jLdnhygoHgU56fSSmceS0J9+EziHdG7M5+K5hoxEwOynzCp6eJwpW1H0j5QbVwwqvx8274KCLASnI145U7hN1fakBwavmEXxr4ZnodLTd/i7NfXbEkMX+QNjYwMzkLax+qqmuS55NSa7xr9B76T8N7AmCSR0qZYpZbO7snie+LuUFpe0R/yYVGp0OZJ+8z+XNzmLxVAHIYXfEBcApSooQKBgQDnWurbg+3+iWcLFogzA4e/NnvaCC9Gjt+AB/dp8m2kSnqvrUcIEK9JDg9E6LvdtO8+RlpiZVfxrtkjKmYEGTvsIT6xAaEI6SN4QGrA/g25DWxxQXEFiSwQIGZaLv2m9Yxl6coaYyhqDFR+83FTD5iBg7wIl3c0QrvsAbmbJYq86QKBgQDYGoFIOEfrY8BdcT/ScIE0AujjYjZm3S9DV86kZ802JlrCBpMqHSztOi2yOzVWfxZukkJrxq4ieSQNy0vuDQi3jOZ475/CeUu4v4XbKICNK8THZOluKZc6rf/qbuZOUTX7alBHlVDer8tai9yp+VHLZtGITZ5DFiAZwnN0LumpoQKBgDT2eEt2gVZC6Eb+ioF7LmlUtLeuJfx2Gq45pKcpa3Yv4rQy687p2ubThCVXTiKsGBTekDR0VNSsHWNSgnYvvKsK1Uo6QD5E+Oy/enDh+AxECW5H9/K3e1HZyPSeS00ca16tTaoKxya32eQSIQBN0rDbkoE5A34ARPr7Th05U/B5AoGBAMGcnY9yvRyrtkEBociMe3V51eVCrLLkH+AZ3010omqNWmrTMS5eDU5M/XvdiCACpLpT16ZvzrucBS1GorSYgOLSHfrj1SKLQcg371k2pFXWjOqIAotsP82W7t2+liA8qjyiN2Kq6FVa8rog7IZslVfCup9/yvcX75KkoIPLNBWBAoGBAI4cEoyb6D+UL34EEwJ4R4QGpqduYT48Hal96fBeBGA4qzvmv1LKS2gQ/0dmnoWA4Xo/GuFhtH33UOyGAbKIZCSX1Ql4sGGFYaI2RAmmcyNVoUWeEd0N5wS+I2hgqJnpEtb7dFCxAqwxwH/DerhJln4X+c9Ib8kKZ8l+GKccWDfp-----END PRIVATE KEY-----"
        val TEST_LEGACY_PRIVATE_KEY_IOS_DECODED = byteArrayOf(48,-126,4,-66,2,1,0,48,13,6,9,42,-122,72,-122,-9,13,1,1,1,5,0,4,-126,4,-88,48,-126,4,-92,2,1,0,2,-126,1,1,0,-61,76,-86,59,38,99,-62,121,-108,111,67,-24,-82,-101,17,115,-1,-124,-74,-29,-117,50,21,124,68,26,27,60,-61,105,38,-124,-113,43,-18,40,-60,-2,-40,72,-14,-97,25,22,-112,21,124,84,-57,93,53,-101,13,61,-65,33,109,-25,25,59,-128,123,105,34,-89,0,104,-120,-63,16,86,13,-92,1,-64,-55,116,-45,-51,-30,14,4,9,84,82,98,-102,-117,-7,-116,-103,-74,-6,-115,-98,65,-116,-19,-8,-79,103,-73,-87,125,80,71,-62,-10,-21,50,-79,119,-28,74,-26,-86,-101,-94,-52,-34,-106,-89,-105,120,28,-105,-21,-120,22,119,-94,74,54,-43,-59,-116,-51,-122,-20,21,-34,-9,6,-7,-65,115,-7,-16,-19,25,-109,-102,-100,125,106,-82,19,125,-29,-65,41,-111,91,-27,-52,-14,-85,-79,-33,-9,125,7,-1,104,126,85,37,-21,52,-77,-59,9,-8,-37,-60,12,66,-123,-9,10,-67,29,69,41,-61,-4,-9,73,-33,-39,61,-35,-67,-3,-92,-56,124,-63,29,-127,-23,2,45,10,-54,-81,21,41,-127,-6,4,-112,124,-95,114,34,-65,122,-57,108,94,-45,124,-56,-43,-1,99,-54,126,22,120,-110,-118,-73,91,82,-110,-56,-98,-35,-124,39,1,7,-97,-119,2,3,1,0,1,2,-126,1,0,22,-68,119,0,120,27,-118,-51,79,-21,36,-91,34,-25,-83,96,91,93,-54,83,98,-11,-9,-109,-98,19,-118,122,120,86,-26,91,-82,21,-53,-115,-2,66,27,112,-95,-93,69,62,-49,-45,26,-124,125,-87,-77,-89,11,-125,117,-37,-8,-47,27,-54,70,46,-57,-38,119,-123,-8,5,124,-79,111,-51,58,123,-44,67,119,-56,-3,41,-35,31,25,-19,113,-62,102,50,-41,-64,103,56,74,-62,-64,42,-22,50,-35,-98,28,-96,-96,120,20,-25,-89,-46,74,103,30,75,66,125,-8,76,-30,29,-47,-69,51,-97,-118,-26,26,49,19,3,-78,-97,48,-87,-23,-30,112,-91,109,71,-46,62,80,109,92,48,-86,-4,124,-37,-66,10,8,-80,18,-100,-115,120,-27,78,-31,55,87,-38,-112,28,26,-66,97,23,-58,-66,25,-98,-121,75,77,-33,-30,-20,-41,-41,108,73,12,95,-28,13,-115,-116,12,-50,66,-38,-57,-22,-86,-102,-28,-71,-28,-44,-102,-17,26,-3,7,-66,-109,-16,-34,-64,-104,36,-111,-46,-90,88,-91,-106,-50,-18,-55,-30,123,-30,-18,80,90,94,-47,31,-14,97,81,-87,-48,-26,73,-5,-52,-2,92,-36,-26,47,21,64,28,-122,23,124,64,92,2,-108,-88,-95,2,-127,-127,0,-25,90,-22,-37,-125,-19,-2,-119,103,11,22,-120,51,3,-121,-65,54,123,-38,8,47,70,-114,-33,-128,7,-9,105,-14,109,-92,74,122,-81,-83,71,8,16,-81,73,14,15,68,-24,-69,-35,-76,-17,62,70,90,98,101,87,-15,-82,-39,35,42,102,4,25,59,-20,33,62,-79,1,-95,8,-23,35,120,64,106,-64,-2,13,-71,13,108,113,65,113,5,-119,44,16,32,102,90,46,-3,-90,-11,-116,101,-23,-54,26,99,40,106,12,84,126,-13,113,83,15,-104,-127,-125,-68,8,-105,119,52,66,-69,-20,1,-71,-101,37,-118,-68,-23,2,-127,-127,0,-40,26,-127,72,56,71,-21,99,-64,93,113,63,-46,112,-127,52,2,-24,-29,98,54,102,-35,47,67,87,-50,-92,103,-51,54,38,90,-62,6,-109,42,29,44,-19,58,45,-78,59,53,86,127,22,110,-110,66,107,-58,-82,34,121,36,13,-53,75,-18,13,8,-73,-116,-26,120,-17,-97,-62,121,75,-72,-65,-123,-37,40,-128,-115,43,-60,-57,100,-23,110,41,-105,58,-83,-1,-22,110,-26,78,81,53,-5,106,80,71,-107,80,-34,-81,-53,90,-117,-36,-87,-7,81,-53,102,-47,-120,77,-98,67,22,32,25,-62,115,116,46,-23,-87,-95,2,-127,-128,52,-10,120,75,118,-127,86,66,-24,70,-2,-118,-127,123,46,105,84,-76,-73,-82,37,-4,118,26,-82,57,-92,-89,41,107,118,47,-30,-76,50,-21,-50,-23,-38,-26,-45,-124,37,87,78,34,-84,24,20,-34,-112,52,116,84,-44,-84,29,99,82,-126,118,47,-68,-85,10,-43,74,58,64,62,68,-8,-20,-65,122,112,-31,-8,12,68,9,110,71,-9,-14,-73,123,81,-39,-56,-12,-98,75,77,28,107,94,-83,77,-86,10,-57,38,-73,-39,-28,18,33,0,77,-46,-80,-37,-110,-127,57,3,126,0,68,-6,-5,78,29,57,83,-16,121,2,-127,-127,0,-63,-100,-99,-113,114,-67,28,-85,-74,65,1,-95,-56,-116,123,117,121,-43,-27,66,-84,-78,-28,31,-32,25,-33,77,116,-94,106,-115,90,106,-45,49,46,94,13,78,76,-3,123,-35,-120,32,2,-92,-70,83,-41,-90,111,-50,-69,-100,5,45,70,-94,-76,-104,-128,-30,-46,29,-6,-29,-43,34,-117,65,-56,55,-17,89,54,-92,85,-42,-116,-22,-120,2,-117,108,63,-51,-106,-18,-35,-66,-106,32,60,-86,60,-94,55,98,-86,-24,85,90,-14,-70,32,-20,-122,108,-107,87,-62,-70,-97,127,-54,-9,23,-17,-110,-92,-96,-125,-53,52,21,-127,2,-127,-127,0,-114,28,18,-116,-101,-24,63,-108,47,126,4,19,2,120,71,-124,6,-90,-89,110,97,62,60,29,-87,125,-23,-16,94,4,96,56,-85,59,-26,-65,82,-54,75,104,16,-1,71,102,-98,-123,-128,-31,122,63,26,-31,97,-76,125,-9,80,-20,-122,1,-78,-120,100,36,-105,-43,9,120,-80,97,-123,97,-94,54,68,9,-90,115,35,85,-95,69,-98,17,-35,13,-25,4,-66,35,104,96,-88,-103,-23,18,-42,-5,116,80,-79,2,-84,49,-64,127,-61,122,-72,73,-106,126,23,-7,-49,72,111,-55,10,103,-55,126,24,-89,28,88,55,-23)

        /* *** Web *** */

        // Old keypair generated in webapp
        const val TEST_LEGACY_PRIVATE_KEY_WEBAPP = "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCsozIzqsaOw38mdOws8ruo3wwUZA+yp9r7svtBfNe6EKYejKA+ojQBP15bDi5u1jbYfgpH58Pd3jSny3QDkH8bxC2WJwVMAUNnYTCWP+Hxe+W8XcqZ71rAAJ+7+ayr1C7DvpwWT0vMDsX1APNUSpy0jnvwu2TwNrX6sZDHu4bWfBE0eMswWLdgrjPw4QQ+l7c6fOCsRNK7XQFdXKJgEDLeP6p2rU6seqEPBtGlCt9/0g5uvKbuAXHz7Td0+E5Ytf2Hjo7JKuuDOIkT1160AFcLgbdCE5wlvZamJq910xSSUmBCduZoD+p23VQ5NxUO5Vhr8ZF09Vbn6/gjRKC+vtbPAgMBAAECggEAENHBhVkhCS2wyJ1hmSt47wX0FlR9VwmVgyo34nR4TA9tL9LBlfiVmn2DAtoJ2hCSvVriQ4koAwiwFpzyy5obWCgvzM42bOY+m+bTJgBJUg6hHk4A/CherhAhf93eXctJVxbN/Dpzhwj1bl5OVqsCQ3ESUCzhs6mQ50C/e0Yg5+p7Ndo1/+/QD3gSyq/lzGL5WtVHUZIsyUTbcRbXLaD+wsnXPfN3UGKp7zzB7aOpcwXN9QIUcRN7mAtJCX3k5Lw33SkoCOOLKZXfmvDo/blD1G/Mq3+tTEe0Q0KTyRJXyxh6J/5NzSgMdeL3rg4c03mI7VY9DUR3OXfkXX8kflGpaQKBgQDuOnJwQqlHTkuHK8fE7E+VGhLyq/S5Ohk4cC0PwljsksUeP66pUDkEpcGzoeAgqYH75W5tSfExbeO0otnnbtnvCk6I9DMPrMVgFJ4ZMbvn+U+uIDhlVs1/LXeTTWMYf0D2taMVCp6fr5u1I16FnIKV7/PhFW40V76rauwRugE0VQKBgQC5hCG0iFXT70H84yQ2sJeHnLhEaOX04Z/49hNLtgvG7MrvVeD3PPvf7vSkWfJeqhQ2ghZQ7eEnpOoS97CHcdBTPDeD2G7lS3FO5KktL75gpjJ48T+eF5aKQwtx0BCvzFTXG6ntiFlfEYiGYvR4cw2LPXAPvIZvBmjOSclX6TCikwKBgF4jxplKHm7JJ9uQl27gG0rGdPpkrV6M8O52lbgGivFhxnd/ntvVkdKeVf9QVqtRkMYkEhEqCSAJVtRJDmcATSrDHenM5mipU432V8uI9cJjqPMcVGzncmlxZDFXgXNqxhc4C+wCuFBOKu/XvEB8arYpjmxB9bzFd9lxdTxEDrnJAoGAM/DPswkc5qqtUg2SfRFyS5DQcq7f+3EUZhMXkfIVWAvCQcJMq0xSxP7VVhKs7l42LJC9iMkhagUdWFQrA7EU/8jzTiHR4rb9sXYJ+9HLPqoppXz+L9gw2OMumB4GHgJamqNvL0cmppx1GhMomXS1DRAViQJirUEhJOi5y7hJWqsCgYB49K1uneythJsChP1AsunLA4bDhOyX4bN+eed3/j93ODuELlrNMW2Y7PzLrNIC5h+JVH7e+abigCMz4OUT+HjLdjLJz2ZBjtoX8TiWj8UWOzYa8whUq/NKdPRrTZPWINipB7V51YQjAleoIrBCJ4OLcBO8h/fi1JdF2TtnvPfOaA==\n-----END PRIVATE KEY-----"
        const val TEST_LEGACY_PUBLIC_KEY_WEBAPP = "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArKMyM6rGjsN/JnTsLPK7qN8MFGQPsqfa+7L7QXzXuhCmHoygPqI0AT9eWw4ubtY22H4KR+fD3d40p8t0A5B/G8QtlicFTAFDZ2Ewlj/h8XvlvF3Kme9awACfu/msq9Quw76cFk9LzA7F9QDzVEqctI578Ltk8Da1+rGQx7uG1nwRNHjLMFi3YK4z8OEEPpe3OnzgrETSu10BXVyiYBAy3j+qdq1OrHqhDwbRpQrff9IObrym7gFx8+03dPhOWLX9h46OySrrgziJE9detABXC4G3QhOcJb2WpiavddMUklJgQnbmaA/qdt1UOTcVDuVYa/GRdPVW5+v4I0Sgvr7WzwIDAQAB\n-----END PUBLIC KEY-----"
        // New libsodium keypair generated in webapp
        const val TEST_PRIVATE_LIBSODIUM_KEY_WEBAPP =  "OVkDL2pzAemehYACNdc7tB7wYB3qejxKiZ2WpPOnr50"
        const val TEST_PUBLIC_LIBSODIUM_KEY_WEBAPP =   "7gpk9z5P-M1LrmMR4upBOCKWsIiSbEno3RlTqOufBA4"
        // DEK, generated on web, encrypted using new keypair on web
        const val TEST_ENCRYPTED_KEY_WEB = "btoctTEaM0M6S8zEkrlZX5o8Ww_ssZTaMXDpAhVZVy9NBCN0PMktI2RodL-s1KHydqiTjfnBtUBmJ49o57pCPK6VVpVUipU_"
        const val TEST_DECRYPTED_KEY_WEB = "--I3banP6C55rh5r7-blBuDqfg-lnLMrrtYXym-cB0M"
        // Same DEK, generated on web, encrypted using legacy keypair on web
        const val TEST_ENCRYPTED_DEK_WEB = "c0Q5IVe3TlDxdP/Fw8laQZP9awN0GgSiGrV8Hu5sfbMRggz2qv+L6toInJrlSMc30xaoWHufrkBr+0zN0unTjLcBQq68qZ5sdOBUHGJYHhsICViQ++KQ/I7G/JM7tdl0bw2cG+WdgUHXcIv6BbyqytAs7RsUhusaEDxxAynW0FTNtcwc5uSeRlBh5h+3hg009tI01mU2EdbnayVqsdz4NhhswcWwZd8ChGSrxadpB2pnh5oad+fUgSm4hiOnNn10UB47S0epVN+Ezin69j/JhMc9xp2BnKMmfVJrgjJbAhhIGPi75Aojy6dyIV2/TYpwPHYbI+IkSg0sRC/QGhtWdA=="
        // Share link master key, generated on web, encrypted using new libsodium keypair on web
        const val ENCRYPTED_MASTER_KEY_WEB = "9ZYO8fakPytfRzrDUbsmISHKpsGY_-04IRfZyVRpM9GyY0_h4nenzdI6ZXh5mQWyqUJehYx3ysfICEFrLjyWF8LMGt9Evyfo"

    }

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun canGenerateKeys() {
        val crypto = Crypto()
        val keyPair = crypto.generateKey()
        assertTrue(crypto.hasKeys())
        assertTrue(crypto.hasNewKeys())
        assertNotNull(keyPair.publicKey)
        assertNotNull(keyPair.secretKey)
    }

    @Test
    fun canGenerateLegacyKey() {
        val crypto = Crypto()
        val keyPair = crypto.generateLegacyKey()
        assertTrue(crypto.hasKeys())
        assertTrue(crypto.hasLegacyKeys())
        assertNotNull(keyPair.private)
        assertNotNull(keyPair.public)
        assertEquals(keyPair.private.algorithm, KeyProperties.KEY_ALGORITHM_RSA)
    }

    @Test
    fun canParseKeysAndroid() {
        Crypto().apply {
            setKeypairFromString(
                secret = TEST_PRIVATE_LIBSODIUM_KEY_ANDROID,
                pub = TEST_PUBLIC_LIBSODIUM_KEY_ANDROID
            )
            assertTrue(hasNewKeys())
            val keyPair = getKeyPairs()
            assertNotNull(keyPair.keypair?.secretKey)
            assertNotNull(keyPair.keypair?.publicKey)
        }
    }

    @Test
    fun canParseKeysWeb() {
        Crypto().apply {
            setKeypairFromString(
                secret = TEST_PRIVATE_LIBSODIUM_KEY_WEBAPP,
                pub = TEST_PUBLIC_LIBSODIUM_KEY_WEBAPP
            )

            assertTrue(hasNewKeys())

            val keyPair = getKeyPairs()
            assertNotNull(keyPair.keypair?.secretKey)
            assertNotNull(keyPair.keypair?.publicKey)
            // Test if it works
            val encryptedDek = encryptDek(TEST_DEK)
            assertNotNull(encryptedDek)
            assertNotEquals(encryptedDek, TEST_DEK_STR)
        }
    }

    @Test
    fun canParseLegacyKeysAndroid() {
        Crypto().apply {
            setLegacyKeypairFromString(
                TEST_LEGACY_PRIVATE_KEY_ANDROID,
                TEST_LEGACY_PUBLIC_KEY_ANDROID
            )

            assertTrue(hasLegacyKeys())

            val keyPair = getKeyPairs()
            assertNotNull(keyPair.legacy?.private)
            assertNotNull(keyPair.legacy?.public)
            assertEquals(keyPair.legacy?.private?.algorithm, KeyProperties.KEY_ALGORITHM_RSA)
        }
    }

    @Test
    fun canParseLegacyKeysIOS() {
        Crypto().apply {
            setLegacyKeypairFromString(
                // Note: this is not a valid PAIR. Only the private key is from another platform
                TEST_LEGACY_PRIVATE_KEY_IOS,
                TEST_LEGACY_PUBLIC_KEY_ANDROID
            )

            assertTrue(hasLegacyKeys())

            val keyPair = getKeyPairs()
            assertNotNull(keyPair.legacy?.private)
            //println("IOS private key: " +  keyPair.legacy?.private?.encoded?.joinToString(", "))
            assertEquals(keyPair.legacy?.private?.algorithm, KeyProperties.KEY_ALGORITHM_RSA)
            assertArrayEquals(keyPair.legacy?.private?.encoded, TEST_LEGACY_PRIVATE_KEY_IOS_DECODED)
        }
    }

    @Test
    fun canParseLegacyKeysWeb() {
        // Webapp
        Crypto().apply {
            setLegacyKeypairFromString(
                TEST_LEGACY_PRIVATE_KEY_WEBAPP,
                TEST_LEGACY_PUBLIC_KEY_WEBAPP
            )

            assertTrue(hasLegacyKeys())

            val keyPair = getKeyPairs()
            assertNotNull(keyPair.legacy?.private)
            assertEquals(keyPair.legacy?.private?.algorithm, KeyProperties.KEY_ALGORITHM_RSA)
            // Test if it works
            val encryptedDek = legacyEncryptDek(TEST_DEK)
            assertNotNull(encryptedDek)
            assertNotEquals(encryptedDek, TEST_DEK_STR)
        }
    }

    @Test
    fun canFormatKeysToString() {
        Crypto().apply {
            setKeypairFromString(
                secret = TEST_PRIVATE_LIBSODIUM_KEY_ANDROID,
                pub = TEST_PUBLIC_LIBSODIUM_KEY_ANDROID
            )

            val keyPair = getKeyPairAsStrings()

            assertEquals(TEST_PRIVATE_LIBSODIUM_KEY_ANDROID, keyPair?.private)
            assertEquals(TEST_PUBLIC_LIBSODIUM_KEY_ANDROID, keyPair?.public)
        }
        Crypto().apply {
            setKeypairFromString(
                TEST_PRIVATE_LIBSODIUM_KEY_WEBAPP,
                TEST_PUBLIC_LIBSODIUM_KEY_WEBAPP
            )

            val keyPair = getKeyPairAsStrings()

            assertEquals(TEST_PRIVATE_LIBSODIUM_KEY_WEBAPP, keyPair?.private)
            assertEquals(TEST_PUBLIC_LIBSODIUM_KEY_WEBAPP, keyPair?.public)
        }
    }

    @Test
    fun canFormatLegacyKeysToString() {
        Crypto().apply {
            setLegacyKeypairFromString(
                TEST_LEGACY_PRIVATE_KEY_ANDROID,
                TEST_LEGACY_PUBLIC_KEY_ANDROID
            )

            val privKey = legacyPrivateKeyAsString()
            val pubKey = legacyPublicKeyAsString()

            assertEquals(TEST_LEGACY_PRIVATE_KEY_ANDROID, privKey)
            assertEquals(TEST_LEGACY_PUBLIC_KEY_ANDROID, pubKey)
        }
    }

    @Test
    fun canEncodeAndDecodeDEK() {
        Crypto().apply {
            val dek = generateDek()
            val encoded = base64encode(dek)
            val decoded = base64decode(encoded)
            assertArrayEquals(dek, decoded)
        }
    }

    @Test
    fun canEncryptAndDecryptDek() {
        Crypto().apply {
            setKeypairFromString(
                secret = TEST_PRIVATE_LIBSODIUM_KEY_ANDROID,
                pub = TEST_PUBLIC_LIBSODIUM_KEY_ANDROID
            )
            val dek = generateDek()
            val encryptedDek = encryptDek(dek)
            assertNotNull(encryptedDek)
            assertNotEquals(encryptedDek, base64encode(dek))
            val decrypted = decryptDek(encryptedDek)
            assertArrayEquals(dek, decrypted)
        }
    }

    @Test
    fun canDecryptKeyFromAndroid() {
        Crypto().apply {
            setKeypairFromString(
                secret = TEST_PRIVATE_LIBSODIUM_KEY_ANDROID,
                pub = TEST_PUBLIC_LIBSODIUM_KEY_ANDROID
            )
            val encryptedDek = TEST_ENCRYPTED_KEY_ANDROID
            val decrypted = decryptDek(encryptedDek)
            assertNotNull(decrypted)
            assertFalse(decrypted.all { it == 0.toByte() })
            assertArrayEquals(TEST_DEK, decrypted)
        }
    }

    @Test
    fun canDecryptKeyFromWeb() {
        Crypto().apply {
            setKeypairFromString(
                secret = TEST_PRIVATE_LIBSODIUM_KEY_WEBAPP,
                pub = TEST_PUBLIC_LIBSODIUM_KEY_WEBAPP
            )
            val encryptedDek = TEST_ENCRYPTED_KEY_WEB
            val decrypted = decryptDek(encryptedDek)
            assertNotNull(decrypted)
            assertFalse(decrypted.all { it == 0.toByte() })
            assertEquals(TEST_DECRYPTED_KEY_WEB, base64encode(decrypted))
        }
    }

    @Test
    fun canEncryptAndDecryptMasterKey() {
        Crypto().apply {
            setKeypairFromString(
                secret = TEST_PRIVATE_LIBSODIUM_KEY_ANDROID,
                pub = TEST_PUBLIC_LIBSODIUM_KEY_ANDROID
            )
            val dek = generateDek()
            val encryptedDek = encryptMasterSecret(dek)
            assertNotNull(encryptedDek)
            val bdek = base64encode(dek)
            assertNotEquals(encryptedDek, bdek)
            println(bdek)
            val decrypted = decryptMasterSecret(encryptedDek)
            assertEquals(base64encode(dek), decrypted)
        }
    }

    @Test
    fun canLegacyEncryptDek() {
        Crypto().apply {
            setLegacyKeypairFromString(
                TEST_LEGACY_PRIVATE_KEY_ANDROID,
                TEST_LEGACY_PUBLIC_KEY_ANDROID
            )
            val encryptedDek = legacyEncryptDek(TEST_DEK)
            // Can't compare against TEST_DEK_ENCRYPTED, because encryption contains randomness
            assertNotNull(encryptedDek)
            assertNotEquals(encryptedDek, TEST_DEK_STR)
        }
    }

    @Test
    fun canLegacyDecryptDek() {
        Crypto().apply {
            setLegacyKeypairFromString(
                TEST_LEGACY_PRIVATE_KEY_ANDROID,
                TEST_LEGACY_PUBLIC_KEY_ANDROID
            )
            val decryptedDek = legacyDecryptDek(TEST_ENCRYPTED_DEK_ANDROID)
            assertArrayEquals(decryptedDek, TEST_DEK)
        }
    }

    @Test
    fun canEncryptAndDecryptUsingDek() {
        val sourceText =
            "Sit sunt consequat fugiat amet deserunt ad eu magna labore esse aliqua non quis ea exercitation aliqua voluptate deserunt in in exercitation eu pariatur veniam excepteur adipisicing amet laborum laboris et occaecat labore duis anim tempor reprehenderit deserunt culpa commodo mollit ut esse sunt mollit laborum amet ullamco adipisicing officia sint ea eiusmod non voluptate reprehenderit incididunt nostrud excepteur velit laboris irure elit anim velit amet culpa id sunt enim exercitation elit ullamco id excepteur sit occaecat aliqua culpa dolore do occaecat aliqua laborum enim ex tempor tempor labore proident laboris pariatur dolore ullamco sit non deserunt dolore exercitation et ut sunt sint veniam consectetur elit veniam excepteur dolore amet deserunt ut in ut elit dolore aliqua eiusmod proident velit culpa sed ullamco magna nostrud labore est ad ut minim ut in do dolore est non est in quis excepteur fugiat qui dolor irure dolor dolor irure enim magna non labore in nisi in cillum laborum officia cillum enim culpa in dolore mollit deserunt cillum et."

        Crypto().apply {
            val dek = generateDek()
            val encrypted = encryptUsingDek(dek, sourceText)
            val decrypted = encrypted?.let { decryptUsingDek(dek, it) }
            assertNotEquals(sourceText, encrypted)
            assertEquals(sourceText, decrypted)
        }
    }

    @Test
    fun canEncryptUsingDek() {
        Crypto().apply {
            setKeypairFromString(
                secret = TEST_PRIVATE_LIBSODIUM_KEY_ANDROID,
                pub = TEST_PUBLIC_LIBSODIUM_KEY_ANDROID
            )
            val dek = decryptDek(TEST_ENCRYPTED_KEY_ANDROID)
            val encrypted = encryptUsingDek(
                dek!!,
                TEST_PAYLOAD_ANDROID
            )
            assertNotNull(encrypted)
            assert(encrypted.length > 10)
            // Note: we can't do this, because the encrypted version
            // is different every time you encrypt
            //assertEquals(encrypted, TEST_PAYLOAD_ENCRYPTED)
        }
    }

    @Test
    fun canDecryptUsingDek() {
        Crypto().apply {
            setKeypairFromString(
                secret = TEST_PRIVATE_LIBSODIUM_KEY_ANDROID,
                pub = TEST_PUBLIC_LIBSODIUM_KEY_ANDROID
            )
            val dek = decryptDek(TEST_ENCRYPTED_KEY_ANDROID)
            val decrypted = decryptUsingDek(
                dek!!,
                TEST_PAYLOAD_ENCRYPTED
            )
            assertEquals(TEST_PAYLOAD_ANDROID, decrypted)
        }
    }

    @Test
    fun canEncryptUsingDekForIOS() {
        Crypto().apply {
            val encrypted = encryptUsingDek(
                TEST_DEK_IOS,
                TEST_PAYLOAD_IOS
            )
            // Can't compare against TEST_PAYLOAD_ENCRYPTED_IOS, because encryption contains randomness
            assertNotNull(encrypted)
            assertNotEquals(TEST_PAYLOAD_IOS, encrypted)
        }
    }

    @Test
    fun canDecryptUsingDekForIOS() {
        Crypto().apply {
            val decrypted = decryptUsingDek(
                TEST_DEK_IOS,
                TEST_PAYLOAD_ENCRYPTED_IOS
            )
            assertEquals(decrypted, TEST_PAYLOAD_IOS)
        }
    }

    @Test
    fun canEncryptAndDecryptFolderKek() {
        Crypto().apply {
            val folderKek = getNewGeneratedKey()
            val usersKeypair = getNewGeneratedKey()

            // Encrypt folderKek's private key with each user's public key
            val privateKEK = folderKek.secretKey
            val ownEncryptedPrivateKEK = seal(
                messageBytes = privateKEK.asBytes,
                publicKeyRecipient = usersKeypair.publicKey.asBytes)

            // Decrypt
            val result = open(
                ownEncryptedPrivateKEK!!,
                usersKeypair
            )
            assertArrayEquals(privateKEK.asBytes, result)
        }
    }

}
