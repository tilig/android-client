package com.tilig.android

import com.tilig.android.data.models.ApiSuccessResponse
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.models.tilig.Brand

object Mocks {
    // It's not up to these tests to test encryption
    const val decryptedUsername = "aUsername"
    const val decryptedSecret = "ASecretPassword"
    const val decryptedNotes = "some encrypted notes too"

//    val mockAccount = Account(
//        id = "id0",
//        name = "Test Account",
//        website = "http://google.com",
//        encryptedUsername = decryptedUsername,
//        encryptedSecret = decryptedSecret,
//        encryptedNotes = decryptedNotes,
//        encryptedSecretLegacy = null,
//        android_app_id = null,
//        encryptedOtp = null,
//        shared = null,
//        createdAt = null,
//        updatedAt = null,
//        brand = null
//    )

    val mockBrand = Brand(
        id = "id0",
        name = "brand",
        domain = "domain",
        publicSuffixDomain = null,
        totp = true,
        mainColorHex = null,
        mainColorBrightness = null,
        logoSource = null,
        iconSource = null,
        isFetched = false
    )

//    val mockAccounts = listOf(
//        mockAccount,
//        mockAccount.copy(id = "id01"),
//        mockAccount.copy(id = "id02"),
//        mockAccount.copy(id = "id03")
//    )

    val successResponseBrand = ApiSuccessResponse(mockBrand)
}