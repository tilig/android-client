//package com.tilig.android.details
//
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.test.assertIsDisplayed
//import androidx.compose.ui.test.hasText
//import androidx.compose.ui.test.junit4.createComposeRule
//import androidx.compose.ui.test.onNodeWithText
//import com.tilig.android.Mocks
//import com.tilig.android.analytics.Mixpanel
//import com.tilig.android.data.models.tilig.PasswordVersion
//import com.tilig.android.ui.detail.AccountContent
//import com.tilig.crypto.Crypto
//import io.mockk.MockKAnnotations
//import io.mockk.coEvery
//import io.mockk.mockk
//import org.junit.Before
//import org.junit.Rule
//import org.junit.Test
//
//class AccountDetailsViewScreenTest{
//
//    @get:Rule
//    val composeTestRule = createComposeRule()
//
//    val tracker: Mixpanel = mockk()
//    val crypto: Crypto = mockk()
//
//    @Before
//    fun setup() {
//        MockKAnnotations.init(this, relaxUnitFun = true)
//        coEvery { tracker.trackEvent(any()) } returns Unit
//        coEvery { crypto.decrypt(any()) } returns "value"
//        coEvery { crypto.decryptSafely(any()) } returns "value"
//    }
//
//    @Test
//    fun viewAccountContentTest() {
//        with(composeTestRule) {
//            setContent {
//                AccountContent(
//                    account = Mocks.mockAccount,
//                    passwordsHistory = null,
//                    crypto = crypto,
//                    tracker = tracker,
//                    modifier = Modifier,
//                    onContentAction = {}
//                )
//            }
//
//            onNode(hasText("Username")).assertIsDisplayed()
//            onNodeWithText("Password").assertIsDisplayed()
//            onNode(hasText("Website")).assertIsDisplayed()
//            onNodeWithText("Notes").assertIsDisplayed()
//            onNodeWithText("View password history").assertDoesNotExist()
//        }
//    }
//
//    @Test
//    fun viewAccountContentWithPassHistoryTest() {
//        with(composeTestRule) {
//            setContent {
//                AccountContent(
//                    account = Mocks.mockAccount,
//                    passwordsHistory = listOf(
//                        PasswordVersion(
//                            id = "01",
//                            encryptedSecret = "encryptedSecret",
//                            createdAt = "createdAt"
//                        )
//                    ),
//                    crypto = crypto,
//                    tracker = tracker,
//                    modifier = Modifier,
//                    onContentAction = {}
//                )
//            }
//
//            onNode(hasText("Username")).assertIsDisplayed()
//            onNodeWithText("Password").assertIsDisplayed()
//            onNode(hasText("Website")).assertIsDisplayed()
//            onNodeWithText("Notes").assertIsDisplayed()
//            onNodeWithText("View password history").assertIsDisplayed()
//        }
//    }
//}