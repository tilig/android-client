//package com.tilig.android.details
//
//import androidx.compose.foundation.layout.Box
//import androidx.compose.foundation.layout.fillMaxSize
//import androidx.compose.material.ExperimentalMaterialApi
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.graphics.Color
//import androidx.compose.ui.platform.LocalContext
//import androidx.compose.ui.test.*
//import androidx.compose.ui.test.junit4.createComposeRule
//import androidx.lifecycle.LiveData
//import com.tilig.android.Mocks
//import com.tilig.android.TestHelpers
//import com.tilig.android.analytics.Mixpanel
//import com.tilig.android.data.models.Resource
//import com.tilig.android.data.models.tilig.DeleteResponse
//import com.tilig.android.data.models.tilig.PasswordVersion
//import com.tilig.android.ui.components.getIcon
//import com.tilig.android.ui.detail.AccountUpdates
//import com.tilig.android.ui.detail.EditAccountContent
//import com.tilig.android.ui.detail.Mode
//import com.tilig.android.ui.detail.components.AccountCollapsableContainer
//import com.tilig.android.ui.detail.holders.rememberEditAccountStateHolder
//import com.tilig.android.ui.theme.TiligTheme
//import com.tilig.crypto.Crypto
//import io.mockk.MockKAnnotations
//import io.mockk.coEvery
//import io.mockk.mockk
//import org.junit.Before
//import org.junit.Rule
//import org.junit.Test
//
//class AccountDetailsEditScreenTest {
//
//    @get:Rule
//    val composeTestRule = createComposeRule()
//
//    val tracker: Mixpanel = mockk()
//    val crypto: Crypto = mockk()
//    val accountUpdates = object : AccountUpdates {
//        override fun updateAccountName(name: String) {
//        }
//
//        override fun updateAccount(
//            crypto: Crypto,
//            name: String,
//            emailOrUsername: String,
//            password: String,
//            website: String,
//            notes: String
//        ) {
//        }
//
//        override fun autosaveEditableData(
//            crypto: Crypto,
//            name: String,
//            emailOrUsername: String,
//            password: String,
//            website: String,
//            notes: String
//        ) {
//        }
//
//        override fun updateWebsiteBrandData(website: String) {
//        }
//
//        override fun isAccountDataChanged(
//            crypto: Crypto,
//            name: String,
//            emailOrUsername: String,
//            password: String,
//            website: String,
//            notes: String
//        ): Boolean {
//            return false
//        }
//
//        override fun deleteAccount() {
//        }
//
//        override var deleteAccountResult: LiveData<Resource<DeleteResponse>>?
//            get() = null
//            set(value) {}
//
//    }
//
//    @Before
//    fun setup() {
//        MockKAnnotations.init(this, relaxUnitFun = true)
//        coEvery { tracker.trackEvent(any()) } returns Unit
//        coEvery { crypto.decrypt(any()) } returns "value"
//        coEvery { crypto.decryptSafely(any()) } returns "value"
//    }
//
//    @OptIn(ExperimentalMaterialApi::class)
//    @Test
//    fun editAccountContentTest() {
//        with(composeTestRule) {
//            setContent {
//                TiligTheme {
//                    AccountCollapsableContainer(
//                        accountIcon = null,
//                        backgroundColor = Color.Red,
//                        currentAccountName = "test",
//                        mode = Mode.EDIT,
//                        onAction = {  },
//                        content = {
//                            Box(modifier = Modifier.fillMaxSize()) {
//                                EditAccountContent(
//                                    accountUpdates = accountUpdates,
//                                    state = rememberEditAccountStateHolder(
//                                        account = Mocks.mockAccount,
//                                        crypto = crypto,
//                                        passwordsHistory = null,
//                                        accountUpdates = accountUpdates
//                                    ),
//                                    tracker = tracker,
//                                    modifier = Modifier,
//                                    onContentAction = {}
//                                )
//                            }
//                        }
//                    )
//                }
//            }
//
//            onNodeWithText("Account name").assertIsDisplayed()
//            onNodeWithText("Password").assertIsDisplayed()
//            onNode(hasText("Website")).assertIsDisplayed()
//            onNodeWithText("Notes", true).assertIsDisplayed()
//            onNodeWithText("Two-factor Authentication").assertDoesNotExist()
//            onNodeWithText("View password history").assertIsNotEnabled()
//        }
//    }
//
//    @OptIn(ExperimentalMaterialApi::class)
//    @Test
//    fun editAccountContentWithPassHistoryAnd2FATest() {
//        with(composeTestRule) {
//            setContent {
//                TiligTheme {
//                    AccountCollapsableContainer(
//                        accountIcon = null,
//                        backgroundColor = Color.Red,
//                        currentAccountName = "test",
//                        mode = Mode.EDIT,
//                        onAction = {  },
//                        content = {
//                            Box(modifier = Modifier.fillMaxSize()) {
//                                EditAccountContent(
//                                    accountUpdates = accountUpdates,
//                                    state = rememberEditAccountStateHolder(
//                                        account = Mocks.mockAccount.copy(brand = Mocks.mockBrand.copy(totp = true)),
//                                        crypto = crypto,
//                                        passwordsHistory = listOf(
//                                            PasswordVersion(
//                                                id = "01",
//                                                encryptedSecret = "encryptedSecret",
//                                                createdAt = "createdAt"
//                                            )
//                                        ),
//                                        accountUpdates = accountUpdates,
//                                    ),
//                                    tracker = tracker,
//                                    modifier = Modifier,
//                                    onContentAction = {}
//                                )
//                            }
//                        }
//                    )
//                }
//            }
//
//            onNodeWithText("Account name").assertIsDisplayed()
//            onNodeWithText("Password").assertIsDisplayed()
//            onNode(hasText("Website")).assertIsDisplayed()
//            onNodeWithText("Notes", true).assertIsDisplayed()
//            onNodeWithText("Two-Factor Authentication").assertIsDisplayed()
//            onNodeWithText("View password history").assertIsEnabled()
//        }
//    }
//}