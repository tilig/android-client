package com.tilig.android

import com.tilig.android.ui.twofa.bottomsheets.securitycode.CountDownFormatUtils
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.*

class CountDownFormatUtilsTest {

    private val calendar = mockk<Calendar>()

    @Before
    fun init() {
        MockKAnnotations.init(this)
    }

    @Test
    fun get_initial_countdown_test() {
        mockkStatic(Calendar::class)
        every { calendar.get(Calendar.SECOND) } returns 0
        every { Calendar.getInstance() } returns calendar
        var millis = CountDownFormatUtils.getInitialTimeCountDown()
        Assert.assertEquals(30000, millis)

        every { calendar.get(Calendar.SECOND) } returns 25
        millis = CountDownFormatUtils.getInitialTimeCountDown()
        Assert.assertEquals(5000, millis)

        every { calendar.get(Calendar.SECOND) } returns 35
        millis = CountDownFormatUtils.getInitialTimeCountDown()
        Assert.assertEquals(25000, millis)

        every { calendar.get(Calendar.SECOND) } returns 60
        millis = CountDownFormatUtils.getInitialTimeCountDown()
        Assert.assertEquals(0, millis)

        every { calendar.get(Calendar.SECOND) } returns 30
        millis = CountDownFormatUtils.getInitialTimeCountDown()
        Assert.assertEquals(0, millis)

        every { calendar.get(Calendar.SECOND) } returns 31
        millis = CountDownFormatUtils.getInitialTimeCountDown()
        Assert.assertEquals(29000, millis)
    }
}