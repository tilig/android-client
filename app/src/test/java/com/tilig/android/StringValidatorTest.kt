package com.tilig.android

import com.tilig.android.utils.isValidUrl
import org.junit.Assert
import org.junit.Test

class StringValidatorTest {

    @Test
    fun websiteUrl_not_valid() {
        Assert.assertFalse("google com".isValidUrl())
        Assert.assertFalse("".isValidUrl())
        Assert.assertFalse("   ".isValidUrl())
        Assert.assertFalse("https://goo gle.com".isValidUrl())
    }

    @Test
    fun websiteUrl_valid() {
        Assert.assertTrue("google".isValidUrl())
        Assert.assertTrue("https://google.com".isValidUrl())
        Assert.assertTrue("google.com".isValidUrl())
    }
}