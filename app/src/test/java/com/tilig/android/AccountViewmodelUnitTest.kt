package com.tilig.android

class AccountViewmodelUnitTest {
//    private val viewModelState = MutableStateFlow(AccountDetailsViewModelState(isInitialState = false))
//    private val scope = CoroutineScope(Job() + Dispatchers.IO)
//    private val uiState = viewModelState
//        .map { it.toUiState() }
//        .stateIn(
//            scope,
//            SharingStarted.Eagerly,
//            viewModelState.value.toUiState()
//        )
//
//    @Test
//    fun getAccount_errorState() {
//        runBlocking {
//            // assume get account returned null
//            viewModelState.update {
//                it.copy(errorMessage = UiError(Mocks.errorResponseAccount.errorMessage), isLoading = false)
//            }
//            delay(1000)
//            Assert.assertEquals(uiState.value.isLoading, false)
//            Assert.assertTrue(uiState.value is AccountDetailsUiState.AccountError)
//        }
//    }
//
//    @Test
//    fun getAccount_successState() {
//        runBlocking {
//            // assume get account returned the account info
//            viewModelState.update {
//                it.copy(account = Mocks.successResponseAccount.body, isLoading = false)
//            }
//            delay(1000)
//            Assert.assertEquals(false, uiState.value.isLoading)
//            Assert.assertTrue(uiState.value is AccountDetailsSuccess)
//            val result = uiState.value as AccountDetailsSuccess
//            Assert.assertEquals("id0", result.account.id)
//            Assert.assertEquals("Test Account", result.account.name)
//            Assert.assertEquals("https://google.com", result.account.website)
//            Assert.assertEquals(Mocks.decryptedNotes, result.account.notes)
//            Assert.assertEquals(Mocks.decryptedUsername, result.account.username)
//            Assert.assertEquals(Mocks.decryptedSecret, result.account.password)
//        }
//    }
//
//    @Test
//    fun getAccount_successState_editFields() {
//        runBlocking {
//            // assume get account returned the account info
//            viewModelState.update {
//                it.copy(account = Mocks.successResponseAccount.body, isLoading = false)
//            }
//            delay(1000)
//            val alteredTag = " Altered"
//            val result = uiState.value as AccountDetailsSuccess
//
//            result.account.password = Mocks.decryptedSecret + alteredTag
//            result.account.username = Mocks.decryptedUsername + alteredTag
//            result.account.notes = Mocks.decryptedNotes + alteredTag
//
//            Assert.assertEquals(result.account.id, "id0")
//            Assert.assertEquals(result.account.name, "Test Account")
//            Assert.assertEquals(result.account.website, "https://google.com")
//            Assert.assertEquals(
//                result.account.notes,
//                Mocks.decryptedNotes + alteredTag
//            )
//            Assert.assertEquals(
//                result.account.username,
//                Mocks.decryptedUsername + alteredTag
//            )
//            Assert.assertEquals(
//                result.account.password,
//                Mocks.decryptedSecret + alteredTag
//            )
//        }
//    }
}
