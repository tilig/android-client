package com.tilig.android.holders

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.focus.FocusRequester
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.cards.CardFieldsStateHolder
import com.tilig.android.ui.secrets.CustomFieldStateWrapper
import io.mockk.mockk
import org.junit.Assert
import org.junit.Test

class CardFieldsStateHolderTest {

    private val focusManager: FocusManager = mockk()
    private val displayNameError: MutableState<Boolean> = mutableStateOf(false)
    private val displayCardNumberError: MutableState<Boolean> = mutableStateOf(false)
    private val displayCardExpireError: MutableState<Boolean> = mutableStateOf(false)
    private val customFields = mutableStateOf(listOf<CustomFieldStateWrapper>())

    private val cardFieldsStateHolder = CardFieldsStateHolder(
        focusManager = focusManager,
        focusRequester = FocusRequester(),
        nameState = TextFieldState("cardName", validator = { !displayNameError.value }),
        cardNumberState = TextFieldState(
            "cardNumber",
            validator = { !displayCardNumberError.value }),
        cardHolderState = TextFieldState("cardholder"),
        cardExpireState = TextFieldState(
            "cardExpire",
            validator = { !displayCardExpireError.value }),
        cardSecurityCodeState = TextFieldState("cardSecret"),
        cardPinState = TextFieldState("cardPin"),
        cardExtraInfoState = TextFieldState("cardExtraInfo"),
        cardZipCodeState = TextFieldState("cardZipCode"),
        displayNameError = displayNameError,
        displayCardNumberError = displayCardNumberError,
        displayCardExpireError = displayCardExpireError,
        cardCustomFields = customFields
    )

    @Test
    fun testCardNumberFieldMaxLength() {
        // Diners Club - International
        cardFieldsStateHolder.cardNumberState.text = "36558078946238"
        Assert.assertEquals(14, cardFieldsStateHolder.getCardNumberFieldMaxLength())
        // American Express (AMEX)
        cardFieldsStateHolder.cardNumberState.text = "342653443635183"
        Assert.assertEquals(15, cardFieldsStateHolder.getCardNumberFieldMaxLength())
        // VISA
        cardFieldsStateHolder.cardNumberState.text = "4539617731203553"
        Assert.assertEquals(null, cardFieldsStateHolder.getCardNumberFieldMaxLength())
        // Discover
        cardFieldsStateHolder.cardNumberState.text = "6011644017544569"
        Assert.assertEquals(null, cardFieldsStateHolder.getCardNumberFieldMaxLength())
        // Visa Electron
        cardFieldsStateHolder.cardNumberState.text = "4844784300652428"
        Assert.assertEquals(null, cardFieldsStateHolder.getCardNumberFieldMaxLength())
        // MasterCard
        cardFieldsStateHolder.cardNumberState.text = "5416064166288921"
        Assert.assertEquals(null, cardFieldsStateHolder.getCardNumberFieldMaxLength())
        // Maestro
        cardFieldsStateHolder.cardNumberState.text = "6762858045704927"
        Assert.assertEquals(null, cardFieldsStateHolder.getCardNumberFieldMaxLength())
    }

    @Test
    fun testAreAllRequiredFieldsSetup() {
        cardFieldsStateHolder.apply {
            nameState.text = ""
            cardNumberState.text = ""
            cardHolderState.text = ""
            cardExpireState.text = ""
            cardPinState.text = ""
            cardExtraInfoState.text = ""
            cardSecurityCodeState.text = ""
        }
        Assert.assertFalse(cardFieldsStateHolder.areAllRequiredFieldsSetup())
        cardFieldsStateHolder.apply {
            nameState.text = ""
            cardNumberState.text = ""
            cardHolderState.text = "Cardholder"
            cardExpireState.text = ""
            cardPinState.text = ""
            cardExtraInfoState.text = ""
            cardSecurityCodeState.text = "2345"
        }
        Assert.assertFalse(cardFieldsStateHolder.areAllRequiredFieldsSetup())
        Assert.assertFalse(cardFieldsStateHolder.displayCardNumberError.value)
        Assert.assertFalse(cardFieldsStateHolder.displayCardExpireError.value)
        Assert.assertTrue(cardFieldsStateHolder.displayNameError.value)
        cardFieldsStateHolder.apply {
            nameState.text = ""
            cardNumberState.text = "4566"
            cardHolderState.text = "Cardholder"
            cardExpireState.text = "123"
            cardPinState.text = ""
            cardExtraInfoState.text = ""
            cardSecurityCodeState.text = "2345"
        }
        Assert.assertFalse(cardFieldsStateHolder.areAllRequiredFieldsSetup())
        Assert.assertTrue(cardFieldsStateHolder.displayCardNumberError.value)
        Assert.assertTrue(cardFieldsStateHolder.displayCardExpireError.value)
        Assert.assertTrue(cardFieldsStateHolder.displayNameError.value)
        cardFieldsStateHolder.apply {
            nameState.text = "Card name"
            cardNumberState.text = "3542916119260905"
            cardHolderState.text = "Cardholder"
            cardExpireState.text = "123"
            cardPinState.text = ""
            cardExtraInfoState.text = ""
            cardSecurityCodeState.text = "2345"
        }
        Assert.assertTrue(cardFieldsStateHolder.areAllRequiredFieldsSetup())
        Assert.assertFalse(cardFieldsStateHolder.displayCardNumberError.value)
        Assert.assertTrue(cardFieldsStateHolder.displayCardExpireError.value)
        Assert.assertFalse(cardFieldsStateHolder.displayNameError.value)
    }
}