package com.tilig.android.holders

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.focus.FocusRequester
import com.tilig.android.analytics.Mixpanel
import com.tilig.android.data.models.tilig.CreditCard
import com.tilig.android.ui.accounts.detail.components.states.TextFieldState
import com.tilig.android.ui.cards.CardFieldsStateHolder
import com.tilig.android.ui.cards.details.holders.EditCardStateHolder
import com.tilig.android.ui.secrets.CustomFieldStateWrapper
import io.mockk.MockKAnnotations
import io.mockk.mockk
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class EditCardStateHolderTest {

    private val mockCreditCard = CreditCard.createEmpty().apply {
        id = "id0"
        name = "Test Card"
        holderName = "Holder Name"
        cardNumber = "5416064166288921"
        securityCode = "1234"
        pinCode = "0000"
        extraInfo = "extra info"
        expireDate = "12/22"
        createdAt = null
        updatedAt = null
    }

    private val itemDetailsViewModel: CardViewModel = mockk()

    private val focusManager: FocusManager = mockk()
    private val tracker: Mixpanel = mockk()

    private val displayNameError: MutableState<Boolean> = mutableStateOf(false)
    private val displayCardNumberError: MutableState<Boolean> = mutableStateOf(false)
    private val displayCardExpireError: MutableState<Boolean> = mutableStateOf(false)
    private val displayUnsavedAlert: MutableState<Boolean> = mutableStateOf(false)
    private val openDialog: MutableState<Boolean> = mutableStateOf(false)
    private val customFields = mutableStateOf(listOf<CustomFieldStateWrapper>())

    val cardFieldsStateHolder = CardFieldsStateHolder(
        focusManager = focusManager,
        focusRequester = FocusRequester(),
        nameState = TextFieldState(
            mockCreditCard.name,
            validator = { !displayNameError.value }),
        cardNumberState = TextFieldState(
            mockCreditCard.cardNumber,
            validator = { !displayCardNumberError.value }),
        cardHolderState = TextFieldState(mockCreditCard.holderName),
        cardExpireState = TextFieldState(
            mockCreditCard.expireDate?.replace("/", "") ?: "",
            validator = { !displayCardExpireError.value }),
        cardSecurityCodeState = TextFieldState(mockCreditCard.securityCode),
        cardPinState = TextFieldState(mockCreditCard.pinCode),
        cardExtraInfoState = TextFieldState(mockCreditCard.extraInfo),
        cardZipCodeState = TextFieldState(mockCreditCard.zipCode),
        displayNameError = displayNameError,
        displayCardNumberError = displayCardNumberError,
        displayCardExpireError = displayCardExpireError,
        cardCustomFields = customFields
    )

    @Before
    fun init() {
        MockKAnnotations.init(this)
    }

    private fun initEditState(): EditCardStateHolder = EditCardStateHolder(
        card = mockCreditCard,
        itemDetailsViewModel = itemDetailsViewModel,
        cardFieldsStateHolder = cardFieldsStateHolder,
        displayUnsavedAlert = displayUnsavedAlert,
        openDialog = openDialog,
        tracker = tracker,
    )

    @Test
    fun testIsItemChangedNumber() {
        val editCardStateHolder = initEditState()
        Assert.assertFalse(editCardStateHolder.isItemChanged())

        editCardStateHolder.cardFieldsStateHolder.cardNumberState.text = "1234"
        Assert.assertTrue(editCardStateHolder.isItemChanged())
    }

    @Test
    fun testIsItemChangedName() {
        val editCardStateHolder = initEditState()
        editCardStateHolder.cardFieldsStateHolder.nameState.text = ""
        Assert.assertTrue(editCardStateHolder.isItemChanged())
    }

    @Test
    fun testIsItemChangedPinChanges() {
        val editCardStateHolder = initEditState()
        editCardStateHolder.cardFieldsStateHolder.cardPinState.text = "123"
        Assert.assertTrue(editCardStateHolder.isItemChanged())
    }

    @Test
    fun testIsItemChangedSecurityCode() {
        val editCardStateHolder = initEditState()
        editCardStateHolder.cardFieldsStateHolder.cardSecurityCodeState.text = ""
        Assert.assertTrue(editCardStateHolder.isItemChanged())
    }

    @Test
    fun testIsItemChangedExpireDate() {
        val editCardStateHolder = initEditState()
        editCardStateHolder.cardFieldsStateHolder.cardExpireState.text = "01/01"
        Assert.assertTrue(editCardStateHolder.isItemChanged())
    }
}