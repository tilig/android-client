package com.tilig.android

import androidx.compose.ui.text.AnnotatedString
import com.tilig.android.utils.CardUtils
import org.junit.Assert
import org.junit.Test

class CardUtilsTest {

    @Test
    fun last_4_digits_get() {
        Assert.assertEquals("1234", CardUtils.getLast4CardDigits("1234"))
        Assert.assertEquals("1234", CardUtils.getLast4CardDigits("11234"))
        Assert.assertEquals("1234", CardUtils.getLast4CardDigits("99991234"))
        Assert.assertEquals("123", CardUtils.getLast4CardDigits("123"))
        Assert.assertEquals("1", CardUtils.getLast4CardDigits("1"))
    }

    @Test
    fun format_card_numbers() {
        Assert.assertEquals("1234 ", CardUtils.formatCardNumber(AnnotatedString("1234")).text.text)
        Assert.assertEquals("1234 4", CardUtils.formatCardNumber(AnnotatedString("12344")).text.text)
        Assert.assertEquals("1234 45", CardUtils.formatCardNumber(AnnotatedString("123445")).text.text)
        Assert.assertEquals("1234 4567 ", CardUtils.formatCardNumber(AnnotatedString("12344567")).text.text)
        Assert.assertEquals("1234 4567 1234 ", CardUtils.formatCardNumber(AnnotatedString("123445671234")).text.text)
        Assert.assertEquals("1234 4567 1234 5678 ", CardUtils.formatCardNumber(AnnotatedString("1234456712345678")).text.text)
        Assert.assertEquals("1234 4567 1234 5678 1111 ", CardUtils.formatCardNumber(AnnotatedString("12344567123456781111")).text.text)
        Assert.assertEquals("1234 4567 1234 5678 1111 1", CardUtils.formatCardNumber(AnnotatedString("123445671234567811111")).text.text)
    }
}