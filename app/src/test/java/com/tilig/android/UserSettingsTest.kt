package com.tilig.android

import com.tilig.android.data.models.tilig.SettingsSection
import com.tilig.android.data.models.tilig.UserSettings
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

class UserSettingsTest {

    @Test
    fun test_updating_user_profile_settings(){
        val profileSettings = UserSettings()

        profileSettings.setField(SettingsSection.ANDROID, "survey_finished", true)
        profileSettings.setField(SettingsSection.IOS, "survey_finished", true)

        Assert.assertTrue(profileSettings.findField(SettingsSection.ANDROID, "survey_finished") as Boolean)
        Assert.assertTrue(profileSettings.findField(SettingsSection.IOS, "survey_finished") as Boolean)
        Assert.assertEquals(1, profileSettings.userProfileSettings.getSectionMap(SettingsSection.ANDROID).size)
        Assert.assertEquals(1, profileSettings.userProfileSettings.getSectionMap(SettingsSection.IOS).size)

        profileSettings.setField(SettingsSection.IOS, "survey_finished", false)

        Assert.assertTrue(profileSettings.findField(SettingsSection.ANDROID, "survey_finished") as Boolean)
        Assert.assertFalse(profileSettings.findField(SettingsSection.IOS, "survey_finished") as Boolean)
        Assert.assertEquals(1, profileSettings.userProfileSettings.getSectionMap(SettingsSection.ANDROID).size)
        Assert.assertEquals(1, profileSettings.userProfileSettings.getSectionMap(SettingsSection.IOS).size)

        profileSettings.setField(SettingsSection.IOS, "ios_tested", true)

        Assert.assertTrue(profileSettings.findField(SettingsSection.ANDROID, "survey_finished") as Boolean)
        Assert.assertFalse(profileSettings.findField(SettingsSection.IOS, "survey_finished") as Boolean)
        Assert.assertTrue(profileSettings.findField(SettingsSection.IOS, "ios_tested") as Boolean)
        Assert.assertEquals(1, profileSettings.userProfileSettings.getSectionMap(SettingsSection.ANDROID).size)
        Assert.assertEquals(2, profileSettings.userProfileSettings.getSectionMap(SettingsSection.IOS).size)

        profileSettings.setField(SettingsSection.ANDROID, "count_test", 1)

        Assert.assertEquals(1, profileSettings.findField(SettingsSection.ANDROID, "count_test") as Int)
        Assert.assertEquals(2, profileSettings.userProfileSettings.getSectionMap(SettingsSection.ANDROID).size)

        profileSettings.setField(SettingsSection.ANDROID, "count_test", 50)

        Assert.assertEquals(50, profileSettings.findField(SettingsSection.ANDROID, "count_test") as Int)
    }
}