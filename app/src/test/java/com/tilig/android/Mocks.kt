package com.tilig.android

import android.net.Uri
import com.tilig.android.data.models.ApiEmptyResponse
import com.tilig.android.data.models.ApiErrorResponse
import com.tilig.android.data.models.ApiSuccessResponse
import com.tilig.android.data.models.tilig.Account
import com.tilig.android.data.models.tilig.SecretItem
import com.tilig.android.utils.getDomain
import com.tilig.android.utils.httpify
import com.tilig.crypto.Crypto
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic

object Mocks {
    // It's not up to these tests to test encryption
    const val decryptedUsername = "aUsername"
    const val decryptedSecret = "ASecretPassword"
    const val decryptedNotes = "some encrypted notes too"

    val mockAccount = Account.createEmpty().apply {

        // Mock Uri.parse before we use it in the website setter
        mockUris()

        id = "id0"
        name = "Test Account"
        website = "https://google.com"
        username = decryptedUsername
        password = decryptedSecret
        notes = decryptedNotes
        androidAppId = null
        otp = null
        createdAt = null
        updatedAt = null
    }

    private fun mockUris() {
        val uriGoogle: Uri = mockk()
        mockkStatic(Uri::class)
        every { uriGoogle.host } returns "google.com"
        every { Uri.parse("https://google.com") } returns uriGoogle
    }

    val mockAccounts = listOf(mockAccount, mockAccount, mockAccount, mockAccount)

    val successResponseAccounts: ApiSuccessResponse<List<SecretItem>> =
        ApiSuccessResponse(mockAccounts)
    val successResponseAccount = ApiSuccessResponse(mockAccount)

    val emptyResponseAccounts = ApiEmptyResponse<List<SecretItem>>()

    val errorResponseAccount = ApiErrorResponse<Account>(0, "oops")
    val errorResponseAccounts = ApiErrorResponse<List<SecretItem>>(0, "oops")
}
