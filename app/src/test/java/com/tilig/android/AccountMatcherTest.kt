package com.tilig.android

import Relation
import android.net.Uri
import com.tilig.android.autofill.AccountMatcher
import com.tilig.android.autofill.google.StructureParser
import com.tilig.android.data.models.tilig.UrlField
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class AccountMatcherTest {

    private var parser: StructureParser = mockk()
    private val uriGoogle: Uri = mockk()
    private val uriFacebook: Uri = mockk()
    private val uriMobileFacebook: Uri = mockk()
    private val uriExample: Uri = mockk()
    private val uriAirBnbCom: Uri = mockk()
    private val uriAirBnbDk: Uri = mockk()
    private val uriWWWAirBnbDk: Uri = mockk()
    private val relation = Relation(
        arrayOf("airbnb.com", "airbnb.dk"),
        arrayOf("com.airbnb.android.development", "com.airbnb.android")
    )

    @Before
    fun init() {
        MockKAnnotations.init(this)
    }

    fun mockUris() {
        mockkStatic(Uri::class)
        every { uriGoogle.host } returns "google.com"
        every { uriFacebook.host } returns "facebook.com"
        every { uriMobileFacebook.host } returns "m.facebook.com"
        every { uriExample.host } returns "example.com"
        every { uriAirBnbCom.host } returns "airbnb.com"
        every { uriAirBnbDk.host } returns "airbnb.dk"
        every { uriWWWAirBnbDk.host } returns "www.airbnb.dk"
        every { Uri.parse("https://google.com") } returns uriGoogle
        every { Uri.parse("http://facebook.com") } returns uriFacebook
        every { Uri.parse("https://facebook.com") } returns uriFacebook
        every { Uri.parse("http://m.facebook.com") } returns uriMobileFacebook
        every { Uri.parse("https://m.facebook.com") } returns uriMobileFacebook
        every { Uri.parse("https://example.com") } returns uriExample
        every { Uri.parse("http://airbnb.com") } returns uriAirBnbCom
        every { Uri.parse("https://airbnb.com") } returns uriAirBnbCom
        every { Uri.parse("https://airbnb.dk") } returns uriAirBnbDk
        every { Uri.parse("https://www.airbnb.dk") } returns uriWWWAirBnbDk
    }

    @Test
    fun accountMatchesContexts_no_match() {
        mockUris()
        every { parser.getWebsite() } returns null
        every { parser.isWebsite } returns false
        every { parser.getStructPackageName() } returns "com.example.app"
        val account = Mocks.mockAccount
        // account with website example.com should not match parser with app id and no relevant relations
        Assert.assertEquals(
            AccountMatcher.NO_MATCH,
            AccountMatcher.accountMatchesContexts(account, parser, relation)
        )

        val account1 = Mocks.mockAccount.copy(
            overview = Mocks.mockAccount.overview.copy(
                urls = mutableListOf(
                    UrlField(url = "https://example.com")
                )
            )
        )
        every { parser.getWebsite() } returns "https://google.com"
        every { parser.isWebsite } returns true
        every { parser.getStructPackageName() } returns null
        // account with website example.com should not match parser with website google.com
        Assert.assertEquals(
            AccountMatcher.NO_MATCH,
            AccountMatcher.accountMatchesContexts(account1, parser, relation)
        )
    }

    @Test
    fun accountMatchesContexts_perfect_match() {
        mockUris()

        val account = Mocks.mockAccount.copy(
            overview = Mocks.mockAccount.overview.copy(
                androidAppIds = mutableListOf("com.example")
            )
        )
        every { parser.getWebsite() } returns null
        every { parser.isWebsite } returns false
        every { parser.getStructPackageName() } returns "com.example"
        // Account with website google.com and app id com.example matches perfectly with parser for app com.example
        Assert.assertEquals(
            1f,
            AccountMatcher.accountMatchesContexts(account, parser, relation)
        )

        // And perfectly for a parser for website google.com
        every { parser.getWebsite() } returns "https://google.com"
        every { parser.isWebsite } returns true
        Assert.assertEquals(
            AccountMatcher.PERFECT_MATCH,
            AccountMatcher.accountMatchesContexts(account, parser, relation)
        )
    }

    @Test
    fun accountMatchesContexts_same_subdomain_score_match() {
        mockUris()
        every { parser.getWebsite() } returns "http://m.facebook.com"
        every { parser.isWebsite } returns true
        every { parser.getStructPackageName() } returns null

        // m.facebook.com matches, despite different protocols (http(s))
        val account1 = Mocks.mockAccount.copy(
            overview = Mocks.mockAccount.overview.copy(
                urls = mutableListOf(
                    UrlField(url = "https://m.facebook.com")
                )
            )
        )
        Assert.assertEquals(
            AccountMatcher.SAME_SUBDOMAIN_SCORE,
            AccountMatcher.accountMatchesContexts(account1, parser, relation)
        )
        // also despite no protocol at all
        val account2 = Mocks.mockAccount.copy(
            overview = Mocks.mockAccount.overview.copy(
                urls = mutableListOf(
                    UrlField(url = "m.facebook.com")
                )
            )
        )
        Assert.assertEquals(
            AccountMatcher.SAME_SUBDOMAIN_SCORE,
            AccountMatcher.accountMatchesContexts(account2, parser, relation)
        )
    }

    @Test
    fun accountMatchesContexts_same_main_domain_score_match() {
        mockUris()
        every { parser.getWebsite() } returns "http://facebook.com"
        every { parser.isWebsite } returns true
        every { parser.getStructPackageName() } returns null

        // Account with m.facebook.com matches parser with website facebook.com
        val account1 = Mocks.mockAccount.copy(
            overview = Mocks.mockAccount.overview.copy(
                urls = mutableListOf(
                    UrlField(url = "https://m.facebook.com")
                )
            )
        )
        Assert.assertEquals(
            AccountMatcher.SAME_MAIN_DOMAIN_SCORE,
            AccountMatcher.accountMatchesContexts(account1, parser, relation)
        )
        // And the other way around
        val account2 = Mocks.mockAccount.copy(
            overview = Mocks.mockAccount.overview.copy(
                urls = mutableListOf(
                    UrlField(url = "https://facebook.com")
                )
            )
        )
        every { parser.getWebsite() } returns "http://m.facebook.com"
        Assert.assertEquals(
            AccountMatcher.SAME_MAIN_DOMAIN_SCORE,
            AccountMatcher.accountMatchesContexts(account2, parser, relation)
        )
    }

    @Test
    fun accountMatchesContexts_related_domain_match() {
        mockUris()
        every { parser.isWebsite } returns true
        every { parser.getStructPackageName() } returns null

        // Account with airbnb.com matches parser with website airbnb.dk.
        // This only works through the given relations, cannot match on TLD
        val account1 = Mocks.mockAccount.copy(
            overview = Mocks.mockAccount.overview.copy(
                urls = mutableListOf(
                    UrlField(url = "https://airbnb.dk")
                )
            )
        )
        every { parser.getWebsite() } returns "http://airbnb.com"
        Assert.assertEquals(
            AccountMatcher.RELATED_DOMAIN_SCORE,
            AccountMatcher.accountMatchesContexts(account1, parser, relation)
        )
        // And the other way around
        val account2 = Mocks.mockAccount.copy(
            overview = Mocks.mockAccount.overview.copy(
                urls = mutableListOf(
                    UrlField(url = "https://airbnb.com")
                )
            )
        )
        every { parser.getWebsite() } returns "https://airbnb.dk"
        Assert.assertEquals(
            AccountMatcher.RELATED_DOMAIN_SCORE,
            AccountMatcher.accountMatchesContexts(account2, parser, relation)
        )
        // Also if one of both has a subdomain that is not in the relations
        val account3 = Mocks.mockAccount.copy(
            overview = Mocks.mockAccount.overview.copy(
                urls = mutableListOf(
                    UrlField(url = "https://www.airbnb.dk")
                )
            )
        )
        every { parser.getWebsite() } returns "http://airbnb.com"
        Assert.assertEquals(
            AccountMatcher.RELATED_DOMAIN_SCORE,
            AccountMatcher.accountMatchesContexts(account3, parser, relation)
        )
        // Annnnnd the other way around again
        val account4 = Mocks.mockAccount.copy(
            overview = Mocks.mockAccount.overview.copy(
                urls = mutableListOf(
                    UrlField(url = "https://airbnb.com")
                )
            )
        )
        every { parser.getWebsite() } returns "https://www.airbnb.dk"
        Assert.assertEquals(
            AccountMatcher.RELATED_DOMAIN_SCORE,
            AccountMatcher.accountMatchesContexts(account4, parser, relation)
        )
    }

    @Test
    fun accountMatchesContexts_related_app_id_match() {
        mockUris()

        // Account with com.airbnb.android matches parser with website airbnb.dk.
        // This only works through the given relations, cannot match on TLD
        val account = Mocks.mockAccount.copy(
            overview = Mocks.mockAccount.overview.copy(
                androidAppIds = mutableListOf("com.airbnb.android")
            )
        )
        every { parser.isWebsite } returns true
        every { parser.getStructPackageName() } returns null
        every { parser.getWebsite() } returns "http://airbnb.com"
        Assert.assertEquals(
            AccountMatcher.RELATED_APP_SCORE,
            AccountMatcher.accountMatchesContexts(account, parser, relation)
        )

        // Account with com.airbnb.android also matches parser for com.airbnb.android.development
        every { parser.isWebsite } returns false
        every { parser.getStructPackageName() } returns "com.airbnb.android.development"
        every { parser.getWebsite() } returns null
        Assert.assertEquals(
            AccountMatcher.RELATED_APP_SCORE,
            AccountMatcher.accountMatchesContexts(account, parser, relation)
        )
    }
}
