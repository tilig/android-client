package com.tilig.android

import androidx.compose.ui.text.input.KeyboardType
import com.tilig.android.utils.TextInputUtils
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class TextInputUtilsTest {

    @Test
    fun is_valid_input_based_on_keyboard_type_number() {
        Assertions.assertFalse(TextInputUtils.isInputTextSatisfyKeyboardType("44 555 55", KeyboardType.Number))
        Assertions.assertFalse(TextInputUtils.isInputTextSatisfyKeyboardType("44 555 g5", KeyboardType.Number))
        Assertions.assertFalse(TextInputUtils.isInputTextSatisfyKeyboardType("-44", KeyboardType.Number))
        Assertions.assertFalse(TextInputUtils.isInputTextSatisfyKeyboardType("44.0", KeyboardType.Number))
        Assertions.assertTrue(TextInputUtils.isInputTextSatisfyKeyboardType("44555", KeyboardType.Number))
    }

    @Test
    fun is_valid_input_based_on_keyboard_type_decimal() {
        Assertions.assertFalse(TextInputUtils.isInputTextSatisfyKeyboardType("44 555 55", KeyboardType.Decimal))
        Assertions.assertFalse(TextInputUtils.isInputTextSatisfyKeyboardType("4aa", KeyboardType.Decimal))
        Assertions.assertTrue(TextInputUtils.isInputTextSatisfyKeyboardType("-44", KeyboardType.Decimal))
        Assertions.assertTrue(TextInputUtils.isInputTextSatisfyKeyboardType("44.0", KeyboardType.Decimal))
        Assertions.assertTrue(TextInputUtils.isInputTextSatisfyKeyboardType("44555", KeyboardType.Decimal))
        Assertions.assertTrue(TextInputUtils.isInputTextSatisfyKeyboardType("44555.5", KeyboardType.Decimal))
    }
}