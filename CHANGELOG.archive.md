# Archive

Active changelogs are in [CHANGELOG.md](./CHANGELOG.md). This file holds the archive
because the total changelog file exceeded the maximum length allowed as release notes on Firebase.

-----------------------------------

## 3.10.4 - 2022-01-06

### Fixed

- Fixed a crash in autocapture flow ([536][https://gitlab.com/subshq/clients/tilig-android/-/issues/536]))
- Autocapture not saving changes ([537][https://gitlab.com/subshq/clients/tilig-android/-/issues/537])


## 3.10.3 - 2022-01-05

### Fixed

- Typo in Add Account screen ([#524][https://gitlab.com/subshq/clients/tilig-android/-/issues/524])
- Wrong text in dialog when deleting item ([#522][https://gitlab.com/subshq/clients/tilig-android/-/issues/522])
- Consistent capitalization of 'WiFi'
- Button 'Delete account' -> 'Delete Login' (and dito capitalization for CC, Note and WiFi) ([#523][https://gitlab.com/subshq/clients/tilig-android/-/issues/523])
- Creating a second note would prefill it with the information of the last note and skip to the View screen ([#529][https://gitlab.com/subshq/clients/tilig-android/-/issues/529])


## 3.10.2 - 2023-01-02 internal only

### Added

- For developers only: a Debug button the account detail screen that can share all details for the account with developers

### Changed

- When clicking on the '+ button' when you use Tilig to autofill it should prefill the title and website ([!260][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/260])
- Animation between list and details screen ([!256][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/256])

### Removed

- Developer toggle to load the list of accounts in the autofill

### Fixed

- Secure and Pin code are visible by default while entering on Add Card screen ([!258][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/258])
- Web site is not updated on Edit/View screen after editing ([!259][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/259])
- Autofill Extension close modal only interacting with the header ([!261][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/261])
- Crash: only fully opaque screens are allowed to request a screen orientation ([ANDROID-APP-GX][https://sentry.io/organizations/tilig/issues/3832469518/?project=5237937])
- Suppressed Sentry notice when Failing to get autofill status, since it is an unfixable OS error ([ANDROID-APP-H7][https://sentry.io/organizations/tilig/issues/3841493410/?project=5237937])
- Avoid logging message about 'setTriggerId'; now has a compatibility check ([ANDROID-APP-H2][https://sentry.io/organizations/tilig/issues/3837099471/?project=5237937])
- Catch sign-in failure and show error to the user ([#512][https://gitlab.com/subshq/clients/tilig-android/-/issues/512], [ANDROID-APP-HE][https://sentry.io/organizations/tilig/issues/3848727605/?project=5237937])
- BadParcelableException occurring in auto-capture (for some users; could not reproduce) ([ANDROID-APP-DZ][https://sentry.io/organizations/tilig/issues/3657273819/?project=5237937])


## 3.10.0 - Upcoming

### Added

- Apps section in the settings ([!234][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/234])
- For developers: quick-switch buttons for autofill providers ([#370][https://gitlab.com/subshq/clients/tilig-android/-/issues/370])

### Fixed

- Show a spinner when loading a long list of accounts ([!227][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/227])
- Feedback on share link design and search input field ([#447][https://gitlab.com/subshq/clients/tilig-android/-/issues/474], [!238][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/238])
- Don't show copy-to-clipboard toast on Android 13, which shows a native one ([!231][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/231])
- UI for scanning QR codes is broken ([!243][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/243])
- Statusbar briefly visible in animation when autofill screen opens ([!244][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/244])
- Sometimes a login error on Samsung S21 ([!242][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/242])
- Search bar would autocorrect names, such as adding spaces in brand names ([#382][https://gitlab.com/subshq/clients/tilig-android/-/issues/382])
- 'Save login' button did not look disabled when it was disabled ([#384][https://gitlab.com/subshq/clients/tilig-android/-/issues/384])
- Valid creditcard numbers were considered invalid ([!257][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/257])
- Aligned share link text ([!253][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/253])
- Removed spinner when updating brand during edit ([!255][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/255])


## 3.9.11 - 2022-12-20

### Fixed

- For existing users, the homescreen needs refreshing after fetching the new keypair


## 3.9.10 - 2022-12-19

### Added

- For developers only: skip button for the autofill step

### Changed

- Hide Microsoft button behind a feature flag ([#505][https://gitlab.com/subshq/clients/tilig-android/-/issues/505])
- Add user ID to conversion analytics

### Fixed

- Restore password history incorrect ([!237][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/237])
- User gets an error if shared link has been deleted on a web ([!223][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/223])
- Original email briefly displays after editing it ([!236][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/236])
- OTP code copied not tracked ([#477][https://gitlab.com/subshq/clients/tilig-android/-/issues/477])
- Regression: Autofill fails when creating a new login item for Apps ([#502][https://gitlab.com/subshq/clients/tilig-android/-/issues/502])
- Delete account confirmation was case sensitive ([#454][https://gitlab.com/subshq/clients/tilig-android/-/issues/454])
- Back navigation in the autofill popup ([!232][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/232])
- Don't offer to autofill or autocapture on app.tilig.com or staging website ([#442][https://gitlab.com/subshq/clients/tilig-android/-/issues/442])
- Note title inconsistent with iOS ([!246][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/246], and [!245][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/245])


## 3.9.9 - Not released

### Fixed

All fixes from 3.9.8, plus:

- Regression: crash when creating a new item ([#487][https://gitlab.com/subshq/clients/tilig-android/-/issues/487])


## 3.9.8 - Not released

### Added

- UTM tracking in Mixpanel ([!219][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/219])
- New autofill design ([!210][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/210])
- Create share links ([!223][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/223])
- Libsodium keypairs ([!222][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/222])
- Microsoft login ([!225][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/225])

### Changed

- Updated copy text before opening autofill settings
- Deleting account now accepts lowercase 'delete' as input as well

### Fixed

- 2FA countdown started from time when screen was opened instead of last half-minute ([!218][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/218])
- Search filter and scroll position lost when navigating back to account list ([!226][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/226])
- Password history missing the first item, and sort order incorrect ([!228][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/228])


## 3.8.4 - 2022-11-28

### Fixed

- Creating and editing secure notes results in empty title and or empty note content field ([!457][https://gitlab.com/subshq/clients/tilig-android/-/issues/457])


## 3.8.3 - 2022-11-15

### Fixed

- The settings screen could repeatedly trigger fetching items ([!446][https://gitlab.com/subshq/clients/tilig-android/-/issues/446])


## 3.8.2 - 2022-11-14 (unreleased)

### Added

- Encryption migration ([!211][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/211])
- Different limit for note length based on if legacy encryption is enabled or not ([!212][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/212])
- Improved UI flow around account deletion ([!217][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/217])

### Fixed

- QR code doesn't scan on certain devices ([#430][https://gitlab.com/subshq/clients/tilig-android/-/issues/430])
- Purple buttons could run over 2 lines for long account names ([!213][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/213])
- Increased tap space on the website URL ([!214][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/214])

## 3.8.1 - 2022-11-7

### Fixed

- Crash for certain users ([ANDROID-APP-EH][https://sentry.io/organizations/tilig/issues/3701759550/?project=5237937])


## 3.8 - 2022-10-27

### Added

- Wifi-account support
- Zipcode field for creditcards ([!198][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/198])
- Confirmation dialog when deleting entire account ([!186][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/186])

### Fixed

- Crash for a user with a `null` `notes` field ([ANDROID-APP-EA][https://sentry.io/organizations/tilig/issues/3694258469/?project=5237937])
- Changed base64 encoding of the DEK to legacy version (default, w/o newlines)
- No search bar visible in login/signup screen in autofill flow ([#427][https://gitlab.com/subshq/clients/tilig-android/-/issues/427])
- Ensure sorting is A-Z ([#426][https://gitlab.com/subshq/clients/tilig-android/-/issues/426])
- Ensure `info` field is nullable ([!185][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/185])
- Same loading animation as account details for other items ([!187][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/187])


## 3.7 - 2022-10-24

### Added

- Creditcard support ([!166][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/166])
- Improved error states ([!161][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/161])
- A/B-test the Test Drive ([!177][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/177])

### Fixed

- Save dialog showing when there are no changes ([#416][https://gitlab.com/subshq/clients/tilig-android/-/issues/416])
- Refactoring/cleanup of crypto key code ([!181][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/181])
- Error messages during sign in were not showing
- Mixpanel ID migration for version 2.0 was throwing authentication errors when not logged in
- Brand not updating after edit ([!184][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/184/])


## 3.6.2 - 2022-10-07

### Added

- Tapping the name of an app (in account details of an account with android_app_id) try to open the app instead of a website (![171][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/171])
- Tracking Firebase conversion events for autofill_enabled and first_autofill (#420)

### Fixed

- Could open the add flow without tapping the Add button ([!167][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/167])
- The Add-button overlapped the yellow autofill warning bar ([!169][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/169])
- Autocapture could show 'Tilig saved your null account' ([!170][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/170])
- Crash in QR scanner ([!172][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/172])
- Crash on older devices when app is paused in the detail screen ([!173][https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/173])
- Crash on older devices in autocapture (![409][https://gitlab.com/subshq/clients/tilig-android/-/issues/409])


## 3.6 - 2022-10-03

### Fixed

- Crash in the background, originating from a TiligService memory issue (ANDROID-APP-CW)
- False report that keypair was broken
- Preloading the service not working on old devices (ANDROID-APP-CZ)
- Crash in background, when TiligService onStartCommand received no Intent (ANDROID-APP-C8)
- Null-check in AddNoteBottomSheet (ANDROID-APP-CQ)
- Not setting ID or email in Sentry reports
- Couldn't always base64-decode encrypted fields created by iOS (#400)
- Fields in details > main would not show on web app if they contained a `name` field
- Field details > notes would prevent account from showing in webapp if it was null


## 3.5 - 2022-09-23

### Added

- Secure notes with updated UI and fixed comments

### Changed

- New encryption implementation

### Fixed

- Crash if the DEK had the wrong format
- Crash in service, probably memory-related [ANDROID_APP_C8](https://sentry.io/organizations/tilig/issues/3602284878/?project=5237937)
- Survey question 3 should be skippable without answering it


## 3.4.7 - 2022-09-13

### Added

- Test-drive feature for new users [!113](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/113)

### Fixed

- Preload and keep autofill service [!155](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/155)
- Reduce startup time of app [!156](https://gitlab.com/subshq/clients/tilig-android/-/merge_requests/156)


## 3.4.5 - 2022-09-01

### Fixed

- Related domains matching not working on www. or other subdomains
- Website edit field did not use the URL keyboard type
- Switching to settings and back would keep the dark icons in the statusbar


## 3.4.4 - 2022-08-31

### Added

- Match accounts based on related domains and related application IDs [#373](https://gitlab.com/subshq/clients/tilig-android/-/issues/373)
- Reinstated a button to enable 2FA for accounts that are not in the 2fa directory. [#378](https://gitlab.com/subshq/clients/tilig-android/-/issues/378)


## 3.4.3 - 2022-08-24

### Added

- Screen that shows when you are not connected to the network

### Fixed

- Notes field does not scroll into view when tapped
- Icons sometimes flickering
- Back button not working in the 'more info about signing' sheet
- Don't autosave passwords that are masked by Chrome or other browser


## 3.4.2 - 2022-08-?

### Added

- Support for autofilling OTP codes (#333)
- Red warning borders when fields are not filled in (#342)
- Saving indicator (#341)

### Changed

- Tweaked some paddings, rounded corners (#340, #343)

### Fixed

- Last character in slot machine animation did not animate (#366)
- New type of storage for the private key, hopefully fixing key errors (#231)
- Crash for one user when autofilling. (ANDROID-APP-A1)


## 3.4.1 - 2022-08-01

### Fixed

- Logging out didn't work the second time during a session (#347)
- No "Sign Out" event in Mixpanel (#348)
- Updated Sentry
- Design tweaks: padding, edit field centering, and many small tweaks in the 2FA instruction screen
- Autofill popup would close without a message when hitting the Fill button, even when storing the new account failed (#355)
- Replaced cryptic error messages with human-readable messages


## 3.4.0 - 2022-07-25

### Added

- Autosave functionality (#329)
- FAQ, terms & conditions, and more information in the settings screen (#325)

### Changed

- Improved autofilling in webviews (#302, #303, #327, #332)
- Replaced the bottom navigation with a gear icon in the toolbar (#325)

### Fixed

- Improved many small design issues in the 2Fa instruction screens (#264)
- Improved background header and icon loading (less flickering) (#338)
- Fixed a strange crash on app start that is likely an Android 13 Beta bug
- Inputfields in webviews with autocomplete="chrome-off" were ignored by Tilig
- In WebViews, the Hint or ID was never considered (#332)
- When autofilling in Chrome, Tilig would sometimes present accounts for Chrome instead of for the website
- Improved detection of previously undetected input fields by input type
- Crash due to a timing issue (ANDROID-APP-99)
- Crash at random times due to checking connection too often: TooManyRequestsException (ANDROID-APP-8A, ANDROID-APP-83)
- Caught 2 impossible crashes in SlotText animation (ANDROID-APP-8B, ANDROID-APP-8R)
- Caught when launching an Activity for result. Could be a Compose bug. (ANDROID-APP-7J)
- Crash when fillRequest is still null from the intent extras. (ANDROID-APP-8Z)
- Timing issue when logging in and out, causing a crash (ANDROID-APP-96)
- Yellow autofill reminder bar did not show if user had enabled it before.


## 3.3.21 - 2022-07-12

### Added

- Biometrics lock screen
- New Account details edit/view

### Changed

- Different icon for brands

### Fixed

- Crash in BottomNavigation


## 3.3.14 - 2022-06-23

### Added

- New add account flow with improvements and autocomplete api call
- New slot animation (#309)

### Fixed

- Possible crash when monkey-tapping actions that return to Autofill target app. (ANDROID-APP-7Q)
- Impossible crash (ANDROID-APP-7K)
- Gracefully handle Firebase login issue (ANDROID-APP-80)
- Catch navigation error (ANDROID-APP-7Z)
- Timing issue when requesting focus on the title field (ANDROID-APP-81)
- Text overlapping during onboaring on smaller screens (#281)
- Styling issues on the success survey screen (#257)


## 3.3.13 - 2022-06-08

### Changed

- Typing website now enters its brandfetch name in the title.
- New survey question

### Fixed

- Mixpanel events 'AutoFill Triggered' and 'AutoFill Suggested' not tracked
- Sentry reports of BottomNavigation still crashing when users are monkeys


## 3.3.12 - 2022-06-01

### Added

- Removed logo from footer in autofill dropdown when other Tilig logo is right above it.
- Close keyboard after searching and selecting account (#299)

### Fixed

- Crash when returning dataset to target app (ANDROID-APP-77)
- Notes field not scrolling nicely into view when tapped (#297)
- Close keyboard when returning to the account list (#296)
- Trim trailing spaces when entering email/username or website (#294)
- Trim trailing spaces when autofilling email/username or website (#294)
- Several Sentry-reported issues regarding monkey-usage (previously fixed but not merged yet)
- Sentry-reported issue about certain OS version crashing the Autofill screen (ANDROID-APP-7B)

### Developers

- Updated Mixpanel events for the recent Autofill changes
- Added Mixpanel events to track the phishing warning usage


## 3.3.11 - 2022005-31 - Internal release only

## 3.3.10 - 2022005-30 - Internal release only

## 3.3.9 - 2022005-27 - Internal release only

## 3.3.8 - 2022005-24 - Internal demo of new Add Account flow

## 3.3.7 - 2022-05-23

###Fixed

- Possibly fixed: Title flipping back to "Untitled" after edit
- Fixed: possible crash when moving focus away from email field at a slow phone


## 3.3.6 - 2022-05-20 (internal release only)

### Added

- New (first) screen in Autofill flow when Tilig finds matching accounts


## 3.3.5 - 2022-05-18 - Internal release only

### Design tweaks

- Vertically center the onboarding steps (#250)
- Survey: adjust button paddings (#256)
- Survey: increase button height when text wraps to second line (#256)
- Survey: decrease line height (#256)
- Empty accounts screen: increase padding below title (#258)
- Empty accounts screen: font in search bar too thin (#258)
- Collapsing header: don't compress background blobs vertically (#260)
- Collapsing header: account icon not aligned to vertical center (#260)
- Collapsing header: add padding when icon is at its smallest (#260)
- When editing the title, the selection cursor had the same color as the header background (#275)

### Fixed

- Fetching brand logo and color would reset other input fields (introduced in 3.3.4)
- Matching accounts filter immediately cleared when accounts list opened in popup mode (introduced in 3.3.4)
- Styling tweaks in survey success screen
- Debounce all button clicks. Should help reduce Sentry-reported crashes.

## 3.3.4 - 2022-05-16 - Internal release only

### Added

- New Autofill flow: single entry that opens a popup.
- Faster autofill: new flow no longer requires fetching accounts before filling
- Fetch brand colors, name, icons and 2fa availability
- Phishing detection warns when user attempts to autofill a mismatching app or website

### Changed

- Removed lesser used websites from the suggested accounts list
- Ignore www. or m. prefix when sorting accounts, or when adding
- Tracks when a user creates an account with a website field or with an android_app_id
- Autofill no longer suggests accounts that match only on a part of the android_app_id

### Fixed

- Less crashes when users are monkeys
- New account not saved if the name was still "Untitled"
- Double tap on "Untitled" would edit it instead of replacing it
- Font sizes and margins tweaked throughout the onboarding flow


## 3.2.1 - 2022-04-20

### Added

- Squirrel icon in the inline autofill

### Changed

- When user presses the 'up' button when adding a new account through autofill, they'll return to the target app

### Fixed

- Latest Android could crash when tapping Search or Add new account in inline autofill


## 3.2.0 - 2022-04-15

### Added

- A survey for new users. Note: does NOT appear if autofill is already enabled (i.e. for existing users)
- Password history


## 3.1.7 - 2022-04-14 - Internal only

### Fixed

- When username or notes field does not lose focus explicitly before exiting the screen, edits could be lost
- Clearing fields sometimes not saved


## 3.1.6 - 2022-04-12 - Internal only

### Fixed

- Back button did not autofill the account
- Selecting account for autofill now autofills it immediately instead of repopulating the dropdown
- Up navigation button in 'Add new account' from Autofill flow ineffective
- Small optimization in autofill
- Add new account from autofill now prefills the username (as long as user logged in in v3.1.6)

### Development

- New staging API base URL
- Fixed: password history not updated if password hasn't actually changed


## 3.1.5 - 2022-0-?

### Fixed

- Typo in new onboarding screens
- Yellow banner appears even though Autofill is enabled
- 'More info about logging in' button jumps weirdly when busy indicator shows


## 3.1.4 - 2022-03-30

### Added

- New onboarding screens, with A/B test

### Fixed

- Progress spinner briefly jumps position after pressing a Google/Apple login button
- 'Skip' buttons positions varies slightly between onboarding screens
- 'Visit website' button crashes the app when the URL is valid but incompatible with Android
- Crash when returning an account to a stopped target app for autofilling
- Account placeholder icon missing when creating a new account
- Crash when tapping navigation-related buttons too rapidly multiple times

### Analytics

- Mixpanel events for 2FA-related actions


## 3.1.3 - 2022-03-22

### Added

- Pull-to-refresh in the account list
- Progress icon now turns green and fades nicely when completing onboarding

### Fixed

- Manual OTP input did not scroll well with the keyboard
- Stop adding spaces after every period in the website field
- Clear focus of the password field after generating a password


## 3.1.2 - 2022-03-18

### Fixed

- Turned the OTP field into an encrypted field


## 3.1.2 - 2022-03-17

### Fixed

- Scan instruction message for accounts without website
- Disabled 2fa during an account creation
- Close bottom sheet on back press
- Fixed close secure dialog show multiple times
- Added extra bottom sheet when user has denied camera permission
- Decreased digit font in confirmation screen to fit 8-digit codes


## 3.1.1 - 2022-03-16 (internal only)

### Added

- Support for 2FA-codes

### Fixed

- Scrolling feels sluggish in the account detail screen
- Logging out would exit the app


## 3.1.0 - Mar 2, 2022

### Added

- Collapsing toolbar snaps into place
- New UI for generating passwords
- Improved account matching to URLs by checking for actual main domain. Identical to iOS now.
- When tapping 'Search' in the autofill dropdown, the search field will be prefilled

### Changed

- Major version bumped from 2.0 to 3.1 to align with iOS
- Password is not automatically revealed/concealed when field has focus; eye icon removed

### Fixed

- Android app icons flicker when scrolling in accounts list or account detail screen
- Possible navigation crash after enabling autofill during onboarding
- Crash: fast tapping of progress button can cause the pages to go out of array bounds (multiple Sentry reports)
- Crash: Autofill not available on certain Huawei device with a compatible OS
- Login issues would show an error with illegible dark blue text on dark background
- Crash: some Sentry logs reported initializing the encrypting module before keys were loaded
- 'Add your first account' button drop shadow too large
- 'Delete account' button had different shadow and corner radius than all other buttons
- Icon in autofill for logged-out users was fully solid
- Smaller APK due to a big cleanup

### Debug tools

- Developers can now delete their Firebase account, delete all secrets, or create dummy secrets through the settings
- Debug builds now track events in Mixpanel project `tilig-staging` instead of `tilig-dev` to match the staging backend
- Added Mixpanel's .alias method to improve identity linking


## 2.0.2 - Feb 24, 2022

### Fixed

- Improvement on previous build: Mixpanel identity not set to the backend user ID
- Retroactiely update Mixpanel events that were not properly linked


## 2.0.1 - Feb 23, 2022

### Fixed

- Mixpanel identity not set to the backend user ID


## 2.0.0 - Feb 21, 2022

Complete redesign!


## 1.4.1

### Added

- Test builds will show the API base url in the settings

### Changed

- Removed the http(s)-requirement. Now only adds the protocol when opening the website
- Slightly better matching of accounts to website URLs

### Fixed

- Add http:// when saving an account, not only when unfocussing the edit field
- Fixed passwords appearing in email fields (by improved heuristics when looking for form fields)
- Improved heuristics by using different methods for webview and native apps
- Adding a new account could end up stored as an Android app instead of website url
- Editing an account could insert the website url in the Android app ID field
- No favicon would appear if android_app_id field was not blank
- No favicon would appear for website urls without protocol
- When autofilling websites, non-http urls were handled differently from http urls


## 1.4.0 - Dec 10, 2021

### Fixed

- Improved triggering of autosave functionality


## 1.4.1 - Upcoming

### Changed

- Is Autofill is enabled, then later disabled, the app will offer to enable it again (instead of doing that only once, ever).

### Fixed

- New heuristics should improve showing the Autofill in more apps
- Urls without http(s) not clickable in detail screen
- Hitting 'Save' without leaving edit field first would not add http to the website url
- Editing accounts containing a website without http(s) could be saved as an Android app ID
- Adding a new account (in Website mode) would show the Delete button even though account was new
- Stricter heuristics when looking for input fields to fill; prevents password being filled into email field

### Analytics

- Improved tracking of Autofill Enabled state (upon return from the settings, the value is now immediately detected)
- Improved tracking of Autofill state when the user changes it outside of our app flow.


## 1.4.0 - Internal app, not released

### Fixed

- Autosave not triggering often enough or not at all


## 1.3.8 - Dec 7, 2021

### Fixed

- Editing account where website was not http-ified the account would be edited with Application Id empty
- Adding a new account manually would not enable http://-ification of the website input field
- Even when enabled, automatically adding http:// to manually entered URLs not working
- Disabled form field validation: you can now add/edit accounts without username, password or website
- Better heuristics for apps that don't set autofillHints; autofill dropdown will show in more apps (in more languages) now

### Analytics

- Mixpanel event 'AutoFill Canceled' when autofill takes too long to respond


## 1.3.7 - Dec 3, 2021 - Not released

### Fixed

- Autofill dropdown not showing matching accounts because matching mechanism
- 'Create new login' renamed to 'Add new account' for consistency
- 'Application ID' showed as hint/label in the 'Add new account' flow.
- Application ID values could be prefixed with http:// in the edit screen
- 'Add new account' flow from a website would show {0}://{1} as the website
- 'Add new account' flow from a website would show the browser name instead of website name
- 'Search' flow from the autofill would try to match by browser name, website and/or app ID
- Without any matches, the Search option would not show in the autofill dropdown
- Emptying the search bar did not clear the search filter

### Analytics

- `AutoFill Triggered` triggered too often


## 1.3.6 - Nov 22, 2021

### Analytics

- After a crash or logout, *automatic* Mixpanel events would lose the x-tilig headers


## 1.3.5 - Nov 18, 2021

### Analytics

- After a crash, Mixpanel events would lose the x-tilig- headers


## 1.3.4 - Nov 18, 2021

### Fixed

- AutofillManager-related crashes on pre-Oreo devices

### Analytics

- Track "Autofill Explanation Skipped" and "Autofill Suggestion Skipped" separately
- Track Autofill suggestion and explanation as 2 steps
- "Autofill Skipped" might've still triggered after accepting the suggestion
- "First Password Website Selected" could trigger twice for one account
- "Open System Settings" renamed to "System Settings Opened"
- "Open App Settings" renamed to "App Settings Viewed"
- New events to distinguish opening app settings and system settings
- Renamed event "Select Prefilled Website" to "First Password Website Selected"
- Don't fire "Account List Viewed" event if immediately followed by "First Password Message Appeared"
- "Welcome Instructions Completed" event should trigger when signin is reached, not when signin is completed


## 1.3.3 - Nov 11, 2021

### Fixed

- Possibly fixed the 'bitmap too large' errors caused by oddly shaped icons
- Mixpanel issue: wrong user id
- Mixpanel issue: no user properties stored


## 1.3.2 - Nov 8, 2021

### Added

- Version number in the settings screen

### Fixed

- StructureParser can now work with nullable packageName, just in case it cannot reach the autofillee. Resolved #ANDROID-APP-2G
- Remember if SecurePrefs don't work. Should prevent incidental crashes (for Samsungs)


## 1.3.1 - Nov 2, 2021

### Added

- More Mixpanel events (autofill, search, etc.)

### Fixed

- Mixpanel superProperties not correctly set
- Incidental crash in Apple Sign-in
- Crash when trying to create an icon for empty website fields


## 1.3.0 - Oct 25, 2021

### Added

- Sign in with Apple

### Fixed

- Incidental crash when tapping account in the list rapidly
- Incidental crash when navigating in the onboarding screens rapidly
- Incidental crash when AutofillManager cannot be found on the system
- Autofill no longer working on Android 11 and up
- Consistent Mixpanel event naming with other clients
- Crash when creating a new secret through the autofill flow
- Crash when attempting autofill in certain webviews (Samsung?)


## 1.2.5 - Oct 21, 2021

### Added

- Several new Mixpanel events

### Fixed

- Mixpanel user identity not matching other clients
- Crash in onboarding when swiping at exactly the wrong time
- Missing x-headers
- Autofill crash on Android Marshmallow


## 1.2.4 - Aug 2, 2021

### Fixed

- Unable to parse keys generated on iOS
- Set maximum input field length to 200 characters


## 1.2.3 - Jul 28, 2021

### Added

- Breadcrumbs added to crash reports

### Fixed

- Set maximum input field length to 250 characters


## 1.2.2 - Jul 28, 2021

### Fixed

- Crash in autofill when footer is added to a logged-out presenter (ANDROID-APP-18)
- Crash when user closes the app before sign-in is finished (ANDROID-APP-17)
- Crash when app encounters an empty key


## 1.2.1 - Jul 24, 2021

### Fixed

- Login issues

## 1.2.0 - Jul 22, 2021

### Added

- New API
- Client-side encryption

### Changed

- Don't show autofill suggestion after logging out
- Use hostname for favicons, not full url


## 1.1.6 - Jun 2, 2021

### Added

- Migration notice support

### Changed

- Improved message box design
- Disabled sharing functionality
- Improved error messages


### 1.1.5 - Mar 4, 2021

### Fixed

- Improved settings UI alignment
- Typo
- App exits after adding first secret
- Crash when loading broken icons
- Crash when error message appears at the wrong moment


## 1.1.4 - Feb 3, 2021

### Added

- Show email or username to distinguish multiple similar accounts in Auto-fill pop-up
- Add yellow footer in Auto-fill pop-up
- Add search option in Auto-fill pop-up

## 1.1.3 - Jan 11, 2021

### Fixed

- Autofill suggestion could pop up even when already enabled


## 1.1.2 - Jan 8, 2021

### Added

- Accounts screen shows suggested accounts when the user's account is empty
- x-tilig-version and x-tilig-platform headers in the requests

### Changed

- Moved sign-in to onboarding
- Changed the autofill suggestion to a more friendly bottomsheet style
- Moved the moment of autofill suggestion from first launch to (i) first close and (ii) after adding first item

### Fixed

- App would try to load favicon for empty urls
- Creating new account from Auto-fill in the browser would use 'application id' instead of website url


## 1.1.1 - 2020-12-12

### Added

- Onboarding
- Mixpanel tracking

### Changed

- New typeface in titles
- Statusbar and statusbar icons now change color depending on screen background


## 1.1.0 - 2020-12-07

### Added

- Prevent screenshots and showing details in the app switcher (#12)

### Fixed

- Crash when autofill fields without a classname (ANDROID-APP-M)
- Generating impossible password would show raw JSON error to the user (#19)
- Should not be possible to generate 'impossible' password (length shorter than the sum of its parts) (#20)
- Users could try to share accounts with themselves, resulting in a generic error (#22)


## 1.0.7 - 2020-11-26

### Added

- New Autofill implementation
- Support for Android application IDs
- Mixpanel tracking
- 'Add new account' option in autofill
- Support for autofilling apps that are not optimize for autofilling

### Changed

- New favicon endpoint
- Improved error messages

### Fixed

- Security measure for autofill too strict
- After signing in, the autofill would still show sign-in instead of accounts
- Signing in from autofill prompt should not continue into the app, but return to autofill
- Favicons with null values could crash the app
- Autofill popup text not readable in night mode

### Removed

- Inactivity lock


## 1.0.6 - Upcoming

### Fixed

- Possible fix for NullPointerException in BiometricPrompt (Sentry issue ANDROID-APP-8)
	Workaround implemented for a bug in the androidx library, that was supposedly fixed in lib update 1.0.1.
	More info:
	https://stackoverflow.com/questions/58089766/biometric-prompt-crashing-on-android-9-and-10-on-some-devices
	https://issuetracker.google.com/issues/141838014


## 1.0.3 - 2020-06-17

### Added

- Filter in main screen

### Fixed

- Biometric login would kick user out of the app if it wasn't set up on the device
- Removed `level=X` from request
- Padding around icons made more similar to iOS
- Icons not loading

## 1.0.2 - 2020-05-28

### Added

- Link to the privacy policy


## 1.0.1 - 2020-05-26

### Fixed

- Not all favicons appearing correctly (#152)


## 1.0 - 2020-05-25

### Fixed

- Skipped trying to load favicons for incorrect urls
- Check for cancellation signal when autofilling
- When unable to load accounts, send correct autofill signal


## 0.6 - 2020-05-15

### Added

- More information in the Settings screen
- A skip button to skip setting the autofill service

### Fixed

- When sharing a secret, the input field would not scroll up to be visible above the keyboard
- Submitting an autofill form would try to save immediately instead of open the New Account screen
- Autofill service would list all entries, not just the matching ones.

-------------------------------------------------------

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

