package com.tilig.sample.crypto

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Base64
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.tilig.crypto.Crypto
import com.tilig.sample.crypto.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "CRYPTOSAMPLE"

        private const val TEST_PRIVATE_KEY_1 = "-----BEGIN RSA PRIVATE KEY-----\n" +
                "MIIEpAIBAAKCAQEAxAIsA2hXHNV5QaK4mqXr3mA90j3ftOk3ImFQM3qt/g1nrrIV\n" +
                "l0cvv0kBdjQTlAEnFaMdbMyOtWSMXgqFpX/LBx11MgWIof/IJuIJU8MDOluCchbH\n" +
                "WSzOB8CDo7DS1ASLbHm8VcgdIhqGlGBHOHB6uCTHzn6fZnouvowhej7SKJ0H7tTN\n" +
                "lV86dQEv/SxXP/vlmVRS09ZPW267yzrr6uFdp93ozn6UwRrZCHpaD2vLYFZvKNoC\n" +
                "JE+o1u521iiubO33/CPeJNPqAvDhRbOvLo+kL/i2hTgpauHh7I0mQTcZ3yE0f+I3\n" +
                "QHmuZa+DXPIUuqF+h9i92H3wio/hA9tGWvdZhwIDAQABAoIBADoeCr7X6Tx7b4Cu\n" +
                "pt3z5IQDNVFpVzNlExv2jrk03vI9N0jtNMmuKdIl63EI7+UqklQwEUF5RbczIQuh\n" +
                "Qw9XXPO3IFC8o06lcMtWqj1TzVk4WO1LI8ClMFHMGzwl6d1Dm7OGRPmKZFkAVHca\n" +
                "NQPfFgQTo8rucIUKJWIKWsLMbbPslofJdrr3q9CXxmlAnlITOKeln2w1jj5zyxPr\n" +
                "9rF13Lhe360avQm+kfKodkIJ0ZYrfQyXM0jEak9zGdGnDt9BnKDzZdnrI0jmYd7L\n" +
                "9nt4+rEyxaCi/d2I+CT8/t2YZS4pPG5TS5IfqdUi3LRdxHJvZK8YAsbO0v3C4vbq\n" +
                "z8Sg0kECgYEA8J28mCV+jHGIF6Y3HIXM6hVxvSeDOEnEEZZ0T0+yprNMdMPnZR9Q\n" +
                "rktcs3gC5qSWMtOcigJ37o0lMFgRQoQ0dAMmj5z+rUbX5MGMInmjHDIRMhx++whe\n" +
                "Cs+T9PTaXTIPfCksz0S/kU/Cm0QrOXtjj/ZNi2B3aWkAap/sh0pE+0UCgYEA0IpS\n" +
                "uLLSyKyv6aMQiCfgo69Ua+nPAApeIRfpJXDBITcGH3axMKB6OvP+cTrEpKeN0Lm6\n" +
                "rZEkTtd4ACH3YAWYlOg7SzGAv69tr53LJ9pYel4ObDqojH51pI0mQATqWU8vTSjd\n" +
                "vy1uErGieYytPDTB5VBZUlAqcXcP7VsA5CvGaFsCgYBnPccmITBjYnNacl9DRcXe\n" +
                "ysS6vLF4/W27fir5vOCS1mv/ze8DXKRppLxp5RyZihVISbPpI5YCLV2/mIoPWFqJ\n" +
                "TLDG/j3oa3VHGiTexgJOnszyC3gQjk6LpPV/LXBkfI5YX/JyHLV1zBtcM6u4RlE5\n" +
                "/BzZqNKT0BH1rhj3E871YQKBgQCxglyBfS3wxRWEEVpcG12a1v5MURvzU9/l2X2K\n" +
                "chzi0fxzQcv3sVH55oDaDwUdhsYvOjkNzvMR7VTnAAKaiIjlj3wROFT4VTJPf0hT\n" +
                "ZwdTOcabF58q3+XxqHg7nDOCy+Vej3+ZYOlGHlBHmJhJHDtDn444g6ZtKMS07Hao\n" +
                "wh5D6wKBgQCMypJoD3Jc7xytiaK/19DjTIQ6kZrbfZEzr6QlVuuJ2knIxhAPywKK\n" +
                "xKu2Yi9tuGsKU7oxRJq3diXx176ZS9WVfRHoK2nXgPCWeDXZRzDIIyAmgLpfBmZv\n" +
                "32bNyS08iZVlD2QHrp/7+AtVePPFIVWZk+znBro3R0s63ncX/+5I/w==\n" +
                "-----END RSA PRIVATE KEY-----"

        private const val TEST_PUBLIC_KEY_1 = "-----BEGIN PUBLIC KEY-----\n" +
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxAIsA2hXHNV5QaK4mqXr\n" +
                "3mA90j3ftOk3ImFQM3qt/g1nrrIVl0cvv0kBdjQTlAEnFaMdbMyOtWSMXgqFpX/L\n" +
                "Bx11MgWIof/IJuIJU8MDOluCchbHWSzOB8CDo7DS1ASLbHm8VcgdIhqGlGBHOHB6\n" +
                "uCTHzn6fZnouvowhej7SKJ0H7tTNlV86dQEv/SxXP/vlmVRS09ZPW267yzrr6uFd\n" +
                "p93ozn6UwRrZCHpaD2vLYFZvKNoCJE+o1u521iiubO33/CPeJNPqAvDhRbOvLo+k\n" +
                "L/i2hTgpauHh7I0mQTcZ3yE0f+I3QHmuZa+DXPIUuqF+h9i92H3wio/hA9tGWvdZ\n" +
                "hwIDAQAB\n" +
                "-----END PUBLIC KEY-----"

        const val TEST_PRIVATE_KEY_2 = "-----BEGIN PRIVATE KEY-----\n" +
                "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDitqRwXko9Y+to\n" +
                "cr+oWLOVsOdFP78GCoBIkekhmqQSeSRSPw4gKjw0yX4R3LqmD6N1O4UW8IqlukEG\n" +
                "qRUUujS65IV2Wnb4JaQzVOoly+xOAhT3vmjaHaLmi+uBY05+xLzh+QyPksiFRACK\n" +
                "3UWi+FQde0IOu+i1CDeX8G0c1VwvK54kLO8/N+wcyg4HyJfzV/fdNAddHiYA6o+X\n" +
                "qKX4emdNoGP7EfMtrgywEJmjV9c4C8beIPB9BBYd/8yGTT5BVeGvZgDd3wfG80wl\n" +
                "5wSFv81qJsYbj9QLnTKIFxAxVYIHWcKydMt9tkFl6z7l3cNlZh4Kz+e9ke6mWfNv\n" +
                "gSlc3IC3AgMBAAECggEABUxGWHfjnT6mBClWUD1OaQuaw/QC50W8jLWYMJhYCDR7\n" +
                "hE6D6n2CLaudvhYd6H6KD9XFvOz/GHNbTKgL3ZGiZNGtSsA/cnoStb5gwGRdD8+d\n" +
                "pNJ2yc4Sffe8F6h176FyVoeG4x4HgJG/v37/Pxk1ggDc4NJEVGKuRecw7ZEQ1+tO\n" +
                "QCS4m5CgEwkLBzK8KMMqbNjMDq+dsyESCWgY1pO/pUHZ1TVwds3RvV3UiHkCG5fY\n" +
                "O2hwEd2fnq8vkzmIe5JqV4yukyDSn4Wujrz/ONkrWuLx08kWYOJj4jN8bSwJ6/qy\n" +
                "ZtvzUPRbYZtp8qlRAcmrPTznLT7fDjc/l3VpYxst6QKBgQD7c9esTXjspKfgr9k+\n" +
                "n6OYxXUhHmwtdhL3Vd7NTU6CDxhQx6dTZIN+lJ00IJrx4fsuYkEBRaRlvILnp9/a\n" +
                "iB86Gjw+jLzBLkXozM6Y9CPRs8mm0thpP2CaRmOv0gucnHUVAW6QorMHFXPuqvLB\n" +
                "LAqKP4h+n58GO6R4fCKSo3AWKQKBgQDm0EPAblJ9enQ9hHAqsNcGagh21ocdRpji\n" +
                "JNkZqdHYNIC0fy0qUVnvx3FoTT2tJ5px/uWfGQCpugeXJ/VAxS6lP5M2xejmLE8Z\n" +
                "Oj701H1xdvYePh3F5xqeAh6WAMR9XBSBTVcasZzJ0vdDLetQmlzTlG2JWnORZbob\n" +
                "+KJlfmf73wKBgAs4/148UvJU4v0/O/X0kAuofHLO9csNowkQ9xG4qpFZdR9d2dT5\n" +
                "/nhuz5lc/y1ehaRrVDVnU1ALGtiWAIKzYploRFoSU/ZXR119jpb7BoI8gPqGHIyN\n" +
                "/JTGIXreaBBEV2Fpn/KqoEznozX6wTzn50yGS2RDYYiYrt2FxNFApCcJAoGACdKR\n" +
                "H45SWl6stbV+JIL0E6TNiWklxNwtQ2p4BryQXpgVZhkHbgaaQjcFn3+yAT1vOCy1\n" +
                "zuh8e7yb+BXOPRxnGMc0157ig3EdSKCO71JfqEV8wdq436QvIMxMSuvrcL5LbRlC\n" +
                "fLNUTc55kVVR71MyYhqt8qtzwZtHqUsSdilD+ZcCgYEA6FJDif1GTb3ZkvvVyoWj\n" +
                "aRs5GbZwJ0xAWCSMK06t4RClJI9QP1R96w5fTqV6wRDJEzeFO+aQ5i38H23ttAvV\n" +
                "il0VgopYGI2HqXlkCVj5Cg/9ZzU15NnOn9FxovskvZxqvpfz7otpf+G4o7neP+zj\n" +
                "0+EWG2K/8C/7HaQuK3J6gbw=\n" +
                "-----END PRIVATE KEY-----"

        const val TEST_PUBLIC_KEY_2 = "-----BEGIN PUBLIC KEY-----\n" +
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4rakcF5KPWPraHK/qFiz\n" +
                "lbDnRT+/BgqASJHpIZqkEnkkUj8OICo8NMl+Edy6pg+jdTuFFvCKpbpBBqkVFLo0\n" +
                "uuSFdlp2+CWkM1TqJcvsTgIU975o2h2i5ovrgWNOfsS84fkMj5LIhUQAit1FovhU\n" +
                "HXtCDrvotQg3l/BtHNVcLyueJCzvPzfsHMoOB8iX81f33TQHXR4mAOqPl6il+Hpn\n" +
                "TaBj+xHzLa4MsBCZo1fXOAvG3iDwfQQWHf/Mhk0+QVXhr2YA3d8HxvNMJecEhb/N\n" +
                "aibGG4/UC50yiBcQMVWCB1nCsnTLfbZBZes+5d3DZWYeCs/nvZHuplnzb4EpXNyA\n" +
                "twIDAQAB\n" +
                "-----END PUBLIC KEY-----"
    }

    private lateinit var binding : ActivityMainBinding

    @SuppressLint("MissingSuperCall") // Android Studio is bugging out
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btLoad.setOnClickListener {
            Crypto().apply {
                clearAllFields()
                setKeypairFromString(TEST_PRIVATE_KEY_2, TEST_PUBLIC_KEY_2)
                testCrypto(this)
            }
        }

        binding.btLoadRsa.setOnClickListener {
            Crypto().apply {
                clearAllFields()
                setKeypairFromString(TEST_PRIVATE_KEY_1, TEST_PUBLIC_KEY_1)
                testCrypto(this)
            }
        }

        binding.btGenerate.setOnClickListener {
            Crypto().apply {
                generateKey()
                testCrypto(this)
            }
        }
    }

    private fun testCrypto(crypto: Crypto) {
        showKeyPair(crypto)

        val sourceText = binding.tvInput.text.toString().trim()
        val cipherText = crypto.legacyEncrypt(sourceText)
        showCipherText(cipherText ?: "(null)")
        cipherText?.let {
            val decrypted = crypto.legacyDecrypt(it)
            showOriginalText(decrypted ?: "(null)")
        }

        val dek = crypto.generateDek()
        binding.tvDek.text = Base64.encodeToString(dek, Base64.DEFAULT)
        val cipherTextSodium = crypto.encryptUsingDek(dek, sourceText)
        binding.tvEncryptedSodium.text = cipherTextSodium
        val decryptedSodium = crypto.decryptUsingDek(dek, cipherTextSodium!!)
        binding.tvDecryptedSodium.text = decryptedSodium
    }

    private fun convertKeyToStringAndBack(crypto: Crypto): Crypto {
        val keyPair = crypto.getKeyPairs()
        val privateKey = crypto.legacyPrivateKeyAsString()!!
        val publicKey = crypto.legacyPublicKeyAsString()!!

        val copy = Crypto().apply {
            setLegacyKeypairFromString(privateKey, publicKey)
        }
        return copy
    }

    private fun clearAllFields() {
        binding.tvPrivate.text = ""
        binding.tvPublic.text = ""
        binding.tvEncrypted.text = ""
        binding.tvPublic.text = ""
    }

    private fun showKeyPair(crypto: Crypto) {

        val privateKey = crypto.legacyPrivateKeyAsString()!!
        val publicKey = crypto.legacyPublicKeyAsString()!!

        binding.tvPrivate.text = privateKey
        binding.tvPublic.text = publicKey

        Log.v(TAG, privateKey)
        Log.v(TAG, publicKey)
    }

    private fun showCipherText(text: String) {
        binding.tvEncrypted.text = text
        Log.v(TAG, "cipher text:")
        Log.v(TAG, text)
    }

    private fun showOriginalText(text: String) {
        binding.tvDecrypted.text = text
        Log.v(TAG, "original text:")
        Log.v(TAG, text)
    }
}
