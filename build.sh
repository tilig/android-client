#!/bin/bash

# Show chooser
while true; do
  echo "Please choose what to do:"
  echo "  [1] Run all tests"
  echo "  [2] Build and distribute a build for QA"
  echo "  [3] Build the release APK"
  echo "  [4] All of the above"
  echo "  [c] Cancel"
  echo ""

  read -n1 -s n
  if [[ $n == "c" ]]; then
    echo "Canceled."
    exit 0
  elif [[ $n == "1" ]]; then
    ./gradlew :app:testDebugUnitTest :crypto:test :app:pixel6DebugAndroidTest --stacktrace
    status=$?
    if [ $status -ne 0 ]; then
      open ./crypto/build/reports/tests/testDebugUnitTest/index.html
      open ./app/build/reports/tests/testDebugUnitTest/index.html
      exit $status
    fi
    break
  elif [[ $n == "2" ]]; then
    ./gradlew :app:testDebugUnitTest :crypto:test :app:assembleQa :app:appDistributionUploadQa --stacktrace
    break
  elif [[ $n == "3" ]]; then
    ./gradlew clean :app:testDebugUnitTest :crypto:test :app:assembleRelease --stacktrace
    echo "Opening result folder..."
    open ./app/build/outputs/apk/release/
    break
  elif [[ $n == "4" ]]; then
    ./gradlew :app:testDebugUnitTest :crypto:test :app:pixel6DebugAndroidTest :app:assembleQa :app:appDistributionUploadQa :app:assembleRelease --stacktrace
    break
  else
    echo "Let's try that again."
    continue
  fi
done

echo "Done."
