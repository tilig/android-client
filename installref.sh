#!/bin/bash

adb uninstall com.tilig.android

# Enable Firebase Analytics debug mode
adb shell setprop debug.firebase.analytics.app com.tilig.android
adb shell setprop log.tag.FA VERBOSE
adb shell setprop log.tag.FA-SVC VERBOSE

echo "Debugging enabled."
read -p "Press enter to continue"

# Open Play store with utm tags. Do NOT press the install button!
adb shell am start -a android.intent.action.VIEW -d 'https://play.google.com/store/apps/details?id=com.tilig.android&referrer=utm_source%3Dtest_source%26utm_medium%3Dtest_medium%26utm_term%3Dtest-term%26utm_content%3Dtest_content%26utm_campaign%3Dtest_name'
echo "Play Store opened. Do not press INSTALL!"
read -p "Press enter to continue"

# Install the app
adb install -r app/build/outputs/apk/release/app-release.apk
echo "App installed. Please press OPEN"
read -p "Press enter to continue"

# Launch installreferrer thingy
adb shell am broadcast -a com.android.vending.INSTALL_REFERRER -n com.tilig.android/com.google.android.gms.analytics.CampaignTrackingReceiver --es "referrer" "utm_source=test_source\&utm_medium=test_medium\&utm_term=test-term\&utm_content=test_content\&utm_campaign=test_name"

# Start Logcat with the FA filter
adb logcat -v time -s FA FA-SVC

# Disable Firebase Analytics debug mode
# adb shell setprop debug.firebase.analytics.app .none.
